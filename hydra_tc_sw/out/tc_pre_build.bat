echo off
rem This script expects to be called in the Eclipse pre-build steps.
rem Having in a single file is much easier to manage and track changes
rem compared to having each in the .cproject file.
rem 
rem Use by calling this bash file in the Eclipse build steps:
rem   1. Navigate to Project Properties->Settings->Build Steps->Pre-build steps
rem   2. Update to include calling the bash script like so:
rem   arm-none-eabi-gcc --version; arm-none-eabi-gcc -Xlinker --version; ../out/tc_pre_build.bat;
rem 
rem Note that the eclipse CWD is the build dir so relative paths are from there.
rem
rem Note that this script is a batch (.bat) script since it can call .exe
rem files, which were converter from .py scripts using PyInstaller.

set build_dir=%cd%
set base_dir=..\..\submodules\pt_ledger\m1b2
set pt_dir=%base_dir%\hydra_tc_pt
set bin_dir=%base_dir%\bin

set git_exe=..\..\tools\git\bin\git.exe
set code_gen_script=%bin_dir%\pt_gen_helper_tc.exe
set srec_gen_script=%bin_dir%\pt_gen_srec_tc.exe
set code_dir=%pt_dir%\code
set pt_dflt_dir=%pt_dir%\top_assembly_defs\A00000_tc_default
set pt_fmt_csv=%pt_dflt_dir%\A00000_tc_default_pt_tc.csv
set pt_def_json=%pt_dflt_dir%\A00000_tc_default.json

set pt_hdr_hfile=%code_dir%\default_pt.h
set pt_src_cfile=%code_dir%\default_pt.c
set pt_hdr_new_hfile=%code_dir%\default_pt.h.autogen
set pt_src_new_cfile=%code_dir%\default_pt.c.autogen

set pt_gen_dir=..\out\pt_default
set pt_bin_file=%pt_gen_dir%\A00000_tc_default_no_hdr.bin
set pt_srec_file=%pt_dflt_dir%\A00000_tc_default.srec

mkdir %pt_gen_dir%

rem Call the exe version of the python script that generates the code files from the PT
%code_gen_script% --format_csv=%pt_fmt_csv% --pt_json=%pt_def_json% --infile_h=%pt_hdr_hfile% --infile_c=%pt_src_cfile%

rem Move the newly generated .h and .c files to replace the originals
move "%pt_hdr_new_hfile%" "%pt_hdr_hfile%"
move "%pt_src_new_cfile%" "%pt_src_cfile%"

rem Generate the .srec of the PT by first creating a base binary file without header info,
rem then generating a full .srec that is output to the same dir as the PT json used, and
rem finally copy it over into the eclipse build dir to be used during post-build steps for
rem final full srec creation.
%code_gen_script% --format_csv=%pt_fmt_csv% --pt_json=%pt_def_json% --out_bin=%pt_bin_file%

rem Change to the pt directory so the srec gen script can use git commands to get the version
echo %pt_dir%
chdir %pt_dir%
%build_dir%\%srec_gen_script% --json=%build_dir%\%pt_def_json% --eclipse_bin=%build_dir%\%pt_bin_file% --git_path=%build_dir%\%git_exe%

rem Return to the CWD of the caller to the batch script
cd %build_dir%

move "%pt_srec_file%" "%pt_gen_dir%"
echo on
