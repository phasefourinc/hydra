Cloning to a new project
1. Copy the FSW project dir, “tools” dir, and mount_m.bat into a new git repo as a starting point

2. Run mount_m.bat in the new repo to mount the new repo into the M: drive
    a. M: drive is used universally for building FSW code because there are hardcoded paths in order
       to get the SDK/ProcessorExpert version controlled in git.

       NOTE: It is NOT recommended to do the following unless users really wish to change the
       drive or SDK path as it will take some time to get right. If you really want to and be
       backwards compatible so you can still compile older FSW then do the following:
        i. Rename all files that have a hard-coded M:/tools/… path in the new repo to the new drive
           and path. Don’t forget hidden files.
        ii. Rename the SDK in windows explorer to a new name to avoid conflicts
            e.g. tools/nxp/S32DS/S32_SDK_RTM_3.0.0/tools/pex/Repositories/SDK_S32K1xx_15_Repository ->
                 tools/nxp/S32DS/S32_SDK_RTM_3.0.0_FSW/tools/pex/Repositories/SDK_S32K1xx_15_Repository_FSW
        iii. Update tools\nxp\S32DS\install_sdk.bat to point to the new URL
        iv. Run tools\nxp\S32DS\install_sdk.bat to install this SDK to S32 IDE

3. Open S32 IDE and File->Open Projects From Filesystem-> Search M: drive and add the project

4. Right-click the project and “Rename…” it to the new project name. This should update most
   of the items and processor expert properly.

5. Refresh policy does not get updated. Can update manually via .cproject per
   https://community.nxp.com/t5/S32-Design-Studio/refresh-policy-problem-S32K142-EVB/m-p/818105

6. Debug and launch configurations must be updated manually as well

7. Any project links (e.g. to param tables) must be updated manually in the .project file to point
   to the new location

8. Update the pre-build and release scripts under /out dir
