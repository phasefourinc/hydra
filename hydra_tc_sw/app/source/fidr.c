/* Copyright (c) 2017-2020 by Phase Four, Inc. All Rights Reserved. Confidential and Proprietary.
 *
 * fidr.c
 */

#include "fidr.h"
#include "mcu_types.h"
#include "ErrAO.h"
#include "default_pt.h"
#include "stdio.h"

#define FLOAT_EQUAL_TOLERANCE    0.1f

/* Arguments for the FIDR functions
 * Each of these arguments are custom selected relative to the type of function being called on.
 * This gives a lot of flexibility on making a specific function to determine a specific error condition.
 * To determine what each of these functions mean look at the argument type data struct.
 *
 * NOTE: These are initialized in the fdir init function
 */

two_eq_u32_arg_type RftPcuCommDropout_args;
two_eq_u32_arg_type RftDaqCommDropout_args;

enum FDIR_Record_Idx
{
    RFT_PCU_HEARTBEAT_DROPOUT = 0,
    RFT_DAQ_COMM_DROPOUT,
    TOTAL_FDIR_RECORD_CNT
};


/* FIDR table.
 * This is the table that is used to determine what error goes with what function and what arguments to put in said function.
 *
 *                      Error to trigger                    pre condition func                            function to determine FIDR     Arguments to be used in function        FIDR Enable
 */
FDIR_Record FDIR_Table [] =
{
        {(const err_type**) &Errors.RftPcuHeartbeatDropout,    is_prop_connected_and_pp_or_wf_enabled,      &prop_comm_check_u32,          (void*) &RftPcuCommDropout_args,        true },
        {(const err_type**) &Errors.RftDaqCommDropout,         is_inverter_connected,                       &greater_then_func_u32,        (void*) &RftDaqCommDropout_args,        true },
};


#define FDIR_INIT_2EQ(idx, tlm_data_item, en, prsis_sec, lim) do {                                               \
        fdir_idx_cnt[idx]++;                                                                                     \
        FDIR_Table[idx].enabled = en;                                                                            \
        ((two_equality_arg_type *)FDIR_Table[idx].args)->super.persistence_amount = prsis_sec / FRAME_PERIOD_MS; \
        ((two_equality_arg_type *)FDIR_Table[idx].args)->super.curr_persist_cnt   = 0UL;                         \
        ((two_equality_arg_type *)FDIR_Table[idx].args)->comparison_value         = lim;                         \
        ((two_equality_arg_type *)FDIR_Table[idx].args)->tlm_data                 = &tlm_data_item; } while(0)

#define FDIR_INIT_2EQ_U32(idx, tlm_data_item, en, prsis_sec, lim) do {                                         \
        fdir_idx_cnt[idx]++;                                                                                   \
        FDIR_Table[idx].enabled = en;                                                                          \
        ((two_eq_u32_arg_type *)FDIR_Table[idx].args)->super.persistence_amount = prsis_sec / FRAME_PERIOD_MS; \
        ((two_eq_u32_arg_type *)FDIR_Table[idx].args)->super.curr_persist_cnt   = 0UL;                         \
        ((two_eq_u32_arg_type *)FDIR_Table[idx].args)->comparison_value         = (uint32_t)lim;               \
        ((two_eq_u32_arg_type *)FDIR_Table[idx].args)->tlm_data                 = &tlm_data_item; } while(0)


void fidr_init(void)
{
    /* Validate that the enumerated index has the same count as the FDIR Record */
    DEV_ASSERT(TOTAL_FDIR_RECORD_CNT == ((sizeof(FDIR_Table)/sizeof(FDIR_Table[0]))));

    uint8_t fdir_idx_cnt[TOTAL_FDIR_RECORD_CNT] = {0U};
    uint32_t i;

    /* Adding these as placeholders for FDIR though likely disabled in PT */
    FDIR_INIT_2EQ_U32(RFT_PCU_HEARTBEAT_DROPOUT,    RftData.prop_comm_connection_status,      1UL,                                100,                                0UL);
    FDIR_INIT_2EQ_U32(RFT_DAQ_COMM_DROPOUT,         RftData.daq_consec_timeout_cnt,           1UL,                                100,                                1UL);

    /* Each macro increments the idx cnt. Check these
     * to ensure that all items in the FDIR table have
     * been initialized once. This is a dev check only,
     * therefore DEV_ASSERT should suffice. */
    for (i = 0UL; i < TOTAL_FDIR_RECORD_CNT; ++i)
    {
        DEV_ASSERT(fdir_idx_cnt[i] == 1U);
    }

    return;
}

/*
 * CURRENTLY NOT USED. THIS IS A IDEA TO HAVE A GENEREAL FUNCTION TO DO ALL FIDR CHECK> IT MIGHT BE NOT AS FAST HOWEVER.
 * STILL UP IN THE AIR IF THIS WILL HAVE SOME USE.
 *
 *
bool equality_function(FDIR_Record record)

{
    bool passed_eqaulity;
    switch (record.arguments.two_equality_arg.equality_value)
    {
        case EQUAL:
            if (TLM_DATA(record) == COMPARISION_VALUE(record))
            {
                passed_eqaulity = true;
            }
            else{
                passed_eqaulity = false;
            }
            break;

        case NOT_EQUAL:
            if (TLM_DATA(record) != COMPARISION_VALUE(record))
            {
                passed_eqaulity = true;
            }
            else{
                passed_eqaulity = false;
            }

            break;
        case GREATER_THAN:
            if (TLM_DATA(record) >COMPARISION_VALUE(record))
            {
                passed_eqaulity = true;
            }
            else{
                passed_eqaulity = false;
            }

            break;
        case LESS_THAN:
            if (TLM_DATA(record) <COMPARISION_VALUE(record))
            {
                passed_eqaulity = true;
            }
            else{
                passed_eqaulity = false;
            }
            break;
        case GREATER_THAN_OR_EQUAL_TO:
            if (TLM_DATA(record) >=COMPARISION_VALUE(record))
            {
                passed_eqaulity = true;
            }
            else{
                passed_eqaulity = false;
            }
            break;
        case LESS_THAN_OR_EQUAL_TO:
            if (TLM_DATA(record)<=COMPARISION_VALUE(record))
             {
                 passed_eqaulity = true;
             }
             else{
                 passed_eqaulity = false;
             }
             break;
    }

    if (passed_eqaulity ==true)
    {
    _report_fidr_error(&AO_Err,(err_type *)*(record.error_fidr),SEND_TO_SPACECRAFT,(float)*record.arguments.two_equality_arg.tlm_data);
    return true;
    }
    return false;

}
*/

/*Comparison Function: Greater Then Function
 *  This function compares a value from memory to see if it is greater then a known float value.
 *  If the check is true it then gets checked for persistence.
 *  If it pass persistence it will log a error to the Error AO
 */
bool greater_then_func(FDIR_Record *record)
{
    bool error_logged = false;
    two_equality_arg_type *arguments = (two_equality_arg_type*) record->args;
    if ((float)*arguments->tlm_data > arguments->comparison_value)
    {
        error_logged = check_persistence_and_log_error(record,(float)*arguments->tlm_data);
    }
    else
    {
        arguments->super.curr_persist_cnt = ZERO_COUNT;
    }
    return error_logged;

}


/*Comparison Function: Greater Then Or Equal To Function
 *  This function compares a value from memory to see if it is greater then or equal to a known float value.
 *  If the check is true it then gets checked for persistence.
 *  If it pass persistence it will log a error to the Error AO
 */
bool greater_then_eq_func(FDIR_Record *record)
{
    bool error_logged = false;
    two_equality_arg_type *arguments = (two_equality_arg_type*) record->args;
    if ((float)*arguments->tlm_data >=arguments->comparison_value)
    {
        error_logged = check_persistence_and_log_error(record,(float)*arguments->tlm_data);
    }
    else
    {
        arguments->super.curr_persist_cnt = ZERO_COUNT;
    }
    return error_logged;

}


/*Comparison Function: Less Then Function
 *  This function compares a value from memory to see if it is less then to a known float value.
 *  If the check is true it then gets checked for persistence.
 *  If it pass persistence it will log a error to the Error AO
 */
bool less_then_func(FDIR_Record *record)
{

    two_equality_arg_type *arguments = (two_equality_arg_type*) record->args;
    bool error_logged = false;
    if ((float)*arguments->tlm_data <arguments->comparison_value)
    {
        error_logged = check_persistence_and_log_error(record,(float)*arguments->tlm_data);
    }
    else{
        arguments->super.curr_persist_cnt = ZERO_COUNT;
    }
    return error_logged;

}


/*Comparison Function: Less Then Or Equal To Function
 *  This function compares a value from memory to see if it is less then or equal to a known float value.
 *  If the check is true it then gets checked for persistence.
 *  If it pass persistence it will log a error to the Error AO
 */
bool less_then_eq_func(FDIR_Record *record)
{
    bool error_logged = false;
    two_equality_arg_type *arguments = (two_equality_arg_type*) record->args;
    if ((float)*arguments->tlm_data <=arguments->comparison_value)
    {
        error_logged = check_persistence_and_log_error(record,(float)*arguments->tlm_data);
    }
    else
    {
        arguments->super.curr_persist_cnt = ZERO_COUNT;
    }
    return error_logged;

}



/*Comparison Function: Equal Comparison Function
 *  This function compares a value from memory to see if it is equal to a known float value.
 *  If the check is true it then gets checked for persistence.
 *  If it pass persistence it will log a error to the Error AO
 */
bool equal_comp_func(FDIR_Record *record)
{
    bool error_logged = false;
    two_equality_arg_type *arguments = (two_equality_arg_type*) record->args;
    if ((float)*arguments->tlm_data <= arguments->comparison_value +(arguments->comparison_value*FLOAT_EQUAL_TOLERANCE)  && (float)*arguments->tlm_data >= arguments->comparison_value -(arguments->comparison_value*FLOAT_EQUAL_TOLERANCE))
    {
        error_logged = check_persistence_and_log_error(record,(float)*arguments->tlm_data);
    }
    else
    {
        arguments->super.curr_persist_cnt = ZERO_COUNT;
    }
    return error_logged;

}

/*Comparison Function: Not Equal Comparison Function
 *  This function compares a value from memory to see if it is not equal to a known float value.
 *  If the check is true it then gets checked for persistence.
 *  If it pass persistence it will log a error to the Error AO
 */
bool not_equal_comp_func(FDIR_Record *record)
{
    bool error_logged = false;
    two_equality_arg_type *arguments = (two_equality_arg_type*) record->args;
    if ((float)*arguments->tlm_data > arguments->comparison_value +(arguments->comparison_value*FLOAT_EQUAL_TOLERANCE)  || (float)*arguments->tlm_data < arguments->comparison_value -(arguments->comparison_value*FLOAT_EQUAL_TOLERANCE))
    {
        error_logged = check_persistence_and_log_error(record,(float)*arguments->tlm_data);
    }
    else
    {
        arguments->super.curr_persist_cnt = ZERO_COUNT;
    }
    return error_logged;

}




/*Comparison Function of two telemetry: Greater Then Function
 *  This function compares a value from memory to see if it is greater then another value from memory.
 *  This can be used to compare two adc values for example.
 *  If the check is true it then gets checked for persistence.
 *  If it pass persistence it will log a error to the Error AO
 */
bool tlmvtlm_greater_then_func(FDIR_Record *record)
{
    two_tlm_eqaulity_arg_type *arguments = (two_tlm_eqaulity_arg_type*)record->args;
    bool error_logged = false;
    float aux_data;
    if ((float)*arguments->tlm_data1 > (float)*arguments->tlm_data2)
    {
        if (arguments->aux_data_select == ARG1)
        {
            aux_data = (float)*arguments->tlm_data1;
        }
        else
        {
            aux_data = (float)*arguments->tlm_data2;
        }
        error_logged = check_persistence_and_log_error(record, aux_data);
    }
    else
    {
        arguments->super.curr_persist_cnt = ZERO_COUNT;
    }
    return error_logged;

}

/*Comparison Function: Greater Then Function
 *  This function compares a value from memory(casted to uint32_t) to see if it is greater then a known uint32 value.
 *  If the check is true it then gets checked for persistence.
 *  If it pass persistence it will log a error to the Error AO
 */
bool greater_then_func_u32(FDIR_Record *record)
{
    bool error_logged = false;
    two_eq_u32_arg_type *arguments = (two_eq_u32_arg_type*) record->args;
    if ((uint32_t)*arguments->tlm_data > arguments->comparison_value)
    {
        error_logged = check_persistence_and_log_error(record,(float)*arguments->tlm_data);
    }
    else
    {
        arguments->super.curr_persist_cnt = ZERO_COUNT;
    }
    return error_logged;

}



/*Custom Function: Prop Communication Check Function
 *
 *  This function  is the same as the Equal Comparison Function u32.
 *  The only difference is that the comparison is uint32_t instead of float, and if the statement is false it sets the value back to 0.
 *  The reason for this is that we want to check if the RxAO gets a good packet from the Prop Controller .
 *  And this is done by setting the value back to 1 if the thruster controller gets a valid packet from the prop controller in RxAO.
 *  Search for the prop_comm_connection_status in RxAO.
 *  If the check is true it then gets checked for persistence.
 *  If it pass persistence it will log a error to the Error AO
 */
bool prop_comm_check_u32(FDIR_Record *record)
{
    bool error_logged = false;
    two_eq_u32_arg_type *arguments = (two_eq_u32_arg_type*) record->args;
    if ((uint32_t)*arguments->tlm_data == arguments->comparison_value)
    {
        error_logged = check_persistence_and_log_error(record,(float)*arguments->tlm_data);
    }
    else
    {
        *(arguments->tlm_data)= (uint32_t)0UL;
        arguments->super.curr_persist_cnt = ZERO_COUNT;
    }
    return error_logged;

}



/*Check Persistence Function
 *  This function is a lower level function that is used if the fidr requires persistence.
 *  This functions uses the arguments to check if the current persistence value is equal to the persistence limit specified from initaliziation.
 *  If it is the current persistence value and the persistence limit are the same it will log the fidr error.
 *  If it is not the same it will just increment by one.
 *
 *  NOTE: That if it goes beyond the persistence value, it will never report another error.
 *  It will report another error once the count is cleared by having the condition that cause fidr cleared.
 */

bool check_persistence_and_log_error(FDIR_Record *record, float aux_data)
{
    two_equality_arg_type *arguments = (two_equality_arg_type*) record->args;
    bool error_logged = false;

    /* Check for exact equality since no errors should be reported if the
     * fault remains active without having been cleared first */
    if (arguments->super.curr_persist_cnt == arguments->super.persistence_amount)
    {
        _report_fidr_error(&AO_Err, (err_type *)*(record->error_fidr), SEND_TO_SPACECRAFT, aux_data);
        error_logged = true;
    }

    if (arguments->super.curr_persist_cnt < 0xFFFFFFFFUL)
    {
        ++arguments->super.curr_persist_cnt;
    }
    return error_logged;

}

/*This is the main function that runs all of FIDR
 * This function first checks if the FIDR is enabled.
 * If it is, it will check the FIDR the condition to see if it passed or failed.
 * */

void run_fidr(void)
{
    uint32_t i= 0UL;
    FDIR_Record *fdir_rec;

    if (RftData.state != MAX_WILD_WEST)
    {
        for (i = 0UL; i < TOTAL_FDIR_RECORD_CNT; ++i)
        {
            fdir_rec = &FDIR_Table[i];
            if ((fdir_rec->enabled==true) && (fdir_rec->prefunc() == true))
            {
                fdir_rec->fidr_func(fdir_rec);
            }
        }
    }

    return;
}


bool fidr_has_active_fault(void)
{
    uint32_t i= 0UL;
    bool in_fdir = false;
    FDIR_Record *fdir_rec;
    fdir_arg_base_t *fdir_arg_base;

    if (RftData.state != MAX_WILD_WEST)
    {
        for (i = 0UL; (i < TOTAL_FDIR_RECORD_CNT) && (!in_fdir); ++i)
        {
            fdir_rec = &FDIR_Table[i];
            fdir_arg_base = (fdir_arg_base_t *)fdir_rec->args;

            /* Check active FDIR items for a persistence count that is not cleared */
            in_fdir = fdir_rec->enabled && (fdir_arg_base->curr_persist_cnt > fdir_arg_base->persistence_amount);
        }
    }

    return in_fdir;
}


void fidr_log_all_persistant_faults(void)
{
    uint32_t i= 0UL;
    bool in_fdir = false;
    FDIR_Record *fdir_rec;
    fdir_arg_base_t *fdir_arg_base;

    reported_error_record current_error;
    current_error.aux_data = 0UL;
    current_error.glados_state = RftData.state;
    current_error.send_to_spacecraft = SEND_TO_SPACECRAFT;

    for (i = 0UL; (i < TOTAL_FDIR_RECORD_CNT); ++i)
    {
        fdir_rec = &FDIR_Table[i];
        fdir_arg_base = (fdir_arg_base_t *)fdir_rec->args;

        /* Check active FDIR items for a persistence count that is not cleared */
        in_fdir = fdir_rec->enabled && (fdir_arg_base->curr_persist_cnt > fdir_arg_base->persistence_amount);

        if (in_fdir)
        {
            current_error.err_type_pointer = (err_type *)*(fdir_rec->error_fidr);
            write_telem_record(current_error);
            write_sram_record(current_error);
        }
    }

    return;
}


bool no_pre_func(void)
{
    return true;
}




bool is_state_sw_update(void)
{
    return (RftData.state == MAX_SW_UPDATE);
}

bool is_pp_or_wf_enabled(void)
{
    return ((RftData.gpio_pp_en_fb) || (RftData.gpio_wf_en_fb ));
}

bool is_prop_connected_and_pp_or_wf_enabled(void)
{
    return ((RftData.prop_connected_flag) &&(is_pp_or_wf_enabled()));
}

bool is_inverter_connected(void)
{
    return (RftData.inverter_connected_flag);
}


