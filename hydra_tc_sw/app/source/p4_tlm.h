/* Copyright (c) 2017-2020 by Phase Four, Inc. All Rights Reserved. Confidential and Proprietary.
 *
 * p4_tlm.h
 */

#ifndef SOURCE_P4_TLM_H_
#define SOURCE_P4_TLM_H_

#include "mcu_types.h"


typedef struct Err_Rec_tag
{
    uint16_t err_code;
    uint32_t err_data;
} BYTE_PACKED Err_Rec_t;


typedef struct Tlm_Err_Rft_Rec_tag
{
    uint16_t powerup_err_code;
    Err_Rec_t err1;
    Err_Rec_t err2;
    Err_Rec_t err3;
    uint16_t err_cnt;
} BYTE_PACKED Tlm_Err_Rft_Rec_t;


typedef struct Status_Err_Rft_Rec_tag
{
    uint16_t sc_err_1;
    uint16_t sc_err_2;
    uint16_t sc_err_3;
} BYTE_PACKED Status_Err_Rft_Rec_t;


/* This is how the errors are structured in sram */
typedef struct Err_Rec_Intr_tag
{
    uint16_t last_err_from_powerup;
    Err_Rec_t err1;
    Err_Rec_t err2;
    Err_Rec_t err3;
    Err_Rec_t err_hx1;
    Err_Rec_t err_hx2;
    Err_Rec_t err_hx3;
    uint16_t most_freq_err_code;
    uint16_t most_freq_err_cnt;
    uint16_t total_err_cnt;
} BYTE_PACKED Err_Rec_Intr_t;



typedef struct Err_Rft_Rec_tag
{
    Err_Rec_Intr_t tlm_err;
    Status_Err_Rft_Rec_t status_err;
} BYTE_PACKED Err_Rft_Rec_t;


typedef struct P4Tlm_TcStatus_tag
{
    uint16_t tc_ack_status;
    uint16_t sc_tc_err_code1;
    uint16_t sc_tc_err_code2;
    uint16_t sc_tc_err_code3;
    uint8_t tc_state;
    uint8_t tc_busy;
    uint8_t tc_needs_reset;
    uint8_t tc_app_select;
    uint16_t tc_acpt_cnt;
    uint16_t tc_rjct_cnt;
    uint32_t tc_unix_time_us_hi;
    uint32_t tc_unix_time_us_lo;

} BYTE_PACKED P4Tlm_TcStatus_t;


typedef struct P4Tlm_RftTlm_tag
{
    P4Tlm_TcStatus_t tc_status;

    Tlm_Err_Rft_Rec_t tlm_err_rec;

    uint8_t  tc_bl_select;
    uint8_t  tc_pt_select;

    uint32_t tc_bl_crc;
    uint32_t tc_app_crc;
    uint32_t tc_pt_crc;

    uint32_t tc_rcm_value;
    uint8_t  tc_wdt_rst_trig;
    uint8_t  tc_wdt_rst_tog_sel;
    uint8_t  tc_wdi_fb;
    uint8_t  tc_fsw_cpu_util;

    /* External ADC 0 Signals */
    uint32_t tc_tfet;
    uint32_t tc_tchoke;
    uint32_t tc_tcoil;
    uint32_t tc_tic;
    uint32_t tc_tpl;
    uint32_t tc_tmatch;
    uint32_t tc_ehvd;
    uint32_t tc_ihvd;
    uint32_t tc_tadc0;

    /* Internal ADC Signals */
    uint32_t tc_ebus;
    uint32_t tc_e12v;
    uint32_t tc_e5v;
    uint32_t tc_evb;
    uint32_t tc_ibus;
    uint32_t tc_i12v;
    uint32_t tc_ivb;
    uint32_t tc_tfb_in;
    uint32_t tc_tfb_out;
    uint32_t tc_tfram;
    uint32_t tc_tmcu;

    uint16_t tc_tfet_raw;
    uint16_t tc_tchoke_raw;
    uint16_t tc_tcoil_raw;
    uint16_t tc_tic_raw;
    uint16_t tc_tpl_raw;
    uint16_t tc_tmatch_raw;
    uint16_t tc_ivb_raw;
    uint16_t tc_tfb_in_raw;
    uint16_t tc_tfb_out_raw;
    uint16_t tc_tfram_raw;
    uint16_t tc_tmcu_raw;

    uint16_t tc_pwm_pp_vset_raw_fb;
    uint32_t tc_pp_vset_fb;
    uint32_t tc_wav_gen_freq_fb;
    uint32_t tc_ecc_sram_1bit_cnt;

    uint8_t  tc_data_mode;
    uint8_t  tc_wf_en_fb;
    uint8_t  tc_pp_en_fb;

    uint8_t  tc_inv_connected;
    uint8_t  tc_rft_connected;
    uint8_t  tc_hp_connected;

} BYTE_PACKED P4Tlm_RftTlm_t;


#define P4TLM_RFT_STATUS_PAYLOAD_SIZE    sizeof(P4Tlm_TcStatus_t)
#define P4TLM_RFT_TLM_PAYLOAD_SIZE       sizeof(P4Tlm_RftTlm_t)

#endif /* SOURCE_P4_TLM_H_ */
