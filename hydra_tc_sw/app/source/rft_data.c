/* Copyright (c) 2017-2020 by Phase Four, Inc. All Rights Reserved. Confidential and Proprietary.
 *
 * rft_data.c
 */


#include "rft_data.h"
#include "mcu_types.h"
#include "timer64.h"
#include "max_info_driver.h"

#define BL1_HDR_ADDR    0x00000F00UL
#define BL2_HDR_ADDR    0x00001F00UL

RftData_t RftData = {0};

void thr_data_init(void)
{
    Img_Header_Rec_t *bl_hdr;

    /* At each reset, reset the timestamp sync for the LPIT from 0 (down-counter) */
    RftData.time_sync_lpit_hi = 0xFFFFFFFFUL;
    RftData.time_sync_lpit_lo = 0xFFFFFFFFUL;

    RftData.bl_select  = (uint8_t)app_hdr_rec->dna_record.bl_select;
    RftData.app_select = (uint8_t)app_hdr_rec->dna_record.app_select;

    if (RftData.bl_select == 1U)
    {
        bl_hdr = (Img_Header_Rec_t *)BL1_HDR_ADDR;
    }
    else
    {
        bl_hdr = (Img_Header_Rec_t *)BL2_HDR_ADDR;
    }

    RftData.bl_crc  = bl_hdr->img_crc;
    RftData.app_crc = app_hdr_rec->app_record.img_crc;

    /*Assuming the inverter is not connected. Once we got a transfer valid value we will set it to true.*/
    RftData.inverter_connected_flag = false;
    RftData.prop_connected_flag=false;
    RftData.prop_comm_connection_status=0UL;

    return;
}


/* Tracking the current unix time is a multi-step process:
 *  1. FSW receives a 64-bit unix time via command
 *  2. FSW associates/syncs the unix time with a 64-bit LPIT tick value
 *  3. Upon each new OS frame, the current time is recalculated:
 *      a. Current 64-bit LPIT tick value is obtained
 *      b. The time elapsed since the sync LPIT tick value is determined
 *      c. The time elapsed is added to the 64-bit unix time received via command
 *
 * At reset, the current unix time is reset to 0 (Jan 1, 1970 12am) and
 * continues to count up from there until a valid unix time is received.
 *
 * Examples:
 *  - If no time elapsed since the last_unix_time_sync_ms, then
 *    unix_time_us_hi/lo = last_unix_time_sync_us_hi/lo
 *
 *  - If the last_unix_time_sync_us_hi/lo is never set (= 0), then
 *    unix_time_us_hi/lo = the usec that have elapsed since 0
 *
 *  - If
 *      unix_time_since_us_hi = 0x0
 *      unix_time_since_us_lo = 0xFFFFFFFF
 *      last_unix_time_sync_us_hi = 0x0
 *      last_unix_time_sync_us_lo = 0xFFFFFFFF
 *    then
 *      unix_time_us_hi = 0x1
 *      unix_time_us_lo = 0xFFFFFFFE
 *
 *  - If
 *      unix_time_since_us_hi = 0x1
 *      unix_time_since_us_lo = 0x0
 *      last_unix_time_sync_us_hi = 0x0
 *      last_unix_time_sync_us_lo = 0xFFFFFFFF
 *    then
 *      unix_time_us_hi = 0x1
 *      unix_time_us_lo = 0xFFFFFFFF
 *
 *  - If
 *      unix_time_since_us_hi = 0x1
 *      unix_time_since_us_lo = 0x1
 *      last_unix_time_sync_us_hi = 0x0
 *      last_unix_time_sync_us_lo = 0xFFFFFFFF
 *    then
 *      unix_time_us_hi = 0x2
 *      unix_time_us_lo = 0x0
 */
void thr_data_unix_time_update(uint32_t curr_tick_upper, uint32_t curr_tick_lower)
{
    float unix_time_since_us = 0.0f;
    uint32_t unix_time_since_us_hi = 0UL;
    uint32_t unix_time_since_us_lo = 0UL;

    /* Read the number of usecs that have elapsed since the sync timestamp */
    unix_time_since_us = 1000.0f * timer64_get_duration_ms(RftData.time_sync_lpit_hi,
                                                           RftData.time_sync_lpit_lo,
                                                           curr_tick_upper,
                                                           curr_tick_lower);

    /* Turn the elapsed usec into high and low usec counters */
    unix_time_since_us_hi = (uint32_t)(unix_time_since_us / 4294967296.0f);
    unix_time_since_us_lo = (uint32_t)(unix_time_since_us - ((float)unix_time_since_us_hi * 4294967296.0f));

    /* Get the current unix time in usec by adding the elapsed time to the sync unix time */
    RftData.unix_time_us_hi = unix_time_since_us_hi + RftData.last_unix_time_sync_us_hi;
    RftData.unix_time_us_lo = unix_time_since_us_lo + RftData.last_unix_time_sync_us_lo;

    /* If the msec low time rolled over, increment the upper. */
    if (RftData.unix_time_us_lo < RftData.last_unix_time_sync_us_lo)
    {
        RftData.unix_time_us_hi++;
    }

    return;
}

uint64_t get_unix_timestamp_micro(void)
{
   return  ((uint64_t)RftData.unix_time_us_hi << 32U) + (uint64_t)RftData.unix_time_us_lo;
}
