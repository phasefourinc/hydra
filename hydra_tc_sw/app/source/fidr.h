/* Copyright (c) 2017-2020 by Phase Four, Inc. All Rights Reserved. Confidential and Proprietary.
 *
 * fidr.h
 */

#ifndef SOURCE_FIDR_H_
#define SOURCE_FIDR_H_

#include "p4_err.h"
#include "rft_data.h"
#include "mcu_types.h"

#define ARG1 1
#define ARG2 2

/* Each count represents 10ms, the device sampling rate */
#define FRAME_PERIOD_MS    10UL
#define MSEC(x)            ((uint32_t)x / FRAME_PERIOD_MS)
#define SEC(x)             (((uint32_t)x * 1000UL) / FRAME_PERIOD_MS)

#define ZERO_COUNT 0

enum EQUALITY_VALUES{
    EQUAL,
    NOT_EQUAL,
    GREATER_THAN,
    LESS_THAN,
    GREATER_THAN_OR_EQUAL_TO,
    LESS_THAN_OR_EQUAL_TO
};

typedef bool FDIR_PreFuncPtr (void);


/* NOTE: All arg types must have this base structure at the beginning
 *       since it is the bare minimum needed for FDIR ops */
typedef struct {
    uint32_t persistence_amount;
    uint32_t curr_persist_cnt;      /* Resets to zero each time condition does not exist */
} fdir_arg_base_t;

/*Custom argument types for each fidr function*/
typedef struct {
    fdir_arg_base_t super;
    float  *tlm_data;
    float comparison_value;
} two_equality_arg_type;

typedef struct {
    fdir_arg_base_t super;
    uint32_t *tlm_data;
    uint32_t comparison_value;
} two_eq_u32_arg_type;


typedef struct {
    fdir_arg_base_t super;
    float * tlm_data1;
    float * tlm_data2;
    uint8_t aux_data_select;
} two_tlm_eqaulity_arg_type;


typedef struct FDIR_Record_tag FDIR_Record;


typedef bool fidr_func_type(FDIR_Record *);


struct FDIR_Record_tag
{   const err_type **error_fidr;
    FDIR_PreFuncPtr *prefunc;
    fidr_func_type *fidr_func;
    void * args;
    bool enabled;
};


typedef struct
{
    uint32_t enable;
    uint32_t persistence_ms;
    float    limit;
} FDIR_PT_Rec_t;


bool equality_function(FDIR_Record *record);
bool greater_then_func(FDIR_Record *record);
bool greater_then_eq_func(FDIR_Record *record);
bool less_then_func(FDIR_Record *record);
bool less_then_eq_func(FDIR_Record *record);
bool equal_comp_func(FDIR_Record *record);
bool not_equal_comp_func(FDIR_Record *record);
bool tlmvtlm_greater_then_func(FDIR_Record *record);
bool equal_comp_func_u32(FDIR_Record *record);
bool greater_then_func_u32(FDIR_Record *record);
bool prop_comm_check_u32(FDIR_Record *record);
bool check_persistence_and_log_error(FDIR_Record *record, float aux_data);
bool no_pre_func(void);
bool is_state_in_idle(void);
bool is_state_not_in_idle(void);
bool is_state_sw_update(void);
bool is_pp_or_wf_enabled(void);
bool is_prop_connected_and_pp_or_wf_enabled(void);
bool is_inverter_connected(void);
void run_fidr(void);
bool fidr_has_active_fault(void);
void fidr_log_all_persistant_faults(void);
void fidr_init(void);

extern FDIR_Record FDIR_Table [];

#endif /* SOURCE_FIDR_H_ */
