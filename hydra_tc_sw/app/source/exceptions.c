/* Copyright (c) 2017-2020 by Phase Four, Inc. All Rights Reserved. Confidential and Proprietary.
 *
 * exceptions.c
 */

#include "exceptions.h"
#include "mcu_types.h"
#include "MaxwellAO.h"
#include "ErrAO.h"
#include "p4_err.h"
#include "gpio.h"
#include "rft_data.h"
#include "xdata.h"
#include "scrubber.h"
#include "default_pt.h"

/* NOTE: Care must be taken when altering functions in this file, since
 * these functions may be called prior to initialization of the OS, they
 * cannot push errors to Error AO, and they may be called in interrupts,
 * which can cause race conditions since the OS is expecting single-threaded
 * code. */

/* The excepting instruction address is stored as part of the exception context
 * that is pushed onto the stack.  The ReturnAddress() contains the address of
 * the instruction that caused the exception.  Depending on the exception, this
 * may be the instruction address of the excepting instruction or the address
 * immediately after.
 *
 * Ref ARMv7-M Arch RM B1.5.6 Exception entry behavior
 *
 * Exception context is pushed onto the stack in the following order:
 *  - R0  (lower address/last pushed onto the stack)
 *  - R1
 *  - R2
 *  - R3
 *  - R12
 *  - Link Register (R14)
 *  - ReturnAddress()
 *  - xPSR (higher address/first pushed onto the stack)
 */

enum StackContext
{
    REG_R0 = 0,
    REG_R1,
    REG_R2,
    REG_R3,
    REG_R12,
    REG_LR,
    REG_PC,
    REG_XPSR
};


/* Semaphores for signaling events in the main processing loop.
 * These are made uint8_t to make their increments and decrements
 * atomic since it can be set in a single instruction.  This is
 * not necessarily the case for uint32_t, which can take 2 instructions. */
static uint8_t excp_fpu_sem           = 0U;
static uint8_t excp_noncrit_sem       = 0U;

static uint32_t excp_fpu_error_ins_addr = 0UL;

static Cmd_Err_Code_t excp_noncrit_error;

void Exceptions_Init(void)
{
    excp_noncrit_error = ERROR_NONE;

    /* Enable the following faults:
     *  - Usage - Instruction execution faults (undefined ins, invalid state, invalid INT return, div by 0)
     *  - Bus - Errors resulting from instruction fetches, data accesses, memory access faults
     *  - Memory Manager - Memory Access violations detected by MPU
     *  - HardFault then becomes only a fault that occurs within other exception handlers
     */
    S32_SCB->SHCSR |= (S32_SCB_SHCSR_USGFAULTENA_MASK |
                       S32_SCB_SHCSR_BUSFAULTENA_MASK |
                       S32_SCB_SHCSR_MEMFAULTENA_MASK);

    /* Clear the Auto Saving (ASPEN) and Lazy Stacking (LSPEN) since no FPU is utilized in interrupts.
     * This also covers issues with errata e6940.
     * Note: Enabling FPU context on the stack can affect accessing of registers stored on the interrupt
     * context stack and affects clearing of FPU interrupts.  If FPU interrupt context saving is enabled,
     * the user would need to clear FPSCR on the stack since it will restore the actual FPSCR once
     * exiting the interrupt. */
    S32_SCB->FPCCR &= ~(S32_SCB_FPCCR_ASPEN_MASK | S32_SCB_FPCCR_LSPEN_MASK);

    /* Configure the FPU Interrupts look for the following. Ignore the
     * Input Denormal and Inexact interrupts since they can fire when
     * doing conversions between floats and ints (e.g. 0.1f -> 0U). */
    MCM->ISCR = MCM_ISCR_FUFCE_MASK |  /* FPU Underflow Interrupt Enable */
                MCM_ISCR_FOFCE_MASK |  /* FPU Overflow Interrupt Enable */
                MCM_ISCR_FDZCE_MASK |  /* FPU Divide-By-Zero Interrupt Enable */
                MCM_ISCR_FIOCE_MASK;   /* FPU Invalid Operation Interrupt Enable (e.g. -2.0f -> u32) */

    /* Clear any prior FPU interrupt flags */
    __set_FPSCR(__get_FPSCR() & (~0x9FUL));

    /* Enable RAM ECC Single and Double-bit error reporting interrupts */
    MCM->LMPECR = MCM_LMPECR_ER1BR_MASK | MCM_LMPECR_ERNCR_MASK;

    /* Clear any prior ECC interrupt flags (w1c) for SRAM_L and SRAM_U
     * prior to enabling the interrupts */
    MCM->LMPEIR = (MCM_LMPEIR_E1B(3UL) | MCM_LMPEIR_ENC(3UL));

    /* Enable Flash ECC Double-bit error reporting interrupts
     * Note: Setting FTFC_FERCNFG_FDFD_MASK emulates nbit Flash ECC error */
    FTFC->FERCNFG = FTFC_FERCNFG_DFDIE_MASK;

    /* Clear any prior FTFC interrupt flags (w1c) prior to enabling the interrupt */
    FTFC->FERSTAT = FTFC_FERSTAT_DFDIF_MASK;

    /* Activate ECC and FPU error interrupts */
    Exceptions_ExtInt_Enable();

    return;
}


/* This function expects to be called in the main thread */
void Exceptions_PushInterrupts(void)
{
    /* Logging errors for exceptions occurs through a semaphore-polling method
     * since interrupt preemption can be tricky and may occur at times when we
     * least expect them to. It is better to log the error sequentially in the
     * main thread. */
    if (excp_fpu_sem > 0U)
    {
        --excp_fpu_sem;
        report_error_uint(&AO_Err, Errors.McuFpuException, DO_NOT_SEND_TO_SPACECRAFT, excp_fpu_error_ins_addr);
    }
    if (excp_noncrit_sem > 0U)
    {
        --excp_noncrit_sem;
        report_error_uint(&AO_Err, excp_noncrit_error.reported_error, DO_NOT_SEND_TO_SPACECRAFT, excp_noncrit_error.aux_data);
    }

    return;
}


void Exceptions_ExtInt_Enable(void)
{
    /* Enable the MCM (FPU and RAM ECC) IRQs in NVIC */
    INT_SYS_EnableIRQ(MCM_IRQn);

    /* Enable FTFC Double-bit Fault IRQ in NVIC */
    INT_SYS_EnableIRQ(FTFC_Fault_IRQn);

    /* ARM recommended memory barrier, sync data and ins */
    __DSB();
    __ISB();

    return;
}


void Exceptions_ExtInt_Disable(void)
{
    /* Disable the MCM (FPU and RAM ECC) IRQs in NVIC */
    INT_SYS_DisableIRQ(MCM_IRQn);

    /* Disable FTFC Double-bit Fault IRQ in NVIC */
    INT_SYS_DisableIRQ(FTFC_Fault_IRQn);

    /* ARM recommended memory barrier, sync data and ins */
    __DSB();
    __ISB();

    return;
}


void Exceptions_HandleUnrecoverable(const err_type *err, uint32_t aux_data)
{
    status_t status;

    /* If ECC error is on the stack, then it could cause recurrent
     * issue with pushing/popping the stack. Therefore disable
     * all interrupts. The MCU does not leave this function until
     * a reset occurs. */
    __disable_irq();
    __DSB();
    __ISB();

    /* If this occurs prior initialization, then the system is already safed,
     * so no need to do it. Also doing so prior to initialization will fail
     * when commanding uninitialized drivers. */
    if (RftData.startup_complete)
    {
        /* Hard safe the system */
        MaxAO_hard_safe();

        /* Strobe the WDT to allow for sufficient time to finish this routine,
         * however delay at least 27ms to ensure we are within the WDT pet window
         * or else it might reset us prematurely. Since we strobe at 4Hz, delaying
         * a little extra here is safe. */
        timer32_delay_ms(30UL);
        GPIO_PetWDT();

        /* log error manually here to get into TLM and attempt to move the single
         * error into NVM so it can be recovered across a reset */
        status = err_log_and_nvm_xfer((err_type *)err, aux_data);
        (void)status;    /* Avoid unused variable compiler warning */
    }

    /* Wait for WDT to timeout the MCU */
    while (1);

    /* This function does not return */
    return;
}


/* Log the Error only, attempt to continue execution */
void Exceptions_HandleNonCritical(const err_type *err, uint32_t aux_data)
{
    /* Noncritical errors do not log directly since this function can be
     * called within an interrupt and there is a race condition when pushing
     * objects to an AOs queue. Serialize it by saving the error and then
     * recording it in the main loop.
     * Note: Unclear if this will work well since delaying its logging will
     * not correctly reflect when the error occurred since it will be placed
     * at the back of the queue to Error AO. */
    ++excp_noncrit_sem;
    excp_noncrit_error = create_error_uint(err, aux_data);
    return;
}


/* This overrides the weak implementation in startup_S32K.
 * Processor ends up here if an unexpected interrupt occurs or a specific
 * handler is not present in the application code. */
void DefaultISR(void)
{
    /* Pass in the currently excepting interrupt number as aux data */
    Exceptions_HandleNonCritical(Errors.McuUnexpectedInterrupt, __get_IPSR());
    return;
}


/* Overriding default core exception handlers from startup_mke18f16.c
 *
 * Generally internal MCU faults issue unrecoverable errors, which
 * will safe the system, log the last error, disable the WDT and
 * await a reset.
 *
 * The overriding functions are generally composed of only assembly
 * instructions.  The reason for this is that many of the faults it is
 * desired to get the last instruction that was executed prior to the
 * interrupt via the PC that is stored automatically by the ARM onto
 * an interrupt stack context (see ARMv7-M Arch RM).  The issue is that
 * this PC can be hard to pinpoint when 1) GCC can arbitrarily store
 * variables it needs depending on the function and 2) when C requires
 * the SP to be on an 8-byte boundary for interrupts.  The workaround
 * and trick thus becomes to implement these overriding functions in assembly
 * and to pass in the SP into r0 as a function parameter to a branched
 * C function.  This way, the interrupt context is always accessible.
 *
 * NOTE: The interrupt handlers start with those with highest priority
 *       (NMI_Handler) and end with the lowest priority interrupts
 *       (MCU External Interrupts)
 */

ASM_ONLY void NMI_Handler(void)
{
    __asm volatile ("mrs r0, msp");
    __asm volatile ("b nmi_hdl");
}


/* Hard Fault is issued for unrecoverable system failure situations */
ASM_ONLY void HardFault_Handler(void)
{
    __asm volatile ("mrs r0, msp");
    __asm volatile ("b hard_fault_hdl");
}


ASM_ONLY void MemManage_Handler(void)
{
    __asm volatile ("mrs r0, msp");
    __asm volatile ("b mem_manage_hdl");
}


ASM_ONLY void BusFault_Handler(void)
{
    __asm volatile ("mrs r0, msp");
    __asm volatile ("b bus_fault_hdl");
}


ASM_ONLY void UsageFault_Handler(void)
{
    __asm volatile ("mrs r0, msp");
    __asm volatile ("b usage_fault_hdl");
}


ASM_ONLY void DebugMon_Handler(void)
{
    __asm volatile ("mrs r0, msp");
    __asm volatile ("b debug_mon_hdl");
}


ASM_ONLY void SVC_Handler(void)
{
    __asm volatile ("mrs r0, msp");
    __asm volatile ("b svc_hdl");
}


ASM_ONLY void PendSV_Handler(void)
{
    __asm volatile ("mrs r0, msp");
    __asm volatile ("b pend_sv_hdl");
}


/* NOTE: SysTick_Handler is defined in osif_baremetal.c
 * and is used by the SDK */


/* Overridden MCU External Interrupts */


ASM_ONLY void FTFC_Fault_IRQHandler(void)
{
    __asm volatile ("mrs r0, msp");
    __asm volatile ("b ftfc_fault_hdl");
}


/* Miscellaneous Control Module (MCM) interrupt reports Floating Point Unit (FPU)
 * (e.g. div-by-zero) and SRAM Error Correcting Code (ECC) interrupts */
ASM_ONLY void MCM_IRQHandler(void)
{
    __asm volatile ("mrs r0, msp");
    __asm volatile ("b mcm_hdl");
}




/* External Non-Maskable Interrupt is unused,
 * therefore consider it an unexpected interrupt */
void nmi_hdl(uint32_t stack[])
{
    Exceptions_HandleNonCritical(Errors.McuNmiInterrupt, stack[REG_PC]);
    return;
}


/* Hard Fault triggered by priority escalation from another fault
 * (which should already have recorded error data)
 * therefore there's no need to replace the error data from the stack. */
void hard_fault_hdl(UNUSED uint32_t stack[])
{
    Exceptions_HandleUnrecoverable(Errors.McuHardFaultInterrupt, stack[REG_PC]);
    return;
}


void mem_manage_hdl(uint32_t stack[])
{
    Exceptions_HandleUnrecoverable(Errors.McuMemManageInterrupt, stack[REG_PC]);
    return;
}


void bus_fault_hdl(uint32_t stack[])
{
    Exceptions_HandleUnrecoverable(Errors.McuBusFaultInterrupt, stack[REG_PC]);
    return;
}


void usage_fault_hdl(uint32_t stack[])
{
    Exceptions_HandleUnrecoverable(Errors.McuUsageFaultInterrupt, stack[REG_PC]);
    return;
}


void debug_mon_hdl(uint32_t stack[])
{
    Exceptions_HandleUnrecoverable(Errors.McuDebugMonInterrupt, stack[REG_PC]);
    return;
}


/* Supervisor Call Interrupt Handler */
void svc_hdl(uint32_t stack[])
{
    Exceptions_HandleUnrecoverable(Errors.McuSvcInterrupt, stack[REG_PC]);
    return;
}


void pend_sv_hdl(uint32_t stack[])
{
    Exceptions_HandleUnrecoverable(Errors.McuPendSvInterrupt, stack[REG_PC]);
    return;
}


/* Manual ECC Flash Test:
 *  - Halt code execution
 *  - Set FTFC->FERCNFG[DFDIE|FDFD]
 *  - Continue execution
 */
void ftfc_fault_hdl(uint32_t stack[])
{

    /* Clear the interrupt flag */
    FTFC->FERSTAT = (uint8_t)FTFC_FERSTAT_DFDIF_MASK;

    /* If there is permanent corruption in a code instruction,
     * then this is an unrecoverable situation */
    Exceptions_HandleUnrecoverable(Errors.McuFlashNbitInterrupt, stack[REG_PC]);

    return;
}


/* Manual FPU Error Test:
 *  - Add code to perform div-by-zero (or similar) to create
 *    error.
 *
 * Manual ECC SRAM Test:
 *   - Halt code execution
 *   - Set MCM->LMDRn[CF0] = 0x2 (Disable ECC Write Gen)
 *   - Modify SRAM through Memory Browser by changing 1-bit
 *     for 1-bit ECC test or more for NC-bit ECC test
 *   - Set MCM->LMDRn[CF0] = 0x3 (Re-enable ECC Write Gen)
 * NOTE: This manual test used to work, but it seems to only generate
 * n-bit errors. Use the EIM to generate and test 1-bit errors.
 */
void mcm_hdl(uint32_t stack[])
{
    /* Ignore the Input Denormal Int and Inexact interrupt since converting
     * between float to uint32_t could cause these interrupts to fire when
     * rounding to zero. */
    if (MCM->ISCR & (MCM_ISCR_FUFC_MASK |  /* FPU Underflow Interrupt Status */
                     MCM_ISCR_FOFC_MASK |  /* FPU Overflow Interrupt Status */
                     MCM_ISCR_FDZC_MASK |  /* FPU Divide-By-Zero Interrupt Status */
                     MCM_ISCR_FIOC_MASK))  /* FPU Invalid Operation Interrupt Status */
    {
        ++excp_fpu_sem;

        /* Aux data is the location of the Program Counter at the time of the error
         * Note: It's possible that if multiple interrupts errors occur, then the address
         * may get overwritten with the most recent one before it actually gets logged.
         * This is okay since it's informational data and likely would mean that the
         * address that errors the most is most likely to get captured by this variable. */
        excp_fpu_error_ins_addr = stack[REG_PC];

        /* Clear Interrupt Flags in the Floating-point Status Control Register
         * for bits 7 (IDC) and 4:0 (IXC, UFC, OFC, DZC, IOC) */
        __set_FPSCR(__get_FPSCR() & (~0x9FUL));

        /* Attempt to recover rather than making this unrecoverable.
         * Note: Divide by zero results in INF, whicn likely will propogate. */
    }

    /* Check if Non-correctable ECC Fault in SRAM_U and SRAM_L banks.
     * Note: The 1-bit ECC LMPEIR error is also generated when non-correctable is triggered,
     * therefore we check ENC bit first. */
    if (MCM->LMPEIR & MCM_LMPEIR_ENC(3UL))
    {
        /* This should handle ECC errors even on the stack.  The main danger is
         * attempting to return from a function and returning to some unknown
         * location that could harm the thruster.  Since this interrupt
         * pushes more data on the stack (moving past the stack ECC error)
         * and once entering the Handle Unrecoverable function below which
         * never returns, the FSW will be in a state that can execute function
         * calls safely to power down the thruster and log the error before
         * entering an infinite loop to wait for a WDT reset. */
        Exceptions_HandleUnrecoverable(Errors.McuSramEccNbitInterrupt, MCM->LMFAR);
    }
    /* Check if 1-bit correctable ECC Fault in SRAM_U and SRAM_L banks */
    else if (MCM->LMPEIR & MCM_LMPEIR_E1B(3UL))
    {
        /* There may be an instance where the ECC bit is on the stack and that
         * the 1bit error continuously reenters this ISR upon returning from
         * it. In this case, there is not much else to do other than wait for
         * the WDT to reset the MCU and hopefully shut things down upon next
         * startup. If it's super duper unlucky, since resetting does not clear
         * some 1bit errors, there's a possibility for a rolling reset, in which
         * the SC should power reset the system if they are monitoring. */

        /* A 1bit ECC was detected, but only count it if it's during scrubbing */
        if (Scrub_IsActive())
        {
            Scrub_IncEccCnt();
        }

        /* Write SRAM_U and SRAM_L E1B bits back to clear error */
        MCM->LMPEIR = MCM_LMPEIR_E1B(3UL);
    }

    return;
}
