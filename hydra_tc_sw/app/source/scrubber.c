/* Copyright (c) 2017-2020 by Phase Four, Inc. All Rights Reserved. Confidential and Proprietary.
 *
 * scrubber.c
 */

#include "scrubber.h"
#include "mcu_types.h"
#include "timer64.h"
#include "rft_data.h"
#include "xdata.h"

/* Start this one as disabled as it's possible the ECC interrupt
 * could fire prior to scrubber initialization */
bool scrub_active = false;
uint32_t scrub_ecc_1bit_cnt = 0UL;

static uint32_t *curr_addr;
static uint32_t scrub_tick_start;

void Scrub_Init(void)
{
    curr_addr = (uint32_t *)SCRUB_MEM_START;
    scrub_ecc_1bit_cnt = 0UL;
    scrub_tick_start = timer32_get_tick();
    return;
}

void Scrub_MemoryProcess(void)
{
    /* Use as little stack as possible for a more accurate ECC count */
    register uint32_t i;
    register volatile uint32_t dummy_read;

    scrub_active = true;

    for (i = 0UL; i < SCRUB_U32_PER_CYCLE; ++i, ++curr_addr)
    {
        dummy_read = *curr_addr;
    }

    scrub_active = false;

    if ((uint32_t)curr_addr >= SCRUB_MEM_STOP)
    {
        (void)dummy_read;  /* Avoids compiler warning */
        XTIME_UPDATE(&Xdata.scrub_xtime, scrub_tick_start);
        XDATA_UPDATE(&Xdata.scrub_ecc_1bit_cnt, scrub_ecc_1bit_cnt);
        RftData.rft_ecc_sram_1bit_cnt = Xdata.scrub_ecc_1bit_cnt.max;
        Scrub_Init();
    }

    return;
}

