/* Copyright (c) 2017-2020 by Phase Four, Inc. All Rights Reserved. Confidential and Proprietary.
 *
 * exceptions.h
 */

#ifndef SOURCE_EXCEPTIONS_H_
#define SOURCE_EXCEPTIONS_H_

#include "mcu_types.h"
#include "p4_err.h"

/* Global invariants:
 *  - FPU is not utilized in interrupts, FPU lazy stacking and auto saving are disabled
 *  - Interrupts are not allowed to push events to Active Objects in the OS since the
 *    current mode is for a single-threaded application.
 *  - The PCU should monitor responses and should hard power cycle in the event that the
 *    firmware is unresponsive, ideally within a few seconds.
 *
 * Future enhancements:
 *  - Enabling and handling the low voltage detect interrupt LVD_LVW_IRQn
 *  - FPU interrupts smartly set the resulting value based on the error type
 */

void Exceptions_Init(void);
void Exceptions_PushInterrupts(void);
void Exceptions_ExtInt_Enable(void);
void Exceptions_ExtInt_Disable(void);
void Exceptions_HandleUnrecoverable(const err_type *err, uint32_t aux_data);
void Exceptions_HandleNonCritical(const err_type *err, uint32_t aux_data);

#endif /* SOURCE_EXCEPTIONS_H_ */
