/* Copyright (c) 2017-2020 by Phase Four, Inc. All Rights Reserved. Confidential and Proprietary.
 *
 * uart_ring.c
 */


#include "uart_ring.h"
#include "mcu_types.h"
#include "lpuart_driver.h"
#include "dmaController0.h"
#include "lpuart_hw_access.h"
#include "string.h"


/* In the event of UART overrun
 *  - Notify AO to abort and restart Rx
 * In the event of a Ring overrun
 *  - Notify AO to abort and restart Rx
 */


/* Private function declarations */

static void UartRing_ReadCallbackDma(void *parameter, edma_chn_status_t event);
static void UartRing_CopyToReadBuf(UartRing_Rec_t *ring_rec);
static uint16_t UartRing_IncIdx(uint16_t ring_start_idx, uint16_t ring_size, uint16_t n);


/* Public function definitions */

/* Expects the provided UART instance to have been initialized */
void UartRing_Init(UartRing_Rec_t *ring_rec,
                   LPUART_Type *lpuart,
                   uint8_t dma_rx_ch,
                   uint8_t *ring_buf,
                   uint16_t ring_size)
{
    DEV_ASSERT(ring_rec);
    DEV_ASSERT(lpuart);
    DEV_ASSERT(ring_buf);

    /* Ring buffer should be 32-byte aligned for DMA */
    DEV_ASSERT(((uint32_t)ring_buf & 0x3UL) == 0x0UL);

    /* Apply max ring size to simplify interactions with int16_t items */
    DEV_ASSERT(ring_size <= 0x7FFFU);

    /* The DMA must be initialized with the appropriate TCD and size
     * must match the Major Iterations in the TCD */
    DEV_ASSERT(ring_size == EDMA_DRV_GetRemainingMajorIterationsCount(dma_rx_ch));

    ring_rec->lpuart = lpuart;
    ring_rec->dma_rx_ch     = dma_rx_ch;
    ring_rec->ring_buf      = ring_buf;
    ring_rec->ring_size     = ring_size;

    UartRing_ResetBuffer(ring_rec);

    (void)EDMA_DRV_InstallCallback(dma_rx_ch, &UartRing_ReadCallbackDma, (void *)ring_rec);

    return;
}


void UartRing_StartBuffer(UartRing_Rec_t *ring_rec)
{
    /* Start the DMA channel, always returns STATUS_SUCCESS */
    (void)EDMA_DRV_StartChannel(ring_rec->dma_rx_ch);

    /* Enable the receiver */
    LPUART_SetReceiverCmd(ring_rec->lpuart, true);

    /* Enable error interrupts */
    LPUART_SetErrorInterrupts(ring_rec->lpuart, true);

    /* Enable rx DMA requests for the current instance */
    LPUART_SetRxDmaCmd(ring_rec->lpuart, true);

    ring_rec->is_active = true;

    return;
}


void UartRing_StopBuffer(UartRing_Rec_t *ring_rec)
{
   /* Stop UART Rx and DMA Rx servicing */
    LPUART_SetRxDmaCmd(ring_rec->lpuart, false);
    EDMA_DRV_StopChannel(ring_rec->dma_rx_ch);

    ring_rec->is_active = false;

    /* Halt any current reads from the ring buffer */
    ring_rec->read_size = 0U;
    ring_rec->read_ongoing = false;

    /* Note: Do not reset read_buf to NULL since it may
     * be accessed after the ring buffer has been halted,
     * which avoids accidental NULL pointer dereferences */

    return;
}


void UartRing_ResetBuffer(UartRing_Rec_t *ring_rec)
{
    ring_rec->head_i = 0U;
    ring_rec->tail_i = 0U;
    ring_rec->read_buf = NULL;
    ring_rec->read_size = 0U;
    ring_rec->read_ongoing = false;
    ring_rec->is_active = false;
    ring_rec->dma_rx_err_sem = 0U;

    /* Zero out the ring buffer */
    memset(ring_rec->ring_buf, 0, ring_rec->ring_size);

    return;
}



bool UartRing_Process(UartRing_Rec_t *ring_rec)
{
    DEV_ASSERT(ring_rec);

    bool read_completed = false;

    if (ring_rec->is_active)
    {
        /* Note: UART statuses should be captured in interrupt callback
         * functions to handle any UART-related errors that occur. */

        /* The ring buffer exactly fits into a complete eDMA transfer. When
         * an eDMA transfer restarts automatically to jumps back to the
         * start of the ring buffer. Therefore, all the tail needs to do is
         * follow the location of the major iter count remaining. */
        ring_rec->tail_i = ring_rec->ring_size - EDMA_DRV_GetRemainingMajorIterationsCount(ring_rec->dma_rx_ch);

        /* If the ring buffer has been signaled to read data, then copy
         * data from ring buffer to the read buffer if it's all there */
        if (ring_rec->read_ongoing)
        {
            /* If all requested data has been captured in the Ring Buffer
             * (i.e. Tail has incremented far enough) */
            if (UartRing_BytesAvailable(ring_rec) >= ring_rec->read_size)
            {
                /* Move data from the ring buffer into the read buffer */
                UartRing_CopyToReadBuf(ring_rec);

                ring_rec->read_ongoing = false;
                read_completed = true;
            }
        }
    }

    return read_completed;
}


uint16_t UartRing_BytesAvailable(const UartRing_Rec_t *ring_rec)
{
    DEV_ASSERT(ring_rec);

    uint16_t bytes_available;

    if (ring_rec->head_i <= ring_rec->tail_i)
    {
        /* Head is behind the Tail or equal (when buffer is empty) */
        bytes_available = ring_rec->tail_i - ring_rec->head_i;
    }
    else
    {
        /* Tail has looped around and is behind Head */
        bytes_available = ring_rec->ring_size - (ring_rec->head_i - ring_rec->tail_i);
    }

    /* The returned bytes should never exceed or equal to the ring buf size */
    DEV_ASSERT(bytes_available < ring_rec->ring_size);

    return bytes_available;
}


/* Signal to ring buffer processing to read the requested number
 * of bytes from the ring buffer into the provided ring buffer.
 *
 * If bytes requested is 0, then the ring buffer processing will
 * still occur and signal the end of the read.
 *
 * Note: Users must ensure that the bytes requested is always less
 * than the size of the ring buffer. */
void UartRing_ReadBuffer(UartRing_Rec_t *ring_rec, uint8_t *read_buf, uint16_t bytes_requested)
{
    DEV_ASSERT(ring_rec);
    DEV_ASSERT(read_buf);

    /* Max size is 1 less than the ring buffer size */
    DEV_ASSERT(bytes_requested < ring_rec->ring_size);

    ring_rec->read_buf = read_buf;
    ring_rec->read_size = bytes_requested;
    ring_rec->read_ongoing = true;
    return;
}


/* Private function definitions */

/* NOTE: This function must remain reentrant as this is a generic callback used
 * for all UART ports, and therefore it may exist in multiple interrupts */
static void UartRing_ReadCallbackDma(void *parameter, edma_chn_status_t event)
{
    DEV_ASSERT(parameter);

    UartRing_Rec_t *ring_rec = (UartRing_Rec_t *)parameter;

    if (event != EDMA_CHN_NORMAL)
    {
        ++ring_rec->dma_rx_err_sem;
    }

    return;
}


static void UartRing_CopyToReadBuf(UartRing_Rec_t *ring_rec)
{
    DEV_ASSERT(ring_rec);
    DEV_ASSERT(ring_rec->ring_buf);
    DEV_ASSERT(ring_rec->read_buf);

    uint16_t bytes_to_end = 0U;
    uint16_t bytes_to_copy = UartRing_BytesAvailable(ring_rec);

    /* If there are more bytes in the ring buffer than are requested,
     * then only copy the bytes that are requested */
    if (bytes_to_copy > ring_rec->read_size)
    {
        bytes_to_copy = ring_rec->read_size;
    }

    /* Get the total number of bytes from the head until the end */
    DEV_ASSERT(ring_rec->ring_size > ring_rec->head_i);
    bytes_to_end = ring_rec->ring_size - ring_rec->head_i;

    /* If Head is behind the Tail, or, if it is not and there is room before the
     * end of the ring buffer, then move data over in single chunk */
    if ((ring_rec->head_i <= ring_rec->tail_i) || (bytes_to_copy <= bytes_to_end))
    {
        memcpy(ring_rec->read_buf, &ring_rec->ring_buf[ring_rec->head_i], bytes_to_copy);
    }
    /* Otherwise copy the chunk at the end of the physical ring
     * buffer and then the final chunk at the start leading up to the Tail  */
    else
    {
        memcpy(&ring_rec->read_buf[0],            &ring_rec->ring_buf[ring_rec->head_i], bytes_to_end);
        memcpy(&ring_rec->read_buf[bytes_to_end], &ring_rec->ring_buf[0],                bytes_to_copy - bytes_to_end);
    }

    /* Update the Head and total number of bytes left to read */
    ring_rec->head_i = UartRing_IncIdx(ring_rec->head_i, ring_rec->ring_size, bytes_to_copy);

    return;
}


static uint16_t UartRing_IncIdx(uint16_t ring_start_idx, uint16_t ring_size, uint16_t n)
{
    /* Note: This implementation does not correctly handle
     * 16-bit rollover conditions. Since very unlikely, precondition
     * should suffice. */
    DEV_ASSERT((ring_start_idx + n) >= ring_start_idx);
    DEV_ASSERT(ring_start_idx < ring_size);

    /* If the Head does not fall off the end of the ring buffer,
     * then increment the Head directly */
    if ((ring_start_idx + n) < ring_size)
    {
        return (ring_start_idx + n);
    }
    else
    {
        return ((ring_start_idx + n) - ring_size);
    }
}
