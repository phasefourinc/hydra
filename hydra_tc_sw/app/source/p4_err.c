/* Copyright (c) 2017-2020 by Phase Four, Inc. All Rights Reserved. Confidential and Proprietary.
 *
 * p4_err.c
 */

#ifndef SOURCE_P4_ERR_C_
#define SOURCE_P4_ERR_C_

#include "mcu_types.h"
#include "p4_err.h"

/* ERROR DEFINITION
 * Each error below is a object that is used throughout the project.
 * The layout is structured below:
 *
 *                                                                     MCU Time         Error    Error       Severity
 *                                                                                      Code     Count        Level
 */

 /*Starting error declaration */

static err_type StatusSuccess                                      = {NO_CURRENT_TIME, 0x0000, ZERO_ERRORS, WARNING, UNSIGNED_INTEGER};    /* This value indicates that the operation was a success */
static err_type StatusFailure                                      = {NO_CURRENT_TIME, 0x0001, ZERO_ERRORS, WARNING, UNSIGNED_INTEGER};    /* This error indicates there was a general failure. This is used if there is no specfic error. */
static err_type InvalidBootloaderError                             = {NO_CURRENT_TIME, 0x0002, ZERO_ERRORS, WARNING, UNSIGNED_INTEGER};    /* This error is used if the error that came from the bootloader is not a valid bootloader error. If this is reported the source code for the bootloader should be checked. */

static err_type GeneralBootloaderError                             = {NO_CURRENT_TIME, 0x0400, ZERO_ERRORS, WARNING, UNSIGNED_INTEGER};    /* This is a general bootloader failure. A catch all error if there is not a specfic enough error. */
static err_type BootloaderFlashMaxSize                             = {NO_CURRENT_TIME, 0x0401, ZERO_ERRORS, WARNING, UNSIGNED_INTEGER};    /* The bootloader application size being loaded into is over the maximum allocated amount */
static err_type InvalidBootloaderCrcValue                          = {NO_CURRENT_TIME, 0x0402, ZERO_ERRORS, WARNING, UNSIGNED_INTEGER};    /* The bootloader the that the startup process to load into had an invalid CRC. */
static err_type InvalidBootAppType                                 = {NO_CURRENT_TIME, 0x0403, ZERO_ERRORS, WARNING, UNSIGNED_INTEGER};    /* The application loaded in the bootloader section of flash is not a bootloader type application. THIS IS NOT IMPLEMENTED YET */
static err_type InvalidBootHardType                                = {NO_CURRENT_TIME, 0x0404, ZERO_ERRORS, WARNING, UNSIGNED_INTEGER};    /* The bootloader loaded in the bootloader section of flash is not meant for this hardware. THIS IS NOT IMPLEMENTED YET */
static err_type InvalidBootProdType                                = {NO_CURRENT_TIME, 0x0405, ZERO_ERRORS, WARNING, UNSIGNED_INTEGER};    /* The application loaded in the bootloadersection of flash is not meant for this product. THIS IS NOT IMPLEMENTED YET */
static err_type InvalidApp1FlashSize                               = {NO_CURRENT_TIME, 0x0406, ZERO_ERRORS, WARNING, UNSIGNED_INTEGER};    /* This error indicates that flash application 1 size is over the maximum allocated amount */
static err_type InvalidFlashApp1CrcValue                           = {NO_CURRENT_TIME, 0x0407, ZERO_ERRORS, WARNING, UNSIGNED_INTEGER};    /* This error indicates that flash application 1 had a invalid crc */
static err_type InvalidFlashApp1AppType                            = {NO_CURRENT_TIME, 0x0408, ZERO_ERRORS, WARNING, UNSIGNED_INTEGER};    /* The application loaded in the App 1 section of flash is not a sw image (EXE TYPE)  type application. */
static err_type InvalidFlashApp1HardType                           = {NO_CURRENT_TIME, 0x0409, ZERO_ERRORS, WARNING, UNSIGNED_INTEGER};    /* The application loaded in the App 1 section of flash is not meant for this hardware.  */
static err_type InvalidFlashApp1ProdType                           = {NO_CURRENT_TIME, 0x040A, ZERO_ERRORS, WARNING, UNSIGNED_INTEGER};    /* The application loaded in the App 1 section of flash is not meant for this product.  */
static err_type InvalidApp2FlashSize                               = {NO_CURRENT_TIME, 0x040B, ZERO_ERRORS, WARNING, UNSIGNED_INTEGER};    /* This error indicates that flash application 2 size is over the maximum allocated amount */
static err_type InvalidFlashApp2CrcValue                           = {NO_CURRENT_TIME, 0x040C, ZERO_ERRORS, WARNING, UNSIGNED_INTEGER};    /* This error indicates that flash application 2 had a invalid crc */
static err_type InvalidFlashApp2AppType                            = {NO_CURRENT_TIME, 0x040D, ZERO_ERRORS, WARNING, UNSIGNED_INTEGER};    /* The application loaded in the App 2 section of flash is not a sw image (EXE TYPE)  type application. */
static err_type InvalidFlashApp2HardType                           = {NO_CURRENT_TIME, 0x040E, ZERO_ERRORS, WARNING, UNSIGNED_INTEGER};    /* The application loaded in the App 2 section of flash is not meant for this hardware.  */
static err_type InvalidFlashApp2ProdType                           = {NO_CURRENT_TIME, 0x040F, ZERO_ERRORS, WARNING, UNSIGNED_INTEGER};    /* The application loaded in the App 2 section of flash is not meant for this product.  */
static err_type InvalidAppGoldFlashSize                            = {NO_CURRENT_TIME, 0x0410, ZERO_ERRORS, WARNING, UNSIGNED_INTEGER};    /* The golden flash application is over the maximum allocated amount */
static err_type InvalidFlashGoldCrcValue                           = {NO_CURRENT_TIME, 0x0411, ZERO_ERRORS, WARNING, UNSIGNED_INTEGER};    /* The golden flash application had a invalid crc */
static err_type InvalidFlashGoldAppType                            = {NO_CURRENT_TIME, 0x0412, ZERO_ERRORS, WARNING, UNSIGNED_INTEGER};    /* The application loaded in the Golden Image section of flash is not a sw image (EXE TYPE)  type application. This should never be triggered */
static err_type InvalidFlashGoldHardType                           = {NO_CURRENT_TIME, 0x0413, ZERO_ERRORS, WARNING, UNSIGNED_INTEGER};    /* The application loaded in the Golden Image section of flash is not meant for this hardware. This should never be triggered */
static err_type InvalidFlashGoldProdType                           = {NO_CURRENT_TIME, 0x0414, ZERO_ERRORS, WARNING, UNSIGNED_INTEGER};    /* The application loaded in the Golden Image section of flash is not meant for this product. This should never be triggered */
static err_type InvalidSramAppSize                                 = {NO_CURRENT_TIME, 0x0415, ZERO_ERRORS, WARNING, UNSIGNED_INTEGER};    /* The application is over the maximum allocated amount */
static err_type InvalidSramCrcValue                                = {NO_CURRENT_TIME, 0x0416, ZERO_ERRORS, WARNING, UNSIGNED_INTEGER};    /* The application loaded into sram had a invalid crc */
static err_type InvalidSramAppType                                 = {NO_CURRENT_TIME, 0x0417, ZERO_ERRORS, WARNING, UNSIGNED_INTEGER};    /* The application loaded to sram is not a sw image (EXE TYPE)  type application. */
static err_type InvalidSramHardType                                = {NO_CURRENT_TIME, 0x0418, ZERO_ERRORS, WARNING, UNSIGNED_INTEGER};    /* The application loaded to sram is not meant for this hardware.  */
static err_type InvalidSramProdType                                = {NO_CURRENT_TIME, 0x0419, ZERO_ERRORS, WARNING, UNSIGNED_INTEGER};    /* The application loaded to sram is not meant for this product.  */
static err_type InvalidBootloaderPcCounter                         = {NO_CURRENT_TIME, 0x041A, ZERO_ERRORS, WARNING, UNSIGNED_INTEGER};    /* The PC counter is above the max bootloader address. This indicates that the PC counter is somewhere not in the bootloader. Something is very long */
static err_type InvalidAppNumber                                   = {NO_CURRENT_TIME, 0x041B, ZERO_ERRORS, WARNING, UNSIGNED_INTEGER};    /* The selected app that was read from flash was is above 3 and is incorrect. The default is to select app the golden image or app=3 */
static err_type InvalidAppSelection                                = {NO_CURRENT_TIME, 0x041C, ZERO_ERRORS, WARNING, UNSIGNED_INTEGER};    /* The app to be selected to go from flash to sram is wrong. This progammically should not happen so something is very wrong */
static err_type EndOfBootloaderMain                                = {NO_CURRENT_TIME, 0x041D, ZERO_ERRORS, WARNING, UNSIGNED_INTEGER};    /* The bootloader program finished before it jump to the application. This should not happen something is very wrong. */

static err_type EmbeddedTimeout                                    = {NO_CURRENT_TIME, 0x0600, ZERO_ERRORS, WARNING, UNSIGNED_INTEGER};    /* This error indicates that a embedded system timed out */
static err_type NotEnoughBytes                                     = {NO_CURRENT_TIME, 0x0601, ZERO_ERRORS, WARNING, UNSIGNED_INTEGER};    /* The incoming data did not have enough information to be parsed correctly. */
static err_type MalformedHeader                                    = {NO_CURRENT_TIME, 0x0602, ZERO_ERRORS, WARNING, UNSIGNED_INTEGER};    /* The clank parser has been improperly been formatted when in use. */
static err_type SyncBytesError                                     = {NO_CURRENT_TIME, 0x0603, ZERO_ERRORS, WARNING, UNSIGNED_INTEGER};    /* The sync byte value from the clank packet did not match the proper sync byte value. */
static err_type SourceAddressError                                 = {NO_CURRENT_TIME, 0x0604, ZERO_ERRORS, WARNING, UNSIGNED_INTEGER};    /* The source address from a data stream came from hardware that was not expected. IE: The TX data's transmit mask did not match there source address. */
static err_type TransmitMaskError                                  = {NO_CURRENT_TIME, 0x0605, ZERO_ERRORS, WARNING, UNSIGNED_INTEGER};    /* The incoming data is not meant for this hardware. IE: The RX Transmit mask does not allgin with this hardwares Source Address */
static err_type PktDataLenError                                    = {NO_CURRENT_TIME, 0x0606, ZERO_ERRORS, WARNING, UNSIGNED_INTEGER};    /* There is a difference in the reported data length and the actual calculated data length */
static err_type ComRxCrcError                                      = {NO_CURRENT_TIME, 0x0607, ZERO_ERRORS, WARNING, UNSIGNED_INTEGER};    /* The CRC on the incoming data mismatched.  */
static err_type RftInternCommTimeout                               = {NO_CURRENT_TIME, 0x0608, ZERO_ERRORS, WARNING, UNSIGNED_INTEGER};    /* The transmission for an internal RFT command timed out. */
static err_type RftInternCommInvalidPkt                            = {NO_CURRENT_TIME, 0x0609, ZERO_ERRORS, WARNING, UNSIGNED_INTEGER};    /* The packet received from the internal command to the RFT failed due to either malformed header, invalid CRC, or failed status acknowledge. */

static err_type CmdMsgIdNotFound                                   = {NO_CURRENT_TIME, 0x0800, ZERO_ERRORS, WARNING, UNSIGNED_INTEGER};    /* The Msg ID sent from the spacecraft/master (PC, Spacecraft, Prop Controller, etc.) is not found in the hardware unit (Prop Controller, Thruster Controller etc.) */
static err_type CmdMsgIdStateNotSupported                          = {NO_CURRENT_TIME, 0x0801, ZERO_ERRORS, WARNING, UNSIGNED_INTEGER};    /* The state the current hardware is in does not support the requested msg id command. Please go to the allowable state to execute command. */
static err_type CmdMsgIdInvalidDataSize                            = {NO_CURRENT_TIME, 0x0802, ZERO_ERRORS, WARNING, UNSIGNED_INTEGER};    /* The data size needed for the msg id command to run correctly is not correct. Please insert the correct data length for proper function of this command. */
static err_type CmdInvalidMaxwellHardwareOffset                    = {NO_CURRENT_TIME, 0x0803, ZERO_ERRORS, WARNING, UNSIGNED_INTEGER};    /* The offset value in flash is greater then the max allowable offset. Thus it will be accessing a area of flash that does not have a hardware ID record. */
static err_type CmdInvalidMaxRecordDataSize                        = {NO_CURRENT_TIME, 0x0804, ZERO_ERRORS, WARNING, UNSIGNED_INTEGER};    /* The hardware record that was sent from the spacecraft/computer to be written is not the correct size.  It will not write the data into flash */
static err_type CmdIllformedCommaMaxRecordPacket                   = {NO_CURRENT_TIME, 0x0805, ZERO_ERRORS, WARNING, UNSIGNED_INTEGER};    /* There is not enough commas to properly parse the data. It will not write the data into flash */
static err_type CmdMaxWriteRecordOffsetFull                        = {NO_CURRENT_TIME, 0x0806, ZERO_ERRORS, WARNING, UNSIGNED_INTEGER};    /* The offest in flash that is trying to be written is not cleared. Thus inorder to write the data properly, erase flash before writing into this record offset. */
static err_type CmdSwUpdateBelowMinDataAmount                      = {NO_CURRENT_TIME, 0x0807, ZERO_ERRORS, WARNING, UNSIGNED_INTEGER};    /* The data coming in for a sw update is below the minimmum needed for a succesfull update. This error occurs in the sw_staging step in the software update process. */
static err_type CmdProcessingPreviousCommand                       = {NO_CURRENT_TIME, 0x0808, ZERO_ERRORS, WARNING, UNSIGNED_INTEGER};    /* The command that was just is being rejected because it is currently processing the previous command. This command will need to be resent to be executed. */
static err_type CmdProcessedCommandTimedout                        = {NO_CURRENT_TIME, 0x0809, ZERO_ERRORS, WARNING, UNSIGNED_INTEGER};    /* The command that was sent had a timeout when processing the command. Please resend the command */
static err_type CmdInvalidStateTransition                          = {NO_CURRENT_TIME, 0x080A, ZERO_ERRORS, WARNING, UNSIGNED_INTEGER};    /* The state the command wanted to go to is invalid. Please refer to the state machine on how to get to the desired state correctly. */
static err_type CmdCurrentStateIsBusy                              = {NO_CURRENT_TIME, 0x080B, ZERO_ERRORS, WARNING, UNSIGNED_INTEGER};    /* The state the command wants to transition to can not be transitioned because  the current transition is running a sequence that can not be interrupted.  */
static err_type CmdTlmDumpInvalidMemChipSelected                   = {NO_CURRENT_TIME, 0x080C, ZERO_ERRORS, WARNING, UNSIGNED_INTEGER};    /* The argument that selects what external memory chip you want to dump from (FRAM or FLASH) is invalid. Please select a apporiprate flash type (FRAM = 0X0A, Flash = 0x0B) */
static err_type CmdTlmDumpInvalidBankSelected                      = {NO_CURRENT_TIME, 0x080D, ZERO_ERRORS, WARNING, UNSIGNED_INTEGER};    /* This error inidicates that the argument that selects what bank of memory to dump from a given external memory chip is wrong. Currently there is bank 0 (0x00), bank 1 (0x01), or both (0x02) */
static err_type CmdSetHeaterCurrentOutOfBounds                     = {NO_CURRENT_TIME, 0x080E, ZERO_ERRORS, WARNING, FLOAT};    /* This error inidicates that the argument that provides how much current should go in the heater is out of bounds. Please select a current that is in the correct bounds. */
static err_type CmdSetHpisoCurrentOutOfBounds                      = {NO_CURRENT_TIME, 0x080F, ZERO_ERRORS, WARNING, FLOAT};    /* This error inidicates that the argument that provides how much current should go in the HPISO is out of bounds. Please select a current that is in the correct bounds. */
static err_type CmdSetLpisoCurrentOutOfBounds                      = {NO_CURRENT_TIME, 0x0810, ZERO_ERRORS, WARNING, FLOAT};    /* This error inidicates that the argument that provides how much current should go in the LPISO is out of bounds. Please select a current that is in the correct bounds. */
static err_type CmdInvalidAutoTlmRange                             = {NO_CURRENT_TIME, 0x0811, ZERO_ERRORS, WARNING, UNSIGNED_INTEGER};    /* The argument that provides the rate of tlm is outside of the current tlm bounds. */
static err_type CmdInvalidMcuDataDumpSize                          = {NO_CURRENT_TIME, 0x0812, ZERO_ERRORS, WARNING, UNSIGNED_INTEGER};    /* The argument that provides the amount of data to return is over the specfied clank limit. Please set the data amount less then the max clank data size which is currently 254 */
static err_type CmdInvalidHeaterPwmRawInvalidDutyCycle             = {NO_CURRENT_TIME, 0x0813, ZERO_ERRORS, WARNING, UNSIGNED_INTEGER};    /* The argument that provides the duty cycle for the heater pwm is over the max amount. Please specify a value under this amount. Currently the max amount is 0x8000 which equals 100% */
static err_type CmdInvalidHpisoPwmRawInvalidDutyCycle              = {NO_CURRENT_TIME, 0x0814, ZERO_ERRORS, WARNING, UNSIGNED_INTEGER};    /* The argument that provides the duty cycle for the HPISO pwm is over the max amount. Please specify a value under this amount. Currently the max amount is 0x8000 which equals 100% */
static err_type CmdInvalidLpisoPwmRawInvalidDutyCycle              = {NO_CURRENT_TIME, 0x0815, ZERO_ERRORS, WARNING, UNSIGNED_INTEGER};    /* The argument that provides the duty cycle for the LPISO pwm is over the max amount. Please specify a value under this amount. Currently the max amount is 0x8000 which equals 100% */
static err_type CmdInvalidMdotSetpt                                = {NO_CURRENT_TIME, 0x0816, ZERO_ERRORS, WARNING, FLOAT};    /* The commanded flow rate in SCCM was out of bounds based on the parameter table. */
static err_type CmdInvalidRfVoltageSetpt                           = {NO_CURRENT_TIME, 0x0817, ZERO_ERRORS, WARNING, FLOAT};    /* The commanded RF Voltage was out of bounds based on the parameter table. */
static err_type CmdInvalidDurationSetpt                            = {NO_CURRENT_TIME, 0x0818, ZERO_ERRORS, WARNING, FLOAT};    /* The error indicates that the commanded duration in seconds was out of bounds based on the parameter table. */
static err_type CmdDataModeIsBusy                                  = {NO_CURRENT_TIME, 0x0819, ZERO_ERRORS, WARNING, UNSIGNED_INTEGER};    /* The error indicates that the commanded data request is busy given the data mode. */
static err_type CmdUpdateStageInvalidParam                         = {NO_CURRENT_TIME, 0x081A, ZERO_ERRORS, WARNING, UNSIGNED_INTEGER};    /* SW update stage command parameter is invalid */
static err_type CmdUpdatePreloadInvalidParam                       = {NO_CURRENT_TIME, 0x081B, ZERO_ERRORS, WARNING, UNSIGNED_INTEGER};    /* SW update preload command parameter is invalid */
static err_type CmdUpdateProgramInvalidParam                       = {NO_CURRENT_TIME, 0x081C, ZERO_ERRORS, WARNING, UNSIGNED_INTEGER};    /* SW update program command parameter is invalid */
static err_type CmdDataAbortDueToSafe                              = {NO_CURRENT_TIME, 0x081D, ZERO_ERRORS, WARNING, UNSIGNED_INTEGER};    /* The current data command could not complete due to a system SAFE function. Likely due to FDIR. */
static err_type CmdUpdateProgramStageInvalid                       = {NO_CURRENT_TIME, 0x081E, ZERO_ERRORS, WARNING, UNSIGNED_INTEGER};    /* SW update program failed due to either an invalid image length or CRC in the FRAM stage area. Error data is the staged CRC */
static err_type CmdRejectedErrorActive                             = {NO_CURRENT_TIME, 0x081F, ZERO_ERRORS, WARNING, UNSIGNED_INTEGER};    /* State transition unable to be performed due to either a FDIR error that remains active or, if transitioning to Thrust, invalid thrust parameters */
static err_type CmdInvalidParameter                                = {NO_CURRENT_TIME, 0x0820, ZERO_ERRORS, WARNING, UNSIGNED_INTEGER};    /* The command parameter passed is invalid. See SUM for the appropriate parameter range. */
static err_type CmdInvalidGetSoftwareInfoSelection                 = {NO_CURRENT_TIME, 0x0821, ZERO_ERRORS, WARNING, UNSIGNED_INTEGER};    /* The argument that selects what software application you want information on is not supported. */
static err_type CmdRftVirtualAddrMismatch                          = {NO_CURRENT_TIME, 0x0822, ZERO_ERRORS, WARNING, UNSIGNED_INTEGER};    /* This error indicates that not all parameters agree on an address space. Commands intended for the Thr Ctrl must all have an address space starting with 0xFXXXX_XXXX. */
static err_type CmdUpdateProgramApp1PtInUse                        = {NO_CURRENT_TIME, 0x0824, ZERO_ERRORS, WARNING, UNSIGNED_INTEGER};    /* SW update program command parameter indicates loading to App1 PT when it is currently in use, recommend loading from Golden Image */
static err_type CmdUpdateProgramApp2PtInUse                        = {NO_CURRENT_TIME, 0x0825, ZERO_ERRORS, WARNING, UNSIGNED_INTEGER};    /* SW update program command parameter indicates loading to App2 PT when it is currently in use, recommend loading from Golden Image */
static err_type CmdInvalidPropPhaseSuspect                         = {NO_CURRENT_TIME, 0x0826, ZERO_ERRORS, WARNING, UNSIGNED_INTEGER};    /* Cannot complete command due to the potentially liquid state of the propellant */
static err_type CmdInvalidHeaterCtrlSetpt                          = {NO_CURRENT_TIME, 0x0827, ZERO_ERRORS, WARNING, FLOAT};    /* Desired heater control setpoint is either invalid or beyond the acceptable range */
static err_type CmdNoSoulToSell                                    = {NO_CURRENT_TIME, 0x0828, ZERO_ERRORS, WARNING, FLOAT};    /* The currently loaded PT failed integrity checking prior to entering the mode, error data is the calculated CRC */
static err_type CmdWishNotGranted                                  = {NO_CURRENT_TIME, 0x0829, ZERO_ERRORS, WARNING, FLOAT};    /* The PT parameter offset into the table is invalid, error data is the offset requested */
static err_type CmdBusySendingAutoTlm                              = {NO_CURRENT_TIME, 0x0830, ZERO_ERRORS, WARNING, UNSIGNED_INTEGER};    /*  */
static err_type CmdInvalidPpVoltSetpointInvalid                    = {NO_CURRENT_TIME, 0x0831, ZERO_ERRORS, WARNING, FLOAT};    /* The push-pull command voltage argument is outside of the bound of acceptable voltage. The current range is 0 - 110 Volts */
static err_type CmdInvalidPpRampToHigh                             = {NO_CURRENT_TIME, 0x0832, ZERO_ERRORS, WARNING, FLOAT};    /* When sending the push-pull ramp command the rate at which the voltage needs to increase is to fast. The current rate is 5 volts every 20 ms. This is for the saftey of the system. */

static err_type MsgIdFunctionNotImplemented                        = {NO_CURRENT_TIME, 0x0C00, ZERO_ERRORS, WARNING, UNSIGNED_INTEGER};    /* The MSG ID that was sent by the master (PC/Spacecraft) Has been found but has not been implented. This is a pure internal software issue. If you find this please report to Phase Four */
static err_type CurrentProcessedCommandTimedout                    = {NO_CURRENT_TIME, 0x0C01, ZERO_ERRORS, WARNING, UNSIGNED_INTEGER};    /* The command that was sent had a timeout when processing the command. Please resend the command. If that fails a bigger issue is happening. */
static err_type UnexpectedTransition                               = {NO_CURRENT_TIME, 0x0C02, ZERO_ERRORS, WARNING, UNSIGNED_INTEGER};    /* There was internal unexcpect transition of state. */
static err_type Ad568SpiSetFailed                                  = {NO_CURRENT_TIME, 0x0C03, ZERO_ERRORS, WARNING, UNSIGNED_INTEGER};    /* The spi transfer to the DAC has failed please try sending the current again. */
static err_type MdotSetPointOutOfBounds                            = {NO_CURRENT_TIME, 0x0C04, ZERO_ERRORS, WARNING, UNSIGNED_INTEGER};    /* The set point for setting the pfcv is out of the current acceptable range. */
static err_type InvalidTxParameters                                = {NO_CURRENT_TIME, 0x0C05, ZERO_ERRORS, WARNING, UNSIGNED_INTEGER};    /* This error inidcates that the TX parameter that was sent to the TxAO was invalid  (TxAO 327) */
static err_type IntermediateTxQueueHasNoData                       = {NO_CURRENT_TIME, 0x0C06, ZERO_ERRORS, WARNING, UNSIGNED_INTEGER};    /* There was no data to be sent in the intermediate queue.  (TxAO 281) */
static err_type TxAoIsBusy                                         = {NO_CURRENT_TIME, 0x0C07, ZERO_ERRORS, WARNING, UNSIGNED_INTEGER};    /* The TxAO module is currently busy.  (TxAO 404) */
static err_type TlmTmrTimeout                                      = {NO_CURRENT_TIME, 0x0C08, ZERO_ERRORS, WARNING, UNSIGNED_INTEGER};    /* Timeout attempting to send one or more full TLM packets simultaneously (TxAO 246), error data is the timeout duration in microseconds */
static err_type InvalidTelemtryEventRequest                        = {NO_CURRENT_TIME, 0x0C09, ZERO_ERRORS, WARNING, UNSIGNED_INTEGER};    /* The event that came from the fifo queue is illfomred or has no data  ( TlmAO 156) */
static err_type RxScReciviedJunkData                               = {NO_CURRENT_TIME, 0x0C0A, ZERO_ERRORS, WARNING, UNSIGNED_INTEGER};    /* This error indicates that junk data has been recived on the receiving end of the  SC port  (RxScAO 223) */
static err_type ReceivedMalformClankPacket                         = {NO_CURRENT_TIME, 0x0C0B, ZERO_ERRORS, WARNING, UNSIGNED_INTEGER};    /* The data we recived is not a valid clank header. (RxScAO 297) */
static err_type ReceivedRxscTimeout                                = {NO_CURRENT_TIME, 0x0C0C, ZERO_ERRORS, WARNING, UNSIGNED_INTEGER};    /* There was a time when receving data from the spacecraft port. (RxScAO 276) */
static err_type MismatchCrc                                        = {NO_CURRENT_TIME, 0x0C0D, ZERO_ERRORS, WARNING, UNSIGNED_INTEGER};    /* The CRC was incorrect. (RxGseAO 370) */
static err_type GseRxCircularBufferOverrun                         = {NO_CURRENT_TIME, 0x0C0E, ZERO_ERRORS, WARNING, UNSIGNED_INTEGER};    /* There was a overrun on the RX's circular buffer from the GSE Port.  (RxGseAO 76) */
static err_type EvtCmdDumpTlmInvalidSize                           = {NO_CURRENT_TIME, 0x0C0F, ZERO_ERRORS, WARNING, UNSIGNED_INTEGER};    /* Indicates that the passed EVT utilized for InterAO communication did not have the expected amount of data. Error data indicates the size of the data passed within the event */
static err_type EvtDataOpTimeout                                   = {NO_CURRENT_TIME, 0x0C10, ZERO_ERRORS, WARNING, UNSIGNED_INTEGER};    /* A data operation timed out, likely indicates external memory comm is struggling. Error data is the duration in microseconds of the timeout */
static err_type Ltc6903SpiSetFailed                                = {NO_CURRENT_TIME, 0x0C11, ZERO_ERRORS, WARNING, FLOAT};    /* The spi transfer to the LTC6903 has failed please try sending the current again. Error data is the attempted frequency setting. */
static err_type InvalidAppSelectParamater                          = {NO_CURRENT_TIME, 0x0C12, ZERO_ERRORS, WARNING, UNSIGNED_INTEGER};    /* The software selected a invalid application to load from. The only valid selection is 1=Primary App 1, 2=Primary App 2, 3=Golden Image */
static err_type CmdStateTransitionFail                             = {NO_CURRENT_TIME, 0x0C13, ZERO_ERRORS, WARNING, UNSIGNED_INTEGER};    /* The state transitioned into was not what was commanded */
static err_type FlashEraseFailed                                   = {NO_CURRENT_TIME, 0x0C20, ZERO_ERRORS, WARNING, UNSIGNED_INTEGER};    /* This error inidcates that erasing flash failed. A generic catch all flash erase failure */
static err_type FlashEraseFailedInvalidEraseAddress                = {NO_CURRENT_TIME, 0x0C21, ZERO_ERRORS, WARNING, UNSIGNED_INTEGER};    /* This error inidcates that when attempting to erase flash the address specfied is lower then the erase address limit. This limit is to protect accidental erasing of the bootloader or the golden image. */
static err_type FlashEraseFailedInvalidSectorSize                  = {NO_CURRENT_TIME, 0x0C22, ZERO_ERRORS, WARNING, UNSIGNED_INTEGER};    /* This error inidcates that when attempting to erase flash the erase size is not the size of the flash sector. To erase flash correctly the length must be the same as the flash sector.  */
static err_type FlashEraseFailedInvalidSectorAddress               = {NO_CURRENT_TIME, 0x0C23, ZERO_ERRORS, WARNING, UNSIGNED_INTEGER};    /* This error inidicates that when attempting to erase flash the specified start address was not at the beginning of a flash sector. Specfy a address that is the start of a flash sector. */
static err_type FlashEraseFailedFlashStatusBusy                    = {NO_CURRENT_TIME, 0x0C24, ZERO_ERRORS, WARNING, UNSIGNED_INTEGER};    /* The flash module is currently busy writing or erasing another section in flash */
static err_type FlashWriteFailed                                   = {NO_CURRENT_TIME, 0x0C28, ZERO_ERRORS, WARNING, UNSIGNED_INTEGER};    /* This error indicates that writing to flash has failed. A generic catch all flash write failure */
static err_type FlashWriteFailedInvalidEraseAddress                = {NO_CURRENT_TIME, 0x0C29, ZERO_ERRORS, WARNING, UNSIGNED_INTEGER};    /* This error inidcates that when attempting to write flash the address specfied is lower then the write address limit. This limit is to protect accidental writing over of the bootloader or the golden image. */
static err_type FlashWriteFailedOverLimitWriteLength               = {NO_CURRENT_TIME, 0x0C2A, ZERO_ERRORS, WARNING, UNSIGNED_INTEGER};    /* This error indicates that when attempting to write to flash the write amount to flash is to large for this module. */
static err_type FlashWriteFailedFlashStatusBusy                    = {NO_CURRENT_TIME, 0x0C2B, ZERO_ERRORS, WARNING, UNSIGNED_INTEGER};    /* The flash module is currently busy writing or erasing another section in flash */
static err_type UsingDefaultPtInvalidAppSel                        = {NO_CURRENT_TIME, 0x0C30, ZERO_ERRORS, WARNING, UNSIGNED_INTEGER};    /* The Default Parameter Table is selected due to an invalid Application Select value, error data is the app_select value */
static err_type UsingDefaultPtTypeMismatch                         = {NO_CURRENT_TIME, 0x0C31, ZERO_ERRORS, WARNING, UNSIGNED_INTEGER};    /* The Default Parameter Table is selected due to the image type code mismatching the one defined for the loaded Application, error data is the PT image type code */
static err_type UsingDefaultPtLengthMismatch                       = {NO_CURRENT_TIME, 0x0C32, ZERO_ERRORS, WARNING, UNSIGNED_INTEGER};    /* The Default Parameter Table is selected due to the image length mismatching the one defined for the loaded Application, error data is the PT image length */
static err_type UsingDefaultPtCrcMismatch                          = {NO_CURRENT_TIME, 0x0C33, ZERO_ERRORS, WARNING, UNSIGNED_INTEGER};    /* The Default Parameter Table is selected due to the Application PT failing its CRC check, error data is the computed App PT CRC */
static err_type UsingAppPt                                         = {NO_CURRENT_TIME, 0x0C34, ZERO_ERRORS, WARNING, UNSIGNED_INTEGER};    /* The App Parameter Table is selected due to the failure to successfully copy over the table to the Runtime PT area, error data is the address of the App PT in use */
static err_type ThrustHpisoSpikeFail                               = {NO_CURRENT_TIME, 0x0C35, ZERO_ERRORS, SAFE, FLOAT};       /* Commanding HPISO to spike failed, error data is the value attempting to be set */
static err_type ThrustHpisoHoldFail                                = {NO_CURRENT_TIME, 0x0C36, ZERO_ERRORS, SAFE, FLOAT};       /* Commanding HPISO to hold failed, error data is the value attempting to be set */
static err_type ThrustLpisoSpikeFail                               = {NO_CURRENT_TIME, 0x0C37, ZERO_ERRORS, SAFE, FLOAT};       /* Commanding LPISO to spike failed, error data is the value attempting to be set */
static err_type ThrustLpisoHoldFail                                = {NO_CURRENT_TIME, 0x0C38, ZERO_ERRORS, SAFE, FLOAT};       /* Commanding LPISO to hold failed, error data is the value attempting to be set */
static err_type FlowRateInvalidCalculation                         = {NO_CURRENT_TIME, 0x0C39, ZERO_ERRORS, WARNING, FLOAT};    /* Cannot complete flow rate determination due to div-by-zero error */
static err_type HeaterCtrlInvalidSetpt                             = {NO_CURRENT_TIME, 0x0C3A, ZERO_ERRORS, WARNING, FLOAT};    /* Cannot set the current heater control setpoint due to an invalid value */
static err_type TlmInvalidRequestData                              = {NO_CURRENT_TIME, 0x0C3B, ZERO_ERRORS, WARNING, UNSIGNED_INTEGER};    /* Internal error in the request to the TLM AO */
static err_type ErraoErrorFlashStorageFull                         = {NO_CURRENT_TIME, 0x0C40, ZERO_ERRORS, WARNING, UNSIGNED_INTEGER};    /* The flash location that hold errors are full. This should normally not happend and ethier there was a implenetation error. Or there clearing flash failed. */
static err_type ErraoCantDetermineCurrentFlashAddress              = {NO_CURRENT_TIME, 0x0C41, ZERO_ERRORS, WARNING, UNSIGNED_INTEGER};    /* The current flash address can not be determined. This is likely due to the fact that the flash location is full of junk or errors and was not cleared correctly. */
static err_type ErraoCurrentFlashAddressOutOfBounds                = {NO_CURRENT_TIME, 0x0C42, ZERO_ERRORS, WARNING, UNSIGNED_INTEGER};    /* The current flash adddress is out of the error flash address bounds. This can only happen if it was this value was updated to go out of bounds in the ERRAO or somewhere else (rouge code) */
static err_type ErraoClrErrorsEvtNoData                            = {NO_CURRENT_TIME, 0x0C43, ZERO_ERRORS, WARNING, UNSIGNED_INTEGER};    /* Clear error event has invalid data, error data returned has both the request has_data flag and the data_size in the LSBs respectively. */
static err_type ErraoClrFlashFail                                  = {NO_CURRENT_TIME, 0x0C44, ZERO_ERRORS, WARNING, UNSIGNED_INTEGER};    /* Erasing flash failed, error data returned is the SDK status */
static err_type ErraoEraseSectorFail                               = {NO_CURRENT_TIME, 0x0C45, ZERO_ERRORS, WARNING, UNSIGNED_INTEGER};    /* Erasing flash sector failed, error data returned is the SDK status */
static err_type PropInvInvalidPropCalculation                      = {NO_CURRENT_TIME, 0x0C50, ZERO_ERRORS, WARNING, FLOAT};    /* This error indicates that the prop inventory/estimation theory calculation is out of the appropriate bounds determined by our analysis. Something is wrong with the prop inventory/estimation theory calculation */
static err_type Uart0RxError                                       = {NO_CURRENT_TIME, 0x0C60, ZERO_ERRORS, WARNING, UNSIGNED_INTEGER};    /* LPUART0 module detected an overrun, noise, framing, or  parity error. STAT reg returned in error data. */
static err_type Uart0RxDmaError                                    = {NO_CURRENT_TIME, 0x0C61, ZERO_ERRORS, WARNING, UNSIGNED_INTEGER};    /* UART Rx DMA Channel tied to LPUART0 detected an error, DMA Error Status register returned in error data */
static err_type Uart1RxError                                       = {NO_CURRENT_TIME, 0x0C62, ZERO_ERRORS, WARNING, UNSIGNED_INTEGER};    /* LPUART1 module detected an overrun, noise, framing, or  parity error. STAT reg returned in error data. */
static err_type Uart1RxDmaError                                    = {NO_CURRENT_TIME, 0x0C63, ZERO_ERRORS, WARNING, UNSIGNED_INTEGER};    /* UART Rx DMA Channel tied to LPUART1 detected an error, DMA Error Status register returned in error data */
static err_type Uart0TxDmaError                                    = {NO_CURRENT_TIME, 0x0C70, ZERO_ERRORS, WARNING, UNSIGNED_INTEGER};    /* DMA error was detected during transmission over LPUART0, DMA Error Status register returned in error data */
static err_type Uart1TxDmaError                                    = {NO_CURRENT_TIME, 0x0C71, ZERO_ERRORS, WARNING, UNSIGNED_INTEGER};    /* DMA error was detected during transmission over LPUART1, DMA Error Status register returned in error data */
static err_type TxaoTxTimeout                                      = {NO_CURRENT_TIME, 0x0C72, ZERO_ERRORS, WARNING, UNSIGNED_INTEGER};    /* The transmission of a packet timed out */
static err_type TxaoInterqFull                                     = {NO_CURRENT_TIME, 0x0C73, ZERO_ERRORS, WARNING, UNSIGNED_INTEGER};    /* Cannot push another Tx request because the FIFO queue is full, the number of entries in the queue is returned in error data */
static err_type TxaoInterqNoData                                   = {NO_CURRENT_TIME, 0x0C74, ZERO_ERRORS, WARNING, UNSIGNED_INTEGER};    /* Attempting to pop from the Tx FIFO queue failed due to having invalid or nonexistent Tx request data, error data returned has both the request has_data flag and the data_size in the LSBs respectively. */
static err_type TxaoInvalidTxParams                                = {NO_CURRENT_TIME, 0x0C75, ZERO_ERRORS, WARNING, UNSIGNED_INTEGER};    /* The Tx request type does not exist. The Tx request type enum is returned in error data */
static err_type TxaoUartBusy                                       = {NO_CURRENT_TIME, 0x0C76, ZERO_ERRORS, WARNING, UNSIGNED_INTEGER};    /* The transmission cannot commence because the UART is currently busy. Error data returned has both the Tx request seq ID and msg ID in the LSBs respectively */
static err_type DataaoFramInitFail                                 = {NO_CURRENT_TIME, 0x0C80, ZERO_ERRORS, WARNING, UNSIGNED_INTEGER};    /* Unable to initialize FRAM and read the device ID, could indicate that SW is loaded on the wrong hardware. */
static err_type DataaoNandInitFail                                 = {NO_CURRENT_TIME, 0x0C81, ZERO_ERRORS, WARNING, UNSIGNED_INTEGER};    /* Unable to initialize NAND and read the device ID */
static err_type DataaoFramWriteFail                                = {NO_CURRENT_TIME, 0x0C82, ZERO_ERRORS, WARNING, UNSIGNED_INTEGER};    /* TLM packet recording failure when writing to FRAM, error data is the current FRAM address */
static err_type DataaoNandLastProgFail                             = {NO_CURRENT_TIME, 0x0C83, ZERO_ERRORS, WARNING, UNSIGNED_INTEGER};    /* TLM packet programming to NAND failed, the error data is the current NAND address (not the failed address) for some context */
static err_type DataaoNandBufWriteFail                             = {NO_CURRENT_TIME, 0x0C84, ZERO_ERRORS, WARNING, UNSIGNED_INTEGER};    /* TLM packet writing to NAND internal buffer failed trnasmissions, the error data is the current NAND address */
static err_type DataaoNandEraseFail                                = {NO_CURRENT_TIME, 0x0C85, ZERO_ERRORS, WARNING, UNSIGNED_INTEGER};    /* Command to initiate a NAND block erase failed, the error data is the current NAND address */
static err_type DataaoNandLoadFail                                 = {NO_CURRENT_TIME, 0x0C86, ZERO_ERRORS, WARNING, UNSIGNED_INTEGER};    /* Command to initiate loading of the TLM data from internal buffer to the cell failed, the error data is the current NAND address */
static err_type DataaoNandEraseFullTimeout                         = {NO_CURRENT_TIME, 0x0C87, ZERO_ERRORS, WARNING, UNSIGNED_INTEGER};    /* Could not complete the full erasure of NAND due to a timeout, the error data is the current NAND address */
static err_type DataaoNandReadFail                                 = {NO_CURRENT_TIME, 0x0C88, ZERO_ERRORS, WARNING, UNSIGNED_INTEGER};    /* Could not read from the NAND internal buffer, the error data is the current NAND address */
static err_type DataaoFramReadFail                                 = {NO_CURRENT_TIME, 0x0C89, ZERO_ERRORS, WARNING, UNSIGNED_INTEGER};    /* Could not read from FRAM, the error data is the current FRAM address, the error data is the current FRAM address */
static err_type DataaoFramMetadataWriteFail                        = {NO_CURRENT_TIME, 0x0C8A, ZERO_ERRORS, WARNING, UNSIGNED_INTEGER};    /* Could not initiate FRAM metadata write due to FRAM being busy */
static err_type DataaoUpProgStagedCrcFail                          = {NO_CURRENT_TIME, 0x0C8B, ZERO_ERRORS, WARNING, UNSIGNED_INTEGER};    /* Aborted SW Update due to an invalid CRC was computed for the staged image in FRAM, the error data is the computed CRC */
static err_type DataaoUpProgStagedCrcTimeout                       = {NO_CURRENT_TIME, 0x0C8C, ZERO_ERRORS, WARNING, UNSIGNED_INTEGER};    /* Aborted SW Update due to a timeout that occurred during staged image computation, the error data is the current FRAM address */
static err_type DataaoUpProgFlashTimeout                           = {NO_CURRENT_TIME, 0x0C8D, ZERO_ERRORS, WARNING, UNSIGNED_INTEGER};    /* Aborted SW Update due to timeout that occurred when reading the staged image during flash programming, the error data is the current FRAM read address */
static err_type DataaoUpProgFlashFail                              = {NO_CURRENT_TIME, 0x0C8E, ZERO_ERRORS, WARNING, UNSIGNED_INTEGER};    /* Aborted SW Update due to a failure to program the flash, the error data is the current flash address */
static err_type DataaoUpProgFinalCrcFail                           = {NO_CURRENT_TIME, 0x0C8F, ZERO_ERRORS, WARNING, UNSIGNED_INTEGER};    /* The final CRC validation of the image in flash failed, the error data is the calculated CRC value */
static err_type SpiAdcDmaRxOverrun                                 = {NO_CURRENT_TIME, 0x0C90, ZERO_ERRORS, WARNING, UNSIGNED_INTEGER};    /* The DMA ADC reception process attempted to read data from the SPI Rx that was already empty */
static err_type SpiAdcDmaTxUnderrun                                = {NO_CURRENT_TIME, 0x0C91, ZERO_ERRORS, WARNING, UNSIGNED_INTEGER};    /* The DMA ADC transmission process attempted to send data to the SPI Tx but it was not empty */
static err_type SpiAdcDmaError                                     = {NO_CURRENT_TIME, 0x0C92, ZERO_ERRORS, WARNING, UNSIGNED_INTEGER};    /* Generic DMA ADC error, error data is the SDK error that was reported */
static err_type PwmDriverInitFail                                  = {NO_CURRENT_TIME, 0x0C93, ZERO_ERRORS, WARNING, UNSIGNED_INTEGER};    /* Initialization of the PWM Drivers failed, likely a development error related to incorrect PWM usage or an invalid Processor Expert config, error data is the Flextimer PWM instance that failed */
static err_type WfOscInitFail                                      = {NO_CURRENT_TIME, 0x0C94, ZERO_ERRORS, WARNING, UNSIGNED_INTEGER};    /* Waveform Generator Oscillator init failed. Error data returned is the SDK status */
static err_type RftaoInterqFull                                    = {NO_CURRENT_TIME, 0x0CA0, ZERO_ERRORS, WARNING, UNSIGNED_INTEGER};    /* Cannot push another RFT Tx request because the FIFO queue is full, the number of entries in the queue is returned in error data */
static err_type RftaoInterqNoData                                  = {NO_CURRENT_TIME, 0x0CA1, ZERO_ERRORS, WARNING, UNSIGNED_INTEGER};    /* Attempting to pop from the Rft Tx FIFO queue failed due to having invalid or nonexistent Tx request data, error data returned has both the request has_data flag and the data_size in the LSBs respectively. */
static err_type RftaoUartBusy                                      = {NO_CURRENT_TIME, 0x0CA2, ZERO_ERRORS, WARNING, UNSIGNED_INTEGER};    /* The transfer cannot commence because the UART is currently busy. Error data returned has both the RFT Tx request type and msg ID in the LSBs respectively */
static err_type RftaoXferTimeout                                   = {NO_CURRENT_TIME, 0x0CA3, ZERO_ERRORS, WARNING, UNSIGNED_INTEGER};    /* The RFT transfer timed out. Error data returned has both the RFT Tx request type and msg ID in the LSBs respectively */
static err_type RftaoXferInvalidPkt                                = {NO_CURRENT_TIME, 0x0CA4, ZERO_ERRORS, WARNING, UNSIGNED_INTEGER};    /* The RFT Rx packet had an invalid clank header or CRC. Error data returned has both the RFT Tx request type and msg ID in the LSBs respectively */
static err_type PpvsetPwmRawInvalid                                = {NO_CURRENT_TIME, 0x0D00, ZERO_ERRORS, WARNING, UNSIGNED_INTEGER};    /* The PP VSET raw value is invalid. Error data returned is the duty cycle count value. */
static err_type PpvsetPwmVInvalid                                  = {NO_CURRENT_TIME, 0x0D01, ZERO_ERRORS, WARNING, FLOAT};    /* The PP VSET voltage value is invalid */

static err_type McuNmiInterrupt                                    = {NO_CURRENT_TIME, 0x1000, ZERO_ERRORS, UNRECOVERABLE, UNSIGNED_INTEGER}; /* MCU detected a Non-Maskable Interrupt Fault, error data is the excepting instruction address */
static err_type McuHardFaultInterrupt                              = {NO_CURRENT_TIME, 0x1001, ZERO_ERRORS, UNRECOVERABLE, UNSIGNED_INTEGER}; /* MCU detected a Hard Fault, only faults when other exception handlers fault, error data is the excepting instruction address */
static err_type McuMemManageInterrupt                              = {NO_CURRENT_TIME, 0x1002, ZERO_ERRORS, UNRECOVERABLE, UNSIGNED_INTEGER}; /* MCU detected a Memory Manager fault, implying a memory access voilation has occurred, error data is the excepting instruction address */
static err_type McuBusFaultInterrupt                               = {NO_CURRENT_TIME, 0x1003, ZERO_ERRORS, UNRECOVERABLE, UNSIGNED_INTEGER}; /* MCU detected a bus fault such as errors from instruction fetches, data access, and memory accesses, error data is the excepting instruction address */
static err_type McuUsageFaultInterrupt                             = {NO_CURRENT_TIME, 0x1004, ZERO_ERRORS, UNRECOVERABLE, UNSIGNED_INTEGER}; /* MCU detected a usage fault such as undefined instruction, invalid state, invalid interrupt return, divide by integer 0, error data is the excepting instruction address */
static err_type McuDebugMonInterrupt                               = {NO_CURRENT_TIME, 0x1005, ZERO_ERRORS, UNRECOVERABLE, UNSIGNED_INTEGER}; /* MCU detected a debug monitor fault, error data is the excepting instruction address */
static err_type McuSvcInterrupt                                    = {NO_CURRENT_TIME, 0x1006, ZERO_ERRORS, UNRECOVERABLE, UNSIGNED_INTEGER}; /* MCU detected a supervisor fault, error data is the excepting instruction address */
static err_type McuPendSvInterrupt                                 = {NO_CURRENT_TIME, 0x1007, ZERO_ERRORS, UNRECOVERABLE, UNSIGNED_INTEGER}; /* MCU detected a pending supervisor fault, error data is the excepting instruction address */
static err_type McuSysTickInterrupt                                = {NO_CURRENT_TIME, 0x1008, ZERO_ERRORS, UNRECOVERABLE, UNSIGNED_INTEGER}; /* MCU detected a system tick fault, error data is the excepting instruction address */
static err_type McuUnexpectedInterrupt                             = {NO_CURRENT_TIME, 0x1009, ZERO_ERRORS, WARNING, UNSIGNED_INTEGER};    /* MCU detected an interrupt that should not be used or enabled, error data is the interrupt number that fired */
static err_type McuFlashNbitInterrupt                              = {NO_CURRENT_TIME, 0x100A, ZERO_ERRORS, UNRECOVERABLE, UNSIGNED_INTEGER}; /* MCU detected a multi-bit ECC error in Program Flash, error data is the excepting instruction address */
static err_type McuFpuException                                    = {NO_CURRENT_TIME, 0x100B, ZERO_ERRORS, WARNING, UNSIGNED_INTEGER};    /* MCU detected a floating point exception, error data is the excepting instruction address */
static err_type McuSramEccNbitInterrupt                            = {NO_CURRENT_TIME, 0x100C, ZERO_ERRORS, UNRECOVERABLE, UNSIGNED_INTEGER}; /* MCU detected a multi-bit ECC error in SRAM, error data is the data address where it occurred */
static err_type McuPcuSramEcc1bitThresh                            = {NO_CURRENT_TIME, 0x100D, ZERO_ERRORS, WARNING, UNSIGNED_INTEGER};    /* PCU MCU detected a number of 1bit errors in SRAM that is beyond a particular threshold, recommend power cycling */
static err_type McuRftSramEcc1bitThresh                            = {NO_CURRENT_TIME, 0x100E, ZERO_ERRORS, WARNING, UNSIGNED_INTEGER};    /* RFT MCU detected a number of 1bit errors in SRAM that is beyond a particular threshold, recommend power cycling */
static err_type GladosErrorCritical                                = {NO_CURRENT_TIME, 0x1010, ZERO_ERRORS, UNRECOVERABLE, UNSIGNED_INTEGER}; /* GladOS critical error was detected */
static err_type GladosErrorNoncritical                             = {NO_CURRENT_TIME, 0x1011, ZERO_ERRORS, WARNING, UNSIGNED_INTEGER};    /* GladOS noncritical error was detected */
static err_type IntermediateTxQueueFull                            = {NO_CURRENT_TIME, 0x1012, ZERO_ERRORS, WARNING, UNSIGNED_INTEGER};    /* The intermediate queue for the TxAo is full. (TxAO 241) */
static err_type WatchdogTrip                                       = {NO_CURRENT_TIME, 0x1013, ZERO_ERRORS, WARNING, UNSIGNED_INTEGER};    /* The external watchdog indicates that it has reset the MCU, error data is the MCU RCM value */
static err_type McuUnexpectedReset                                 = {NO_CURRENT_TIME, 0x1014, ZERO_ERRORS, WARNING, UNSIGNED_INTEGER};    /* The Reset Control register reports an unexpected reset event. Expected events include: Normal POR (0x82) and Debug Reset (0x420). Error data is the MCU RCM value. */

static err_type RftPcuHeartbeatDropout                             = {NO_CURRENT_TIME, 0x1475, ZERO_ERRORS, SAFE, UNSIGNED_INTEGER};       /* Communication with the Prop has dropped out during a Thrust event and hindering FDIR capabilities */
static err_type RftDaqCommDropout                                  = {NO_CURRENT_TIME, 0x1476, ZERO_ERRORS, SAFE, UNSIGNED_INTEGER};       /* Communication with the ADCs has dropped and hindering FDIR capabilities */
const err_record Errors = {

        &StatusSuccess,
        &StatusFailure,
        &InvalidBootloaderError,
        &GeneralBootloaderError,
        &BootloaderFlashMaxSize,
        &InvalidBootloaderCrcValue,
        &InvalidBootAppType,
        &InvalidBootHardType,
        &InvalidBootProdType,
        &InvalidApp1FlashSize,
        &InvalidFlashApp1CrcValue,
        &InvalidFlashApp1AppType,
        &InvalidFlashApp1HardType,
        &InvalidFlashApp1ProdType,
        &InvalidApp2FlashSize,
        &InvalidFlashApp2CrcValue,
        &InvalidFlashApp2AppType,
        &InvalidFlashApp2HardType,
        &InvalidFlashApp2ProdType,
        &InvalidAppGoldFlashSize,
        &InvalidFlashGoldCrcValue,
        &InvalidFlashGoldAppType,
        &InvalidFlashGoldHardType,
        &InvalidFlashGoldProdType,
        &InvalidSramAppSize,
        &InvalidSramCrcValue,
        &InvalidSramAppType,
        &InvalidSramHardType,
        &InvalidSramProdType,
        &InvalidBootloaderPcCounter,
        &InvalidAppNumber,
        &InvalidAppSelection,
        &EndOfBootloaderMain,
        &EmbeddedTimeout,
        &NotEnoughBytes,
        &MalformedHeader,
        &SyncBytesError,
        &SourceAddressError,
        &TransmitMaskError,
        &PktDataLenError,
        &ComRxCrcError,
        &RftInternCommTimeout,
        &RftInternCommInvalidPkt,
        &CmdMsgIdNotFound,
        &CmdMsgIdStateNotSupported,
        &CmdMsgIdInvalidDataSize,
        &CmdInvalidMaxwellHardwareOffset,
        &CmdInvalidMaxRecordDataSize,
        &CmdIllformedCommaMaxRecordPacket,
        &CmdMaxWriteRecordOffsetFull,
        &CmdSwUpdateBelowMinDataAmount,
        &CmdProcessingPreviousCommand,
        &CmdProcessedCommandTimedout,
        &CmdInvalidStateTransition,
        &CmdCurrentStateIsBusy,
        &CmdTlmDumpInvalidMemChipSelected,
        &CmdTlmDumpInvalidBankSelected,
        &CmdSetHeaterCurrentOutOfBounds,
        &CmdSetHpisoCurrentOutOfBounds,
        &CmdSetLpisoCurrentOutOfBounds,
        &CmdInvalidAutoTlmRange,
        &CmdInvalidMcuDataDumpSize,
        &CmdInvalidHeaterPwmRawInvalidDutyCycle,
        &CmdInvalidHpisoPwmRawInvalidDutyCycle,
        &CmdInvalidLpisoPwmRawInvalidDutyCycle,
        &CmdInvalidMdotSetpt,
        &CmdInvalidRfVoltageSetpt,
        &CmdInvalidDurationSetpt,
        &CmdDataModeIsBusy,
        &CmdUpdateStageInvalidParam,
        &CmdUpdatePreloadInvalidParam,
        &CmdUpdateProgramInvalidParam,
        &CmdDataAbortDueToSafe,
        &CmdUpdateProgramStageInvalid,
        &CmdRejectedErrorActive,
        &CmdInvalidParameter,
        &CmdInvalidGetSoftwareInfoSelection,
        &CmdRftVirtualAddrMismatch,
        &CmdUpdateProgramApp1PtInUse,
        &CmdUpdateProgramApp2PtInUse,
        &CmdInvalidPropPhaseSuspect,
        &CmdInvalidHeaterCtrlSetpt,
        &CmdNoSoulToSell,
        &CmdWishNotGranted,
        &CmdBusySendingAutoTlm,
        &CmdInvalidPpVoltSetpointInvalid,
        &CmdInvalidPpRampToHigh,
        &MsgIdFunctionNotImplemented,
        &CurrentProcessedCommandTimedout,
        &UnexpectedTransition,
        &Ad568SpiSetFailed,
        &MdotSetPointOutOfBounds,
        &InvalidTxParameters,
        &IntermediateTxQueueHasNoData,
        &TxAoIsBusy,
        &TlmTmrTimeout,
        &InvalidTelemtryEventRequest,
        &RxScReciviedJunkData,
        &ReceivedMalformClankPacket,
        &ReceivedRxscTimeout,
        &MismatchCrc,
        &GseRxCircularBufferOverrun,
        &EvtCmdDumpTlmInvalidSize,
        &EvtDataOpTimeout,
        &Ltc6903SpiSetFailed,
        &InvalidAppSelectParamater,
        &CmdStateTransitionFail,
        &FlashEraseFailed,
        &FlashEraseFailedInvalidEraseAddress,
        &FlashEraseFailedInvalidSectorSize,
        &FlashEraseFailedInvalidSectorAddress,
        &FlashEraseFailedFlashStatusBusy,
        &FlashWriteFailed,
        &FlashWriteFailedInvalidEraseAddress,
        &FlashWriteFailedOverLimitWriteLength,
        &FlashWriteFailedFlashStatusBusy,
        &UsingDefaultPtInvalidAppSel,
        &UsingDefaultPtTypeMismatch,
        &UsingDefaultPtLengthMismatch,
        &UsingDefaultPtCrcMismatch,
        &UsingAppPt,
        &ThrustHpisoSpikeFail,
        &ThrustHpisoHoldFail,
        &ThrustLpisoSpikeFail,
        &ThrustLpisoHoldFail,
        &FlowRateInvalidCalculation,
        &HeaterCtrlInvalidSetpt,
        &TlmInvalidRequestData,
        &ErraoErrorFlashStorageFull,
        &ErraoCantDetermineCurrentFlashAddress,
        &ErraoCurrentFlashAddressOutOfBounds,
        &ErraoClrErrorsEvtNoData,
        &ErraoClrFlashFail,
        &ErraoEraseSectorFail,
        &PropInvInvalidPropCalculation,
        &Uart0RxError,
        &Uart0RxDmaError,
        &Uart1RxError,
        &Uart1RxDmaError,
        &Uart0TxDmaError,
        &Uart1TxDmaError,
        &TxaoTxTimeout,
        &TxaoInterqFull,
        &TxaoInterqNoData,
        &TxaoInvalidTxParams,
        &TxaoUartBusy,
        &DataaoFramInitFail,
        &DataaoNandInitFail,
        &DataaoFramWriteFail,
        &DataaoNandLastProgFail,
        &DataaoNandBufWriteFail,
        &DataaoNandEraseFail,
        &DataaoNandLoadFail,
        &DataaoNandEraseFullTimeout,
        &DataaoNandReadFail,
        &DataaoFramReadFail,
        &DataaoFramMetadataWriteFail,
        &DataaoUpProgStagedCrcFail,
        &DataaoUpProgStagedCrcTimeout,
        &DataaoUpProgFlashTimeout,
        &DataaoUpProgFlashFail,
        &DataaoUpProgFinalCrcFail,
        &SpiAdcDmaRxOverrun,
        &SpiAdcDmaTxUnderrun,
        &SpiAdcDmaError,
        &PwmDriverInitFail,
        &WfOscInitFail,
        &RftaoInterqFull,
        &RftaoInterqNoData,
        &RftaoUartBusy,
        &RftaoXferTimeout,
        &RftaoXferInvalidPkt,
        &PpvsetPwmRawInvalid,
        &PpvsetPwmVInvalid,
        &McuNmiInterrupt,
        &McuHardFaultInterrupt,
        &McuMemManageInterrupt,
        &McuBusFaultInterrupt,
        &McuUsageFaultInterrupt,
        &McuDebugMonInterrupt,
        &McuSvcInterrupt,
        &McuPendSvInterrupt,
        &McuSysTickInterrupt,
        &McuUnexpectedInterrupt,
        &McuFlashNbitInterrupt,
        &McuFpuException,
        &McuSramEccNbitInterrupt,
        &McuPcuSramEcc1bitThresh,
        &McuRftSramEcc1bitThresh,
        &GladosErrorCritical,
        &GladosErrorNoncritical,
        &IntermediateTxQueueFull,
        &WatchdogTrip,
        &McuUnexpectedReset,
        &RftPcuHeartbeatDropout,
        &RftDaqCommDropout
};

/* Generic used for initializing a success type */
const Cmd_Err_Code_t ERROR_NONE = {(err_type*) &StatusSuccess, 0x0000U};

Cmd_Err_Code_t create_error_uint(const err_type *error, uint32_t aux_data)
{
    Cmd_Err_Code_t error_output;
    error_output.aux_data = aux_data;
    error_output.reported_error = (err_type *)error;
    return error_output;
}
Cmd_Err_Code_t create_error_float(const err_type *error, float aux_data)
{
    Cmd_Err_Code_t error_output;
    error_output.aux_data = *(uint32_t *)&aux_data;
    error_output.reported_error = (err_type *)error;
    return error_output;
}


#endif /* SOURCE_P4_ERR_C_ */
