/* Copyright (c) 2017-2020 by Phase Four, Inc. All Rights Reserved. Confidential and Proprietary.
 *
 * rft_data.h
 */

#ifndef SOURCE_RFT_DATA_H_
#define SOURCE_RFT_DATA_H_

#include "mcu_types.h"


typedef enum
{
    MAX_LOW_PWR   = 0x1,
    MAX_IDLE      = 0x2,
    MAX_THRUST    = 0x4,
    MAX_MANUAL    = 0x7,
    MAX_SW_UPDATE = 0x8,
    MAX_VENT_SAFE = 0xB,
    MAX_WILD_WEST = 0xD,
    MAX_SAFE      = 0xF
} MaxState_t;


typedef enum
{
    DATA_LOUNGE         = 0x0,
    DATA_RECORD         = 0x3,
    DATA_ERASE_NAND     = 0x5,
    DATA_DUMP_MCU       = 0x6,
    DATA_DUMP_TLM       = 0x9,
    DATA_UP_CMD_FAIL    = 0xA,
    DATA_UP_CMD         = 0xC,
    DATA_UP_FLSH_CLR    = 0xD,
    DATA_UP_FLSH_PRG    = 0xE,
    DATA_UP_CMD_SUCCESS = 0xF,
} DataMode_t;


typedef struct RftData_tag
{
    /* Holds the current unix time, 0 at reset until new unix time is received */
    uint32_t unix_time_us_hi;
    uint32_t unix_time_us_lo;

    /* Holds the timestamp of the last received time sync value,
     * used to calculate the current unix time since the last commanded value */
    uint32_t time_sync_lpit_hi;           /* 0xFFFFFFFF at startup */
    uint32_t time_sync_lpit_lo;           /* 0xFFFFFFFF at startup */
    uint32_t last_unix_time_sync_us_hi;
    uint32_t last_unix_time_sync_us_lo;

    MaxState_t state;
    uint16_t cmd_acpt_cnt;
    uint16_t cmd_rjct_cnt;

    uint8_t  bl_select;
    uint8_t  app_select;
    uint8_t  pt_select;

    uint32_t bl_crc;
    uint32_t app_crc;
    uint32_t pt_crc;

    uint32_t rcm_value;
    bool     wdt_rst_trig;
    bool     wdt_tog_sel;
    uint8_t  fsw_cpu_util;

    DataMode_t data_mode;
    uint8_t next_app_select;



    /* External ADC 0 Signals */
    float tfet;
    float tchoke;
    float tcoil;
    float tic;
    float tpl;
    float tmatch;
    float ehvd;
    float ihvd;
    float tadc0;

    /* Internal ADC Signals */
    float ebus;
    float e12v;
    float e5v;
    float evb;
    float ibus;
    float i12v;
    float ivb;
    float tfb_in;
    float tfb_out;
    float tfram;
    float tmcu;

    uint16_t pwm_pp_vset_raw_fb;
    float pp_vset_fb;
    float wav_gen_freq_fb;

    uint32_t rft_ecc_sram_1bit_cnt;
    uint32_t rx_crc_fail_cnt;
    bool needs_reset_flag;
	
    bool gpio_wdi_fb;
    bool gpio_wdt_rst_nclr_fb;
    bool gpio_hum_temp_sel_fb;
    bool gpio_wf_en_fb;
    bool gpio_pp_en_fb;

    bool busy;

    bool using_runtime_pt;
    bool using_default_pt;
    bool using_app_pt;
    bool inverter_connected_flag;
    bool rft_connected_flag;
    bool hp_connected_flag;
    bool prop_connected_flag;
    uint32_t prop_comm_connection_status;

    bool startup_complete;

    uint32_t daq_consec_err_cnt;
    uint32_t daq_consec_timeout_cnt;


} RftData_t;

extern RftData_t RftData;


void thr_data_init(void);
void thr_data_unix_time_update(uint32_t curr_tick_upper, uint32_t curr_tick_lower);
uint64_t get_unix_timestamp_micro(void);


#endif /* SOURCE_RFT_DATA_H_ */
