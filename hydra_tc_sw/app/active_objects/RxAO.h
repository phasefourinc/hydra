/* Copyright (c) 2017-2020 by Phase Four, Inc. All Rights Reserved. Confidential and Proprietary.
 *
 * RxAO.h
 */

#ifndef ACTIVE_OBJECTS_RXAO_H_
#define ACTIVE_OBJECTS_RXAO_H_

/* Global invariants:
 *  - No constants that might affect reception of data should be placed in the PT, lest run
 *    the risk of having a valid PT loaded and then the user is unable to communicate.
 */

#include "mcu_types.h"
#include "glados.h"
#include "uart_ring.h"

#define RXAO_PRIORITY        10UL
#define RXAO_FIFO_SIZE       6UL

#define PROP_CONTROLLER_SOURCE_ADDRESS 0UL

/* Each of these must be unique values for the entire system */
enum RxAO_event_names
{
    EVT_TMR_RX_TIMEOUT = (RXAO_PRIORITY << 16U),
    EVT_TMR_RX_CMD_TIMEOUT,
    EVT_UART0_RX_DATA,
    EVT_RX_REINIT,
    EVT_RX_START_RX,
    EVT_RX_CMD_RCVD,
    EVT_TMR_RX_DROPOUT_DELAY
};

extern GLADOS_AO_t AO_Rx;
extern GLADOS_Timer_t Tmr_RxTimeout;
extern GLADOS_Timer_t Tmr_RxCmdTimeout;
extern GLADOS_Timer_t Tmr_RxDropoutDelay;

void RxAO_init(void);
void RxAO_push_interrupts(void);
void RxAO_state(GLADOS_AO_t *this_ao, GLADOS_Event_t *event);
void RxAO_state_rx_reinit(GLADOS_AO_t *this_ao, GLADOS_Event_t *event);
void RxAO_state_rx_start(GLADOS_AO_t *this_ao, GLADOS_Event_t *event);
void RxAO_state_rx_data(GLADOS_AO_t *this_ao, GLADOS_Event_t *event);
void RxAO_state_rx_data_payload(GLADOS_AO_t *this_ao, GLADOS_Event_t *event);
void RxAO_state_rx_cmd_ack(GLADOS_AO_t *this_ao, GLADOS_Event_t *event);


#endif /* ACTIVE_OBJECTS_RXAO_H_ */
