/* Copyright (c) 2017-2020 by Phase Four, Inc. All Rights Reserved. Confidential and Proprietary.
 *
 * DataAO.h
 */

#ifndef ACTIVE_OBJECTS_DATAAO_H_
#define ACTIVE_OBJECTS_DATAAO_H_

/*
 * Global invariants:
 *  - The CRC32 algorithm can validate a 128KB buffer within 10ms
 */

#include "mcu_types.h"
#include "glados.h"

#define DATAAO_MEMTYPE_FRAM    0xAU
#define DATAAO_MEMTYPE_NAND    0xBU

#define DATAAO_PRIORITY        8UL
#define DATAAO_FIFO_SIZE       6UL

extern GLADOS_AO_t AO_Data;
extern GLADOS_Timer_t Tmr_DataAux;
extern GLADOS_Timer_t Tmr_DataTimeout;
extern bool spi0_callback_send_dma_done;
extern bool spi1_callback_send_dma_done;


/*Currently the events the watchdog timer has is pet the watchdog and stop the watchdog.*/
enum DataAO_event_names
{
    EVT_DATA_OP_DONE = (DATAAO_PRIORITY << 16U),
    EVT_DATA_DMA_FRAM_DONE,
    EVT_TMR_DATA_AUX_DONE,
    EVT_TMR_DATA_TIMEOUT,
    EVT_DATA_CRC_ERROR,
};

typedef struct
{
    uint32_t pflash_addr;
    uint32_t length;
    uint32_t fram_offset;
} BYTE_PACKED DataAO_PreloadData_t;

typedef struct
{
    uint32_t fram_offset;
    uint32_t data_ptr;
    uint32_t length;
} BYTE_PACKED DataAO_StageData_t;


typedef struct
{
    uint32_t pflash_addr;
    uint32_t length;
    uint32_t crc;
} BYTE_PACKED DataAO_ProgData_t;


void DataAO_init(void);
void DataAO_push_interrupts(void);
void DataAO_state(GLADOS_AO_t *this_ao, GLADOS_Event_t *event);
void DataAO_state_lounge(GLADOS_AO_t *this_ao, GLADOS_Event_t *event);
void DataAO_state_update_cmd(GLADOS_AO_t *this_ao, GLADOS_Event_t *event);
void DataAO_state_update_cmd_success(GLADOS_AO_t *this_ao, GLADOS_Event_t *event);
void DataAO_state_update_cmd_fail(GLADOS_AO_t *this_ao, GLADOS_Event_t *event);
void DataAO_state_preload(GLADOS_AO_t *this_ao, GLADOS_Event_t *event);
void DataAO_state_stage(GLADOS_AO_t *this_ao, GLADOS_Event_t *event);
void DataAO_state_fram_validate(GLADOS_AO_t *this_ao, GLADOS_Event_t *event);
void DataAO_state_pflash_erase(GLADOS_AO_t *this_ao, GLADOS_Event_t *event);
void DataAO_state_pflash_program(GLADOS_AO_t *this_ao, GLADOS_Event_t *event);
void DataAO_state_pflash_validate(GLADOS_AO_t *this_ao, GLADOS_Event_t *event);

#endif /* ACTIVE_OBJECTS_DATAAO_H_ */
