/* Copyright (c) 2017-2020 by Phase Four, Inc. All Rights Reserved. Confidential and Proprietary.
 *
 * MaxwellAO.h
 */

#ifndef ACTIVE_OBJECTS_MAXWELLAO_H_
#define ACTIVE_OBJECTS_MAXWELLAO_H_

#include "mcu_types.h"
#include "rft_data.h"
#include "glados.h"

#define MAXAO_PRIORITY        1UL
#define MAXAO_FIFO_SIZE       6UL

/* Each of these must be unique values for the entire system */
enum MaxwellAO_event_names
{
    EVT_MAX_STATE_IN___BASE    = (MAXAO_PRIORITY << 16U),
    EVT_MAX_STATE_IN_LOW_PWR   = EVT_MAX_STATE_IN___BASE + MAX_LOW_PWR,
    EVT_MAX_STATE_IN_IDLE      = EVT_MAX_STATE_IN___BASE + MAX_IDLE,
    EVT_MAX_STATE_IN_THRUST    = EVT_MAX_STATE_IN___BASE + MAX_THRUST,
    EVT_MAX_STATE_IN_SW_UPDATE = EVT_MAX_STATE_IN___BASE + MAX_SW_UPDATE,
    EVT_MAX_STATE_IN_MANUAL    = EVT_MAX_STATE_IN___BASE + MAX_MANUAL,
    EVT_MAX_STATE_IN_WILD_WEST = EVT_MAX_STATE_IN___BASE + MAX_WILD_WEST,
    EVT_MAX_STATE_IN_SAFE      = EVT_MAX_STATE_IN___BASE + MAX_SAFE,
    EVT_TMR_NEXT_SAFE_SEQ     = ((MAXAO_PRIORITY << 16U) + 256U),
    EVT_MAX_STATE_INVALID,
    EVT_MAX_STATE_BUSY,
    EVT_MAX_DATA_ABORT,
    EVT_MAX_START_RECORDING,
    EVT_MAX_FRAM_REC_ENBL,
    EVT_MAX_FRAM_REC_DSBL,
    EVT_MAX_NAND_REC_ENBL,
    EVT_MAX_NAND_REC_DSBL,
};

extern GLADOS_AO_t AO_Maxwell;
extern GLADOS_Timer_t Tmr_MaxSafeSeq;

void MaxAO_init(void);
void MaxAO_state(GLADOS_AO_t *this_ao, GLADOS_Event_t *event);
void MaxAO_state_safe(GLADOS_AO_t *this_ao, GLADOS_Event_t *event);
void MaxAO_state_sw_update(GLADOS_AO_t *this_ao, GLADOS_Event_t *event);
void MaxAO_state_manual(GLADOS_AO_t *this_ao, GLADOS_Event_t *event);
void MaxAO_state_wild_west(GLADOS_AO_t *this_ao, GLADOS_Event_t *event);

void MaxAO_hard_safe(void);
#endif /* ACTIVE_OBJECTS_MAXWELLAO_H_ */
