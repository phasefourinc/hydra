/* Copyright (c) 2017-2020 by Phase Four, Inc. All Rights Reserved. Confidential and Proprietary.
 *
 * MaxwellAO.c
 */

#include "MaxwellAO.h"
#include "mcu_types.h"
#include "glados.h"
#include "ErrAO.h"
#include "CmdAO.h"
#include "DaqCtlAO.h"
#include "DataAO.h"
#include "rft_data.h"
#include "gpio.h"
#include "default_pt.h"

GLADOS_AO_t AO_Maxwell;
GLADOS_Timer_t Tmr_MaxSafeSeq;

/* Active Object public interface */


/* Active Object private variables */
static GLADOS_Event_t max_fifo[MAXAO_FIFO_SIZE];


/* Active Object state definitions */

void MaxAO_init(void)
{
    /* Initialize the Active Object with both its Event FIFO and initial state function */
    GLADOS_AO_Init(&AO_Maxwell, MAXAO_PRIORITY, max_fifo, MAXAO_FIFO_SIZE, &MaxAO_state);

    return;
}


void MaxAO_state(GLADOS_AO_t *this_ao, GLADOS_Event_t *event)
{
    DEV_ASSERT(this_ao);
    DEV_ASSERT(event);

    switch(event->name)
    {
        /* GLADOS_ENTRY/GLADOS_EXIT optional event for each state */
        case GLADOS_ENTRY:
            /* Configure the abort sequence timer to 40ms for each step */
            GLADOS_Timer_Init(&Tmr_MaxSafeSeq, PT->max_safe_seq_delay_us, EVT_TMR_NEXT_SAFE_SEQ);
            GLADOS_Timer_Subscribe_AO(&Tmr_MaxSafeSeq, this_ao);

            /* Initialize the system into a SAFE state */
            GLADOS_STATE_TRAN_TO_CHILD(&MaxAO_state_safe);
            break;

        case GLADOS_EXIT:
            break;

        case EVT_CMD_MAX_TO_THRUST:
        case EVT_CMD_MAX_TO_MANUAL:
        case EVT_CMD_MAX_TO_WILD_WEST:
        case EVT_CMD_MAX_TO_SW_UPDATE:
            /* Signal to CmdAO that the requested state transition is invalid */
            GLADOS_AO_PUSH(&AO_Cmd, EVT_MAX_STATE_INVALID);
            break;

        case EVT_ERR_MAX_TO_SAFE:
        case EVT_CMD_MAX_TO_SAFE:
            GLADOS_STATE_TRAN_TO_CHILD(&MaxAO_state_safe);
            break;

        default:
            /* Top level state, skip undefined Events */
            break;
    }

    return;
}


void MaxAO_state_safe(GLADOS_AO_t *this_ao, GLADOS_Event_t *event)
{
    DEV_ASSERT(this_ao);
    DEV_ASSERT(event);

    static uint8_t safe_seq_cnt = 0U;

    switch(event->name)
    {
        /* GLADOS_ENTRY/GLADOS_EXIT optional event for each state */
        case GLADOS_ENTRY:
            safe_seq_cnt = 0U;
            RftData.state = MAX_SAFE;
            GLADOS_AO_PUSH(&AO_Cmd, EVT_MAX_STATE_IN_SAFE);

            /* Immediately enter into the first step */
            GLADOS_AO_PUSH(this_ao, EVT_TMR_NEXT_SAFE_SEQ);

            /* Kick off the continuous to sequence the steps */
            GLADOS_Timer_Set(&Tmr_MaxSafeSeq, PT->max_safe_seq_delay_us);
            GLADOS_Timer_EnableContinuous(&Tmr_MaxSafeSeq);
            break;

        case GLADOS_EXIT:
            GLADOS_Timer_Disable(&Tmr_MaxSafeSeq);
            break;

        case EVT_TMR_NEXT_SAFE_SEQ:
            /* Increment to the next sequence */
            ++safe_seq_cnt;

            switch(safe_seq_cnt)
            {
                case 1U:
                default:
                    /* The first step in the SAFE sequence.
                     * Note: Use this case as default to catch unexpectedly
                     * large counts. If it is large, restart the sequence. */
                    DEV_ASSERT(safe_seq_cnt == 1U);
                    safe_seq_cnt = 1U;

                    GPIO_SetOutput(GPIO_PP_EN, false);
                    RftData.gpio_pp_en_fb = false;

                    /* At this point, the inverter should no longer be radiating */
                    break;

                case 2U:
                    GPIO_SetOutput(GPIO_WF_EN, false);
                    RftData.gpio_wf_en_fb = false;
                    break;

                case 3U:
                    /* Set the Push-Pull Vset PWM to 0% duty cycle, always succeeds */
                    (void)DaqCtlAO_PpVset_SetPwmRaw(0U, true); /* Stopping ramp because we don't want to ramp to continue when we are safing.*/
                    RftData.pp_vset_fb = 0.0f;

                    /* Abort any data dumping, SW loading or updating in progress */
                    GLADOS_AO_PUSH(&AO_Data, EVT_MAX_DATA_ABORT);

                    /* Transfer Errors to Flash via ErrAO */
                    GLADOS_AO_PushEvtName(&AO_Maxwell, &AO_Err, EVT_TRANSFER_TO_NVM);

                    /* Transition out of SAFE */
                    GLADOS_STATE_TRAN_TO_SIBLING(&MaxAO_state_manual);
                    break;
            }
            break;

        case EVT_CMD_MAX_TO_SAFE:
            /* Already safe-ing, signal this to CmdAO */
            GLADOS_AO_PUSH(&AO_Cmd, EVT_MAX_STATE_IN_SAFE);
            break;

        case EVT_ERR_MAX_TO_SAFE:
            /* Ignore Errors signaling to go to safe since already safe-ing */
            break;

        default:
            GLADOS_STATE_CHECK_PARENT(&MaxAO_state);
            break;
    }

    return;
}


void MaxAO_state_sw_update(GLADOS_AO_t *this_ao, GLADOS_Event_t *event)
{
    DEV_ASSERT(this_ao);
    DEV_ASSERT(event);

    switch(event->name)
    {
        /* GLADOS_ENTRY/GLADOS_EXIT optional event for each state */
        case GLADOS_ENTRY:
            RftData.state = MAX_SW_UPDATE;
            GLADOS_AO_PUSH(&AO_Cmd, EVT_MAX_STATE_IN_SW_UPDATE);
            GLADOS_AO_PUSH(&AO_Data, EVT_MAX_STATE_IN_SW_UPDATE);
            break;

        case GLADOS_EXIT:
            break;

        case EVT_CMD_MAX_TO_SW_UPDATE:
            GLADOS_AO_PUSH(&AO_Cmd, EVT_MAX_STATE_IN_SW_UPDATE);
            break;

        default:
            GLADOS_STATE_CHECK_PARENT(&MaxAO_state);
            break;
    }

    return;
}


void MaxAO_state_manual(GLADOS_AO_t *this_ao, GLADOS_Event_t *event)
{
    DEV_ASSERT(this_ao);
    DEV_ASSERT(event);

    switch(event->name)
    {
        /* GLADOS_ENTRY/GLADOS_EXIT optional event for each state */
        case GLADOS_ENTRY:
            RftData.state = MAX_MANUAL;
            break;

        case GLADOS_EXIT:
            break;

        case EVT_CMD_MAX_TO_MANUAL:
            GLADOS_AO_PUSH(&AO_Cmd, EVT_MAX_STATE_IN_MANUAL);
            break;

        case EVT_CMD_MAX_TO_WILD_WEST:
            GLADOS_STATE_TRAN_TO_SIBLING(&MaxAO_state_wild_west);
            break;

        case EVT_CMD_MAX_TO_SW_UPDATE:
            /* Disable everything before entering SW_UPDATE */
            (void)DaqCtlAO_PpVset_SetPwmRaw(0U, true);
            GPIO_SetOutput(GPIO_PP_EN, false);
            RftData.gpio_pp_en_fb = false;
            GPIO_SetOutput(GPIO_WF_EN, false);
            RftData.gpio_wf_en_fb = false;

            GLADOS_STATE_TRAN_TO_SIBLING(&MaxAO_state_sw_update);
            break;

        default:
            GLADOS_STATE_CHECK_PARENT(&MaxAO_state);
            break;
    }

    return;
}


void MaxAO_state_wild_west(GLADOS_AO_t *this_ao, GLADOS_Event_t *event)
{
    DEV_ASSERT(this_ao);
    DEV_ASSERT(event);

    switch(event->name)
    {
        /* GLADOS_ENTRY/GLADOS_EXIT optional event for each state */
        case GLADOS_ENTRY:
            RftData.state = MAX_WILD_WEST;
            GLADOS_AO_PUSH(&AO_Cmd, EVT_MAX_STATE_IN_WILD_WEST);
            break;

        case GLADOS_EXIT:
            /* FDIR uses rft_state to re-enable FDIR checking */
            break;

        case EVT_CMD_MAX_TO_WILD_WEST:
            GLADOS_AO_PUSH(&AO_Cmd, EVT_MAX_STATE_IN_WILD_WEST);
            break;

        case EVT_CMD_MAX_TO_MANUAL:
            GLADOS_STATE_TRAN_TO_SIBLING(&MaxAO_state_manual);

            /* Since SAFEing gets spit right into MANUAL mode for the Thr Ctrl,
             * we can't have the MANUAL state sending anything to CmdAO indicating
             * it's state. To avoid this race condition, we let the CmdAO know
             * just prior to entering the state. */
            GLADOS_AO_PUSH(&AO_Cmd, EVT_MAX_STATE_IN_MANUAL);
            break;

        default:
            GLADOS_STATE_CHECK_PARENT(&MaxAO_state);
            break;
    }

    return;
}


void MaxAO_hard_safe(void)
{
    /* Set the Push-Pull Vset PWM to 0% duty cycle, always succeeds */
    (void)DaqCtlAO_PpVset_SetPwmRaw(0U, true); /* Stopping ramp because we don't want to ramp to continue when we are safing.*/

    GPIO_SetOutput(GPIO_PP_EN, false);
    RftData.gpio_pp_en_fb = false;

    timer32_delay_ms(50UL);

    GPIO_SetOutput(GPIO_WF_EN, false);
    RftData.gpio_wf_en_fb = false;

    return;
}

