/* Copyright (c) 2017-2020 by Phase Four, Inc. All Rights Reserved. Confidential and Proprietary.
 *
 * CmdAO.c
 */

/* Global invariants:
 *  - All commands in the command table are sorted from smallest number ID to largest
 *  - No constants that might affect reception of data (e.g. CMD timeout) should be
 *    placed in the PT, lest run the risk of having a valid PT loaded and then the
 *    user is unable to communicate.
 */

#include "CmdAO.h"
#include "mcu_types.h"
#include "glados.h"
#include "ErrAO.h"
#include "TxAO.h"
#include "RxAO.h"
#include "TlmAO.h"
#include "WdtAO.h"
#include "p4_cmd.h"
#include "MaxwellAO.h"
#include "DaqCtlAO.h"
#include "DataAO.h"
#include "gpio.h"
#include "rft_data.h"
#include "p4_err.h"
#include "max_info_driver.h"
#include "ltc6903.h"
#include "pwm_driver.h"
#include "max_defs.h"
#include "default_pt.h"
#include "crc.h"
#include "fidr.h"
#include "math.h"
#include "string.h"




/* Active Object public interface */

GLADOS_AO_t AO_Cmd;

/* Active Object timers */
GLADOS_Timer_t Tmr_CmdTimeout;
GLADOS_Timer_t Tmr_CmdCustomAppTimer;

/* Active Object private variables */
static GLADOS_Event_t cmd_fifo[CMDAO_FIFO_SIZE];
static GLADOS_Event_t evt_cmd_rqst_complete;
static CmdAO_Data_t cmd_data;
static P4Cmd_Id_t cmd_id;

static int32_t get_cmd_table_idx(uint16_t command_id);


/* IMPORTANT: Command Table must be sorted from lowest Command ID number to highest
 * NOTE: Commands with a NULL handle function are custom commands handled uniquely */
static const CmdMgr_Cmd_Table_t Cmd_Table[] =
{
    /* Cmd ID,         Params Size, Rqst Cmd, Valid States,                 Handle Function Pointer */
    { GET_STATUS,               0U,  false,     ALL_STATES,                 NULL },
    { SET_UTIME,                8U,  false,     ALL_STATES,                 &cmd_hdl_set_utime },
    { CLR_ERRORS,               0U,  false,     MAN_WW_STATES,              &cmd_hdl_clr_errors },
    { GET_SW_INFO,              1U,  true,      MAN_WW_STATES,              &cmd_hdl_get_sw_info },
    { SET_NEXT_APP,             1U,  true,      ALL_STATES,                 &cmd_hdl_set_next_app },
    { ABORT,                    0U,  false,     ALL_STATES,                 NULL },


    { WRT_ERR,                  3U,   false,    MAN_WW_STATES,              &cmd_hdl_wrt_err },
    { WRT_FLSH,                 0U,   false,    MAN_WW_STATES,              &cmd_hdl_prg_flsh },
    { TRN_SRAM_FLASH,           0U,   false,    MAN_WW_STATES,              &cmd_hdl_trnsf_flash },
    { DFT_SRAM_ECC_1BIT,        1U,   false,    MAN_WW_STATES,              &cmd_hdl_dft_sram_ecc_1bit },
    { DFT_PT_SELL_YOUR_SOUL,    0U,   false,    WILD_WEST_STATE,            &cmd_hdl_dft_pt_sell_your_soul },
    { DFT_PT_SET,               8U,   false,    WILD_WEST_STATE,            &cmd_hdl_dft_pt_set },

    { SET_WDI_EN,               1U,  false,     MAN_WW_STATES,              &cmd_hdl_set_wdi_en },

    { RFT_SET_DEBUG_LED0,       1U,  false,     MAN_WW_STATES,              &cmd_hdl_set_debug_led0_en },
    { RFT_SET_WDT_RST_NCLR_EN,  1U,  false,     MAN_WW_STATES,              &cmd_hdl_set_wdt_rst_nclr_en },
    { RFT_SET_HUM_TEMP_SEL_EN,  1U,  false,     MAN_WW_STATES,              &cmd_hdl_set_hum_temp_sel_en },
    { RFT_SET_WF_EN,            1U,  false,     MAN_WW_STATES,              &cmd_hdl_set_wf_en },
    { RFT_SET_PP_EN,            1U,  false,     MAN_WW_STATES,              &cmd_hdl_set_pp_en },
    { RFT_SET_PP_VSET_PWM_RAW,  2U,  false,     MAN_WW_STATES,              &cmd_hdl_pp_vset_pwm_raw },
    { RFT_SET_WAV_GEN_FREQ,     4U,  false,     MAN_WW_STATES,              &cmd_hdl_wav_gen_freq },
    { RFT_SET_PP_VSET,          4U,  false,     MAN_WW_STATES,              &cmd_hdl_pp_vset },
    { RFT_RAMP_PUSH_PULL,       8U,  false,     MAN_WW_STATES,              &cmd_hdl_pp_vset_ramp },

    { SET_MAX_HRD_INFO,         129U, false,    MAN_WW_STATES,              &cmd_hdl_write_max_hdr_info },
    { ERASE_MAX_HRD_INFO,       0U,   false,    MAN_WW_STATES,              &cmd_hdl_erase_max_hdr_info },

    { MANUAL,                   0U,  false,     ALL_STATES,                 NULL },
    { SW_UPDATE,                0U,  false,     ALL_STATES,                 NULL },
    { WILD_WEST,                0U,  false,     ALL_STATES,                 NULL },
    { SW_UP_PRELOAD,            12U, true,      SW_UPDATE_STATE,            &cmd_hdl_sw_up_preload },
    { SW_UP_STAGE,              4U,  true,      SW_UPDATE_STATE,            NULL },
    { SW_UP_PROGRAM,            12U, true,      SW_UPDATE_STATE,            &cmd_hdl_sw_up_program },
    { GET_TLM,                  0U,  true,      ALL_STATES,                 &cmd_hdl_get_tlm },
    { SET_AUTO_TLM,             5U,  false,     ALL_STATES,                 &cmd_hdl_set_auto_tlm },
    { ABORT_DATA_OP,            0U,  false,     MAN_WW_UP_STATES,           &cmd_hdl_abort_data_op },
    { CLEAR_ERRORS_ADVANCED,    1U,  false,     MAN_WW_STATES,              &cmd_hdl_clr_errors_advanced },
    { DUMP_MCU,                 5U,  true,      MAN_WW_STATES,              &cmd_hdl_dump_mcu },
    { STOP_WDT,                 0U,  false,     MAN_WW_STATES,              &cmd_hdl_stp_wtd },
    { GET_MAX_HRD_INFO,         1U,  true,      MAN_WW_STATES,              &cmd_hdl_read_max_hdr_info },
};


/* Active Object state definitions */

void CmdAO_init(void)
{
    /* Initialize the Active Object with both its Event FIFO and initial state function */
    GLADOS_AO_Init(&AO_Cmd, CMDAO_PRIORITY, cmd_fifo, CMDAO_FIFO_SIZE, &CmdAO_state);

    return;
}

void CmdAO_state(GLADOS_AO_t *this_ao, GLADOS_Event_t *event)
{
    DEV_ASSERT(this_ao);
    DEV_ASSERT(event);

    switch(event->name)
    {
        /* GLADOS_ENTRY/GLADOS_EXIT optional event for each state */
        case GLADOS_ENTRY:
            /* Configure timeout for receiving the message to 5ms. Selected based
             * on max 265B transfer at 1Mbps rate */
            GLADOS_Timer_Init(&Tmr_CmdTimeout, MAX_CMD_TIMEOUT_US, EVT_TMR_CMD_TIMEOUT);
            GLADOS_Timer_Subscribe_AO(&Tmr_CmdTimeout, this_ao);

            /* This timer value is expected to be overwritten by the feature using it */
            GLADOS_Timer_Init(&Tmr_CmdCustomAppTimer, 1000000UL, EVT_TMR_CMD_TIMEOUT);
            GLADOS_Timer_Subscribe_AO(&Tmr_CmdCustomAppTimer, this_ao);
            /* Create a self event used as a consistent way to signal the
             * restarting of the Rx process */
            GLADOS_Event_New(&evt_cmd_rqst_complete, EVT_CMD_RQST_COMPLETE);

            GLADOS_STATE_TRAN_TO_CHILD(&CmdAO_state_cmd_process);
            break;

        case GLADOS_EXIT:
            break;

        case EVT_MAX_STATE_IN_WILD_WEST:
        case EVT_MAX_STATE_IN_SW_UPDATE:
            report_error_uint(&AO_Cmd, Errors.UnexpectedTransition , DO_NOT_SEND_TO_SPACECRAFT, 0x0000);
            break;

        case EVT_MAX_STATE_IN_SAFE:
        case EVT_MAX_STATE_IN_LOW_PWR:
            /* Ignore state changes that occur due to FDIR or critical errors */
            break;

        default:
            /* Top level state, skip undefined Events */
            break;
    }

    return;
}


void CmdAO_state_cmd_process(GLADOS_AO_t *this_ao, GLADOS_Event_t *event)
{
    DEV_ASSERT(this_ao);
    DEV_ASSERT(event);

    int32_t cmd_idx;
    Cmd_Err_Code_t error_code;
    bool cmd_ackd = false;

    switch(event->name)
    {
        /* GLADOS_ENTRY/GLADOS_EXIT optional event for each state */
        case GLADOS_ENTRY:
            break;

        case GLADOS_EXIT:
            break;

        case EVT_RX_CMD_RCVD:
            DEV_ASSERT(event->has_data);
            DEV_ASSERT(event->data_size == sizeof(CmdAO_Data_t));

            /* Store a copy of the command data for all internal states to use.
             * Note: This pointer relies on CmdAO being of a higher priority
             * than the Rx AOs in order to preserve this data until the next
             * command received is pushed by an Rx AO. */
            cmd_data = *(CmdAO_Data_t *)&event->data[0];

            cmd_idx = get_cmd_table_idx(cmd_data.msg_id);

            /* Check if command exists */
            if (cmd_idx < 0)
            {
                cmd_ackd = true;
                RftData.cmd_rjct_cnt++;
                report_error_uint(&AO_Cmd, Errors.CmdMsgIdNotFound , DO_NOT_SEND_TO_SPACECRAFT, cmd_data.msg_id);
                TxAO_SendMsg_Ack(this_ao, (TxPort_t)cmd_data.port,(uint8_t)cmd_data.src_id, cmd_data.seq_id, cmd_data.msg_id, Errors.CmdMsgIdNotFound->error_id);
            }
            /* Check if command is defined for the current state */
            else if (0U == (Cmd_Table[cmd_idx].valid_states & (1U << RftData.state)))
            {
                cmd_ackd = true;
                RftData.cmd_rjct_cnt++;
                report_error_uint(&AO_Cmd, Errors.CmdMsgIdStateNotSupported , DO_NOT_SEND_TO_SPACECRAFT, RftData.state);
                TxAO_SendMsg_Ack(this_ao, (TxPort_t)cmd_data.port, (uint8_t)cmd_data.src_id, cmd_data.seq_id, cmd_data.msg_id, Errors.CmdMsgIdStateNotSupported->error_id);
            }
            /* Handle commands with defined function handles */
            else if (Cmd_Table[cmd_idx].cmd_func_ptr != NULL)
            {
                /* Check if all parameters exist. This is checked here rather than one level up since
                 * the parameter check does not work for custom commands that have sent variable data. */
                if (Cmd_Table[cmd_idx].params_size != cmd_data.data_size)
                {
                    cmd_ackd = true;
                    RftData.cmd_rjct_cnt++;
                    report_error_uint(&AO_Cmd, Errors.CmdMsgIdInvalidDataSize , DO_NOT_SEND_TO_SPACECRAFT, Cmd_Table[cmd_idx].params_size);
                    TxAO_SendMsg_Ack(this_ao, (TxPort_t)cmd_data.port,(uint8_t)cmd_data.src_id, cmd_data.seq_id, cmd_data.msg_id, Errors.CmdMsgIdInvalidDataSize->error_id);
                }
                else
                {
                    /* Call the function and pass in the command arguments */
                    error_code = Cmd_Table[cmd_idx].cmd_func_ptr(&cmd_data.data_ptr[0]);

                    /* If a request command to another AO, then transition to the CmdAO request
                     * state and await a response from the appropriate AO. No Ack is returned
                     * to the sender until the appropriate AO responds. */
                    if ((error_code.reported_error == Errors.StatusSuccess) && (Cmd_Table[cmd_idx].is_request))
                    {
                        cmd_ackd = false;
                        GLADOS_STATE_TRAN_TO_SIBLING(&CmdAO_state_request);
                        GLADOS_STATE_TRAN_TO_CHILD(&CmdAO_state_request_norm);
                    }
                    /* If not a request command, then report an Ack if no error. */
                    else if (error_code.reported_error == Errors.StatusSuccess)
                    {
                        RftData.cmd_acpt_cnt++;
                        /* Transmits no error */
                        cmd_ackd = true;
                        TxAO_SendMsg_Ack(this_ao, (TxPort_t)cmd_data.port,(uint8_t)cmd_data.src_id, cmd_data.seq_id, cmd_data.msg_id, error_code.reported_error->error_id);
                    }
                    else
                    {
                        /* Error occurred in the handle function, send Nack back */
                        RftData.cmd_rjct_cnt++;
                        cmd_ackd = true;
                        report_error_uint(&AO_Cmd, (err_type*) error_code.reported_error , DO_NOT_SEND_TO_SPACECRAFT, error_code.aux_data);
                        TxAO_SendMsg_Ack(this_ao, (TxPort_t)cmd_data.port,(uint8_t)cmd_data.src_id, cmd_data.seq_id, cmd_data.msg_id, error_code.reported_error->error_id);
                    }
                }
            }
            /* If the command is defined within the table and does not have a handle function,
             * then treat these commands as a custom command */
            else
            {
                cmd_id = Cmd_Table[cmd_idx].cmd_id;

                switch(cmd_id)
                {
                    case ABORT:
                    case MANUAL:
                    case WILD_WEST:
                    case SW_UPDATE:
                        if (Cmd_Table[cmd_idx].params_size != cmd_data.data_size)
                        {
                            cmd_ackd = true;
                            RftData.cmd_rjct_cnt++;
                            report_error_uint(&AO_Cmd, Errors.CmdMsgIdInvalidDataSize , DO_NOT_SEND_TO_SPACECRAFT, Cmd_Table[cmd_idx].params_size);
                            TxAO_SendMsg_Ack(this_ao, (TxPort_t)cmd_data.port,(uint8_t)cmd_data.src_id, cmd_data.seq_id, cmd_data.msg_id, Errors.CmdMsgIdInvalidDataSize->error_id);
                        }
                        else
                        {
                            /* All commands that forward their requests to another AO do not send an ACK
                             * since the AO will signal the appropriate ACK */
                            cmd_ackd = false;
                            GLADOS_AO_PUSH(&AO_Maxwell, EVT_CMD_MAX_TO___BASE + (uint32_t)(0xFUL & ((uint32_t)cmd_id)));
                            GLADOS_STATE_TRAN_TO_SIBLING(&CmdAO_state_request);
                            GLADOS_STATE_TRAN_TO_CHILD(&CmdAO_state_request_state);
                        }
                        break;

                    case GET_STATUS:
                        /* Command is always accepted, however do not increment accept counter */
                        cmd_ackd = true;
                        TxAO_SendMsg_Ack(this_ao, (TxPort_t)cmd_data.port,(uint8_t)cmd_data.src_id, cmd_data.seq_id, cmd_data.msg_id, Errors.StatusSuccess->error_id);
                        break;

                    case SW_UP_STAGE:
                        error_code = custom_cmd_hdl_sw_up_stage(&cmd_data.data_ptr[0], cmd_data.data_size);
                        if (error_code.reported_error->error_id != 0U)
                        {
                            cmd_ackd = true;
                            RftData.cmd_rjct_cnt++;
                            report_error_uint(&AO_Cmd, (err_type*) error_code.reported_error , DO_NOT_SEND_TO_SPACECRAFT,  error_code.aux_data);
                            TxAO_SendMsg_Ack(this_ao, (TxPort_t)cmd_data.port, (uint8_t)cmd_data.src_id, cmd_data.seq_id, cmd_data.msg_id, error_code.reported_error->error_id);
                        }
                        else
                        {
                            cmd_ackd = false;
                            GLADOS_STATE_TRAN_TO_SIBLING(&CmdAO_state_request);
                            GLADOS_STATE_TRAN_TO_CHILD(&CmdAO_state_request_norm);
                        }
                        break;

                    default:
                        /* This case is hit if the command exists in the command table, but
                         * does not yet have a defined command handle function or is not
                         * handled as a request command. */
                        /* Transmits Ack if no error, Nack with error code otherwise */
                        cmd_ackd = true;
                        report_error_uint(&AO_Cmd, Errors.MsgIdFunctionNotImplemented , DO_NOT_SEND_TO_SPACECRAFT, cmd_data.msg_id);
                        TxAO_SendMsg_Ack(this_ao, (TxPort_t)cmd_data.port, (uint8_t)cmd_data.src_id, cmd_data.seq_id, cmd_data.msg_id, Errors.MsgIdFunctionNotImplemented->error_id);
                        break;
                }
            }

            /* Signal to the appropriate Rx AO that the Rx buffer can be used
             * to receive the next command.  If the command was not yet ack'd,
             * then do not free up the Rx AO as this will be done upon
             * completion of the command request. */
            if (cmd_ackd)
            {
                GLADOS_AO_PUSH(&AO_Rx, EVT_CMD_PROCESS_DONE);
            }

            break;

        default:
            GLADOS_STATE_CHECK_PARENT(&CmdAO_state);
            break;
    }

    return;
}


void CmdAO_state_request(GLADOS_AO_t *this_ao, GLADOS_Event_t *event)
{
    DEV_ASSERT(this_ao);
    DEV_ASSERT(event);

    CmdAO_Data_t *tmp_cmd_data;

    switch(event->name)
    {
        /* GLADOS_ENTRY/GLADOS_EXIT optional event for each state */
        case GLADOS_ENTRY:
            GLADOS_Timer_EnableOnce(&Tmr_CmdTimeout);
            break;

        case GLADOS_EXIT:
            GLADOS_Timer_Disable(&Tmr_CmdTimeout);
            /* Signal to the Rx AO that the Rx buffer can be used
             * to receive the next command */
            GLADOS_AO_PUSH(&AO_Rx, EVT_CMD_PROCESS_DONE);
            break;

        case EVT_RX_CMD_RCVD:
            /* If we get another command in the middle of processing, send Nack.
             * This should not happen since the Rx AOs are lower in priority and
             * are not freed up to read from their Rx buffers until signaled by
             * this AO that processing has completed with the current command. */
            RftData.cmd_rjct_cnt++;
            tmp_cmd_data = (CmdAO_Data_t *)&event->data[0];
            report_error_uint(&AO_Cmd, Errors.CmdProcessingPreviousCommand , DO_NOT_SEND_TO_SPACECRAFT, 0x0000);
            TxAO_SendMsg_Ack(this_ao, (TxPort_t)tmp_cmd_data->port,(uint8_t)tmp_cmd_data->src_id, tmp_cmd_data->seq_id, tmp_cmd_data->msg_id, Errors.CmdProcessingPreviousCommand->error_id);

            break;

        case EVT_TMR_CMD_TIMEOUT:
            /* Send Nack */
            RftData.cmd_rjct_cnt++;
            report_error_uint(&AO_Cmd, Errors.CurrentProcessedCommandTimedout , DO_NOT_SEND_TO_SPACECRAFT, 0x0000);
            TxAO_SendMsg_Ack(this_ao, (TxPort_t)cmd_data.port,(uint8_t)cmd_data.src_id, cmd_data.seq_id, cmd_data.msg_id, Errors.CurrentProcessedCommandTimedout->error_id);

            GLADOS_STATE_TRAN_TO_SIBLING(&CmdAO_state_cmd_process);
            break;

        case EVT_CMD_RQST_COMPLETE:
            GLADOS_STATE_TRAN_TO_SIBLING(&CmdAO_state_cmd_process);
            break;

        default:
            GLADOS_STATE_CHECK_PARENT(&CmdAO_state);
            break;
    }

    return;
}


void CmdAO_state_request_state(GLADOS_AO_t *this_ao, GLADOS_Event_t *event)
{
    DEV_ASSERT(this_ao);
    DEV_ASSERT(event);

    switch(event->name)
    {
        /* GLADOS_ENTRY/GLADOS_EXIT optional event for each state */
        case GLADOS_ENTRY:
            break;

        case GLADOS_EXIT:
            break;

        case EVT_MAX_STATE_IN_SAFE:
        case EVT_MAX_STATE_IN_MANUAL:
        case EVT_MAX_STATE_IN_WILD_WEST:
        case EVT_MAX_STATE_IN_SW_UPDATE:
            /* If state mismatches the commanded, then send NACK */
            if ((event->name & 0xFUL) != (uint32_t)(cmd_id & 0xFUL))
            {
                RftData.cmd_rjct_cnt++;
                report_error_uint(&AO_Cmd, Errors.CmdStateTransitionFail, DO_NOT_SEND_TO_SPACECRAFT, 0x0000);
                TxAO_SendMsg_Ack(this_ao, (TxPort_t)cmd_data.port,(uint8_t)cmd_data.src_id, cmd_data.seq_id, cmd_data.msg_id,  Errors.CmdStateTransitionFail->error_id);
            }
            /* Otherwise, send ACK */
            else
            {
                RftData.cmd_acpt_cnt++;
                /* If state mismatches the commanded, then send NACK */
                TxAO_SendMsg_Ack(this_ao, (TxPort_t)cmd_data.port,
                                    (uint8_t)cmd_data.src_id, cmd_data.seq_id, cmd_data.msg_id, 0U);
            }
            GLADOS_AO_PUSH_EVT_SELF(&evt_cmd_rqst_complete);
            break;

        case EVT_MAX_STATE_INVALID:
            RftData.cmd_rjct_cnt++;
            /*Send error for invalid for state to Error AO */

            report_error_uint(&AO_Cmd, Errors.CmdInvalidStateTransition , DO_NOT_SEND_TO_SPACECRAFT, 0x0000);
            TxAO_SendMsg_Ack(this_ao, (TxPort_t)cmd_data.port, (uint8_t)cmd_data.src_id, cmd_data.seq_id, cmd_data.msg_id,  Errors.CmdInvalidStateTransition->error_id);

            /* Transition back to process the next command */
            GLADOS_AO_PUSH_EVT_SELF(&evt_cmd_rqst_complete);
            break;

        case EVT_MAX_STATE_BUSY:
            RftData.cmd_rjct_cnt++;

            /*Send error for state busy to Error AO */
            report_error_uint(&AO_Cmd, Errors.CmdCurrentStateIsBusy , DO_NOT_SEND_TO_SPACECRAFT, 0x0000);
            TxAO_SendMsg_Ack(this_ao, (TxPort_t)cmd_data.port,(uint8_t)cmd_data.src_id,
                                 cmd_data.seq_id, cmd_data.msg_id, Errors.CmdCurrentStateIsBusy->error_id);

            /* Transition back to process the next command */
            GLADOS_AO_PUSH_EVT_SELF(&evt_cmd_rqst_complete);
            break;

        default:
            GLADOS_STATE_CHECK_PARENT(&CmdAO_state_request);
            break;
    }

    return;
}


void CmdAO_state_request_norm(GLADOS_AO_t *this_ao, GLADOS_Event_t *event)
{
    DEV_ASSERT(this_ao);
    DEV_ASSERT(event);

    CmdAO_NackData_t *nack_data;

    switch(event->name)
    {
        /* GLADOS_ENTRY/GLADOS_EXIT optional event for each state */
        case GLADOS_ENTRY:
            break;

        case GLADOS_EXIT:
            break;

        /* Return a NACK with the error data */
        case EVT_CMD_NACK_W_SEND:
            DEV_ASSERT(event->has_data);
            DEV_ASSERT(event->data_size == sizeof(CmdAO_NackData_t));

            nack_data = (CmdAO_NackData_t *)&event->data[0];

            /* Errors aren't logged, the status response should suffice. */

            /* Send Nack */
            TxAO_SendMsg_Ack(this_ao, (TxPort_t)cmd_data.port, (uint8_t)cmd_data.src_id,
                                 cmd_data.seq_id, cmd_data.msg_id, nack_data->err_code.reported_error->error_id);

            RftData.cmd_rjct_cnt++;
            GLADOS_AO_PUSH_EVT_SELF(&evt_cmd_rqst_complete);
            break;

        case EVT_CMD_NACK:
            /* Log error for denial of request because the AO is busy */
            report_error_uint(&AO_Cmd, Errors.CmdCurrentStateIsBusy , DO_NOT_SEND_TO_SPACECRAFT, 0x0000);
            RftData.cmd_rjct_cnt++;
            GLADOS_AO_PUSH_EVT_SELF(&evt_cmd_rqst_complete);
            break;

        case EVT_CMD_ACK_W_SEND:
            /* Send Ack */
            TxAO_SendMsg_Ack(this_ao, (TxPort_t)cmd_data.port, (uint8_t)cmd_data.src_id, cmd_data.seq_id, cmd_data.msg_id, 0U);
            RftData.cmd_acpt_cnt++;
            GLADOS_AO_PUSH_EVT_SELF(&evt_cmd_rqst_complete);
            break;

        case EVT_CMD_ACK:
            RftData.cmd_acpt_cnt++;
            GLADOS_AO_PUSH_EVT_SELF(&evt_cmd_rqst_complete);
            break;

        case EVT_CMD_ACK_NO_INC:
            GLADOS_AO_PUSH_EVT_SELF(&evt_cmd_rqst_complete);
            break;
        case EVT_CMD_CUST_NEXT_APP_ERASE:
             cmd_hdl_set_next_app(Tmr_CmdCustomAppTimer.tmr_evt.data);
            break;

        default:
            GLADOS_STATE_CHECK_PARENT(&CmdAO_state_request);
            break;
    }

    return;
}



void CmdAO_push_nack_w_send_err(GLADOS_AO_t *src_ao, Cmd_Err_Code_t err_code)
{
    GLADOS_Event_t evt;
    CmdAO_NackData_t nack_data = { .err_code = err_code };

    GLADOS_Event_New_Data(&evt, EVT_CMD_NACK_W_SEND, (uint8_t *)&nack_data, sizeof(CmdAO_NackData_t));
    GLADOS_AO_PushEvt(src_ao, &AO_Cmd, &evt);

    return;
}

/* Performs binary search of table until command ID is found */
static int32_t get_cmd_table_idx(uint16_t command_id)
{

    int32_t num_of_rows = sizeof(Cmd_Table) / sizeof(CmdMgr_Cmd_Table_t);
    int32_t first_i = 0L;
    int32_t last_i = num_of_rows - 1L;
    int32_t middle_i;

    int32_t found_idx = -1L;    /* Holds the matching index, -1 if not found */

    while (first_i <= last_i)
    {
        middle_i = (first_i + last_i) / 2U;

        if (command_id < Cmd_Table[middle_i].cmd_id)
        {
            last_i = middle_i - 1U;
        }
        else if (command_id == Cmd_Table[middle_i].cmd_id)
        {
            found_idx = middle_i;
            break;
        }
        else
        {
            first_i = middle_i + 1U;
        }
    }

    return found_idx;
}


Cmd_Err_Code_t cmd_hdl_clr_errors(UNUSED uint8_t *args)
{
    DEV_ASSERT(args);
    Cmd_Err_Code_t error_code = ERROR_NONE;
    uint8_t clear_all_err_arg = 0x0F;
    GLADOS_Event_t evt;
    GLADOS_Event_New_Data(&evt, EVT_CLEAR_ERRORS, &clear_all_err_arg, 1UL);
    GLADOS_AO_PushEvt(&AO_Cmd, &AO_Err, &evt);
    RftData.cmd_acpt_cnt = 0U;
    RftData.cmd_rjct_cnt = 0U;

    return error_code;
}

Cmd_Err_Code_t cmd_hdl_clr_errors_advanced(uint8_t *args)
{
    DEV_ASSERT(args);
    Cmd_Err_Code_t error_code = ERROR_NONE;

    GLADOS_Event_t evt;
    GLADOS_Event_New_Data(&evt, EVT_CLEAR_ERRORS, args, 1UL);
    GLADOS_AO_PushEvt(&AO_Cmd, &AO_Err, &evt);
    RftData.cmd_acpt_cnt = 0U;
    RftData.cmd_rjct_cnt = 0U;

    return error_code;
}

Cmd_Err_Code_t cmd_hdl_set_utime(uint8_t *args)
{
    DEV_ASSERT(args);
    Cmd_Err_Code_t error_code = ERROR_NONE;

    /* Record the current tick when the unix time was received.  This tick is used
     * to calculate the number of ticks that have elapsed since this sync.  The
     * time that has elapsed in msec is then added to the last Unix Time received
     * to get the current Unix Time */
    timer64_get_tick(&RftData.time_sync_lpit_hi, &RftData.time_sync_lpit_lo);

    /* Set the last received 64-bit Unix Time stamp in msec */
    RftData.last_unix_time_sync_us_hi = ((uint32_t)args[0] << 24U) +
                                        ((uint32_t)args[1] << 16U) +
                                        ((uint32_t)args[2] << 8U)  +
                                        ((uint32_t)args[3]);

    RftData.last_unix_time_sync_us_lo = ((uint32_t)args[4] << 24U) +
                                        ((uint32_t)args[5] << 16U) +
                                        ((uint32_t)args[6] << 8U)  +
                                        ((uint32_t)args[7]);
    return error_code;
}




Cmd_Err_Code_t cmd_hdl_get_tlm(UNUSED uint8_t *args)
{
    DEV_ASSERT(args);
    Cmd_Err_Code_t error_code = ERROR_NONE;


    /* cmd_data is a private variable and has the current info for the command */
    TlmAO_RequestTlm(&AO_Cmd, cmd_data.port, cmd_data.seq_id, cmd_data.src_id, cmd_data.msg_id);

    return error_code;
}


Cmd_Err_Code_t cmd_hdl_set_auto_tlm(uint8_t *args)
{
    DEV_ASSERT(args);
    Cmd_Err_Code_t error_code = ERROR_NONE;


    uint8_t auto_en = args[0];
    uint32_t period_ms = ((uint32_t)args[1] << 24U) +
                         ((uint32_t)args[2] << 16U) +
                         ((uint32_t)args[3] << 8U)  +
                         ((uint32_t)args[4]);

    if (auto_en == 0U)
    {
        TlmAO_AutoTlmEnable(cmd_data.src_id, cmd_data.port, false);
    }
    else
    {
        error_code = TlmAO_AutoTlmPeriod(period_ms);
        if (error_code.reported_error == Errors.StatusSuccess)
        {
            TlmAO_AutoTlmEnable(cmd_data.src_id, cmd_data.port, true);
        }
    }

    return error_code;
}

Cmd_Err_Code_t cmd_hdl_dump_mcu(uint8_t *args)
{
    DEV_ASSERT(args);

    enum Constants
    {
        ACK_STATUS_LENGTH      = 2,
        MCU_PFLASH_START_ADDR  = 0x0,
        MCU_PFLASH_END_ADDR    = 0x200000,
        MCU_FLEXNVM_START_ADDR = 0x10000000,
        MCU_FLEXNVM_END_ADDR   = 0x10080000,
        MCU_SRAM_START_ADDR    = 0x1FFE0000,
        MCU_SRAM_END_ADDR      = 0x2001F000,
        MCU_PERIPH_START_ADDR  = 0x40000000,
        MCU_PERIPH_END_ADDR    = 0x40100000
    };

    /* Convert from BE to LE */
    uint32_t mcu_addr = ((uint32_t)args[0] << 24U) +
                        ((uint32_t)args[1] << 16U) +
                        ((uint32_t)args[2] << 8U)  +
                        ((uint32_t)args[3]);

    uint32_t data_len = (uint32_t)args[4];

    /* Starting with error, normal operation should cast to status successful */
    Cmd_Err_Code_t error_code = create_error_uint(Errors.CmdInvalidMcuDataDumpSize,  data_len);

    /* Validate the address and length fall within one of the valid
     * memory regions */
    if (( (mcu_addr <  MCU_PFLASH_END_ADDR)                 &&
          ((mcu_addr + data_len) <= MCU_PFLASH_END_ADDR))      ||
        ( (mcu_addr >= MCU_FLEXNVM_START_ADDR)              &&
          (mcu_addr <  MCU_FLEXNVM_END_ADDR)                &&
          ((mcu_addr + data_len) >  MCU_FLEXNVM_START_ADDR) &&
          ((mcu_addr + data_len) <= MCU_FLEXNVM_END_ADDR))     ||
        ( (mcu_addr >= MCU_SRAM_START_ADDR)                 &&
          (mcu_addr <  MCU_SRAM_END_ADDR)                   &&
          ((mcu_addr + data_len) >  MCU_SRAM_START_ADDR)    &&
          ((mcu_addr + data_len) <= MCU_SRAM_END_ADDR))        ||
        ( (mcu_addr >= MCU_PERIPH_START_ADDR)               &&
          (mcu_addr <  MCU_PERIPH_END_ADDR)                 &&
          ((mcu_addr + data_len) >  MCU_PERIPH_START_ADDR)  &&
          ((mcu_addr + data_len) <= MCU_PERIPH_END_ADDR)))
    {
        /* Validate that the length of data requested fits within a clank payload */
        if (data_len <= (TXAO_CLANK_MAX_XFER_SIZE - ACK_STATUS_LENGTH))
        {
             error_code = ERROR_NONE;

            /* Initiate the transmit */
            TxAO_SendMsg_Data_wAck(&AO_Cmd, (TxPort_t)cmd_data.port, cmd_data.src_id, cmd_data.seq_id,
                                       cmd_data.msg_id, mcu_addr, data_len);

            /* Pushing this event to self serves as a way to avoid returning a status message for this
             * specific command. How this works is that this command is configured as a 'request command'
             * and transitions into this state immediately after returning from this function, which does
             * not transmit a status TLM packet as a result and awaits an ACK/NACK from the requesting
             * state. In this case, there is no requesting state and, instead, an ACK (without send) is
             * pushed here to indicate that the command was executed and is ready to receive the next
             * message. */
            GLADOS_AO_PushEvtName(&AO_Cmd, &AO_Cmd, EVT_CMD_ACK);
        }
    }

    return error_code;
}


Cmd_Err_Code_t cmd_hdl_abort_data_op(UNUSED uint8_t *args)
{
    Cmd_Err_Code_t error_code = ERROR_NONE;

    /* Note: This command always succeeds and therefore does not
     * need to be a request command. */
    GLADOS_AO_PushEvtName(&AO_Cmd, &AO_Data, EVT_CMD_DATA_ABORT);
    return error_code;
}


Cmd_Err_Code_t cmd_hdl_sw_up_preload(uint8_t *args)
{
    DEV_ASSERT(args);

    Cmd_Err_Code_t error_code = ERROR_NONE;
    GLADOS_Event_t evt;
    DataAO_PreloadData_t preld_data;

    /* Convert from BE to LE */
    uint32_t pflash_addr = ((uint32_t)args[0] << 24U) +
                           ((uint32_t)args[1] << 16U) +
                           ((uint32_t)args[2] << 8U)  +
                           ((uint32_t)args[3]);

    uint32_t length      = ((uint32_t)args[4] << 24U) +
                           ((uint32_t)args[5] << 16U) +
                           ((uint32_t)args[6] << 8U)  +
                           ((uint32_t)args[7]);

    uint32_t fram_offset = ((uint32_t)args[8] << 24U) +
                           ((uint32_t)args[9] << 16U) +
                           ((uint32_t)args[10] << 8U) +
                           ((uint32_t)args[11]);

    preld_data.pflash_addr = pflash_addr;
    preld_data.length      = length;
    preld_data.fram_offset = fram_offset;

    GLADOS_Event_New_Data(&evt, EVT_CMD_UPDATE_PRELOAD, (uint8_t *)&preld_data, sizeof(DataAO_PreloadData_t));
    GLADOS_AO_PushEvt(&AO_Cmd, &AO_Data, &evt);

    return error_code;
}


Cmd_Err_Code_t custom_cmd_hdl_sw_up_stage(uint8_t *args, uint8_t payload_length)
{
    DEV_ASSERT(args);

    enum Constants { MIN_DATA_LENGTH = 4U };

    Cmd_Err_Code_t error_code = ERROR_NONE;
    GLADOS_Event_t evt;
    DataAO_StageData_t stage_data;

    /* Convert from BE to LE */
    uint32_t fram_offset = ((uint32_t)args[0] << 24U) +
                           ((uint32_t)args[1] << 16U) +
                           ((uint32_t)args[2] << 8U)  +
                           ((uint32_t)args[3]);

    /* Staging must have at least 1 byte to stage for the given address */
    if (payload_length <= MIN_DATA_LENGTH)
    {
        error_code = create_error_uint(Errors.CmdSwUpdateBelowMinDataAmount,  payload_length);
    }

    else
    {

        stage_data.fram_offset = fram_offset;
        /* The data pointer points to the payload data just after the fram_offset parameter */
        stage_data.data_ptr    = (uint32_t)&args[MIN_DATA_LENGTH];
        stage_data.length      = (uint32_t)payload_length - (uint32_t)MIN_DATA_LENGTH;

        GLADOS_Event_New_Data(&evt, EVT_CMD_UPDATE_STAGE, (uint8_t *)&stage_data, sizeof(DataAO_StageData_t));
        GLADOS_AO_PushEvt(&AO_Cmd, &AO_Data, &evt);
    }

    return error_code;
}


Cmd_Err_Code_t cmd_hdl_sw_up_program(uint8_t *args)
{
    DEV_ASSERT(args);

    Cmd_Err_Code_t error_code = ERROR_NONE;
    GLADOS_Event_t evt;
    DataAO_ProgData_t prog_data;

    /* Convert from BE to LE */
    uint32_t pflash_addr = ((uint32_t)args[0] << 24U) +
                           ((uint32_t)args[1] << 16U) +
                           ((uint32_t)args[2] << 8U)  +
                           ((uint32_t)args[3]);

    uint32_t length      = ((uint32_t)args[4] << 24U) +
                           ((uint32_t)args[5] << 16U) +
                           ((uint32_t)args[6] << 8U)  +
                           ((uint32_t)args[7]);

    uint32_t crc         = ((uint32_t)args[8] << 24U) +
                           ((uint32_t)args[9] << 16U) +
                           ((uint32_t)args[10] << 8U) +
                           ((uint32_t)args[11]);

    prog_data.pflash_addr = pflash_addr;
    prog_data.length      = length;
    prog_data.crc         = crc;

    GLADOS_Event_New_Data(&evt, EVT_CMD_UPDATE_PROGRAM, (uint8_t *)&prog_data, sizeof(DataAO_ProgData_t));
    GLADOS_AO_PushEvt(&AO_Cmd, &AO_Data, &evt);

    return error_code;
}


Cmd_Err_Code_t cmd_hdl_set_wdi_en(uint8_t *args)
{
    DEV_ASSERT(args);
    Cmd_Err_Code_t error_code = ERROR_NONE;
    GPIO_SetOutput(GPIO_WDT_PET, (bool)args[0]);
    RftData.gpio_wdi_fb = (bool)args[0];
    return error_code;
}

Cmd_Err_Code_t cmd_hdl_set_wdt_rst_nclr_en(uint8_t *args)
{
    DEV_ASSERT(args);

    Cmd_Err_Code_t error_code = ERROR_NONE;  /* No error */
    GPIO_SetOutput(GPIO_WDT_TRIG_nCLR_EN, (bool)args[0]);
    RftData.gpio_wdt_rst_nclr_fb = (bool)args[0];
    return error_code;
}

Cmd_Err_Code_t cmd_hdl_set_debug_led0_en(uint8_t *args)
{
    DEV_ASSERT(args);

    Cmd_Err_Code_t error_code = ERROR_NONE; /* No error */
    GPIO_SetOutput(GPIO_LED0, (bool)args[0]);

    if ((bool)args[0])
    {
        PINS_DRV_SetPins(EVB_DEBUG_LED_PORT, 1U << 22U);
    }
    else
    {
        PINS_DRV_ClearPins(EVB_DEBUG_LED_PORT, 1U << 22U);
    }
    return error_code;
}

Cmd_Err_Code_t cmd_hdl_set_hum_temp_sel_en(uint8_t *args)
{
    DEV_ASSERT(args);
    Cmd_Err_Code_t error_code = ERROR_NONE;
    GPIO_SetOutput(GPIO_HUM_TEMP_SEL, (bool)args[0]);
    RftData.gpio_hum_temp_sel_fb = (bool)args[0];
    return error_code;
}

Cmd_Err_Code_t cmd_hdl_set_wf_en(uint8_t *args)
{
    DEV_ASSERT(args);
    Cmd_Err_Code_t error_code = ERROR_NONE;
    GPIO_SetOutput(GPIO_WF_EN, (bool)args[0]);
    RftData.gpio_wf_en_fb = (bool)args[0];
    return error_code;
}

Cmd_Err_Code_t cmd_hdl_set_pp_en(uint8_t *args)
{
    DEV_ASSERT(args);
    Cmd_Err_Code_t error_code = ERROR_NONE;
    GPIO_SetOutput(GPIO_PP_EN, (bool)args[0]);
    RftData.gpio_pp_en_fb = (bool)args[0];
    return error_code;
}


Cmd_Err_Code_t cmd_hdl_pp_vset(uint8_t *args)
{
    DEV_ASSERT(args);

    uint32_t vset_bytes = ((uint32_t)args[0] << 24U) +
                          ((uint32_t)args[1] << 16U) +
                          ((uint32_t)args[2] << 8U)  +
                          ((uint32_t)args[3]);

    Cmd_Err_Code_t error_code = DaqCtlAO_PpVset_SetVoltage(BYTES_TO_F32(vset_bytes), true);

    if (error_code.reported_error->error_id != STATUS_SUCCESS){
        error_code = create_error_float(Errors.CmdInvalidPpVoltSetpointInvalid, BYTES_TO_F32(vset_bytes));
    }


    return error_code;
}

Cmd_Err_Code_t cmd_hdl_pp_vset_pwm_raw(uint8_t *args)
{
    DEV_ASSERT(args);
    uint16_t duty_cycle_cnt = ((uint16_t)args[0] << 8U) + (uint16_t)args[1];
    Cmd_Err_Code_t error_code = DaqCtlAO_PpVset_SetPwmRaw(duty_cycle_cnt, true);
    return error_code;
}


Cmd_Err_Code_t cmd_hdl_pp_vset_ramp(uint8_t *args)
{
    DEV_ASSERT(args);

    uint32_t vset_bytes = ((uint32_t)args[0] << 24U) +
                          ((uint32_t)args[1] << 16U) +
                          ((uint32_t)args[2] << 8U)  +
                          ((uint32_t)args[3]);

    uint32_t ramp_time_bytes = ((uint32_t)args[4] << 24U) +
                          ((uint32_t)args[5] << 16U) +
                          ((uint32_t)args[6] << 8U)  +
                          ((uint32_t)args[7]);
    float voltage_set = BYTES_TO_F32(vset_bytes);
    float ramp_time  = BYTES_TO_F32(ramp_time_bytes);

    Cmd_Err_Code_t error_code = DaqCtlAO_Initalize_PP_Ramp(voltage_set,ramp_time);

    return error_code;
}



Cmd_Err_Code_t cmd_hdl_wav_gen_freq(uint8_t *args)
{
    DEV_ASSERT(args);

    Cmd_Err_Code_t error_code = ERROR_NONE;
    uint32_t float_bytes = ((uint32_t)args[0] << 24U) +
                           ((uint32_t)args[1] << 16U) +
                           ((uint32_t)args[2] << 8U)  +
                           ((uint32_t)args[3]);


    error_code = DaqCtlAO_WF_SetFreq(BYTES_TO_F32(float_bytes));

    return error_code;
}


Cmd_Err_Code_t cmd_hdl_stp_wtd(UNUSED uint8_t *args)
{
    Cmd_Err_Code_t error_code = ERROR_NONE;
    WdtAO_Stop_WDT(&AO_Cmd);
    return error_code;
}


Cmd_Err_Code_t cmd_hdl_get_sw_info(uint8_t *args)
{
    Cmd_Err_Code_t error_code = ERROR_NONE;
    uint8_t sw_application_selection = args[0];
    error_code = update_flash_hdr(sw_application_selection);
    if (error_code.reported_error->error_id == STATUS_SUCCESS)
    {

        uint32_t size_of_sw_info = sizeof(*app_hdr_rec);
        TxAO_SendMsg_Data_wAck(&AO_Cmd, (TxPort_t)cmd_data.port,(uint8_t)cmd_data.src_id, cmd_data.seq_id, cmd_data.msg_id, header_address, size_of_sw_info);
        GLADOS_AO_PushEvtName(&AO_Cmd, &AO_Cmd, EVT_CMD_ACK);
    }

    return error_code;
}


Cmd_Err_Code_t cmd_hdl_write_max_hdr_info(uint8_t *args)
{
    DEV_ASSERT(args);
    Cmd_Err_Code_t error_code = write_maxwell_hrd_record(&AO_Cmd,(char*)args,129);
    return error_code;

}

Cmd_Err_Code_t cmd_hdl_read_max_hdr_info( uint8_t *args)
{
    uint8_t mcu_dump_args[5];

    Cmd_Err_Code_t error_code = calculate_maxwell_hrd_record_address(args[0],mcu_dump_args);

    if (error_code.reported_error->error_id==STATUS_SUCCESS){
        error_code =cmd_hdl_dump_mcu(mcu_dump_args);
    }
    return error_code;

}

Cmd_Err_Code_t cmd_hdl_erase_max_hdr_info(UNUSED uint8_t *args)
{
    Cmd_Err_Code_t error_code = ERROR_NONE;
    erase_max_hrd_record(&AO_Cmd);
    return error_code;

}


Cmd_Err_Code_t cmd_hdl_set_next_app(uint8_t *args)
{
    DEV_ASSERT(args);

       static uint8_t command_state = 0;
       Cmd_Err_Code_t error_code = ERROR_NONE;

       if ((args[0] >= 1U) && (args[0] <= 3U))
       {
           if (command_state == 0)
           {
               status_t status = erase_app_selection_sector(&AO_Cmd);
               if (status != STATUS_SUCCESS)
               {
                   /* Set error code, which does not transition to the request state */
                   error_code = create_error_uint(Errors.FlashEraseFailed, (uint32_t)status);
               }
               else
               {
                   command_state = 1;
                   GLADOS_Timer_Disable(&Tmr_CmdCustomAppTimer);

                   /* Set up the timer, worst case timing for sector erase is 130ms */
                   GLADOS_Timer_Init(&Tmr_CmdCustomAppTimer, 130000UL, EVT_CMD_CUST_NEXT_APP_ERASE);
                   Tmr_CmdCustomAppTimer.tmr_evt.data[0] = args[0];
                   Tmr_CmdCustomAppTimer.tmr_evt.data_size = 1;
                   Tmr_CmdCustomAppTimer.tmr_evt.has_data = true;
                   GLADOS_Timer_Subscribe_AO(&Tmr_CmdCustomAppTimer, &AO_Cmd);
                   GLADOS_Timer_EnableOnce(&Tmr_CmdCustomAppTimer);
               }
           }
           else if (command_state == 1)
           {
               GLADOS_Timer_Disable(&Tmr_CmdCustomAppTimer);
               error_code = write_next_app_select(&AO_Cmd, args[0]);
               GLADOS_AO_PushEvtName(&AO_Cmd, &AO_Cmd, EVT_CMD_ACK);
               command_state = 0;
               TxAO_SendMsg_Ack(&AO_Cmd, (TxPort_t)cmd_data.port,(uint8_t)cmd_data.src_id, cmd_data.seq_id, cmd_data.msg_id, error_code.reported_error->error_id);
           }
       }
       else
       {
           error_code = create_error_uint(Errors.CmdInvalidParameter, args[0]);
       }

       return error_code;
}


Cmd_Err_Code_t cmd_hdl_trnsf_flash(UNUSED uint8_t *args)
{
    Cmd_Err_Code_t error_code = ERROR_NONE;
    GLADOS_AO_PushEvtName(&AO_Cmd, &AO_Err, EVT_TRANSFER_TO_NVM);
    return error_code;
}



Cmd_Err_Code_t cmd_hdl_prg_flsh(UNUSED uint8_t *args)
{
    Cmd_Err_Code_t error_code = ERROR_NONE;

    GLADOS_AO_PushEvtName(&AO_Cmd, &AO_Err, EVT_WRITE_FLASH);

    return error_code;
}



Cmd_Err_Code_t cmd_hdl_wrt_err(uint8_t *args)
{
    Cmd_Err_Code_t error_code = ERROR_NONE;
    static uint16_t increment=0;
    uint8_t spaceraft_visable = args[2];
    uint16_t msg_id = ((uint16_t)args[0] << 8U) + (uint16_t)args[1];
    err_type* error_sent = (err_type*) Errors.StatusSuccess;
    switch(msg_id){
        case 1:
            error_sent = (err_type*) Errors.EmbeddedTimeout;
            break;
        case 2:
            error_sent = (err_type*)Errors.NotEnoughBytes;
            break;
        case 3:
            error_sent =  (err_type*) Errors.MalformedHeader;
            break;
        case 4:
            error_sent = (err_type*) Errors.SyncBytesError;
            break;
        case 5:
            error_sent = (err_type*) Errors.SourceAddressError;
            break;
        case 6:
            error_sent = (err_type*) Errors.TransmitMaskError ;
            break;
        case 7:
            error_sent = (err_type*) Errors.PktDataLenError ;
            break;
        case 8:
            error_sent = (err_type*) Errors.ComRxCrcError;
            break;
        case 9:
            error_sent = (err_type*) Errors.MsgIdFunctionNotImplemented ;
            break;
        case 10:
            error_sent = (err_type*) Errors.CurrentProcessedCommandTimedout;
            break;
        case 11:
            error_sent = (err_type*) Errors.UnexpectedTransition;
            break;
        case 12:
            error_sent = (err_type*) Errors.Ad568SpiSetFailed;
            break;
        case 13:
            error_sent = (err_type*) Errors.MdotSetPointOutOfBounds ;
            break;
        case 14:
            error_sent = (err_type*) Errors.InvalidTxParameters;
            break;
        case 15:
            error_sent = (err_type*) Errors.IntermediateTxQueueHasNoData;
            break;
        case 16:
            error_sent = (err_type*) Errors.TxAoIsBusy;
            break;
        case 17:
            error_sent = (err_type*) Errors.TlmTmrTimeout;
            break;
        case 18:
            error_sent = (err_type*) Errors.InvalidTelemtryEventRequest;
            break;
        case 19:
            error_sent = (err_type*) Errors.RxScReciviedJunkData;
            break;
        case 20:
            error_sent = (err_type*) Errors.ReceivedMalformClankPacket;
            break;
        case 21:
            error_sent = (err_type*) Errors.ReceivedRxscTimeout;
            break;
        case 22:
            error_sent = (err_type*) Errors.MismatchCrc;
            break;
        case 23:
            error_sent = (err_type*) Errors.GseRxCircularBufferOverrun;
            break;
        case 24:
            error_sent = (err_type*) Errors.EvtCmdDumpTlmInvalidSize;
            break;
        case 25:
            error_sent = (err_type*) Errors.EvtDataOpTimeout;
            break;
        case 26:
            error_sent = (err_type*) Errors.CmdInvalidDurationSetpt;
            break;
        case 27:
            error_sent = (err_type*) Errors.CmdDataModeIsBusy;
            break;
        case 28:
            error_sent = (err_type*) Errors.CmdUpdateStageInvalidParam;
            break;
        case 29:
            error_sent = (err_type*) Errors.CmdUpdatePreloadInvalidParam;
            break;
        case 30:
            error_sent = (err_type*) Errors.CmdUpdateProgramInvalidParam;
            break;
        case 31:
             error_sent = (err_type*) Errors.CmdDataAbortDueToSafe;
             break;
    };

    report_error_uint(&AO_Cmd, error_sent,spaceraft_visable,0x56789ABCUL+increment);

    increment = increment + 1;
    return error_code;
}


Cmd_Err_Code_t cmd_hdl_dft_sram_ecc_1bit(uint8_t *args)
{
    DEV_ASSERT(args);

    enum Constants { SRAM_DUMMY_ADDR = 0x1FFFFFF0 };

    Cmd_Err_Code_t error_rec = ERROR_NONE;
    uint8_t num_of_reads = args[0];

    uint8_t i;
    uint32_t dummy_read;

    /* Initialize address used to test */
    *(uint32_t *)SRAM_DUMMY_ADDR = 0U;

    EIM_DRV_Init(INST_EIM1, EIM_CHANNEL_COUNT0, eim1_ChannelConfig0);

    for (i = 0U; i < num_of_reads; ++i)
    {
        /* Read any address on RAM  */
        /* Enable read region Ram (0x1FFF8000 - 0x20006FFF) when debug equal Flash */
        dummy_read = *(uint32_t *)SRAM_DUMMY_ADDR;
    }

    /* Removes compiler warning */
    (void)dummy_read;

    /* Deinit EIM module */
    EIM_DRV_Deinit(INST_EIM1);

    return error_rec;
}


Cmd_Err_Code_t cmd_hdl_dft_pt_sell_your_soul(UNUSED uint8_t *args)
{
    DEV_ASSERT(((uint32_t)PT_Default % 4U) == 0U);
    DEV_ASSERT(((uint32_t)PT % 4U) == 0U);

    enum Constants { PT_SELECT_MASK = 0x3, PT_SYS_MODE_MASK = 0xF0 };

    Cmd_Err_Code_t error_rec = ERROR_NONE;
    Img_Header_Rec_t *img_hdr;
    uint32_t act_img_crc;

    /* Sell your soul is intended as a way to adjust PT values
     * while the FSW is running. It is entered by performing
     * the following steps:
     * 1. Determine the PT that is in use
     *    a. If default table is in use, then nothing more to
     *       do since we've already sold our soul
     * 2. Integrity check the PT (unless if it's default PT)
     * 3. Copy over the PT to the default table
     */

    /* If PT is loaded from either Runtime or App tables, then perform the CRC and copying.
     * Note: Redo if already in SYS since it can reset with a valid PT config. */
    if (((RftData.pt_select & PT_SELECT_MASK) == 1U) ||
        ((RftData.pt_select & PT_SELECT_MASK) == 2U))
    {
        /* Start from the PT core and work backward to the image header */
        img_hdr = (Img_Header_Rec_t *)(((uint32_t)PT) - PARAM_TABLE_IMG_HDR_SIZE);

        act_img_crc = crc32_compute(0xFFFFFFFFUL, (uint8_t *)((uint32_t)img_hdr + IMG_HDR_MAX_SIZE),
                                        img_hdr->img_length, true);

        if (img_hdr->img_crc == act_img_crc)
        {
            /* Copy the PT into the default PT and point the current PT there */
            memcpy((uint8_t *)PT_Default, (uint8_t *)PT, PARAM_TABLE_CORE_SIZE);
            PT = PT_Default;
            fidr_init();
            RftData.pt_select |= PT_SYS_MODE_MASK;
        }
        else
        {
            error_rec = create_error_float(Errors.CmdNoSoulToSell, act_img_crc);
        }
    }
    else
    {
        RftData.pt_select |= PT_SYS_MODE_MASK;
    }

    return error_rec;
}

Cmd_Err_Code_t cmd_hdl_dft_pt_set(uint8_t *args)
{
    DEV_ASSERT(args);
    DEV_ASSERT(((uint32_t)PT_Default % 4U) == 0U);

    uint32_t *pt_value;

    Cmd_Err_Code_t error_rec = ERROR_NONE;

    uint32_t pt_offset = ((uint32_t)args[0] << 24U) +
                         ((uint32_t)args[1] << 16U) +
                         ((uint32_t)args[2] << 8U)  +
                         ((uint32_t)args[3]);

    uint32_t raw_value = ((uint32_t)args[4] << 24U) +
                         ((uint32_t)args[5] << 16U) +
                         ((uint32_t)args[6] << 8U)  +
                         ((uint32_t)args[7]);

    /* All values in the PT are 4-byte aligned, so check for this
     * and also validate that the offset does not exceed the table
     * size or rollover a u32 */
    if (((pt_offset % 4U) != 0U)              ||
        ((pt_offset > PARAM_TABLE_CORE_SIZE)) ||
        ((pt_offset + sizeof(uint32_t)) >= PARAM_TABLE_CORE_SIZE))
    {
        error_rec = create_error_float(Errors.CmdWishNotGranted, pt_offset);
    }
    else
    {
        /* Copying over a raw value should handle both u32 and f32 */
        pt_value = (uint32_t *)(((uint32_t)PT_Default) + pt_offset);
        *pt_value = raw_value;

        /* Reinitialize the FDIR table every time since it's possible
         * one of those values were updated */
        fidr_init();
    }

    return error_rec;
}

