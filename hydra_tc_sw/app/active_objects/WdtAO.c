/*Copyright (c) 2018-2019 by Phase Four, Inc. All Rights Reserved. Confidential and Proprietary.
 * 
 * WdtAO.c
 */

#include "glados.h"
#include "WdtAO.h"
#include "gpio.h"

GLADOS_AO_t AO_Wdt;
GLADOS_Timer_t Tmr_4HzPetWtd;

static GLADOS_Event_t wdtao_fifo[WDT_FIFO_SIZE];


void WdtAO_init(void)
{
    GLADOS_AO_Init(&AO_Wdt, WDT_PRIORITY, wdtao_fifo, WDT_FIFO_SIZE,&WdtAO_state_idle);
    return;
}


void WdtAO_state_idle(GLADOS_AO_t *this_ao, GLADOS_Event_t *event)
{

    DEV_ASSERT(this_ao);
    DEV_ASSERT(event);

    switch (event->name)
    {
        /* GLADOS_ENTRY/GLADOS_EXIT optional event for each state */
        /* Initialize private data and starts the watchdog */
        case GLADOS_ENTRY:
            /* Do the first pet manually */
            GPIO_PetWDT();
            GLADOS_Timer_Init(&Tmr_4HzPetWtd, 250000UL, EVT_4HZ_TMR_PET_WTDG);
            GLADOS_Timer_Subscribe_AO(&Tmr_4HzPetWtd, this_ao);
            GLADOS_Timer_EnableContinuous(&Tmr_4HzPetWtd);
            break;

            /* Toggles the watchdog pin */
        case EVT_4HZ_TMR_PET_WTDG:
            /* This strobes a dev board LED, leave in as it may be useful */
            PINS_DRV_TogglePins(EVB_DEBUG_LED_PORT, 1U << EVB_DEBUG_LED_BLUE_PIN);
            /* This strobes the LED on the flight hardware */
            GPIO_ToggleOutput(GPIO_LED0);
            GPIO_PetWDT();
            break;

            /* Stops the timer from toggling the pin and sets the pin to a logic 0. THIS WILL RESET THE MCU. */
        case EVT_STP_WTDG_TMR:
            GLADOS_Timer_Disable(&Tmr_4HzPetWtd);
            GPIO_SetOutput(GPIO_WDT_PET, 0U);
            GPIO_SetOutput(GPIO_LED0, false);
            break;

            /* A optional event but should never get here.*/
        case GLADOS_EXIT:
            DEV_ASSERT(0); /*Should never get here.*/
            break;

        default:
            /* Top level state, skip undefined Events */
            DEV_ASSERT(0);
            break;
    }

    return;;
}



/* Public function to stop the watchdog*/
void WdtAO_Stop_WDT(GLADOS_AO_t *src_ao)
{
    GLADOS_AO_PushEvtName(src_ao, &AO_Wdt, EVT_STP_WTDG_TMR);

}

