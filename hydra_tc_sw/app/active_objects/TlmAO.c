/* Copyright (c) 2017-2020 by Phase Four, Inc. All Rights Reserved. Confidential and Proprietary.
 *
 * TlmAO.c
 */


#include "TlmAO.h"
#include "mcu_types.h"
#include "glados.h"
#include "DaqCtlAO.h"
#include "CmdAO.h"
#include "TxAO.h"
#include "p4_tlm.h"
#include "rft_data.h"
#include "clank.h"
#include "p4_cmd.h"
#include "max_info_driver.h"
#include "DataAO.h"
#include "ErrAO.h"
#include "default_pt.h"
#include "string.h"

/* Note: Keeping in for SOE debugging purposes */
#include "mcu_adc.h"

GLADOS_AO_t AO_Tlm;

/* Active Object timers */
GLADOS_Timer_t Tmr_TlmTimeout;
GLADOS_Timer_t Tmr_TlmAuto;

/* Active Object private variables */
static GLADOS_Event_t tlm_fifo[TLMAO_FIFO_SIZE];

static uint8_t tlm_daq_buf[TLMAO_MAX_REC_TLM_SIZE] = {0};
static uint8_t tlm_cmd_buf[TLMAO_MAX_REC_TLM_SIZE] = {0};
static uint8_t tlm_cmd2_buf[TLMAO_MAX_REC_TLM_SIZE] = {0};

static uint8_t auto_tlm_uart_port = PORT_SC;
static uint8_t auto_tlm_dest_id   = 0xFFU;
static uint8_t tlm_cnt_seq_id = 0U;

static void create_tlm_payload(P4Tlm_RftTlm_t *tlm_pkt);



/* Active Object state definitions */


void TlmAO_init(void)
{
    /* Initialize the Active Object with both its Event FIFO and initial state function */
    GLADOS_AO_Init(&AO_Tlm, TLMAO_PRIORITY, tlm_fifo, TLMAO_FIFO_SIZE, &TlmAO_state);

    return;
}


void TlmAO_state(GLADOS_AO_t *this_ao, GLADOS_Event_t *event)
{
    DEV_ASSERT(this_ao);
    DEV_ASSERT(event);

    switch(event->name)
    {
        /* GLADOS_ENTRY/GLADOS_EXIT optional event for each state */
        case GLADOS_ENTRY:
            /* Arbitrary number as this value is set via command */
            GLADOS_Timer_Init(&Tmr_TlmAuto, 1000000UL, EVT_TMR_AUTO_TLM);
            GLADOS_Timer_Subscribe_AO(&Tmr_TlmAuto, this_ao);

            GLADOS_Timer_Init(&Tmr_TlmTimeout, PT->tlm_xfer_timeout_us, EVT_TMR_TLM_TIMEOUT);
            GLADOS_Timer_Subscribe_AO(&Tmr_TlmTimeout, this_ao);

            GLADOS_STATE_TRAN_TO_CHILD(&TlmAO_state_idle);
            break;

        case GLADOS_EXIT:
            break;

        default:
            /* Top level state, skip undefined Events */
            break;
    }

    return;
}


void TlmAO_state_idle(GLADOS_AO_t *this_ao, GLADOS_Event_t *event)
{
    DEV_ASSERT(this_ao);
    DEV_ASSERT(event);

    TlmAO_Rqst_Data_t *tlm_rqst;
    Cmd_Err_Code_t error;

    switch(event->name)
    {
        /* GLADOS_ENTRY/GLADOS_EXIT optional event for each state */
        case GLADOS_ENTRY:
            break;

        case GLADOS_EXIT:
            break;

        case EVT_DAQ_DATA_READY:
            create_tlm_payload((P4Tlm_RftTlm_t *)&tlm_daq_buf[0]);
            break;

        /* Data: uart_port, src_id, seq_id, msg_id */
        case EVT_TMR_AUTO_TLM:
            /* Copy DAQ Buffer to a CMD Buffer to protect the TLM data during transmission */
            memcpy(&tlm_cmd_buf[0], &tlm_daq_buf[0], sizeof(tlm_cmd_buf));

            /* Queue the transmit of the auto TLM packet */
            TxAO_SendMsg_Tlm(this_ao, auto_tlm_uart_port, auto_tlm_dest_id, tlm_cnt_seq_id,
                                 RX_AUTO_TLM_RFT, (uint32_t)&tlm_cmd_buf[0], TLMAO_MAX_TLM_DATA);

            /* For auto TLM, start at 0 and increment by one on each auto TLM transfer, let
             * the uint8_t handle the rollover back to 0 */
            ++tlm_cnt_seq_id;

            GLADOS_STATE_TRAN_TO_CHILD(&TlmAO_state_idle_xfer_tlm);
            break;

        case EVT_CMD_TLM_REQUEST:
            if (!event->has_data || (event->data_size != sizeof(TlmAO_Rqst_Data_t)))
            {
                error = create_error_uint(Errors.TlmInvalidRequestData, 0U);
                CmdAO_push_nack_w_send_err(this_ao, error);
            }
            else
            {
                tlm_rqst = (TlmAO_Rqst_Data_t *)&event->data[0];

                /* Copy DAQ Buffer to a CMD Buffer to protect the TLM data during transmission.
                 * Note: Simply copying over the TLM buffer does not increment the command
                 * acpt counter for the current command. It's prob just simpler to leave this
                 * as is rather than updating the acpt count or error data since this could
                 * very easily lead to data mismatches between what is returned at time x and
                 * what is recorded at time x. Therefore, we expect GET_TLM to indicate an
                 * increased cmd_acpt_cnt upon the next status/TLM packet rather than this one. */
                memcpy(&tlm_cmd_buf[0], &tlm_daq_buf[0], TLMAO_MAX_TLM_DATA);

                TxAO_SendMsg_Tlm(this_ao, tlm_rqst->port, tlm_rqst->src_id, tlm_rqst->seq_id,
                                     tlm_rqst->msg_id, (uint32_t)&tlm_cmd_buf[0], TLMAO_MAX_TLM_DATA);

                /* Signal command completion but also to not increment the command accept
                 * counter, which avoids use cases where high rate polling for TLM spams
                 * the accept counter. */
                GLADOS_AO_PUSH(&AO_Cmd, EVT_CMD_ACK_NO_INC);

                GLADOS_STATE_TRAN_TO_CHILD(&TlmAO_state_idle_xfer_tlm);
            }
            break;

        case EVT_TX_TLM_DONE:
            /* Any extras from TxAO ignore as we have already timed out */
            break;

        default:
            GLADOS_STATE_CHECK_PARENT(&TlmAO_state);
            break;
    }

    return;
}


void TlmAO_state_idle_xfer_tlm(GLADOS_AO_t *this_ao, GLADOS_Event_t *event)
{
    DEV_ASSERT(this_ao);
    DEV_ASSERT(event);

    static int8_t tlm_xfer_cnt;
    static bool sent_second_tlm;

    TlmAO_Rqst_Data_t *tlm_rqst;
    Cmd_Err_Code_t error;

    switch(event->name)
    {
        /* GLADOS_ENTRY/GLADOS_EXIT optional event for each state */
        case GLADOS_ENTRY:
            tlm_xfer_cnt = 1;
            sent_second_tlm = false;
            GLADOS_Timer_EnableOnce(&Tmr_TlmTimeout);
            break;

        case GLADOS_EXIT:
            GLADOS_Timer_Disable(&Tmr_TlmTimeout);
            break;

        case EVT_TMR_AUTO_TLM:
            /* Ignore since TLM transfer is already underway */
            break;

        case EVT_CMD_TLM_REQUEST:
            if (!event->has_data || (event->data_size != sizeof(TlmAO_Rqst_Data_t)))
            {
                error = create_error_uint(Errors.TlmInvalidRequestData, 0U);
                CmdAO_push_nack_w_send_err(this_ao, error);
            }
            else
            {
                tlm_rqst = (TlmAO_Rqst_Data_t *)&event->data[0];

                /* Copy DAQ Buffer to a CMD Buffer to protect the TLM data during transmission.
                 * Note: Simply copying over the TLM buffer does not increment the command
                 * acpt counter for the current command. It's prob just simpler to leave this
                 * as is rather than updating the acpt count or error data since this could
                 * very easily lead to data mismatches between what is returned at time x and
                 * what is recorded at time x. Therefore, we expect GET_TLM to indicate an
                 * increased cmd_acpt_cnt upon the next status/TLM packet rather than this one. */
                memcpy(&tlm_cmd2_buf[0], &tlm_daq_buf[0], TLMAO_MAX_TLM_DATA);

                TxAO_SendMsg_Tlm(this_ao, tlm_rqst->port, tlm_rqst->src_id, tlm_rqst->seq_id,
                                     tlm_rqst->msg_id, (uint32_t)&tlm_cmd2_buf[0], TLMAO_MAX_TLM_DATA);

                /* Delay sending the ACK to CMD AO since this is a special case that only
                 * expects to be used during testing, which is with Auto TLM enabled and
                 * asking for TLM at the same time. In this instance, it is okay to delay
                 * returning the ACK until after the message transmission has completed.
                 * This, in a way, throttles the number of TLM packets that can be requested
                 * since CmdAO will not accept the next command (even if it is a GET_TLM) until
                 * the TxAO has indicated transmission has completed.
                 *
                 * Delaying the ACK to CMD AO occurs through the use of a flag
                 * that will send the ACK after the TLM has completed. */
                sent_second_tlm = true;
                ++tlm_xfer_cnt;

                /* Stay in this state */
            }
            break;

        case EVT_TMR_TLM_TIMEOUT:
            if (sent_second_tlm)
            {
                /* If it's a secondary TLM, then a NACK needs to be transmitted */
                error = create_error_uint(Errors.TlmTmrTimeout, Tmr_TlmTimeout.duration_us);
                CmdAO_push_nack_w_send_err(this_ao, error);
            }
            else
            {
                /* If for some reason the EVT_TX_TLM_DONE did not return from TxAO, then
                 * log the error internally */
                report_error_uint(this_ao, Errors.TlmTmrTimeout, DO_NOT_SEND_TO_SPACECRAFT, Tmr_TlmTimeout.duration_us);
            }
            GLADOS_STATE_TRAN_TO_PARENT(&TlmAO_state_idle);
            break;

        case EVT_TX_TLM_DONE:
            --tlm_xfer_cnt;

            /* No other TLM to be transmitted! */
            if (tlm_xfer_cnt <= 0)
            {
                /* If a secondary TLM packet was transmitted, then forward the ACK. */
                if (sent_second_tlm)
                {
                    GLADOS_AO_PUSH(&AO_Cmd, EVT_CMD_ACK_NO_INC);
                }
                GLADOS_STATE_TRAN_TO_PARENT(&TlmAO_state_idle);
            }
            break;

        default:
            GLADOS_STATE_CHECK_PARENT(&TlmAO_state_idle);
            break;
    }

    return;
}


/* Public Helper Functions */


Cmd_Err_Code_t TlmAO_AutoTlmPeriod(uint32_t period_ms)
{
    bool timer_is_enabled;
    Cmd_Err_Code_t error_code = create_error_uint(Errors.CmdInvalidAutoTlmRange, period_ms);

    if ((period_ms >= (PT->tlm_auto_tlm_min_period_us/1000UL)) &&
        (period_ms <= (PT->tlm_auto_tlm_max_period_us/1000UL)))
    {
        error_code = create_error_uint(Errors.StatusSuccess, 0UL);

        timer_is_enabled = Tmr_TlmAuto.enabled;
        GLADOS_Timer_Disable(&Tmr_TlmAuto);
        GLADOS_Timer_Set(&Tmr_TlmAuto, period_ms * 1000UL);
        if (timer_is_enabled)
        {
            GLADOS_Timer_EnableContinuous(&Tmr_TlmAuto);
        }
    }

    return error_code;
}


void TlmAO_AutoTlmEnable(uint8_t dest_id, uint8_t uart_port, bool enable)
{
    /* Destination ID is already checked upon reception for validity. But
     * if dest_id is invalid or not configured (i.e. > 0xF), this should
     * transmit 0 as the transmit mask and will be an invalid Clank packet. */
    DEV_ASSERT(dest_id < CLANK_MAX_SRC_ADDR);

    auto_tlm_dest_id   = dest_id;
    auto_tlm_uart_port = (uart_port & (uint8_t)PORT_GSE);

    if (enable)
    {
        GLADOS_Timer_EnableContinuous(&Tmr_TlmAuto);
    }
    else
    {
        GLADOS_Timer_Disable(&Tmr_TlmAuto);
    }

    return;
}


void TlmAO_RequestTlm(GLADOS_AO_t *ao_src, uint8_t port, uint8_t seq_id, uint8_t src_id, uint16_t msg_id)
{
    GLADOS_Event_t evt;

    TlmAO_Rqst_Data_t tlm_request =
    {
        .port   = port,
        .seq_id = seq_id,
        .src_id = src_id,
        .msg_id = msg_id
    };

    GLADOS_Event_New_Data(&evt, EVT_CMD_TLM_REQUEST, (uint8_t *)&tlm_request, sizeof(TlmAO_Rqst_Data_t));
    GLADOS_AO_PushEvt(ao_src, &AO_Tlm, &evt);

    return;
}


static void create_tlm_payload(P4Tlm_RftTlm_t *tlm_pkt)
{
    DEV_ASSERT(sizeof(P4Tlm_RftTlm_t) <= TLMAO_MAX_TLM_DATA);

    Err_Rft_Rec_t *total_err = (Err_Rft_Rec_t *)&telemetry_record[0];
    Tlm_Err_Rft_Rec_t *tlm_err = (Tlm_Err_Rft_Rec_t*) &total_err->tlm_err;
    Status_Err_Rft_Rec_t *status_err = (Status_Err_Rft_Rec_t*) &total_err->status_err;
    tlm_pkt->tc_status.tc_ack_status      = 0U;
    tlm_pkt->tc_status.sc_tc_err_code1    = REV16(status_err->sc_err_1);
    tlm_pkt->tc_status.sc_tc_err_code2    = REV16(status_err->sc_err_2);
    tlm_pkt->tc_status.sc_tc_err_code3    = REV16(status_err->sc_err_3);
    tlm_pkt->tc_status.tc_state           = (uint8_t)RftData.state;
    tlm_pkt->tc_status.tc_busy            = (uint8_t)RftData.busy;
    tlm_pkt->tc_status.tc_needs_reset     = (uint8_t)RftData.needs_reset_flag;
    tlm_pkt->tc_status.tc_app_select      = (uint8_t)RftData.app_select;
    tlm_pkt->tc_status.tc_acpt_cnt        = REV16(RftData.cmd_acpt_cnt);
    tlm_pkt->tc_status.tc_rjct_cnt        = REV16(RftData.cmd_rjct_cnt);
    tlm_pkt->tc_status.tc_unix_time_us_hi = REV32(RftData.unix_time_us_hi);
    tlm_pkt->tc_status.tc_unix_time_us_lo = REV32(RftData.unix_time_us_lo);

    tlm_pkt->tlm_err_rec.powerup_err_code = REV16(tlm_err->powerup_err_code);
    tlm_pkt->tlm_err_rec.err1.err_code    = REV16(tlm_err->err1.err_code);
    tlm_pkt->tlm_err_rec.err1.err_data    = REV32(tlm_err->err1.err_data);
    tlm_pkt->tlm_err_rec.err2.err_code    = REV16(tlm_err->err2.err_code);
    tlm_pkt->tlm_err_rec.err2.err_data    = REV32(tlm_err->err2.err_data);
    tlm_pkt->tlm_err_rec.err3.err_code    = REV16(tlm_err->err3.err_code);
    tlm_pkt->tlm_err_rec.err3.err_data    = REV32(tlm_err->err3.err_data);
    tlm_pkt->tlm_err_rec.err_cnt          = REV16(tlm_err->err_cnt);

    tlm_pkt->tc_bl_select       = (uint8_t)RftData.bl_select;
    tlm_pkt->tc_pt_select       = (uint8_t)RftData.pt_select;
    tlm_pkt->tc_bl_crc          = REV32(RftData.bl_crc);
    tlm_pkt->tc_app_crc         = REV32(RftData.app_crc);
    tlm_pkt->tc_pt_crc          = REV32(RftData.pt_crc);
    tlm_pkt->tc_rcm_value       = REV32(RftData.rcm_value);
    tlm_pkt->tc_wdt_rst_trig    = (uint8_t)RftData.wdt_rst_trig;
    tlm_pkt->tc_wdt_rst_tog_sel = (uint8_t)RftData.wdt_tog_sel;
    tlm_pkt->tc_wdi_fb          = (uint8_t)RftData.gpio_wdi_fb;
    tlm_pkt->tc_fsw_cpu_util    = (uint8_t)RftData.fsw_cpu_util;
    tlm_pkt->tc_data_mode       = (uint8_t)RftData.data_mode;

    tlm_pkt->tc_tfet            = REV32(F32_TO_BYTES(RftData.tfet));
    tlm_pkt->tc_tchoke          = REV32(F32_TO_BYTES(RftData.tchoke));
    tlm_pkt->tc_tcoil           = REV32(F32_TO_BYTES(RftData.tcoil));
    tlm_pkt->tc_tic             = REV32(F32_TO_BYTES(RftData.tic));
    tlm_pkt->tc_tpl             = REV32(F32_TO_BYTES(RftData.tpl));
    tlm_pkt->tc_tmatch          = REV32(F32_TO_BYTES(RftData.tmatch));
    tlm_pkt->tc_ehvd            = REV32(F32_TO_BYTES(RftData.ehvd));
    tlm_pkt->tc_ihvd            = REV32(F32_TO_BYTES(RftData.ihvd));
    tlm_pkt->tc_tadc0           = REV32(F32_TO_BYTES(RftData.tadc0));

    tlm_pkt->tc_ebus            = REV32(F32_TO_BYTES(RftData.ebus));
    tlm_pkt->tc_e12v            = REV32(F32_TO_BYTES(RftData.e12v));
    tlm_pkt->tc_e5v             = REV32(F32_TO_BYTES(RftData.e5v));
    tlm_pkt->tc_evb             = REV32(F32_TO_BYTES(RftData.evb));
    tlm_pkt->tc_ibus            = REV32(F32_TO_BYTES(RftData.ibus));
    tlm_pkt->tc_i12v            = REV32(F32_TO_BYTES(RftData.i12v));
    tlm_pkt->tc_ivb             = REV32(F32_TO_BYTES(RftData.ivb));
    tlm_pkt->tc_tfb_in          = REV32(F32_TO_BYTES(RftData.tfb_in));
    tlm_pkt->tc_tfb_out         = REV32(F32_TO_BYTES(RftData.tfb_out));
    tlm_pkt->tc_tfram           = REV32(F32_TO_BYTES(RftData.tfram));
    tlm_pkt->tc_tmcu            = REV32(F32_TO_BYTES(RftData.tmcu));

    tlm_pkt->tc_tfet_raw        = REV16(Adc0.raw_dma_ch_arr[IN0]);
    tlm_pkt->tc_tchoke_raw      = REV16(Adc0.raw_dma_ch_arr[IN1]);
    tlm_pkt->tc_tcoil_raw       = REV16(Adc0.raw_dma_ch_arr[IN2]);
    tlm_pkt->tc_tic_raw         = REV16(Adc0.raw_dma_ch_arr[IN3]);
    tlm_pkt->tc_tpl_raw         = REV16(Adc0.raw_dma_ch_arr[IN4]);
    tlm_pkt->tc_tmatch_raw      = REV16(Adc0.raw_dma_ch_arr[IN5]);

    tlm_pkt->tc_ivb_raw         = REV16(mcu_adc_get_ch_raw(MCU_ADC0_CH14));
    tlm_pkt->tc_tfb_in_raw      = REV16(mcu_adc_get_ch_raw(MCU_ADC1_CH6));
    tlm_pkt->tc_tfb_out_raw     = REV16(mcu_adc_get_ch_raw(MCU_ADC1_CH8));
    tlm_pkt->tc_tfram_raw       = REV16(mcu_adc_get_ch_raw(MCU_ADC1_CH23));
    tlm_pkt->tc_tmcu_raw        = REV16(mcu_adc_get_ch_raw(MCU_ADC1_CH24));

    tlm_pkt->tc_pwm_pp_vset_raw_fb = REV16(RftData.pwm_pp_vset_raw_fb);
    tlm_pkt->tc_pp_vset_fb         = REV32(F32_TO_BYTES(RftData.pp_vset_fb));
    tlm_pkt->tc_wav_gen_freq_fb    = REV32(F32_TO_BYTES(RftData.wav_gen_freq_fb));
    tlm_pkt->tc_ecc_sram_1bit_cnt  = REV32(F32_TO_BYTES(RftData.rft_ecc_sram_1bit_cnt));

    tlm_pkt->tc_wf_en_fb        = (uint8_t)RftData.gpio_wf_en_fb;
    tlm_pkt->tc_pp_en_fb        = (uint8_t)RftData.gpio_pp_en_fb;

    tlm_pkt->tc_inv_connected   = (uint8_t)RftData.inverter_connected_flag;
    tlm_pkt->tc_rft_connected   = (uint8_t)RftData.rft_connected_flag;
    tlm_pkt->tc_hp_connected    = (uint8_t)RftData.hp_connected_flag;
    return;
}
