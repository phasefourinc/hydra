/* Copyright (c) 2017-2020 by Phase Four, Inc. All Rights Reserved. Confidential and Proprietary.
 *
 * DataAO.c
 */


#include "DataAO.h"
#include "mcu_types.h"
#include "glados.h"
#include "MaxwellAO.h"
#include "ErrAO.h"
#include "CmdAO.h"
#include "TlmAO.h"
#include "TxAO.h"
#include "cy15.h"
#include "crc.h"
#include "p4_cmd.h"
#include "clank.h"
#include "max_info_driver.h"
#include "MCUflash.h"
#include "default_pt.h"
#include "string.h"

/* Redefine these so it's easier to remember the correct port */
#define LPSPICOM1_FRAM           LPSPICOM1

#define FRAM_REC_MAX_PKT_SIZE    (TLMAO_TLM_MAX_SIZE - 2U)  /* Max size is based on 508B TLM data and 2B CRC, last 2B are extra */
#define FRAM_BANK_MAX_SIZE       0x00040000UL               /* 256KB FRAM Bank Size */
#define FRAM_BANK_MAX_TLM_CNT    (FRAM_BANK_MAX_SIZE / FRAM_REC_MAX_PKT_SIZE)  /* This equates to the last 4B of each bank as unused */
#define FRAM_ADDR_BANK0_START    0xA0000000UL
#define FRAM_ADDR_BANK0_END      (FRAM_ADDR_BANK0_START + ((FRAM_BANK_MAX_SIZE / FRAM_REC_MAX_PKT_SIZE) * FRAM_REC_MAX_PKT_SIZE))
#define FRAM_ADDR_BANK1_START    (FRAM_ADDR_BANK0_START + FRAM_BANK_MAX_SIZE)
#define FRAM_ADDR_BANK1_END      (FRAM_ADDR_BANK1_START + ((FRAM_BANK_MAX_SIZE / FRAM_REC_MAX_PKT_SIZE) * FRAM_REC_MAX_PKT_SIZE))

#define UPDATE_MAX_FRAM_XFER     CY15_BLOCK_SIZE
#define UPDATE_PRELD_ADDR_END    0x00100000UL    /* First 1MB in PFlash can be preloaded */
#define UPDATE_PROG_ADDR_BEGIN   GOLDEN_IMAGE_FLASH_HDR_RECORD
#define UPDATE_PROG_ADDR_END     0x00100000UL    /* This ensures Runtime PT, Error table, etc. located >0x100000 are not corrupted */
#define UPDATE_MAX_LOAD_SIZE     (APP_HDR_MAX_SIZE + (128UL*1024UL))  /* Max size of the header and app image */
#define UPDATE_MAX_STAGE_SIZE    250UL    /* Determined by max Clank payload size minus 4 bytes for addr data */
#define IS_128KB_ALIGNED(x)      ((x & ((128UL*1024UL)-1UL)) == 0UL)


GLADOS_AO_t AO_Data;

/* Active Object timers */
GLADOS_Timer_t Tmr_DataAux;
GLADOS_Timer_t Tmr_DataTimeout;

bool spi1_callback_send_dma_done = false;

/* Active Object private variables */
static GLADOS_Event_t dataao_fifo[TLMAO_FIFO_SIZE];

static uint32_t dao_cmd_pflash_addr = 0UL;
static uint32_t dao_cmd_fram_offset = 0UL;
static uint32_t dao_cmd_length      = 0UL;
static uint32_t dao_cmd_crc         = 0UL;
static uint32_t dao_cmd_data_ptr    = 0UL;

static uint8_t spi1_xfer_done = 0U;
static bool sw_update_hdr_valid(App_Header_Rec_t *img_hdr, uint32_t exp_addr, uint32_t exp_length, uint32_t exp_crc);


void LPSPI1_TxIdleCallback(UNUSED void *driverState, UNUSED spi_event_t event, void *userData)
{
    bool *send_dma_done = (bool *)userData;

    if (*send_dma_done)
    {
        ++spi1_xfer_done;
    }
    return;
}


/* Active Object state definitions */


void DataAO_init(void)
{
    /* Initialize the Active Object with both its Event FIFO and initial state function */
    GLADOS_AO_Init(&AO_Data, DATAAO_PRIORITY, dataao_fifo, DATAAO_FIFO_SIZE, &DataAO_state);

    return;
}


void DataAO_push_interrupts(void)
{
    /* If the callback was called, then decrement semaphore and post event */
    if (spi1_xfer_done > 0U)
    {
        --spi1_xfer_done;
        GLADOS_AO_PushEvtName(&AO_Data, &AO_Data, EVT_DATA_DMA_FRAM_DONE);
    }

    return;
}


void DataAO_state(GLADOS_AO_t *this_ao, GLADOS_Event_t *event)
{
    DEV_ASSERT(this_ao);
    DEV_ASSERT(event);

    status_t status;

    switch(event->name)
    {
        /* GLADOS_ENTRY/GLADOS_EXIT optional event for each state */
        case GLADOS_ENTRY:
            /* Init timers with arbitrary period value, their actual values are
             * configured upon each usage of the timer. */
            GLADOS_Timer_Init(&Tmr_DataAux, 100000UL, EVT_TMR_DATA_AUX_DONE);
            GLADOS_Timer_Subscribe_AO(&Tmr_DataAux, this_ao);

            GLADOS_Timer_Init(&Tmr_DataTimeout, 100000UL, EVT_TMR_DATA_TIMEOUT);
            GLADOS_Timer_Subscribe_AO(&Tmr_DataTimeout, this_ao);

            /* Initialize FRAM */
            status = CY15_FRAM_Init(LPSPICOM1_FRAM, LPSPI_PCS0);
            /* NOTE: Could hit this assert if loaded on wrong hardware */
            DEV_ASSERT(status == STATUS_SUCCESS);
            if (status != STATUS_SUCCESS)
            {
                report_error_uint(&AO_Data, Errors.DataaoFramInitFail, DO_NOT_SEND_TO_SPACECRAFT, 0UL);
            }


            GLADOS_STATE_TRAN_TO_CHILD(&DataAO_state_lounge);
            break;

        case GLADOS_EXIT:
            break;

        case EVT_CMD_UPDATE_PRELOAD:
        case EVT_CMD_UPDATE_STAGE:
        case EVT_CMD_UPDATE_PROGRAM:
            CmdAO_push_nack_w_send_err(this_ao, create_error_uint(Errors.CmdDataModeIsBusy, 0UL));
            break;

        case EVT_CMD_DATA_ABORT:
            /* No need to return ACK since command always succeeds */
        case EVT_MAX_DATA_ABORT:
            GLADOS_STATE_TRAN_TO_CHILD(&DataAO_state_lounge);
            break;

        default:
            /* Top level state, skip undefined Events */
            break;
    }

    return;
}


void DataAO_state_lounge(GLADOS_AO_t *this_ao, GLADOS_Event_t *event)
{
    DEV_ASSERT(this_ao);
    DEV_ASSERT(event);

    switch(event->name)
    {
        /* GLADOS_ENTRY/GLADOS_EXIT optional event for each state */
        case GLADOS_ENTRY:
            RftData.data_mode = DATA_LOUNGE;
            RftData.busy = false;
            break;

        case GLADOS_EXIT:
            break;

        case EVT_MAX_STATE_IN_SW_UPDATE:
            GLADOS_STATE_TRAN_TO_SIBLING(&DataAO_state_update_cmd);
            break;

        default:
            GLADOS_STATE_CHECK_PARENT(&DataAO_state);
            break;
    }

    return;
}


void DataAO_state_update_cmd(GLADOS_AO_t *this_ao, GLADOS_Event_t *event)
{
    DEV_ASSERT(this_ao);
    DEV_ASSERT(event);

    DataAO_PreloadData_t *preload_data;
    DataAO_StageData_t *stage_data;
    DataAO_ProgData_t *prog_data;

    switch(event->name)
    {
        /* GLADOS_ENTRY/GLADOS_EXIT optional event for each state */
        case GLADOS_ENTRY:
            RftData.data_mode = DATA_UP_CMD;

            break;

        case GLADOS_EXIT:
            break;

        case EVT_CMD_UPDATE_PRELOAD:
            DEV_ASSERT(event->has_data);
            DEV_ASSERT(event->data_size == sizeof(DataAO_PreloadData_t));

            preload_data = (DataAO_PreloadData_t *)&event->data[0];

            dao_cmd_pflash_addr = preload_data->pflash_addr;
            dao_cmd_length      = preload_data->length;
            dao_cmd_fram_offset = preload_data->fram_offset;

            /* Validate alignment, length, and region, taking into account possible addr rollover */
            if ((dao_cmd_length >  0UL)                                            &&
                (dao_cmd_length <= UPDATE_MAX_LOAD_SIZE)                           &&
                (dao_cmd_fram_offset < UPDATE_MAX_LOAD_SIZE)                       &&
                ((dao_cmd_fram_offset + dao_cmd_length) <= UPDATE_MAX_LOAD_SIZE)   &&
                (dao_cmd_pflash_addr <  UPDATE_PRELD_ADDR_END)                     &&
                ((dao_cmd_pflash_addr + dao_cmd_length) <= UPDATE_PRELD_ADDR_END))
            {
                /* The FRAM staging occurs in BANK1 */
                dao_cmd_fram_offset += FRAM_ADDR_BANK1_START;

                /* Transition to Preload state */
                GLADOS_STATE_TRAN_TO_SIBLING(&DataAO_state_preload);
            }
            else
            {
                /* Otherwise invalid, send NACK */
                CmdAO_push_nack_w_send_err(this_ao, create_error_uint(Errors.CmdUpdatePreloadInvalidParam, 0UL));
            }
            break;

        case EVT_CMD_UPDATE_STAGE:
            DEV_ASSERT(event->has_data);
            DEV_ASSERT(event->data_size == sizeof(DataAO_StageData_t));

            stage_data = (DataAO_StageData_t *)&event->data[0];

            dao_cmd_fram_offset = stage_data->fram_offset;
            dao_cmd_data_ptr    = stage_data->data_ptr;
            dao_cmd_length      = stage_data->length;

            /* Validate the staging params and that it does not exceed the load boundary */
            if ((dao_cmd_data_ptr != 0UL)                    &&
                (dao_cmd_length > 0UL)                       &&
                (dao_cmd_length <= UPDATE_MAX_STAGE_SIZE)    &&
                (dao_cmd_fram_offset < UPDATE_MAX_LOAD_SIZE) &&
                ((dao_cmd_fram_offset + dao_cmd_length) <= UPDATE_MAX_LOAD_SIZE))
            {
                /* The FRAM staging occurs in BANK1 */
                dao_cmd_fram_offset += FRAM_ADDR_BANK1_START;

                /* Transition to Stage state */
                GLADOS_STATE_TRAN_TO_SIBLING(&DataAO_state_stage);
                /* Only send ACK after the stage has completed */
            }
            else
            {
                /* Otherwise invalid, send NACK */
                CmdAO_push_nack_w_send_err(this_ao, create_error_uint(Errors.CmdUpdateStageInvalidParam, 0UL));
            }
            break;

        case EVT_CMD_UPDATE_PROGRAM:
            DEV_ASSERT(event->has_data);
            DEV_ASSERT(event->data_size == sizeof(DataAO_ProgData_t));

            prog_data = (DataAO_ProgData_t *)&event->data[0];

            dao_cmd_pflash_addr = prog_data->pflash_addr;
            dao_cmd_length      = prog_data->length;
            dao_cmd_crc         = prog_data->crc;

            if ((RftData.using_app_pt) && (RftData.app_select == 1U) &&
                     (dao_cmd_pflash_addr >= PARM_TABLE_APP1) && (dao_cmd_pflash_addr < (PARM_TABLE_APP1 + IMG_MAX_SIZE)))
            {
                /* Do not permit loads to the App PT if it's currently in use */
                CmdAO_push_nack_w_send_err(this_ao, create_error_uint(Errors.CmdUpdateProgramApp1PtInUse, 0UL));
            }
            else if ((RftData.using_app_pt) && (RftData.app_select == 2U) &&
                     (dao_cmd_pflash_addr >= PARM_TABLE_APP2) && (dao_cmd_pflash_addr < (PARM_TABLE_APP2 + IMG_MAX_SIZE)))
            {
                /* Do not permit loads to the App PT if it's currently in use */
                CmdAO_push_nack_w_send_err(this_ao, create_error_uint(Errors.CmdUpdateProgramApp2PtInUse, 0UL));
            }
            /* Validate alignment, length, and region, taking into account possible addr rollover */
            /* Note: CRC32 implementation requires that the length fall on a 4B boundary */
            else if ((IS_128KB_ALIGNED(dao_cmd_pflash_addr))                            &&
                     (dao_cmd_length >  0UL)                                            &&
                     (dao_cmd_length <= UPDATE_MAX_LOAD_SIZE)                           &&
                     ((dao_cmd_length % 4U) == 0UL)                                     &&
                     ((dao_cmd_pflash_addr + dao_cmd_length) >  UPDATE_PROG_ADDR_BEGIN) &&
                     ((dao_cmd_pflash_addr + dao_cmd_length) <= UPDATE_PROG_ADDR_END))
            {
                /* Transition to begin the programming sequence */
                GLADOS_STATE_TRAN_TO_SIBLING(&DataAO_state_fram_validate);
                /* Only send ACK after the staged image has completed */
            }
            else
            {
                /* Otherwise invalid, send NACK */
                CmdAO_push_nack_w_send_err(this_ao, create_error_uint(Errors.CmdUpdateProgramInvalidParam, 0UL));
            }
            break;

        case EVT_TMR_DATA_TIMEOUT:
            /* Ignore any timeouts that are posted after an operation has completed */
            break;

        default:
            GLADOS_STATE_CHECK_PARENT(&DataAO_state);
            break;
    }

    return;
}


void DataAO_state_update_cmd_success(GLADOS_AO_t *this_ao, GLADOS_Event_t *event)
{
    DEV_ASSERT(this_ao);
    DEV_ASSERT(event);

    switch(event->name)
    {
        /* GLADOS_ENTRY/GLADOS_EXIT optional event for each state */
        case GLADOS_ENTRY:
            RftData.data_mode = DATA_UP_CMD_SUCCESS;

            /* Place this here since it's possible that other things
             * could be busy when just sitting idle in SW_UPDATE */
            RftData.busy = false;
            break;

        default:
            GLADOS_STATE_CHECK_PARENT(&DataAO_state_update_cmd);
            break;
    }

    return;
}


void DataAO_state_update_cmd_fail(GLADOS_AO_t *this_ao, GLADOS_Event_t *event)
{
    DEV_ASSERT(this_ao);
    DEV_ASSERT(event);

    switch(event->name)
    {
        /* GLADOS_ENTRY/GLADOS_EXIT optional event for each state */
        case GLADOS_ENTRY:
            RftData.data_mode = DATA_UP_CMD_FAIL;

            /* Place this here since it's possible that other things
             * could be busy when just sitting idle in SW_UPDATE */
            RftData.busy = false;

            /* Note: The error that occurred leading into this state
             * has already been logged */
            break;

        default:
            GLADOS_STATE_CHECK_PARENT(&DataAO_state_update_cmd);
            break;
    }

    return;
}


void DataAO_state_preload(GLADOS_AO_t *this_ao, GLADOS_Event_t *event)
{
    DEV_ASSERT(this_ao);
    DEV_ASSERT(event);

    status_t status;

    switch(event->name)
    {
        /* GLADOS_ENTRY/GLADOS_EXIT optional event for each state */
        case GLADOS_ENTRY:
        case EVT_DATA_DMA_FRAM_DONE:
            GLADOS_Timer_Disable(&Tmr_DataTimeout);

            status = STATUS_SUCCESS;

            /* First time through, dao_cmd_fram_offset and dao_cmd_pflash_addr
             * have already been initialized with the command parameters.
             * Rest of the times through, these params are updated upon completion
             * of each transfer to FRAM. */

            if (dao_cmd_length == 0UL)
            {
                /* The preload transfer to FRAM has completed */
                GLADOS_AO_PUSH(&AO_Cmd, EVT_CMD_ACK_W_SEND);
                GLADOS_STATE_TRAN_TO_SIBLING(&DataAO_state_update_cmd);
            }
            else if (dao_cmd_length < UPDATE_MAX_FRAM_XFER)
            {
                /* Transfer the last data to FRAM */

                /* Initiate the non-blocking DMA transfer for data */
                spi1_callback_send_dma_done = false;
                status  = CY15_FRAM_SetWEL(LPSPICOM1_FRAM, LPSPI_PCS0, true);
                spi1_callback_send_dma_done = true;
                status |= CY15_FRAM_WriteBlock_NonBlocking(LPSPICOM1_FRAM, LPSPI_PCS0, dao_cmd_fram_offset,
                                                           dao_cmd_length, (uint8_t *)dao_cmd_pflash_addr);

                dao_cmd_length       = 0UL;
                dao_cmd_fram_offset += UPDATE_MAX_FRAM_XFER;
                dao_cmd_pflash_addr += UPDATE_MAX_FRAM_XFER;
            }
            else
            {
                /* Transfer the next block of data to FRAM */

                /* Initiate the non-blocking DMA transfer for data */
                spi1_callback_send_dma_done = false;
                status  = CY15_FRAM_SetWEL(LPSPICOM1_FRAM, LPSPI_PCS0, true);
                spi1_callback_send_dma_done = true;
                status |= CY15_FRAM_WriteBlock_NonBlocking(LPSPICOM1_FRAM, LPSPI_PCS0, dao_cmd_fram_offset,
                                                           UPDATE_MAX_FRAM_XFER, (uint8_t *)dao_cmd_pflash_addr);

                dao_cmd_length      -= UPDATE_MAX_FRAM_XFER;
                dao_cmd_fram_offset += UPDATE_MAX_FRAM_XFER;
                dao_cmd_pflash_addr += UPDATE_MAX_FRAM_XFER;
            }

            if (status != STATUS_SUCCESS)
            {
                report_error_uint(&AO_Data, Errors.DataaoFramWriteFail, DO_NOT_SEND_TO_SPACECRAFT, dao_cmd_fram_offset);
            }

            GLADOS_Timer_Set(&Tmr_DataTimeout, PT->tmr_fram_xfer_timeout_us);
            GLADOS_Timer_EnableOnce(&Tmr_DataTimeout);
            break;

        case GLADOS_EXIT:
            /* Halt posting of the EVT_DATA_DMA_FRAM_DONE event and disable timer */
            spi1_callback_send_dma_done = false;
            GLADOS_Timer_Disable(&Tmr_DataTimeout);
            break;

        case EVT_TMR_DATA_TIMEOUT:
            /* Abort the transfer, send NACK, and record error */
            CY15_FRAM_AbortTransfer(LPSPICOM1_FRAM);
            CmdAO_push_nack_w_send_err(this_ao, create_error_uint(Errors.EvtDataOpTimeout, Tmr_DataTimeout.duration_us));
            GLADOS_STATE_TRAN_TO_SIBLING(&DataAO_state_update_cmd);
            break;

        case EVT_MAX_DATA_ABORT:
            /* Handle this event specifically (rather than leaving it to the parent state)
             * since the NACK must be returned. This logs an error as well, which may double
             * up the error but it at least indicates that a command was cancelled due to
             * a FDIR safe-ing mechanism. */
            CmdAO_push_nack_w_send_err(this_ao, create_error_uint(Errors.CmdDataAbortDueToSafe, 0UL));
            CY15_FRAM_AbortTransfer(LPSPICOM1_FRAM);
            GLADOS_STATE_TRAN_TO_SIBLING(&DataAO_state_lounge);
            break;

        default:
            GLADOS_STATE_CHECK_PARENT(&DataAO_state);
            break;
    }

    return;
}


void DataAO_state_stage(GLADOS_AO_t *this_ao, GLADOS_Event_t *event)
{
    DEV_ASSERT(this_ao);
    DEV_ASSERT(event);

    status_t status;

    switch(event->name)
    {
        /* GLADOS_ENTRY/GLADOS_EXIT optional event for each state */
        case GLADOS_ENTRY:
            /* Initiate the non-blocking DMA transfer for data */
            spi1_callback_send_dma_done = false;
            status  = CY15_FRAM_SetWEL(LPSPICOM1_FRAM, LPSPI_PCS0, true);
            spi1_callback_send_dma_done = true;
            status |= CY15_FRAM_WriteBlock_NonBlocking(LPSPICOM1_FRAM, LPSPI_PCS0, dao_cmd_fram_offset,
                                                       dao_cmd_length, (uint8_t *)dao_cmd_data_ptr);

            if (status != STATUS_SUCCESS)
            {
                report_error_uint(&AO_Data, Errors.DataaoFramWriteFail, DO_NOT_SEND_TO_SPACECRAFT, dao_cmd_fram_offset);
            }

            GLADOS_Timer_Set(&Tmr_DataTimeout, PT->tmr_fram_xfer_timeout_us);
            GLADOS_Timer_EnableOnce(&Tmr_DataTimeout);
            break;

        case GLADOS_EXIT:
            /* Halt posting of the EVT_DATA_DMA_FRAM_DONE event and disable timer */
            spi1_callback_send_dma_done = false;
            GLADOS_Timer_Disable(&Tmr_DataTimeout);
            break;

        case EVT_DATA_DMA_FRAM_DONE:
            GLADOS_Timer_Disable(&Tmr_DataTimeout);
            dao_cmd_length = 0UL;
            GLADOS_AO_PUSH(&AO_Cmd, EVT_CMD_ACK_W_SEND);
            GLADOS_STATE_TRAN_TO_SIBLING(&DataAO_state_update_cmd);
            break;

        case EVT_TMR_DATA_TIMEOUT:
            /* Abort the transfer, send NACK */
            CY15_FRAM_AbortTransfer(LPSPICOM1_FRAM);
            CmdAO_push_nack_w_send_err(this_ao, create_error_uint(Errors.EvtDataOpTimeout, Tmr_DataTimeout.duration_us));
            GLADOS_STATE_TRAN_TO_SIBLING(&DataAO_state_update_cmd);
            break;

        case EVT_MAX_DATA_ABORT:
            /* Handle this event specifically (rather than leaving it to the parent state)
             * since the NACK must be returned. This logs an error as well, which may double
             * up the error but it at least indicates that a command was cancelled due to
             * a FDIR safe-ing mechanism. */
            CmdAO_push_nack_w_send_err(this_ao, create_error_uint(Errors.CmdDataAbortDueToSafe, 0UL));
            CY15_FRAM_AbortTransfer(LPSPICOM1_FRAM);
            GLADOS_STATE_TRAN_TO_SIBLING(&DataAO_state_lounge);
            break;

        default:
            GLADOS_STATE_CHECK_PARENT(&DataAO_state);
            break;
    }

    return;
}


void DataAO_state_fram_validate(GLADOS_AO_t *this_ao, GLADOS_Event_t *event)
{
    DEV_ASSERT(this_ao);
    DEV_ASSERT(event);

    static uint32_t curr_crc;
    static uint8_t *curr_data_ptr;
    static uint32_t curr_length;
    static uint32_t curr_fram_offset;
    static bool has_sent_ack_mutex;

    status_t status;
    App_Header_Rec_t *hdr;

    switch(event->name)
    {
        /* GLADOS_ENTRY/GLADOS_EXIT optional event for each state */
        case GLADOS_ENTRY:
            has_sent_ack_mutex = false;

            /* Restart the CRC */
            curr_crc = 0xFFFFFFFFUL;

            /* Use another variable to hold the current length since the
             * dao_cmd_length should remain static to retain the initial
             * program command parameters */
            curr_length = dao_cmd_length;

            /* The start of staging begins at FRAM BANK1, so start there */
            curr_fram_offset = FRAM_ADDR_BANK1_START;

            /*  Start by reading the header. Initiate the non-blocking DMA transfer for data. */
            spi1_callback_send_dma_done = true;
            status = CY15_FRAM_ReadBlock_NonBlocking(LPSPICOM1_FRAM, LPSPI_PCS0, curr_fram_offset,
                                                     APP_HDR_MAX_SIZE, &curr_data_ptr);
            if (status != STATUS_SUCCESS)
            {
                report_error_uint(&AO_Data, Errors.DataaoFramReadFail, DO_NOT_SEND_TO_SPACECRAFT, curr_fram_offset);
            }

            GLADOS_Timer_Set(&Tmr_DataTimeout, PT->tmr_fram_xfer_timeout_us);
            GLADOS_Timer_EnableOnce(&Tmr_DataTimeout);
            break;

        case GLADOS_EXIT:
            /* Halt posting of the EVT_DATA_DMA_FRAM_DONE event and disable timer */
            spi1_callback_send_dma_done = false;
            GLADOS_Timer_Disable(&Tmr_DataTimeout);
            break;

        case EVT_DATA_DMA_FRAM_DONE:
            GLADOS_Timer_Disable(&Tmr_DataTimeout);

            /* If it's the first read, then it contains the flash header */
            if (curr_fram_offset == FRAM_ADDR_BANK1_START)
            {
                /* Grab the CRC and length within the header and validate that it matches what is expected */
                hdr = (App_Header_Rec_t *)curr_data_ptr;

                if (!sw_update_hdr_valid(hdr, dao_cmd_pflash_addr, dao_cmd_length, dao_cmd_crc))
                {
                    /* If CRC or length mismatch what is expected, then fail out */
                    CmdAO_push_nack_w_send_err(this_ao, create_error_uint(Errors.CmdUpdateProgramStageInvalid, hdr->app_record.img_crc));
                    has_sent_ack_mutex = true;
                    GLADOS_STATE_TRAN_TO_SIBLING(&DataAO_state_update_cmd);
                    /* Note: Do not go to the FAIL state since a NACK is returned,
                     * indicating that the command was rejected. Technically the
                     * SW update only starts after successfully validating that the
                     * header CRC and length match */
                }
                else
                {
                    /* Success! Transition to the next update step */
                    GLADOS_AO_PUSH(&AO_Cmd, EVT_CMD_ACK_W_SEND);
                    has_sent_ack_mutex = true;
                    RftData.busy = true;

                    /* The first read was of the header, only increment by that */
                    curr_length      -= APP_HDR_MAX_SIZE;
                    curr_fram_offset += APP_HDR_MAX_SIZE;

                    /* CRC is not performed on the header, read next block from FRAM.
                     * Note: Reading beyond the last address of FRAM rolls over to the beginning,
                     * which works for this implementation since all reads from FRAM can be read
                     * using the same block size without special consideration for the last read,
                     * which may only be a couple of bytes. The dao_cmd_length is used in a way
                     * that will not include this extra rollover data that may have been read. */
                    spi1_callback_send_dma_done = true;
                    status = CY15_FRAM_ReadBlock_NonBlocking(LPSPICOM1_FRAM, LPSPI_PCS0, curr_fram_offset,
                                                             UPDATE_MAX_FRAM_XFER, &curr_data_ptr);
                    if (status != STATUS_SUCCESS)
                    {
                        /* This is an internal error may be useful. This failure will ultimately
                         * lead to an failed CRC computation and SC-visible error logged. */
                        report_error_uint(&AO_Data, Errors.DataaoFramReadFail, DO_NOT_SEND_TO_SPACECRAFT, curr_fram_offset);
                    }

                    GLADOS_Timer_Set(&Tmr_DataTimeout, PT->tmr_fram_xfer_timeout_us);
                    GLADOS_Timer_EnableOnce(&Tmr_DataTimeout);
                }
            }
            /* If the previous read was the last one */
            else if (curr_length < UPDATE_MAX_FRAM_XFER)
            {
                /* Only accumulate the CRC for the last of the data. This function
                 * handles the case when curr_length is zero. */
                curr_crc = crc32_compute(curr_crc, curr_data_ptr, curr_length, true);

                /* Check the CRC and transition to the appropriate state */
                if (curr_crc != dao_cmd_crc)
                {
                    /* Command has already been ACKd, now it's on the SW to report errors to the SC */
                    report_error_uint(&AO_Data, Errors.DataaoUpProgStagedCrcFail, SEND_TO_SPACECRAFT, curr_crc);

                    /* Failed CRC, return to base camp */
                    GLADOS_STATE_TRAN_TO_SIBLING(&DataAO_state_update_cmd);
                    GLADOS_STATE_TRAN_TO_CHILD(&DataAO_state_update_cmd_fail);
                }
                else
                {
                    GLADOS_STATE_TRAN_TO_SIBLING(&DataAO_state_pflash_erase);
                }
            }
            else
            {
                /* Continue to accumulate the CRC for the full block read from FRAM */
                curr_crc = crc32_compute(curr_crc, curr_data_ptr, UPDATE_MAX_FRAM_XFER, false);

                curr_length      -= UPDATE_MAX_FRAM_XFER;
                curr_fram_offset += UPDATE_MAX_FRAM_XFER;

                /* Initiate the next full block read from FRAM
                 * Note: Reading beyond the last address of FRAM rolls over to the beginning,
                 * which works for this implementation since all reads from FRAM can be read
                 * using the same block size without special consideration for the last read,
                 * which may only be a couple of bytes. The dao_cmd_length is used in a way
                 * that will not include this extra rollover data that may have been read. */
                spi1_callback_send_dma_done = true;
                status = CY15_FRAM_ReadBlock_NonBlocking(LPSPICOM1_FRAM, LPSPI_PCS0, curr_fram_offset,
                                                         UPDATE_MAX_FRAM_XFER, &curr_data_ptr);
                if (status != STATUS_SUCCESS)
                {
                    /* This is an internal error may be useful. This failure will ultimately
                     * lead to an failed CRC computation and SC-visible error logged. */
                    report_error_uint(&AO_Data, Errors.DataaoFramReadFail, DO_NOT_SEND_TO_SPACECRAFT, curr_fram_offset);
                }

                GLADOS_Timer_Set(&Tmr_DataTimeout, PT->tmr_fram_xfer_timeout_us);
                GLADOS_Timer_EnableOnce(&Tmr_DataTimeout);
            }
            break;

        case EVT_TMR_DATA_TIMEOUT:
            /* Note: There is a race condition where it's possible that the DMA transfer
             * completes just before this event is pushed to the AO. Which would initiate
             * the next transfer but the very next event will be this timeout, causing the
             * exiting of this state prematurely. This is not ideal, but still okay since the
             * DMA transfer should never be anywhere near the timeout time. And if it does,
             * then it deserves to be aborted.
             *
             * On that note, this race condition could potentially affect other states that
             * might be transitioned to as a result of the last DMA transfer completing. The
             * current transitions from this state to others are accounted for (i.e. to
             * update_cmd and pflash_erase) and don't use the EVT_TMR_DATA_TIMEOUT event.
             * However any new states that may be transitioned to from this state must take
             * this into account. However that is true for any AO design that uses a timeout
             * paradigm. But just restating here for clarity. */

            /* Abort transfer, send NACK, and record error */
            CY15_FRAM_AbortTransfer(LPSPICOM1_FRAM);

            /* This state handles sending the ACK/NACK to the SC, however don't double-send
             * if it has already been done, record an error otherwise. */
            if (!has_sent_ack_mutex)
            {
                CmdAO_push_nack_w_send_err(this_ao, create_error_uint(Errors.DataaoUpProgStagedCrcTimeout, curr_fram_offset));
                has_sent_ack_mutex = true;
            }
            else
            {
                /* This is an internal error may be useful. This failure will ultimately
                 * lead to an failed CRC computation and SC-visible error logged. */
                report_error_uint(&AO_Data, Errors.DataaoUpProgStagedCrcTimeout, SEND_TO_SPACECRAFT, curr_fram_offset);
            }

            GLADOS_STATE_TRAN_TO_SIBLING(&DataAO_state_update_cmd);
            GLADOS_STATE_TRAN_TO_CHILD(&DataAO_state_update_cmd_fail);
            break;

        case EVT_MAX_DATA_ABORT:
            /* Handle this event specifically (rather than leaving it to the parent state)
             * since the NACK may need to be sent. This logs an error as well, which may double
             * up the error but it at least indicates that a command was cancelled due to
             * a FDIR safe-ing mechanism. */
            CY15_FRAM_AbortTransfer(LPSPICOM1_FRAM);

            /* This state handles sending the ACK/NACK to the SC, however don't double-send
             * if it has already been done, record an error otherwise. */
            if (!has_sent_ack_mutex)
            {
                CmdAO_push_nack_w_send_err(this_ao, create_error_uint(Errors.CmdDataAbortDueToSafe, 0UL));
                has_sent_ack_mutex = true;
            }
            else
            {
                report_error_uint(&AO_Data, Errors.CmdDataAbortDueToSafe, SEND_TO_SPACECRAFT, 0UL);
            }

            GLADOS_STATE_TRAN_TO_SIBLING(&DataAO_state_lounge);
            break;

        default:
            GLADOS_STATE_CHECK_PARENT(&DataAO_state);
            break;
    }

    return;
}


void DataAO_state_pflash_erase(GLADOS_AO_t *this_ao, GLADOS_Event_t *event)
{
    DEV_ASSERT(this_ao);
    DEV_ASSERT(event);

    static uint32_t curr_pflash_addr;
    static uint32_t end_pflash_addr;

    status_t status;

    switch(event->name)
    {
        /* GLADOS_ENTRY/GLADOS_EXIT optional event for each state */
        case GLADOS_ENTRY:
            RftData.data_mode = DATA_UP_FLSH_CLR;

            /* Note: At this stage, the address and length parameters have
             * already been validated for appropriate ranges and byte
             * boundaries so assert checking ones pertinent to this state
             * is sufficient. */
            DEV_ASSERT((dao_cmd_pflash_addr & (ERASE_SECTOR_SIZE - 1)) == 0UL);

            /* Use another variable to hold the current address since the
             * dao_cmd_pflash_addr should remain static to retain the initial
             * program command parameters */
            curr_pflash_addr = dao_cmd_pflash_addr;

            /* Get end address, rounding up to the next PFlash sector */
            end_pflash_addr  = dao_cmd_pflash_addr + dao_cmd_length + (ERASE_SECTOR_SIZE - 1UL);
            end_pflash_addr &= ~(ERASE_SECTOR_SIZE - 1UL);

            /* Manually push the first event to self to get into the erasing, which
             * removes the need to handle the same actions in multiple places */
            GLADOS_AO_PUSH(&AO_Data, EVT_TMR_DATA_AUX_DONE);
            break;

        case GLADOS_EXIT:
            GLADOS_Timer_Disable(&Tmr_DataAux);
            break;

        case EVT_TMR_DATA_AUX_DONE:
            /* Check if all sectors of the pflash region have been erased */
            if (curr_pflash_addr >= end_pflash_addr)
            {
                /* Once complete, move on to programming the image */
                GLADOS_STATE_TRAN_TO_SIBLING(&DataAO_state_pflash_program);
            }
            else
            {
                /* Erase the next PFlash sector
                 * Note: There are some instances where the erase operation returns busy
                 * immediately when there is nothing left to erase. If there is something
                 * to erase, then it returns STATUS_SUCCESS. Given this behavior, we only
                 * STATUS_ERROR is the only status left, and even that is just rechecking
                 * the inputs into the function, which already happened upon entry to start
                 * the program. For this reason, check STATUS_ERROR for development only. */
                status = erase_flash_sector((uint32_t )curr_pflash_addr, ERASE_SECTOR_SIZE);
                DEV_ASSERT(status != STATUS_ERROR);
                (void)status;  /* Avoids compiler warning */

                curr_pflash_addr += ERASE_SECTOR_SIZE;

                /* Wait a fixed set of time for the PFlash sector to erase */
                GLADOS_Timer_Set(&Tmr_DataAux, PT->tmr_pflash_sector_erase_us);
                GLADOS_Timer_EnableOnce(&Tmr_DataAux);
            }
            break;

        default:
            GLADOS_STATE_CHECK_PARENT(&DataAO_state);
            break;
    }

    return;
}


void DataAO_state_pflash_program(GLADOS_AO_t *this_ao, GLADOS_Event_t *event)
{
    DEV_ASSERT(this_ao);
    DEV_ASSERT(event);

    static uint32_t curr_prog_pflash_addr;
    static uint32_t curr_prog_length;
    static uint32_t curr_prog_fram_offset;
    static uint8_t *curr_prog_data_ptr;

    status_t status;

    switch(event->name)
    {
        /* GLADOS_ENTRY/GLADOS_EXIT optional event for each state */
        case GLADOS_ENTRY:
            RftData.data_mode = DATA_UP_FLSH_PRG;

            curr_prog_pflash_addr = dao_cmd_pflash_addr;
            curr_prog_length      = dao_cmd_length;
            curr_prog_fram_offset = FRAM_ADDR_BANK1_START;

            /* Manually push the first event to self to get into the FRAM read, which
             * removes the need to handle the same actions in multiple places */
            GLADOS_AO_PUSH(&AO_Data, EVT_TMR_DATA_AUX_DONE);
            break;

        case GLADOS_EXIT:
            spi1_callback_send_dma_done = false;
            GLADOS_Timer_Disable(&Tmr_DataTimeout);
            GLADOS_Timer_Disable(&Tmr_DataAux);
            break;

        case EVT_TMR_DATA_TIMEOUT:
            CY15_FRAM_AbortTransfer(LPSPICOM1_FRAM);
            report_error_uint(&AO_Data, Errors.DataaoUpProgFlashTimeout, SEND_TO_SPACECRAFT, curr_prog_fram_offset);
            GLADOS_STATE_TRAN_TO_SIBLING(&DataAO_state_update_cmd);
            GLADOS_STATE_TRAN_TO_CHILD(&DataAO_state_update_cmd_fail);
            break;

        case EVT_TMR_DATA_AUX_DONE:
            if (curr_prog_length == 0UL)
            {
                /* Programming has completed, go to CRC image validation step */
                GLADOS_STATE_TRAN_TO_SIBLING(&DataAO_state_pflash_validate);
            }
            else
            {
                /* Initiate the next full block read from FRAM
                 * Note: Reading beyond the last address of FRAM rolls over to the beginning,
                 * which works for this implementation since all reads from FRAM can be read
                 * using the same block size without special consideration for the last read,
                 * which may only be a couple of bytes. The dao_cmd_length is used in a way
                 * that will not include this extra rollover data that may have been read. */
                spi1_callback_send_dma_done = true;
                status = CY15_FRAM_ReadBlock_NonBlocking(LPSPICOM1_FRAM, LPSPI_PCS0, curr_prog_fram_offset,
                                                         UPDATE_MAX_FRAM_XFER, &curr_prog_data_ptr);
                if (status != STATUS_SUCCESS)
                {
                    /* This is an internal error may be useful. This failure will ultimately
                     * lead to a timeout and SC-visible error logged. */
                    report_error_uint(&AO_Data, Errors.DataaoFramReadFail, DO_NOT_SEND_TO_SPACECRAFT, curr_prog_fram_offset);
                }

                GLADOS_Timer_Set(&Tmr_DataTimeout, PT->tmr_fram_xfer_timeout_us);
                GLADOS_Timer_EnableOnce(&Tmr_DataTimeout);
            }
            break;

        case EVT_DATA_DMA_FRAM_DONE:
            GLADOS_Timer_Disable(&Tmr_DataTimeout);
            if (curr_prog_length <= UPDATE_MAX_FRAM_XFER)
            {
                /* Round up to the nearest 8 bytes since programming flash must occur in
                 * multiples of 8 bytes. Writing unused junk data at the end of the image
                 * never hurt nobody. */
                curr_prog_length += 0x7;
                curr_prog_length &= ~0x7;

                /* This is the last program
                 * Note: Programming PFlash is a blocking call. No need to check if program
                 * was successful since this is done later in the final CRC check. */
                status = write_flash_ao(&AO_Data, (uint32_t )curr_prog_pflash_addr, curr_prog_length, curr_prog_data_ptr);

                curr_prog_pflash_addr += curr_prog_length;
                curr_prog_fram_offset += curr_prog_length;
                curr_prog_length       = 0UL;
            }
            else
            {
                /* This is the last program
                 * Note: Programming PFlash is a blocking call. No need to check if program
                 * was successful since this is done later in the final CRC check. */
                status = write_flash_ao(&AO_Data, (uint32_t )curr_prog_pflash_addr, UPDATE_MAX_FRAM_XFER, curr_prog_data_ptr);

                curr_prog_pflash_addr += UPDATE_MAX_FRAM_XFER;
                curr_prog_fram_offset += UPDATE_MAX_FRAM_XFER;
                curr_prog_length      -= UPDATE_MAX_FRAM_XFER;
            }

            if (status != STATUS_SUCCESS)
            {
                report_error_uint(&AO_Data, Errors.DataaoUpProgFlashFail, SEND_TO_SPACECRAFT, curr_prog_pflash_addr);
                GLADOS_STATE_TRAN_TO_SIBLING(&DataAO_state_update_cmd);
                GLADOS_STATE_TRAN_TO_CHILD(&DataAO_state_update_cmd_fail);
            }
            else
            {
                /* This is delay out of courtesy, allowing other AOs to execute */
                GLADOS_Timer_Set(&Tmr_DataAux, PT->tmr_pflash_prog_delay_us);
                GLADOS_Timer_EnableOnce(&Tmr_DataAux);
            }
            break;

        default:
            GLADOS_STATE_CHECK_PARENT(&DataAO_state);
            break;
    }

    return;
}


void DataAO_state_pflash_validate(GLADOS_AO_t *this_ao, GLADOS_Event_t *event)
{
    DEV_ASSERT(this_ao);
    DEV_ASSERT(event);

    static uint32_t curr_crc;
    static uint32_t crc_length1;
    static uint32_t crc_length2;

    uint32_t curr_pflash_addr;
    App_Header_Rec_t *hdr;

    switch(event->name)
    {
        /* GLADOS_ENTRY/GLADOS_EXIT optional event for each state */
        case GLADOS_ENTRY:
            /* Expect to spend 10-20ms in this state. */

            /* Note: At this stage, the address and length parameters have
             * already been validated for appropriate ranges and byte
             * boundaries so assert checking ones pertinent to this state
             * is sufficient. */
            /* Validate length since we don't want to wait forever for the CRC calc to finish */
            DEV_ASSERT(dao_cmd_length > APP_HDR_MAX_SIZE);
            DEV_ASSERT(dao_cmd_length <= UPDATE_MAX_LOAD_SIZE);
            DEV_ASSERT((dao_cmd_length % 4U) == 0U);

            /* Make the CRC for the first section roughly half. Since the CRC32 implementation
             * requires the length to be divisible by four, we round up to the nearest number
             * divisible by 4. */
            crc_length1 = (dao_cmd_length - APP_HDR_MAX_SIZE) / 2UL;
            crc_length1 = (crc_length1 + 3UL) & (~0x3UL);
            crc_length2 = (dao_cmd_length - APP_HDR_MAX_SIZE) - crc_length1;

            /* CRC does not include the Application header, so skip */
            curr_pflash_addr = dao_cmd_pflash_addr + APP_HDR_MAX_SIZE;

            curr_crc = crc32_compute(0xFFFFFFFFUL, (uint8_t *)curr_pflash_addr, crc_length1, false);

            /* This is delay out of courtesy, allowing other AOs to execute */
            GLADOS_Timer_Set(&Tmr_DataAux, PT->tmr_pflash_prog_delay_us);
            GLADOS_Timer_EnableOnce(&Tmr_DataAux);
            break;

        case GLADOS_EXIT:
            break;

        case EVT_TMR_DATA_AUX_DONE:
            curr_pflash_addr = dao_cmd_pflash_addr + APP_HDR_MAX_SIZE + crc_length1;

            curr_crc = crc32_compute(curr_crc, (uint8_t *)curr_pflash_addr, crc_length2, true);

            /* Grab the CRC and length within the header and validate that it matches what is expected */
            hdr = (App_Header_Rec_t *)dao_cmd_pflash_addr;

            /* Validate the the CRC matches what was command, as well as the header info */
            if ((curr_crc                   != dao_cmd_crc) ||
                (hdr->app_record.img_crc    != dao_cmd_crc) ||
                (hdr->app_record.img_length != (dao_cmd_length - APP_HDR_MAX_SIZE)))
            {
                /* Doh! After all that, the upload failed */
                report_error_uint(&AO_Data, Errors.DataaoUpProgFinalCrcFail, SEND_TO_SPACECRAFT, curr_crc);
                GLADOS_STATE_TRAN_TO_SIBLING(&DataAO_state_update_cmd);
                GLADOS_STATE_TRAN_TO_CHILD(&DataAO_state_update_cmd_fail);
            }
            else
            {
                /* Upload has completed! Wow it really worked */
                GLADOS_STATE_TRAN_TO_SIBLING(&DataAO_state_update_cmd);
                GLADOS_STATE_TRAN_TO_CHILD(&DataAO_state_update_cmd_success);
            }

            break;

        default:
            GLADOS_STATE_CHECK_PARENT(&DataAO_state);
            break;
    }

    return;
}

static bool sw_update_hdr_valid(App_Header_Rec_t *img_hdr, uint32_t exp_addr, uint32_t exp_length, uint32_t exp_crc)
{
    enum Constants { STR_MATCH = 0 };

    bool hdr_valid = false;

    /* Validate image CRC and length matches the expected */
    if ((img_hdr->app_record.img_crc    != exp_crc) ||
        (img_hdr->app_record.img_length != (exp_length - APP_HDR_MAX_SIZE)))
    {
        hdr_valid = false;
    }

    /* Validate the load is intended for the product, e.g. Max-1 vs Max-2 */
    else if (STR_MATCH != memcmp(&img_hdr->app_record.product_version_type[0],
                                 &app_hdr_rec->app_record.product_version_type[0],
                                 sizeof(img_hdr->app_record.product_version_type)))
    {
        hdr_valid = false;
    }

    /* Validate the load is intended for the hardware, e.g. Prop Ctrl vs Thr Ctrl */
    else if (STR_MATCH != memcmp(&img_hdr->app_record.hardware_version_type[0],
                                 &app_hdr_rec->app_record.hardware_version_type[0],
                                 sizeof(img_hdr->app_record.hardware_version_type)))
    {
        hdr_valid = false;
    }

    /* If load is to an Executable, it must match one of the proper addresses */
    else if (img_hdr->app_record.img_type == APP_IMG_TYPE_ID)
    {
        /* Only check for Golden Image if loading is enabled */
        if ((PT->pt_end == GOLD_IMG_LOAD_PWD) &&
            (exp_addr   == GOLDEN_IMAGE_FLASH_HDR_RECORD))
        {
            hdr_valid = true;
        }
        else if ((exp_addr == APP_1_FLASH_HDR_RECORD) ||
                 (exp_addr == APP_2_FLASH_HDR_RECORD))
        {
            hdr_valid = true;
        }
    }

    /* If load is to a Parameter table, it must match one of the proper addresses */
    else if (img_hdr->app_record.img_type == PARAM_TABLE_IMG_TYPE_ID)
    {
        /* Only check for Golden Image if loading is enabled */
        if ((PT->pt_end == GOLD_IMG_LOAD_PWD) &&
            (exp_addr   == PARM_TABLE_GOLD))
        {
            hdr_valid = true;
        }
        else if ((exp_addr == PARM_TABLE_APP1) ||
                 (exp_addr == PARM_TABLE_APP2))
        {
            hdr_valid = true;
        }
    }

    return hdr_valid;
}
