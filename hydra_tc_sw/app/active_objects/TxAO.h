/* Copyright (c) 2018-2019 by Phase Four, Inc. All Rights Reserved. Confidential and Proprietary.
 *
 * HostTxMgr.h
 */

#ifndef TXAO_H_
#define TXAO_H_

#include "mcu_types.h"
#include "glados.h"

#define TXAO_PRIORITY        7UL
#define TXAO_FIFO_SIZE       6UL
#define TXAO_XFER_FIFO_SIZE  8UL

#define TXAO_CLANK_MAX_XFER_SIZE  254U

/* Each of these must be unique values for the entire system */
enum TxAO_event_names
{
    EVT_UART0_TX_DONE = (TXAO_PRIORITY << 16U),
    EVT_UART1_TX_DONE,
    EVT_TX_SEND_MSG,
    EVT_TX_SEND_OS_PRINT,
    EVT_TX_INIT_XFER,
    EVT_TX_COMPLETE,
    EVT_TX_TIMEOUT,
    EVT_TX_ERR,
    EVT_TX_TLM_DONE,
    EVT_TX_DUMP_NEXT,
};

#define SC_PORT     0U
#define GSE_PORT    1U

enum TxAO_TxTypes
{
    TX_ACK       = 0x01,
    TX_TLM       = 0x02,
    TX_DUMP      = 0x03,
    TX_OS_PRINT  = 0x04,
    TX_DATA      = 0x05,
    TX_RAW       = 0x06,
    TX_DATA_WACK = 0x07,

    TX_SC_ACK       = 0x01,
    TX_SC_TLM       = 0x02,
    TX_SC_DUMP      = 0x03,
    TX_SC_OS_PRINT  = 0x04,
    TX_SC_DATA      = 0x05,
    TX_SC_RAW       = 0x06,
    TX_SC_DATA_WACK = 0x07,

    TX_GSE_ACK      = 0x11,
    TX_GSE_TLM      = 0x12,
    TX_GSE_DUMP     = 0x13,
    TX_GSE_OS_PRINT = 0x14,
    TX_GSE_DATA     = 0x15,
    TX_GSE_RAW      = 0x16,
    TX_GSE_DATA_WACK= 0x17,
};

#define TX_TYPE_PORT(x)    ((x & 0x10) >> 4U)

typedef enum
{
    PORT_PCU =  0x00,
    PORT_SC  =  0x00,
    PORT_GSE =  0x10
} TxPort_t;

typedef struct TxAO_TxParams_tag
{
    uint8_t dest_id;
    uint8_t  tx_type;
    uint8_t  seq_id;
    uint16_t msg_id;
    uint16_t ack_status;
    uint32_t data_addr;
    uint16_t length;
} BYTE_PACKED TxAO_TxParams_t;


/* Determines the destination address to use for OS prints */
#define OSPRINT_GSE_DEST_PORT    0xEU

extern GLADOS_AO_t AO_Tx;
extern GLADOS_Timer_t Tmr_TxTimeout;

void TxAO_init(void);
void TxAO_push_interrupts(void);
void TxAO_state(GLADOS_AO_t *this_ao, GLADOS_Event_t *event);
void TxAO_state_tx_idle(GLADOS_AO_t *this_ao, GLADOS_Event_t *event);
void TxAO_state_tx_busy(GLADOS_AO_t *this_ao, GLADOS_Event_t *event);
void TxAO_state_tx_busy_xfer(GLADOS_AO_t *this_ao, GLADOS_Event_t *event);
void TxAO_state_tx_busy_xfer_tlm(GLADOS_AO_t *this_ao, GLADOS_Event_t *event);
void TxAO_state_tx_busy_xfer_dump(GLADOS_AO_t *this_ao, GLADOS_Event_t *event);
void TxAO_state_tx_busy_ack(GLADOS_AO_t *this_ao, GLADOS_Event_t *event);
void TxAO_state_tx_busy_debug(GLADOS_AO_t *this_ao, GLADOS_Event_t *event);
void TxAO_state_tx_busy_os_print(GLADOS_AO_t *this_ao, GLADOS_Event_t *event);
void TxAO_state_tx_busy_data(GLADOS_AO_t *this_ao, GLADOS_Event_t *event);
void TxAO_state_tx_busy_raw(GLADOS_AO_t *this_ao, GLADOS_Event_t *event);
void TxAO_state_tx_busy_data_wack(GLADOS_AO_t *this_ao, GLADOS_Event_t *event);

void TxAO_SendMsg_Tlm(GLADOS_AO_t *src_ao, TxPort_t port, uint8_t dest_id, uint8_t seq_id,
                                        uint16_t msg_id, uint32_t data_addr, uint16_t length);

void TxAO_SendMsg_Dump(GLADOS_AO_t *src_ao, TxPort_t port, uint8_t dest_id, uint8_t seq_id,
                                        uint16_t msg_id, uint32_t data_addr, uint16_t length);

void TxAO_SendMsg_Ack(GLADOS_AO_t *src_ao, TxPort_t port, uint8_t dest_id,
                                        uint8_t seq_id, uint16_t msg_id, uint16_t ack_status);

/* Use this to send arbitrary data to be formatted into a clank payload */
void TxAO_SendMsg_Data(GLADOS_AO_t *src_ao, TxPort_t port, uint8_t dest_id, uint8_t seq_id,
                                        uint16_t msg_id, uint32_t data_addr, uint16_t length);

/* Use this to send raw data over the specified port */
void TxAO_SendMsg_Raw(GLADOS_AO_t *src_ao, TxPort_t port, uint32_t data_addr, uint16_t length);

/* Use this to send arbitrary data to be formatted into a clank payload BUT
 * with a 2B ACK status inserted at the front of the data. */
void TxAO_SendMsg_Data_wAck(GLADOS_AO_t *src_ao, TxPort_t port, uint8_t dest_id, uint8_t seq_id,
                                        uint16_t msg_id, uint32_t data_addr, uint16_t length);

void TxAO_SendMsg_OSPrint(void);



/*!
 * @brief  Performs a blocking transmission of a null-terminated string over the Debug UART port (GSE).
 *         This function should be called through the macro, which supports automatic removal
 *         of the print statement upon definition of the GLADOS_NDEBUG compiler flag.
 *
 *         NOTE: THIS FUNCTION IS INTEDED FOR DEVELOPMENT USE ONLY.  Do not use for flight as there
 *         is potentially a permanent blocking call.
 *
 * @pre    GSE UART Port has been configured
 *
 * @param str    Null-terminated string
 * @return       SDK UART status (optional)
 */
#ifndef MAXIMUS_NDEBUG
    #define TXAO_PRINT(str)    TxAO_SendMsg_Debug_Blocking(str)
#else
    #define TXAO_PRINT(str)    ((void)0)
#endif
void TxAO_SendMsg_Debug_Blocking(const char *str);

#endif /* HOSTTXMGR_CODE_HOSTTXMGR_H_ */
