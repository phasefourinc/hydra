/* Copyright (c) 2018-2019 by Phase Four, Inc. All Rights Reserved. Confidential and Proprietary.
 *
 * TxAO.c
 */

#include "TxAO.h"
#include "mcu_types.h"
#include "glados.h"
#include "clank.h"
#include "lpuart_driver.h"
#include "ErrAO.h"
#include "TlmAO.h"
#include "DataAO.h"
#include "p4_tlm.h"
#include "rft_data.h"
#include "default_pt.h"
#include "max_defs.h"
#include "string.h"

/* Notes:
 *  - Most errors that occur within this AO should be sent to the spacecraft
 *    since all transmissions are potentially customer facing
 */


/* Active Object private variables */

static GLADOS_Event_t txao_fifo[TXAO_FIFO_SIZE];
static GLADOS_Event_t txao_xfer_fifo_buf[TXAO_XFER_FIFO_SIZE] = { {0} };
static GLADOS_Fifo_t txao_xfer_fifo = { 0 };
static GLADOS_Event_t txao_init_xfer_evt;
static GLADOS_Event_t txao_complete_evt;
static TxAO_TxParams_t *txao_xfer_params_ptr;                  /* Points to the current transfer parameters */

static uint8_t txao_clank_buf[CLANK_MAX_MSG_SIZE] = { 0 };
static uint16_t txao_clank_buf_size = 0U;

/* Active Object public interface */

GLADOS_AO_t AO_Tx;

/* Active Object timers */
GLADOS_Timer_t Tmr_TxTimeout;

/* Semaphore used to signal to GLADOS to signal HostTxMgr that the
 * uart transmission has completed. */
static uint8_t uart0_tx_done = 0U;
static uint8_t uart0_dma_err_sem = 0U;


static uint16_t TxAO_FormatClank_Norm(uint8_t *buf, uint16_t *buf_size, TxAO_TxParams_t *tx_params);
static void TxAO_FormatClank_Ack(uint8_t *buf, uint16_t *buf_size, TxAO_TxParams_t *tx_params);
static uint16_t TxAO_FormatClank_DataWAck(uint8_t *buf, uint16_t *buf_size, TxAO_TxParams_t *tx_params);
static uint32_t TxAO_GetPort(uint8_t dest_id);


/* Note: This Callback only works for DMA transfers
 * Note: Callback does not need to set driverState->txSize to zero since the
 * callback is called by the DMA Major Loop completion callback, meaning the
 * full transfer has already completed. */
void Uart0_TxIdleCallback(UNUSED void *driverState, uart_event_t event, UNUSED void *userData)
{
    if (event == UART_EVENT_END_TRANSFER)
    {
        ++uart0_tx_done;
    }
    else if (event == UART_EVENT_ERROR)
    {
        ++uart0_dma_err_sem;
    }

    return;
}


/* Active Object state definitions */

void TxAO_init(void)
{
    /* Initialize the Active Object with both its Event FIFO and initial state function */
    GLADOS_AO_Init(&AO_Tx, TXAO_PRIORITY, txao_fifo, TXAO_FIFO_SIZE, &TxAO_state);

    return;
}

void TxAO_push_interrupts(void)
{
    /* If uart1 tx idle callback was called, then decrement semaphore and post event */
    if (uart0_tx_done > 0U)
    {
        --uart0_tx_done;
        GLADOS_AO_PushEvtName(&AO_Tx, &AO_Tx, EVT_UART0_TX_DONE);
    }
    if (uart0_dma_err_sem > 0U)
    {
        /* The LPUART driver handles recovering from DMA errors, making
         * reporting of the error all that is needed. Reset the Tx AO state.
         * Note: Grabbing the DMA Error Status reg outside of the interrupt that it
         * occurred may change from when it actually gets logged but this should
         * suffice for supplementary error data. */
        --uart0_dma_err_sem;
        report_error_uint(&AO_Tx, Errors.Uart0TxDmaError, SEND_TO_SPACECRAFT, DMA->ES);
        GLADOS_AO_PushEvtName(&AO_Tx, &AO_Tx, EVT_TX_ERR);
    }

    return;
}


void TxAO_state(GLADOS_AO_t *this_ao, GLADOS_Event_t *event)
{
    DEV_ASSERT(this_ao);
    DEV_ASSERT(event);

    bool success;

    switch(event->name)
    {
        /* GLADOS_ENTRY/GLADOS_EXIT optional event for each state */
        case GLADOS_ENTRY:
            /* Arbitrary here since each Tx transfer type sets its own timeout */
            GLADOS_Timer_Init(&Tmr_TxTimeout, PT->tx_tlm_and_dump_timeout_us, EVT_TX_TIMEOUT);
            GLADOS_Timer_Subscribe_AO(&Tmr_TxTimeout, this_ao);

            /* Initialize a done event for all states to use to transition
             * back to TX_IDLE.  This event exists on the heap since Self Events
             * must not exist on the stack */
            GLADOS_Event_New(&txao_init_xfer_evt, EVT_TX_INIT_XFER);

            /* Create a self event used as a consistent way to signal the
             * restarting of the Rx process */
            GLADOS_Event_New(&txao_complete_evt, EVT_TX_COMPLETE);

            GLADOS_Fifo_Init(&txao_xfer_fifo, TXAO_XFER_FIFO_SIZE, &txao_xfer_fifo_buf[0]);

            /* Install the callback function indicating when the UART transmission has completed.
             * The callback function increments a semaphore which will cause GLADOS to post EVT_UART0_TX_DONE
             * when it is thread safe to do so (since callback is called in an interrupt)
             * Note: This Callback only works for DMA transfers since there is a bug in the interrupt code */
            (void)LPUART_DRV_InstallTxCallback(INST_LPUART0, &Uart0_TxIdleCallback, NULL);

            GLADOS_STATE_TRAN_TO_CHILD(&TxAO_state_tx_idle);
            break;

        case EVT_TX_SEND_MSG:
        case EVT_TX_SEND_OS_PRINT:
            /* Push the Tx event to the back of the intermediate Tx queue */
            success = GLADOS_Fifo_PushTail(&txao_xfer_fifo, event);
            if (!success)
            {
                report_error_uint(&AO_Tx, Errors.TxaoInterqFull, SEND_TO_SPACECRAFT, txao_xfer_fifo.count);
            }
            break;

        case EVT_TX_COMPLETE:
        case EVT_TX_ERR:
            GLADOS_STATE_TRAN_TO_CHILD(&TxAO_state_tx_idle);
            break;

        default:
            /* Top level state, skip undefined Events */
            break;
    }

    return;
}


void TxAO_state_tx_idle(GLADOS_AO_t *this_ao, GLADOS_Event_t *event)
{
    DEV_ASSERT(this_ao);
    DEV_ASSERT(event);

    bool success;
    bool evt_exists;
    GLADOS_Event_t *tx_evt;

    switch(event->name)
    {
        /* GLADOS_ENTRY/GLADOS_EXIT optional event for each state */
        case GLADOS_ENTRY:
            evt_exists = GLADOS_Fifo_PeekHead(&txao_xfer_fifo, &tx_evt);
            if (evt_exists)
            {
                /* Allow for a slight pause between transfers to give other AOs
                 * a chance to do their thing. If this was a self-evt, this would
                 * also be pushing up against the maximum number of self-evts
                 * permitted per AO processing frame. */
                GLADOS_AO_PUSH(&AO_Tx, EVT_TX_INIT_XFER);
            }
            break;

        case GLADOS_EXIT:
            break;

        case EVT_TX_SEND_MSG:
        case EVT_TX_SEND_OS_PRINT:
            /* Push the Tx event to the back of the intermediate Tx queue */
            success = GLADOS_Fifo_PushTail(&txao_xfer_fifo, event);
            if (!success)
            {
                report_error_uint(&AO_Tx, Errors.TxaoInterqFull, SEND_TO_SPACECRAFT, txao_xfer_fifo.count);
            }

            /* Start the transfer */
            GLADOS_AO_PUSH_EVT_SELF(&txao_init_xfer_evt);
            break;

        case EVT_TX_INIT_XFER:
            GLADOS_STATE_TRAN_TO_SIBLING(&TxAO_state_tx_busy);
            break;

        default:
            GLADOS_STATE_CHECK_PARENT(&TxAO_state);
            break;
    }

    return;
}


void TxAO_state_tx_busy(GLADOS_AO_t *this_ao, GLADOS_Event_t *event)
{
    DEV_ASSERT(this_ao);
    DEV_ASSERT(event);

    bool evt_exists;
    GLADOS_Event_t *tx_evt;

    switch(event->name)
    {
        /* GLADOS_ENTRY/GLADOS_EXIT optional event for each state */
        case GLADOS_ENTRY:
            evt_exists = GLADOS_Fifo_PeekHead(&txao_xfer_fifo, &tx_evt);

            /* Order matters.  We expect an event to exist if we get to this state, but if
             * it does not exist or the event data within is invalid then throw error */
            if ((!evt_exists) || (!tx_evt->has_data) || (tx_evt->data_size != sizeof(TxAO_TxParams_t)))
            {
                /* No data in event INTERQ, report error and restart Tx process */
                report_error_uint(&AO_Tx, Errors.TxaoInterqNoData, DO_NOT_SEND_TO_SPACECRAFT,
                                     ((uint32_t)tx_evt->has_data << 8U) + (uint32_t)tx_evt->data_size);
                GLADOS_AO_PUSH(&AO_Tx, EVT_TX_ERR);
            }
            else
            {
                /* Get a pointer to the transfer parameters. This pointer points
                 * to the top item of the intermediate transfer FIFO, therefore
                 * the item in that FIFO should not be popped until the transfer
                 * has completed. */
                txao_xfer_params_ptr = (TxAO_TxParams_t *)&tx_evt->data[0];

                switch (txao_xfer_params_ptr->tx_type)
                {
                    case TX_SC_TLM:
                    case TX_GSE_TLM:
                        GLADOS_STATE_TRAN_TO_CHILD(&TxAO_state_tx_busy_xfer);
                        GLADOS_STATE_TRAN_TO_CHILD(&TxAO_state_tx_busy_xfer_tlm);
                        break;

                    case TX_SC_DUMP:
                    case TX_GSE_DUMP:
                        GLADOS_STATE_TRAN_TO_CHILD(&TxAO_state_tx_busy_xfer);
                        GLADOS_STATE_TRAN_TO_CHILD(&TxAO_state_tx_busy_xfer_dump);
                        break;

                    case TX_SC_ACK:
                    case TX_GSE_ACK:
                        GLADOS_STATE_TRAN_TO_CHILD(&TxAO_state_tx_busy_ack);
                        break;

                    case TX_DATA:
                        GLADOS_STATE_TRAN_TO_CHILD(&TxAO_state_tx_busy_data);
                        break;

                    case TX_RAW:
                        GLADOS_STATE_TRAN_TO_CHILD(&TxAO_state_tx_busy_raw);
                        break;

                    case TX_DATA_WACK:
                        GLADOS_STATE_TRAN_TO_CHILD(&TxAO_state_tx_busy_data_wack);
                        break;

                    default:
                        /* Tx request has an unsupported type, report error and restart Tx process */
                        report_error_uint(&AO_Tx, Errors.TxaoInvalidTxParams, SEND_TO_SPACECRAFT, txao_xfer_params_ptr->tx_type);
                        GLADOS_AO_PUSH(&AO_Tx, EVT_TX_ERR);
                        break;
                }
            }
            break;

        case GLADOS_EXIT:
            GLADOS_Timer_Disable(&Tmr_TxTimeout);

            /* Transfer has completed, pop the transfer record and params */
            GLADOS_Fifo_PopHead(&txao_xfer_fifo);
            break;

        case EVT_TX_TIMEOUT:
            /* Abort any transfers currently in progress and report an error.
             * Note: Success is always returned so return is ignored. */
            (void)LPUART_DRV_AbortSendingData(INST_LPUART0);
            report_error_uint(&AO_Tx, Errors.TxaoTxTimeout, SEND_TO_SPACECRAFT, 0UL);
            GLADOS_STATE_TRAN_TO_SIBLING(&TxAO_state_tx_idle);
            break;

        case EVT_TX_COMPLETE:
            GLADOS_STATE_TRAN_TO_SIBLING(&TxAO_state_tx_idle);
            break;

        default:
            GLADOS_STATE_CHECK_PARENT(&TxAO_state);
            break;
    }

    return;
}



void TxAO_state_tx_busy_xfer(GLADOS_AO_t *this_ao, GLADOS_Event_t *event)
{
    DEV_ASSERT(this_ao);
    DEV_ASSERT(event);

    status_t status;

    switch(event->name)
    {
        /* GLADOS_ENTRY/GLADOS_EXIT optional event for each state */
        case GLADOS_ENTRY:
        case EVT_UART0_TX_DONE:
            GLADOS_Timer_Disable(&Tmr_TxTimeout);

            /* End the transfer if there is nothing left to transmit */
            if (txao_xfer_params_ptr->length == 0UL)
            {
                /* Do not push as self-event since there is a possibility that
                 * the max supported OS self-events could be hit due to the many
                 * state levels combined with self-event push in idle state entry,
                 * which could in theory lead to an infinite loop. */
                GLADOS_AO_PUSH(this_ao, EVT_TX_COMPLETE);
            }
            else
            {
                /* Format into a Clank message based on transfer parameters */
                txao_xfer_params_ptr->length = TxAO_FormatClank_Norm(&txao_clank_buf[0],
                                                                      &txao_clank_buf_size,
                                                                      txao_xfer_params_ptr);

                /* Initiate transfer to the appropriate UART port based on port specified in tx_type */
                status = LPUART_DRV_SendData(TxAO_GetPort(txao_xfer_params_ptr->tx_type),
                                             (const uint8_t *)&txao_clank_buf[0],
                                             txao_clank_buf_size);
                if (status != STATUS_SUCCESS)
                {
                    /* Send error and restart the Tx process */
                    report_error_uint(&AO_Tx, Errors.TxaoUartBusy, SEND_TO_SPACECRAFT,
                                         ((uint32_t)txao_xfer_params_ptr->seq_id << 16U) + (uint32_t)txao_xfer_params_ptr->msg_id);
                    GLADOS_AO_PUSH(&AO_Tx, EVT_TX_ERR);
                }

                /* Start by sending the currently requested seq_id, but if the message must be split
                 * up into multiple messages then the seq_id must be incremented each time */
                txao_xfer_params_ptr->seq_id++;

                GLADOS_Timer_Set(&Tmr_TxTimeout, PT->tx_tlm_and_dump_timeout_us);
                GLADOS_Timer_EnableOnce(&Tmr_TxTimeout);
            }
            break;

        case GLADOS_EXIT:
            break;

        default:
            GLADOS_STATE_CHECK_PARENT(&TxAO_state_tx_busy);
            break;
    }

    return;
}


void TxAO_state_tx_busy_xfer_tlm(GLADOS_AO_t *this_ao, GLADOS_Event_t *event)
{
    DEV_ASSERT(this_ao);
    DEV_ASSERT(event);

    switch(event->name)
    {
        /* GLADOS_ENTRY/GLADOS_EXIT optional event for each state */
        case GLADOS_ENTRY:
            break;

        case GLADOS_EXIT:
            /* Signal to TlmAO that TxAO has completed the transfer */
            GLADOS_AO_PUSH(&AO_Tlm, EVT_TX_TLM_DONE);
            break;

        default:
            GLADOS_STATE_CHECK_PARENT(&TxAO_state_tx_busy_xfer);
            break;
    }

    return;
}


void TxAO_state_tx_busy_xfer_dump(GLADOS_AO_t *this_ao, GLADOS_Event_t *event)
{
    DEV_ASSERT(this_ao);
    DEV_ASSERT(event);

    switch(event->name)
    {
        /* GLADOS_ENTRY/GLADOS_EXIT optional event for each state */
        case GLADOS_ENTRY:
            break;

        case GLADOS_EXIT:
            /* Signal to DataAO that transfer has completed */
            GLADOS_AO_PUSH(&AO_Data, EVT_TX_DUMP_NEXT);
            break;

        default:
            GLADOS_STATE_CHECK_PARENT(&TxAO_state_tx_busy_xfer);
            break;
    }

    return;
}


void TxAO_state_tx_busy_ack(GLADOS_AO_t *this_ao, GLADOS_Event_t *event)
{
    DEV_ASSERT(this_ao);
    DEV_ASSERT(event);

    status_t status;

    switch(event->name)
    {
        /* GLADOS_ENTRY/GLADOS_EXIT optional event for each state */
        case GLADOS_ENTRY:
            TxAO_FormatClank_Ack(&txao_clank_buf[0], &txao_clank_buf_size, txao_xfer_params_ptr);

            /* Initiate transfer to the appropriate UART port based on port specified in tx_type */
            status = LPUART_DRV_SendData(TxAO_GetPort(txao_xfer_params_ptr->tx_type), (const uint8_t *)&txao_clank_buf[0], txao_clank_buf_size);
            if (status != STATUS_SUCCESS)
            {
                /* Send error and restart the Tx process */
                report_error_uint(&AO_Tx, Errors.TxaoUartBusy, SEND_TO_SPACECRAFT,
                                     ((uint32_t)txao_xfer_params_ptr->seq_id << 16U) + (uint32_t)txao_xfer_params_ptr->msg_id);
                GLADOS_AO_PUSH(&AO_Tx, EVT_TX_ERR);
            }

            GLADOS_Timer_Set(&Tmr_TxTimeout, PT->tx_ack_timeout_us);
            GLADOS_Timer_EnableOnce(&Tmr_TxTimeout);
            break;

        case GLADOS_EXIT:
            break;

        case EVT_UART0_TX_DONE:
            GLADOS_Timer_Disable(&Tmr_TxTimeout);
            GLADOS_AO_PUSH_EVT_SELF(&txao_complete_evt);
            break;

        default:
            GLADOS_STATE_CHECK_PARENT(&TxAO_state_tx_busy);
            break;
    }

    return;
}


void TxAO_state_tx_busy_data(GLADOS_AO_t *this_ao, GLADOS_Event_t *event)
{
    DEV_ASSERT(this_ao);
    DEV_ASSERT(event);

    status_t status;

    switch(event->name)
    {
        /* GLADOS_ENTRY/GLADOS_EXIT optional event for each state */
        case GLADOS_ENTRY:
            /* Format into a Clank message based on transfer parameters */
            txao_xfer_params_ptr->length = TxAO_FormatClank_Norm(&txao_clank_buf[0],
                                                                  &txao_clank_buf_size,
                                                                  txao_xfer_params_ptr);

            /* This is expected to always be zero since data transfers
             * should be no larger than the maximum clank payload size. */
            DEV_ASSERT(txao_xfer_params_ptr->length == 0U);

            /* Initiate transfer to the appropriate UART port based on port specified in tx_type */
            status = LPUART_DRV_SendData(TxAO_GetPort(txao_xfer_params_ptr->tx_type), (const uint8_t *)&txao_clank_buf[0], txao_clank_buf_size);
            if (status != STATUS_SUCCESS)
            {
                /* Send error and restart the Tx process */
                report_error_uint(&AO_Tx, Errors.TxaoUartBusy, SEND_TO_SPACECRAFT,
                                     ((uint32_t)txao_xfer_params_ptr->seq_id << 16U) + (uint32_t)txao_xfer_params_ptr->msg_id);
                GLADOS_AO_PUSH(&AO_Tx, EVT_TX_ERR);
            }

            GLADOS_Timer_Set(&Tmr_TxTimeout, PT->tx_data_timeout_us);
            GLADOS_Timer_EnableOnce(&Tmr_TxTimeout);
            break;

        case GLADOS_EXIT:
            break;

        case EVT_UART0_TX_DONE:
            GLADOS_Timer_Disable(&Tmr_TxTimeout);
            GLADOS_AO_PUSH_EVT_SELF(&txao_complete_evt);
            break;

        default:
            GLADOS_STATE_CHECK_PARENT(&TxAO_state_tx_busy);
            break;
    }

    return;
}


void TxAO_state_tx_busy_raw(GLADOS_AO_t *this_ao, GLADOS_Event_t *event)
{
    DEV_ASSERT(this_ao);
    DEV_ASSERT(event);

    status_t status;

    switch(event->name)
    {
        /* GLADOS_ENTRY/GLADOS_EXIT optional event for each state */
        case GLADOS_ENTRY:
            /* In the parent state, txao_xfer_params_ptr has already been set
             * to the latest transfer parameters */

            /* Initiate transfer to the appropriate UART port based on port specified in tx_type */
            status = LPUART_DRV_SendData(TxAO_GetPort(txao_xfer_params_ptr->tx_type), (const uint8_t *)txao_xfer_params_ptr->data_addr, txao_xfer_params_ptr->length);
            if (status != STATUS_SUCCESS)
            {
                /* Send error and restart the Tx process */
                report_error_uint(&AO_Tx, Errors.TxaoUartBusy, SEND_TO_SPACECRAFT,
                                     ((uint32_t)txao_xfer_params_ptr->seq_id << 16U) + (uint32_t)txao_xfer_params_ptr->msg_id);
                GLADOS_AO_PUSH(&AO_Tx, EVT_TX_ERR);
            }

            GLADOS_Timer_Set(&Tmr_TxTimeout, PT->tx_raw_timeout_us);
            GLADOS_Timer_EnableOnce(&Tmr_TxTimeout);
            break;

        case GLADOS_EXIT:
            break;

        case EVT_UART0_TX_DONE:
            GLADOS_Timer_Disable(&Tmr_TxTimeout);
            GLADOS_AO_PUSH_EVT_SELF(&txao_complete_evt);
            break;

        default:
            GLADOS_STATE_CHECK_PARENT(&TxAO_state_tx_busy);
            break;
    }

    return;
}


void TxAO_state_tx_busy_data_wack(GLADOS_AO_t *this_ao, GLADOS_Event_t *event)
{
    DEV_ASSERT(this_ao);
    DEV_ASSERT(event);

    status_t status;

    switch(event->name)
    {
        /* GLADOS_ENTRY/GLADOS_EXIT optional event for each state */
        case GLADOS_ENTRY:
            /* Format into a Clank message based on transfer parameters */
            txao_xfer_params_ptr->length = TxAO_FormatClank_DataWAck(&txao_clank_buf[0],
                                                                     &txao_clank_buf_size,
                                                                     txao_xfer_params_ptr);

            /* This is expected to always be zero since data transfers
             * should be no larger than the maximum clank payload size. */
            DEV_ASSERT(txao_xfer_params_ptr->length == 0U);

            /* Initiate transfer to the appropriate UART port based on port specified in tx_type */
            status = LPUART_DRV_SendData(TxAO_GetPort(txao_xfer_params_ptr->tx_type), (const uint8_t *)&txao_clank_buf[0], txao_clank_buf_size);
            if (status != STATUS_SUCCESS)
            {
                /* Send error and restart the Tx process */
                report_error_uint(&AO_Tx, Errors.TxaoUartBusy, SEND_TO_SPACECRAFT,
                                     ((uint32_t)txao_xfer_params_ptr->seq_id << 16U) + (uint32_t)txao_xfer_params_ptr->msg_id);
                GLADOS_AO_PUSH(&AO_Tx, EVT_TX_ERR);
            }

            GLADOS_Timer_Set(&Tmr_TxTimeout, PT->tx_data_wack_timeout_us);
            GLADOS_Timer_EnableOnce(&Tmr_TxTimeout);
            break;

        case GLADOS_EXIT:
            break;

        case EVT_UART0_TX_DONE:
            GLADOS_Timer_Disable(&Tmr_TxTimeout);
            GLADOS_AO_PUSH_EVT_SELF(&txao_complete_evt);
            break;

        default:
            GLADOS_STATE_CHECK_PARENT(&TxAO_state_tx_busy);
            break;
    }

    return;
}


/* Public function definitions */

/* If sending a tx_type other than Ack/Nack, then the onus is on the user
 * to ensure that the data remains untouched until the transmission has
 * completed */
void TxAO_SendMsg(GLADOS_AO_t *src_ao, uint32_t evt_name, uint8_t dest_id, uint8_t tx_type, uint8_t seq_id,
                      uint16_t msg_id, uint16_t ack_status, uint32_t data_addr, uint16_t length)
{
    GLADOS_Event_t evt;
    TxAO_TxParams_t tx_params;

    tx_params.dest_id    = dest_id;
    tx_params.tx_type    = tx_type;
    tx_params.seq_id     = seq_id;
    tx_params.msg_id     = msg_id;
    tx_params.ack_status = ack_status;
    tx_params.data_addr  = data_addr;
    tx_params.length     = length;

    GLADOS_Event_New_Data(&evt, evt_name, (uint8_t *)&tx_params, sizeof(TxAO_TxParams_t));
    GLADOS_AO_PushEvt(src_ao, &AO_Tx, &evt);

    return;
}


void TxAO_SendMsg_Tlm(GLADOS_AO_t *src_ao, TxPort_t port, uint8_t dest_id, uint8_t seq_id,
                                        uint16_t msg_id, uint32_t data_addr, uint16_t length)
{
    TxAO_SendMsg(src_ao, EVT_TX_SEND_MSG, dest_id, (uint8_t)(port | TX_TLM), seq_id, msg_id, 0U, data_addr, length);
    return;
}


void TxAO_SendMsg_Dump(GLADOS_AO_t *src_ao, TxPort_t port, uint8_t dest_id, uint8_t seq_id,
                                        uint16_t msg_id, uint32_t data_addr, uint16_t length)
{
    TxAO_SendMsg(src_ao, EVT_TX_SEND_MSG, dest_id, (uint8_t)(port | TX_DUMP), seq_id, msg_id, 0U, data_addr, length);
    return;
}


void TxAO_SendMsg_Ack(GLADOS_AO_t *src_ao, TxPort_t port, uint8_t dest_id,
                                        uint8_t seq_id, uint16_t msg_id, uint16_t ack_status)
{
    TxAO_SendMsg(src_ao, EVT_TX_SEND_MSG, dest_id, (uint8_t)(port | TX_ACK), seq_id, msg_id, ack_status, 0UL, 0U);
    return;
}


void TxAO_SendMsg_Data(GLADOS_AO_t *src_ao, TxPort_t port, uint8_t dest_id, uint8_t seq_id,
                                        uint16_t msg_id, uint32_t data_addr, uint16_t length)
{
    TxAO_SendMsg(src_ao, EVT_TX_SEND_MSG, dest_id, (uint8_t)(port | TX_DATA), seq_id, msg_id, 0U, data_addr, length);
    return;
}


void TxAO_SendMsg_Raw(GLADOS_AO_t *src_ao, TxPort_t port, uint32_t data_addr, uint16_t length)
{
    TxAO_SendMsg(src_ao, EVT_TX_SEND_MSG, 0U, (uint8_t)(port | TX_RAW), 0U, 0U, 0U, data_addr, length);
    return;
}


void TxAO_SendMsg_Data_wAck(GLADOS_AO_t *src_ao, TxPort_t port, uint8_t dest_id, uint8_t seq_id,
                                        uint16_t msg_id, uint32_t data_addr, uint16_t length)
{
    TxAO_SendMsg(src_ao, EVT_TX_SEND_MSG, dest_id, (uint8_t)(port | TX_DATA_WACK), seq_id, msg_id, 0U, data_addr, length);
    return;
}


/* Private function definitions */

static uint16_t TxAO_FormatClank_Norm(uint8_t *buf, uint16_t *buf_size, TxAO_TxParams_t *tx_params)
{
    uint8_t data_length;

    if (tx_params->length > TXAO_CLANK_MAX_XFER_SIZE)
    {
        data_length = TXAO_CLANK_MAX_XFER_SIZE;
    }
    else
    {
        data_length = tx_params->length;
    }

    Clank_CreateMessage(buf,
                        buf_size,
                        false,   /* Acts as the slave, SC/GSE is the master */
                        MAX_ADDR_RFT,
                        tx_params->dest_id,
                        tx_params->seq_id,
                        tx_params->msg_id,
                        (uint8_t *)tx_params->data_addr,
                        data_length);

    /* Return the number of bytes that still need to be transmitted */
    return (tx_params->length - data_length);
}


static uint16_t TxAO_FormatClank_DataWAck(uint8_t *buf, uint16_t *buf_size, TxAO_TxParams_t *tx_params)
{
    DEV_ASSERT(buf);
    DEV_ASSERT(buf_size);
    DEV_ASSERT(tx_params);

    enum Constants { ACK_STATUS_LENGTH = 2U };

    uint8_t data_length;

    if (tx_params->length > (TXAO_CLANK_MAX_XFER_SIZE - ACK_STATUS_LENGTH))
    {
        data_length = (TXAO_CLANK_MAX_XFER_SIZE - ACK_STATUS_LENGTH);
    }
    else
    {
        data_length = tx_params->length;
    }

    /* Preemptively copy the data over into the buffer used for Clank framing
     * prior to framing the clank packet. This permits more efficient memory
     * usage when custom data (ACK_STATUS in this case) is needed to be added
     * to the data packet. */
    /* Set the ack_status in Big Endian format right after the Clank header.
     * Note: Sending the ACK_STATUS is included in the framed packet even in
     * cases when data_length is zero. */
    buf[CLANK_HDR_SIZE]   = (tx_params->ack_status >> 8U) & 0xFFU;
    buf[CLANK_HDR_SIZE+1] = (tx_params->ack_status)       & 0xFFU;
    if ((data_length > 0U) && (tx_params->data_addr != 0UL))
    {
        /* Copy the data over-preemptively prior to creating the Clank packet */
        memcpy(&buf[CLANK_HDR_SIZE+ACK_STATUS_LENGTH], (uint8_t *)tx_params->data_addr, data_length);
    }

    /* Create a Clank message "in-place" (data_buf=NULL but data_size>0)
     * such that the Clank header and CRC are added to buf, but the data
     * remains untouched since it has already been copied over. */
    Clank_CreateMessage(buf,
                        buf_size,
                        false,   /* Acts as the slave, SC/GSE is the master */
                        MAX_ADDR_RFT,
                        tx_params->dest_id,
                        tx_params->seq_id,
                        tx_params->msg_id,
                        NULL,
                        data_length + ACK_STATUS_LENGTH);

    /* Return the number of bytes that still need to be transmitted */
    return (tx_params->length - data_length);
}

static void TxAO_FormatClank_Ack(uint8_t *buf, uint16_t *buf_size, TxAO_TxParams_t *tx_params)
{
    DEV_ASSERT(buf);
    DEV_ASSERT(buf_size);
    DEV_ASSERT(tx_params);

    uint8_t ack_data[P4TLM_RFT_STATUS_PAYLOAD_SIZE] = {0U};

    Err_Rft_Rec_t *total_err = (Err_Rft_Rec_t *)&telemetry_record[0];
    Status_Err_Rft_Rec_t *status_err = (Status_Err_Rft_Rec_t*) &total_err->status_err;

    P4Tlm_TcStatus_t *rft_status = (P4Tlm_TcStatus_t *)&ack_data[0];

    rft_status->tc_ack_status      = REV16(tx_params->ack_status);
    rft_status->sc_tc_err_code1    = REV16(status_err->sc_err_1);
    rft_status->sc_tc_err_code2    = REV16(status_err->sc_err_2);
    rft_status->sc_tc_err_code3    = REV16(status_err->sc_err_3);
    rft_status->tc_state           = (uint8_t)RftData.state;
    rft_status->tc_busy            = (uint8_t)RftData.busy;
    rft_status->tc_needs_reset     = (uint8_t)RftData.needs_reset_flag;
    rft_status->tc_app_select      = (uint8_t)RftData.app_select;
    rft_status->tc_acpt_cnt        = REV16(RftData.cmd_acpt_cnt);
    rft_status->tc_rjct_cnt        = REV16(RftData.cmd_rjct_cnt);
    rft_status->tc_unix_time_us_hi = REV32(RftData.unix_time_us_hi);
    rft_status->tc_unix_time_us_lo = REV32(RftData.unix_time_us_lo);

    Clank_CreateMessage(buf,
                        buf_size,
                        false,    /* Acts as the slave, SC/GSE is the master */
                        MAX_ADDR_RFT,
                        tx_params->dest_id,
                        tx_params->seq_id,
                        tx_params->msg_id,
                        (uint8_t *)&ack_data[0],
                        (uint8_t)sizeof(P4Tlm_TcStatus_t));

    return;
}


static uint32_t TxAO_GetPort(UNUSED uint8_t tx_type)
{
    /* Thruster only has one port, so return this. Doing it this way
     * allows for the infrastructure to remain similar to the PCU. */
    return INST_LPUART0;
}
