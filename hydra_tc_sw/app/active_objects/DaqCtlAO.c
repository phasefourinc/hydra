/* Copyright (c) 2017-2020 by Phase Four, Inc. All Rights Reserved. Confidential and Proprietary.
 *
 * DaqCtlAO.c
 */

#include "DaqCtlAO.h"
#include "mcu_types.h"
#include "glados.h"
#include "TlmAO.h"
#include "MaxwellAO.h"
#include "ErrAO.h"
#include "edma_adc.h"
#include "ad768x.h"
#include "gpio.h"
#include "timer64.h"
#include "rft_data.h"
#include "mcu_adc.h"
#include "mcu_types.h"
#include "pwm_driver.h"
#include "ltc6903.h"
#include "default_pt.h"
#include "math.h"


/* Active Object public interface */

GLADOS_AO_t AO_DaqCtl;

/* Active Object timers */
GLADOS_Timer_t Tmr_DaqStart;
GLADOS_Timer_t Tmr_DaqRftTimeout;

/* Active Object private variables */
static GLADOS_Event_t daqctl_fifo[DAQCTLAO_FIFO_SIZE];

AD768x_t Adc0 = { 0, 0, 0, {0} };
Pwm_t Pwm_PpVset = {0};

static volatile uint8_t dma_adc_tx_err  = 0U;
static volatile uint8_t dma_adc_rx_done = 0U;
static volatile uint8_t dma_adc_rx_err  = 0U;

#define PP_VSET_MIN_VOLTAGE           35.0f
#define PP_RAMP_UPDATE_INTERVAL_MS    20

/* Max voltage of a single step when ramping. This includes the final step where it sets the setpoint */
#define MAX_PP_RAMP_VOLTAGE_STEP      5.0f

typedef struct PP_Ramp_Parameters_t{
    uint32_t total_pp_ramp_steps;
    uint32_t current_pp_ramp_step;
    float pp_step_voltage;
    float next_step_voltage;
    float final_voltage_setpoint;
    uint32_t ten_ms_count_val; /*This value counts up to the PP_RAMP_UPDATE_INTERVAL_MS in 10 ms increments. Once hit it will do a ramp. */
    bool is_ramping;

} BYTE_PACKED PP_Ramp_Parameters_t;

static PP_Ramp_Parameters_t PP_Ramp_Parameters = {0, 0, 0.0, 0.0, 0.0, 0, false};

static void DmaAdc_RxCallback(UNUSED void *parameter, edma_chn_status_t event)
{
    if (event == EDMA_CHN_NORMAL)
    {
        ++dma_adc_rx_done;
    }
    else
    {
        ++dma_adc_rx_err;
    }

    return;
}


static void DmaAdc_TxCallback(UNUSED void *parameter, edma_chn_status_t event)
{
    if (event != EDMA_CHN_NORMAL)
    {
        ++dma_adc_tx_err;
    }

    return;
}


static bool update_ramp_voltage(void)
{
    Cmd_Err_Code_t error_code = ERROR_NONE;

    /*If the voltage level is currently above the final ramp voltage setpoint, or if we are on the last step of the ramp, or if the next step voltage is above the final voltage setpoint, set it to the final voltage setpoint. */
    if ((PP_Ramp_Parameters.current_pp_ramp_step >= PP_Ramp_Parameters.total_pp_ramp_steps) || (PP_Ramp_Parameters.final_voltage_setpoint <= PP_Ramp_Parameters.next_step_voltage))
    {
        error_code = DaqCtlAO_PpVset_SetVoltage(PP_Ramp_Parameters.final_voltage_setpoint, false);
    }
    /*If not set it to the next step */
    else
    {
        error_code = DaqCtlAO_PpVset_SetVoltage(PP_Ramp_Parameters.next_step_voltage, false);
    }

    if (error_code.reported_error->error_id == STATUS_SUCCESS)
    {
        return true;
    }
    else
    {
        return false;
    }

}

/* Active Object state definitions */


void DaqCtlAO_init(void)
{
    /* Initialize the Active Object with both its Event FIFO and initial state function */
    GLADOS_AO_Init(&AO_DaqCtl, DAQCTLAO_PRIORITY, daqctl_fifo, DAQCTLAO_FIFO_SIZE, &DaqCtlAO_state);

    return;
}


void DaqCtlAO_push_interrupts(void)
{
    if (dma_adc_rx_done > 0U)
    {
        --dma_adc_rx_done;
        GLADOS_AO_PushEvtName(&AO_DaqCtl, &AO_DaqCtl, EVT_DAQ_DMA_COMPLETE);
    }
    if (dma_adc_rx_err > 0U)
    {
        --dma_adc_rx_err;
        report_error_uint(&AO_DaqCtl, Errors.SpiAdcDmaRxOverrun, DO_NOT_SEND_TO_SPACECRAFT, 0U);
    }
    if (dma_adc_tx_err > 0U)
    {
        --dma_adc_tx_err;
        report_error_uint(&AO_DaqCtl, Errors.SpiAdcDmaTxUnderrun, DO_NOT_SEND_TO_SPACECRAFT, 0U);
    }
    return;
}



void DaqCtlAO_state(GLADOS_AO_t *this_ao, GLADOS_Event_t *event)
{
    DEV_ASSERT(this_ao);
    DEV_ASSERT(event);

    status_t status;

    switch(event->name)
    {
        /* GLADOS_ENTRY/GLADOS_EXIT optional event for each state */
        case GLADOS_ENTRY:
            /* Configure DAQ rate, this also determines the overall system frame rate */
            GLADOS_Timer_Init(&Tmr_DaqStart, DAQCTLAO_PERIOD_US, EVT_TMR_DAQ_START);
            GLADOS_Timer_Subscribe_AO(&Tmr_DaqStart, this_ao);
            GLADOS_Timer_EnableContinuous(&Tmr_DaqStart);

            EdmaAdc_Init(&DmaAdc_TxCallback, &DmaAdc_RxCallback);

            /* Initialize the Waveform Generator VSET External Oscillator to 0% duty cycle */
            status = LTC6903_Osc_Init(LPSPICOM0);
            if (status != STATUS_SUCCESS)
            {
                report_error_uint(&AO_DaqCtl, Errors.WfOscInitFail, DO_NOT_SEND_TO_SPACECRAFT, (uint32_t)status);
            }

            AD768x_Init(&Adc0, &edma_adc_rx[EDMA_ADC0_CH0_IDX], &PT->adc0_ch0_cal_tfet, 5.0f);

            status = PWM_Driver_Init(&Pwm_PpVset, INST_FLEXTIMER_PWM1, 5U, &flexTimer_pwm1_InitConfig,
                                         &flexTimer_pwm1_PwmConfig, PT->pp_vset_to_pwm_cnt_cal, PT->pp_vset_min_v, PT->pp_vset_max_v);
            if (status != STATUS_SUCCESS)
            {
                report_error_uint(&AO_DaqCtl, Errors.PwmDriverInitFail, DO_NOT_SEND_TO_SPACECRAFT, INST_FLEXTIMER_PWM1);
            }

            mcu_adc_init();

            GLADOS_STATE_TRAN_TO_CHILD(&DaqCtlAO_state_idle);
            break;

        case GLADOS_EXIT:
            break;

        default:
            /* Top level state, skip undefined Events */
            break;
    }

    return;
}


void DaqCtlAO_state_idle(GLADOS_AO_t *this_ao, GLADOS_Event_t *event)
{
    DEV_ASSERT(this_ao);
    DEV_ASSERT(event);

    status_t status;
    Cmd_Err_Code_t error;
    float evb_check;

    switch(event->name)
    {
        /* GLADOS_ENTRY/GLADOS_EXIT optional event for each state */
        case GLADOS_ENTRY:
            break;

        case GLADOS_EXIT:
            break;

        case EVT_TMR_DAQ_START:
            /* If still in RFT_DATA state, then we have a frame overrun */
            DEV_ASSERT(this_ao->state_fn_ptr == &DaqCtlAO_state_idle);

            status = EdmaAdc_EndTransfer();
            if (status != STATUS_SUCCESS)
            {
                report_error_uint(&AO_DaqCtl, Errors.SpiAdcDmaError, DO_NOT_SEND_TO_SPACECRAFT, (uint32_t)status);
            }

            EdmaAdc_StartTransfer();

            /* Start blocking sample of ADCs while External ADC is read in parallel */
            mcu_adc_sample_ch_all();
            break;

        case EVT_DAQ_DMA_COMPLETE:
            status = EdmaAdc_EndTransfer();

            if (status != STATUS_SUCCESS)
            {
                if (RftData.daq_consec_err_cnt < 0xFFFFFFFFUL)
                {
                    ++RftData.daq_consec_err_cnt;
                }
                report_error_uint(&AO_DaqCtl, Errors.SpiAdcDmaError, DO_NOT_SEND_TO_SPACECRAFT, (uint32_t)status);
            }
            else
            {
                /* Reset the consecutive DAQ ADC error counter */
                RftData.daq_consec_err_cnt = 0UL;

                AD768x_RawToVoltage_AllChannels(&Adc0);

                RftData.tfet    = AD768x_CalibrateChannel(&Adc0, IN0);
                RftData.tchoke  = AD768x_CalibrateChannel(&Adc0, IN1);
                RftData.tcoil   = AD768x_CalibrateChannel(&Adc0, IN2);
                RftData.tic     = AD768x_CalibrateChannel(&Adc0, IN3);
                RftData.tmatch  = AD768x_CalibrateChannel(&Adc0, IN4);
                RftData.tpl     = AD768x_CalibrateChannel(&Adc0, IN5);
                RftData.ehvd    = AD768x_CalibrateChannel(&Adc0, IN6);
                RftData.ihvd    = AD768x_CalibrateChannel(&Adc0, IN7);
                RftData.tadc0   = AD768x_CalibrateChannel(&Adc0, TEMP);

                RftData.ebus    = mcu_adc_ch_calibrate(MCU_ADC0_CH3,  &PT->mcu_adc0_ch3_cal_ebus);
                RftData.e12v    = mcu_adc_ch_calibrate(MCU_ADC0_CH7,  &PT->mcu_adc0_ch7_cal_e12v);
                RftData.e5v     = mcu_adc_ch_calibrate(MCU_ADC0_CH8,  &PT->mcu_adc0_ch8_cal_e5v);
                RftData.evb     = mcu_adc_ch_calibrate(MCU_ADC0_CH9,  &PT->mcu_adc0_ch9_cal_evb);
                RftData.ibus    = mcu_adc_ch_calibrate(MCU_ADC0_CH10, &PT->mcu_adc0_ch10_cal_ibus);
                RftData.i12v    = mcu_adc_ch_calibrate(MCU_ADC0_CH11, &PT->mcu_adc0_ch11_cal_i12v);

                /* Use a 2-step calibration to get ivb since there is a dependency on the push-pull Vset,
                 * which shifts the ivb calibration curve up or down. The slope is obtained by calibrating
                 * the sensed ivb and then the y-intercept is obtained by applying a Vset calibration to
                 * the sensed evb value that senses the Vset. However, evb is only valid if the push-pull
                 * is enabled so we check for it, otherwise we assume Vset is its minimum value. */
                if (RftData.gpio_pp_en_fb)
                {
                    evb_check = RftData.evb;
                }
                else
                {
                    evb_check = PP_VSET_MIN_VOLTAGE;
                }
                RftData.ivb  = mcu_adc_ch_calibrate(MCU_ADC0_CH14, &PT->mcu_adc0_ch14_cal_ivb);
                RftData.ivb += (PT->mcu_adc0_ch14_cal_ivb_vset[1] * evb_check) + PT->mcu_adc0_ch14_cal_ivb_vset[0];

                RftData.tfb_in  = mcu_adc_ch_calibrate(MCU_ADC1_CH6,  &PT->mcu_adc1_ch6_cal_tfb_in);
                RftData.tfb_out = mcu_adc_ch_calibrate(MCU_ADC1_CH8,  &PT->mcu_adc1_ch8_cal_tfb_out);
                RftData.tfram   = mcu_adc_ch_calibrate(MCU_ADC1_CH23, &PT->mcu_adc1_ch23_cal_tfram);
                RftData.tmcu    = mcu_adc_ch_calibrate(MCU_ADC1_CH24, &PT->mcu_adc1_ch24_cal_tmcu);

                /* RFT is considered connected if either of the RTDs are connected. Block 1 the TPL rails high if not connected. */
                RftData.rft_connected_flag = (RftData.tpl < 400.0f) || (RftData.tmatch < 400.0f);

                /* Min supported bus voltage is 18V. There may be additional restrictions for
                 * plasma operations, however if just passive then we check for this min value. */
                RftData.hp_connected_flag = RftData.ebus > 18.0f;

                /* Checking if our data is valid or not */
                /* This is done by seeing if the value is 0 (the data is pull down when not connected.)
                 * or vref (this can only happen if the adc count is railed high.)
                 * We are adding some margin for some noise however the values are still in a crazy porportian.
                 * If the voltage value was > 4.0 (vref -1) at the current calibration it will indicate of a temp greater then of 4,690 c.
                 * If the voltage value was < .01 at the current calibration it will indicate a temp less then - 297.5 c
                 * Based on the survival limit, maxwell should never expect to see those values. */
                /*If we see a normal adc temp value report true and report the timeout count back to 0 */
                if ((Adc0.ch_voltage[TEMP] > 0.01f) && (Adc0.ch_voltage[TEMP] < (Adc0.vref - 1.0f)))
                {
                    RftData.inverter_connected_flag=true;
                    RftData.daq_consec_timeout_cnt = 0;
                }
                /* If we don't get a good value this may indicate a timeout.*/
                else
                {
                    /*If we have receive data before we should increment this error count and report a error.*/
                    if (RftData.inverter_connected_flag == true)
                    {
                        if (RftData.daq_consec_timeout_cnt < 0xFFFFFFFFUL)
                        {
                            ++RftData.daq_consec_timeout_cnt;
                        }
                    }
                }

                if(PP_Ramp_Parameters.is_ramping == true)
                {
                    PP_Ramp_Parameters.ten_ms_count_val = PP_Ramp_Parameters.ten_ms_count_val + 10; /* We count at the beginning because after we initialize it will take another 10 ms */

                    /*If we are at the same interval ms count then we waited enough update the ramp voltage */
                    if (PP_Ramp_Parameters.ten_ms_count_val == PP_RAMP_UPDATE_INTERVAL_MS)
                    {
                        bool update_status = update_ramp_voltage();

                        /*If this fails try one more time if not give up*/
                        if (update_status == false)
                        {
                            update_ramp_voltage();
                        }

                        /*If your finish all your steps and your on the last step reset */
                        if ( PP_Ramp_Parameters.current_pp_ramp_step == PP_Ramp_Parameters.total_pp_ramp_steps)
                        {
                            /*Reseting the ramp back to 0*/
                            reset_ramp_parameters();
                        }
                        /*If we still have to ramp some more, update the next voltage value and update to the next step*/
                        else
                        {
                            PP_Ramp_Parameters.ten_ms_count_val = 0;
                            PP_Ramp_Parameters.next_step_voltage = PP_Ramp_Parameters.next_step_voltage + PP_Ramp_Parameters.pp_step_voltage;
                            PP_Ramp_Parameters.current_pp_ramp_step =  PP_Ramp_Parameters.current_pp_ramp_step + 1;
                        }
                    }
                }
            }

            /* No matter what, sample GPIO input pins */
            RftData.wdt_rst_trig = GPIO_GetInput(GPIO_WDT_RESET);
            RftData.wdt_tog_sel  = GPIO_GetInput(GPIO_WDT_TOGGLE_SEL);

            /* Reassert PWM drivers and External oscillators each DAQ cycle.
             * Important for radiation.
             * Note: Returned status will be successful because they are set using
             * values that were previously successful. Assert statement suffices. */
            error = DaqCtlAO_WF_SetFreq(RftData.wav_gen_freq_fb);
            DEV_ASSERT(error.reported_error == Errors.StatusSuccess);
            error = DaqCtlAO_PpVset_SetPwmRaw(RftData.pwm_pp_vset_raw_fb, false);
            DEV_ASSERT(error.reported_error == Errors.StatusSuccess);

            (void)error;  /* Removes unused compiler warning */

            GPIO_SetOutput(GPIO_PP_EN, RftData.gpio_pp_en_fb);
            GPIO_SetOutput(GPIO_WF_EN, RftData.gpio_wf_en_fb);

            GLADOS_AO_PUSH(&AO_Tlm, EVT_DAQ_DATA_READY);
            break;

        default:
            GLADOS_STATE_CHECK_PARENT(&DaqCtlAO_state);
            break;
    }

    return;
}



/* Public Helper Functions */

Cmd_Err_Code_t DaqCtlAO_WF_SetFreq(float freq)
{
    Cmd_Err_Code_t error = ERROR_NONE;
    status_t status = LTC6903_Osc_SetFreq(LPSPICOM0, freq, LTC6903_CNF_BOTH_ENBL);

    if (status == STATUS_SUCCESS)
    {
        RftData.wav_gen_freq_fb = freq;
    }
    else
    {
        error = create_error_float(Errors.Ltc6903SpiSetFailed, freq);
    }

    return error;
}




Cmd_Err_Code_t DaqCtlAO_PpVset_SetVoltage(float voltage, bool stop_ppu_ramp)
{
    /* Validate that Pwm_Heater structure has been properly initialized */
    DEV_ASSERT(Pwm_PpVset.state.ftmClockSource != FTM_CLOCK_SOURCE_NONE);

    Cmd_Err_Code_t error = ERROR_NONE;

    status_t status;

    /* If this is a "external" command then there was command to set a push at a specific value. We should disable ramping then.*/
    if (stop_ppu_ramp == true)
    {
        /*If we are ramping when a command comes in to set the Push-Pull to a specific voltage  lets stop ramping this command takes precedent. */
        reset_ramp_parameters();
    }
    /* Since the VSET is a discontinuous range, handle the zero case separately */
    if (fpclassify(voltage) == FP_ZERO)
    {
        error = DaqCtlAO_PpVset_SetPwmRaw(0U, false); /*Setting this to false always because the case of stopping the ramp is handled above in stop_ppu_ramp check*/
    }
    else
    {
        status = PWM_Driver_SetVoltage(&Pwm_PpVset, voltage);
        if (status == STATUS_SUCCESS)
        {
            /* Set the setpt current as the duty cycle percentage */
            RftData.pwm_pp_vset_raw_fb = Pwm_PpVset.duty_cycle_cnt_fb;
            RftData.pp_vset_fb = voltage;
        }
        else
        {
            error = create_error_float(Errors.PpvsetPwmVInvalid, voltage);
        }
    }

    return error;
}

/* This assume a liner calibration DaqCtlAO_Initalize_PP_Ramp when calculating the voltage setpoint of the pwm for ramping. */
Cmd_Err_Code_t DaqCtlAO_Initalize_PP_Ramp(float voltage_setpoint,float ramp_time_ms)
{
    /* Validate that Pwm_Heater structure has been properly initialized */
    DEV_ASSERT(Pwm_PpVset.state.ftmClockSource != FTM_CLOCK_SOURCE_NONE);

    Cmd_Err_Code_t error = ERROR_NONE;

    /*status_t status; */
    bool start_ramp = true;

    float low_level_base_voltage = (RftData.pwm_pp_vset_raw_fb - PT->pp_vset_to_pwm_cnt_cal[0]) / PT->pp_vset_to_pwm_cnt_cal[1]; /* This will need to change if the calibration changes order.*/
    float voltage_diff;
    uint32_t num_of_steps;
    float pp_step_voltage;

    /* Since the VSET is a discontinuous range, handle the zero case separately */
    if ((fpclassify(voltage_setpoint) == FP_ZERO) || (voltage_setpoint < 0.0f))
    {
        error = DaqCtlAO_PpVset_SetPwmRaw(0, true);
        start_ramp = false;
    }

    else if (low_level_base_voltage >= voltage_setpoint)
    {
        error = DaqCtlAO_PpVset_SetVoltage(voltage_setpoint, true);
        start_ramp = false;
    }

    else if (voltage_setpoint >= 110.0f)
    {
        error = create_error_float(Errors.CmdInvalidPpVoltSetpointInvalid, voltage_setpoint);
        start_ramp = false;
    }

    /* If the ramp_time is less then 2 times the ramp interval. That means there will only be one setpoint in the ramp lets just set it to the setpoint and call it a day.*/
    else if (ramp_time_ms < (float)(PP_RAMP_UPDATE_INTERVAL_MS * 2))
    {
        voltage_diff = voltage_setpoint - low_level_base_voltage;
        if (check_over_max_voltage_step(voltage_diff, 1, low_level_base_voltage, voltage_setpoint) == false)
        {
            error = DaqCtlAO_PpVset_SetVoltage(voltage_setpoint, true);
        }

        else
        {
            error = create_error_float(Errors.CmdInvalidPpRampToHigh, voltage_setpoint);

        }
        start_ramp = false;
    }

    if (start_ramp == true)
    {   /* Divide by 0 has been checked from the statements above */
         voltage_diff = voltage_setpoint - low_level_base_voltage;
         num_of_steps = floorf(ramp_time_ms / PP_RAMP_UPDATE_INTERVAL_MS); /* The reason why we are flooring this to a uint is we want to make sure we are under the alotted time. */
         pp_step_voltage = voltage_diff / num_of_steps;

        /*Checking if the any of the voltage steps will be above max allowable voltage step before we start ramping */
        if (check_over_max_voltage_step(pp_step_voltage, num_of_steps, low_level_base_voltage, voltage_setpoint) == false)
        {
            /* The check voltage has passed lets set up the parameters for a ramp and set the is_ramping flag to true to start the ppu ramp */
            PP_Ramp_Parameters.total_pp_ramp_steps = num_of_steps; /*Total amount of steps we are running the ramp for.*/
            PP_Ramp_Parameters.current_pp_ramp_step = 1;          /* The current step we are at in our ramp*/
            PP_Ramp_Parameters.final_voltage_setpoint = voltage_setpoint; /*Final Voltage Setpoint we are ramping to*/
            PP_Ramp_Parameters.pp_step_voltage = pp_step_voltage; /*The amount of voltage we are changing from each step in the ramp.*/
            PP_Ramp_Parameters.next_step_voltage = low_level_base_voltage + pp_step_voltage; /*The next voltage setpoint we are setting to. This will increase/decrease by pp_step_voltage per step.*/
            PP_Ramp_Parameters.ten_ms_count_val = 0;    /* Number of 10 ms count we had, this number will increment until we hit the step time, which after decerement back to  0. */
            PP_Ramp_Parameters.is_ramping = true; /*Main flag that is will ramp the ppu using the settings above. */
        }
        else
        {
            error = create_error_float(Errors.CmdInvalidPpRampToHigh, pp_step_voltage);
        }
    }
    return error;
}


Cmd_Err_Code_t DaqCtlAO_PpVset_SetPwmRaw(uint16_t duty_cycle_cnt, bool stop_ppu_ramp)
{
    /* Validate that Pwm_Heater structure has been properly initialized */
    DEV_ASSERT(Pwm_PpVset.state.ftmClockSource != FTM_CLOCK_SOURCE_NONE);

    Cmd_Err_Code_t error = ERROR_NONE;

    status_t status;

    if (stop_ppu_ramp)
    {
        /*If we are ramping when a command comes in to set a specific duty cycle lets stop ramping this command takes precedent over another. */
        reset_ramp_parameters();
    }

    status = PWM_Driver_SetRaw(&Pwm_PpVset, duty_cycle_cnt);
    if (status == STATUS_SUCCESS)
    {
        /* Set the setpt current as the duty cycle percentage */
        RftData.pwm_pp_vset_raw_fb = Pwm_PpVset.duty_cycle_cnt_fb;
        RftData.pp_vset_fb = (RftData.pwm_pp_vset_raw_fb - PT->pp_vset_to_pwm_cnt_cal[0]) / PT->pp_vset_to_pwm_cnt_cal[1];
    }
    else
    {
        error = create_error_uint(Errors.PpvsetPwmRawInvalid, duty_cycle_cnt);
    }

    return error;
}



/* Helper function to determine what is the worst case step in this ramping if we are over a certain rate of ramping we will return true. This will stop a ramp to start.*/
bool check_over_max_voltage_step(float pp_step_voltage, uint32_t total_steps, float inital_voltage,float final_voltage)
{
    if(pp_step_voltage > MAX_PP_RAMP_VOLTAGE_STEP)
    {
        return true;
    }


    /* We are trying to calculate what the final step voltage jump. The reason is that the final step  will always be set to the final voltage.*/
    float voltage_of_final_step = final_voltage - (inital_voltage + (pp_step_voltage*(total_steps-1)));

    /* If the final step of the voltage is over the max_pp_ramp then do not run the ramp. */
    if(voltage_of_final_step >= MAX_PP_RAMP_VOLTAGE_STEP)
    {
        return true;
    }

    return false;
    }



/* If we need to stop the ppu ramp or we need to change the ppu at a given setpoint this function will make sure ramping will stop (setting is_ramping to false) and setting all the ramp values to 0.*/
/*To start a ramp run the DaqCtlAO_Initalize_PP_Ramp function */
void reset_ramp_parameters(void)
{
    PP_Ramp_Parameters.final_voltage_setpoint = 0.0;
    PP_Ramp_Parameters.pp_step_voltage = 0.0;
    PP_Ramp_Parameters.next_step_voltage = 0.0;
    PP_Ramp_Parameters.current_pp_ramp_step = 0;
    PP_Ramp_Parameters.total_pp_ramp_steps = 0;
    PP_Ramp_Parameters.ten_ms_count_val = 0;
    PP_Ramp_Parameters.is_ramping = false;
}
