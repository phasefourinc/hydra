/* Copyright (c) 2017-2020 by Phase Four, Inc. All Rights Reserved. Confidential and Proprietary.
 *
 * DaqCtlAO.h
 *
 *
 * This assume a liner calibration DaqCtlAO_Initalize_PP_Ramp when calculating the voltage setpoint of the pwm for ramping.
 */

#ifndef ACTIVE_OBJECTS_DAQCTLAO_H_
#define ACTIVE_OBJECTS_DAQCTLAO_H_

#include "mcu_types.h"
#include "glados.h"
#include "p4_err.h"
#include "ad768x.h"
#include "pwm_driver.h"

extern AD768x_t Adc0;
extern Pwm_t Pwm_PpVset;

/* Configure the data acquisition rate to 10ms period (100Hz) system frame rate
 *
 * Note: Placing this value into a PT is not an easy or reliable thing to do. The system has been
 * designed around this value and it is unclear what other dependencies or assumptions have been
 * based off of it. Also there would need to be additional checking added to validate the PT is
 * not trying to set something that will kill the system. Not to mention all the additional
 * testing that would be required to validate setting of this value via PT is safe. */
#define DAQCTLAO_PERIOD_US       10000UL

#define DAQCTLAO_PRIORITY        3UL
#define DAQCTLAO_FIFO_SIZE       6UL

/* Each of these must be unique values for the entire system */
enum DaqCtlAO_event_names
{
    EVT_TMR_DAQ_START = (DAQCTLAO_PRIORITY << 16U),
    EVT_DAQ_DMA_COMPLETE,
    EVT_DAQ_DATA_READY
};

extern GLADOS_AO_t AO_DaqCtl;
extern GLADOS_Timer_t Tmr_DaqStart;

void DaqCtlAO_init(void);
void DaqCtlAO_push_interrupts(void);
void DaqCtlAO_state(GLADOS_AO_t *this_ao, GLADOS_Event_t *event);
void DaqCtlAO_state_idle(GLADOS_AO_t *this_ao, GLADOS_Event_t *event);


/* Public Helper Functions */
Cmd_Err_Code_t DaqCtlAO_WF_SetFreq(float freq);
Cmd_Err_Code_t DaqCtlAO_PpVset_SetVoltage(float voltage, bool stop_ppu_ramp);
Cmd_Err_Code_t DaqCtlAO_PpVset_SetPwmRaw(uint16_t duty_cycle_cnt, bool stop_ppu_ramp);
Cmd_Err_Code_t DaqCtlAO_Initalize_PP_Ramp(float voltage_setpoint, float ramp_time_ms);
void reset_ramp_parameters(void);
/*Private Helper Functions */
bool check_over_max_voltage_step(float pp_step_voltage, uint32_t total_steps, float inital_voltage,float final_voltage);

#endif /* ACTIVE_OBJECTS_DAQCTLAO_H_ */
