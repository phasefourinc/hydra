/* Copyright (c) 2017-2020 by Phase Four, Inc. All Rights Reserved. Confidential and Proprietary.
 *
 * ErrAO.h
 */

#ifndef ACTIVE_OBJECTS_ERRAO_H_
#define ACTIVE_OBJECTS_ERRAO_H_

#include "glados.h"
#include "mcu_types.h"
#include "p4_err.h"

/* Error AO FIFO Offset*/
/*This is the FIFO for the Error AO that is sent */
#define ERR_ID_FIFO_BYTE_OFFSET 0X00                   /*decimal = 0   The Error ID of the error from Error AO */
#define EXT_HARD_VIS_FIFO_BYTE_OFFSET 0X02             /*decimal = 2   The offset is to show if the error should be sent to external hardware (spacecraft). */


/* Log Record Header Offset*/
/*This data is located first in sram that is used to save pointers to the other data structures*/
#define SRAM_ADR_HEADER_BYTE_OFFSET 0X00               /*decimal = 0   The SRAM address of the log record*/
#define FLASH_ADR_HEADER_BYTE_OFFSET 0X04              /*decimal = 4   The FLASH address of the log record. */
#define REC_CNT_HEADER_BYTE_OFFSET 0X08                /*decimal = 8   The number of errors that was counted since the last sram to flash transfer. */
#define FRQ_UNIQUE_ERR_CNT_HEADER_BYTE_OFFSET 0X0A     /*decimal = 10   The number of errors that was counted since the last sram to flash transfer. */
#define BOOTLOADER_ERROR_OFFSET 0X0C                   /*decimal = 12   The number of errors that was counted since the last sram to flash transfer. */

/* Log Record Offset*/
/*This data is located first in sram and then is saved in Flash as archival data*/
#define CRR_TIME_RECORD_BYTE_OFFSET 0X00               /*decimal = 0   The current MCU time of the reported error offset.*/
#define ERR_ID_RECORD_BYTE_OFFSET 0X08                 /*decimal = 8   The Error ID of the reported error offset. */
#define AUX_DATA_RECORD_BYTE_OFFSET 0X0A               /*decimal = 10  Auxiliary data of the reported error offset. */
#define CRR_STATE_RECORD_BYTE_OFFSET 0X0E              /*decimal = 14  State of the state machine when the error was reported.*/


/* Telemetry Record Format Offset*/
/*This data is located in sram to be used for telemetry AO for telemetry*/
#define LAST_ERROR_PWR_UP_TELEMETRY_BYTE_OFFSET 0X00   /*decimal = 0   The last error from the last powerup offset */
#define FIRST_ERROR_TELEMETRY_BYTE_OFFSET 0X02         /*decimal = 2   First error offset */
#define LAST_ERROR_TELEMETRY_BYTE_OFFSET 0X14          /*decimal = 20  Last error offset */
#define MFRQ_ERROR_ID_TELEMETRY_BYTE_OFFSET 0X26       /*decimal = 38  The offset is for the Error ID of the most frequent error seen. */
#define TNUMB_ERROR_ID_TELEMETRY_BYTE_OFFSET 0X28      /*decimal = 40  The offset is for the total count of errors of the Error ID that has the most frequent error seen since running.*/
#define TTL_ERROR_CNT_TELEMETRY_BYTE_OFFSET 0X2A       /*decimal = 42  The offset is for to display the total amount of errors reported when booted. */
#define SPC_ERROR_1_TELEMETRY_BYTE_OFFSET 0X2C         /*decimal = 44  The offset is for the first Error ID that should be sent to the spacecraft. */
#define SPC_ERROR_2_TELEMETRY_BYTE_OFFSET 0X2E         /*decimal = 46  The offset is for the second Error ID that should be sent to the spacecraft. */
#define SPC_ERROR_3_TELEMETRY_BYTE_OFFSET 0X30         /*decimal = 48  The offset is for the third Error ID that should be sent to the spacecraft. */



/* Error Frequency Count Offset*/
/*This data is to store how often a error has been seen for this mcu power cycle*/
#define CRR_TIME_FREQCOUN_BYTE_OFFSET 0X00              /*decimal = 0   The current MCU time of the latest reported error offset.*/
#define ERR_ID_FREQCOUNT_BYTE_OFFSET 0X08               /*decimal = 8   The Error ID of the reported error offset. */
#define ERR_ID_CNT_FREQCOUN_BYTE_OFFSET 0X0A            /*decimal = 10  The number of errors that was counted since a mcu power cycle. */


#define RECORD_OFFSET 0x10      /*decimal = 16  This offset is used to jump over record entries from the . */


#define ERRAO_PRIORITY        2UL
#define ERRAO_FIFO_SIZE       15UL

#define FLASH_SECTOR_SIZE 4096U
#define CLEAR_TYPE_INDEX 0U

#define ERROR_RECORD_SECTOR_1_ADDRESS 0x000F0000
#define ERROR_RECORD_SECTOR_2_ADDRESS 0x000F1000
#define ERROR_RECORD_SECTOR_2_LIMIT 0x000F2000


#define TOTAL_AMOUNT_OF_RECORDS_IN_ERROR_SRAM 256U
#define SRAM_ERROR_RECORD_SIZE 16U
#define ERR_FREQ_COUNT_RECORD_SIZE 16U
#define TOTAL_AMOUNT_OF_RECORDS_ERR_FREQ_COUNT 128U
#define TELEM_ERR_LOG_SIZE 50U
#define TELM_ERR_AND_DATA_SIZE 6U
#define TELM_SIZE 2U
#define TOTAL_TLM_ERR_COUNT 6U
#define TOTAL_TLM_SPC_ERR_COUNT 3U
#define LOG_REC_HDR_SIZE 16U

#define MSG_ID_OFFSET 2U
#define TOTAL_SIZE_RECENT_ERRORS_TELM 30U
#define LAST_ERROR_LOCATION 12U

/* Each of these must be unique values for the entire system */

#define SEND_TO_SPACECRAFT        0xFFU
#define DO_NOT_SEND_TO_SPACECRAFT 0x00U

#define TEN_MIN_IN_MS 0x927C0
#define ONE_MIN_IN_US 0x3938700

#define FLASH_SECTOR_ONE_75_PERCENT_BOUNDRY ERROR_RECORD_SECTOR_2_ADDRESS -0x400
#define FLASH_SECTOR_TWO_75_PERCENT_BOUNDRY ERROR_RECORD_SECTOR_2_LIMIT -0x400



enum ErrAO_event_names
{
    EVT_ERR_PROCESS_DONE = (ERRAO_PRIORITY << 16U),
    EVT_TMR_ERR_TIMEOUT,
    EVT_CLEAR_ERRORS,
    EVT_TMR_FLASH_ERASE_DONE,
    EVT_TRANSFER_TO_NVM,
    EVT_CLEAR_FLASH,
    EVT_WRITE_FLASH,
    EVT_NVM_PREPARED,
    EVT_SAVE_FLASH,
    EVT_REPORT_ERR,
    EVT_REPORT_ERR_BULK,
    EVT_RUN_FIDR,
    START_FIDR_RUN,
    EVT_ERR_MAX_TO_SAFE,
};

enum update_type
{ SRAM_ADDRESS = 1,
   FLASH_ADDRESS,
   RECORD_COUNT,
   FREQ_UNIQ_ERR_COUNT
    };


enum overflow_status
{ NO_OVERFLOW = 0,
    OVERFLOW
    };


/*ALWAYS UPDATE ERASE_MEM_OPTIONS! IF ADDING A OPTION*/
enum erase_option
{ CLEAR_COUNT_LOG = 1,
   CLEAR_SRAM_TELM = 2,
   CLEAR_SRAM_ERROR_LOG=4,
   CLEAR_FLASH=8,
    };

typedef struct reported_error_record{
err_type *err_type_pointer;
uint32_t aux_data;
uint8_t glados_state;
uint8_t send_to_spacecraft;
} BYTE_PACKED reported_error_record ;



typedef struct err_freq_record{
uint64_t mcu_time;
uint16_t error_id;
uint16_t err_count;
bool  latest_error;
uint16_t reserved_1;
uint8_t reserved_2;
} BYTE_PACKED err_freq_record ;



typedef struct telm_error_info{
    uint16_t err_id;
    uint32_t aux_data;
}BYTE_PACKED telm_error_info ;


typedef struct telm_most_frq_err_struct{
    uint16_t err_id;
    uint16_t err_count;
}BYTE_PACKED telm_most_frq_err_struct ;


typedef struct telm_err_struct {
uint16_t latest_power_up_err;
telm_error_info first_error[3];
telm_error_info latest_error[3];
uint16_t most_freq_err_id;
uint16_t most_freq_err_amt;
uint16_t spacecraft_err_id;
uint16_t total_err_count;
uint32_t reserved;
} BYTE_PACKED telm_err_struct;


typedef struct log_rec_error_struct{
uint64_t mcu_time;
uint16_t error_id;
uint32_t aux_data;
uint8_t glados_state;
uint8_t reserved;
} BYTE_PACKED log_rec_error_struct;

typedef struct log_rec_header_struct{
uint32_t current_sram_address;
uint32_t current_flash_address;
uint16_t sram_error_record_count;
uint16_t freq_unique_error_count;
uint16_t bootloader_error_id;
uint16_t reserved;
} BYTE_PACKED log_rec_header_struct;



#define ERASE_MEM_OPTIONS 4U


extern log_rec_header_struct log_record_header;
extern log_rec_error_struct log_record_sram [TOTAL_AMOUNT_OF_RECORDS_IN_ERROR_SRAM];
extern uint8_t telemetry_record [TELEM_ERR_LOG_SIZE];
extern err_freq_record error_frequency_count [TOTAL_AMOUNT_OF_RECORDS_ERR_FREQ_COUNT];


extern GLADOS_AO_t AO_Err;
extern GLADOS_Timer_t Tmr_errAOTimeout;
extern GLADOS_Timer_t Tmr_errAOFidr;

void ErrAO_init(void);
void ErrAO_state(GLADOS_AO_t *this_ao, GLADOS_Event_t *event);

int32_t Format_Log_Record(uint16_t err_id);

void ErrAO_CLR_Flash(GLADOS_AO_t *this_ao, GLADOS_Event_t *event);
void ErrAO_TRNS_NVM(GLADOS_AO_t *this_ao, GLADOS_Event_t *event);
void write_sram_record(reported_error_record current_error);
void write_telem_record(reported_error_record current_error);
void update_log_rec_header(uint8_t type,uint32_t data);

void clear_error_frequency_count (void);
void ErrAO_ERASE_SECTOR(GLADOS_AO_t *this_ao, GLADOS_Event_t *event);
void ErrAO_ERASE_SECTOR_PROACTIVE(GLADOS_AO_t *this_ao, GLADOS_Event_t *event);
uint8_t sram_record_overflow_check(void);
void insert_telm_packet(reported_error_record current_error);
void insert_telm_packet_spacecraft_visable(uint16_t current_error_id);
void log_error(reported_error_record current_error);
void clear_error_ao_sram(void);


void report_error_float(GLADOS_AO_t *ao_src, const err_type *err_sent, uint8_t send_to_spacecraft, float aux_data);
void report_error_uint(GLADOS_AO_t *ao_src, const err_type *err_sent, uint8_t send_to_spacecraft, uint32_t aux_data);
void _report_fidr_error(GLADOS_AO_t *ao_src, err_type *err_sent, uint8_t send_to_spacecraft, float aux_data);
uint8_t search_for_longest_and_least_often_error(void);
err_type* find_bootloader_errors(uint16_t err_id);
bool error_filtration_check(err_type * reported_error);
void running_fidr(void);
void clear_error_log(void);
uint32_t search_for_current_flash_address(void);
status_t add_errors_in_flash(uint32_t start_address, uint32_t finish_address);
status_t attempt_write_flash_ao_twice(uint32_t flash_size, uint32_t start_address);
status_t err_log_and_nvm_xfer(err_type *err, uint32_t aux_data);
#endif /* ACTIVE_OBJECTS_CMDAO_H_ */
