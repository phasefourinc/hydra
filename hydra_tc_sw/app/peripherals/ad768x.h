/* Copyright (c) 2017-2020 by Phase Four, Inc. All Rights Reserved. Confidential and Proprietary.
 *
 * ad768x.h
 */

#ifndef PERIPHERALS_AD768X_H_
#define PERIPHERALS_AD768X_H_

/*!
 * @addtogroup AD768x
 * @{
 *
 * @file
 * @brief
 * This file defines the device driver for the AD768X
 * Analog to Digital Converter chip.
 *
 * Datasheet:
 *  - https://www.analog.com/media/en/technical-documentation/data-sheets/AD7682_7689.pdf
 *
 * Hardware configuration:
 *  - 3V <= VDD <= 5.5V decoupled with 10uF and 100nF caps
 *  - Exposed PAD connected to GND (optional)
 *
 * Global Invariants:
 *  - None
 *
 * Hardware considerations:
 *  - Max SPI SCLK is 40MHz at VIO > 3V
 *  - SPI modes: 0
 *  - At least 5us between transfers
 *
 * Usage:
 * Expects DMA transfers to be configured to place ADC data into
 * an array of 16-bit values in channel order (IN0-IN7,TEMP)
 *
 * Temp conversion:
 *  - Datasheet indicates that reported voltage is 0.283V for 25degC and 1degC/0.001V slope,
 *    which leads to the equation:
 *        TEMP = 1000x - 258
 *  - However actual values may differ. Online indicates that actual slope is ~1degC/0.0008V
 *    and it was noted that our ADCs output ~0.268V for 25degC. Should consider making
 *    a calibration step to handle this temperature difference between parts.
 *
 * Configuration Register:
 *
 *   Read ADC Channels
 *     CFG[13]    = 1b, overwrite register
 *     CFG[12:10] = 111b, Unipolar, INx referenced to GND
 *     CFG[9:7]   = xxxb, channel select
 *     CFG[6]     = 1b, full bandwidth
 *     CFG[5:3]   = 001b, for now use internal reference
 *     CFG[2:1]   = 00b, disable channel sequencer
 *     CFG[0]     = 1b, disable CFG readback
 *
 *     Shifted value: 0xF124 - IN0
 *                    0xF324 - IN1
 *                    0xF524 - IN2
 *                    0xF724 - IN3
 *                    0xF924 - IN4
 *                    0xFB24 - IN5
 *                    0xFD24 - IN6
 *                    0xFF24 - IN7
 *
 *   Read Temperature
 *     CFG[13]    = 1b, overwrite register
 *     CFG[12:10] = 011b, temperature sensor
 *     CFG[9:7]   = 000b, don't care since temp sensor reading
 *     CFG[6]     = 1b, full bandwidth
 *     CFG[5:3]   = 001b, for now use internal reference 4.096
 *     CFG[2:1]   = 00b, disable channel sequencer
 *     CFG[0]     = 1b, disable CFG readback
 *
 *     Shifted value: 0xB124
 */


#include "mcu_types.h"
#include "edma_driver.h"

#define AD768X_CAL_POLY_SIZE    4U


typedef enum
{
    IN0 = 0,
    IN1,
    IN2,
    IN3,
    IN4,
    IN5,
    IN6,
    IN7,
    TEMP,
    AD768X_CH_CNT
} AD768x_Ch_t;


typedef struct AD768x_tag
{
    float vref;
    uint16_t *raw_dma_ch_arr;

    /* Calibration Poly Constants for each channel {C0,C1,C2,C3,C4,C5} */
    const float (*cal_poly_cn)[AD768X_CAL_POLY_SIZE];

    float ch_voltage[AD768X_CH_CNT];

} AD768x_t;


/*!
 * @brief  This function initializes the ADC structure
 *
 * @note   The cal_poly_cn address must fall on a 4-byte aligned address,
 *         otherwise MCU hardfault errors may arise due to floating-point
 *         instruction errors.
 *
 * @pre @p adc is not NULL
 * @pre @p raw_dma_ch_arr is not NULL
 * @pre @p cal_poly_cn is not NULL and 4-byte aligned
 * @pre @p vref is finite and greater than zero
 *
 * @param[out] adc        The ADC structure to initialize
 * @param raw_dma_ch_arr  Pointer to the eDMA array that holds all read
 *                        channels in order from IN0-IN7,TEMP
 * @param cal_poly_cn     Pointer to 2D array, and array of calibration
 *                        polynomial constants {C0,C1..Cn} arrays, one
 *                        for each ADC channel.
 * @param vref            The reference voltage supplied to Vref
 */
void AD768x_Init(AD768x_t *adc,
                 uint16_t raw_dma_ch_arr[AD768X_CH_CNT],
                 const float cal_poly_cn[AD768X_CH_CNT][AD768X_CAL_POLY_SIZE],
                 float vref);


/*!
 * @brief  Converts all channels from the DMA Rx array into channel
 *         voltages stored within the ADC structure.
 *
 * @pre @p adc is not NULL and has been initialized
 *
 * @param adc  The ADC structure
 */
void AD768x_RawToVoltage_AllChannels(AD768x_t *adc);


/*!
 * @brief  Returns the calibrated value of the selected channel.
 *
 * @pre @p adc is not NULL and has been initialized
 *
 * @param adc  The ADC structure
 * @param ch   The channel to calibrate
 * @return     The calibrated value
 */
float AD768x_CalibrateChannel(AD768x_t *adc, AD768x_Ch_t ch);

/*! @} */

#endif /* PERIPHERALS_AD768X_H_ */
