/* Copyright (c) 2017-2020 by Phase Four, Inc. All Rights Reserved. Confidential and Proprietary.
 *
 * LTC6903.h
 */

#ifndef LTC6903_H_
#define LTC6903_H_

/*!
 * @addtogroup LTC6903
 * @{
 *
 * @file
 * @brief
 * This module defines the device driver for the LTC6903 I2C Oscillator
 * with a range between 1KHz to 64MHz square wave output.  This is a
 * receive-only (slave) device.
 *
 * Datasheet:
 *  - http://cds.linear.com/docs/en/datasheet/69034fe.pdf
 *
 * freq = 2^OCT * 2078 / (2 - (DAC / 1024))
 *
 * where:
 *  - OCT is 4-bit code determined by: OCT = 3.322log(freq/1039) and rounded down
 *  - DAC is 10-bit code determined by: DAC = 2048-((2078*2^(10+OCT))/freq) and rounded to nearest integer
 *
 * Hardware configuration:
 *  - 2.7 <= VCC <= 5.5 bypassed to GND with 0.1uF cap
 *  - ADR pin sets the I2C address
 *  - Output Enable (OE) pin set high to enable clock out
 *
 * Global Invariants:
 *  - Changing the OCT and DAC simultaneously is performed, even though it can lead to "temporary
 *    excursion beyond the requested frequencies"
 *
 * Hardware considerations:
 *  - Unused CLK outputs should be disabled to reduce power dissipation and improve accuracy
 *  - Power-up output frequency is 1039Hz
 *  - Frequency accuracy is within 2%, Jitter is within 1%
 *  - Output CLK pins can drive up to 5pF, note that an O-scope is between 10-15pF
 *  - Capacitor loading on the output CLK becomes a greater factor at 1MHz+
 *  - Settling times for changes in frequency depend on whether the OCT or DAC
 *    values are changing from the prior setting:
 *     + OCT changes < 10 are instantaneous, OCT >= 10 take 100us to settle
 *     + DAC changes take 100us to settle
 *     + OCT and DAC changes simultaneously "may result in considerable excursion
 *       beyond the frequencies requested before settling"
 *  - Max supported I2C clock is 100KHz
 *  - I2C data is MSB first, 8 bits per transfer
 *  - 4.7us minimum free time between STOP and START condition
 *
 * @verbatim
 * I2C Command Format
 *  --------------------------------------------------------------------------------
 * |OCT3|OCT2|OCT1|OCT0|DAC9|DAC8|DAC7|DAC6|DAC5|DAC4|DAC3|DAC2|DAC1|DAC0|CNF1|CNF0|
 *  --------------------------------------------------------------------------------
 * @endverbatim
 */

#include "mcu_types.h"

/* Custom device defines */

#define LTC6903_MAX_OUT_FREQ       (68030000.0f - 1.0f)
#define LTC6903_MIN_OUT_FREQ       1039.0f
#define LTC6903_DFLT_TIMEOUT_MS    2U

/*! CLock Configuration for CNFx bits. Disabling unused output reduces
 *  power dissipation and improves accuracy */
typedef enum
{
    LTC6903_CNF_BOTH_ENBL = 0,  /*!< CLK and /CLK enabled */
    LTC6903_CNF_NCLK_ONLY = 1,  /*!< /CLK enabled only */
    LTC6903_CNF_CLK_ONLY  = 2,  /*!< CLK enabled only */
    LTC6903_CNF_BOTH_DSBL = 3   /*!< CLK and /CLK disabled */
} LTC6903_CNF_t;


/* Public function declarations */

/*!
 * @brief  This function initializes the provided Oscillator Sensor device and its
 *         peripheral.
 *
 * @note   This function expects to be the first function called prior to
 *         communicating with the peripheral.
 *
 * @post @p device handle has been initialized even if a communication error status
 *       is returned
 *
 * @param instance  The selected SPI Oscillator instance handle
 * @return          The status of the SPI communication with the device
 */
status_t LTC6903_Osc_Init(uint32_t instance);


/*!
 * @brief  This function sets the Oscillator chip to the provided frequency
 *         and output configuration.
 *
 * @note   The provided frequency is railed if beyond the maximum (to 68.03MHz)
 *         or minimum (to 1.039KHz) supported frequencies.
 *
 * @pre @p device is not NULL
 * @pre @p freq is not +/-NaN, +/-INF
 *
 * @param instance  The selected Oscillator device handle
 * @param freq      The desired frequency in hertz
 * @param cnf       The output configuration setting
 * @return          The status of the I2C communication with the device
 */
status_t LTC6903_Osc_SetFreq(uint32_t instance,
                             float freq_hz,
                             LTC6903_CNF_t cnf);


/*!
 * @brief  This function sets the desired output frequency based on the
 *         provided OCT and DAC codes and output configuration.
 *
 * @pre @p device is not NULL
 *
 * @param instance  The selected Oscillator device handle
 * @param oct       The 4-bit OCT code
 * @param dac       The 10-bit DAC code
 * @param cnf       The output configuration setting
 * @return          The status of the I2C communication with the device
 */
status_t LTC6903_Osc_SetFreqRaw(uint32_t instance,
                                uint16_t oct,
                                uint16_t dac,
                                LTC6903_CNF_t cnf);

/*!@} */

#endif /* LTC6903_H_ */
