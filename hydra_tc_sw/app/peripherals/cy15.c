/* Copyright (c) 2018-2020 by Phase Four, Inc. All Rights Reserved. Confidential and Proprietary.
 *
 * cy15.c
 */

#include "cy15.h"
#include "mcu_types.h"
#include "lpspi_master_driver.h"
#include "string.h"

static uint8_t xfer_tx_buf[CY15_MAX_XFER_SIZE] = { 0U };
static uint8_t xfer_rx_buf[CY15_MAX_XFER_SIZE] = { 0U };


status_t CY15_FRAM_Init(uint32_t instance, uint8_t cs)
{
    enum { RX_SIZE = 10U, SUPP_ID_CNT = 2, ID_LENGTH = 3 };

    uint8_t curr_id;
    uint32_t read_id_b0;
    uint32_t read_id_b1;
    uint32_t read_id_b2;
    status_t status = STATUS_ERROR;
    uint8_t rx_buf[RX_SIZE] = {0U};

    /* Supporting multiple device IDs. If one fails, check the others */
    uint32_t exp_ids[SUPP_ID_CNT][ID_LENGTH] = {
        {0x0000007FUL, 0x7F7F7F7FUL, 0x7FC22608UL},    /* CY15B104Q-SXI */
        {0x00000040UL, 0x2CC27F7FUL, 0x7F7F7F7FUL},    /* CY15V104QN-50SXA */
    };

    status = CY15_FRAM_ReadID(instance, cs, &rx_buf[0]);

    /* IDs are 9 bytes long, so configure into 3 U32s. The first Rx byte is junk
     * received when transmitting the SPI command byte. */
    read_id_b0 = (uint32_t)rx_buf[1];

    read_id_b1 = ((uint32_t)rx_buf[2] << 24U) +
                 ((uint32_t)rx_buf[3] << 16U) +
                 ((uint32_t)rx_buf[4] << 8U) +
                 ((uint32_t)rx_buf[5]);

    read_id_b2 = ((uint32_t)rx_buf[6] << 24U) +
                 ((uint32_t)rx_buf[7] << 16U) +
                 ((uint32_t)rx_buf[8] << 8U) +
                 ((uint32_t)rx_buf[9]);

    if (status == STATUS_SUCCESS)
    {
        status = STATUS_ERROR;
        for (curr_id = 0; (curr_id < SUPP_ID_CNT) && (status != STATUS_SUCCESS); ++curr_id)
        {
            if ((exp_ids[curr_id][0] == read_id_b0) &&
                (exp_ids[curr_id][1] == read_id_b1) &&
                (exp_ids[curr_id][2] == read_id_b2))
            {
                status = STATUS_SUCCESS;
            }
        }
    }

    return status;
}


status_t CY15_FRAM_ReadID(uint32_t instance, uint8_t cs, uint8_t *rx_buf)
{
    enum { CMD_READ_ID_SIZE = 10U };

    status_t status = STATUS_ERROR;
    uint8_t tx_buf[CMD_READ_ID_SIZE] = {0U};

    /* Set the command byte */
    tx_buf[0] = CY15_CMD_RDID;

    /* Set the Chip Select */
    (void)LPSPI_DRV_SetPcs(instance, cs, LPSPI_ACTIVE_LOW);

    status = LPSPI_DRV_MasterTransferBlocking(instance,  &tx_buf[0], rx_buf, CMD_READ_ID_SIZE, CY15_DEFAULT_TIMEOUT_MS);

    return status;
}


status_t CY15_FRAM_SetWEL(uint32_t instance, uint8_t cs, bool enable)
{
    /* Note: Old code had a XFER_SIZE of 4 bytes to work around a race condition that caused the
     * SPI xfer Done to not be called in the dspi_driver. This no longer seems to affect the FSW.
     * Returning to 1B transfer size since not all FRAM chips can handle the additional bytes.*/
    enum Constants { XFER_SIZE = 1U };

    status_t status = STATUS_ERROR;
    uint8_t tx_buf[XFER_SIZE] = {0U};
    uint8_t rx_buf[XFER_SIZE] = {0U};

    /* Set the Chip Select */
    (void)LPSPI_DRV_SetPcs(instance, cs, LPSPI_ACTIVE_LOW);

    /* Check the feature table for the OIP feature at address 0xC0 */
    tx_buf[0] = (enable) ? CY15_CMD_WREN : CY15_CMD_WRDI;
    status = LPSPI_DRV_MasterTransferBlocking(instance, &tx_buf[0], &rx_buf[0], XFER_SIZE, CY15_DEFAULT_TIMEOUT_MS);

    return status;
}


status_t CY15_FRAM_ReadStatus(uint32_t instance, uint8_t cs, uint8_t *reg_value)
{
    enum Constants { XFER_SIZE = 2U, STATUS_RX_IDX = 1U };

    status_t status = STATUS_ERROR;
    uint8_t tx_buf[XFER_SIZE] = {0U};
    uint8_t rx_buf[XFER_SIZE] = {0U};

    /* Set the Chip Select */
    (void)LPSPI_DRV_SetPcs(instance, cs, LPSPI_ACTIVE_LOW);

    /* Check the feature table for the OIP feature at address 0xC0 */
    tx_buf[0] = CY15_CMD_RDSR;
    status = LPSPI_DRV_MasterTransferBlocking(instance, &tx_buf[0], &rx_buf[0], XFER_SIZE, CY15_DEFAULT_TIMEOUT_MS);

    if (status == STATUS_SUCCESS)
    {
        *reg_value = rx_buf[STATUS_RX_IDX];
    }

    return status;

}


status_t CY15_FRAM_WriteStatus(uint32_t instance, uint8_t cs, uint8_t reg_value)
{
    enum Constants { XFER_SIZE = 2U };

    status_t status = STATUS_ERROR;
    uint8_t tx_buf[XFER_SIZE] = {0U};
    uint8_t rx_buf[XFER_SIZE] = {0U};

    /* Set the Chip Select */
    (void)LPSPI_DRV_SetPcs(instance, cs, LPSPI_ACTIVE_LOW);

    /* Check the feature table for the OIP feature at address 0xC0 */
    tx_buf[0] = CY15_CMD_WRSR;
    tx_buf[1] = reg_value;
    status = LPSPI_DRV_MasterTransferBlocking(instance, &tx_buf[0], &rx_buf[0], XFER_SIZE, CY15_DEFAULT_TIMEOUT_MS);

    return status;
}


status_t CY15_FRAM_ReadBlock_NonBlocking(uint32_t instance, uint8_t cs, uint32_t addr, uint16_t num_of_bytes, uint8_t **read_buf)
{
    DEV_ASSERT(read_buf);

    enum Constants { ADDR_MASK = 0x7FFFFU, RX_DATA_START_IDX = 4U };

    status_t status = STATUS_ERROR;
    uint16_t xfer_size = num_of_bytes + RX_DATA_START_IDX;

    if (num_of_bytes <= CY15_BLOCK_SIZE)
    {
        /* Set the Chip Select */
        (void)LPSPI_DRV_SetPcs(instance, cs, LPSPI_ACTIVE_LOW);

        /* Address is 19-bits */
        addr = addr & ADDR_MASK;

        memset(&xfer_tx_buf[0], 0, sizeof(xfer_tx_buf));
        memset(&xfer_rx_buf[0], 0, sizeof(xfer_rx_buf));

        xfer_tx_buf[0] = CY15_CMD_READ;
        xfer_tx_buf[1] = (uint8_t)((addr >> 16U) & 0xFFUL);
        xfer_tx_buf[2] = (uint8_t)((addr >> 8U)  & 0xFFUL);
        xfer_tx_buf[3] = (uint8_t)((addr)        & 0xFFUL);
        status = LPSPI_DRV_MasterTransfer(instance, &xfer_tx_buf[0], &xfer_rx_buf[0], xfer_size);
    }

    *read_buf = &xfer_rx_buf[RX_DATA_START_IDX];

    return status;
}


/* NOTE: Blocking SPI block transfers require >5MHz clock due to hard timeout code implementation */
status_t CY15_FRAM_ReadBlock(uint32_t instance, uint8_t cs, uint32_t addr, uint16_t num_of_bytes, uint8_t **read_buf)
{
    DEV_ASSERT(read_buf);

    enum Constants { ADDR_MASK = 0x7FFFFU, RX_DATA_START_IDX = 4U };

    status_t status = STATUS_ERROR;
    uint16_t xfer_size = num_of_bytes + RX_DATA_START_IDX;

    if (num_of_bytes <= CY15_BLOCK_SIZE)
    {
        /* Set the Chip Select */
        (void)LPSPI_DRV_SetPcs(instance, cs, LPSPI_ACTIVE_LOW);

        /* Address is 19-bits */
        addr = addr & ADDR_MASK;

        memset(&xfer_tx_buf[0], 0, sizeof(xfer_tx_buf));
        memset(&xfer_rx_buf[0], 0, sizeof(xfer_rx_buf));

        xfer_tx_buf[0] = CY15_CMD_READ;
        xfer_tx_buf[1] = (uint8_t)((addr >> 16U) & 0xFFUL);
        xfer_tx_buf[2] = (uint8_t)((addr >> 8U)  & 0xFFUL);
        xfer_tx_buf[3] = (uint8_t)((addr)        & 0xFFUL);
        status = LPSPI_DRV_MasterTransferBlocking(instance, &xfer_tx_buf[0], &xfer_rx_buf[0], xfer_size, 2/*CY15_DEFAULT_TIMEOUT_MS*/);
    }

    *read_buf = &xfer_rx_buf[RX_DATA_START_IDX];

    return status;
}


status_t CY15_FRAM_WriteBlock_NonBlocking(uint32_t instance, uint8_t cs, uint32_t addr, uint16_t num_of_bytes, uint8_t *write_buf)
{
    DEV_ASSERT(write_buf);

    enum Constants { ADDR_MASK = 0x7FFFFU, TX_DATA_START_IDX = 4U };

    status_t status = STATUS_ERROR;
    uint16_t xfer_size = num_of_bytes + TX_DATA_START_IDX;

    /* Validate that the transfer size does not exceed the block size
     * Note: LPSPI driver already handles transfers of 0 size with success. */
    if (num_of_bytes <= CY15_BLOCK_SIZE)
    {
        /* Set the Chip Select */
        (void)LPSPI_DRV_SetPcs(instance, cs, LPSPI_ACTIVE_LOW);

        /* Address is 19-bits */
        addr = addr & ADDR_MASK;

        memset(&xfer_tx_buf[0], 0, sizeof(xfer_tx_buf));
        memset(&xfer_rx_buf[0], 0, sizeof(xfer_rx_buf));

        xfer_tx_buf[0] = CY15_CMD_WRITE;
        xfer_tx_buf[1] = (uint8_t)((addr >> 16U) & 0xFFUL);
        xfer_tx_buf[2] = (uint8_t)((addr >> 8U)  & 0xFFUL);
        xfer_tx_buf[3] = (uint8_t)((addr)        & 0xFFUL);
        memcpy(&xfer_tx_buf[TX_DATA_START_IDX], &write_buf[0], num_of_bytes);

        status = LPSPI_DRV_MasterTransfer(instance, &xfer_tx_buf[0], &xfer_rx_buf[0], xfer_size);
    }

    return status;
}


/* NOTE: Blocking SPI block transfers require >5MHz clock due to hard timeout restriction */
status_t CY15_FRAM_WriteBlock(uint32_t instance, uint8_t cs, uint32_t addr, uint16_t num_of_bytes, uint8_t *write_buf)
{
    DEV_ASSERT(write_buf);

    enum Constants { ADDR_MASK = 0x7FFFFU, TX_DATA_START_IDX = 4U };

    status_t status = STATUS_ERROR;
    uint16_t xfer_size = num_of_bytes + TX_DATA_START_IDX;

    if (num_of_bytes <= CY15_BLOCK_SIZE)
    {
        /* Set the Chip Select */
        (void)LPSPI_DRV_SetPcs(instance, cs, LPSPI_ACTIVE_LOW);

        /* Address is 19-bits */
        addr = addr & ADDR_MASK;

        memset(&xfer_tx_buf[0], 0, sizeof(xfer_tx_buf));
        memset(&xfer_rx_buf[0], 0, sizeof(xfer_rx_buf));

        xfer_tx_buf[0] = CY15_CMD_WRITE;
        xfer_tx_buf[1] = (uint8_t)((addr >> 16U) & 0xFFUL);
        xfer_tx_buf[2] = (uint8_t)((addr >> 8U)  & 0xFFUL);
        xfer_tx_buf[3] = (uint8_t)((addr)        & 0xFFUL);
        memcpy(&xfer_tx_buf[TX_DATA_START_IDX], &write_buf[0], num_of_bytes);

        status = LPSPI_DRV_MasterTransferBlocking(instance, &xfer_tx_buf[0], &xfer_rx_buf[0], xfer_size, CY15_DEFAULT_TIMEOUT_MS);
    }

    return status;
}


/* NOTE: Blocking SPI block transfers require >5MHz clock due to hard timeout restriction */
status_t CY15_FRAM_Write(uint32_t instance, uint8_t cs, uint32_t addr, uint16_t num_of_bytes, uint8_t *write_buf)
{
    DEV_ASSERT(write_buf);

    status_t status = STATUS_ERROR;
    uint32_t i;
    uint16_t curr_offset = 0U;
    uint32_t num_of_xfers = num_of_bytes / CY15_BLOCK_SIZE;
    uint32_t bytes_remaining = num_of_bytes - (num_of_xfers * CY15_BLOCK_SIZE);

    /* Check for valid address accessing range */
    if (((addr + num_of_bytes) <= CY15_MEMORY_SIZE) &&
         (addr                 <  CY15_MEMORY_SIZE))
    {
        status = STATUS_SUCCESS;
        for (i = 0UL; (i < num_of_xfers) && (status == STATUS_SUCCESS); ++i)
        {
            status |= CY15_FRAM_SetWEL(instance, cs, true);
            status |= CY15_FRAM_WriteBlock(instance, cs, addr + curr_offset, CY15_BLOCK_SIZE, &write_buf[curr_offset]);
            curr_offset += CY15_BLOCK_SIZE;
        }

        if ((bytes_remaining > 0UL) && (status == STATUS_SUCCESS))
        {
            status |= CY15_FRAM_SetWEL(instance, cs, true);
            status |= CY15_FRAM_WriteBlock(instance, cs, addr + curr_offset, bytes_remaining, &write_buf[curr_offset]);
        }
    }

    return status;
}


void CY15_FRAM_AbortTransfer(uint32_t instance)
{
    (void)LPSPI_DRV_MasterAbortTransfer(instance);
    return;
}

