/* Copyright (c) 2017-2020 by Phase Four, Inc. All Rights Reserved. Confidential and Proprietary.
 *
 * ad768x.c
 */


#include "ad768x.h"
#include "mcu_types.h"
#include <string.h>
#include <math.h>


/*!
 * @brief  Calculates and returns the actual voltage given the raw ADC
 *         counts and provided reference voltage.
 *
 * @pre @p vref is finite
 *
 * @param vref         The reference voltage
 * @param raw_adc_val  The raw ADC count
 * @return             The actual voltage
 */
static float raw_to_voltage(float vref, uint16_t raw_adc_val);


void AD768x_Init(AD768x_t *adc,
                 uint16_t raw_dma_ch_arr[AD768X_CH_CNT],
                 const float cal_poly_cn[AD768X_CH_CNT][AD768X_CAL_POLY_SIZE],
                 float vref)
{
    DEV_ASSERT(adc);
    DEV_ASSERT(isfinite(vref));
    DEV_ASSERT(vref > 0.0f);
    DEV_ASSERT(cal_poly_cn);
    DEV_ASSERT(((uint32_t)cal_poly_cn & 0x3UL) == 0x0UL);  /* Validate 4-byte alignment for FP */

    adc->vref = vref;
    adc->raw_dma_ch_arr = &raw_dma_ch_arr[0];
    adc->cal_poly_cn = &cal_poly_cn[0];
    memset(&adc->ch_voltage[0], 0, sizeof(adc->ch_voltage));

    return;
}


void AD768x_RawToVoltage_AllChannels(AD768x_t *adc)
{
    DEV_ASSERT(adc);
    DEV_ASSERT(adc->raw_dma_ch_arr);
    DEV_ASSERT(adc->ch_voltage);
    DEV_ASSERT(isfinite(adc->vref));

    adc->ch_voltage[IN0]  = raw_to_voltage(adc->vref, adc->raw_dma_ch_arr[IN0]);
    adc->ch_voltage[IN1]  = raw_to_voltage(adc->vref, adc->raw_dma_ch_arr[IN1]);
    adc->ch_voltage[IN2]  = raw_to_voltage(adc->vref, adc->raw_dma_ch_arr[IN2]);
    adc->ch_voltage[IN3]  = raw_to_voltage(adc->vref, adc->raw_dma_ch_arr[IN3]);
    adc->ch_voltage[IN4]  = raw_to_voltage(adc->vref, adc->raw_dma_ch_arr[IN4]);
    adc->ch_voltage[IN5]  = raw_to_voltage(adc->vref, adc->raw_dma_ch_arr[IN5]);
    adc->ch_voltage[IN6]  = raw_to_voltage(adc->vref, adc->raw_dma_ch_arr[IN6]);
    adc->ch_voltage[IN7]  = raw_to_voltage(adc->vref, adc->raw_dma_ch_arr[IN7]);
    adc->ch_voltage[TEMP] = raw_to_voltage(adc->vref, adc->raw_dma_ch_arr[TEMP]);

    return;
}


float AD768x_CalibrateChannel(AD768x_t *adc, AD768x_Ch_t ch)
{
    DEV_ASSERT(adc);

    uint32_t i;
    float cal_value = 0.0f;
    float x = adc->ch_voltage[ch];
    float xn = adc->ch_voltage[ch];

    /* Calibrate the ADC value: C0 + C1x + C2x^2 + ... */
    cal_value = adc->cal_poly_cn[ch][0];
    for (i = 1UL; i < AD768X_CAL_POLY_SIZE; ++i)
    {
        cal_value += adc->cal_poly_cn[ch][i] * xn;
        xn *= x;
    }

    /* Return a value converted to fixed point */
    return cal_value;
}


static float raw_to_voltage(float vref, uint16_t raw_adc_val)
{
    DEV_ASSERT(isfinite(vref));
    return (((float)raw_adc_val / 65535.0f) * vref);
}
