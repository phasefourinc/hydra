/* Copyright (c) 2017-2020 by Phase Four, Inc. All Rights Reserved. Confidential and Proprietary.
 *
 * LTC6903.c
 */

#include "ltc6903.h"
#include "mcu_types.h"
#include "lpspi_master_driver.h"
#include <math.h>

#define DATA_WORD_SIZE  2U

#define OCT_MASK        0xFU
#define OCT_SHIFT       12U

#define DAC_MASK        0x3FFU
#define DAC_SHIFT       2U


status_t LTC6903_Osc_Init(uint32_t instance)
{
    status_t status = STATUS_ERROR;

    /* Reset to default */
    status = LTC6903_Osc_SetFreqRaw(instance, 0U, 0U, LTC6903_CNF_BOTH_ENBL);

    return status;
}


status_t LTC6903_Osc_SetFreq(uint32_t instance,
                             float freq_hz,
                             LTC6903_CNF_t cnf)
{
    DEV_ASSERT(isfinite(freq_hz));

    status_t status = STATUS_ERROR;
    float oct_f32 = 0.0f;
    float dac_f32 = 0.0f;
    uint16_t oct_u16 = 0U;
    uint16_t dac_u16 = 0U;

    /* Rail any values that are out of range to ensure float calculation
     * performs without any issues. */
    if (isfinite(freq_hz))
    {
        if (freq_hz > LTC6903_MAX_OUT_FREQ)
        {
            freq_hz = LTC6903_MAX_OUT_FREQ;
        }
        else if (freq_hz < LTC6903_MIN_OUT_FREQ)
        {
            freq_hz = LTC6903_MIN_OUT_FREQ;
        }

        /* Calculate the OCT and round down to nearest integer via integer cast */
        oct_f32 = 3.322f * log10f(freq_hz/1039.0f);
        oct_u16 = (uint16_t)oct_f32;

        /* Calculate the DAC and round to nearest integer via integer cast */
        dac_f32 = 2048.0f - ((2078.0f * powf(2.0f, 10.0f + (float)oct_u16)) / freq_hz);
        dac_u16 = (uint16_t)(dac_f32 + 0.5f);

        status = LTC6903_Osc_SetFreqRaw(instance, oct_u16, dac_u16, cnf);
    }

    return status;
}


status_t LTC6903_Osc_SetFreqRaw(uint32_t instance,
                                uint16_t oct,
                                uint16_t dac,
                                LTC6903_CNF_t cnf)
{
    status_t status;

    uint16_t data_word = ((oct & OCT_MASK) << OCT_SHIFT) +
                         ((dac & DAC_MASK) << DAC_SHIFT) +
                         (uint16_t)cnf;

    uint8_t tx_buf[DATA_WORD_SIZE] = {0U};
    uint8_t rx_buf[DATA_WORD_SIZE] = {0U};

    /* Note: Since transfers are 16-bits each, MCU tries to be smart and flip bytes
     * around from LE to BE behind the scenes. However, since passing in a byte array,
     * this byte swap is undesirable and must be accounted for here. */
    tx_buf[1] = (data_word >> 8U) & 0xFFU;
    tx_buf[0] = (data_word)       & 0xFFU;

    status = LPSPI_DRV_MasterTransferBlocking(instance, &tx_buf[0], &rx_buf[0], DATA_WORD_SIZE, LTC6903_DFLT_TIMEOUT_MS);

    return status;
}

