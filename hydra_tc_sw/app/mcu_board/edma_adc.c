/* Copyright (c) 2017-2020 by Phase Four, Inc. All Rights Reserved. Confidential and Proprietary.
 *
 * edma_adc.c
 */


#include "edma_adc.h"
#include "mcu_types.h"
#include "lpspi_master_driver.h"
#include "lpspi_hw_access.h"
#include "edma_driver.h"
#include "edma_hw_access.h"
#include "dmaController0.h"
#include <string.h>

/* eDMA notes:
 *  - If scatter-gather is enabled (SCE), then TCD will automatically load the
 *    next TCD pointed to by the SCG Next Descriptor Address upon completion
 *  - For the last TCD in a scatter-gather, the CSR[DREQ] must be set to
 *    disable the eDMA channel upon TCD completion.
 *
 * Trouble shooting note:
 * If no SPI info is being transmitted, it's possible that it's because the
 * eDMA channels are not being enabled. Recommend first double checking that
 * EDMA_CHN4_NUMBER and EDMA_CHN5_NUMBER are, in fact, channels 4 and 5 since
 * it appears that Processor Expert actually enumerates the channel numbers
 * as an index in their SW structure rather than giving it the number for the
 * channel it's actually configured for. Check out dmaController0.c to validate
 * the channel numbers line up with the #defines.
 *
 * eDMA debugging tips:
 *  - Set DMA->CR to halt on debug and halt on error
 *        DMA->CR |= (DMA_CR_EDBG_MASK | DMA_CR_HOE_MASK);
 *  - Set assert/breakpoint in DMA Interrupt handlers to cause it to pause when an error hits
 *  - Inspect DMA->ER for the TCDn source and error cause
 *  - Use logic analyzer to determine how many SPI transfers are occurring
 *  - Validate all configs have been pushed to TCDs via EDMA_DRV_PushConfigToSTCD()
 *        (likely failure is DMA->ER will report an NBYTES/CITER error)
 *  - Validate that each TCD either has
 *      + Scatter-gather (SGE) enabled with a valid next TCD address
 *          (likely failure is DMA->ER will report an NBYTES/CITER error)
 *      OR
 *      + SGE is disabled and the last TCD has DMA->CR[DREQ] Disable Request
 *          set to 1 to end the transfer (otherwise will continue forever).
 *          Note: Setting DREQ is a manual process that must be done after TCDs
 *          are pushed via EDMA_DRV_PushConfigToSTCD
 *
 * Future enhancements:
 *  - There is no real need to store the SW TCD structures, once they are converted
 *    and copied to the actual TCD array for scatter-gather, there is no reason to
 *    keep them around.  The reason to keep them around is for clarity, maintenance,
 *    and for ease in adding additional TCDs.  If memory space is needed, then one
 *    option is to get the actual TCD arrays for both SPI2 Tx and Rx and just copy
 *    them directly into spi2_tx_stcd_arr[] and spi2_tx_stcd_arr[].  Once this is
 *    done, users can load the first STCD into the appropriate TCD reg via direct
 *    copy.  When attempting this, it appears there were some issues getting a memcpy
 *    going.  The structures not being perfectly aligned could have been the culprit.
 */


/* The TCR values to write via DMA in order to switch
 * between Chip Select 0 and 1 */
static uint32_t adc_tx_tcr_pcs[] =
{
    0x0000000FUL
};

/* Generic ADC channel read sequence to read all channels
 * and the temperature sensor.  The commanded read order
 * reads channels IN0 to IN7 followed by the Temperature
 * sensor.  Due to the nature of reading the ADC, the actual
 * commanded read values are returned 2 commands later.
 * Therefore the last two commands are dummies in order to
 * read the IN7 and Temp values.
 * Note: Since SPI is configured for 16-bit width transfers,
 * the user should use 16-bit values when defining this array
 * since the MCU is smart enough (for better or worse) to
 * byte swap them from LE to BE format. E.g. if changing to
 * uint8_t[] then values will need to be [0x44, 0xF1, ...] */
static uint16_t adc_tx_cmds[EDMA_ADC_TX_CMDS_CNT] =
{
    0xF144,    /* IN0 */
    0xF344,    /* IN1 */
    0xF544,    /* IN2 */
    0xF744,    /* IN3 */
    0xF944,    /* IN4 */
    0xFB44,    /* IN5 */
    0xFD44,    /* IN6 */
    0xFF44,    /* IN7 */
    0xB144,    /* TEMP */
    0xF144,    /* IN0 (dummy) */
    0xF144     /* IN0 (dummy) */
};

#define SPI2_TX_TCD_CNT    2
#define SPI2_RX_TCD_CNT    1

static edma_software_tcd_t spi2_tx_stcd_arr[SPI2_TX_TCD_CNT] SECTION(".spi2_tx_dma_tcd") = { {0} };
static edma_software_tcd_t spi2_rx_stcd_arr[SPI2_RX_TCD_CNT] SECTION(".spi2_rx_dma_tcd") = { {0} };


/* Forward declare the TCD arrays, sizes set after TCD definition.
 * These arrays hold the next TCDs for the eDMA to pull from when
 * using scatter-gather */



/* SPI2 Transmit DMA TCD
 *
 * Sequence:
 *    1. Set SPI2 Periph Chip Select to ADC0
 *    2. Perform ADC0 CMD sequence (11 commands, 8 ch and 1 temp reads)
 *    3. Set SPI2 Periph Chip Select to ADC1
 *    4. Perform ADC1 CMD sequence (11 commands, 8 ch and 1 temp reads)
 *    5. Return SPI2 Periph Chip Select to ADC0
 */

static edma_loop_transfer_config_t spi2_tx_edma_loop_cfg_adc =
{
    .majorLoopIterationCount = EDMA_ADC_TX_CMDS_CNT,
    .srcOffsetEnable = false,
    .dstOffsetEnable = false,
    .minorLoopOffset = 0L,
    .minorLoopChnLinkEnable = false,
    .minorLoopChnLinkNumber = 0U,
    .majorLoopChnLinkEnable = false,
    .majorLoopChnLinkNumber = 0U
};

static edma_loop_transfer_config_t spi2_tx_edma_loop_cfg_pcs =
{
    .majorLoopIterationCount = 1,
    .srcOffsetEnable = false,
    .dstOffsetEnable = false,
    .minorLoopOffset = 0L,
    .minorLoopChnLinkEnable = false,
    .minorLoopChnLinkNumber = 0U,
    .majorLoopChnLinkEnable = false,
    .majorLoopChnLinkNumber = 0U
};

static const edma_transfer_config_t spi2_tx_edma_tcds[SPI2_TX_TCD_CNT] =
{
    {   /* Set SPI2 Peripheral Chip Select to ADC0 */
        .srcAddr = (uint32_t)&adc_tx_tcr_pcs[0],     /* Source address is the SPI Tx array */
        .destAddr = (uint32_t)&LPSPI2->TCR,          /* Destination is the Transmit Data Reg */
        .srcTransferSize = EDMA_TRANSFER_SIZE_4B,    /* Perform 1 byte transfer */
        .destTransferSize = EDMA_TRANSFER_SIZE_4B,   /* Perform 1 byte transfer */
        .srcOffset = 4,                              /* Not applicable since only single transfer */
        .destOffset = 0,                             /* Not applicable since only single transfer */
        .srcLastAddrAdjust = 0L,
        .destLastAddrAdjust = 0L,
        .srcModulo = EDMA_MODULO_OFF,
        .destModulo = EDMA_MODULO_OFF,
        .minorByteTransferCount = 4UL,               /* 4 bytes total per minor loop */
        .scatterGatherEnable = true,                 /* Scatter-gather used to auto-grab the next TCD */
        .scatterGatherNextDescAddr = (uint32_t)&spi2_tx_stcd_arr[1],
        .interruptEnable = false,                    /* Upon TCD completion, fire interrupt */
        .loopTransferConfig = &spi2_tx_edma_loop_cfg_pcs  /* Grab major loop iter */
    },
    {   /* Perform transfers to read all ADC0 channels */
        .srcAddr = (uint32_t)&adc_tx_cmds[0],        /* Source address is the SPI Tx array */
        .destAddr = (uint32_t)&LPSPI2->TDR,          /* Destination is the Transmit Data Reg */
        .srcTransferSize = EDMA_TRANSFER_SIZE_2B,    /* Perform 1 byte transfer */
        .destTransferSize = EDMA_TRANSFER_SIZE_2B,   /* Perform 1 byte transfer */
        .srcOffset = 2,                              /* Inc source addr by 2 bytes each minor loop transfer */
        .destOffset = 0,                             /* Inc dest addr by 0 bytes each minor loop transfer */
        .srcLastAddrAdjust = 0L,
        .destLastAddrAdjust = 0L,
        .srcModulo = EDMA_MODULO_OFF,
        .destModulo = EDMA_MODULO_OFF,
        .minorByteTransferCount = 2UL,               /* 2 bytes total per minor loop */
        .scatterGatherEnable = false,                /* Scatter-gather used to auto-grab the next TCD */
        .scatterGatherNextDescAddr = (uint32_t)&spi2_tx_stcd_arr[0],
        .interruptEnable = true,                     /* Upon TCD completion, fire interrupt */
        .loopTransferConfig = &spi2_tx_edma_loop_cfg_adc  /* Grab major loop iter */
    }
};



/* SPI2 Receive DMA TCD
 *
 * Sequence:
 *    1. Perform ADC reads for all ADC0 commands
 */

static edma_loop_transfer_config_t spi2_rx_edma_loop_cfg_adc =
{
    .majorLoopIterationCount = EDMA_ADC_TX_CMDS_CNT,
    .srcOffsetEnable = false,
    .dstOffsetEnable = false,
    .minorLoopOffset = 0L,
    .minorLoopChnLinkEnable = false,
    .minorLoopChnLinkNumber = 0U,
    .majorLoopChnLinkEnable = false,
    .majorLoopChnLinkNumber = 0U
};

static edma_transfer_config_t spi2_rx_edma_tcds[SPI2_RX_TCD_CNT] =
{
    {
        .srcAddr = (uint32_t)&LPSPI2->RDR,           /* Source is the Receive Data Reg */
        .destAddr = (uint32_t)&edma_adc_rx[0],       /* Destination is the eDMA ADC Rx structure */
        .srcTransferSize = EDMA_TRANSFER_SIZE_2B,    /* Perform 2 byte transfer */
        .destTransferSize = EDMA_TRANSFER_SIZE_2B,   /* Perform 2 byte transfer */
        .srcOffset = 0,                              /* Inc source addr by 0 bytes each minor loop transfer */
        .destOffset = 2,                             /* Inc dest addr by 0 bytes each minor loop transfer */
        .srcLastAddrAdjust = 0L,
        .destLastAddrAdjust = 0L,
        .srcModulo = EDMA_MODULO_OFF,
        .destModulo = EDMA_MODULO_OFF,
        .minorByteTransferCount = 2UL,               /* 2 bytes total per minor loop */
        .scatterGatherEnable = false,                /* Scatter-gather disabled */
        .scatterGatherNextDescAddr = (uint32_t)&spi2_rx_stcd_arr[0],
        .interruptEnable = true,                     /* Upon TCD completion, fire interrupt */
        .loopTransferConfig = &spi2_rx_edma_loop_cfg_adc  /* Grab major loop iter config */
    }
};



/* Stores the read channel values for ADC0 */
uint16_t edma_adc_rx[EDMA_ADC_TX_CMDS_CNT] = {0};



void EdmaAdc_Init(edma_callback_t edma_tx_callback_ptr, edma_callback_t edma_rx_callback_ptr)
{
    DEV_ASSERT(edma_tx_callback_ptr);
    DEV_ASSERT(edma_rx_callback_ptr);

    uint32_t i;

    /* spi2_tx/rx_stcd_arr are zeroed at elaboration */

    /* For the SPI transmit, copy configuration to software TCD structure
     * NOTE: The linker is used to guarantee 32-byte aligned SW TCD address */
    for (i = 0; i < SPI2_TX_TCD_CNT; ++i)
    {
        EDMA_DRV_PushConfigToSTCD(&spi2_tx_edma_tcds[i], (edma_software_tcd_t *)&spi2_tx_stcd_arr[i]);
    }
    /* Set last TCD to disable the eDMA channel */
    spi2_tx_stcd_arr[SPI2_TX_TCD_CNT-1].CSR |= DMA_TCD_CSR_DREQ_MASK;

    /* For the SPI receive, copy configuration to software TCD structure
     * NOTE: The linker is used to guarantee 32-byte aligned SW TCD address */
    for (i = 0; i < SPI2_RX_TCD_CNT; ++i)
    {
        EDMA_DRV_PushConfigToSTCD(&spi2_rx_edma_tcds[i], (edma_software_tcd_t *)&spi2_rx_stcd_arr[i]);
    }
    /* Set last TCD to disable the eDMA channel */
    spi2_rx_stcd_arr[SPI2_RX_TCD_CNT-1].CSR |= DMA_TCD_CSR_DREQ_MASK;

    /* Install the callbacks */
    (void)EDMA_DRV_InstallCallback(EDMA_CHN4_NUMBER, edma_tx_callback_ptr, NULL);
    (void)EDMA_DRV_InstallCallback(EDMA_CHN5_NUMBER, edma_rx_callback_ptr, NULL);


    /* Configure SPI Rx watermark to trigger whenever number of words
     * in Rx FIFO is greater than or equal to 0 */
    LPSPI_SetRxWatermarks(LPSPI2, 0U);

    /* Configure SPI Tx watermark to trigger whenever number of words
     * in Tx FIFO is less than or equal to 2 (Max Tx FIFO size) */
    LPSPI_SetTxWatermarks(LPSPI2, 2U);

    /* Clear any error statuses already set.  Always returns STATUS_SUCCESS */
    (void)LPSPI_ClearStatusFlag(LPSPI2, LPSPI_RECEIVE_ERROR);
    (void)LPSPI_ClearStatusFlag(LPSPI2, LPSPI_TRANSMIT_ERROR);

    return;
}


/* Note: No transfers should be in progress when calling this function */
void EdmaAdc_StartTransfer(void)
{
    /* Clear the ADC Rx buffer now that all transfers are guaranteed
     * to be halted */
    memset(&edma_adc_rx[0], 0, sizeof(edma_adc_rx));

    /* Assert that Receive data is not being masked from the Rx FIFO */
    LPSPI_ClearRxmaskBit(LPSPI2);

    /* Return eDMA status to Normal prior to transfer in
     * case EDMA_CHN_ERROR was triggered the last transfer */
    dmaController0Chn4_State.status = EDMA_CHN_NORMAL;
    dmaController0Chn5_State.status = EDMA_CHN_NORMAL;

    /* Push the configuration for the first descriptor to registers
     * Note: For some reason, directly copying the first TCD into DMA->TCD[ch] directly via
     * memcpy does not always work, where we hit a DMA error for SPI Tx.  It's unclear if
     * this is due to the structures mismatching (e.g. not packed) or due to a DMA restriction
     * of some kind. Using the SDK API to copy over the first TCD item seems to work well. */
    EDMA_TCDClearReg(DMA, EDMA_CHN5_NUMBER);
    EDMA_TCDClearReg(DMA, EDMA_CHN4_NUMBER);
    EDMA_DRV_PushConfigToReg(EDMA_CHN5_NUMBER, &spi2_rx_edma_tcds[0]);
    EDMA_DRV_PushConfigToReg(EDMA_CHN4_NUMBER, &spi2_tx_edma_tcds[0]);

    /* Start the Rx DMA channel first, followed by the Tx DMA channel, always returns STATUS_SUCCESS */
    (void)EDMA_DRV_StartChannel(EDMA_CHN5_NUMBER);
    (void)EDMA_DRV_StartChannel(EDMA_CHN4_NUMBER);

    /* Start the SPI transfer */
    LPSPI_SetRxDmaCmd(LPSPI2, true);
    LPSPI_SetTxDmaCmd(LPSPI2, true);

    return;
}


status_t EdmaAdc_EndTransfer(void)
{
    status_t status = STATUS_SUCCESS;

    EDMA_DRV_StopChannel(EDMA_CHN5_NUMBER);
    EDMA_DRV_StopChannel(EDMA_CHN4_NUMBER);

    /* No SPI2 interrupts are utilized, therefore the check for
     * SPI errors occurs manually here */

    /* Check if last transfer had Rx FIFO overrun */
    if (true == LPSPI_GetStatusFlag(LPSPI2, LPSPI_RECEIVE_ERROR))
    {
        status = STATUS_SPI_RX_OVERRUN;

        /* Always returns STATUS_SUCCESS */
        (void)LPSPI_ClearStatusFlag(LPSPI2, LPSPI_RECEIVE_ERROR);
    }

    /* Check if last transfer had Tx FIFO underrun */
    if (true == LPSPI_GetStatusFlag(LPSPI2, LPSPI_TRANSMIT_ERROR))
    {
        status = STATUS_SPI_TX_UNDERRUN;

        /* Always returns STATUS_SUCCESS */
        (void)LPSPI_ClearStatusFlag(LPSPI2, LPSPI_TRANSMIT_ERROR);
    }

    /* Abort any SPI2 transfers that are currently in progress and
     * flush the Tx and Rx FIFOs */
    (void)LPSPI_DRV_MasterAbortTransfer(LPSPICOM2);

    return status;
}
