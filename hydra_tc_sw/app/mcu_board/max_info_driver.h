/* Copyright (c) 2017-2020 by Phase Four, Inc. All Rights Reserved. Confidential and Proprietary.
 *
 * max_info_driver.h
 */
#ifndef MCU_BOARD_MAX_INFO_DRIVER_H_
#define MCU_BOARD_MAX_INFO_DRIVER_H_

/* Global invariants:
 *  - Adjusting the App_Header_Rec_t structure in any way has downstream effects,
 *    particularly to the size of the packet returned for the GET_SW_INFO command.
 *    Care must be taken if adjusting this structure and related substructure.
 */

#include "mcu_types.h"
#include "status.h"
#include "p4_err.h"
#include "ErrAO.h"
/* SRAM Structures */

typedef struct
{
    uint32_t img_type;
    uint32_t img_length;
    uint32_t img_crc;
    uint8_t reserved[4];
    uint8_t img_ver[32];
    uint8_t hardware_version_type[4];
    uint8_t product_version_type[4];
    uint8_t reserved_two[40];
} BYTE_PACKED Img_Header_Rec_t;

typedef struct
{
    uint32_t bl_select;
    uint32_t app_select;
} BYTE_PACKED Dna_Rec_t;


typedef struct
{
    Img_Header_Rec_t app_record;
    Dna_Rec_t        dna_record;
} BYTE_PACKED App_Header_Rec_t;


#define FIELD_NAME_WIDTH 48
#define PART_NUMBER_WIDTH 32
#define REVISION_WIDTH 16
#define SERIAL_NAME_WIDTH 32
#define TOTAL_HARDWARE_WIDTH  (FIELD_NAME_WIDTH + PART_NUMBER_WIDTH +REVISION_WIDTH +SERIAL_NAME_WIDTH)

typedef struct
{
    char field_name[FIELD_NAME_WIDTH];
    char part_number[PART_NUMBER_WIDTH];
    char revision[REVISION_WIDTH];
    char serial_number[SERIAL_NAME_WIDTH];

} BYTE_PACKED Max_Hardware_Rec_t;

#define TOTAL_MAXWELL_HARDWARE_RECORDS 32


extern Max_Hardware_Rec_t maxwell_hardware_record[TOTAL_MAXWELL_HARDWARE_RECORDS];

extern uint32_t __APP_HDR_REC;


#define APP_HDR_RECORD      (&__APP_HDR_REC) /*This is legacy from the app_hdr.c/h we might want to port over later. */
#define SRAM_APP_HDR_RECORD    (&__APP_HDR_REC)

#define APP_HDR_MAX_SIZE    0x100UL

#define IMG_HDR_MAX_SIZE    APP_HDR_MAX_SIZE
#define IMG_MAX_SIZE        (128UL * 1024UL)

#define APP_IMG_TYPE_ID     0x45584521UL  /* EXE! */

#define APP_1_FLASH_HDR_RECORD 0x00080000UL
#define APP_2_FLASH_HDR_RECORD 0x000C0000UL
#define GOLDEN_IMAGE_FLASH_HDR_RECORD 0x00040000UL

#define BOOTLOADER_1_HDR_RECORD 0x00000F00UL
#define BOOTLOADER_2_HDR_RECORD 0x00001F00UL

#define PARM_TABLE_GOLD    0x00060000UL
#define PARM_TABLE_APP1    0x000A0000UL
#define PARM_TABLE_APP2    0x000E0000UL
#define PARM_TABLE_RUN     0x10000000UL

#define GET_CURRENT_APP_INFO_ARG          0U
#define GET_BOOTLOADER_1_SW_INFO_ARG      1U
#define GET_BOOTLOADER_2_SW_INFO_ARG      2U
#define GET_GOLDEN_IMAGE_APP_SW_INFO_ARG  3U
#define GET_FLASH_1_APP_SW_INFO_ARG       4U
#define GET_FLASH_2_APP_SW_INFO_ARG       5U
#define GET_GOLDEN_PARM_TABLE_SW_INFO_ARG 6U
#define GET_PARM_TABLE_1_SW_INFO_ARG      7U
#define GET_PARM_TABLE_2_SW_INFO_ARG      8U
#define GET_PARM_TABLE_CURR_SW_INFO_ARG   9U


#define SRAM_RECORD_TYPE 1
#define FLASH_RECORD_TYPE 2




/*These data structs are located on the base header record location. This enables to read the record header in a "program friendly matter". Thus we can manipulate the data relativity easily.*/
extern App_Header_Rec_t *app_hdr_rec;
extern uint32_t header_address;

extern Img_Header_Rec_t *parm1_tbl_hdr_rec;
extern Img_Header_Rec_t *parm2_tbl_hdr_rec;
extern Img_Header_Rec_t *parm3_tbl_hdr_rec;





Cmd_Err_Code_t update_flash_hdr(uint8_t sw_application_selection);
Cmd_Err_Code_t calculate_maxwell_hrd_record_address(uint8_t sector_offset,uint8_t* mcu_dump_args);
Cmd_Err_Code_t write_maxwell_hrd_record(GLADOS_AO_t *ao_src, char *data_contents, size_t data_size);
void erase_max_hrd_record(GLADOS_AO_t *ao_src);
size_t find_comma(char *data_contents, size_t data_size);

Cmd_Err_Code_t parse_maxwell_header_record(char *data_contents, size_t data_size, Max_Hardware_Rec_t * maxwell_record );
Cmd_Err_Code_t  write_next_app_select(GLADOS_AO_t *ao_src, uint8_t app_selection);
status_t erase_app_selection_sector(GLADOS_AO_t *ao_src);
status_t erase_app_selection_sector_startup(void);
#endif /* MCU_BOARD_MAX_INFO_DRIVER_H_ */
