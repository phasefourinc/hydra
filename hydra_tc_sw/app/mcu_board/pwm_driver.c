/* Copyright (c) 2017-2020 by Phase Four, Inc. All Rights Reserved. Confidential and Proprietary.
 *
 * pwm_load_driver.c
 */

#include "pwm_driver.h"
#include "mcu_types.h"
#include "ftm_pwm_driver.h"
#include "math.h"


status_t PWM_Driver_Init(Pwm_t *pwm,
                         uint32_t instance,
                         uint8_t channel,
                         ftm_user_config_t *init_config,
                         ftm_pwm_param_t *pwm_config,
                         const float *cal_poly_cn,
                         float min_v,
                         float max_v)
{
    DEV_ASSERT(pwm);
    DEV_ASSERT(cal_poly_cn);
    DEV_ASSERT((((uint32_t)cal_poly_cn) & 0x3UL) == 0x0UL);  /* Validate 4-byte alignment for FP */


    /* IMPORTANT: Validate that the FTM defaults to zero duty cycle and that it
     *            outputs to 0 when halted for debugging, unless of course there
     *            is a yearning for fireworks. Active high means POLn is 0. See
     *            RM p1238 and p1351 for more detail. However testing indicates
     *            that the debug halted value is determined by safeState rather
     *            than polarity. Check both anyways since this driver assumes
     *            active high polarity. */
    DEV_ASSERT(pwm_config->pwmIndependentChannelConfig->uDutyCyclePercent == 0U);
    DEV_ASSERT(init_config->BDMMode == FTM_BDM_MODE_01);
    DEV_ASSERT(pwm_config->pwmIndependentChannelConfig->polarity == FTM_POLARITY_HIGH);
    DEV_ASSERT(pwm_config->pwmIndependentChannelConfig->safeState == FTM_LOW_STATE);

    status_t status;

    pwm->instance = instance;
    pwm->channel = channel;
    pwm->cal_poly_cn = &cal_poly_cn[0];
    pwm->duty_cycle_cnt_fb = 0U;


    if (isfinite(min_v) && isfinite(max_v) && (min_v <= max_v))
    {
        pwm->min_v = min_v;
        pwm->max_v = max_v;
        status = STATUS_SUCCESS;
    }
    else
    {
        pwm->min_v = 0.0f;
        pwm->max_v = 0.0f;
        status = STATUS_ERROR;
    }

    status |= FTM_DRV_Init(instance, init_config, &pwm->state);
    status |= FTM_DRV_InitPwm(instance, pwm_config);

    return status;
}


status_t PWM_Driver_SetVoltage(Pwm_t *pwm, float voltage)
{
    DEV_ASSERT(pwm);

    uint16_t duty_cycle_cnt;
    uint32_t i;
    status_t status = STATUS_ERROR;
    float cal_value = 0.0f;
    float x = voltage;
    float xn = voltage;

    if (isfinite(voltage))
    {
        /* Coerce pre-cal values */
        if (voltage < pwm->min_v)
        {
            x = pwm->min_v;
            xn = pwm->min_v;
        }
        else if (voltage > pwm->max_v)
        {
            x = pwm->max_v;
            xn = pwm->max_v;
        }

        /* Calibrate the ADC value: C0 + C1x + C2x^2 + ... */
        cal_value = pwm->cal_poly_cn[0];
        for (i = 1UL; i < PWM_LOAD_CAL_POLY_SIZE; ++i)
        {
            cal_value += pwm->cal_poly_cn[i] * xn;
            xn *= x;
        }

        if (isfinite(cal_value))
        {
            /* Coerce post-cal values */
            if (cal_value < 0.0f)
            {
                duty_cycle_cnt = 0U;
            }
            else if (cal_value > FTM_MAX_DUTY_CYCLE)
            {
                duty_cycle_cnt = FTM_MAX_DUTY_CYCLE;
            }
            else
            {
                duty_cycle_cnt = (uint16_t)cal_value;
            }

            status = PWM_Driver_SetRaw(pwm, duty_cycle_cnt);
        }
    }

    return status;
}


status_t PWM_Driver_SetDuty(Pwm_t *pwm, float duty_cycle_percent)
{
    DEV_ASSERT(pwm);

    uint16_t duty_cycle_cnt;
    status_t status = STATUS_ERROR;

    /* If the value is within the configured scale */
    if(isfinite(duty_cycle_percent))
    {
        if (duty_cycle_percent < 0.0f)
        {
            duty_cycle_percent = 0.0f;
        }
        else if (duty_cycle_percent > 1.0f)
        {
            duty_cycle_percent = 1.0f;
        }

        /* Get duty cycle percentage and then multiply by max FTM duty cycle counts */
        duty_cycle_cnt = (uint16_t)(duty_cycle_percent * ((float)FTM_MAX_DUTY_CYCLE));
        DEV_ASSERT(duty_cycle_cnt <= FTM_MAX_DUTY_CYCLE);
        status = PWM_Driver_SetRaw(pwm, duty_cycle_cnt);
    }

    return status;
}


status_t PWM_Driver_SetRaw(Pwm_t *pwm, uint16_t duty_cycle_cnt)
{
    status_t status = FTM_DRV_UpdatePwmChannel(pwm->instance, pwm->channel,
                                                   FTM_PWM_UPDATE_IN_DUTY_CYCLE, duty_cycle_cnt, 0U, true);
    if (status == STATUS_SUCCESS)
    {
        pwm->duty_cycle_cnt_fb = duty_cycle_cnt;
    }

    return status;
}
