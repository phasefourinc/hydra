/* Copyright (c) 2017-2020 by Phase Four, Inc. All Rights Reserved. Confidential and Proprietary.
 *
 * max_info_driver.c
 */

#include "ErrAO.h"
#include "max_info_driver.h"
#include "mcu_types.h"
#include "MCUflash.h"
#include "string.h"
#include "stdint.h"
#include "timer64.h"
#include "CmdAO.h"

App_Header_Rec_t *app_hdr_rec = (App_Header_Rec_t *)APP_HDR_RECORD;




Img_Header_Rec_t *parm1_tbl_hdr_rec = (Img_Header_Rec_t *)PARM_TABLE_APP1;
Img_Header_Rec_t *parm2_tbl_hdr_rec = (Img_Header_Rec_t *)PARM_TABLE_APP2;
Img_Header_Rec_t *parm3_tbl_hdr_rec = (Img_Header_Rec_t *)PARM_TABLE_GOLD;


uint32_t header_address =0;

Max_Hardware_Rec_t  __attribute__ ((section (".__MAX_REC_HEAD"))) maxwell_hardware_record[TOTAL_MAXWELL_HARDWARE_RECORDS];

uint32_t  __attribute__ ((section (".__MAX_APP_SELECT_HEAD"))) app_select_start_value;

Cmd_Err_Code_t update_flash_hdr(uint8_t sw_application_selection)
{
    Cmd_Err_Code_t error_code = ERROR_NONE;

     switch (sw_application_selection){
         case GET_CURRENT_APP_INFO_ARG:
             header_address = (uint32_t) APP_HDR_RECORD;
             break;
         case GET_BOOTLOADER_1_SW_INFO_ARG:
             header_address = BOOTLOADER_1_HDR_RECORD;
             break;
         case GET_BOOTLOADER_2_SW_INFO_ARG:
             header_address = BOOTLOADER_2_HDR_RECORD;
             break;
         case GET_GOLDEN_IMAGE_APP_SW_INFO_ARG:
             header_address = GOLDEN_IMAGE_FLASH_HDR_RECORD;
             break;
         case GET_FLASH_1_APP_SW_INFO_ARG:
             header_address = APP_1_FLASH_HDR_RECORD;
             break;
         case GET_FLASH_2_APP_SW_INFO_ARG:
             header_address = APP_2_FLASH_HDR_RECORD;
             break;
         case GET_GOLDEN_PARM_TABLE_SW_INFO_ARG:
             header_address = PARM_TABLE_GOLD;
             break;
         case GET_PARM_TABLE_1_SW_INFO_ARG:
             header_address = PARM_TABLE_APP1;
             break;
         case GET_PARM_TABLE_2_SW_INFO_ARG:
             header_address = PARM_TABLE_APP2;
             break;
         case GET_PARM_TABLE_CURR_SW_INFO_ARG:
             if (RftData.using_runtime_pt)
             {
                 header_address = PARM_TABLE_RUN;
             }
             else if (RftData.using_app_pt)
             {
                 if (RftData.app_select == 1U)
                 {
                     header_address = PARM_TABLE_APP1;
                 }
                 else if (RftData.app_select == 2U)
                 {
                     header_address = PARM_TABLE_APP2;
                 }
                 else
                 {
                     header_address = PARM_TABLE_GOLD;
                 }
             }
             else
             {
                 /* Using the default table, point to current App Exe image */
                 if (RftData.app_select == 1U)
                 {
                     header_address = APP_1_FLASH_HDR_RECORD;
                 }
                 else if (RftData.app_select == 2U)
                 {
                     header_address = APP_2_FLASH_HDR_RECORD;
                 }
                 else
                 {
                     header_address = GOLDEN_IMAGE_FLASH_HDR_RECORD;
                 }
             }
             break;
         default:
             error_code = create_error_uint((err_type*) Errors.CmdInvalidGetSoftwareInfoSelection,  sw_application_selection);
             break;
     }

     return error_code;
    }

/*This function erases the records for the maxwell hardware record. This can be useful when wanting to rewrite the hardware log.*/

 void erase_max_hrd_record(GLADOS_AO_t *ao_src)
 {
     erase_flash_sector_ao(ao_src, (uint32_t)maxwell_hardware_record, ERASE_SECTOR_SIZE);
 }

 /*This function calculates what the hardware address should be based on the offset it received..*/
 Cmd_Err_Code_t calculate_maxwell_hrd_record_address(uint8_t sector_offset, uint8_t* mcu_dump_args)
{
     Cmd_Err_Code_t error_code = ERROR_NONE;
    if (sector_offset > TOTAL_MAXWELL_HARDWARE_RECORDS)
    {
        /*The offset that is being asked is not valid. It is outside of the maxwell hardware record  sector.*/
        error_code = create_error_uint((err_type*) Errors.CmdInvalidMaxwellHardwareOffset,  sector_offset);
    }
    else
    {
        uint32_t maxwell_hrd_info_addr = (uint32_t)maxwell_hardware_record + (TOTAL_HARDWARE_WIDTH * sector_offset);
        uint8_t count = 0;
        while (count < 4)
        {
            mcu_dump_args[count] = (uint8_t)(maxwell_hrd_info_addr  >> (8 * (3 - count)));
            count = count + 1;
        }
        mcu_dump_args[4] = TOTAL_HARDWARE_WIDTH;

    }
    return error_code;

}


/*This function takes the incoming data and writes it into flash memory.
 * It does this by formatting the incoming data with parse_maxwell_header_record and then writes that to flash.
 * Due to timing constraints it assumes it is successful.
 *
 */
Cmd_Err_Code_t write_maxwell_hrd_record(GLADOS_AO_t *ao_src, char *data_contents, size_t data_size)
{
    Cmd_Err_Code_t error_code = ERROR_NONE;

    uint8_t maxwell_flash_offset = (uint8_t)*data_contents;
    bool is_flash_cleared = false;
    status_t status;

    if (maxwell_flash_offset < TOTAL_MAXWELL_HARDWARE_RECORDS)
    {
        is_flash_cleared = check_if_flash_is_empty( (uint32_t)&maxwell_hardware_record[maxwell_flash_offset], sizeof(maxwell_hardware_record[maxwell_flash_offset]));
        if (is_flash_cleared)
        {
            Max_Hardware_Rec_t maxwell_record = { .field_name = "FN:", .part_number = "PN:", .revision = "RV:", .serial_number = "SN:" };

            /* We are adding one to the data_contents due to the fact the first byte was used to get maxwell's flash offset.
             * The minus one is because we one less byte due to the maxwell flash offset argument.*/
            error_code = parse_maxwell_header_record(data_contents + 1, data_size - 1, &maxwell_record);

            if (error_code.reported_error->error_id == STATUS_SUCCESS)
            {
                status = write_flash_ao(ao_src, (uint32_t)&maxwell_hardware_record[maxwell_flash_offset], sizeof(maxwell_hardware_record[maxwell_flash_offset]), (uint8_t*)&maxwell_record);
                if (status != STATUS_SUCCESS)
                {
                    error_code = create_error_uint((err_type*)Errors.FlashWriteFailed , (uint32_t)&maxwell_hardware_record[maxwell_flash_offset]);
                }
            }
        }
        else
        {
            error_code = create_error_uint((err_type*) Errors.CmdMaxWriteRecordOffsetFull,  maxwell_flash_offset);
        }

    }
    else
    {
        /*The offset that is being asked is not valid. It is outside of the maxwell hardware record sector.*/
        error_code = create_error_uint((err_type*) Errors.CmdInvalidMaxwellHardwareOffset,  maxwell_flash_offset);
    }
    return error_code;
}

/*This function parses the data coming in and formats the data into a maxwell hardware data struct.
 *This data struct is returned which will be used to write that data into flash.
 */
Cmd_Err_Code_t parse_maxwell_header_record(char *data_contents, size_t data_size, Max_Hardware_Rec_t * maxwell_record )
{
    Cmd_Err_Code_t error_code = ERROR_NONE;

    enum { FIELDS_PER_HRD_REC = 4U };

    char* data_field_pointer = data_contents;
    uint8_t data_field_offsets [FIELDS_PER_HRD_REC] = {FIELD_NAME_WIDTH, PART_NUMBER_WIDTH, REVISION_WIDTH, SERIAL_NAME_WIDTH};
    size_t csv_offset = 0;
    if (data_size !=sizeof(Max_Hardware_Rec_t))
    {
        /*The amount of data that came in is not a full Maxwell hardware record. Thus we can not trust this data. Report a error with data size it saw.*/
        error_code = create_error_uint((err_type*) Errors.CmdInvalidMaxRecordDataSize,  data_size);
        return error_code;
    }
    else
    {
        uint8_t count = 0;
        while (count <FIELDS_PER_HRD_REC)
        {
            uint8_t field_size = data_field_offsets[count];
              csv_offset = find_comma(data_field_pointer, field_size);
            if (csv_offset != 0)
            {
                switch (count)
                {
                    case 0:
                        memcpy((*maxwell_record).field_name,data_field_pointer,csv_offset);
                        break;
                    case 1:
                        memcpy((*maxwell_record).part_number,data_field_pointer,csv_offset);
                        break;
                    case 2:
                        memcpy((*maxwell_record).revision,data_field_pointer,csv_offset);
                        break;
                    case 3:
                        memcpy((*maxwell_record).serial_number,data_field_pointer,csv_offset);
                        break;
                };

            }
            else
            {
                /*find_commma could not find a comma. That means the data packet that came in to write as a Maxwell hardware record  was illformed. Sending the number of commas it did find before it crashed.*/
                error_code = create_error_uint((err_type*) Errors.CmdIllformedCommaMaxRecordPacket,  count);
                return error_code;
            }
            data_field_pointer = data_field_pointer + data_field_offsets[count];
            count = count + 1;
        }

    }
    return error_code;

}





status_t erase_app_selection_sector(GLADOS_AO_t *ao_src)
{
    uint32_t* app_selection_address_location = &app_select_start_value;
    status_t status = erase_flash_sector_ao(ao_src, (uint32_t)app_selection_address_location, ERASE_SECTOR_SIZE);
    if (status != STATUS_SUCCESS)
    {
        report_error_uint(ao_src, (err_type*) Errors.FlashEraseFailed , SEND_TO_SPACECRAFT, (uint32_t) app_selection_address_location);
    }
    return status;
}


status_t erase_app_selection_sector_startup(void)
{
    uint32_t* app_selection_address_location = &app_select_start_value;
    status_t status = erase_flash_sector((uint32_t)app_selection_address_location, ERASE_SECTOR_SIZE);
    return status;
}

Cmd_Err_Code_t write_next_app_select(GLADOS_AO_t *ao_src, uint8_t app_selection){
    Cmd_Err_Code_t error_code = ERROR_NONE;
    if (app_selection > 3)
    {
        error_code = create_error_uint((err_type*)Errors.InvalidAppSelectParamater, app_selection);

        return error_code;
    }
    else
    {

        uint32_t* app_selection_address_location = &app_select_start_value;
        uint8_t select_buff[MINIMUM_PROGRAM_SIZE];
        uint8_t count = 0;
        status_t  status = check_erase_flash_status((uint32_t)app_selection_address_location);
        if (status != STATUS_SUCCESS)
        {
            report_error_uint(ao_src, (err_type*) Errors.FlashEraseFailed , SEND_TO_SPACECRAFT, (uint32_t) app_selection_address_location);
        }
        else {
            while (count < MINIMUM_PROGRAM_SIZE)
            {
                select_buff[count] = app_selection;
                count++;
            }

            status = write_flash_ao(ao_src, (uint32_t)app_selection_address_location, MINIMUM_PROGRAM_SIZE, select_buff);
            if (status != STATUS_SUCCESS)
            {
                report_error_uint(ao_src, (err_type*) Errors.FlashWriteFailed , SEND_TO_SPACECRAFT, (uint32_t) app_selection_address_location);
            }
        }

    }
    return error_code;
}





/*This function finds the first comma in a string. It then reports that offset.
 * This is used to determine how the string is to be copied over.
 */
size_t find_comma(char *data_contents, size_t data_size)
{
    uint8_t count = 0;
    bool found_comma = false;
    uint8_t key_offset =0;
    while ((count < data_size) && (found_comma == false))
    {
        if (data_contents[count] == ',')
        {
            key_offset=count+1;
            found_comma= true;
        }
        count = count + 1;
    }
    return key_offset;
}
