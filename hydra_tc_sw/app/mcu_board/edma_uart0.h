/* Copyright (c) 2017-2020 by Phase Four, Inc. All Rights Reserved. Confidential and Proprietary.
 *
 * edma_uart0.h
 */

#ifndef MCU_BOARD_EDMA_UART0_H_
#define MCU_BOARD_EDMA_UART0_H_

#include "mcu_types.h"
#include "dmaController0.h"

#define EDMA_UART0_RING_SIZE    640U              /* 640 Hold a little more than 2 full Clank packets */
#define EDMA_UART0_RX_CH_NUM    EDMA_CHN1_NUMBER

extern uint8_t edma_uart0_ring_buffer[EDMA_UART0_RING_SIZE];


void EdmaUart0_Init(void);

#endif /* MCU_BOARD_EDMA_UART0_H_ */
