/* Copyright (c) 2017-2020 by Phase Four, Inc. All Rights Reserved. Confidential and Proprietary.
 *
 * gpio.c
 */

#include "gpio.h"
#include "mcu_types.h"
#include "pins_driver.h"
#include "timer64.h"

typedef struct
{
    GPIO_Type *base_gpio;
    uint8_t    pin;
} GPIO_Table_t;

static const GPIO_Table_t GPIO_OutputTable[] =
{
    { PTE, 2U  },  /* GPIO_WDT_PET */
    { PTE, 5U  },  /* GPIO_WDT_RST_nCLR */
    { PTE, 14U },  /* GPIO_HUM_TEMP_SEL */
    { PTA, 13U },  /* GPIO_WF_EN */
    { PTA, 14U },  /* GPIO_PP_EN */
    { PTE, 6U  }   /* GPIO_LED0 */
};

static const GPIO_Table_t GPIO_InputTable[] =
{
    { PTE, 0U  },  /* GPIO_WDT_TOGGLE_SEL */
    { PTE, 1U  }   /* GPIO_WDT_RESET */
};


void GPIO_SetOutput(GPIO_Output_t pin, bool is_high)
{
    if (is_high)
    {
        PINS_DRV_SetPins(GPIO_OutputTable[pin].base_gpio, (1UL << GPIO_OutputTable[pin].pin));
    }
    else
    {
        PINS_DRV_ClearPins(GPIO_OutputTable[pin].base_gpio, (1UL << GPIO_OutputTable[pin].pin));
    }

    return;
}


void GPIO_ToggleOutput(GPIO_Output_t pin)
{
    PINS_DRV_TogglePins(GPIO_OutputTable[pin].base_gpio, (1UL << GPIO_OutputTable[pin].pin));
}


uint8_t GPIO_GetInput(GPIO_Input_t pin)
{
    return (uint8_t)((PINS_DRV_ReadPins(GPIO_InputTable[pin].base_gpio) >> GPIO_InputTable[pin].pin) & 0x1UL);
}


void GPIO_PetWDT(void)
{
    /* Pet the WDT, minimum pulse high for chip is 50ns */
    /* Note: Pet pulse min is 50ns, window for a pet is between 27-818ms */
    GPIO_SetOutput(GPIO_WDT_PET, 1);
    timer32_delay_us(1U);
    GPIO_SetOutput(GPIO_WDT_PET, 0);
    return;
}
