/* Copyright (c) 2017-2020 by Phase Four, Inc. All Rights Reserved. Confidential and Proprietary.
 *
 * mcu_types.h
 */

#ifndef MCU_TYPES_H_
#define MCU_TYPES_H_

#include "device_registers.h"
#include "system_S32K148.h"
#include "status.h"
#include "stddef.h"

#define UNUSED         __attribute__((unused))    /* Marks unused function parameters */
#define BYTE_PACKED    __attribute__((packed, aligned(1)))
#define ALIGNED(x)     __attribute__((aligned(x)))
#define SECTION(x)     __attribute__((section(x)))
#define ASM_ONLY       __attribute__((naked))
#define ALWAYS_INLINE  __attribute__((always_inline))
#define ASM_ONLY       __attribute__((naked))
#define F32_TO_BYTES(x)    (*(uint32_t *)&(x))
#define BYTES_TO_F32(x)    (*(float *)&(x))


/*!
 * @brief Reverses the byte order of the 4-byte passed value
 * @param value
 * @return 4B swap of value
 */
#define REV_BYTES_32(a, b) __asm volatile ("rev %0, %1" : "=r" (b) : "r" (a))
ALWAYS_INLINE static inline uint32_t REV32(uint32_t value)
{
    uint32_t result;
    REV_BYTES_32(value, result);
    return result;
}


/*!
 * @brief Reverses the byte order of the 2-byte passed value
 * @param value
 * @return 2B swap of value
 */
#define REV_BYTES_16(a, b) __asm volatile ("rev16 %0, %1" : "=r" (b) : "r" (a))
ALWAYS_INLINE static inline uint16_t REV16(uint32_t value)
{
    uint32_t result;
    REV_BYTES_16(value, result);
    /* Technically this reverses the upper 16-bits as well,
     * however only return the lower 16-bits for ease of use */
    return (uint16_t)result;
}


/*!
  @brief   Enable IRQ Interrupts
  @details Enables IRQ interrupts by clearing the I-bit in the CPSR.
           Can only be executed in Privileged modes.
 */
ALWAYS_INLINE static inline void __enable_irq(void)
{
    __asm volatile ("cpsie i" : : : "memory");
}


/*!
  @brief   Disable IRQ Interrupts
  @details Disables IRQ interrupts by setting the I-bit in the CPSR.
           Can only be executed in Privileged modes.
 */
ALWAYS_INLINE static inline void __disable_irq(void)
{
    __asm volatile ("cpsid i" : : : "memory");
}


/*!
  @brief   Get FPSCR
  @details Returns the current value of the Floating Point Status/Control register.
  @return               Floating Point Status/Control register value
 */
ALWAYS_INLINE static inline uint32_t __get_FPSCR(void)
{
    uint32_t result;
    /* Empty asm statement works as a scheduling barrier */
    __asm volatile ("");
    __asm volatile ("VMRS %0, fpscr" : "=r" (result) );
    __asm volatile ("");
    return(result);
}


/*!
  @brief   Set FPSCR
  @details Assigns the given value to the Floating Point Status/Control register.
  @param [in]    fpscr  Floating Point Status/Control value to set
 */
ALWAYS_INLINE static inline void __set_FPSCR(uint32_t fpscr)
{
    /* Empty asm statement works as a scheduling barrier */
    __asm volatile ("");
    __asm volatile ("VMSR fpscr, %0" : : "r" (fpscr) : "vfpcc");
    __asm volatile ("");
}


/*!
  @brief   Get IPSR Register
  @details Returns the content of the IPSR Register.
  @return               IPSR Register value
 */
ALWAYS_INLINE static inline uint32_t __get_IPSR(void)
{
  uint32_t result;
  __asm volatile ("MRS %0, ipsr" : "=r" (result) );
  return(result);
}


/*!
  @brief   Instruction Synchronization Barrier
  @details Instruction Synchronization Barrier flushes the pipeline in the processor,
           so that all instructions following the ISB are fetched from cache or memory,
           after the instruction has been completed.
 */
ALWAYS_INLINE static inline void __ISB(void)
{
    __asm volatile ("isb 0xF":::"memory");
}


/*!
  @brief   Data Synchronization Barrier
  @details Acts as a special kind of Data Memory Barrier.
           It completes when all explicit memory accesses before this instruction complete.
 */
ALWAYS_INLINE static inline void __DSB(void)
{
    __asm volatile ("dsb 0xF":::"memory");
}


/*!
  @brief   Data Memory Barrier
  @details Ensures the apparent order of the explicit memory operations before
           and after the instruction, without ensuring their completion.
 */
ALWAYS_INLINE static inline void __DMB(void)
{
    __asm volatile ("dmb 0xF":::"memory");
}


#endif /* MCU_TYPES_H_ */
