/* Copyright (c) 2017-2020 by Phase Four, Inc. All Rights Reserved. Confidential and Proprietary.
 *
 * MCUflash.h
 */

#ifndef MCU_BOARD_MCUFLASH_H_
#define MCU_BOARD_MCUFLASH_H_

#include "mcu_types.h"
#include "status.h"
#include "glados.h"

#define MINIMUM_PROGRAM_SIZE       0x08UL
#define MEMORY_FPROT_BITS_FULL     0xFFFFFF00UL  /* Protect the first 8 64KB sectors from 0x0-0x7FFFF */
#define MEMORY_FPROT_BITS_GOLD_UPDATABLE 0xFFFFFFF0UL  /* Unprotected Golden Image region */
#define GOLD_IMG_LOAD_PWD          0xBAAD1DEAUL  /* String to unlock Golden Image region */
#define MEMORY_MODIFY_ADDRESS_LINE 0x40000UL     /* Anything below this line should not be erased no matter what circumstance. */
#define MAX_MODIFY_LENGTH          0x20000U      /* No flash modifications greater than 128KB */
#define ERASE_SECTOR_SIZE          0x1000U
#define ERASE_SECTOR_SIZE_IN_DOUBLE_PHRASE   (ERASE_SECTOR_SIZE/16U)
#define MAX_ERASE_ATTEMPTS 3U
#define NORMAL_READ_MARGIN_LEVEL 0U
#define USER_READ_MARGIN_LEVEL 1U
#define ERASE_SECTOR_CHECK 1U
#define PROGRAM_SECTOR_CHECK 2U


status_t initialize_flash(void);
void mcuflash_set_fprot_full(void);
status_t erase_flash_sector(uint32_t start_address, UNUSED uint32_t length);
status_t erase_flash_sector_ao(GLADOS_AO_t *ao_src,uint32_t start_address, uint32_t length);
status_t write_flash(uint32_t start_address, uint32_t length, uint8_t *pData);
status_t write_flash_ao(GLADOS_AO_t *ao_src, uint32_t start_address, uint32_t length, uint8_t *pData);
status_t check_program_flash_status(uint32_t * start_address, uint32_t length, uint8_t *pData, uint32_t *pFailAddr);
status_t check_erase_flash_status(uint32_t start_address);
bool check_if_flash_is_empty(uint32_t flash_address, uint32_t size);

#endif /* MCU_BOARD_MCUFLASH_H_ */
