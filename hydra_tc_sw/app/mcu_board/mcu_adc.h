/* Copyright (c) 2017-2020 by Phase Four, Inc. All Rights Reserved. Confidential and Proprietary.
 *
 * mcu_adc.h
 */

#ifndef MCU_ADC_H_
#define MCU_ADC_H_

/*!
 * Only one channel conversion can be active per ADC module. Configuring for a new channel
 * in SW Trigger mode automatically initiates a new conversion with the new channel.
 *
 */

#include "mcu_types.h"

#define MCUADC_VREF             3.3f
#define MCUADC_MAX_RAW_12BIT    0xFFFUL
#define MCUADC_CAL_POLY_SIZE    4U

typedef enum
{
    MCU_ADC0_CH3 = 0,
    MCU_ADC0_CH7,
    MCU_ADC0_CH8,
    MCU_ADC0_CH9,
    MCU_ADC0_CH10,
    MCU_ADC0_CH11,
    MCU_ADC0_CH14,
    MCU_ADC1_CH6,
    MCU_ADC1_CH8,
    MCU_ADC1_CH23,
    MCU_ADC1_CH24,
    MCU_ADC_CH_CNT
} McuAdc_Ch_Idx_t;

void mcu_adc_init(void);
status_t mcu_adc_sample_ch_all(void);
status_t mcu_adc_sample_ch(uint32_t instance, McuAdc_Ch_Idx_t ch);
uint16_t mcu_adc_get_ch_raw(McuAdc_Ch_Idx_t ch);
float mcu_adc_get_ch_voltage(McuAdc_Ch_Idx_t ch);
float mcu_adc_ch_calibrate(McuAdc_Ch_Idx_t ch, const float (*cal_poly_cn)[MCUADC_CAL_POLY_SIZE]);
bool mcu_adc_is_busy(uint32_t instance);


#endif /* MCU_ADC_H_ */
