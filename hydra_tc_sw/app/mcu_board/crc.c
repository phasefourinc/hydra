/* Copyright (c) 2017-2020 by Phase Four, Inc. All Rights Reserved. Confidential and Proprietary.
 *
 * crc.c
 */


#include "crc.h"
#include "mcu_types.h"
#include "crc0.h"


uint16_t crc16_compute(uint16_t seed_crc, uint8_t *buffer, uint16_t size)
{
    DEV_ASSERT(buffer);

    uint32_t i;

    crc0_CRC16_CCITT_FALSE.seed = seed_crc;

    /* Always returns success */
    (void)CRC_DRV_Init(INST_CRC0, &crc0_CRC16_CCITT_FALSE);

    for (i = 0; i < size; ++i, ++buffer)
    {
        CRC->DATAu.DATA_8.LL = *buffer;
    }

    return (uint16_t)CRC->DATAu.DATA;
}


uint32_t crc32_compute(uint32_t seed_crc, uint8_t *buffer, uint32_t size, bool last_compute)
{
    DEV_ASSERT(buffer);

    uint32_t i;
    uint32_t *u32_buf = (uint32_t *)buffer;
    uint32_t u32_bytes = size / sizeof(uint32_t);

    crc0_CRC32.seed = seed_crc;

    if (!last_compute)
    {
        /* The user expects further accumulate a CRC, therefore
         * do not impose functions that occur at the end of the
         * CRC computation. The result will be to return the exact
         * value in the DATA reg as-is to allow it to be fed in
         * as a seed value later. This permits splitting up calls
         * to the CRC32 algorithm. */
        crc0_CRC32.readTranspose = CRC_TRANSPOSE_NONE;
        crc0_CRC32.complementChecksum = false;
    }
    else
    {
        /* The user expects to have a resulting CRC at the end of
         * this computation, therefore impose the functions that
         * occur at the end of the CRC32 algorithm */
        crc0_CRC32.readTranspose = CRC_TRANSPOSE_BITS_AND_BYTES;
        crc0_CRC32.complementChecksum = true;
    }

    /* Always returns success */
    (void)CRC_DRV_Init(INST_CRC0, &crc0_CRC32);

    for (i = 0; i < u32_bytes; ++i, ++u32_buf)
    {
        CRC->DATAu.DATA = *u32_buf;
    }

    return CRC->DATAu.DATA;
}
