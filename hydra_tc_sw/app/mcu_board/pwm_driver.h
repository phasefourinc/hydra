/* Copyright (c) 2017-2020 by Phase Four, Inc. All Rights Reserved. Confidential and Proprietary.
 *
 * pwm_load_driver.h
 */

#ifndef PERIPHERALS_PWM_LOAD_DRIVER_H_
#define PERIPHERALS_PWM_LOAD_DRIVER_H_

/*!
 * @addtogroup PWM_Load_Driver
 * @{
 *
 * @file
 * @brief
 * This file defines the PWM Driver device driver.
 *
 * Global Invariants:
 *  - Configured for active high output
 *  - Output pulls low when halted for debug
 *  - PWM configured for 100KHz
 */

#include "mcu_types.h"
#include "ftm_pwm_driver.h"

#define PWM_LOAD_CAL_POLY_SIZE    4

/*! Structure to hold the PWM configuration */
typedef struct
{
    uint32_t instance;
    uint8_t  channel;
    ftm_state_t state;

    /* Calibration Poly Constants (C0+C1(x)+C2(x^2)+C3(x^3)) */
    const float *cal_poly_cn;

    float min_v;
    float max_v;
    uint16_t duty_cycle_cnt_fb;

} Pwm_t;


/*!
 * @brief  This function initializes the passed pwm structure used to
 *         standardize PWM usage. The user is expected to configure
 *         the scale value for the maximum size of a full range of
 *         values. For example, if @p scale_max_val is 2.5, then the
 *         the full scale is 0.0 (0% duty cycle) to 2.5 (100%) and
 *         setting a value of 1.25 will lead to a 50% duty cycle.
 *
 * @note   The FTM PWM must be configured for Debug Mode 01 and a
 *         Safe State of Low to ensure that the PWM does not accidentally
 *         drive any of the loads high when halting the MCU for
 *         debugging.
 *
 * @pre FTM module configured for Active High polarity, initial value of
 *      0% duty cycle (output low), Debug Mode 01, and Safe State low.
 * @pre @p pwm is not NULL
 * @pre @p init_config is not NULL
 * @pre @p pwm_config is not NULL
 * @pre @p scale_max_val is finite and greater than 0.0
 * @pre @p cal_poly_cn is not NULL and 4-byte aligned
 *
 * @param[out] pwm       The pwm structure to initialize
 * @param instance       The FTM_PWM instance to use from Processor Expert
 * @param channel        The PWM channel to use
 * @param init_config    The user config structure from Processor Expert
 * @param pwm_config     The PWM config structure from Processor Expert
 * @param cal_poly_cn    Pointer to the calibration polynomial constants
 * @param min_v          The minimum supported PWM driver voltage
 * @param max_v          The maximum supported PWM driver voltage
 * @return               Status of the FTM PWM module initialization
 */
status_t PWM_Driver_Init(Pwm_t *pwm,
                         uint32_t instance,
                         uint8_t channel,
                         ftm_user_config_t *init_config,
                         ftm_pwm_param_t *pwm_config,
                         const float *cal_poly_cn,
                         float min_v,
                         float max_v);


/*!
 * @brief  Sets the device that the PWM is driving to the provided
 *         current value. The duty cycle is set appropriately based
 *         on the full scale calibration configured at initialization
 *         and the current bus voltage. The PWM module is not set and
 *         STATUS_ERROR returned if @p voltage is outside of the
 *         initialized full scale range.
 *
 * @pre @p pwm is not NULL and has been initialized
 *
 * @param pwm      The pwm structure
 * @param voltage  The value to set the driving device to
 * @return         Status of the FTM PWM module update
 */
status_t PWM_Driver_SetVoltage(Pwm_t *pwm, float voltage);


/*!
 * @brief  Sets the PWM Load Driver current directly based off of the
 *         provided duty cycle percentage. The value is not set and
 *         STATUS_ERROR returned if @p duty_cycle_percent extends
 *         beyond the range [0.0, 1.0].
 *
 * @pre @p pwm is not NULL and has been initialized
 * @pre @p duty_cycle_percent is finite
 *
 * @param pwm                 The pwm structure
 * @param duty_cycle_percent  The load driver current to set
 * @return                    Status of the FTM PWM module update
 */
status_t PWM_Driver_SetDuty(Pwm_t *pwm, float duty_cycle_percent);


/*!
 * @brief  Sets the PWM Load Drive with the raw duty cycle code
 *         count, whose value is between 0 (0%) and 0x8000 (100%)
 *         inclusive.
 *
 * @param pwm             The pwm structure
 * @param duty_cycle_cnt  The duty cycle count
 * @return                Status of the FTM PWM module update
 */
status_t PWM_Driver_SetRaw(Pwm_t *pwm, uint16_t duty_cycle_cnt);

/*! @} */

#endif /* PERIPHERALS_PWM_LOAD_DRIVER_H_ */
