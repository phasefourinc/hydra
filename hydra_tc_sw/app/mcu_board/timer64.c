/* Copyright (c) 2017-2020 by Phase Four, Inc. All Rights Reserved. Confidential and Proprietary.
 *
 * timer64.c
 */

#include "timer64.h"
#include "mcu_types.h"
#include "lpit_driver.h"
#include "lpit0.h"
#include "clock_manager.h"


/* 64-bit timer using 2 32-bit LPIT Channels */


uint32_t timer_clk_freq = 0xFFFFFFFFUL;
uint32_t pit_inst = 0UL;
#define LPIT_CHANNEL	    0UL
#define LPIT_Channel_IRQn   LPIT0_Ch0_IRQn
#define Channel_0_Mask 0x01U
#define Channel_1_Mask 0x02U

void timer64_init(void)
{
	/* Initialize to an impossible frequency */
    status_t status = STATUS_ERROR;

    LPIT_DRV_Init(INST_LPIT0, &lpit0_InitConfig);

    /* Configure Channel 0 as a simple 32-bit periodic down-counter */
    status = LPIT_DRV_InitChannel(INST_LPIT0, LPIT_CHANNEL, &lpit0_ChnConfig0);
    DEV_ASSERT(status == STATUS_SUCCESS);

    /* Configure Channel 1 as a simple 32-bit periodic down-counter chained to Ch0 */

    status = LPIT_DRV_InitChannel(INST_LPIT0, 1UL, &lpit0_ChnConfig1);
    DEV_ASSERT(status == STATUS_SUCCESS);  /* Only fails if chain bit configured for Ch0 */

    status = CLOCK_SYS_GetFreq(LPIT0_CLK, &timer_clk_freq);
    DEV_ASSERT(status == STATUS_SUCCESS);
    /* DEV_ASSERT that the frequency returned is within a valid frequency for the MCU */
    DEV_ASSERT((timer_clk_freq > 0) && (timer_clk_freq <= TIMER32_MAX_CLK_FREQ));


    /* Start the timers, which begin right away */
	LPIT_DRV_StartTimerChannels(INST_LPIT0, Channel_0_Mask);
    LPIT_DRV_StartTimerChannels(INST_LPIT0, Channel_1_Mask);


    (void)status;  /* Avoids unused variable compiler warning */

    return;
}


void timer64_get_tick(uint32_t *tick_upper, uint32_t *tick_lower)
{
    DEV_ASSERT(tick_upper);
    DEV_ASSERT(tick_lower);

    /* Only reading the upper and lower timestamps once can lead to the following
     * race condition:
     *   1. Upper timestamp read at 0xFFFFFFFF
     *   2. Lower timestamp underflow, upper decremented to 0xFFFFFFFE
     *   3. Lower timestamp read at 0xFFFFFFFF
     *   4. Result looks like time went backwards to zero (i.e. 0xFFFFFFFF, 0xFFFFFFFF)
     *
     * To resolve this, read the upper and lower timestamps twice and then
     * check to see if an underflow event occurred.  If so, then use the
     * second (newly rolled over) reading.
     */

    uint32_t tick_upper1 = LPIT_DRV_GetCurrentTimerCount(INST_LPIT0, 1U);
    uint32_t tick_lower1 = LPIT_DRV_GetCurrentTimerCount(INST_LPIT0, 0U);

    uint32_t tick_upper2 = LPIT_DRV_GetCurrentTimerCount(INST_LPIT0, 1U);
    uint32_t tick_lower2 = LPIT_DRV_GetCurrentTimerCount(INST_LPIT0, 0U);

    /* Use first reading if no underflow (since it's possible the second
     * reading fell victim to the race condition */
    if (tick_upper1 == tick_upper2)
    {
        *tick_upper = tick_upper1;
        *tick_lower = tick_lower1;
    }
    /* Otherwise use the second reading just after the underflow */
    else
    {
        *tick_upper = tick_upper2;
        *tick_lower = tick_lower2;
    }

    return;
}



float timer64_get_duration_ms(uint32_t start_tick_upper,
                              uint32_t start_tick_lower,
                              uint32_t stop_tick_upper,
                              uint32_t stop_tick_lower)
{
    float duration_ms_upper;
    float duration_ms_lower;

    /* Calculate the number of ticks that have elapsed in the upper.  Upper tick rollover
     * is not handled since it will not happen */
    uint32_t count_upper = timer32_get_duration_ticks(start_tick_upper, stop_tick_upper);

    /* If the lower rolls over, then one of the upper ticks doesn't count */
    if (start_tick_lower < stop_tick_lower)
    {
        --count_upper;
    }

    /* The upper duration is the number of ticks that have elapsed in the upper multiplied
     * by the number of milliseconds per lower (2^32) rollover. */
    duration_ms_upper = (float)count_upper * (4294967296.0f / (float)(timer_clk_freq/1000UL));

    duration_ms_lower = timer32_get_duration_ms(start_tick_lower, stop_tick_lower);

    return (duration_ms_upper + duration_ms_lower);
}



uint32_t timer64_get_curr_time_ms(void)
{
    uint32_t ticks_hi;
    uint32_t ticks_lo;
    timer64_get_tick(&ticks_hi, &ticks_lo);

    /* Return the time since the start of both high and low tick counters */
    return (uint32_t)timer64_get_duration_ms(0xFFFFFFFFUL, 0xFFFFFFFFUL, ticks_hi, ticks_lo);
}


/* Important test cases:
 *  - if ticks = 0x0
 *  - if ticks = 0x1 and start_tick = 0
 *  - if ticks = 0x1 and start_tick = 1
 *  - if ticks = 0x1 and start_tick = 0xFFFF_FFFF
 *  - if ticks = 0xFFFF_FFFF and start_tick = 0
 *  - if ticks = 0xFFFF_FFFF and start_tick = 0xFFFF_FFFE
 *  - if ticks = 0xFFFF_FFFF and start_tick = 0xFFFF_FFFF
 */
void timer32_delay_ticks(uint32_t ticks)
{
    uint32_t start_tick = timer32_get_tick();
    uint32_t prev_tick = start_tick;
    uint32_t curr_tick = start_tick;
    uint32_t stop_tick = start_tick - ticks;

    /* If the stop_tick requires a down counter underflow/rollover
     * then wait for the curr_tick to rollover */
    if (stop_tick > start_tick)
    {
        while (curr_tick <= prev_tick)
        {
            prev_tick = curr_tick;
            curr_tick = timer32_get_tick();
        }

        /* Upon exiting this loop, the counter has just rolled over.
         * For very large wait ticks, it's possible that by the time
         * this while loop is exited, the delay has completed because
         * it not only rolled over but it fell below the stop_tick and/or
         * start_tick (e.g. stop_tick 0xFFFFFFFF, start_tick 0xFFFFFFFE). */
    }

    /* Wait for the counter to fall below the stop_tick */
    while (curr_tick > stop_tick)
    {
        prev_tick = curr_tick;
        curr_tick = timer32_get_tick();

        /* Don't expect any more rollovers, break if one does happen
         * as it means the counter fell past the stop_tick if
         * stop_tick is really close to 0x0. */
        if (curr_tick > prev_tick)
        {
            break;
        }
    }

    return;
}


void timer32_delay_us(uint32_t us)
{
    /* Ensure that the specified time does not exceed the max uint32_t tick value */
    DEV_ASSERT(us < (0xFFFFFFFFUL / (timer_clk_freq / 1000000UL)));
    timer32_delay_ticks((timer_clk_freq / 1000000UL) * us);
    return;
}


void timer32_delay_ms(uint32_t ms)
{
    /* Ensure that the specified time does not exceed the max uint32_t tick value */
    DEV_ASSERT(ms < (0xFFFFFFFFUL / (timer_clk_freq / 1000UL)));
    timer32_delay_ticks((timer_clk_freq / 1000UL) * ms);
    return;
}


float timer32_ticks_to_ms(uint32_t ticks)
{
    DEV_ASSERT(timer_clk_freq != 0UL);

    /* Converting ticks to float loses precision for large values */
    return ((float)ticks / ((float)timer_clk_freq / 1000.0f));
}


float timer32_get_duration_ms(uint32_t start_tick, uint32_t stop_tick)
{
    uint32_t duration_ticks = timer32_get_duration_ticks(start_tick, stop_tick);
    return timer32_ticks_to_ms(duration_ticks);
}


float timer32_time_since_ms(uint32_t start_tick)
{
    uint32_t curr_tick = timer32_get_tick();
    return timer32_get_duration_ms(start_tick, curr_tick);
}

