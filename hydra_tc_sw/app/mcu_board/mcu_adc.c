/* Copyright (c) 2017-2020 by Phase Four, Inc. All Rights Reserved. Confidential and Proprietary.
 *
 * mcu_adc.c
 */

#include "mcu_adc.h"
#include "mcu_types.h"
#include "adc_driver.h"
#include "mcuAdc0.h"
#include "mcuAdc1.h"
#include "timer64.h"

static uint16_t mcu_adc_ch_raw[MCU_ADC_CH_CNT] = { 0U };


void mcu_adc_init(void)
{
    DEV_ASSERT(mcuAdc0_ConvConfig0.continuousConvEnable == false);
    DEV_ASSERT(mcuAdc0_ConvConfig0.resolution == ADC_RESOLUTION_12BIT);
    DEV_ASSERT(mcuAdc1_ConvConfig0.continuousConvEnable == false);
    DEV_ASSERT(mcuAdc1_ConvConfig0.resolution == ADC_RESOLUTION_12BIT);

    ADC_DRV_ConfigConverter(INST_MCUADC0, &mcuAdc0_ConvConfig0);
    ADC_DRV_AutoCalibration(INST_MCUADC0);

    ADC_DRV_ConfigConverter(INST_MCUADC1, &mcuAdc1_ConvConfig0);
    ADC_DRV_AutoCalibration(INST_MCUADC1);

    return;
}


status_t mcu_adc_sample_ch_all(void)
{
    status_t status = STATUS_SUCCESS;

    /* Parallelize ADC reads as much as possible.
     * Delays selected based on 256 ADC cycles minimum
     * with 10MHz ADC clock */

    /* Interleave ADC0 and ADC1 to get samples in parallel */
    ADC_DRV_ConfigChan(INST_MCUADC0, 0U, &mcuAdc0_ChnConfig3);
    ADC_DRV_ConfigChan(INST_MCUADC1, 0U, &mcuAdc1_ChnConfig6);
    timer32_delay_us(28UL);
    status |= mcu_adc_sample_ch(INST_MCUADC0, MCU_ADC0_CH3);
    status |= mcu_adc_sample_ch(INST_MCUADC1, MCU_ADC1_CH6);

    ADC_DRV_ConfigChan(INST_MCUADC0, 0U, &mcuAdc0_ChnConfig7);
    ADC_DRV_ConfigChan(INST_MCUADC1, 0U, &mcuAdc1_ChnConfig8);
    timer32_delay_us(28UL);
    status |= mcu_adc_sample_ch(INST_MCUADC0, MCU_ADC0_CH7);
    status |= mcu_adc_sample_ch(INST_MCUADC1, MCU_ADC1_CH8);

    ADC_DRV_ConfigChan(INST_MCUADC0, 0U, &mcuAdc0_ChnConfig8);
    ADC_DRV_ConfigChan(INST_MCUADC1, 0U, &mcuAdc1_ChnConfig23);
    timer32_delay_us(28UL);
    status |= mcu_adc_sample_ch(INST_MCUADC0, MCU_ADC0_CH8);
    status |= mcu_adc_sample_ch(INST_MCUADC1, MCU_ADC1_CH23);

    ADC_DRV_ConfigChan(INST_MCUADC0, 0U, &mcuAdc0_ChnConfig9);
    ADC_DRV_ConfigChan(INST_MCUADC1, 0U, &mcuAdc1_ChnConfig24);
    timer32_delay_us(28UL);
    status |= mcu_adc_sample_ch(INST_MCUADC0, MCU_ADC0_CH9);
    status |= mcu_adc_sample_ch(INST_MCUADC1, MCU_ADC1_CH24);

    ADC_DRV_ConfigChan(INST_MCUADC0, 0U, &mcuAdc0_ChnConfig10);
    timer32_delay_us(28UL);
    status |= mcu_adc_sample_ch(INST_MCUADC0, MCU_ADC0_CH10);

    ADC_DRV_ConfigChan(INST_MCUADC0, 0U, &mcuAdc0_ChnConfig11);
    timer32_delay_us(28UL);
    status |= mcu_adc_sample_ch(INST_MCUADC0, MCU_ADC0_CH11);

    ADC_DRV_ConfigChan(INST_MCUADC0, 0U, &mcuAdc0_ChnConfig14);
    timer32_delay_us(28UL);
    status |= mcu_adc_sample_ch(INST_MCUADC0, MCU_ADC0_CH14);

    return status;
}


status_t mcu_adc_sample_ch(uint32_t instance, McuAdc_Ch_Idx_t ch)
{
    DEV_ASSERT((uint32_t)ch < MCU_ADC_CH_CNT);

    status_t status = STATUS_BUSY;

    if (!mcu_adc_is_busy(instance))
    {
        ADC_DRV_GetChanResult(instance, 0U, &mcu_adc_ch_raw[ch]);
        status = STATUS_SUCCESS;
    }

    return status;
}


uint16_t mcu_adc_get_ch_raw(McuAdc_Ch_Idx_t ch)
{
    DEV_ASSERT((uint32_t)ch < MCU_ADC_CH_CNT);
    return mcu_adc_ch_raw[ch];
}


float mcu_adc_get_ch_voltage(McuAdc_Ch_Idx_t ch)
{
    DEV_ASSERT((uint32_t)ch < MCU_ADC_CH_CNT);
    return (((float)mcu_adc_ch_raw[ch]/(float)MCUADC_MAX_RAW_12BIT) * MCUADC_VREF);
}


float mcu_adc_ch_calibrate(McuAdc_Ch_Idx_t ch, const float (*cal_poly_cn)[MCUADC_CAL_POLY_SIZE])
{
    uint32_t i;
    float cal_value = 0.0f;
    float x = mcu_adc_get_ch_voltage(ch);
    float xn = x;

    /* Calibrate the ADC value: C0 + C1x + C2x^2 + ... */
    cal_value = (*cal_poly_cn)[0];
    for (i = 1UL; i < MCUADC_CAL_POLY_SIZE; ++i)
    {
        cal_value += (*cal_poly_cn)[i] * xn;
        xn *= x;
    }

    /* Return a value converted to fixed point */
    return cal_value;
}


bool mcu_adc_is_busy(uint32_t instance)
{
    uint32_t reg_val = (instance == INST_MCUADC0) ? ADC0->SC2 : ADC1->SC2;
    return ((reg_val & ADC_SC2_ADACT_MASK) ? true : false);
}
