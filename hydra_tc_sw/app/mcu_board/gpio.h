/* Copyright (c) 2017-2020 by Phase Four, Inc. All Rights Reserved. Confidential and Proprietary.
 *
 * gpio.h
 */

#ifndef MCU_BOARD_GPIO_H_
#define MCU_BOARD_GPIO_H_

#include "mcu_types.h"


typedef enum
{
    GPIO_WDT_PET = 0,
    GPIO_WDT_TRIG_nCLR_EN,
    GPIO_HUM_TEMP_SEL,
    GPIO_WF_EN,
    GPIO_PP_EN,
    GPIO_LED0
} GPIO_Output_t;

typedef enum
{
    GPIO_WDT_TOGGLE_SEL,
    GPIO_WDT_RESET
} GPIO_Input_t;


/*!
 * @brief  Sets the specified GPIO output pin either high
 *         or low.
 *
 * @param pin      The pin to set
 * @param is_high  True is logic high, false is logic low
 */
void GPIO_SetOutput(GPIO_Output_t pin, bool is_high);


/*!
 * @brief  Toggles the specified GPIO output pin.
 *
 * @param pin  The pin to toggle
 */
void GPIO_ToggleOutput(GPIO_Output_t pin);


/*!
 * @brief  Returns the value of the GPIO input pin
 *
 * @param pin  The pin to read
 * @return     The read value
 */
uint8_t GPIO_GetInput(GPIO_Input_t pin);


/*!
 * @brief  Performes one full strobe of the WDT
 */
void GPIO_PetWDT(void);

#endif /* MCU_BOARD_GPIO_H_ */
