/* Copyright (c) 2017-2020 by Phase Four, Inc. All Rights Reserved. Confidential and Proprietary.
 *
 * MCUflash.c
 */

#include "MCUflash.h"
#include "Flash1.h"
#include "ErrAO.h"

/* Note: Considered adding custom errors to many of these functions rather than
 * using SDK status errors. Many of these errors are development-type errors, as such,
 * it seems reasonable to keep them as SDK errors for simplicity. The important one is
 * STATUS_BUSY which indicates that the flash is busy. This can be checked by the
 * caller if a custom error is desired.
 *
 * WARNING: Avoid adding Parameter Table dependencies to this file since these functions
 * are used to initialize them, which could turn into a chicken and the egg scenario. */

/* Future enhancements:
 *  - erase_flash_sector should pass retry as a parameter since its use may have
 *    timing restrictions (e.g. at startup)
 */

static flash_ssd_config_t flash1SSDConfig;

static status_t _erase_sector_drv( uint32_t dest, uint32_t size);
static status_t _flash_command_sequence(void);


status_t initialize_flash(void)
{
    status_t status = STATUS_SUCCESS;

    /* Initialize the Program Flash driver, which always returns STATUS_SUCCESS */
    status = FLASH_DRV_Init(&Flash1_InitConfig0, &flash1SSDConfig);
    DEV_ASSERT(status == STATUS_SUCCESS);

    /* Set the Program Flash protection for blocks. Once bits are set to protected (0),
     * they cannot be unprotected (1) for the rest of execution. Given this, start with
     * Golden Image region unprotected and then at a later time, protect that region
     * accordingly after the parameter tables are initialized.
     * Note: Failures are development related so an assert suffices */
    status = FLASH_DRV_SetPFlashProtection(MEMORY_FPROT_BITS_GOLD_UPDATABLE);
    DEV_ASSERT(status == STATUS_SUCCESS);

    return status;
}


/* Sets the Program Flash Protection Bits to maximum protection */
void mcuflash_set_fprot_full(void)
{
    status_t status = FLASH_DRV_SetPFlashProtection(MEMORY_FPROT_BITS_FULL);
    DEV_ASSERT(status == STATUS_SUCCESS);
    (void)status;  /* Removes unused variable compiler warning */
    return;
}


status_t erase_flash_sector(uint32_t start_address, UNUSED uint32_t length)
{
    status_t status = STATUS_SUCCESS;
    uint8_t try_count = 0;

    /* Must not be accessing the read-only flash memory area */
    if (start_address < MEMORY_MODIFY_ADDRESS_LINE)
    {
        status = STATUS_ERROR;
    }

    for (try_count = 0U; (try_count < MAX_ERASE_ATTEMPTS) && (status == STATUS_SUCCESS); ++try_count)
    {
        status = _erase_sector_drv(start_address, ERASE_SECTOR_SIZE);
    }

    return status;
}



/* Note:
 *  - Consider removing the try count and to pass that onto the user, as
 *    user may not want to retry and it is unclear how long each try takes.
 *
 *  - Length parameter is deprecated
 */
status_t erase_flash_sector_ao(GLADOS_AO_t *ao_src, uint32_t start_address, uint32_t length)
{
    status_t status = STATUS_SUCCESS;
    if (start_address < MEMORY_MODIFY_ADDRESS_LINE)
    {
        /*Error trying to write in the bootloader region*/
        report_error_uint(ao_src, (err_type*) Errors.FlashEraseFailedInvalidEraseAddress , DO_NOT_SEND_TO_SPACECRAFT, (uint32_t) start_address);
        status = STATUS_FLASH_ERASE_INVALID_ERASE_ADDRESS;
        return status;
    }
    else if (length != ERASE_SECTOR_SIZE)
    { /*Error length to big*/
        report_error_uint(ao_src, (err_type*) Errors.FlashEraseFailedInvalidSectorSize , DO_NOT_SEND_TO_SPACECRAFT, (uint32_t) length);
        status = STATUS_FLASH_ERASE_INVALID_SECTOR_SIZE;
    }
    /*Must be on a 4096 KB sector to erase.*/
    else if ((uint32_t)start_address % FEATURE_FLS_PF_BLOCK_SECTOR_SIZE != 0)
    { /*Error of non sector erase size*/
        report_error_uint(ao_src, (err_type*) Errors.FlashEraseFailedInvalidSectorAddress , DO_NOT_SEND_TO_SPACECRAFT, (uint32_t) start_address);
        status = STATUS_FLASH_ERASE_INVALID_SECTOR_ADDRESS;
    }
    /* Check CCIF to verify the previous command is completed */
    else if (0U == (FTFx_FSTAT & FTFx_FSTAT_CCIF_MASK))
    {
        report_error_uint(ao_src, (err_type*) Errors.FlashEraseFailedFlashStatusBusy, DO_NOT_SEND_TO_SPACECRAFT, 0UL);
        status = STATUS_FLASH_ERASE_INVALID_STATUS_BUSY;
    }
    else
    {
        uint8_t try_count = 0;
        bool erase_busy = true; /* If we get a erase busy try 5 more times. If it fails something is wrong and return the last status. */
        while ((try_count < MAX_ERASE_ATTEMPTS) && (erase_busy == true))
        {
            status = _erase_sector_drv(start_address, length);
            if (status == STATUS_SUCCESS)
            {
                erase_busy = false;
            }
            try_count++;
        }
        if (erase_busy==true)
        {
            report_error_uint(ao_src, (err_type*) Errors.FlashEraseFailed , DO_NOT_SEND_TO_SPACECRAFT, (uint32_t) start_address);
        }
    }
    return status;
}


status_t write_flash(uint32_t start_address, uint32_t length, uint8_t *pData)
{
    status_t status = STATUS_SUCCESS;

    if (start_address < MEMORY_MODIFY_ADDRESS_LINE)
    {
        status = STATUS_FLASH_WRITE_INVALID_ERASE_ADDRESS;
    }
    else if (length > MAX_MODIFY_LENGTH)
    {
        status = STATUS_FLASH_WRITE_OVERLIMIT_WRITE_LENGTH;
    }
    else
    {
        status = FLASH_DRV_Program(&flash1SSDConfig, start_address, length, pData);
    }
    return status;
}

/* This initiates a blocking call to write flash */
status_t write_flash_ao(GLADOS_AO_t *ao_src, uint32_t start_address, uint32_t length, uint8_t *pData)
{
    status_t status = STATUS_SUCCESS;
    if (length == 0UL)
    {
        status = STATUS_SUCCESS;
    }
    else if (start_address < MEMORY_MODIFY_ADDRESS_LINE)
    {
        report_error_uint(ao_src, (err_type*) Errors.FlashWriteFailedInvalidEraseAddress , DO_NOT_SEND_TO_SPACECRAFT, start_address);
        status = STATUS_FLASH_WRITE_INVALID_ERASE_ADDRESS;
    }
    else if (length > MAX_MODIFY_LENGTH)
    {
        report_error_uint(ao_src, (err_type*) Errors.FlashWriteFailedOverLimitWriteLength , DO_NOT_SEND_TO_SPACECRAFT, start_address);
        status = STATUS_FLASH_WRITE_OVERLIMIT_WRITE_LENGTH;
    }
    /* Check CCIF to verify the previous command is completed */
    else if (0U == (FTFx_FSTAT & FTFx_FSTAT_CCIF_MASK))
    {
        report_error_uint(ao_src, (err_type*) Errors.FlashWriteFailedFlashStatusBusy , DO_NOT_SEND_TO_SPACECRAFT, start_address);
        status = STATUS_FLASH_WRITE_INVALID_STATUS_BUSY;
    }
    else
    {
        status = FLASH_DRV_Program(&flash1SSDConfig, start_address, length, pData);

        if (status != STATUS_SUCCESS)
        {
            report_error_uint(ao_src, (err_type*) Errors.FlashWriteFailed , DO_NOT_SEND_TO_SPACECRAFT, start_address);
        }
    }
    return status;
}

status_t check_erase_flash_status(uint32_t start_address)
{
    status_t status = STATUS_SUCCESS;

    if ((FTFx_FSTAT & (FTFx_FSTAT_MGSTAT0_MASK | FTFx_FSTAT_FPVIOL_MASK| FTFx_FSTAT_ACCERR_MASK | FTFx_FSTAT_RDCOLERR_MASK)) != 0U)
    {
        status = STATUS_ERROR;
    }
    else
    {
        status = FLASH_DRV_VerifySection(&flash1SSDConfig, start_address, ERASE_SECTOR_SIZE_IN_DOUBLE_PHRASE, NORMAL_READ_MARGIN_LEVEL); /* NOTE: This has a while(1) in it and uses the blocking SDK. However the max delay time is 100 us */
    }
    return status;
}


status_t check_program_flash_status(uint32_t * start_address, uint32_t length, uint8_t *pData, uint32_t *pFailAddr)
{
    status_t status = STATUS_SUCCESS;

    if (length > MAX_MODIFY_LENGTH)
    {
        status = STATUS_ERROR;
    }
    else
    {
        status = FLASH_DRV_ProgramCheck(&flash1SSDConfig, (uint32_t)start_address, length, pData, pFailAddr, USER_READ_MARGIN_LEVEL);
    }
    return status;
}


static status_t _erase_sector_drv( uint32_t dest, uint32_t size)
{
    status_t ret = STATUS_SUCCESS;       /* Return code variable */
    uint32_t sectorSize;                 /* Size of one sector   */
    uint32_t temp;                       /* Temporary variable   */
    uint32_t tempSize = size;            /* Temporary of size variation */

#if FEATURE_FLS_HAS_FLEX_NVM
    temp = flash1SSDConfig.DFlashBase;
    if ((dest >= temp) && (dest < (temp + flash1SSDConfig.DFlashSize)))
    {
        DEV_ASSERT((dest % FEATURE_FLS_DF_SECTOR_CMD_ADDRESS_ALIGMENT) == 0U);
        dest += 0x800000U - temp;
        sectorSize = (uint32_t)FEATURE_FLS_DF_BLOCK_SECTOR_SIZE;
    }
    else
#endif
    {
        temp = flash1SSDConfig.PFlashBase;
        if ((dest >= temp) && (dest < (temp + flash1SSDConfig.PFlashSize)))
        {
            DEV_ASSERT((dest % FEATURE_FLS_PF_SECTOR_CMD_ADDRESS_ALIGMENT) == 0U);
            dest -= temp;
            sectorSize = (uint32_t)FEATURE_FLS_PF_BLOCK_SECTOR_SIZE;
        }
        else
        {
            ret = STATUS_ERROR;
            tempSize = 0U;
            sectorSize = 0U;
        }
    }

    /* Check if the size is sector alignment or not */
    if ((tempSize & (sectorSize - 1U)) != 0U)
    {
        /* Return an error code */
        ret = STATUS_ERROR;
    }

    while ((tempSize > 0U) && (STATUS_SUCCESS == ret))
    {
        /* Check CCIF to verify the previous command is completed */
        if (0U == (FTFx_FSTAT & FTFx_FSTAT_CCIF_MASK))
        {
            ret = STATUS_BUSY;
        }
        else
        {
            /* Clear RDCOLERR & ACCERR & FPVIOL flag in flash status register. Write 1 to clear */
            CLEAR_FTFx_FSTAT_ERROR_BITS;

            /* Passing parameter to the command */
            FTFx_FCCOB0 = FTFx_ERASE_SECTOR;
            FTFx_FCCOB1 = GET_BIT_16_23(dest);
            FTFx_FCCOB2 = GET_BIT_8_15(dest);
            FTFx_FCCOB3 = GET_BIT_0_7(dest);

            /* Calling flash command sequence function to execute the command */
            ret = _flash_command_sequence();

            /* Update size and destination address */
            tempSize -= sectorSize;
            dest += sectorSize;
        }
    }

    return ret;
}

/*This function will check if a section of flash is empty or not by seeing if the value is 0xff which it is its default value.
 * It does this by taking the flash_address and converting it to a pointer.
 * From there, the pointer checks if each value is empty (0xff) up until the size argument.
 * */
bool check_if_flash_is_empty(uint32_t flash_address, uint32_t size)
{
    uint8_t* start_point = (uint8_t *)flash_address;
    uint32_t count = 0;
    while (count < size)
    {

        if (*start_point != 0xFF)
        {
            return false;
        }
        start_point ++;
        count++;

    }

    return true;
}


status_t _flash_command_sequence(void)
{
    status_t ret = STATUS_SUCCESS;    /* Return code variable */

    /* Clear CCIF to launch command */
    FTFx_FSTAT |= FTFx_FSTAT_CCIF_MASK;

    /* Check if an error is occurred */
    if ((FTFx_FSTAT & (FTFx_FSTAT_MGSTAT0_MASK | FTFx_FSTAT_FPVIOL_MASK | FTFx_FSTAT_ACCERR_MASK | FTFx_FSTAT_RDCOLERR_MASK)) != 0U)
    {
        ret = STATUS_ERROR;
    }

    return ret;
}
