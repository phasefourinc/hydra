set mypath=%~dp0
echo "Unmounting any prior mounts to M drive"
subst /d M:
echo "Mounting the current directory to M drive"
subst M: "%mypath:~0,-1%"
