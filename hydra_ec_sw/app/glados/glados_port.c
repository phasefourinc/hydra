/* Copyright (c) 2017-2020 by Phase Four, Inc. All Rights Reserved. Confidential and Proprietary.
 *
 * glados_port.c
 */


#include "glados_port.h"
#include "mcu_types.h"
#include "exceptions.h"
#include "p4_err.h"

void GLADOS_Error_Critical(void)
{
    Exceptions_HandleUnrecoverable(Errors.GladosErrorCritical, 0UL);
    return;
}


void GLADOS_Error_NonCritical(void)
{
    Exceptions_HandleNonCritical(Errors.GladosErrorNoncritical, 0UL);
    return;
}

