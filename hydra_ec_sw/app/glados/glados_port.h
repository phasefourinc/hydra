/* Copyright (c) 2017-2020 by Phase Four, Inc. All Rights Reserved. Confidential and Proprietary.
 *
 * glados_port.h
 */

#ifndef GLADOS_PORT_H_
#define GLADOS_PORT_H_

/*!
 * @file
 * This file contains the interface file for porting the OS into
 * different user systems.  The user is expected to customize each
 * of these interface functions for OS to use.
 *
 * Global invariants:
 *  - Timer providing ticks to the OS must be a down-counter (glados_timer requires it)
 */

/* User must define mcu_types.h */
#include "mcu_types.h"
#include "timer64.h"

#define GLADOS_PORT_PRINT_MEM_ALLOC                1024U
#define GLADOS_PORT_PRINT_OUTBUF_SIZE               512U

/* Define these stack address macros for the OS to use, typically
 * pulled from linker variables called out in the .map file */
extern uint32_t __StackTop;
extern uint32_t __HeapLimit;
extern uint32_t __StackLimit;
#define GLADOS_PORT_STACK_BASE    (&__StackTop)
#define GLADOS_PORT_STACK_TOP     (&__StackLimit)
/* Define the assert function for the OS to use */

#define GLADOS_ASSERT    DEV_ASSERT


/*!
 * @brief  Unlock the critical section by re-enabling interrupt service
 *         requests and clearing the instruction pipeline.
 */
static inline void GLADOS_CritSection_Unlock(void)
{
#ifdef GLADOS_INTERRUPT_SAFE
    /* Note: This code is for ARM */

    /* Only enable interrupts if they were already manually disabled */
    if (!__get_PRIMASK())
    {
        __enable_irq();

        /* ARM recommended memory barrier, sync data and ins */
        __DSB();
        __ISB();
    }
#endif
    return;
}


/*!
 * @brief  Lock the critical section by disabling interrupt service
 *         requests and clearing the instruction pipeline.
 */
static inline void GLADOS_CritSection_Lock(void)
{
#ifdef GLADOS_INTERRUPT_SAFE
    /* Note: This code is for ARM */

    __disable_irq();

    /* ARM recommended memory barrier, sync data and ins */
    __DSB();
    __ISB();
#endif
    return;
}


/*!
 * @brief  Get the 64-bit clock tick
 *
 * @pre @tick_upper is not NULL
 * @pre @tick_lower is not NULL
 *
 * @param @tick_upper[out]  Upper 32-bits of clock tick
 * @param @tick_upper[out]  Lower 32-bits of clock tick
 */
static inline void GLADOS_Get_Tick(uint32_t *tick_upper, uint32_t *tick_lower)
{
    timer64_get_tick(tick_upper, tick_lower);
    return;
}


/*!
 * @brief  Get the lower 32 bits of the clock.
 *
 * @return uint32_t  Lower 32 bits of clock tick
 */
static inline uint32_t GLADOS_Get_Tick_Lower(void)
{
    return timer32_get_tick();
}


/*!
 * @brief  Get the OS clock frequency.
 *
 * @return uint32_t  OS clock frequency
 */
static inline uint32_t GLADOS_Get_Clock_Freq(void)
{
    return timer_clk_freq;
}


/*!
 * @brief  Compute elapsed time in clock ticks
 *
 * @param @start_tick  duration start tick count
 * @param @start_tick  duration end tick count
 * @return uint32_t    elapsed duration in timer ticks
 */
static inline uint32_t GLADOS_Time_Duration_ticks(uint32_t start_tick, uint32_t stop_tick)
{
    return start_tick - stop_tick;
}


/*!
 * @brief  Compute elapsed time in milliseconds
 *
 * @param @start_tick  duration start tick count
 * @param @start_tick  duration end tick count
 * @return float       elapsed duration in milliseconds
 */
static inline float GLADOS_Time_Duration_ms(uint32_t start_tick, uint32_t stop_tick)
{
    return timer32_get_duration_ms(start_tick, stop_tick);
}


/*!
 * @brief  Get the current clock time in milliseconds
 *
 * @param @tick_upper  clock upper
 * @param @tick_lower  clock lower
 * @return float       current clock time in milliseconds
 */
static inline float GLADOS_Time_Now_ms(uint32_t tick_upper, uint32_t tick_lower)
{
    return timer64_get_duration_ms(0xFFFFFFFFUL, 0xFFFFFFFFUL, tick_upper, tick_lower);
}


/*!
 * @brief  Get the current clock time in microseconds
 *
 * @param @tick_upper  clock upper
 * @param @tick_lower  clock lower
 * @return uint32_t    current clock time in microseconds
 */
static inline uint64_t GLADOS_Time_Now_us(uint32_t tick_upper, uint32_t tick_lower)
{
    float curr_time_us = GLADOS_Time_Now_ms(tick_upper, tick_lower) * 1000.0f;
    return (uint64_t)curr_time_us;
}


/* Critical Errors should safe the system and place into a safe-mode if attempting to recover */
void GLADOS_Error_Critical(void);

/* Recoverable errors */
void GLADOS_Error_NonCritical(void);


void GLADOS_Port_Print(int8_t *print_buffer, int16_t char_cnt);


#endif /* GLADOS_PORT_H_ */
