/* Copyright (c) 2018-2020 by Phase Four, Inc. All Rights Reserved. Confidential and Proprietary.
 *
 * cy15.h
 */

#ifndef CY15_CODE_CY15_H_
#define CY15_CODE_CY15_H_

/*!
 * @addtogroup CY15B104Q
 * @{
 *
 * @file
 * @brief
 * This file defines the device driver for the CY15B104Q series
 * serial FRAM chip.
 *
 * Datasheet:
 *  - http://www.cypress.com/file/209146/download
 *
 * Hardware configuration:
 *  - 2.0V <= VDD <= 3.6V decoupled with 0.1uF cap
 *  - /HOLD pin tied to VDD (unused)
 *  - /WP Write Protection pin to VDD (unused)
 *  - Exposed pad connected (optional) to GND or NC
 *
 * Global Invariants:
 *  - Reads are into a private buffer that must be protected until it is copied by the user
 *  - Blocking SPI block transfers require at least 5MHz due to hard timeout code implementation
 *  - Bytes per SPI frame is 1
 *
 * Hardware considerations:
 *  - Max SPI SCLK is 40MHz
 *  - SPI modes 0 and 3
 *  - FRAM rolls over to the start address if reading past the last address
 *
 * Timings:
 *  - Data is read/written in real time (sweet jesus!)
 *
 * Command Format:
 *  - First byte is a command byte to indicate which command to perform,
 *  - the bytes that follow depend on the type of command
 *  - Read commands follow with 4 bytes of Address data (MSB first)
 */

#include "mcu_types.h"

#define CY15_MEMORY_SIZE           (512U * 1024U)   /*!< Total memory size in bytes */
#define CY15_SPI_BITS_PER_FRAME    8U               /*!< The device's minimum required SPI frame */
#define CY15_BLOCK_SIZE            512              /*!< Arbitrary size to use for block transfers (DSPI limitation) */
#define CY15_MAX_XFER_SIZE         (CY15_BLOCK_SIZE + 4U)
#define CY15_DEFAULT_TIMEOUT_MS    2                /*!< This value depends on the SPI CLK rate */

/*! Enumerates the supported commands */
typedef enum
{
    CY15_CMD_WREN        = 0x06,    /*!< Write Enable */
    CY15_CMD_WRDI        = 0x04,    /*!< Write Disable */
    CY15_CMD_RDSR        = 0x05,    /*!< Read Status Register */
    CY15_CMD_WRSR        = 0x01,    /*!< Write Status Register */
    CY15_CMD_READ        = 0x03,    /*!< Normal Read */
    CY15_CMD_FAST_READ   = 0x0B,    /*!< Fast Read */
    CY15_CMD_WRITE       = 0x02,    /*!< Write memory data */
    CY15_CMD_SLEEP       = 0xB9,    /*!< Enter sleep mode */
    CY15_CMD_RDID        = 0x9F     /*!< Read device ID */
} CY15_Command_t;


/* Public function declarations */

/*!
 * @brief This function initializes the provided Flash Device and its peripheral.
 *
 * @note   This function expects to be the first function called prior to
 *         communicating with the peripheral.
 *
 * @pre @p device is not NULL
 * @pre @p spi_bus is not NULL
 *
 * @post @p device handle has been initialized even if a communication error status
 *       is returned
 *
 * @param instance  The selected Flash device handle to be initialized
 * @param cs           The SPI Chip Select
 * @return             The status of the SPI communication
 */
status_t CY15_FRAM_Init(uint32_t instance, uint8_t cs);


status_t CY15_FRAM_ReadID(uint32_t instance, uint8_t cs, uint8_t *rx_buf);
status_t CY15_FRAM_SetWEL(uint32_t instance, uint8_t cs, bool enable);
status_t CY15_FRAM_ReadStatus(uint32_t instance, uint8_t cs, uint8_t *reg_value);
status_t CY15_FRAM_WriteStatus(uint32_t instance, uint8_t cs, uint8_t reg_value);
status_t CY15_FRAM_ReadBlock_NonBlocking(uint32_t instance, uint8_t cs, uint32_t addr, uint16_t num_of_bytes, uint8_t **read_buf);
status_t CY15_FRAM_ReadBlock(uint32_t instance, uint8_t cs, uint32_t addr, uint16_t num_of_bytes, uint8_t **read_buf);
status_t CY15_FRAM_Read(uint32_t instance, uint8_t cs, uint32_t addr, uint16_t num_of_bytes, uint8_t *read_buf);
status_t CY15_FRAM_WriteBlock_NonBlocking(uint32_t instance, uint8_t cs, uint32_t addr, uint16_t num_of_bytes, uint8_t *write_buf);
status_t CY15_FRAM_WriteBlock(uint32_t instance, uint8_t cs, uint32_t addr, uint16_t num_of_bytes, uint8_t *write_buf);
status_t CY15_FRAM_Write(uint32_t instance, uint8_t cs, uint32_t addr, uint16_t num_of_bytes, uint8_t *write_buf);
void CY15_FRAM_AbortTransfer(uint32_t instance);

/*! @} */

#endif /* CY15_CODE_CY15_H_ */
