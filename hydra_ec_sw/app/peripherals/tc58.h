/* Copyright (c) 2018-2019 by Phase Four, Inc. All Rights Reserved. Confidential and Proprietary.
 *
 * tc58.h
 */
#ifndef TC58_CODE_TC58_H_
#define TC58_CODE_TC58_H_

/*!
 * @addtogroup TC58CVG
 * @{
 *
 * @file
 * @brief
 * This file defines the device driver for the TC58CVG series
 * serial NAND Flash chip.
 *
 * Datasheet:
 *  - https://media.digikey.com/pdf/Data%20Sheets/Toshiba%20PDFs/TC58CVG0S3HxAIx_Rev1_2016-11-08.pdf
 *
 * Hardware configuration:
 *  - 2.0V <= VDD <= 3.6V decoupled with 0.1uF cap
 *  - /HOLD pin tied high to VDD (unused)
 *  - /WP Write Protection pin to VDD (unused)
 *  - Exposed pad connected (optional) to GND or NC
 *
 * Global Invariants:
 *  - ECC is enabled (device default)
 *  - Only main memory is programmable, spare is unused and unaccessible,
 *    which allows for easy address translation
 *
 * Hardware considerations:
 *  - Max SPI SCLK is 40MHz
 *  - SPI modes 0 and 3
 *  - ONLY 4 partial writes within a page (i.e. 4x 512B writes)
 *  - Max 1 page of data can be read for any given read
 *
 * Timings:
 *  - Page write time: 500us per 2KB page (2176B no ECC)
 *  - Block Erase time: 7ms/128KB block
 */

#include "mcu_types.h"
#include "lpspi_master_driver.h"

#define TC58_MIN_PAGE_SIZE           512U
#define TC58_PAGE_SIZE               2048UL
#define TC58_SPARE_SIZE              16UL
#define TC58_PAGES_PER_BLOCK         64UL
#define TC58_TOTAL_BLOCKS            1024UL

#define TC58_BLOCK_SIZE              (TC58_PAGES_PER_BLOCK * TC58_PAGE_SIZE)
#define TC58_MEMORY_SIZE             (TC58_TOTAL_BLOCKS * TC58_BLOCK_SIZE)
#define TC58_2ADDR(addr)             ((uint32_t)addr & (TC58_MEMORY_SIZE - 1UL))
#define TC58_FLASH_ROW(addr)         ((uint32_t)addr / TC58_PAGE_SIZE)
#define TC58_FLASH_COL(addr)         ((uint32_t)addr & (TC58_PAGE_SIZE - 1UL))
#define TC58_ON_PAGE_BOUNDARY(addr)  ((addr & (TC58_PAGE_SIZE  - 1UL)) == 0UL)
#define TC58_ON_BLOCK_BOUNDARY(addr) ((addr & (TC58_BLOCK_SIZE - 1UL)) == 0UL)

#define TC58_SPI_BITS_PER_FRAME      8U                      /*!< The device's minimum required SPI frame */
#define TC58_MAX_XFER_CMD_SIZE       4U                      /*!< 1 byte CMD, 2 bytes address, 1 dummy byte */
#define TC58_MAX_XFER                (TC58_PAGE_SIZE + TC58_MAX_XFER_CMD_SIZE)

#define TC58_XFER_TIMEOUT_MS         2U

/*! Enumerates the supported commands */
typedef enum
{
    TC58_CMD_READ_CELL   = 0x13,    /*!< Read Cell Away to prep data into internal buffer */
    TC58_CMD_READ_PAGE   = 0x03,    /*!< Read up to 1 page in 1-wire mode (0x03/0x0B) */
    TC58_CMD_READ_FAST   = 0x0B,    /*!< Fast Read */
    TC58_CMD_READ_2      = 0x3B,    /*!< Read Buffer 2-wire mode */
    TC58_CMD_READ_4      = 0x6B,    /*!< Read Buffer 4-wire mode */
    TC58_CMD_PROG_LOAD   = 0x02,    /*!< Write Data to internal buffer */
    TC58_CMD_PROG_EXE    = 0x10,    /*!< Program Execute */
    TC58_CMD_PROT_EXE    = 0x2A,    /*!< Protect Execute */
    TC58_CMD_LOAD_RAND   = 0x84,    /*!< Load Random Data */
    TC58_CMD_BLK_ERASE   = 0xD8,    /*!< Block Erase */
    TC58_CMD_WRITE_EN    = 0x06,    /*!< Write Enable */
    TC58_CMD_WRITE_DS    = 0x04,    /*!< Write Disable */
    TC58_CMD_GET_FEAT    = 0x0F,    /*!< Get Feature/Status Register */
    TC58_CMD_SET_FEAT    = 0x1F,    /*!< Set Feature/Status Register */
    TC58_CMD_READ_ID     = 0x9F,    /*!< Read device ID */
    TC58_CMD_RESET       = 0xFE     /*!< Reset device */

} TC58_Command_t;


/* Public function declarations */

/*!
 * @brief This function initializes the provided Flash Device and its peripheral.
 *
 * @note   This function expects to be the first function called prior to
 *         communicating with the peripheral.
 *
 * @pre @p device is not NULL
 * @pre @p spi_bus is not NULL
 *
 * @post @p device handle has been initialized even if a communication error status
 *       is returned
 *
 * @param instance  The selected Flash device handle to be initialized
 * @param whichPCS      The SPI bus configuration the device is connected to
 * @param cs           The SPI Chip Select
 * @return             The status of the SPI communication
 */
status_t TC58_Flash_Init(uint32_t instance, uint8_t cs);
uint16_t TC58_Flash_ReadID(uint32_t instance, uint8_t cs);
status_t TC58_Flash_LoadPage(uint32_t instance, uint8_t cs, uint32_t addr, uint16_t num_of_bytes, uint8_t *write_buf);
status_t TC58_Flash_BlockErase(uint32_t instance, uint8_t cs, uint32_t addr);
void TC58_Flash_AbortTransfer(uint32_t instance);

status_t TC58_Flash_ReadCell(uint32_t instance, uint8_t cs, uint32_t addr);
status_t TC58_Flash_LoadCell(uint32_t instance, uint8_t cs, uint32_t addr);
status_t TC58_Flash_GetInternalBuf(uint32_t instance, uint8_t cs, uint16_t addr_col, uint16_t num_of_bytes, uint8_t **read_buf);
status_t TC58_Flash_GetInternalBuf_NonBlocking(uint32_t instance, uint8_t cs, uint16_t addr_col, uint16_t num_of_bytes, uint8_t **read_buf);
status_t TC58_Flash_SetInternalBuf(uint32_t instance, uint8_t cs, uint16_t addr_col, uint16_t num_of_bytes, uint8_t *write_buf);
status_t TC58_Flash_SetInternalBuf_NonBlocking(uint32_t instance, uint8_t cs, uint16_t addr_col, uint16_t num_of_bytes, uint8_t *write_buf);
status_t TC58_Flash_SetWEL(uint32_t instance, uint8_t cs, bool enable);
status_t TC58_Flash_Reset(uint32_t instance, uint8_t cs);
status_t TC58_Flash_BusyWait(uint32_t instance, uint8_t cs, uint32_t cnt);
status_t TC58_Flash_WELEnableWait(uint32_t instance, uint8_t cs, uint32_t cnt);
status_t TC58_Flash_LastProgStatus(uint32_t instance, uint8_t cs);
status_t TC58_Flash_LastEraseStatus(uint32_t instance, uint8_t cs);
status_t TC58_Flash_GetPageEcc(uint32_t instance, uint8_t cs, uint32_t *corr_counts, uint32_t *uncorr_counts);

/*! @} */

#endif /* TC58_CODE_TC58_H_ */
