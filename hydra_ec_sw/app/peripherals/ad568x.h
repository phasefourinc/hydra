/* Copyright (c) 2017-2020 by Phase Four, Inc. All Rights Reserved. Confidential and Proprietary.
 *
 * ad568x.h
 */

#ifndef PERIPHERALS_AD568X_H_
#define PERIPHERALS_AD568X_H_

/*!
 * @addtogroup AD568x
 * @{
 *
 * @file
 * @brief
 * This file defines the device driver for the AD568x series
 * Digital to Analog Converter.
 *
 * Vout = Vref * Gain * D / (2^n)
 * where:
 *  - Gain defaults to 1
 *  - D is the 16-bit count
 *  - n is the bits of precision
 *
 * Datasheet:
 *  - https://www.analog.com/media/en/technical-documentation/data-sheets/AD5683R_5682R_5681R_5683.pdf
 *
 * Hardware configuration:
 *  - 2.7V <= VDD <= 5.5V decoupled with 0.1uF cap
 *  - 1.62V <= Vlogic <= 5.5V
 *  - /RESET tied to Vlogic if unused
 *  - /LDAC tied to GND (unused)
 *  - Vref decoupled with 10nF cap
 *
 * Global Invariants:
 *  - No SPI reads supported
 *  - Resets to output 0 and internal 2.5V reference (device default)
 *  - Only supports 16-bit DAC operation
 *
 * Hardware considerations:
 *  - Max SPI SCLK is 50MHz for Vlogic > 2.7V, 30MHz for Vlogic < 2.7V
 *  - SPI modes: 2
 *  - 20ns between transmissions
 *  - Minimum /RESET pulse width low is 75ns
 *
 */

#include "mcu_types.h"
#include "flexio.h"
#include "flexio_spi_driver.h"

#define AD568X_BITS_PRECISION    16U  /* Valid values are 12, 14, 16 */
#define AD568X_CAL_POLY_SIZE     4

/*! The LSnibble is the number of format bits to left shift
 *  prior to transferring the data. MSnibble is the value
 *  of the command bits. This is necessary because even
 *  though all commands fit into a 24-bit word, the
 *  formatting of the data within the word may differ for
 *  each command.
 */
typedef enum
{
    AD568X_NOP = 0x00,  /*!< Do nothing */
    AD568X_WIR = 0x14,  /*!< Write Input Register */
    AD568X_UDR = 0x20,  /*!< Update DAC Register */
    AD568X_WDI = 0x34,  /*!< Write DAC and Input Register */
    AD568X_WCR = 0x40,  /*!< Write Control Register */
    AD568X_RIR = 0x50,  /*!< Readback Input Register */
} AD568x_Cmds_t;

#define AD568X_CMD_C_BITS_MASK      0xF0UL
#define AD568X_CMD_FMT_BITS_MASK    0x0FUL

#define AD568X_WCR_GAIN_BIT         (1UL << 15U)
#define AD568X_WCR_INTREF_BIT       (1UL << 16U)


typedef struct AD568x_tag
{
    uint32_t instance;
    float max_value;

    /* Calibration Poly Constants for each channel {C0,C1,C2,C3} */
    const float *cal_poly_cn;

    flexio_device_state_t flexio_state;
    flexio_spi_master_state_t flexio_master;

} AD568x_t;


/*!
 * @brief  Initializes the DAC structure and FlexIO port for SPI communication.
 *
 * @pre @p dac is not NULL
 * @pre @p user_config is not NULL
 * @pre @p max_value is finite and greater than 0
 * @pre @p cal_poly_cn is not NULL
 *
 * @param[out] dac     The DAC structure to initialize
 * @param instance     The FlexIO instance from Processor Expert
 * @param user_config  The FlexIO SPI user config from Processor Expert
 * @param max_value    The max value that the DAC sets
 * @param cal_poly_cn  Address of the calibration polynomial constants array
 */
void AD568x_Init(AD568x_t *dac, uint32_t instance, const flexio_spi_master_user_config_t * user_config,
                             float max_value, const float cal_poly_cn[AD568X_CAL_POLY_SIZE]);


/*!
 * @brief  Writes the DAC Control Register using a blocking SPI call.
 *
 * @pre @p dac is not NULL and is initialized
 *
 * @param dac        The DAC structure
 * @param reg_value  The data value to write, size changes depending on command
 * @return
 */
status_t AD568x_SetControlReg_Blocking(AD568x_t *dac, uint32_t reg_value);


/*!
 * @brief  Calibrates the provided value to a DAC code and transfers it to the DAC.
 *         If the provided value is outside the initialized range (0-max_value),
 *         then the value is coerced to the allowable boundary.
 *
 * @pre @p dac is not NULL and is initialized
 * @pre @p val is finite
 *
 * @param dac  The DAC structure
 * @param val  The value to calibrate to a code and set the DAC
 * @return     The status of the SPI transfer
 */
status_t AD568x_SetVal_Blocking(AD568x_t *dac, float val);


/*!
 * @brief  Performs a SPI transfer to the DAC using the specified command and
 *         code value.
 *
 * @pre @p dac is not NULL and is initialized
 *
 * @param dac   The DAC structure
 * @param cmd   The DAC command to use for the transfer
 * @param code  The raw code to set the register per the command
 * @return      The status of the SPI transfer
 */
status_t AD568x_XferRaw_Blocking(AD568x_t *dac, AD568x_Cmds_t cmd, uint32_t code);


/*!
 * @brief  Aborts the current FlexIO SPI transfer and resets shift registers.
 *
 * @pre @p dac is not NULL and is initialized
 */
void AD568x_XferAbort(AD568x_t *dac);

/*! @} */

#endif /* PERIPHERALS_AD568X_H_ */
