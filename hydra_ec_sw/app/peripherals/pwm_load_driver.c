/* Copyright (c) 2017-2020 by Phase Four, Inc. All Rights Reserved. Confidential and Proprietary.
 *
 * pwm_load_driver.c
 */

#include "pwm_load_driver.h"
#include "mcu_types.h"
#include "ftm_pwm_driver.h"
#include <math.h>


status_t PWM_Load_Init(Pwm_t *pwm,
                       uint32_t instance,
                       uint8_t channel,
                       ftm_user_config_t *init_config,
                       ftm_pwm_param_t *pwm_config,
                       const float (*cal_poly_cn)[PWM_LOAD_CAL_POLY_SIZE],
                       float load_ohms,
                       float max_load_v)
{
    DEV_ASSERT(pwm);
    DEV_ASSERT(isfinite(load_ohms));
    DEV_ASSERT(load_ohms > 0.0f);
    DEV_ASSERT(isfinite(max_load_v));
    DEV_ASSERT(max_load_v > 0.0f);
    DEV_ASSERT(cal_poly_cn);
    DEV_ASSERT((((uint32_t)(*cal_poly_cn)) & 0x3UL) == 0x0UL);  /* Validate 4-byte alignment for FP */


    /* IMPORTANT: Validate that the FTM defaults to zero duty cycle and that it
     *            outputs to 0 when halted for debugging, unless of course there
     *            is a yearning for fireworks. Active high means POLn is 0. See
     *            RM p1238 and p1351 for more detail. However testing indicates
     *            that the debug halted value is determined by safeState rather
     *            than polarity. Check both anyways since this driver assumes
     *            active high polarity. */
    DEV_ASSERT(pwm_config->pwmIndependentChannelConfig->uDutyCyclePercent == 0U);
    DEV_ASSERT(init_config->BDMMode == FTM_BDM_MODE_01);
    DEV_ASSERT(pwm_config->pwmIndependentChannelConfig->polarity == FTM_POLARITY_HIGH);
    DEV_ASSERT(pwm_config->pwmIndependentChannelConfig->safeState == FTM_LOW_STATE);

    status_t status;

    pwm->instance = instance;
    pwm->channel = channel;
    pwm->load_ohms = load_ohms;
    pwm->max_load_v = max_load_v;
    pwm->cal_poly_cn = cal_poly_cn;

    /* Though unlikely, if the bus_v is not set sometime prior to usage of the PWM
     * driver, it will default to this initial value. Therefore this initial value
     * has implications. If the value defaults to the minimum (22V) and the actual
     * bus voltage is 36V, it will drive much more current than expected. Conversely,
     * if value defaults to the maximum (36V) and the actual bus voltage is 22V, it
     * will drive much less current than expected, which can potentially not drive
     * or actuate the appropriate hardware.
     *
     * For now, given the hardware available to us, it seems better to assume the
     * bus is low and to actually drive more than expected since heating ops are
     * less affected, and isolation valves are still actuating but above the rating
     * which is only expected to reduce lifetime of the valve. */
    pwm->bus_v = PWM_LOAD_BUS_V_MIN;

    status  = FTM_DRV_Init(instance, init_config, &pwm->state);
    status |= FTM_DRV_InitPwm(instance, pwm_config);

    return status;
}


void PWM_Load_SetVBus(Pwm_t *pwm, float bus_v)
{
    DEV_ASSERT(pwm);

    if ((!isfinite(bus_v)) || (bus_v < PWM_LOAD_BUS_V_MIN))
    {
        pwm->bus_v = PWM_LOAD_BUS_V_MIN;
    }
    else if (bus_v > PWM_LOAD_BUS_V_MAX)
    {
        pwm->bus_v = PWM_LOAD_BUS_V_MAX;
    }
    else
    {
        pwm->bus_v = bus_v;
    }


    return;
}


status_t PWM_Load_SetCurrent(Pwm_t *pwm, float i_mA)
{
    DEV_ASSERT(pwm);
    DEV_ASSERT(pwm->bus_v > 0.0f);
    DEV_ASSERT(pwm->load_ohms > 0.0f);

    float duty_cycle_percent;
    status_t status = STATUS_ERROR;
    uint32_t i;
    float cal_value = 0.0f;
    float x;
    float xn;

    if (isfinite(i_mA))
    {
        /* Coerce the current to the max if beyond the max load voltage */
        if (((i_mA / 1000.0f) * pwm->load_ohms) > pwm->max_load_v)
        {
            i_mA = (pwm->max_load_v / pwm->load_ohms) * 1000.0f;
        }
        else if (i_mA < 0.0f)
        {
            i_mA = 0.0f;
        }

        /* Calibrate the ADC value: C0 + C1x + C2x^2 + ... */
        x = i_mA;
        xn = i_mA;
        cal_value = (*pwm->cal_poly_cn)[0];
        for (i = 1UL; i < PWM_LOAD_CAL_POLY_SIZE; ++i)
        {
            cal_value += (*pwm->cal_poly_cn)[i] * xn;
            xn *= x;
        }

        /* Get a percent of the maximum current */
        duty_cycle_percent = cal_value / ((pwm->bus_v / pwm->load_ohms) * 1000.0f);

        /* Note: This function clamps duty_cycle_percent bounds and checks
         * for finite-ness prior to setting */
        status = PWM_Load_SetDriver(pwm, duty_cycle_percent);
    }

    return status;
}


status_t PWM_Load_SetDriver(Pwm_t *pwm, float duty_cycle_percent)
{
    DEV_ASSERT(pwm);
    DEV_ASSERT(pwm->bus_v > 0.0f);

    status_t status = STATUS_ERROR;
    uint16_t duty_cycle_cnt;

    /* If the value is within the configured scale */
    if(isfinite(duty_cycle_percent))
    {
        if (duty_cycle_percent < 0.0f)
        {
            duty_cycle_percent = 0.0f;
        }
        else if (duty_cycle_percent > 1.0f)
        {
            duty_cycle_percent = 1.0f;
        }

        /* Get duty cycle percentage and then multiply by max FTM duty cycle counts */
        duty_cycle_cnt = (uint16_t)(duty_cycle_percent * ((float)FTM_MAX_DUTY_CYCLE));
        DEV_ASSERT(duty_cycle_cnt <= FTM_MAX_DUTY_CYCLE);
        status = FTM_DRV_UpdatePwmChannel(pwm->instance, pwm->channel,
                                              FTM_PWM_UPDATE_IN_DUTY_CYCLE, duty_cycle_cnt, 0U, true);
        /* Only fails if PWM config is invalid */
        DEV_ASSERT(status == STATUS_SUCCESS);
    }

    return status;
}


status_t PWM_Load_SetDriverRaw(Pwm_t *pwm, uint16_t duty_cycle_cnt)
{
    return FTM_DRV_UpdatePwmChannel(pwm->instance, pwm->channel,
                                        FTM_PWM_UPDATE_IN_DUTY_CYCLE, duty_cycle_cnt, 0U, true);

}
