/* Copyright (c) 2018-2019 by Phase Four, Inc. All Rights Reserved. Confidential and Proprietary.
 *
 * tc58.c
 */

#include "tc58.h"
#include "mcu_types.h"
#include "lpspi_master_driver.h"
#include <string.h>

/* Feature Table macros */

#define FT_A0_ADDR            0xA0U
#define FT_A0_BRWD_MASK       0x80U
#define FT_A0_BRWD_SHIFT      7U
#define FT_A0_BRWD(x)         ((x & FT_A0_BRWD_MASK) >> FT_A0_BRWD_SHIFT)
#define FT_A0_BL_MASK         0x38U
#define FT_A0_BL_SHIFT        3U
#define FT_A0_BL(x)           ((x & FT_A0_BL_MASK)   >> FT_A0_BL_SHIFT)

#define FT_C0_ADDR            0xC0U
#define FT_C0_ECCS_MASK       0x30U
#define FT_C0_ECCS_SHIFT      4U
#define FT_C0_ECCS(x)         ((x & FT_C0_ECCS_MASK) >> FT_C0_ECCS_SHIFT)
#define FT_C0_PRGF_MASK       0x08U
#define FT_C0_PRGF_SHIFT      3U
#define FT_C0_PRGF(x)         ((x & FT_C0_PRGF_MASK) >> FT_C0_PRGF_SHIFT)
#define FT_C0_ERSF_MASK       0x04U
#define FT_C0_ERSF_SHIFT      2U
#define FT_C0_ERSF(x)         ((x & FT_C0_ERSF_MASK) >> FT_C0_ERSF_SHIFT)
#define FT_C0_WEL_MASK        0x02U
#define FT_C0_WEL_SHIFT       1U
#define FT_C0_WEL(x)          ((x & FT_C0_WEL_MASK)  >> FT_C0_WEL_SHIFT)
#define FT_C0_OIP_MASK        0x01U
#define FT_C0_OIP_SHIFT       0U
#define FT_C0_OIP(x)          ((x & FT_C0_OIP_MASK)  >> FT_C0_OIP_SHIFT)

#define FT_40_ADDR            0x40U
#define FT_40_BFR_SECT0_MASK  0x0FU
#define FT_40_BFR_SECT0_SHIFT 0U
#define FT_40_BFR_SECT0(x)    ((x & FT_40_BFR_SECT0_MASK) >> FT_40_BFR_SECT0_SHIFT)
#define FT_40_BFR_SECT1_MASK  0xF0U
#define FT_40_BFR_SECT1_SHIFT 4U
#define FT_40_BFR_SECT1(x)    ((x & FT_40_BFR_SECT1_MASK) >> FT_40_BFR_SECT1_SHIFT)

#define FT_50_ADDR            0x50U
#define FT_50_BFR_SECT2_MASK  0x0FU
#define FT_50_BFR_SECT2_SHIFT 0U
#define FT_50_BFR_SECT2(x)    ((x & FT_50_BFR_SECT2_MASK) >> FT_50_BFR_SECT2_SHIFT)
#define FT_50_BFR_SECT3_MASK  0xF0U
#define FT_50_BFR_SECT3_SHIFT 4U
#define FT_50_BFR_SECT3(x)    ((x & FT_50_BFR_SECT3_MASK) >> FT_50_BFR_SECT3_SHIFT)


static uint8_t flash_pg_tx_buf[TC58_MAX_XFER] = { 0U };
static uint8_t flash_pg_rx_buf[TC58_MAX_XFER] = { 0U };


static status_t TC58_Flash_GetFeature(uint32_t instance, uint8_t cs, uint8_t feat_addr, uint8_t *feat_value);
static status_t TC58_Flash_SetFeature(uint32_t instance, uint8_t cs, uint8_t feat_addr, uint8_t feat_value);
static bool TC58_Flash_IsBusy(uint32_t instance, uint8_t cs);
static bool TC58_Flash_IsWriteEnabled(uint32_t instance, uint8_t cs);


status_t TC58_Flash_Init(uint32_t instance, uint8_t cs)
{
    enum Constants { EXP_DEVICE_ID = 0x98C2U };

    status_t status = STATUS_ERROR;

    /* Check Device ID, Reset regs and ops, unlock all blocks */
    if ((EXP_DEVICE_ID  == TC58_Flash_ReadID(instance, cs)) &&
        (STATUS_SUCCESS == TC58_Flash_Reset(instance, cs))  &&
        (STATUS_SUCCESS == TC58_Flash_SetFeature(instance, cs, FT_A0_ADDR, 0U)))
    {
        status = STATUS_SUCCESS;
    }

    return status;
}


/* Only 4 loads max to any given page is permitted and writes must occur in 512B multiples.
 *
 * Note: Unfortunately, separate writes to the internal buffer prior to loading the cell
 * doesn't work.  Boo! Only the last write is retained and written to the cell.  It does
 * mean, however, that the Nand Flash device only loads the values written in the last
 * transfer and does not touch other data. It also appears that, despite the requirement
 * that pages must be written lowest to highest (page 0 within a block to page 63), any
 * partial programs within a page could occur out of order.  For example, the last 512B
 * of a page could be programmed first.  What is unclear is how the spare area behaves
 * given this new behavior.  My best guess is that the spare area is written as part of
 * a partial program, writing 528B (512B main + 16B spare) to the internal buffer
 * sequentially and that the device will write the spare to the appropriate area.
 * Currently not using spare but documented here just in case. */
status_t TC58_Flash_LoadPage(uint32_t instance, uint8_t cs, uint32_t addr, uint16_t num_of_bytes, uint8_t *write_buf)
{
    DEV_ASSERT(write_buf);

    status_t status = STATUS_ERROR;
    uint16_t addr_col = TC58_FLASH_COL(addr);
    uint32_t addr_phy = TC58_2ADDR(addr);

    /* Only 512B sections in the main (no spare) can be written,
     * the column address must fall on a 512B boundary,
     * check for valid addressing, and for any address overflow */
    if (((num_of_bytes % TC58_MIN_PAGE_SIZE)    == 0U)                        &&
        ((addr_col & (TC58_MIN_PAGE_SIZE - 1U)) == 0U)                        &&
        ((addr_col + num_of_bytes)              <=  (uint16_t)TC58_PAGE_SIZE) &&
        ((addr_phy + (uint32_t)num_of_bytes)    <=  TC58_MEMORY_SIZE)         &&
        ((addr_phy                              <   TC58_MEMORY_SIZE)))
    {
        /* Ensure no other operations are already underway, retain returned status */
        status = TC58_Flash_BusyWait(instance, cs, 1UL);

        if ((STATUS_SUCCESS == status) &&
            (STATUS_SUCCESS == TC58_Flash_SetWEL(instance, cs, true))       &&
            (STATUS_SUCCESS == TC58_Flash_WELEnableWait(instance, cs, 2UL)) &&
            (STATUS_SUCCESS == TC58_Flash_SetInternalBuf(instance, cs, addr_col, num_of_bytes, write_buf)))
        {
            /* Once edits to the internal buffer are complete, program the page to cell memory */
            status = TC58_Flash_LoadCell(instance, cs, addr);
        }

    }

    return status;
}


status_t TC58_Flash_BlockErase(uint32_t instance, uint8_t cs, uint32_t addr)
{
    enum Constants { XFER_SIZE = 4U };

    status_t status = STATUS_ERROR;
    uint8_t tx_buf[XFER_SIZE] = {0U};
    uint8_t rx_buf[XFER_SIZE] = {0U};
    uint16_t addr_row = TC58_FLASH_ROW(TC58_2ADDR(addr));

    if (TC58_2ADDR(addr) < TC58_MEMORY_SIZE)
    {
        /* Set the Chip Select */
        (void)LPSPI_DRV_SetPcs(instance, cs, LPSPI_ACTIVE_LOW);

        /* Read Cell Array to pull page into intermediate buffer */
        tx_buf[0] = TC58_CMD_BLK_ERASE;
        tx_buf[1] = 0U;
        tx_buf[2] = (uint8_t)((addr_row >> 8U) & 0xFFU);
        tx_buf[3] = (uint8_t)((addr_row)       & 0xFFU);

        /* Ensure no other operations are already underway, retain returned status */
        status = TC58_Flash_BusyWait(instance, cs, 1UL);

        if ((STATUS_SUCCESS == status) &&
            (STATUS_SUCCESS == TC58_Flash_SetWEL(instance, cs, true)) &&
            (STATUS_SUCCESS == TC58_Flash_WELEnableWait(instance, cs, 2UL)))
        {
            status = LPSPI_DRV_MasterTransferBlocking(instance, &tx_buf[0], &rx_buf[0], XFER_SIZE, TC58_XFER_TIMEOUT_MS);

        }
    }

    /* Note: Since it takes 500us to erase a block, the ERS_F is not checked
     * here in order to not block. Checking is up to the user */

    return status;
}


void TC58_Flash_AbortTransfer(uint32_t instance)
{
    (void)LPSPI_DRV_MasterAbortTransfer(instance);
}



uint16_t TC58_Flash_ReadID(uint32_t instance, uint8_t cs)
{
    enum
    {
        /* CMD ID byte, dummy byte, 2 device ID bytes */
        XFER_SIZE = 4U,
        ID_HI_RX_BYTE = 2U,
        ID_LO_RX_BYTE = 3U
    };

    status_t status = STATUS_ERROR;
    uint16_t device_id = 0U;
    uint8_t tx_buf[XFER_SIZE] = {0U};
    uint8_t rx_buf[XFER_SIZE] = {0U};

    /* Set the command byte */
    tx_buf[0] = TC58_CMD_READ_ID;

    (void)LPSPI_DRV_SetPcs(instance, cs, LPSPI_ACTIVE_LOW);

    status = LPSPI_DRV_MasterTransferBlocking(instance, &tx_buf[0], &rx_buf[0], XFER_SIZE, TC58_XFER_TIMEOUT_MS);
    if (status == STATUS_SUCCESS)
    {
        device_id = ((uint16_t)rx_buf[ID_HI_RX_BYTE] << 8U) + (uint16_t)rx_buf[ID_LO_RX_BYTE];
    }

    return device_id;
}


/* Reads/loads the specified page into the internal buffer */
status_t TC58_Flash_ReadCell(uint32_t instance, uint8_t cs, uint32_t addr)
{
    enum Constants { XFER_SIZE = 4, PG_RX_DATA_START = 4 };

    status_t status = STATUS_ERROR;
    uint8_t tx_buf[XFER_SIZE] = {0U};
    uint8_t rx_buf[XFER_SIZE] = {0U};
    uint16_t addr_row = TC58_FLASH_ROW(TC58_2ADDR(addr));

    /* Set the Chip Select */
    (void)LPSPI_DRV_SetPcs(instance, cs, LPSPI_ACTIVE_LOW);

    /* Read Cell Array to pull page into intermediate buffer */
    tx_buf[0] = TC58_CMD_READ_CELL;
    tx_buf[1] = 0U;  /* Dummy byte */
    tx_buf[2] = (uint8_t)((addr_row >> 8U) & 0xFFU);
    tx_buf[3] = (uint8_t)((addr_row)       & 0xFFU);

    /* Ensure no other operations are already underway, retain returned status */
    status = TC58_Flash_BusyWait(instance, cs, 1UL);
    if (status == STATUS_SUCCESS)
    {
        status = LPSPI_DRV_MasterTransferBlocking(instance, &tx_buf[0], &rx_buf[0], XFER_SIZE, TC58_XFER_TIMEOUT_MS);
    }

    return status;
}


/* Reads/loads the specified page into the internal buffer */
status_t TC58_Flash_LoadCell(uint32_t instance, uint8_t cs, uint32_t addr)
{
    enum Constants { XFER_SIZE = 4 };

    status_t status = STATUS_ERROR;
    uint8_t tx_buf[XFER_SIZE] = {0U};
    uint8_t rx_buf[XFER_SIZE] = {0U};
    uint16_t addr_row = TC58_FLASH_ROW(TC58_2ADDR(addr));

    if (TC58_2ADDR(addr) < TC58_MEMORY_SIZE)
    {
        /* Set the Chip Select */
        (void)LPSPI_DRV_SetPcs(instance, cs, LPSPI_ACTIVE_LOW);

        /* Program Execute to write page from the intermediate buffer to the flash cell */
        tx_buf[0] = TC58_CMD_PROG_EXE;
        tx_buf[1] = 0U;  /* Dummy byte */
        tx_buf[2] = (uint8_t)((addr_row >> 8U) & 0xFFU);
        tx_buf[3] = (uint8_t)((addr_row)       & 0xFFU);

        /* Ensure no other operations are already underway, retain returned status */
        status = TC58_Flash_BusyWait(instance, cs, 1UL);
        if (status == STATUS_SUCCESS)
        {
            status = LPSPI_DRV_MasterTransferBlocking(instance, &tx_buf[0], &rx_buf[0], XFER_SIZE, TC58_XFER_TIMEOUT_MS);
        }
    }

    return status;
}


/* Reads up to 512 bytes from a page that has already been read into the internal buffer.
 * To read a full page of the internal buffer, multiple calls to this function must be made.
 * SPI can only handle 2048KB transfers max, which cannot handle a full page transfer.
 * Therefore, split up transfers into smaller chunks to work around this restriction.
 * NOTE: Assumes that device is not busy. */
status_t TC58_Flash_GetInternalBuf(uint32_t instance, uint8_t cs, uint16_t addr_col, uint16_t num_of_bytes, uint8_t **read_buf)
{
    DEV_ASSERT(read_buf);

    enum Constants { PG_RX_DATA_START = 4 };

    status_t status = STATUS_ERROR;
    uint16_t pg_xfer_size = PG_RX_DATA_START + num_of_bytes;

    if (num_of_bytes <= TC58_MIN_PAGE_SIZE)
    {
        /* Set the Chip Select */
        (void)LPSPI_DRV_SetPcs(instance, cs, LPSPI_ACTIVE_LOW);

        /* Prepare the transmit buffer */
        memset(&flash_pg_tx_buf[0], 0, sizeof(flash_pg_tx_buf));
        flash_pg_tx_buf[0] = TC58_CMD_READ_PAGE;
        flash_pg_tx_buf[1] = (uint8_t)((addr_col >> 8U) & 0x0FU);  /* 12-bit column address */
        flash_pg_tx_buf[2] = (uint8_t)((addr_col)       & 0xFFU);
        flash_pg_tx_buf[3] = 0U;                                   /* Dummy byte */

        /* Clear the receive buffer */
        memset(&flash_pg_rx_buf[0], 0, sizeof(flash_pg_rx_buf));

        status = LPSPI_DRV_MasterTransferBlocking(instance, &flash_pg_tx_buf[0], &flash_pg_rx_buf[0], pg_xfer_size, TC58_XFER_TIMEOUT_MS);
    }

    *read_buf = &flash_pg_rx_buf[PG_RX_DATA_START];

    return status;
}


/* Reads up to 512 bytes from a page that has already been read into the internal buffer.
 * To read a full page of the internal buffer, multiple calls to this function must be made.
 * SPI can only handle 2048KB transfers max, which cannot handle a full page transfer.
 * Therefore, split up transfers into smaller chunks to work around this restriction.
 * NOTE: Assumes that device is not busy. */
status_t TC58_Flash_GetInternalBuf_NonBlocking(uint32_t instance, uint8_t cs, uint16_t addr_col, uint16_t num_of_bytes, uint8_t **read_buf)
{
    DEV_ASSERT(read_buf);

    enum Constants { PG_RX_DATA_START = 4 };

    status_t status = STATUS_ERROR;
    uint16_t pg_xfer_size = PG_RX_DATA_START + num_of_bytes;

    if (num_of_bytes <= TC58_MIN_PAGE_SIZE)
    {
        /* Set the Chip Select */
        (void)LPSPI_DRV_SetPcs(instance, cs, LPSPI_ACTIVE_LOW);

        /* Prepare the transmit buffer */
        memset(&flash_pg_tx_buf[0], 0, sizeof(flash_pg_tx_buf));
        flash_pg_tx_buf[0] = TC58_CMD_READ_PAGE;
        flash_pg_tx_buf[1] = (uint8_t)((addr_col >> 8U) & 0x0FU);  /* 12-bit column address */
        flash_pg_tx_buf[2] = (uint8_t)((addr_col)       & 0xFFU);
        flash_pg_tx_buf[3] = 0U;                                   /* Dummy byte */

        /* Clear the receive buffer */
        memset(&flash_pg_rx_buf[0], 0, sizeof(flash_pg_rx_buf));

        status = LPSPI_DRV_MasterTransfer(instance, &flash_pg_tx_buf[0], &flash_pg_rx_buf[0], pg_xfer_size);
    }

    *read_buf = &flash_pg_rx_buf[PG_RX_DATA_START];

    return status;
}


/* Writes up to 2045 (2048-3) bytes of data to the device's internal buffer.
 * To write a full page of the internal buffer, multiple calls to this function must be made.
 * SPI can only handle 2048KB transfers max, which cannot handle a full page transfer.
 * Therefore, split up transfers into smaller chunks to work around this restriction.
 * NOTE: Assumes that device is not busy. */
status_t TC58_Flash_SetInternalBuf(uint32_t instance, uint8_t cs, uint16_t addr_col, uint16_t num_of_bytes, uint8_t *write_buf)
{
    DEV_ASSERT(write_buf);

    enum Constants { PG_TX_DATA_START = 3U, MAX_SPI_XFER_SIZE = (TC58_PAGE_SIZE - 3UL) };

    status_t status = STATUS_ERROR;
    uint16_t pg_xfer_size = PG_TX_DATA_START + num_of_bytes;

    if (num_of_bytes <= MAX_SPI_XFER_SIZE)
    {
        /* Set the Chip Select */
        (void)LPSPI_DRV_SetPcs(instance, cs, LPSPI_ACTIVE_LOW);

        /* Prepare the page transmit buffer with all 1's (0xFF) */
        memset(&flash_pg_tx_buf[0], -1, sizeof(flash_pg_tx_buf));
        flash_pg_tx_buf[0] = TC58_CMD_PROG_LOAD;
        flash_pg_tx_buf[1] = (uint8_t)((addr_col >> 8U) & 0x0FU);  /* 12-bit col addr */
        flash_pg_tx_buf[2] = (uint8_t)((addr_col)       & 0xFFU);

        /* Copy write data into the transmit buffer */
        memcpy(&flash_pg_tx_buf[PG_TX_DATA_START], &write_buf[0], num_of_bytes);

        /* No need to clear flash_pg_rx_buf since it will be filled with junk */

        status = LPSPI_DRV_MasterTransferBlocking(instance, &flash_pg_tx_buf[0], &flash_pg_rx_buf[0], pg_xfer_size, TC58_XFER_TIMEOUT_MS);
    }

    return status;
}


status_t TC58_Flash_SetInternalBuf_NonBlocking(uint32_t instance, uint8_t cs, uint16_t addr_col, uint16_t num_of_bytes, uint8_t *write_buf)
{
    DEV_ASSERT(write_buf);

    enum Constants { PG_TX_DATA_START = 3U, MAX_SPI_XFER_SIZE = (TC58_PAGE_SIZE - 3UL) };

    status_t status = STATUS_ERROR;
    uint16_t pg_xfer_size = PG_TX_DATA_START + num_of_bytes;

    if (num_of_bytes <= MAX_SPI_XFER_SIZE)
    {
        /* Set the Chip Select */
        (void)LPSPI_DRV_SetPcs(instance, cs, LPSPI_ACTIVE_LOW);

        /* Prepare the page transmit buffer with all 1's (0xFF) */
        memset(&flash_pg_tx_buf[0], -1, sizeof(flash_pg_tx_buf));
        flash_pg_tx_buf[0] = TC58_CMD_PROG_LOAD;
        flash_pg_tx_buf[1] = (uint8_t)((addr_col >> 8U) & 0x0FU);  /* 12-bit col addr */
        flash_pg_tx_buf[2] = (uint8_t)((addr_col)       & 0xFFU);

        /* Copy write data into the transmit buffer */
        memcpy(&flash_pg_tx_buf[PG_TX_DATA_START], &write_buf[0], num_of_bytes);

        /* No need to clear flash_pg_rx_buf since it will be filled with junk */

        status = LPSPI_DRV_MasterTransfer(instance, &flash_pg_tx_buf[0], &flash_pg_rx_buf[0], pg_xfer_size);
    }

    return status;
}


status_t TC58_Flash_SetWEL(uint32_t instance, uint8_t cs, bool enable)
{
    /* Note: Only sending 1 byte however SPI driver requires more to send properly with blocking?
     * I believe this is related to a race condition such that the SPI xfer Done does not get called.
     * I believe this is exacerbated by increasing the SCLK. No time to properly track this down
     * so for now increasing the xfer size will have to do and keeping the SPI SCLK at a low freq,
     * but it seems there might be something in the dspi_driver. */
    enum Constants { XFER_SIZE = 4U };

    status_t status = STATUS_ERROR;
    uint8_t tx_buf[XFER_SIZE] = {0U};
    uint8_t rx_buf[XFER_SIZE] = {0U};

    /* Set the Chip Select */
    (void)LPSPI_DRV_SetPcs(instance, cs, LPSPI_ACTIVE_LOW);

    /* Check the feature table for the OIP feature at address 0xC0 */
    tx_buf[0] = (enable) ? TC58_CMD_WRITE_EN : TC58_CMD_WRITE_DS;
    status = LPSPI_DRV_MasterTransferBlocking(instance, &tx_buf[0], &rx_buf[0], XFER_SIZE, TC58_XFER_TIMEOUT_MS);

    return status;
}


status_t TC58_Flash_Reset(uint32_t instance, uint8_t cs)
{
    /* Note: Only sending 1 byte however SPI driver requires more to send properly with blocking.
     * I believe this is related to a race condition such that the SPI xfer Done does not get called.
     * I believe this is exacerbated by increasing the SCLK. No time to properly track this down
     * so for now increasing the xfer size will have to do and keeping the SPI SCLK at a low freq,
     * but it seems there might be something in the dspi_driver. */
    enum Constants { XFER_SIZE = 4U };

    status_t status = STATUS_ERROR;
    uint8_t tx_buf[XFER_SIZE] = {0U};
    uint8_t rx_buf[XFER_SIZE] = {0U};

    /* Set the Chip Select */
    (void)LPSPI_DRV_SetPcs(instance, cs, LPSPI_ACTIVE_LOW);

    /* Check the feature table for the OIP feature at address 0xC0 */
    tx_buf[0] = TC58_CMD_RESET;
    status = LPSPI_DRV_MasterTransferBlocking(instance, &tx_buf[0], &rx_buf[0], XFER_SIZE, TC58_XFER_TIMEOUT_MS);

    return status;
}


status_t TC58_Flash_BusyWait(uint32_t instance, uint8_t cs, uint32_t cnt)
{
    DEV_ASSERT(cnt > 0UL);

    status_t status = STATUS_BUSY;

    while (TC58_Flash_IsBusy(instance, cs) && (--cnt > 0UL));

    if (cnt > 0UL)
    {
        status = STATUS_SUCCESS;
    }

    return status;
}


status_t TC58_Flash_WELEnableWait(uint32_t instance, uint8_t cs, uint32_t cnt)
{
    DEV_ASSERT(cnt > 0UL);

    status_t status = STATUS_TIMEOUT;

    while ((!TC58_Flash_IsWriteEnabled(instance, cs)) && (--cnt > 0UL));

    if (cnt > 0UL)
    {
        status = STATUS_SUCCESS;
    }

    return status;
}


status_t TC58_Flash_LastProgStatus(uint32_t instance, uint8_t cs)
{
    uint8_t feature_value = 0xFFU;
    status_t status = TC58_Flash_GetFeature(instance, cs, FT_C0_ADDR, &feature_value);

    if (status == STATUS_SUCCESS)
    {
        if (0x00U == (feature_value & FT_C0_PRGF_MASK))
        {
            status = STATUS_SUCCESS;
        }
        else
        {
            status = STATUS_ERROR;
        }
    }

    return status;
}


status_t TC58_Flash_LastEraseStatus(uint32_t instance, uint8_t cs)
{
    uint8_t feature_value = 0xFFU;
    status_t status = TC58_Flash_GetFeature(instance, cs, FT_C0_ADDR, &feature_value);

    if (status == STATUS_SUCCESS)
    {
        if (0x00U == (feature_value & FT_C0_ERSF_MASK))
        {
            status = STATUS_SUCCESS;
        }
        else
        {
            status = STATUS_ERROR;
        }
    }

    return status;
}


/* Appends the total number of bit flips detected in the last page read and appends
 * it to the page_ecc_counts value */
status_t TC58_Flash_GetPageEcc(uint32_t instance, uint8_t cs, uint32_t *corr_counts, uint32_t *uncorr_counts)
{
    enum Constants
    {
        ECC_NONE = 0U,
        ECC_CORRECTED,
        ECC_UNCORRECTED,
        ECC_CORRECTED_THRESH
    };

    uint8_t ecc_val = 0U;
    uint8_t feature_value_c0 = 0U;
    uint8_t feature_value_40 = 0U;
    uint8_t feature_value_50 = 0U;
    status_t status = STATUS_ERROR;

    if ((STATUS_SUCCESS == TC58_Flash_GetFeature(instance, cs, FT_C0_ADDR, &feature_value_c0)) &&
        (STATUS_SUCCESS == TC58_Flash_GetFeature(instance, cs, FT_40_ADDR, &feature_value_40)) &&
        (STATUS_SUCCESS == TC58_Flash_GetFeature(instance, cs, FT_50_ADDR, &feature_value_50)))
    {
        status = STATUS_SUCCESS;

        ecc_val = FT_C0_ECCS(feature_value_c0);
        switch (ecc_val)
        {
            case ECC_CORRECTED:
            case ECC_CORRECTED_THRESH:
                *corr_counts += (uint32_t)(FT_40_BFR_SECT0(feature_value_40) + FT_40_BFR_SECT1(feature_value_40));
                *corr_counts += (uint32_t)(FT_50_BFR_SECT2(feature_value_50) + FT_50_BFR_SECT3(feature_value_50));
                break;

            case ECC_UNCORRECTED:
                *uncorr_counts += (uint32_t)(FT_40_BFR_SECT0(feature_value_40) + FT_40_BFR_SECT1(feature_value_40));
                *uncorr_counts += (uint32_t)(FT_50_BFR_SECT2(feature_value_50) + FT_50_BFR_SECT3(feature_value_50));
                break;

            case ECC_NONE:
            default:
                break;
        }
    }

    return status;
}

static status_t TC58_Flash_GetFeature(uint32_t instance, uint8_t cs, uint8_t feat_addr, uint8_t *feat_value)
{
    DEV_ASSERT(feat_value);

    enum Constants
    {
        XFER_SIZE   = 4U,
        FEAT_RX_IDX = 2U,
    };

    status_t status = STATUS_ERROR;
    uint8_t tx_buf[XFER_SIZE] = {0U};
    uint8_t rx_buf[XFER_SIZE] = {0U};

    /* Set the Chip Select */
    (void)LPSPI_DRV_SetPcs(instance, cs, LPSPI_ACTIVE_LOW);

    /* Check the feature table for the OIP feature at address 0xC0 */
    tx_buf[0] = TC58_CMD_GET_FEAT;
    tx_buf[1] = feat_addr;
    status = LPSPI_DRV_MasterTransferBlocking(instance, &tx_buf[0], &rx_buf[0], XFER_SIZE, TC58_XFER_TIMEOUT_MS);

    /* Check the Flash chip Operation In Progress field */
    if (status == STATUS_SUCCESS)
    {
        *feat_value = rx_buf[FEAT_RX_IDX];
    }

    return status;

}


static status_t TC58_Flash_SetFeature(uint32_t instance, uint8_t cs, uint8_t feat_addr, uint8_t feat_value)
{
    enum Constants
    {
        XFER_SIZE   = 4U,
        FEAT_RX_IDX = 2U,
    };

    status_t status = STATUS_ERROR;
    uint8_t tx_buf[XFER_SIZE] = {0U};
    uint8_t rx_buf[XFER_SIZE] = {0U};

    /* Set the Chip Select */
    (void)LPSPI_DRV_SetPcs(instance, cs, LPSPI_ACTIVE_LOW);

    /* Check the feature table for the OIP feature at address 0xC0 */
    tx_buf[0] = TC58_CMD_SET_FEAT;
    tx_buf[1] = feat_addr;
    tx_buf[2] = feat_value;
    status = LPSPI_DRV_MasterTransferBlocking(instance, &tx_buf[0], &rx_buf[0], XFER_SIZE, TC58_XFER_TIMEOUT_MS);

    return status;

}


static bool TC58_Flash_IsBusy(uint32_t instance, uint8_t cs)
{
    bool is_busy = true;
    uint8_t feature_value = 0U;
    status_t status = STATUS_BUSY;

    status = TC58_Flash_GetFeature(instance, cs, FT_C0_ADDR, &feature_value);

    if ((status == STATUS_SUCCESS) &&
        (0x00U == FT_C0_OIP(feature_value)))
    {
        is_busy = false;
    }

    return is_busy;
}


static bool TC58_Flash_IsWriteEnabled(uint32_t instance, uint8_t cs)
{
    bool is_wel_set = true;
    uint8_t feature_value = 0U;
    status_t status = STATUS_ERROR;

    status = TC58_Flash_GetFeature(instance, cs, FT_C0_ADDR, &feature_value);

    if ((status == STATUS_SUCCESS) &&
        (0x00U == FT_C0_WEL(feature_value)))
    {
        is_wel_set = false;
    }

    return is_wel_set;
}

