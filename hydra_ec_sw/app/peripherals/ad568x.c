/* Copyright (c) 2017-2020 by Phase Four, Inc. All Rights Reserved. Confidential and Proprietary.
 *
 * ad568x.c
 */

#include "ad568x.h"
#include "mcu_types.h"
#include "flexio.h"
#include "flexio_spi_driver.h"
#include <math.h>


void AD568x_Init(AD568x_t *dac, uint32_t instance, const flexio_spi_master_user_config_t * user_config,
                             float max_value, const float cal_poly_cn[AD568X_CAL_POLY_SIZE])
{
    DEV_ASSERT(dac);
    DEV_ASSERT(user_config);
    DEV_ASSERT(isfinite(max_value));
    DEV_ASSERT(max_value > 0.0f);
    DEV_ASSERT(cal_poly_cn);
    DEV_ASSERT(((uint32_t)cal_poly_cn & 0x3UL) == 0x0UL);  /* Validate 4-byte alignment for FP */

    dac->instance = instance;
    dac->max_value = max_value;
    dac->cal_poly_cn = &cal_poly_cn[0];

    /* Always returns STATUS_SUCCESS */
    (void)FLEXIO_DRV_InitDevice(instance, &dac->flexio_state);
    (void)FLEXIO_SPI_DRV_MasterInit(instance, user_config, &dac->flexio_master);

    return;
}


status_t AD568x_SetControlReg_Blocking(AD568x_t *dac, uint32_t reg_value)
{
    DEV_ASSERT(dac);
    DEV_ASSERT(dac->max_value > 0.0f);

    status_t status;

    /* Only bits 19-14 are applicable, all others should be masked to zero
     * which the data sheet says is necessary for writing the register. */
    enum Constant { CTRL_REG_MASK = 0xFC000 };

    status = AD568x_XferRaw_Blocking(dac, AD568X_WCR, reg_value & CTRL_REG_MASK);

    return status;
}


status_t AD568x_SetVal_Blocking(AD568x_t *dac, float val)
{
    DEV_ASSERT(dac);
    DEV_ASSERT(dac->max_value > 0.0f);
    DEV_ASSERT(isfinite(val));

    enum Constants { MAX_U16_VALUE = 0xFFFF };

    status_t status;
    uint32_t i;
    float x;
    float xn;
    float cal_value;

    if (val < 0.0f)
    {
        x = 0.0f;
        xn = 0.0f;
    }
    else if (val > dac->max_value)
    {
        x = dac->max_value;
        xn = dac->max_value;
    }
    else
    {
        x = val;
        xn = val;
    }

    /* Calibrate the ADC value: C0 + C1x + C2x^2 + ... */
    cal_value = dac->cal_poly_cn[0];
    for (i = 1UL; i < AD568X_CAL_POLY_SIZE; ++i)
    {
        cal_value += dac->cal_poly_cn[i] * xn;
        xn *= x;
    }

    /* Calibration may place this value outside of the expected
     * range, in which case converting to an u16 may have some
     * issues (e.g. -6.0f). Safest to bound this value. */
    if (cal_value < 0.0f)
    {
        cal_value = 0.0f;
    }
    else if (cal_value > (float)MAX_U16_VALUE)
    {
        cal_value = (float)MAX_U16_VALUE;
    }

    status  = AD568x_SetControlReg_Blocking(dac, AD568X_WCR_GAIN_BIT);
    status |= AD568x_XferRaw_Blocking(dac, AD568X_WDI, (uint16_t)cal_value);

    if (status != STATUS_SUCCESS)
    {
        status = STATUS_ERROR;
    }

    return status;
}


status_t AD568x_XferRaw_Blocking(AD568x_t *dac, AD568x_Cmds_t cmd, uint32_t code)
{
    DEV_ASSERT(dac);
    DEV_ASSERT(dac->max_value > 0.0f);

    enum Constants {
        CMD_DATA_MASK = 0xFFFFF  /* 20 bits of data */
    };

    status_t status;
    uint32_t xfer_tx = 0UL;
    uint32_t xfer_rx;        /* unused, there is no SDO on this chip */

    /* Shift the code data by the number of bits required by the specified command */
    uint8_t data_shift = (uint8_t)(cmd & AD568X_CMD_FMT_BITS_MASK);
    uint32_t data = code << data_shift;

    /* Construct the uint32_t to transfer by placing the command in bits 31:28 (4 bits)
     * and the data in bits 27:8 (20 bits).
     * Note: Shifting in 0s means any extra data transmitted will transmit 0s as well. */
    xfer_tx = (((uint32_t)cmd & AD568X_CMD_C_BITS_MASK) << 24U) | ((data & CMD_DATA_MASK) << 8U);

    /* Convert uint32_t to a byte array when passing in such that the first byte to
     * transfer (and MSB of xfer_tx) is in uint8_t[3] and the last byte to transfer
     * is in uint8_t[0].  This occurs due to the nature of a Little Endian system.
y      * The FlexIO is configured to handle the rest. */
    status = FLEXIO_SPI_DRV_MasterTransferBlocking(&dac->flexio_master,
                                                   (uint8_t *)&xfer_tx,
                                                   (uint8_t *)&xfer_rx,
                                                   sizeof(uint32_t),
                                                   5UL);
    return status;
}


void AD568x_XferAbort(AD568x_t *dac)
{
    DEV_ASSERT(dac);
    DEV_ASSERT(dac->max_value > 0.0f);

    status_t status;

    /* Always returns success */
    status = FLEXIO_SPI_DRV_MasterTransferAbort(&dac->flexio_master);
    DEV_ASSERT(status == STATUS_SUCCESS);

    /* Avoid unused parameter compiler warning */
    (void)status;

    return;
}
