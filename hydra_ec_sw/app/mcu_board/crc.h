/* Copyright (c) 2017-2020 by Phase Four, Inc. All Rights Reserved. Confidential and Proprietary.
 *
 * crc.h
 */

#ifndef MCU_BOARD_CRC_H_
#define MCU_BOARD_CRC_H_

/*!
 * Global invariants:
 *  - The CRC16 algorithm is set to the CRC16-CCITT/FALSE, note that if switching this to
 *    another algorithm that has functions at the end of computation (e.g. transpose), then
 *    it needs to be properly split up to permit the transpose to only occur upon the last
 *    computation or else it may work for a single bulk computation but not if split up into
 *    smaller computations.
 *  - The CRC16 algorithm does not utilize the 16-bit write feature (could not get to work)
 *
 * Notes:
 *  - The CRC_DATA register does the CRC calculation based off of MSB first (i.e. Big Endian),
 *    however the MCU is Little Endian, therefore the code must use reflectIn and reflectOut
 *    to be false (sets TOT=3) to indicate the conversion to Big Endian.
 */

#include "mcu_types.h"

/*!
 * @brief This function computes the CRC16-CCITT/FALSE for the provided buffer using the
 *        normal polynomial representation. The user is expected to pass @p 0xFFFF as the
 *        CRC as an initial seed value.  The user may also split up the accumulation of
 *        the CRC into multiple sections, ensuring to feed the output of the previous CRC
 *        computation as the seed input for the current one.
 *
 * @pre @p buffer is not NULL
 * @pre @p buffer holds at least size elements
 *
 * @param seed_crc  The starting CRC to use for the accumulation. Set to 0xFFFF if first call.
 * @param buffer    The byte array to operate on
 * @param size      The number of bytes in the buffer
 * @return          The accumulated CRC
 */
uint16_t crc16_compute(uint16_t seed_crc, uint8_t *buffer, uint16_t size);


/*!
 * @brief This function computes the CRC32 for the provided buffer using the
 *        normal polynomial representation. The user is expected to pass @p 0xFFFF as the
 *        CRC as an initial seed value.  If doing a single, bulk calculation, the user is
 *        expected to pass in true for the @p last_compute parameter. The user may also
 *        split up the accumulation of the CRC into multiple sections, ensuring to feed
 *        the output of the previous CRC computation as the seed input for the current one.
 *        The @p last_compute parameter should be false for all computations except the
 *        last, which that yields the final result.
 *
 * @pre @p buffer is not NULL
 * @pre @p buffer holds at least size elements
 *
 * @param seed_crc      The starting CRC to use for the accumulation. Set to 0xFFFF if first call.
 * @param buffer        The byte array to operate on
 * @param size          The number of bytes in the buffer
 * @param last_compute  True if expecting the final CRC result, false otherwise
 * @return              The accumulated CRC
 */
uint32_t crc32_compute(uint32_t seed_crc, uint8_t *buffer, uint32_t size, bool last_compute);

#endif /* MCU_BOARD_CRC_H_ */
