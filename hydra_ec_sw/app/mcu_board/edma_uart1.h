/* Copyright (c) 2017-2020 by Phase Four, Inc. All Rights Reserved. Confidential and Proprietary.
 *
 * edma_uart1.h
 */

#ifndef MCU_BOARD_EDMA_UART1_H_
#define MCU_BOARD_EDMA_UART1_H_

#include "mcu_types.h"
#include "dmaController0.h"

#define EDMA_UART1_RING_SIZE    640U              /* Hold a little more than 2 full Clank packets */
#define EDMA_UART1_RX_CH_NUM    EDMA_CHN3_NUMBER

extern uint8_t edma_uart1_ring_buffer[EDMA_UART1_RING_SIZE];


void EdmaUart1_Init(void);

#endif /* MCU_BOARD_EDMA_UART1_H_ */
