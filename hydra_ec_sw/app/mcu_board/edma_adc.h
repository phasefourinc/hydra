/* Copyright (c) 2017-2020 by Phase Four, Inc. All Rights Reserved. Confidential and Proprietary.
 *
 * edma_adc.h
 */

#ifndef MCU_BOARD_EDMA_ADC_H_
#define MCU_BOARD_EDMA_ADC_H_

#include "mcu_types.h"
#include "edma_driver.h"


/* eDMA ADC configured for SPI2 */

/* edma_adc_rx array contains the read channel values for ADC0 and
 * then ADC1. Due to the nature of ADC reads, the value of each ADC
 * channel is returned 2 commanded reads after the read command was
 * requested. To command all channels, the first 2 command transfers
 * return junk.  It is recommended to use the macros of indexing into
 * this structure for the start of ADCx data. */
#define EDMA_ADC_TX_CMDS_CNT    11
#define EDMA_ADC0_CH0_IDX       2
#define EDMA_ADC1_CH0_IDX       (EDMA_ADC0_CH0_IDX + EDMA_ADC_TX_CMDS_CNT)
uint16_t edma_adc_rx[EDMA_ADC_TX_CMDS_CNT*2];

void EdmaAdc_Init(edma_callback_t edma_tx_callback_ptr, edma_callback_t edma_rx_callback_ptr);
void EdmaAdc_StartTransfer(void);
status_t EdmaAdc_EndTransfer(void);

#endif /* MCU_BOARD_EDMA_ADC_H_ */
