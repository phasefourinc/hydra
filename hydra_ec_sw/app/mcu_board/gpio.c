/* Copyright (c) 2017-2020 by Phase Four, Inc. All Rights Reserved. Confidential and Proprietary.
 *
 * gpio.c
 */

#include "gpio.h"
#include "mcu_types.h"
#include "pins_driver.h"
#include "timer64.h"

typedef struct
{
    GPIO_Type *base_gpio;
    uint8_t    pin;
} GPIO_Table_t;

static const GPIO_Table_t GPIO_OutputTable[] =
{
    { PTE, 2U  },  /* GPIO_WDT_PET */
    { PTE, 5U  },  /* GPIO_WDT_TRIG_nCLR_EN */
    { PTA, 6U  },  /* GPIO_RFT_RST_EN */
    { PTB, 12U },  /* GPIO_HEATER_EN */
    { PTC, 10U },  /* GPIO_HEATER_RST_EN */
    { PTB, 9U  },  /* GPIO_HPISO_EN */
    { PTD, 2U  },  /* GPIO_PFCV_RST_EN */
    { PTE, 6U  },  /* GPIO_LED0 */
    { PTE, 7U  },  /* GPIO_LED1 */
    { PTE, 8U  },  /* GPIO_LED2 */
    { PTE, 9U  },  /* GPIO_LED3 */
};

static const GPIO_Table_t GPIO_InputTable[] =
{
    { PTE, 0U  },  /* GPIO_WDT_TOGGLE_SEL */
    { PTE, 1U  },  /* GPIO_WDT_RESET */
    { PTA, 9U  },  /* GPIO_RFT_FAULT */
    { PTB, 13U },  /* GPIO_HEATER_FAULT */
    { PTB, 11U },  /* GPIO_HPISO_FAULT */
    { PTD, 4U  }   /* GPIO_PFCV_FAULT */
};


/* Sets the LEDs to the value of the lower nibble */
void GPIO_SetLEDs(uint8_t val)
{
    /* LEDs are from pins [9:6], mask out upper nibble */
    PINS_DRV_SetPins  (PTE, ((uint32_t)((val)  & 0xFU)) << 6U);
    PINS_DRV_ClearPins(PTE, ((uint32_t)((~val) & 0xFU)) << 6U);
    return;
}


void GPIO_SetOutput(GPIO_Output_t pin, bool is_high)
{
    if (is_high)
    {
        PINS_DRV_SetPins(GPIO_OutputTable[pin].base_gpio, (1UL << GPIO_OutputTable[pin].pin));
    }
    else
    {
        PINS_DRV_ClearPins(GPIO_OutputTable[pin].base_gpio, (1UL << GPIO_OutputTable[pin].pin));
    }

    return;
}


void GPIO_ToggleOutput(GPIO_Output_t pin)
{
    PINS_DRV_TogglePins(GPIO_OutputTable[pin].base_gpio, (1UL << GPIO_OutputTable[pin].pin));
}


uint8_t GPIO_GetInput(GPIO_Input_t pin)
{
    return (uint8_t)((PINS_DRV_ReadPins(GPIO_InputTable[pin].base_gpio) >> GPIO_InputTable[pin].pin) & 0x1UL);
}


void GPIO_PetWDT(void)
{
    /* Pet the WDT, minimum pulse high for chip is 50ns */
    /* Note: Pet pulse min is 50ns, window for a pet is between 27-818ms */
    GPIO_SetOutput(GPIO_WDT_PET, 1);
    timer32_delay_us(1U);
    GPIO_SetOutput(GPIO_WDT_PET, 0);
    return;
}
