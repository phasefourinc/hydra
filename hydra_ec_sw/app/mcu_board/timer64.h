/* Copyright (c) 2017-2020 by Phase Four, Inc. All Rights Reserved. Confidential and Proprietary.
 *
 * timer64.h
 */

#ifndef TIMER64_CODE_TIMER64_H_
#define TIMER64_CODE_TIMER64_H_

/*!
 * @file
 * @brief
 * This file uses two channels of the Periodic Interval Timer (LPIT) to
 * create a 64-bit timer.  The utility functions are designed to operate
 * on either the least-significant 32-bit portion or the entire 64-bit timer.
 * Most operations do not require the use of the full 64-bit timer, therefore
 * providing the 32-bit option allows for faster processing.
 */

#include "lpit0.h"
#include "lpit_driver.h"
#include "mcu_types.h"

#define TIMER32_MAX_CLK_FREQ   80000000UL
#define TIMER32_MAX_TICK_FREQ  (TIMER32_MAX_CLK_FREQ/2UL)
#define TIMER32_MAX_MSEC       (0xFFFFFFFFUL/(TIMER32_MAX_TICK_FREQ/1000UL))
#define TIMER32_MAX_SECONDS    (TIMER32_MAX_MSEC/1000UL)


extern uint32_t timer_clk_freq;
extern uint32_t pit_inst;

/*!
 * @brief This function initializes the Periodic Interval Timer (LPIT) for use as a
 *        64-bit timer that is sourced from the external oscillator.  The frequency
 *        in which the tick is based off of comes from the clock_config.c.
 *
 */
void timer64_init(void);


/*!
 * @brief This function gets the current timer tick count. The upper and lower halves
 *        of the timer are returned separately.
 *
 * @param[out] tick_upper  Upper 32 bits of the clock tick count
 * @param[out] tick_lower  Lower 32 bits of the clock tick count
 */
void timer64_get_tick(uint32_t *tick_upper, uint32_t *tick_lower);


/*!
 * @brief This function returns the duration in milliseconds between two 64-bit
 *        tick recordings.  Ticks are with reference to the SPLL Clock frequency
 *        divided by 2.
 *
 * @note  This function loses precision for large tick values since
 *        floating point may not be able to accurately represent large
 *        uint32's (e.g. 0xFFFFFFFF). Avoid using for sensitive timings

 * @param start_tick_upper  The first tick value (upper 32 bits)
 * @param start_tick_lower  The first tick value (lower 32 bits)
 * @param stop_tick_upper   The second tick value (upper 32 bits)
 * @param stop_tick_lower   The second tick value (lower 32 bits)
 * @return            The elapsed time in milliseconds
 * */
float timer64_get_duration_ms(uint32_t start_tick_upper,
                              uint32_t start_tick_lower,
                              uint32_t stop_tick_upper,
                              uint32_t stop_tick_lower);


/*!
 * @brief This function returns the elapsed time, in milliseconds, since the last
 *        timer tick-count overflow.
 *
 * @return The elapsed time, in milliseconds, since the last tick count counter
 *         overflow
 */
uint32_t timer64_get_curr_time_ms(void);


/*!
 * @brief This function returns the maximum possible time in milliseconds supported
 *        by the configured 32-bit timer.
 *
 * @note  This function expects timer64_init() to have been called prior to execution.
 *
 * @return The maximum tick value in milliseconds for the configured timer
 */
static inline uint32_t timer32_get_max_time_ms(void)
{
    return (0xFFFFFFFFUL / (timer_clk_freq / 1000UL));
}


/*!
 * @brief This function returns the current 32-bit tick of the configured LPIT0.
 * */
static inline uint32_t timer32_get_tick(void)
{
    return LPIT_DRV_GetCurrentTimerCount(pit_inst, 0U);
}


/*!
 * @brief This function delays for the specified number of ticks.  Ticks are based
 *        off of the SPLL clock frequency divided by 2.  If the PLL is 168MHz, then
 *        the timer32 is 84MHz, then there are 84000 ticks = 1ms.
 *
 * @param ticks  The number of ticks to delay.
 * */
void timer32_delay_ticks(uint32_t ticks);


/*!
 * @brief This function delays for the specified number of microseconds.
 *
 * Preconditions:
 *  - us specifies less than 89sec (at 168MHz PLL)
 *
 * @param ms  The number of microseconds to delay.
 * */
void timer32_delay_us(uint32_t us);


/*!
 * @brief This function delays for the specified number of milliseconds.
 *
 * Preconditions:
 *  - ms specifies less than 89sec (at 168MHz PLL)
 *
 * @param ms  The number of milliseconds to delay.
 * */
void timer32_delay_ms(uint32_t ms);


/*!
 * @brief This function converts timer32 ticks to milliseconds.  This
 *        function is not expected to return the elapsed time of the
 *        timer32, but rather, it simply converts the number of ticks
 *        to a millisecond value based on the SPLL frequency that was
 *        passed into timer32 initialization.
 *
 * @note  This function loses precision for large tick values since
 *        floating point may not be able to accurately represent large
 *        uint32's (e.g. 0xFFFFFFFF). Avoid using for sensitive timings
 *
 * @param ticks  The tick value to convert
 * */
float timer32_ticks_to_ms(uint32_t ticks);


/*!
 * @brief This function returns the duration in ticks between 2 tick
 *        recordings.  Ticks are with reference to the SPLL Clock
 *        frequency divided by 2.
 *
 * @param start_tick  The first tick value
 * @param stop_tick   The second tick value
 * */
static inline uint32_t timer32_get_duration_ticks(uint32_t start_tick, uint32_t stop_tick)
{
    return start_tick - stop_tick;
}


/*!
 * @brief This function returns the duration in milliseconds between 2 tick
 *        recordings.  Ticks are with reference to the SPLL Clock frequency
 *        divided by 2.
 *
 * @note  This function loses precision for large tick values since
 *        floating point may not be able to accurately represent large
 *        uint32's (e.g. 0xFFFFFFFF). Avoid using for sensitive timings

 * @param start_tick  The first tick value
 * @param stop_tick   The second tick value
 * @return            The elapsed time in milliseconds
 * */
float timer32_get_duration_ms(uint32_t start_tick, uint32_t stop_tick);


/*!
 * @brief This function returns the time, in milliseconds, that have elapsed
 *        since the provided start tick.
 *
 * @note  This function cannot handle durations that rollover a 32-bit
 *        counter, which for a PLL of 168MHz (84MHz tick), this function will
 *        only work for times no more than 89 seconds.
 *
 * @note  This function loses precision for large tick values since
 *        floating point may not be able to accurately represent large
 *        uint32's (e.g. 0xFFFFFFFF). Avoid using for sensitive timings
 *
 * @param start_tick  The start tick
 * @return            The elapsed time in milliseconds
 */
float timer32_time_since_ms(uint32_t start_tick);

#endif /* TIMER64_CODE_TIMER64_H_ */
