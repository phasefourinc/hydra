/* Copyright (c) 2017-2020 by Phase Four, Inc. All Rights Reserved. Confidential and Proprietary.
 *
 * gpio.h
 */

#ifndef MCU_BOARD_GPIO_H_
#define MCU_BOARD_GPIO_H_

#include "mcu_types.h"


typedef enum
{
    GPIO_WDT_PET = 0,
    GPIO_WDT_TRIG_nCLR_EN,
    GPIO_RFT_nRST_EN,
    GPIO_HEATER_EN,
    GPIO_HEATER_nRST_EN,
    GPIO_HPISO_EN,
    GPIO_PFCV_DAC_nRST_EN,
    GPIO_LED0,
    GPIO_LED1,
    GPIO_LED2,
    GPIO_LED3
} GPIO_Output_t;

typedef enum
{
    GPIO_WDT_TOGGLE_SEL,
    GPIO_WDT_RESET,
    GPIO_RFT_FAULT,
    GPIO_HEATER_FAULT,
    GPIO_HPISO_FAULT,
    GPIO_PFCV_FAULT
} GPIO_Input_t;


/*!
 * @brief  Set the Debug LEDs to the specified 4-bit value wherein
 *         bit 3 is Debug LED3 and bit 0 is Debug LED0.
 *
 * @param val  The 4-bit value to set all debug LEDs to
 */
void GPIO_SetLEDs(uint8_t val);


/*!
 * @brief  Sets the specified GPIO output pin either high
 *         or low.
 *
 * @param pin      The pin to set
 * @param is_high  True is logic high, false is logic low
 */
void GPIO_SetOutput(GPIO_Output_t pin, bool is_high);


/*!
 * @brief  Toggles the specified GPIO output pin.
 *
 * @param pin  The pin to toggle
 */
void GPIO_ToggleOutput(GPIO_Output_t pin);


/*!
 * @brief  Returns the value of the GPIO input pin
 *
 * @param pin  The pin to read
 * @return     The read value
 */
uint8_t GPIO_GetInput(GPIO_Input_t pin);


/*!
 * @brief  Performes one full strobe of the WDT
 */
void GPIO_PetWDT(void);

#endif /* MCU_BOARD_GPIO_H_ */
