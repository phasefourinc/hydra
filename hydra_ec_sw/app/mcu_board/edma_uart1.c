/* Copyright (c) 2017-2020 by Phase Four, Inc. All Rights Reserved. Confidential and Proprietary.
 *
 * edma_uart1.c
 */

#include "edma_uart1.h"
#include "mcu_types.h"
#include "dmaController0.h"


/* This array structure must be 32-byte aligned */
static uint8_t uart1_stcd_arr[sizeof(edma_software_tcd_t)] SECTION(".uart1_dma_tcd") = {0};

static edma_loop_transfer_config_t uart1_rx_edma_maj_loop_cfg =
{
    .majorLoopIterationCount = (uint32_t)sizeof(edma_uart1_ring_buffer),
    .srcOffsetEnable = false,
    .dstOffsetEnable = false,
    .minorLoopOffset = 0L,
    .minorLoopChnLinkEnable = false,
    .minorLoopChnLinkNumber = 0U,
    .majorLoopChnLinkEnable = false,
    .majorLoopChnLinkNumber = 0U
} ;

static edma_transfer_config_t uart1_rx_edma_tcd =
{
        .srcAddr = (uint32_t)&LPUART1->DATA,         /* Source address is the UART DATA reg */
        .destAddr = (uint32_t)&edma_uart1_ring_buffer[0], /* Destination is the ring buffer */
        .srcTransferSize = EDMA_TRANSFER_SIZE_1B,    /* Perform 1 byte transfer */
        .destTransferSize = EDMA_TRANSFER_SIZE_1B,   /* Perform 1 byte transfer */
        .srcOffset = 0,                              /* Always read from the same DATA address */
        .destOffset = 1,                             /* Write to the next byte in ring buffer */
        .srcLastAddrAdjust = 0L,
        .destLastAddrAdjust = 0L,
        .srcModulo = EDMA_MODULO_OFF,
        .destModulo = EDMA_MODULO_OFF,
        .minorByteTransferCount = 1UL,               /* Perform 1 byte transfer */
        .scatterGatherEnable = true,                 /* Scatter-gather used to auto-grab this same TCD */
        .scatterGatherNextDescAddr = (uint32_t)&uart1_stcd_arr[0],
        .interruptEnable = true,                     /* Upon TCD completion, fire interrupt */
        .loopTransferConfig = &uart1_rx_edma_maj_loop_cfg  /* Grab major loop iter */
};


uint8_t edma_uart1_ring_buffer[EDMA_UART1_RING_SIZE] = {0};


/* Used to initialize or reset the eDMA UART0 channel
 * Expects DMA channel to be stopped */
void EdmaUart1_Init(void)
{
    /* Return eDMA status to Normal prior to transfer in
     * case EDMA_CHN_ERROR was triggered the last transfer */
    dmaController0Chn3_State.status = EDMA_CHN_NORMAL;

    /* Push the configuration for the first descriptor to registers */
    EDMA_DRV_PushConfigToReg(EDMA_UART1_RX_CH_NUM, &uart1_rx_edma_tcd);

    /* Copy configuration to software TCD structure
     * NOTE: The linker is used to guarantee 32-byte aligned SW TCD address
     * NOTE: TCD CSR Disable Request (DREQ) is not set to infinitely loop */
    EDMA_DRV_PushConfigToSTCD(&uart1_rx_edma_tcd, (edma_software_tcd_t *)&uart1_stcd_arr[0]);

    return;
}
