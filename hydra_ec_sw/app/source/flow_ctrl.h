/* Copyright (c) 2017-2020 by Phase Four, Inc. All Rights Reserved. Confidential and Proprietary.
 *
 * flow_ctrl.h
 */

#ifndef SOURCE_FLOW_CTRL_H_
#define SOURCE_FLOW_CTRL_H_

#include "mcu_types.h"
#include "p4_err.h"

void flow_ctrl_init(void);
void flow_ctrl_enable(bool enable);
void flow_ctrl_set(float mdot_setpt);
Cmd_Err_Code_t flow_ctrl_setpt(float mdot_setpt);
void flow_ctrl_iterate(void);
float get_current_mdot(float mfs_in_volt, float rtd2);

#endif /* SOURCE_FLOW_CTRL_H_ */
