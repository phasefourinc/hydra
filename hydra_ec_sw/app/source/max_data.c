/* Copyright (c) 2017-2020 by Phase Four, Inc. All Rights Reserved. Confidential and Proprietary.
 *
 * max_data.c
 */


#include "max_data.h"
#include "mcu_types.h"
#include "timer64.h"
#include "max_info_driver.h"
#include "math.h"

#define BL1_HDR_ADDR    0x00000F00UL
#define BL2_HDR_ADDR    0x00001F00UL

MaxData_t MaxData = {0};


/* Note: Do not utilize any parameter table items in this function since
 * they are most likely not initialized yet. */
void max_data_init(void)
{
    Img_Header_Rec_t *bl_hdr;

    /* At each reset, reset the timestamp sync for the LPIT from 0 (down-counter) */
    MaxData.time_sync_lpit_hi = 0xFFFFFFFFUL;
    MaxData.time_sync_lpit_lo = 0xFFFFFFFFUL;

    MaxData.bl_select  = (uint8_t)app_hdr_rec->dna_record.bl_select;
    MaxData.app_select = (uint8_t)app_hdr_rec->dna_record.app_select;

    if (MaxData.bl_select == 1U)
    {
        bl_hdr = (Img_Header_Rec_t *)BL1_HDR_ADDR;
    }
    else
    {
        bl_hdr = (Img_Header_Rec_t *)BL2_HDR_ADDR;
    }

    MaxData.bl_crc  = bl_hdr->img_crc;
    MaxData.app_crc = app_hdr_rec->app_record.img_crc;

#ifdef DUMMY_LOAD_STUBS

    /* Following devices are stubbed
     *  - IPFCV
     *  - IHPISO
     *  - PTANK
     *  - TRTD4
     *  - PLOWP
     *  - MFS_MDOT
     *  - TRTD2
     */

    MaxData.ipfcv = 0.004f;
    MaxData.ihpiso = 0.008f;
    MaxData.pprop = 526.0f;  /* 1500 max */
    MaxData.plowp = 0.7f;
    MaxData.tmfs = 0.011f;

    /* Heating starts at just above 0degC to make math easier */
    MaxData.tprop = 0.76f;
    MaxData.stub_ttank_temp_setpoint = 0.76;
    MaxData.stub_heater_setpt_mA = 1240.0f;                /* 1.25A max */
    MaxData.stub_thrust_anomaly_sec_cnt = 0xFFFFFFFFUL;    /* Initialize to impossible number */
    MaxData.stub_needs_reset_sec_cnt = 0xFFFFFFFFUL;       /* Initialize to impossible number */
#endif

    return;
}


/* Tracking the current unix time is a multi-step process:
 *  1. FSW receives a 64-bit unix time via command
 *  2. FSW associates/syncs the unix time with a 64-bit LPIT tick value
 *  3. Upon each new OS frame, the current time is recalculated:
 *      a. Current 64-bit LPIT tick value is obtained
 *      b. The time elapsed since the sync LPIT tick value is determined
 *      c. The time elapsed is added to the 64-bit unix time received via command
 *
 * At reset, the current unix time is reset to 0 (Jan 1, 1970 12am) and
 * continues to count up from there until a valid unix time is received.
 *
 * Examples:
 *  - If no time elapsed since the last_unix_time_sync_ms, then
 *    unix_time_us_hi/lo = last_unix_time_sync_us_hi/lo
 *
 *  - If the last_unix_time_sync_us_hi/lo is never set (= 0), then
 *    unix_time_us_hi/lo = the usec that have elapsed since 0
 *
 *  - If
 *      unix_time_since_us_hi = 0x0
 *      unix_time_since_us_lo = 0xFFFFFFFF
 *      last_unix_time_sync_us_hi = 0x0
 *      last_unix_time_sync_us_lo = 0xFFFFFFFF
 *    then
 *      unix_time_us_hi = 0x1
 *      unix_time_us_lo = 0xFFFFFFFE
 *
 *  - If
 *      unix_time_since_us_hi = 0x1
 *      unix_time_since_us_lo = 0x0
 *      last_unix_time_sync_us_hi = 0x0
 *      last_unix_time_sync_us_lo = 0xFFFFFFFF
 *    then
 *      unix_time_us_hi = 0x1
 *      unix_time_us_lo = 0xFFFFFFFF
 *
 *  - If
 *      unix_time_since_us_hi = 0x1
 *      unix_time_since_us_lo = 0x1
 *      last_unix_time_sync_us_hi = 0x0
 *      last_unix_time_sync_us_lo = 0xFFFFFFFF
 *    then
 *      unix_time_us_hi = 0x2
 *      unix_time_us_lo = 0x0
 */
void max_data_unix_time_update(uint32_t curr_tick_upper, uint32_t curr_tick_lower)
{
    float unix_time_since_us = 0.0f;
    uint32_t unix_time_since_us_hi = 0UL;
    uint32_t unix_time_since_us_lo = 0UL;

    /* Read the number of usecs that have elapsed since the sync timestamp */
    unix_time_since_us = 1000.0f * timer64_get_duration_ms(MaxData.time_sync_lpit_hi,
                                                           MaxData.time_sync_lpit_lo,
                                                           curr_tick_upper,
                                                           curr_tick_lower);

    /* Turn the elapsed usec into high and low usec counters */
    unix_time_since_us_hi = (uint32_t)(unix_time_since_us / 4294967296.0f);
    unix_time_since_us_lo = (uint32_t)(unix_time_since_us - ((float)unix_time_since_us_hi * 4294967296.0f));

    /* Get the current unix time in usec by adding the elapsed time to the sync unix time */
    MaxData.unix_time_us_hi = unix_time_since_us_hi + MaxData.last_unix_time_sync_us_hi;
    MaxData.unix_time_us_lo = unix_time_since_us_lo + MaxData.last_unix_time_sync_us_lo;

    /* If the msec low time rolled over, increment the upper. */
    if (MaxData.unix_time_us_lo < MaxData.last_unix_time_sync_us_lo)
    {
        MaxData.unix_time_us_hi++;
    }

    return;
}

uint64_t get_unix_timestamp_micro(void)
{
   return  ((uint64_t)MaxData.unix_time_us_hi << 32U) + (uint64_t)MaxData.unix_time_us_lo;
}


/* This function generates a psudo random float. This is generated from decimal portion of the internal temp sensor of adc0 onthe prop controller.*/
/* Note this srand will generate a float from 0.00 - 0.999...*/
float max_srand_float_tadc0(void)
{
    return (float)(MaxData.tadc0 - floorf(MaxData.tadc0));
}

float max_srand_float_tadc1(void)
{
    return (float)(MaxData.tadc1 - floorf(MaxData.tadc1));
}


/* Coerces the passed in value to zero should the value be near or below zero. */
float max_coerce(float input)
{
    float coerced_value = input;

    if (input < 0.0f)
    {
        coerced_value = 0.0f;
    }

    return coerced_value;
}
