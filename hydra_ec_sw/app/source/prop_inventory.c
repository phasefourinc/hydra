/* Copyright (c) 2017-2020 by Phase Four, Inc. All Rights Reserved. Confidential and Proprietary.
 *
 * prop_inventory.c
 */

#include "prop_inventory.h"
#include "math.h"
#include "default_pt.h"

float calculate_density_lowp(float pressure, float temp){
    const float MIN_TEMP    =  25.0f;
    const float MAX_TEMP    =  65.0f;
    const float MIN_PRESS   =  1.0f;
    const float MAX_PRESS    = 975.0f;
    float x_in = ((2 / (MAX_PRESS - MIN_PRESS)) * (pressure - MIN_PRESS)) - 1;
    float y_in = ((2 / (MAX_TEMP - MIN_TEMP)) * (temp - MIN_TEMP)) - 1;
    float density = eval_chevy_chase_lowp(PADUA_N_LOW_PSI, (const float (*)[31][31])&PT_PropInv->prop_inv_c_lo_psi[0], x_in, y_in);
    return density;
}


float calculate_density_highp(float pressure, float temp){
    const float MIN_TEMP = 25.0f;
    const float MAX_TEMP = 65.0f;
    const float MIN_PRESS = 975.0f;
    const float MAX_PRESS = 3000.0f;
    float x_in = ((2.0f / (MAX_PRESS - MIN_PRESS)) * (pressure - MIN_PRESS)) - 1.0f;
    float y_in = ((2.0f / (MAX_TEMP - MIN_TEMP)) * (temp - MIN_TEMP)) - 1.0f;
    float density = eval_chevy_chase_highp(PADUA_N_HIGH_PSI, (const float (*)[26][26])&PT_PropInv->prop_inv_c_hi_psi[0], x_in, y_in);
    return density;
}


float eval_chevy_chase_lowp(uint32_t n, const float (*c_2darr)[31][31], float x1, float x2)
{
    float res = 0.0f;
    uint32_t k, j;
    for (k = 0UL; k < (n + 1UL); ++k)
    {
        for (j = 0UL; j < (k + 1UL); ++j)
        {
            res += (*c_2darr)[j][k-j] * T_hat(j, x1) * T_hat(k - j, x2);
        }
    }

    res -= ((*c_2darr)[n][0]/2.0f) * T_hat(n, x1) * T_hat(0, x2);

    return res;
}


float eval_chevy_chase_highp(uint32_t n, const float (*c_2darr)[26][26], float x1, float x2)
{
    float res = 0.0f;
    uint32_t k, j;
    for (k = 0UL; k < (n + 1UL); ++k)
    {
        for (j = 0UL; j < (k + 1UL); ++j)
        {
            res += (*c_2darr)[j][k-j] * T_hat(j, x1) * T_hat(k - j, x2);
        }
    }

    res -= ((*c_2darr)[n][0]/2.0f) * T_hat(n, x1) * T_hat(0, x2);

    return res;
}


float T_hat(uint32_t p, float x)
{
    if (p == 0UL)
    {
        return 1.0f;
    }
    else
    {
        return (sqrtf(2.0f) * cosf((float)p * acosf(x)));
    }
}

