/* Copyright (c) 2017-2020 by Phase Four, Inc. All Rights Reserved. Confidential and Proprietary.
 *
 * p4_cmd.h
 */

#ifndef SOURCE_P4_CMD_H_
#define SOURCE_P4_CMD_H_

typedef enum
{
    IDLE                          = 0x0002,
    THRUST                        = 0x0014,
    BIST                          = 0x001B,
    GET_STATUS                    = 0x0028,
    CONFIG_THRUST                 = 0x0032,
    SET_UTIME                     = 0x003C,
    CLR_ERRORS                    = 0x0046,
    SET_AUTO_STATUS               = 0x0050,
    GET_SW_INFO                   = 0x005A,
    RX_AUTO_STATUS_PCU            = 0x0064,
    SET_NEXT_APP                  = 0x0070,
    ABORT                         = 0x00FF,

    WRT_ERR                       = 0x3805,
    WRT_ERR_ERRAO_BLK             = 0x3806,
    WRT_FLSH                      = 0x3807,
    TRN_SRAM_FLASH                = 0x3808,
    TEST_VENT_SAFE                = 0x3809,
    TEST_THRUST_ANOMALY           = 0x380A,
    TEST_NEEDS_RESET              = 0x380B,
    DFT_SRAM_ECC_1BIT             = 0x380C,
    DFT_PT_SELL_YOUR_SOUL         = 0x380D,
    DFT_PT_SET                    = 0x380E,
    AUX_ENABLE_CMD                = 0x380F,

    SET_HPISO_EN                  = 0x4010,
    SET_HPISO_I                   = 0x4011,
    SET_HPISO_PWM_RAW             = 0x4012,
    SET_HEATER_EN                 = 0x4020,
    SET_HEAT_I                    = 0x4021,
    SET_HEATER_PWM_RAW            = 0x4022,
    SET_HEATER_RST_EN             = 0x4023,
    SET_HEAT_SETPT                = 0x4024,
    SET_HEAT_CTL_ALG_EN           = 0x4025,
    SET_PFCV_I                    = 0x4030,
    SET_PFCV_DAC_RAW              = 0x4031,
    SET_PFCV_DAC_RST_EN           = 0x4032,
    SET_FLOW_RATE                 = 0x4033,
    SET_FLOW_CTL_ALG_EN           = 0x4034,
    SET_DAQ_RATE                  = 0x4035,
    SET_RFT_RST_EN                = 0x4040,
    SET_WDI_EN                    = 0x4041,
    SET_DEBUG_LEDS                = 0x4042,
    RX_AUTO_TLM_PCU               = 0x4043,
    RX_AUTO_TLM_RFT               = 0x4044,
    RX_AUTO_TLM_DEBUG             = 0x4045,
    CONFIG_THRUST_ADV             = 0x4070,

    SET_MAX_HRD_INFO              = 0x5000,
    ERASE_MAX_HRD_INFO            = 0x5001,

    MANUAL                        = 0x5807,
    SW_UPDATE                     = 0x5808,
    WILD_WEST                     = 0x580D,
    SW_UP_PRELOAD                 = 0x5810,
    SW_UP_STAGE                   = 0x5811,
    SW_UP_PROGRAM                 = 0x5812,
    GET_TLM                       = 0x5820,
    GET_TLM_RFT_CACHED            = 0x5821,
    SET_AUTO_TLM                  = 0x5822,
    SET_REC_BANK                  = 0x5823,
    SET_REC_NAND_EN               = 0x5824,
    SET_REC_FRAM_EN               = 0x5825,
    ENBL_REC_TLM                  = 0x5826,
    DUMP_TLM                      = 0x5827,
    DUMP_TLM_DONE                 = 0x5828,
    ABORT_DATA_OP                 = 0x5829,
    CLR_REC_TLM                   = 0x582A,
    CLR_NAND_FULL                 = 0x582B,
    CLEAR_ERRORS_ADVANCED         = 0x582C,
    RX_DUMP_TLM_PCU               = 0x583E,
    RX_DUMP_TLM_RFT               = 0x583F,
    DUMP_MCU                      = 0x5842,
    STOP_WDT                      = 0x5843,
    GET_MAX_HRD_INFO              = 0x5844,


/* Passthrough Thruster Commands */

    RFT_SET_DEBUG_LED0            = 0x4F00,
    RFT_SET_WDT_RST_NCLR_EN       = 0x4F01,
    RFT_SET_HUM_TEMP_SEL_EN       = 0x4F02,
    RFT_SET_WF_EN                 = 0x4F03,
    RFT_SET_PP_EN                 = 0x4F04,
    RFT_SET_PP_VSET_PWM_RAW       = 0x4F05,
    RFT_SET_WAV_GEN_FREQ          = 0x4F06,
    RFT_SET_PP_VSET               = 0x4F07,
    RFT_RAMP_PUSH_PULL            = 0x4F08,



} P4Cmd_Id_t;


#endif /* SOURCE_P4_CMD_H_ */
