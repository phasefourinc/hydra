/* Copyright (c) 2017-2020 by Phase Four, Inc. All Rights Reserved. Confidential and Proprietary.
 *
 * bad_block.h
 */

#ifndef SOURCE_BAD_BLOCK_H_
#define SOURCE_BAD_BLOCK_H_


/*!
 * @addtogroup bad_block
 * @{
 *
 * @file
 * @brief
 * This file defines the bad block map for the NAND Flash
 *
 * According to TC58 documentation, an entire page is marked as bad if
 * any column returns with a zero value. In this design, only the last
 * byte of the first page of the block is checked (byte 2047), as this
 * byte is expected to remain untouched during TLM recordings.
 *
 * Global invariants:
 *  - NAND Flash column 511 and 2047 for every page is not written and
 *    can be trusted as an untouched byte to check for bad blocks.
 *    Recommend defaulting the buffer to all 1's when writing to NAND.
 */

#include "mcu_types.h"

#define BB_COL_CHECK_OFFSET_TLM    511U
#define BB_COL_CHECK_OFFSET        2047U
#define BB_VALID_BLOCK_VALUE       0xFFU


/*!
 * @brief Resets the bad block mapping
 */
void bad_block_map_reset(void);


/*!
 * @brief Marks the block associated with the current address as bad in the bad block map.
 *
 * @note  The provided address is masked to fit the NAND Flash address space from
 *        0x0 - 0x0800_0000. E.g. If address is 0xB600_0000, then the actual NAND
 *        Flash address is 0x0600_0000.
 *
 * @param addr  NAND Flash address to mark as bad.
 */
void bad_block_mark(uint32_t addr);


/*!
 * @brief Checks in the bad block map if the block associated with the provided address is
 *        marked as 'bad'.
 *
 * @param addr  The address to check
 * @return      True if the associated block is bad, false otherwise
 */
bool bad_block_is_marked(uint32_t addr);


/*!
 * @brief Checks if the current address within the provided address range is within a bad block
 *        of the NAND Flash. If the block is good, then true is returned and the current address
 *        is untouched. If the block is bad, the current address is incremented and the next
 *        block in NAND Flash and checked. This continues until a good block is found within the
 *        address range; returning true and the current address of the valid block. If no valid
 *        block is found, then false is returned and @p curr_addr is untouched.
 *
 * @note  If @p curr_addr is outside of the specified address region, then it is set to
 *        @p range_addr_start.
 *
 * @pre @p curr_addr is not NULL
 * @pre @p range_addr_start and range_addr_stop falls on a NAND Flash block boundary
 * @pre @p range_addr_start is less than range_addr_stop
 *
 * @param[inout] curr_addr  The current address to check and the resulting address of the next
 *                          valid block. Untouched if no valid block is found.
 * @param range_addr_start  The inclusive start address of the NAND Flash region to check
 * @param range_addr_stop   The non-inclusive end address of the NAND Flash region to check
 * @return                  True if a valid block is found, false otherwise
 */
bool bad_block_get_next_valid_addr(uint32_t *curr_addr, uint32_t range_addr_start, uint32_t range_addr_stop);

/*! @} */

#endif /* SOURCE_BAD_BLOCK_H_ */
