/* Copyright (c) 2017-2020 by Phase Four, Inc. All Rights Reserved. Confidential and Proprietary.
 *
 * bad_block.c
 */

#include "bad_block.h"
#include "mcu_types.h"
#include "tc58.h"
#include <string.h>

/* Track Nand Flash good and bad blocks */

#define BB_MAP_SIZE          (TC58_TOTAL_BLOCKS / 8U)
#define BB_MAP_BLOCK_NUM(x)  (x / TC58_BLOCK_SIZE)
#define BB_MAP_IDX(addr)     (BB_MAP_BLOCK_NUM(TC58_2ADDR(addr)) / 8U)
#define BB_MAP_BIT(addr)     (0x80U >> (BB_MAP_BLOCK_NUM(TC58_2ADDR(addr)) % 8U))


/* In efforts to reduce memory usage of the bad block map, each Nand Flash block
 * status gets it's own bit indicating good (0b) or bad (1b).  The map is an array
 * of uint8_t, each storing the status of 8 blocks.  The layout is such that the
 * MSBit of bad_block_map[0] = block0 (0x80) and LSBit of bad_block_map[1] = block15 (0x01) */
static uint8_t bad_block_map[BB_MAP_SIZE] = {0U};

static uint32_t bad_block_cnt = 0UL;


void bad_block_map_reset(void)
{
    memset(&bad_block_map[0], 0, sizeof(bad_block_map));
    return;
}

/* Set the bad block bit in the map
 * Note: If setting a block that is already bad, the bad_block_cnt is still incremented. */
void bad_block_mark(uint32_t addr)
{
    bad_block_map[BB_MAP_IDX(addr)] |= BB_MAP_BIT(addr);
    ++bad_block_cnt;
    return;
}


/* Return if the bad block bit is set in the map */
bool bad_block_is_marked(uint32_t addr)
{
    return (0U != (bad_block_map[BB_MAP_IDX(addr)] & BB_MAP_BIT(addr)));
}


bool bad_block_get_next_valid_addr(uint32_t *curr_addr, uint32_t range_addr_start, uint32_t range_addr_stop)
{
    /* Validate that the start and stop addresses fall on a
     * NAND Flash block boundary and that start and stop
     * address are valid. */
    DEV_ASSERT(curr_addr);
    DEV_ASSERT((range_addr_start & (TC58_BLOCK_SIZE - 1UL)) == 0UL);
    DEV_ASSERT((range_addr_stop  & (TC58_BLOCK_SIZE - 1UL)) == 0UL);
    DEV_ASSERT(range_addr_start <= range_addr_stop);

    uint32_t i;
    bool found_next_block = false;
    uint32_t addr = *curr_addr;
    uint32_t region_block_cnt = (range_addr_stop - range_addr_start) / TC58_BLOCK_SIZE;

    /* If the address is beyond the range or has rolled over, then
     * return indicating that no block was detected */
    if (range_addr_start <= range_addr_stop)
    {
        for (i = 0UL; (i < region_block_cnt) && (!found_next_block); ++i)
        {
            /* Placing this check here is safer in the case the current
             * address passed in is is outside of the provided address range.
             * This also allows for this function to handle rollover for the
             * user when the current address has hit the end of their region */
            if ((addr < range_addr_start) || (addr >= range_addr_stop))
            {
                addr = range_addr_start;
            }

            /* If the block is marked as bad, increment to the beginning of the next block. */
            if (bad_block_is_marked(addr))
            {
                addr = (addr + TC58_BLOCK_SIZE) & (~(TC58_BLOCK_SIZE - 1UL));
            }
            else
            {
                /* Return the addr of the valid block */
                *curr_addr = addr;
                found_next_block = true;
            }
        }
    }

    return found_next_block;
}


