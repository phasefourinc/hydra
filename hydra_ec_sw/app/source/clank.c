/* Copyright (c) 2017-2020 by Phase Four, Inc. All Rights Reserved. Confidential and Proprietary.
 *
 * clank.c
 */


#include "clank.h"
#include "mcu_types.h"
#include "crc.h"
#include <string.h>


int16_t Clank_GetStartIdx(uint8_t *buf, uint16_t buf_length)
{
    DEV_ASSERT(buf);
    DEV_ASSERT(buf_length <= 0x7FFFU);  /* Avoid issues with comparing int and uint */

    int16_t i;
    bool found = false;

    for (i = 0UL; i < (int16_t)buf_length; ++i, ++buf)
    {
        if (*buf == CLANK_SYNC_VER0)
        {
            found = true;
            break;
        }
    }

    if (!found)
    {
        i = -1;
    }

    return i;
}


void Clank_GetHeader(uint8_t *message_in, Clank_Hdr_t *hdr)
{
    DEV_ASSERT(message_in);

    hdr->SyncByte    = message_in[0];
    hdr->Src_Address = message_in[1];
    hdr->Trans_Mask  = (((uint16_t)message_in[2]) << 8U) + (uint16_t)message_in[3];
    hdr->Pkt_Seq_Cnt = message_in[4];
    hdr->MsgID       = (((uint16_t)message_in[5]) << 8U) + (uint16_t)message_in[6];
    hdr->Pkt_DataLen = message_in[7];

    return;
}

bool Clank_CheckHeaderInfo(const Clank_Hdr_t *hdr)
{
    DEV_ASSERT(hdr);

    uint16_t is_valid = false;

    if ((CLANK_SYNC_VER0      == hdr->SyncByte)    &&
        (CLANK_MAX_SRC_ADDR   >= hdr->Src_Address) &&
        (CLANK_INVALID_MSG_ID != hdr->MsgID))
    {
        is_valid = true;
    }

    return is_valid;
}


bool Clank_CheckTxMask(const Clank_Hdr_t *hdr, uint8_t rx_clank_addr)
{
    DEV_ASSERT(rx_clank_addr <= CLANK_MAX_CLANK_ADDR);

    return ((hdr->Trans_Mask & (1U << rx_clank_addr)) > 0U);
}


bool Clank_CheckCrc(uint8_t *message_in)
{
    DEV_ASSERT(message_in);

    bool is_valid = false;
    uint16_t exp_crc;
    uint16_t act_crc;
    Clank_Hdr_t hdr;

    Clank_GetHeader(message_in, &hdr);

    /* Expected CRC should be the last 2 bytes at the end of the packet */
    exp_crc = ((uint16_t)message_in[CLANK_HDR_SIZE + hdr.Pkt_DataLen] << 8U) +
              ((uint16_t)message_in[CLANK_HDR_SIZE + hdr.Pkt_DataLen + 1]);

    act_crc = crc16_compute(0xFFFFU, (uint8_t *)&message_in[0], CLANK_HDR_SIZE + hdr.Pkt_DataLen);

    if (exp_crc == act_crc)
    {
        is_valid = true;
    }

    return is_valid;
}


void Clank_CreateMessage(uint8_t *message_out,
                         uint16_t *message_len_out,
                         bool is_master,
                         uint8_t src_id,
                         uint16_t dest_id,
                         uint8_t pkt_seq_cnt,
                         uint16_t msg_id,
                         uint8_t *data_buf,
                         uint8_t data_size)
{
    DEV_ASSERT(message_out);

    uint16_t crc;
    uint16_t curr_idx;

    /* If a master transmission, then send the msg ID as is, otherwise
     * for slave transmissions, then send msg ID with MSb set */
    uint16_t tx_msg_id = (is_master) ? msg_id : CLANK_RESP_MSG_ID(msg_id);

    /* Configure the header info */
    message_out[0] = CLANK_SYNC_VER0;
    message_out[1] = src_id;
    message_out[2] = (uint8_t)(((1U << dest_id) >> 8U)  & 0xFFU);
    message_out[3] = (uint8_t)(((1U << dest_id))        & 0xFFU);
    message_out[4] = pkt_seq_cnt;
    message_out[5] = (uint8_t)((tx_msg_id >> 8U)  & 0xFFU);
    message_out[6] = (uint8_t)((tx_msg_id)        & 0xFFU);
    message_out[7] = data_size;

    /* Configure the data if there is any. Note that order matters here
     * since the user may want to create a message that does not have
     * any payload data. */
    if ((data_size > 0U) && (data_buf != NULL))
    {
        memcpy(&message_out[CLANK_HDR_SIZE], data_buf, data_size);
    }

    crc = crc16_compute(0xFFFFU, (uint8_t *)&message_out[0], CLANK_HDR_SIZE + data_size);

    /* Set the current index to the end of the data section */
    curr_idx = CLANK_HDR_SIZE + data_size;

    /* Configure the CRC at the end of the packet */
    message_out[curr_idx++] = (uint8_t)((crc >> 8U) & 0xFFU);
    message_out[curr_idx++] = (uint8_t)((crc)       & 0xFFU);

    /* Return the length of the resulting message */
    *message_len_out = curr_idx;

    return;
}
