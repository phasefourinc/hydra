/* Copyright (c) 2017-2022 by Phase Four, Inc. All Rights Reserved. Confidential and Proprietary.
 *
 * bist.c
 */

#include "bist.h"
#include "mcu_types.h"
#include "default_pt.h"
#include "MaxwellAO.h"
#include "ErrAO.h"
#include "DaqCtlAO.h"
#include "RftAO.h"
#include "cy15.h"
#include "p4_cmd.h"
#include "max_data.h"
#include "rft_data.h"


/* Since commands intended for the Thr Ctrl require passing in a static buffer
 * for any command data, we define a generic buffer for the whole BIST to
 * share. This permits only 1 Thr Ctrl command to be transmitted at a time. */
static uint8_t bist_rft_params[8U] = {0U};


void bist_internal_ec_test(void)
{
    uint8_t test_num;
    float tmp;
    uint32_t bist_err_code = Errors.BistInternEcFail->error_id & 0xFFF0U;
    err_type *bist_err = (err_type *)Errors.BistInternEcFail;

    /* Skip test if not enabled */
    if (!PT->bist_intern_ec_test_enbl)
    {
        return;
    }

    /* Test 0, logic bus voltage low */
    test_num = 0U;
    if (MaxData.ebus < PT->bist_intern_ec_ebus_low)
    {
        bist_err->error_id = bist_err_code + test_num;
        report_error_float(&AO_Maxwell, bist_err, SEND_TO_SPACECRAFT, MaxData.ebus);
        return;
    }

    /* Test 1, logic bus voltage high */
    test_num++;
    if (MaxData.ebus > PT->bist_intern_ec_ebus_high)
    {
        bist_err->error_id = bist_err_code + test_num;
        report_error_float(&AO_Maxwell, bist_err, SEND_TO_SPACECRAFT, MaxData.ebus);
        return;
    }

    /* Test 2, logic bus power high */
    test_num++;
    tmp = MaxData.ebus * MaxData.ibus;
    if (tmp > PT->bist_intern_ec_ibus_pow_high)
    {
        bist_err->error_id = bist_err_code + test_num;
        report_error_float(&AO_Maxwell, bist_err, SEND_TO_SPACECRAFT, tmp);
        return;
    }

    /* Test 3, TC logic power high */
    test_num++;
    tmp = MaxData.ebus * MaxData.irft;
    if (tmp > PT->bist_intern_ec_irft_pow_high)
    {
        bist_err->error_id = bist_err_code + test_num;
        report_error_float(&AO_Maxwell, bist_err, SEND_TO_SPACECRAFT, tmp);
        return;
    }

    /* Test 4, pprop not railed */
    test_num++;
    if ((PT->bist_intern_ec_pprop_enbl) &&
        ((Adc0.ch_voltage[IN5] < ADC_MIN_V) || (Adc0.ch_voltage[IN5] > ADC_MAX_V)))
    {
        bist_err->error_id = bist_err_code + test_num;
        report_error_float(&AO_Maxwell, bist_err, SEND_TO_SPACECRAFT, Adc0.ch_voltage[IN5]);
        return;
    }

    /* Test 5, tprop not railed */
    test_num++;
    if ((PT->bist_intern_ec_tprop_enbl) &&
        ((Adc1.ch_voltage[IN0] < ADC_MIN_V) || (Adc1.ch_voltage[IN0] > ADC_MAX_V)))
    {
        bist_err->error_id = bist_err_code + test_num;
        report_error_float(&AO_Maxwell, bist_err, SEND_TO_SPACECRAFT, Adc1.ch_voltage[IN0]);
        return;
    }

    /* Test 6, plowp not railed */
    test_num++;
    if ((PT->bist_intern_ec_plowp_enbl) &&
        ((Adc1.ch_voltage[IN3] < ADC_MIN_V) || (Adc1.ch_voltage[IN3] > ADC_MAX_V)))
    {
        bist_err->error_id = bist_err_code + test_num;
        report_error_float(&AO_Maxwell, bist_err, SEND_TO_SPACECRAFT, Adc1.ch_voltage[IN3]);
        return;
    }

    /* Test 7, mfs_mdot not railed */
    test_num++;
    if ((PT->bist_intern_ec_mfs_mdot_enbl) &&
        ((Adc1.ch_voltage[IN6] < ADC_MIN_V) || (Adc1.ch_voltage[IN6] > ADC_MAX_V)))
    {
        bist_err->error_id = bist_err_code + test_num;
        report_error_float(&AO_Maxwell, bist_err, SEND_TO_SPACECRAFT, Adc1.ch_voltage[IN6]);
        return;
    }

    /* Test 8, tmfs not railed */
    test_num++;
    if ((PT->bist_intern_ec_tmfs_enbl) &&
        ((Adc1.ch_voltage[IN7] < ADC_MIN_V) || (Adc1.ch_voltage[IN7] > ADC_MAX_V)))
    {
        bist_err->error_id = bist_err_code + test_num;
        report_error_float(&AO_Maxwell, bist_err, SEND_TO_SPACECRAFT, Adc1.ch_voltage[IN7]);
        return;
    }

    return;
}


void bist_internal_tc_test(void)
{
    uint8_t test_num;
    uint32_t bist_err_code = Errors.BistInternTcFail->error_id & 0xFFF0U;
    err_type *bist_err = (err_type *)Errors.BistInternTcFail;

    /* Skip test if not enabled */
    if (!PT->bist_intern_tc_test_enbl)
    {
        return;
    }

    /* Test 0, Thr Ctrl will report error here if no connection, which indicates if
     * other tests that follow are skipped due to no comm */
    test_num = 0U;
    if (!MaxData.tc_connected)
    {
        bist_err->error_id = bist_err_code + test_num;
        report_error_uint(&AO_Maxwell, bist_err, SEND_TO_SPACECRAFT, MaxData.tc_comm_fail_cum_cnt);
        return;
    }

    /* Test 1, TC logic bus voltage low */
    test_num++;
    if (TcData.ebus < PT->bist_intern_tc_ebus_low)
    {
        bist_err->error_id = bist_err_code + test_num;
        report_error_float(&AO_Maxwell, bist_err, SEND_TO_SPACECRAFT, TcData.ebus);
        return;
    }

    /* Test 2, TC logic bus voltage high */
    test_num++;
    if (TcData.ebus > PT->bist_intern_tc_ebus_high)
    {
        bist_err->error_id = bist_err_code + test_num;
        report_error_float(&AO_Maxwell, bist_err, SEND_TO_SPACECRAFT, TcData.ebus);
        return;
    }

    /* Test 3, TC 12v reg current draw high */
    test_num++;
    if (TcData.i12v > PT->bist_intern_tc_i12v_high)
    {
        bist_err->error_id = bist_err_code + test_num;
        report_error_float(&AO_Maxwell, bist_err, SEND_TO_SPACECRAFT, TcData.i12v);
        return;
    }

    /* Test 4, TC Inverter connected */
    test_num++;
    if (!TcData.inverter_connected_flag)
    {
        bist_err->error_id = bist_err_code + test_num;
        report_error_float(&AO_Maxwell, bist_err, SEND_TO_SPACECRAFT, TcData.tadc0);
        return;
    }

    /* Test 5, RFT tpl is low */
    test_num++;
    if (PT->bist_intern_tc_tpl_enbl && (TcData.tpl < PT->bist_intern_tc_tpl_low))
    {
        bist_err->error_id = bist_err_code + test_num;
        report_error_float(&AO_Maxwell, bist_err, SEND_TO_SPACECRAFT, TcData.tpl);
        return;
    }

    /* Test 6, RFT tpl is high */
    test_num++;
    if (PT->bist_intern_tc_tpl_enbl && (TcData.tpl > PT->bist_intern_tc_tpl_high))
    {
        bist_err->error_id = bist_err_code + test_num;
        report_error_float(&AO_Maxwell, bist_err, SEND_TO_SPACECRAFT, TcData.tpl);
        return;
    }

    /* Test 7, RFT tmatch is low */
    test_num++;
    if (PT->bist_intern_tc_tmatch_enbl && (TcData.tmatch < PT->bist_intern_tc_tmatch_low))
    {
        bist_err->error_id = bist_err_code + test_num;
        report_error_float(&AO_Maxwell, bist_err, SEND_TO_SPACECRAFT, TcData.tmatch);
        return;
    }

    /* Test 8, RFT tmatch is high */
    test_num++;
    if (PT->bist_intern_tc_tmatch_enbl && (TcData.tmatch > PT->bist_intern_tc_tmatch_high))
    {
        bist_err->error_id = bist_err_code + test_num;
        report_error_float(&AO_Maxwell, bist_err, SEND_TO_SPACECRAFT, TcData.tmatch);
        return;
    }

    return;
}


void bist_heater_test(Bist_Part_t part)
{
    static bool test_passing = false;

    uint8_t test_num;
    uint32_t bist_err_code = Errors.BistHeaterFail->error_id & 0xFFF0U;
    err_type *bist_err = (err_type *)Errors.BistHeaterFail;
    Cmd_Err_Code_t tmp_err;

    /* Skip test if not enabled */
    if (!PT->bist_heater_test_enbl)
    {
        return;
    }

    if (part == PARTA)
    {
        /* Start of a new test so initialize to passing */
        test_passing = true;

        /* Test 0, heater current reading low to start */
        test_num = 0U;
        if (MaxData.iheater > PT->bist_heater_ec_iheater_low)
        {
            test_passing = false;
            bist_err->error_id = bist_err_code + test_num;
            report_error_float(&AO_Maxwell, bist_err, SEND_TO_SPACECRAFT, MaxData.iheater);
            return;
        }

        /* Test 1, set max current on the heater and enable it */
        test_num++;
        DaqCtlAO_Heater_Enable(true);
        tmp_err = DaqCtlAO_Heater_Set(PT->bist_heater_iheater_set_ma);
        if (tmp_err.reported_error != Errors.StatusSuccess)
        {
            test_passing = false;
            bist_err->error_id = bist_err_code + test_num;
            report_error_uint(&AO_Maxwell, bist_err, SEND_TO_SPACECRAFT, tmp_err.reported_error->error_id);
            return;
        }
    }
    else if (part == PARTB)
    {
        /* Test 2, ensure heater current draw is above the minimum expected, validating basic control */
        test_num = 2U;
        if (test_passing && (MaxData.iheater < PT->bist_heater_ec_iheater_low))
        {
            bist_err->error_id = bist_err_code + test_num;
            report_error_float(&AO_Maxwell, bist_err, SEND_TO_SPACECRAFT, MaxData.iheater);
        }

        /* Test 3, always ensure heater is disabled at the end of the test */
        DaqCtlAO_Heater_Enable(false);
        (void)DaqCtlAO_Heater_Set(0.0f);
    }

    return;
}


void bist_pfcv_test(Bist_Part_t part)
{
    static bool test_passing = false;

    uint8_t test_num;
    uint32_t bist_err_code = Errors.BistFluidicsFail->error_id & 0xFFF0U;
    err_type *bist_err = (err_type *)Errors.BistFluidicsFail;
    Cmd_Err_Code_t tmp_err;

    /* Skip test if not enabled */
    if (!PT->bist_pfcv_test_enbl)
    {
        return;
    }

    if (part == PARTA)
    {
        /* Start of a new test so initialize to passing */
        test_passing = true;

        /* Test 0, PFCV reading low to start */
        test_num = 0U;
        if (MaxData.ipfcv > PT->bist_pfcv_ec_ipfcv_low)
        {
            test_passing = false;
            bist_err->error_id = bist_err_code + test_num;
            report_error_float(&AO_Maxwell, bist_err, SEND_TO_SPACECRAFT, MaxData.ipfcv);
            return;
        }

        /* Test 1, set to a non-actuating current and enable it */
        test_num++;
        tmp_err = DaqCtlAO_Pfcv_SetCurrent(PT->bist_pfcv_setpt_ma);
        if (tmp_err.reported_error != Errors.StatusSuccess)
        {
            test_passing = false;
            bist_err->error_id = bist_err_code + test_num;
            report_error_uint(&AO_Maxwell, bist_err, SEND_TO_SPACECRAFT, tmp_err.reported_error->error_id);
            return;
        }
    }
    else if (part == PARTB)
    {
        /* Test 2, Ensure PFCV current draw is above the minimum expected, validating basic control */
        test_num = 2U;
        if (test_passing && (MaxData.ipfcv < PT->bist_pfcv_ec_ipfcv_low))
        {
            test_passing = false;
            bist_err->error_id = bist_err_code + test_num;
            report_error_float(&AO_Maxwell, bist_err, SEND_TO_SPACECRAFT, MaxData.ipfcv);
        }

        /* Test 3, Ensure PFCV current draw is below the maximum expected, validating basic control */
        test_num++;
        if (test_passing && (MaxData.ipfcv > PT->bist_pfcv_ec_ipfcv_high))
        {
            test_passing = false;
            bist_err->error_id = bist_err_code + test_num;
            report_error_float(&AO_Maxwell, bist_err, SEND_TO_SPACECRAFT, MaxData.ipfcv);
        }

        /* Test 4, Always ensure PFCV is disabled at the end of the test */
        (void)DaqCtlAO_Pfcv_SetCurrent(0.0f);
    }

    return;
}


void bist_memory_test(void)
{
    uint8_t test_num;
    status_t status;
    uint32_t bist_err_code = Errors.BistMemoryFail->error_id & 0xFFF0U;
    err_type *bist_err = (err_type *)Errors.BistMemoryFail;

    /* Skip test if not enabled */
    if (!PT->bist_memory_test_enbl)
    {
        return;
    }

    /* Test 0, Validate FRAM Device ID can be read successfully, checking its connection */
    test_num = 0U;
    status = CY15_FRAM_Init(LPSPICOM1, LPSPI_PCS0);
    if (status != STATUS_SUCCESS)
    {
        bist_err->error_id = bist_err_code + test_num;
        report_error_uint(&AO_Maxwell, bist_err, SEND_TO_SPACECRAFT, (uint32_t)status);
        return;
    }

    /* Test 1, No command to explicitly command TC FRAM so just check for any startup errors
     * relating to it */
    test_num++;
    if ((TcData.err1_code == Errors.DataaoFramInitFail->error_id) ||
        (TcData.err2_code == Errors.DataaoFramInitFail->error_id) ||
        (TcData.err3_code == Errors.DataaoFramInitFail->error_id))
    {
        bist_err->error_id = bist_err_code + test_num;
        report_error_uint(&AO_Maxwell, bist_err, SEND_TO_SPACECRAFT, Errors.DataaoFramInitFail->error_id);
        return;
    }

    return;
}


void bist_push_pull_test(Bist_Part_t part)
{
    static bool test_passing = false;

    uint8_t test_num;
    uint32_t raw_bytes;
    uint32_t bist_err_code = Errors.BistPushPullFail->error_id & 0xFFF0U;
    err_type *bist_err = (err_type *)Errors.BistPushPullFail;
    float tmp;

    /* Skip test if not enabled or if no Thr Ctrl or if no high power harness power applied */
    if ((!PT->bist_pp_test_enbl) || (!MaxData.tc_connected) || (!TcData.hp_connected_flag))
    {
        return;
    }

    if (part == PARTA)
    {
        /* Start of a new test so initialize to passing */
        test_passing = true;

        /* Test 0, Ensure the waveform generator is disabled */
        test_num = 0U;
        if (TcData.gpio_wf_en_fb)
        {
            test_passing = false;
            bist_err->error_id = bist_err_code + test_num;
            report_error_uint(&AO_Maxwell, bist_err, SEND_TO_SPACECRAFT, TcData.gpio_wf_en_fb);
            return;
        }

        /* Test 1, Push Pull reading low to start */
        test_num++;
        if (TcData.ehvd > PT->bist_pp_tc_ehvd_low)
        {
            test_passing = false;
            bist_err->error_id = bist_err_code + test_num;
            report_error_float(&AO_Maxwell, bist_err, SEND_TO_SPACECRAFT, TcData.ehvd);
            return;
        }

        /* Test 2, command Push Pull to initial value and enable it */
        raw_bytes = F32_TO_BYTES(PT->bist_pp_vset_init);
        bist_rft_params[0] = (uint8_t)((raw_bytes >> 24U) & 0xFFUL);
        bist_rft_params[1] = (uint8_t)((raw_bytes >> 16U) & 0xFFUL);
        bist_rft_params[2] = (uint8_t)((raw_bytes >> 8U)  & 0xFFUL);
        bist_rft_params[3] = (uint8_t)((raw_bytes)        & 0xFFUL);
        RftAO_push_cmd_request(&AO_Maxwell, RFT_CMD_INTERNAL, RFT_SET_PP_VSET, &bist_rft_params[0], 4U, 0U);
    }
    else if ((part == PARTB) && test_passing)
    {
        /* Test 3, Ensure Thr Ctrl took the proper Push Pull Vset setting */
        test_num = 3U;
        if ((TcData.pp_vset_fb < (PT->bist_pp_vset_init * 0.99f)) ||
            (TcData.pp_vset_fb > (PT->bist_pp_vset_init * 1.01f)))
        {
            test_passing = false;
            bist_err->error_id = bist_err_code + test_num;
            report_error_float(&AO_Maxwell, bist_err, SEND_TO_SPACECRAFT, TcData.pp_vset_fb);
            return;
        }

        /* Test 4, Command to enable the Push Pull */
        bist_rft_params[0] = 0x01U;
        RftAO_push_cmd_request(&AO_Maxwell, RFT_CMD_INTERNAL, RFT_SET_PP_EN, &bist_rft_params[0], 1U, 0U);
    }
    else if ((part == PARTC) && test_passing)
    {
        /* Test 5, Validate the Thr Ctrl did in fact enable the Push Pull */
        test_num = 5U;
        if (!TcData.gpio_pp_en_fb)
        {
            test_passing = false;
            bist_err->error_id = bist_err_code + test_num;
            report_error_uint(&AO_Maxwell, bist_err, SEND_TO_SPACECRAFT, TcData.gpio_pp_en_fb);

            /* Command Thr Ctrl to safe since clearly there's something wrong */
            RftAO_push_cmd_request(&AO_Maxwell, RFT_CMD_INTERNAL, ABORT, NULL, 0U, 0U);
            return;
        }

        /* Test 6, Validate bounds of the initial Vset setting */
        test_num++;
        if (TcData.ehvd < PT->bist_pp_vset_init_low)
        {
            test_passing = false;
            bist_err->error_id = bist_err_code + test_num;
            report_error_float(&AO_Maxwell, bist_err, SEND_TO_SPACECRAFT, TcData.ehvd);

            /* Command Thr Ctrl to safe since clearly there's something wrong */
            RftAO_push_cmd_request(&AO_Maxwell, RFT_CMD_INTERNAL, ABORT, NULL, 0U, 0U);
            return;
        }

        /* Test 7, Validate bounds of the initial Vset setting */
        test_num++;
        if (TcData.ehvd > PT->bist_pp_vset_init_high)
        {
            test_passing = false;
            bist_err->error_id = bist_err_code + test_num;
            report_error_float(&AO_Maxwell, bist_err, SEND_TO_SPACECRAFT, TcData.ehvd);

            /* Command Thr Ctrl to safe since clearly there's something wrong */
            RftAO_push_cmd_request(&AO_Maxwell, RFT_CMD_INTERNAL, ABORT, NULL, 0U, 0U);
            return;
        }

        /* Test 8, Command the Push Pull value and ramp */
        /* First input is the commanded thrust RF Vset */
         raw_bytes = F32_TO_BYTES(PT->bist_pp_vset_final);
         bist_rft_params[0] = (uint8_t)((raw_bytes >> 24U) & 0xFFUL);
         bist_rft_params[1] = (uint8_t)((raw_bytes >> 16U) & 0xFFUL);
         bist_rft_params[2] = (uint8_t)((raw_bytes >> 8U)  & 0xFFUL);
         bist_rft_params[3] = (uint8_t)((raw_bytes)        & 0xFFUL);

        /* Push-Pull ramp command expects timing in milliseconds */
         tmp = PT->bist_pp_vset_ramp_dur_sec * 1000.0f;
         raw_bytes = F32_TO_BYTES(tmp);
         bist_rft_params[4] = (uint8_t)((raw_bytes >> 24U) & 0xFFUL);
         bist_rft_params[5] = (uint8_t)((raw_bytes >> 16U) & 0xFFUL);
         bist_rft_params[6] = (uint8_t)((raw_bytes >> 8U)  & 0xFFUL);
         bist_rft_params[7] = (uint8_t)((raw_bytes)        & 0xFFUL);
         RftAO_push_cmd_request(&AO_Maxwell, RFT_CMD_INTERNAL, RFT_RAMP_PUSH_PULL, &bist_rft_params[0], 8U, 0U);
    }
    else if (part == PARTD)
    {
        /* Test 9, validate Push Pull final setpoint is within bounds */
        test_num = 9U;
        if (test_passing && (TcData.ehvd < PT->bist_pp_vset_final_low))
        {
            test_passing = false;
            bist_err->error_id = bist_err_code + test_num;
            report_error_float(&AO_Maxwell, bist_err, SEND_TO_SPACECRAFT, TcData.ehvd);
        }

        /* Test 10, validate Push Pull final setpoint is within bounds */
        test_num++;
        if (test_passing && (TcData.ehvd > PT->bist_pp_vset_final_high))
        {
            test_passing = false;
            bist_err->error_id = bist_err_code + test_num;
            report_error_float(&AO_Maxwell, bist_err, SEND_TO_SPACECRAFT, TcData.ehvd);
        }

        /* Test 11, Safe the Thr Ctrl no matter the passing result */
        RftAO_push_cmd_request(&AO_Maxwell, RFT_CMD_INTERNAL, ABORT, NULL, 0U, 0U);
    }

    return;
}


void bist_waveform_test(Bist_Part_t part)
{
    static bool test_passing = false;

    uint8_t test_num;
    uint32_t raw_bytes;
    uint32_t bist_err_code = Errors.BistWaveformFail->error_id & 0xFFF0U;
    err_type *bist_err = (err_type *)Errors.BistWaveformFail;

    /* Skip test if not enabled or if no Thr Ctrl */
    if ((!PT->bist_wf_test_enbl) || (!MaxData.tc_connected))
    {
        return;
    }

    if (part == PARTA)
    {
        /* Start of a new test so initialize to passing */
        test_passing = true;

        /* Test 0, Ensure the Push Pull is disabled */
        test_num = 0U;
        if ((TcData.gpio_pp_en_fb) || (TcData.ehvd > PT->bist_pp_vset_init_low))
        {
            test_passing = false;
            bist_err->error_id = bist_err_code + test_num;
            report_error_float(&AO_Maxwell, bist_err, SEND_TO_SPACECRAFT, TcData.ehvd);
            return;
        }

        /* Test 1, Command the RFT with the expected WF oscillator frequency */
        raw_bytes = F32_TO_BYTES(PT->bist_wf_freq_setpt_hz);
        bist_rft_params[0] = (uint8_t)((raw_bytes >> 24U)& 0xFFUL);
        bist_rft_params[1] = (uint8_t)((raw_bytes >> 16U)& 0xFFUL);
        bist_rft_params[2] = (uint8_t)((raw_bytes >> 8U)& 0xFFUL);
        bist_rft_params[3] = (uint8_t)((raw_bytes) & 0xFFUL);
        RftAO_push_cmd_request(&AO_Maxwell, RFT_CMD_INTERNAL, RFT_SET_WAV_GEN_FREQ, &bist_rft_params[0], 4U, 0U);
    }
    else if ((part == PARTB) && test_passing)
    {
        /* Test 2, Confirm the frequency was accepted */
        test_num = 2U;
        if ((TcData.wav_gen_freq_fb < (0.99f * PT->bist_wf_freq_setpt_hz)) ||
            (TcData.wav_gen_freq_fb > (1.01f * PT->bist_wf_freq_setpt_hz)))
        {
            test_passing = false;
            bist_err->error_id = bist_err_code + test_num;
            report_error_float(&AO_Maxwell, bist_err, SEND_TO_SPACECRAFT, TcData.wav_gen_freq_fb);
            return;
        }

        /* Test 3, Enable the waveform generator */
        bist_rft_params[0] = 0x01U;
        RftAO_push_cmd_request(&AO_Maxwell, RFT_CMD_INTERNAL, RFT_SET_WF_EN, &bist_rft_params[0], 1U, 0U);
    }
    else if (part == PARTC)
    {
        /* Test 4, Confirm the waveform enable command was accepted */
        test_num = 4U;
        if (test_passing && (!TcData.gpio_wf_en_fb))
        {
            test_passing = false;
            bist_err->error_id = bist_err_code + test_num;
            report_error_uint(&AO_Maxwell, bist_err, SEND_TO_SPACECRAFT, TcData.gpio_wf_en_fb);
        }

        /* Test 5, Confirm the waveform is drawing the expected current */
        test_num++;
        if (test_passing && (TcData.i12v < PT->bist_wf_en_tc_i12v_low))
        {
            test_passing = false;
            bist_err->error_id = bist_err_code + test_num;
            report_error_float(&AO_Maxwell, bist_err, SEND_TO_SPACECRAFT, TcData.i12v);
        }

        /* Test 6, Confirm the waveform is drawing the expected current */
        test_num++;
        if (test_passing && (TcData.i12v > PT->bist_wf_en_tc_i12v_high))
        {
            test_passing = false;
            bist_err->error_id = bist_err_code + test_num;
            report_error_float(&AO_Maxwell, bist_err, SEND_TO_SPACECRAFT, TcData.i12v);
        }

        /* Test 7, Safe the Thr Ctrl no matter the passing result */
        RftAO_push_cmd_request(&AO_Maxwell, RFT_CMD_INTERNAL, ABORT, NULL, 0U, 0U);
    }

    return;
}


void bist_lp_blowout_test(Bist_Part_t part)
{
    static bool test_passing = false;

    uint8_t test_num;
    uint32_t bist_err_code = Errors.BistLpFail->error_id & 0xFFF0U;
    err_type *bist_err = (err_type *)Errors.BistLpFail;
    Cmd_Err_Code_t tmp_err;

    /* Skip test if not enabled or if the prop could be liquid */
    if ((!PT->bist_lp_blow_test_enbl) || (MaxData.tprop < PT->heater_idle_complete_degC))
    {
        return;
    }

    if (part == PARTA)
    {
        /* Start of a new test so initialize to passing */
        test_passing = true;

        /* Test 0, Ensure the HPISO valve is closed so we don't accidental flow from the tank */
        test_num = 0U;
        if (MaxData.hpiso_en_fb || (MaxData.ihpiso > PT->bist_lp_blow_ihpiso_low))
        {
            test_passing = false;
            bist_err->error_id = bist_err_code + test_num;
            report_error_float(&AO_Maxwell, bist_err, SEND_TO_SPACECRAFT, MaxData.ihpiso);
            return;
        }

        /* Test 1, Command the PFCV open to drain the low pressure fluidics */
        tmp_err = DaqCtlAO_Pfcv_SetCurrent(PT->bist_lp_blow_pfcv_setpt_ma);
        if (tmp_err.reported_error != Errors.StatusSuccess)
        {
            test_passing = false;
            bist_err->error_id = bist_err_code + test_num;
            report_error_uint(&AO_Maxwell, bist_err, SEND_TO_SPACECRAFT, tmp_err.reported_error->error_id);
            return;
        }
    }
    else if (part == PARTB)
    {
        /* Test 2, Confirm the PFCV current is within the expected bounds */
        test_num = 2U;
        if (test_passing && (MaxData.ipfcv < PT->bist_lp_blow_ec_ipfcv_low))
        {
            test_passing = false;
            bist_err->error_id = bist_err_code + test_num;
            report_error_float(&AO_Maxwell, bist_err, SEND_TO_SPACECRAFT, MaxData.ipfcv);
        }

        /* Test 3, Confirm the PFCV current is within the expected bounds */
        test_num++;
        if (test_passing && (MaxData.ipfcv > PT->bist_lp_blow_ec_ipfcv_high))
        {
            test_passing = false;
            bist_err->error_id = bist_err_code + test_num;
            report_error_float(&AO_Maxwell, bist_err, SEND_TO_SPACECRAFT, MaxData.ipfcv);
        }

        /* Test 4, Ensure the PFCV is closed */
        (void)DaqCtlAO_Pfcv_SetCurrent(0.0f);
    }

    return;
}
