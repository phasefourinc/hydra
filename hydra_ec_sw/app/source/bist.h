/* Copyright (c) 2017-2022 by Phase Four, Inc. All Rights Reserved. Confidential and Proprietary.
 *
 * bist.h
 */

#ifndef SOURCE_BIST_H_
#define SOURCE_BIST_H_

#define ADC_MIN_V   0.05f
#define ADC_MAX_V   4.95f

typedef enum Bist_Part_tag {
    PARTA,
    PARTB,
    PARTC,
    PARTD
} Bist_Part_t;

void bist_internal_ec_test(void);
void bist_internal_tc_test(void);
void bist_heater_test(Bist_Part_t part);
void bist_pfcv_test(Bist_Part_t part);
void bist_memory_test(void);
void bist_push_pull_test(Bist_Part_t part);
void bist_waveform_test(Bist_Part_t part);
void bist_lp_blowout_test(Bist_Part_t part);


#endif /* SOURCE_BIST_H_ */
