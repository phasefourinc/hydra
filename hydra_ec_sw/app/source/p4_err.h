/* Copyright (c) 2017-2020 by Phase Four, Inc. All Rights Reserved. Confidential and Proprietary.
 *
 * p4_err.h
 */

#ifndef SOURCE_P4_ERR_H_
#define SOURCE_P4_ERR_H_

#include "mcu_types.h"

enum error_conditions {
    WARNING = 0U,
    SAFE = 1U,
    UNRECOVERABLE = 2U,
    VENT_SAFE = 3,
};

typedef struct err_type{
    uint64_t mcu_time;
    uint16_t error_id;
    uint16_t err_count;
    enum error_conditions severity_level;
    uint8_t aux_data_type;
} BYTE_PACKED err_type;

typedef struct Cmd_Err_Code_t{
    err_type *reported_error;
    uint32_t aux_data;
} BYTE_PACKED Cmd_Err_Code_t;

#define NO_CURRENT_TIME 0
#define ZERO_ERRORS 0

#define UNSIGNED_INTEGER 1
#define FLOAT 2

typedef struct err_record{

   const err_type *StatusSuccess;                                      /* This value indicates that the operation was a success */
   const err_type *StatusFailure;                                      /* This error indicates there was a general failure. This is used if there is no specfic error. */
   const err_type *InvalidBootloaderError;                             /* This error is used if the error that came from the bootloader is not a valid bootloader error. If this is reported the source code for the bootloader should be checked. */

   const err_type *GeneralBootloaderError;                             /* This is a general bootloader failure. A catch all error if there is not a specfic enough error. */
   const err_type *BootloaderFlashMaxSize;                             /* The bootloader application size being loaded into is over the maximum allocated amount */
   const err_type *InvalidBootloaderCrcValue;                          /* The bootloader the that the startup process to load into had an invalid CRC. */
   const err_type *InvalidBootAppType;                                 /* The application loaded in the bootloader section of flash is not a bootloader type application. */
   const err_type *InvalidBootHardType;                                /* The bootloader loaded in the bootloader section of flash is not meant for this hardware. */
   const err_type *InvalidBootProdType;                                /* The application loaded in the bootloadersection of flash is not meant for this product. */
   const err_type *InvalidApp1FlashSize;                               /* This error indicates that flash application 1 size is over the maximum allocated amount */
   const err_type *InvalidFlashApp1CrcValue;                           /* This error indicates that flash application 1 had a invalid crc */
   const err_type *InvalidFlashApp1AppType;                            /* The application loaded in the App 1 section of flash is not a sw image (EXE TYPE)  type application. */
   const err_type *InvalidFlashApp1HardType;                           /* The application loaded in the App 1 section of flash is not meant for this hardware.  */
   const err_type *InvalidFlashApp1ProdType;                           /* The application loaded in the App 1 section of flash is not meant for this product.  */
   const err_type *InvalidApp2FlashSize;                               /* This error indicates that flash application 2 size is over the maximum allocated amount */
   const err_type *InvalidFlashApp2CrcValue;                           /* This error indicates that flash application 2 had a invalid crc */
   const err_type *InvalidFlashApp2AppType;                            /* The application loaded in the App 2 section of flash is not a sw image (EXE TYPE)  type application. */
   const err_type *InvalidFlashApp2HardType;                           /* The application loaded in the App 2 section of flash is not meant for this hardware.  */
   const err_type *InvalidFlashApp2ProdType;                           /* The application loaded in the App 2 section of flash is not meant for this product.  */
   const err_type *InvalidAppGoldFlashSize;                            /* The golden flash application is over the maximum allocated amount */
   const err_type *InvalidFlashGoldCrcValue;                           /* The golden flash application had a invalid crc */
   const err_type *InvalidFlashGoldAppType;                            /* The application loaded in the Golden Image section of flash is not a sw image (EXE TYPE)  type application. This should never be triggered */
   const err_type *InvalidFlashGoldHardType;                           /* The application loaded in the Golden Image section of flash is not meant for this hardware. This should never be triggered */
   const err_type *InvalidFlashGoldProdType;                           /* The application loaded in the Golden Image section of flash is not meant for this product. This should never be triggered */
   const err_type *InvalidSramAppSize;                                 /* The application is over the maximum allocated amount */
   const err_type *InvalidSramCrcValue;                                /* The application loaded into sram had a invalid crc */
   const err_type *InvalidSramAppType;                                 /* The application loaded to sram is not a sw image (EXE TYPE)  type application. */
   const err_type *InvalidSramHardType;                                /* The application loaded to sram is not meant for this hardware.  */
   const err_type *InvalidSramProdType;                                /* The application loaded to sram is not meant for this product.  */
   const err_type *InvalidBootloaderPcCounter;                         /* The PC counter is above the max bootloader address. This indicates that the PC counter is somewhere not in the bootloader. Something is very long */
   const err_type *InvalidAppNumber;                                   /* The selected app that was read from flash was is above 3 and is incorrect. The default is to select app the golden image or app=3 */
   const err_type *InvalidAppSelection;                                /* The app to be selected to go from flash to sram is wrong. This progammically should not happen so something is very wrong */
   const err_type *EndOfBootloaderMain;                                /* The bootloader program finished before it jump to the application. This should not happen something is very wrong. */

   const err_type *EmbeddedTimeout;                                    /* This error indicates that a embedded system timed out */
   const err_type *NotEnoughBytes;                                     /* The incoming data did not have enough information to be parsed correctly. */
   const err_type *MalformedHeader;                                    /* The clank parser has been improperly been formatted when in use. */
   const err_type *SyncBytesError;                                     /* The sync byte value from the clank packet did not match the proper sync byte value. */
   const err_type *SourceAddressError;                                 /* The source address from a data stream came from hardware that was not expected. IE: The TX data's transmit mask did not match there source address. */
   const err_type *TransmitMaskError;                                  /* The incoming data is not meant for this hardware. IE: The RX Transmit mask does not allgin with this hardwares Source Address */
   const err_type *PktDataLenError;                                    /* There is a difference in the reported data length and the actual calculated data length */
   const err_type *ComRxCrcError;                                      /*  */
   const err_type *RftInternCommTimeout;                               /* The transmission for an internal RFT command timed out. */
   const err_type *RftInternCommInvalidPkt;                            /* The packet received from the internal command to the RFT failed due to either malformed header, invalid CRC, or failed status acknowledge. */

   const err_type *CmdMsgIdNotFound;                                   /* The Msg ID sent from the spacecraft/master (PC, Spacecraft, Prop Controller, etc.) is not found in the hardware unit (Prop Controller, Thruster Controller etc.) */
   const err_type *CmdMsgIdStateNotSupported;                          /* The state the current hardware is in does not support the requested msg id command. Please go to the allowable state to execute command. */
   const err_type *CmdMsgIdInvalidDataSize;                            /* The data size needed for the msg id command to run correctly is not correct. Please insert the correct data length for proper function of this command. */
   const err_type *CmdInvalidMaxwellHardwareOffset;                    /* The offset value in flash is greater then the max allowable offset. Thus it will be accessing a area of flash that does not have a hardware ID record. */
   const err_type *CmdInvalidMaxRecordDataSize;                        /* The hardware record that was sent from the spacecraft/computer to be written is not the correct size.  It will not write the data into flash */
   const err_type *CmdIllformedCommaMaxRecordPacket;                   /* There is not enough commas to properly parse the data. It will not write the data into flash */
   const err_type *CmdMaxWriteRecordOffsetFull;                        /* The offest in flash that is trying to be written is not cleared. Thus inorder to write the data properly, erase flash before writing into this record offset. */
   const err_type *CmdSwUpdateBelowMinDataAmount;                      /* The data coming in for a sw update is below the minimmum needed for a succesfull update. This error occurs in the sw_staging step in the software update process. */
   const err_type *CmdProcessingPreviousCommand;                       /* The command that was just is being rejected because it is currently processing the previous command. This command will need to be resent to be executed. */
   const err_type *CmdProcessedCommandTimedout;                        /* The command that was sent had a timeout when processing the command. Please resend the command */
   const err_type *CmdInvalidStateTransition;                          /* The state the command wanted to go to is invalid. Please refer to the state machine on how to get to the desired state correctly. */
   const err_type *CmdCurrentStateIsBusy;                              /* Cannot transition to new state because the unit is busy. */
   const err_type *CmdTlmDumpInvalidMemChipSelected;                   /* The argument that selects what external memory chip you want to dump from (FRAM or FLASH) is invalid. Please select a apporiprate flash type (FRAM = 0X0A, Flash = 0x0B) */
   const err_type *CmdTlmDumpInvalidBankSelected;                      /* This error inidicates that the argument that selects what bank of memory to dump from a given external memory chip is wrong. Currently there is bank 0 (0x00), bank 1 (0x01), or both (0x02) */
   const err_type *CmdSetHeaterCurrentOutOfBounds;                     /* This error inidicates that the argument that provides how much current should go in the heater is out of bounds. Please select a current that is in the correct bounds. */
   const err_type *CmdSetHpisoCurrentOutOfBounds;                      /* This error inidicates that the argument that provides how much current should go in the HPISO is out of bounds. Please select a current that is in the correct bounds. */
   const err_type *CmdSetLpisoCurrentOutOfBounds;                      /* This error inidicates that the argument that provides how much current should go in the LPISO is out of bounds. Please select a current that is in the correct bounds. */
   const err_type *CmdInvalidAutoTlmRange;                             /* The argument that provides the rate of tlm is outside of the current tlm bounds. */
   const err_type *CmdInvalidMcuDataDumpSize;                          /* The argument that provides the amount of data to return is over the specfied clank limit. Please set the data amount less then the max clank data size which is currently 254 */
   const err_type *CmdInvalidHeaterPwmRawInvalidDutyCycle;             /* The argument that provides the duty cycle for the heater pwm is over the max amount. Please specify a value under this amount. Currently the max amount is 0x8000 which equals 100% */
   const err_type *CmdInvalidHpisoPwmRawInvalidDutyCycle;              /* The argument that provides the duty cycle for the HPISO pwm is over the max amount. Please specify a value under this amount. Currently the max amount is 0x8000 which equals 100% */
   const err_type *CmdInvalidLpisoPwmRawInvalidDutyCycle;              /* The argument that provides the duty cycle for the LPISO pwm is over the max amount. Please specify a value under this amount. Currently the max amount is 0x8000 which equals 100% */
   const err_type *CmdInvalidMdotSetpt;                                /* The commanded flow rate in SCCM was out of bounds based on the parameter table. */
   const err_type *CmdInvalidRfVoltageSetpt;                           /* The commanded RF Voltage was out of bounds based on the parameter table. */
   const err_type *CmdInvalidDurationSetpt;                            /* The error indicates that the commanded duration in seconds was out of bounds based on the parameter table. */
   const err_type *CmdDataModeIsBusy;                                  /* The error indicates that the commanded data request is busy given the data mode. */
   const err_type *CmdUpdateStageInvalidParam;                         /* SW update stage command parameter is invalid */
   const err_type *CmdUpdatePreloadInvalidParam;                       /* SW update preload command parameter is invalid */
   const err_type *CmdUpdateProgramInvalidParam;                       /* SW update program command parameter is invalid */
   const err_type *CmdDataAbortDueToSafe;                              /* The current data command could not complete due to a system SAFE function. Likely due to FDIR. */
   const err_type *CmdUpdateProgramStageInvalid;                       /* SW update program failed due to either an invalid image length or CRC in the FRAM stage area or due to mismatching hardware type, image type, or addresses. Error data is the staged CRC */
   const err_type *CmdRejectedErrorActive;                             /* State transition unable to be performed due to either a FDIR error that remains active or, if transitioning to Thrust, invalid thrust parameters */
   const err_type *CmdInvalidParameter;                                /* The command parameter passed is invalid. See SUM for the appropriate parameter range. */
   const err_type *CmdInvalidGetSoftwareInfoSelection;                 /* The argument that selects what software application you want information on is not supported. */
   const err_type *CmdRftVirtualAddrMismatch;                          /* This error indicates that not all parameters agree on an address space. Commands intended for the Thr Ctrl must all have an address space starting with 0xFXXXX_XXXX. */
   const err_type *CmdUpdateProgramApp1PtInUse;                        /* Update to the parameter table is not permitted since the it currently active as a form redundancy. Recommend running from the Golden Application and retrying to program command. */
   const err_type *CmdUpdateProgramApp2PtInUse;                        /* Update to the parameter table is not permitted since the it currently active as a form redundancy. Recommend running from the Golden Application and retrying to program command. */
   const err_type *CmdInvalidHeaterCtrlSetpt;                          /* Desired heater control setpoint is either invalid or beyond the acceptable range */
   const err_type *CmdNoSoulToSell;                                    /* The currently loaded PT failed integrity checking prior to entering the mode, error data is the calculated CRC */
   const err_type *CmdWishNotGranted;                                  /* The PT parameter offset into the table is invalid, error data is the offset requested */
   const err_type *CmdBusySendingAutoTlm;                              /*  */

   const err_type *MsgIdFunctionNotImplemented;                        /* The MSG ID that was sent by the master (PC/Spacecraft) Has been found but has not been implented. This is a pure internal software issue. If you find this please report to Phase Four */
   const err_type *CurrentProcessedCommandTimedout;                    /* The command that was sent had a timeout when processing the command. Please resend the command. If that fails a bigger issue is happening. */
   const err_type *UnexpectedTransition;                               /* There was internal unexcpect transition of state. */
   const err_type *Ad568SpiSetFailed;                                  /* The spi transfer to the DAC has failed please try sending the current again. */
   const err_type *MdotSetPointOutOfBounds;                            /* The flow rate setpoint is out of the acceptable range. */
   const err_type *InvalidTxParameters;                                /* This error inidcates that the TX parameter that was sent to the TxAO was invalid  (TxAO 327) */
   const err_type *IntermediateTxQueueHasNoData;                       /* There was no data to be sent in the intermediate queue.  (TxAO 281) */
   const err_type *TxAoIsBusy;                                         /* The TxAO module is currently busy.  (TxAO 404) */
   const err_type *TlmTmrTimeout;                                      /* Timeout attempting to send one or more full TLM packets simultaneously (TxAO 246), error data is the timeout duration in microseconds */
   const err_type *InvalidTelemtryEventRequest;                        /* The event that came from the fifo queue is illfomred or has no data  ( TlmAO 156) */
   const err_type *RxScReciviedJunkData;                               /* This error indicates that junk data has been recived on the receiving end of the  SC port  (RxScAO 223) */
   const err_type *ReceivedMalformClankPacket;                         /* The data we recived is not a valid clank header. (RxScAO 297) */
   const err_type *ReceivedRxscTimeout;                                /* There was a time when receving data from the spacecraft port. (RxScAO 276) */
   const err_type *MismatchCrc;                                        /* The CRC was incorrect. (RxGseAO 370) */
   const err_type *GseRxCircularBufferOverrun;                         /* There was a overrun on the RX's circular buffer from the GSE Port.  (RxGseAO 76) */
   const err_type *EvtCmdDumpTlmInvalidSize;                           /* Indicates that the passed EVT utilized for InterAO communication did not have the expected amount of data. Error data indicates the size of the data passed within the event */
   const err_type *EvtDataOpTimeout;                                   /* A data operation timed out, likely indicates external memory comm is struggling. Error data is the duration in microseconds of the timeout */
   const err_type *Ltc6903SpiSetFailed;                                /* The spi transfer to the LTC6903 has failed please try sending the current again. */
   const err_type *InvalidAppSelectParamater;                          /* The software selected a invalid application to load from. The only valid selection is 1=Primary App 1, 2=Primary App 2, 3=Golden Image */
   const err_type *CmdStateTransitionFail;                             /* Unable to transition into the commanded state. */
   const err_type *FlashEraseFailed;                                   /* Failed to erase flash. Likely still busy from another operation. */
   const err_type *FlashEraseFailedInvalidEraseAddress;                /* This error inidcates that when attempting to erase flash the address specfied is lower then the erase address limit. This limit is to protect accidental erasing of the bootloader or the golden image. */
   const err_type *FlashEraseFailedInvalidSectorSize;                  /* This error inidcates that when attempting to erase flash the erase size is not the size of the flash sector. To erase flash correctly the length must be the same as the flash sector.  */
   const err_type *FlashEraseFailedInvalidSectorAddress;               /* This error inidicates that when attempting to erase flash the specified start address was not at the beginning of a flash sector. Specfy a address that is the start of a flash sector. */
   const err_type *FlashEraseFailedFlashStatusBusy;                    /* The flash module is currently busy writing or erasing another section in flash */
   const err_type *FlashWriteFailed;                                   /* Failed to write flash. Likely still busy from another operation. */
   const err_type *FlashWriteFailedInvalidEraseAddress;                /* This error inidcates that when attempting to write flash the address specfied is lower then the write address limit. This limit is to protect accidental writing over of the bootloader or the golden image. */
   const err_type *FlashWriteFailedOverLimitWriteLength;               /* This error indicates that when attempting to write to flash the write amount to flash is to large for this module. */
   const err_type *FlashWriteFailedFlashStatusBusy;                    /* The flash module is currently busy writing or erasing another section in flash */
   const err_type *UsingDefaultPtInvalidAppSel;                        /* The Default Parameter Table is selected due to an invalid Application Select value, error data is the app_select value */
   const err_type *UsingDefaultPtTypeMismatch;                         /* The Default Parameter Table is selected due to the image type code mismatching the one defined for the loaded Application, error data is the PT image type code */
   const err_type *UsingDefaultPtLengthMismatch;                       /* The Default Parameter Table is selected due to the image length mismatching the one defined for the loaded Application, error data is the PT image length */
   const err_type *UsingDefaultPtCrcMismatch;                          /* The Default Parameter Table is selected due to the Application PT failing its CRC check, error data is the computed App PT CRC */
   const err_type *UsingAppPt;                                         /* The App Parameter Table is selected due to the failure to successfully copy over the table to the Runtime PT area, error data is the address of the App PT in use */
   const err_type *FlowRateInvalidCalculation;                         /* Cannot complete flow rate determination due to div-by-zero error */
   const err_type *HeaterCtrlInvalidSetpt;                             /* Cannot set the current heater control setpoint due to an invalid value */
   const err_type *TlmInvalidRequestData;                              /* Internal error in the request to the TLM AO */
   const err_type *ErraoErrorFlashStorageFull;                         /* The flash location that hold errors are full. This should normally not happend and ethier there was a implenetation error. Or there clearing flash failed. */
   const err_type *ErraoCantDetermineCurrentFlashAddress;              /* The current flash address can not be determined. This is likely due to the fact that the flash location is full of junk or errors and was not cleared correctly. */
   const err_type *ErraoCurrentFlashAddressOutOfBounds;                /* The current flash adddress is out of the error flash address bounds. This can only happen if it was this value was updated to go out of bounds in the ERRAO or somewhere else (rouge code) */
   const err_type *ErraoClrErrorsEvtNoData;                            /* Clear error event has invalid data, error data returned has both the request has_data flag and the data_size in the LSBs respectively. */
   const err_type *ErraoClrFlashFail;                                  /* Erasing flash failed, error data returned is the SDK status */
   const err_type *ErraoEraseSectorFail;                               /* Erasing flash sector failed, error data returned is the SDK status */
   const err_type *PropInvInvalidPropCalculation;                      /* This error indicates that the prop inventory/estimation theory calculation is out of the appropriate bounds determined by our analysis. Something is wrong with the prop inventory/estimation theory calculation */
   const err_type *Uart0RxError;                                       /* LPUART0 module detected an overrun, noise, framing, or  parity error. STAT reg returned in error data. */
   const err_type *Uart0RxDmaError;                                    /* UART Rx DMA Channel tied to LPUART0 detected an error, DMA Error Status register returned in error data */
   const err_type *Uart1RxError;                                       /* LPUART1 module detected an overrun, noise, framing, or  parity error. STAT reg returned in error data. */
   const err_type *Uart1RxDmaError;                                    /* UART Rx DMA Channel tied to LPUART1 detected an error, DMA Error Status register returned in error data */
   const err_type *Uart0TxDmaError;                                    /* DMA error during transmission over the Spacecraft port LPUART0, DMA Error Status register returned in error data */
   const err_type *Uart1TxDmaError;                                    /* DMA error during transmission over the GSE port LPUART1, DMA Error Status register returned in error data */
   const err_type *TxaoTxTimeout;                                      /* The transmission of a packet timed out */
   const err_type *TxaoInterqFull;                                     /* Cannot push another Tx request because the FIFO queue is full, the number of entries in the queue is returned in error data */
   const err_type *TxaoInterqNoData;                                   /* Attempting to pop from the Tx FIFO queue failed due to having invalid or nonexistent Tx request data, error data returned has both the request has_data flag and the data_size in the LSBs respectively. */
   const err_type *TxaoInvalidTxParams;                                /* The Tx request type does not exist. The Tx request type enum is returned in error data */
   const err_type *TxaoUartBusy;                                       /* The transmission cannot commence because the UART is currently busy. Error data returned has both the Tx request seq ID and msg ID in the LSBs respectively */
   const err_type *DataaoFramInitFail;                                 /* Unable to initialize FRAM and read the device ID, could indicate that SW is loaded on the wrong hardware. */
   const err_type *DataaoNandInitFail;                                 /* Unable to initialize NAND and read the device ID */
   const err_type *DataaoFramWriteFail;                                /* TLM packet recording failure when writing to FRAM, error data is the current FRAM address */
   const err_type *DataaoNandLastProgFail;                             /* TLM packet programming to NAND failed, the error data is the current NAND address (not the failed address) for some context */
   const err_type *DataaoNandBufWriteFail;                             /* TLM packet writing to NAND internal buffer failed trnasmissions, the error data is the current NAND address */
   const err_type *DataaoNandEraseFail;                                /* Command to initiate a NAND block erase failed, the error data is the current NAND address */
   const err_type *DataaoNandLoadFail;                                 /* Command to initiate loading of the TLM data from internal buffer to the cell failed, the error data is the current NAND address */
   const err_type *DataaoNandEraseFullTimeout;                         /* Could not complete the full erasure of NAND due to a timeout, the error data is the current NAND address */
   const err_type *DataaoNandReadFail;                                 /* Could not read from the NAND internal buffer, the error data is the current NAND address */
   const err_type *DataaoFramReadFail;                                 /* Could not read from FRAM, the error data is the current FRAM address, the error data is the current FRAM address */
   const err_type *DataaoFramMetadataWriteFail;                        /* Could not initiate FRAM metadata write due to FRAM being busy */
   const err_type *DataaoUpProgStagedCrcFail;                          /* Aborted SW Update due to an invalid CRC was computed for the staged image in FRAM, the error data is the computed CRC */
   const err_type *DataaoUpProgStagedCrcTimeout;                       /* Aborted SW Update due to a timeout that occurred during staged image computation, the error data is the current FRAM address */
   const err_type *DataaoUpProgFlashTimeout;                           /* Aborted SW Update due to timeout that occurred when reading the staged image during flash programming, the error data is the current FRAM read address */
   const err_type *DataaoUpProgFlashFail;                              /* Aborted SW Update due to a failure to program the flash, the error data is the current flash address */
   const err_type *DataaoUpProgFinalCrcFail;                           /* The final CRC validation of the image in flash failed, the error data is the calculated CRC value */
   const err_type *SpiAdcDmaRxOverrun;                                 /* The DMA ADC reception process attempted to read data from the SPI Rx that was already empty */
   const err_type *SpiAdcDmaTxUnderrun;                                /* The DMA ADC transmission process attempted to send data to the SPI Tx but it was not empty */
   const err_type *SpiAdcDmaError;                                     /* Generic DMA ADC error, error data is the SDK error that was reported */
   const err_type *PwmDriverInitFail;                                  /* Initialization of the PWM Drivers failed, likely a development error related to incorrect PWM usage or an invalid Processor Expert config, error data is the Flextimer PWM instance that failed */
   const err_type *RftaoInterqFull;                                    /* Cannot push another RFT Tx request because the FIFO queue is full, the number of entries in the queue is returned in error data */
   const err_type *RftaoInterqNoData;                                  /* Attempting to pop from the Rft Tx FIFO queue failed due to having invalid or nonexistent Tx request data, error data returned has both the request has_data flag and the data_size in the LSBs respectively. */
   const err_type *RftaoUartBusy;                                      /* The transfer cannot commence because the UART is currently busy. Error data returned has both the RFT Tx request type and msg ID in the LSBs respectively */
   const err_type *RftaoXferTimeout;                                   /* The RFT transfer timed out. Error data returned has both the RFT Tx request type and msg ID in the LSBs respectively */
   const err_type *RftaoXferInvalidPkt;                                /* The RFT Rx packet had an invalid clank header or CRC. Error data returned has both the RFT Tx request type and msg ID in the LSBs respectively */

   const err_type *McuNmiInterrupt;                                    /* MCU detected a Non-Maskable Interrupt Fault, error data is the excepting instruction address */
   const err_type *McuHardFaultInterrupt;                              /* MCU detected a Hard Fault, only faults when other exception handlers fault, error data is the excepting instruction address */
   const err_type *McuMemManageInterrupt;                              /* MCU detected a Memory Manager fault, implying a memory access voilation has occurred, error data is the excepting instruction address */
   const err_type *McuBusFaultInterrupt;                               /* MCU detected a bus fault such as errors from instruction fetches, data access, and memory accesses, error data is the excepting instruction address */
   const err_type *McuUsageFaultInterrupt;                             /* MCU detected a usage fault such as undefined instruction, invalid state, invalid interrupt return, divide by integer 0, error data is the excepting instruction address */
   const err_type *McuDebugMonInterrupt;                               /* MCU detected a debug monitor fault, error data is the excepting instruction address */
   const err_type *McuSvcInterrupt;                                    /* MCU detected a supervisor fault, error data is the excepting instruction address */
   const err_type *McuPendSvInterrupt;                                 /* MCU detected a pending supervisor fault, error data is the excepting instruction address */
   const err_type *McuSysTickInterrupt;                                /* MCU detected a system tick fault, error data is the excepting instruction address */
   const err_type *McuUnexpectedInterrupt;                             /* MCU detected an interrupt that should not be used or enabled, error data is the interrupt number that fired */
   const err_type *McuFlashNbitInterrupt;                              /* MCU detected a multi-bit ECC error in Program Flash, error data is the excepting instruction address */
   const err_type *McuFpuException;                                    /* MCU detected a floating point exception, error data is the excepting instruction address */
   const err_type *McuSramEccNbitInterrupt;                            /* MCU detected a multi-bit ECC error in SRAM, error data is the data address where it occurred */
   const err_type *McuPcuSramEcc1bitThresh;                            /* PCU MCU detected a number of 1bit errors in SRAM that is beyond a particular threshold, recommend power cycling */
   const err_type *McuRftSramEcc1bitThresh;                            /* RFT MCU detected a number of 1bit errors in SRAM that is beyond a particular threshold, recommend power cycling */
   const err_type *GladosErrorCritical;                                /* GladOS critical error was detected */
   const err_type *GladosErrorNoncritical;                             /* GladOS noncritical error was detected */
   const err_type *IntermediateTxQueueFull;                            /* The intermediate queue for the TxAo is full. (TxAO 241) */
   const err_type *WatchdogTrip;                                       /* The external watchdog indicates that it has reset the MCU, error data is the MCU RCM value */
   const err_type *McuUnexpectedReset;                                 /* The Reset Control register reports an unexpected reset event. Expected events include: Normal POR (0x82) and Debug Reset (0x420). Error data is the MCU RCM value. */

   const err_type *EcEbusHigh;                                         /* The bus voltage is over a specified amount in FIDR */
   const err_type *EcEbusLow;                                          /* The bus voltage is under a specified amount in FIDR */
   const err_type *EcIbusHigh;                                         /* The bus current is over a specified amount in FIDR */
   const err_type *EcE12vHigh;                                         /* The 12 volt rail voltage is over a specified amount in FIDR */
   const err_type *EcE12vLow;                                          /* The 12 volt rail voltage under a specified amount in FIDR */
   const err_type *EcE5vHigh;                                          /* The 5 volt rail voltage is over a specified amount in FIDR */
   const err_type *EcE5vLow;                                           /* The 5 volt rail voltage is under a specified amount in FIDR */
   const err_type *EcFlowLow;                                          /* Unexpected loss of propellant flow  */
   const err_type *EcHeaterActive;                                     /* The heater is drawing current in a non-heating state */
   const err_type *EcHeaterFuse;                                       /* The heater efuse tripped */
   const err_type *EcHpisoFuse;                                        /* The High Pressure Isolation Valve PTC fuse tripped */
   const err_type *EcPfcvFuse;                                         /* The Proportional Flow Control Valve PTC fuse tripped */
   const err_type *EcRftFuse;                                          /* The RFT efuse tripped */
   const err_type *EcNoTcComm;                                         /* Loss of communication with the Thruster Controller */
   const err_type *EcNoDaqComm;                                        /* Loss of communication with the ADCs */
   const err_type *TcE5vHigh;                                          /* The Thruster Controller 5 volt rail voltage is high */
   const err_type *TcE5vLow;                                           /* The Thruster Controller 5 volt rail voltage is low */
   const err_type *TcIhvdLow;                                          /* Power dropout detected on the inverter */
   const err_type *TcTfbInHigh;                                        /* The Thruster Controller input flyback temperature is low */
   const err_type *TcTfbOutHigh;                                       /* The Thruster Controller output flyback temperature is high */
   const err_type *TcTplHigh;                                          /* Plasma Liner temperature is high */
   const err_type *TcVsetLow;                                          /* Voltage sag detected on the Push Pull */
   const err_type *TcIhvdHigh;                                         /* High power draw detected on the inverter */
   const err_type *EcTcAborted;                                        /* Thr Ctrl safed itself unexpectedly */
   const err_type *EcFlowHigh;                                         /* Mass flow reading is high */
   const err_type *TcE12vHigh;                                         /* The Thruster Controller 12 volt rail voltage is high */
   const err_type *TcE12vLow;                                          /* The Thruster Controller 12 volt rail voltage low */
   const err_type *TcTfetHigh;                                         /* The FET temperature is high */
   const err_type *TcTcoilHigh;                                        /* The coil temperature is high */
   const err_type *TcTchokeHigh;                                       /* The choke temperature is high */
   const err_type *TcTmatchHigh;                                       /* The matching network temperature is high */
   const err_type *EcPpropHigh;                                        /* Propellant pressure is too high */

   const err_type *ThrustHpisoSpikeFail;                               /* Commanding HPISO to spike failed, error data is the value attempting to be set */
   const err_type *ThrustHpisoHoldFail;                                /* Commanding HPISO to hold failed, error data is the value attempting to be set */
   const err_type *ThrustRftWfGenFreqFail;                             /* Commanding the RFT waveform generator frequency failed, the reported value by the RFT is not within 1% of the set value, error data is the value being read from the RFT */
   const err_type *ThrustInaccurateFlow;                               /* The thrust ignition flow setpoint is outside of the defined tolerance, the error data isthe current flow value */
   const err_type *ThrustFlowUnstable;                                 /* The thrust flow control is unstable, the error data is the maximum difference in IPFCV current witnessed over the configured time period */
   const err_type *ThrustPushPullEnableFail;                           /* Commanding the RFT to enable the Push Pull failed, the error data is the status of the PP GPIO pin reported by the RFT */
   const err_type *ThrustWaveformEnableFail;                           /* Commanding the RFT to enable the Waveform Generator failed, the error data is the status of the WF GPIO pin reported by the RFT */
   const err_type *ThrustIgnitionFail;                                 /* Failed to ignite, the error data is the IHVD level that was detected */
   const err_type *ThrustRftCommFail;                                  /* Thrust sequence command to the RFT failed, the error data is the step of the thrust sequence that failed */
   const err_type *ThrustRftIgnitePpVsetCmdFail;                       /* Checking the pp_vset_fb telemetery to make sure the thruster controller got the push pull vset cmd and set to the proper set point by +/- 5 V prior to ignition */
   const err_type *ThrustRftIgnitePpVsetFail;                          /* Checking the EVB telemetery to make sure the ignition push pull vset is set to the proper set point by +/- 5 V during ignition */
   const err_type *ThrustRftPushPullFinalVsetFail;                     /* Checking the EVB telemetery to make sure the final push pull vset is set to the proper set point by +/- 5 V after ramping */

   const err_type *BistTimeout;                                        /*  */
   const err_type *BistHeaterFail;                                     /*  */
   const err_type *BistInternEcFail;                                   /*  */
   const err_type *BistInternTcFail;                                   /*  */
   const err_type *BistMemoryFail;                                     /*  */
   const err_type *BistPushPullFail;                                   /*  */
   const err_type *BistWaveformFail;                                   /*  */
   const err_type *BistFluidicsFail;                                   /*  */
   const err_type *BistLpFail;                                         /*  */
} err_record;

extern const err_record Errors;
extern const Cmd_Err_Code_t ERROR_NONE;

Cmd_Err_Code_t create_error_uint(const err_type *error, uint32_t aux_data);
Cmd_Err_Code_t create_error_float(const err_type *error, float aux_data);


#endif /* SOURCE_P4_ERR_H_ */
