/* Copyright (c) 2017-2020 by Phase Four, Inc. All Rights Reserved. Confidential and Proprietary.
 *
 * rft_data.h
 */

#ifndef SOURCE_RFT_DATA_H_
#define SOURCE_RFT_DATA_H_

#include "mcu_types.h"
#include "max_data.h"

typedef struct RftData_tag
{
    uint16_t sc_err1_code;
    uint16_t sc_err2_code;
    uint16_t sc_err3_code;
    uint16_t powerup_err_code;
    uint16_t err1_code;
    uint32_t err1_data;
    uint16_t err2_code;
    uint32_t err2_data;
    uint16_t err3_code;
    uint32_t err3_data;
    uint16_t err_cnt;

    /* Holds the current unix time, 0 at reset until new unix time is received */
    uint32_t unix_time_us_hi;
    uint32_t unix_time_us_lo;

    /* Holds the timestamp of the last received time sync value,
     * used to calculate the current unix time since the last commanded value */
    uint32_t time_sync_lpit_hi;           /* 0xFFFFFFFF at startup */
    uint32_t time_sync_lpit_lo;           /* 0xFFFFFFFF at startup */
    uint32_t last_unix_time_sync_us_hi;
    uint32_t last_unix_time_sync_us_lo;

    MaxState_t state;
    uint16_t ack_status;
    uint16_t cmd_acpt_cnt;
    uint16_t cmd_rjct_cnt;

    uint8_t  bl_select;
    uint8_t  app_select;
    uint8_t  pt_select;

    uint32_t bl_crc;
    uint32_t app_crc;
    uint32_t pt_crc;

    uint32_t rcm_value;
    bool     wdt_rst_trig;
    bool     wdt_rst_tog_sel;
    bool     gpio_wdi_fb;
    uint8_t  fsw_cpu_util;

    DataMode_t data_mode;

    /* External ADC 0 Signals */
    float tfet;
    float tchoke;
    float tcoil;
    float tic;
    float tpl;
    float tmatch;
    float ehvd;
    float ihvd;
    float tadc0;

    /* Internal ADC Signals */
    float ebus;
    float e12v;
    float e5v;
    float evb;
    float ibus;
    float i12v;
    float ivb;
    float tfb_in;
    float tfb_out;
    float tfram;
    float tmcu;

    uint16_t pwm_pp_vset_raw_fb;
    float pp_vset_fb;
    float wav_gen_freq_fb;

    uint32_t ecc_sram_1bit_cnt;
    bool needs_reset_flag;

    bool gpio_wf_en_fb;
    bool gpio_pp_en_fb;

    bool tc_busy;

    bool using_runtime_pt;
    bool using_default_pt;
    bool using_app_pt;

    bool inverter_connected_flag;
    bool rft_connected_flag;
    bool hp_connected_flag;

    /* Voltage bias power, not in Thr Ctrl TLM but derived from it */
    float pvb;

} TcData_t;

extern TcData_t TcData;

#endif /* SOURCE_RFT_DATA_H_ */
