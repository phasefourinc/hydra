/* Copyright (c) 2017-2020 by Phase Four, Inc. All Rights Reserved. Confidential and Proprietary.
 *
 * p4_tlm.h
 */

#ifndef SOURCE_P4_TLM_H_
#define SOURCE_P4_TLM_H_

#include "mcu_types.h"


typedef struct Err_Rec_tag
{
    uint16_t err_code;
    uint32_t err_data;
} BYTE_PACKED Err_Rec_t;

typedef struct Tlm_Err_Ec_Rec_tag
{
    uint16_t powerup_err_code;
    Err_Rec_t err1;
    Err_Rec_t err2;
    Err_Rec_t err3;
    uint16_t total_err_cnt;
} BYTE_PACKED Tlm_Err_Ec_Rec_t;


typedef struct Tlm_Err_Tc_Rec_tag
{
    uint16_t powerup_err_code;
    Err_Rec_t err1;
    Err_Rec_t err2;
    Err_Rec_t err3;
    uint16_t total_err_cnt;
} BYTE_PACKED Tlm_Err_Tc_Rec_t;





typedef struct P4Tlm_EcStatus_tag
{
    uint16_t ec_ack_status;             /* Command status, 0=SUCCESS, error code otherwise */
    uint16_t sc_ec_err1_code;           /* Holds the first detected error code, which persists until cleared. A value of 0 indicates no error. */
    uint16_t sc_ec_err2_code;           /* The second thruster error code. A value of 0 indicates no error. This error is part of a shift buffer and stores the 2nd to last (2nd most recent) error */
    uint16_t sc_ec_err3_code;           /* The third thruster error code. A value of 0 indicates no error. This error is part of a shift buffer and stores the last (most recent) error. */
    uint8_t ec_state;                   /* Maxwell State: 1=LOW_PWR, 2=IDLE, 4=THRUST, 7=MANUAL, 8=SW Loading, 13=WILD_WEST, 15=SAFE */
    uint8_t ec_busy_flag;               /* Flag indicating if Maxwell is currently busy, such as heating, SW updating, thrusting: 1 - Busy, 0 - Not busy */
    uint8_t ec_needs_reset;             /* Indicates that the thruster should be power cycled: 0 - no reset necessary, 1 - reset recommended */
    uint8_t reserved0[1];
    uint8_t reserved1[4];
    uint32_t sc_tprop;                  /* The tank temperature in degC */
    uint32_t sc_pprop;                  /* The tank pressure in PSIA */
    uint32_t sc_plowp;                  /* The pressure in PSIA of the low pressure transducer downstream of the tank but upstream of the flow controller. */
    uint32_t sc_mfs_mdot;               /* The measured propellant mass flow rate in SCCM */
    uint32_t sc_ehvd;                   /* The measured High Voltage Detect RF voltage in Volts used to bias the inverter for thrust operations */
    uint32_t sc_ihvd;                   /* The measured High Voltage Detect current in Amps */
    uint32_t sc_tliner;                 /* Temperature of the plasma liner in degC */
    uint32_t sc_tfet;                   /* Temperature of the inverter FET in degC */
    uint32_t ec_thrust_rf_vset_fb;      /* The feedback setting validating what was set in the last CONFIG_THRUST command in Volts */
    uint32_t ec_thrust_mdot_fb;         /* The feedback setting validating what was set in the last CONFIG_THRUST command in SCCM */
    uint32_t ec_thrust_dur_sec_fb;      /* The feedback setting validating what was set in the last CONFIG_THRUST command in seconds */
    uint32_t sc_tcoil;
    uint8_t reserved2[1];
    uint16_t sc_sn;                     /* Unit's serial number (Annnnn), is 0 if unset */
    uint8_t ec_app_select;              /* Indicates the loaded Application image. This value is loaded once at startup. 1 - Primary, 2 - Secondary, 3 - Golden */
    uint16_t ec_acpt_cnt;               /* The current command accept counter. Does not increment for GET_STATUS and GET_TLM commands */
    uint16_t ec_rjct_cnt;               /* The current command reject counter */
    uint32_t ec_unix_time_us_hi;        /* Holds the current tracked unix time in microseconds. Otherwise it holds the current up-time in microseconds */
    uint32_t ec_unix_time_us_lo;        /* Holds the current tracked unix time in microseconds. Otherwise it holds the current up-time in microseconds */

} BYTE_PACKED P4Tlm_EcStatus_t;

typedef struct P4Tlm_EcTlmtag
{
    P4Tlm_EcStatus_t ec_status;

    uint32_t ec_e3v3;
    uint32_t ec_ipfcv;
    uint32_t ec_ibus;
    uint32_t ec_ebus;
    uint32_t ec_ihpiso;
    uint32_t ec_pprop;
    uint32_t ec_irft;
    uint32_t ec_e12v;
    uint32_t ec_tadc0;
    uint32_t ec_tprop;
    uint32_t ec_iheater;
    uint32_t ec_plowp;
    uint32_t ec_e5v;
    uint32_t ec_e2v5;
    uint32_t ec_mfs_mdot;               /* Flow rate (unfiltered) determined by a function of current flow setpoint, MFS voltage reading, and ec_tmfs */
    uint32_t ec_tmfs;                   /* Temperature measured near the mass flow sensor */
    uint32_t ec_tadc1;
    uint8_t ec_rft_fault;               /* GPIO input pin indicating the state of the RFT resettable fuse: 1 - in fault, 0 - no fault */
    uint8_t ec_heater_fault;            /* GPIO input pin indicating the state of the Heater resettable fuse: 1 - in fault, 0 - no fault */
    uint8_t ec_hpiso_fault;             /* GPIO input pin indicating the state of the High Pressure Iso Valve CC driver resettable fuse: 1 - in fault, 0 - no fault */
    uint8_t ec_pfcv_fault;              /* GPIO input pin indicating the state of the PFCV CC driver resettable fuse: 1 - in fault, 0 - no fault */
    uint8_t ec_bl_select;               /* Bootloader Select: 1 - Primary, 2 - Secondary */
    uint8_t ec_pt_select;               /* Parameter Table Select: 1 - Run-Time Table, 2 - App Table, 3 - Default Table, 0xF1 - SUS Runtime PT, 0xF2 - SUS App PT, 0xF3 - SUS Default PT */
    uint32_t ec_bl_crc;                 /* CRC of the selected Bootloader */
    uint32_t ec_app_crc;                /* CRC of the selected Application */
    uint32_t ec_pt_crc;                 /* CRC of the selected PT, not applicable for Default Table */

    Tlm_Err_Ec_Rec_t tlm_err_rec;

    uint8_t ec_heat_ctrl_en_fb;         /* Software heater control enable feedback: 1 - enabled, 0 - disabled */
    uint8_t ec_heater_en_fb;            /* Heater CC driver enable feedback: 1 - enabled, 0 - disabled */
    uint8_t ec_hpiso_en_fb;             /* High Pressure Isolation Valve CC driver enable feedback: 1 - enabled, 0 - disabled */
    uint8_t ec_flow_ctrl_en_fb;         /* Software flow control enable feedback: 1 - enabled, 0 - disabled */
    uint32_t ec_heat_ctrl_temp_fb;      /* Heater control temperature setting feedback in degC */
    uint32_t ec_heater_setpt_ma_fb;     /* Heater CC driver current setting in milliamperes feedback */
    uint32_t ec_hpiso_setpt_ma_fb;      /* High Pressure Isolation Valve CC driver current setting in milliamperes feedback */
    uint32_t ec_flow_ctrl_mdot_fb;      /* Software flow control desired setpoint feedback in SCCM */
    uint32_t ec_desired_mdot_v_fb;      /* Flow control desired voltage target in volts */
    uint32_t ec_pfcv_ma_fb;             /* Proportional Flow Control Valve CC driver current setting in milliamperes feedback */
    uint32_t ec_rcm_value;              /* Reset Control Module Register Value at startup, determines reset cause */
    uint8_t ec_wdt_rst_trig;            /* GPIO input pin determines if the WDT was triggered */
    uint8_t ec_wdt_toggle_sel;          /* GPIO input pin that determines which Bootloader to select */
    uint8_t ec_data_mode;               /* FSW mode for data operations: 0 - Lounge/Idle, 3 -Recording, 5 - Erasing Nand Flash, 6 - Dumping MCU data, 9 - Dumping TLM data, 10 - SW update CMD fail, 12 - Ready for SW update CMDs, 13 - SW update clear flash step, 14 - SW update program flash step, 15 - SW update successful */
    uint8_t ec_rec_nand_en_fb;          /* NAND Flash long-term storage recording enable feedback: 1 - enabled, 0 - disabled */
    uint32_t ec_rec_nand_tlm_cnt;       /* Total number of recorded TLM packets (1 PCU + 1 Thr) stored to NAND Flash long-term storage in bank 0 */
    uint16_t ec_pprop_raw;              /* pprop (HPD_VOUT) ADC raw count reading */
    uint16_t ec_tprop_raw;              /* tprop (TANK_ISENSE) ADC raw count reading */
    uint16_t ec_plowp_raw;              /* plowp (LPD_VOUT) ADC raw count reading */
    uint16_t ec_mfs_mdot_raw;           /* mfs_mdot (MFS_VOUT) ADC raw count reading */
    uint16_t ec_tmfs_raw;               /* tmfs (RTD2_VOUT) ADC raw count reading */
    uint32_t ec_ecc_sram_1bit_cnt;      /* Number of 1bit SRAM errors */
    uint16_t ec_tc_comm_fail_cnt;       /* Cumulative number of failed comms with the Thr Ctrl */
    uint8_t ec_fsw_cpu_util;            /* Prop Ctrl FSW CPU utilization percentage */

    uint8_t ec_heater_connected;        /* Flag for heater connection */
    uint8_t ec_mfs_connected;           /* Flag for MFS connection */
    uint8_t ec_tc_connected;            /* Flag for Thr Ctrl connection */

} BYTE_PACKED P4Tlm_EcTlm_t;


typedef struct P4Tlm_TcStatus_tag
{
    uint16_t tc_ack_status;
    uint16_t sc_tc_err1_code;
    uint16_t sc_tc_err2_code;
    uint16_t sc_tc_err3_code;
    uint8_t tc_state;
    uint8_t tc_busy;
    uint8_t tc_needs_reset;
    uint8_t tc_app_select;
    uint16_t tc_acpt_cnt;
    uint16_t tc_rjct_cnt;
    uint32_t tc_unix_time_us_hi;
    uint32_t tc_unix_time_us_lo;

} BYTE_PACKED P4Tlm_TcStatus_t;


typedef struct P4Tlm_TcTlm_tag
{
    P4Tlm_TcStatus_t tc_status;

    Tlm_Err_Tc_Rec_t tlm_err_rec;

    uint8_t  tc_bl_select;
    uint8_t  tc_pt_select;

    uint32_t tc_bl_crc;
    uint32_t tc_app_crc;
    uint32_t tc_pt_crc;

    uint32_t tc_rcm_value;
    uint8_t  tc_wdt_rst_trig;
    uint8_t  tc_wdt_rst_tog_sel;
    uint8_t  tc_wdi_fb;
    uint8_t  tc_fsw_cpu_util;

    /* External ADC 0 Signals */
    uint32_t tc_tfet;
    uint32_t tc_tchoke;
    uint32_t tc_tcoil;
    uint32_t tc_tic;
    uint32_t tc_tpl;
    uint32_t tc_tmatch;
    uint32_t tc_ehvd;
    uint32_t tc_ihvd;
    uint32_t tc_tadc0;

    /* Internal ADC Signals */
    uint32_t tc_ebus;
    uint32_t tc_e12v;
    uint32_t tc_e5v;
    uint32_t tc_evb;
    uint32_t tc_ibus;
    uint32_t tc_i12v;
    uint32_t tc_ivb;
    uint32_t tc_tfb_in;
    uint32_t tc_tfb_out;
    uint32_t tc_tfram;
    uint32_t tc_tmcu;

    uint16_t tc_tfet_raw;
    uint16_t tc_tchoke_raw;
    uint16_t tc_tcoil_raw;
    uint16_t tc_tic_raw;
    uint16_t tc_tpl_raw;
    uint16_t tc_tmatch_raw;
    uint16_t tc_ivb_raw;
    uint16_t tc_tfb_in_raw;
    uint16_t tc_tfb_out_raw;
    uint16_t tc_tfram_raw;
    uint16_t tc_tmcu_raw;

    uint16_t tc_pwm_pp_vset_raw_fb;
    uint32_t tc_pp_vset_fb;
    uint32_t tc_wav_gen_freq_fb;
    uint32_t tc_ecc_sram_1bit_cnt;

    uint8_t  tc_data_mode;
    uint8_t  tc_wf_en_fb;
    uint8_t  tc_pp_en_fb;

    uint8_t  tc_inv_connected;
    uint8_t  tc_rft_connected;
    uint8_t  tc_hp_connected;

} BYTE_PACKED P4Tlm_TcTlm_t;


#define P4TLM_EC_STATUS_PAYLOAD_SIZE    sizeof(P4Tlm_EcStatus_t)
#define P4TLM_EC_TLM_PAYLOAD_SIZE       sizeof(P4Tlm_PcuTlm_t)
#define P4TLM_TC_STATUS_PAYLOAD_SIZE     sizeof(P4Tlm_TcStatus_t)
#define P4TLM_TC_TLM_PAYLOAD_SIZE        sizeof(P4Tlm_TcTlm_t)

#endif /* SOURCE_P4_TLM_H_ */
