/* Copyright (c) 2017-2020 by Phase Four, Inc. All Rights Reserved. Confidential and Proprietary.
 *
 * main.c
 */
/*!
** @file main.c
** @brief
**         Main module.
**         This module contains user's application code.
*/
/*!
**  @addtogroup main_module main module documentation
**  @{
*/

/* Future enhancements:
 *  - Utilize low voltage detection features via PMC_LVDSC1[LVDRE]
 */

#include "mcu_types.h"
#include "max_data.h"
#include "glados.h"
#include "timer64.h"
#include "xdata.h"
#include "clock.h"
#include "gpio.h"
#include "Cpu.h"
#include "MCUflash.h"
#include "pt_load.h"
#include "exceptions.h"
#include "max_info_driver.h"
#include "scrubber.h"

#include "MaxwellAO.h"
#include "ErrAO.h"
#include "DaqCtlAO.h"
#include "RftAO.h"
#include "TlmAO.h"
#include "TxAO.h"
#include "DataAO.h"
#include "CmdAO.h"
#include "RxScAO.h"
#include "RxGseAO.h"
#include "WdtAO.h"


/* List must be in order, from highest priority to lowest.  Kept on the heap
 * to avoid any possibility of stack corruption */
GLADOS_AO_t* ao_list[] =
{
    &AO_Maxwell,
    &AO_Err,
    &AO_DaqCtl,
    &AO_Rft,
    &AO_Tlm,
    &AO_Tx,
    &AO_Data,
    &AO_Cmd,
    &AO_RxSc,
    &AO_RxGse,
    &AO_Wdt
};

/* Timer order is not significant. Kept on the heap to avoid
 * any possibility of stack corruption */
GLADOS_Timer_t* timer_list[] =
{
    &Tmr_MaxSafeSeq,
    &Tmr_MaxThrustSeq,
    &Tmr_MaxThrustTimeout,
    &Tmr_DaqStart,
    &Tmr_DaqRftTimeout,
    &Tmr_RftRqstTimeout,
    &Tmr_TlmAuto,
    &Tmr_TlmTimeout,
    &Tmr_TxTimeout,
    &Tmr_DataAux,
    &Tmr_DataTimeout,
    &Tmr_CmdTimeout,
    &Tmr_CmdCustomAppTimer,
    &Tmr_RxScTimeout,
    &Tmr_RxScCmdTimeout,
    &Tmr_RxScDropoutDelay,
    &Tmr_RxGseTimeout,
    &Tmr_RxGseCmdTimeout,
    &Tmr_RxGseDropoutDelay,
    &Tmr_4HzPetWtd,
    &Tmr_WtdAux,
    &Tmr_errAOTimeout,
    &Tmr_errAOFidr,
#ifdef DUMMY_LOAD_STUBS
    &Tmr_MaxStub1sec,
#endif
};


static void Board_Init(void);
static void Data_Init(void);
static void ActiveObjects_Init(void);
static void Startup_Checks(void);
static void PushInterruptEvents(void);
static void IdleFrame(void);


int main(void)
{
    bool event_processed = false;
    uint32_t curr_tick_upper = 0UL;
    uint32_t curr_tick_lower = 0UL;
    uint32_t idle_tick_start = 0UL;
    uint32_t last_idle_tick  = 0UL;
    uint32_t cpu_util_start_tick = 0UL;
    uint32_t cpu_util_dur_ticks  = 0UL;
    uint32_t cpu_util_idle_ticks = 0UL;

    Board_Init();

    Data_Init();

    /* Note: DO NOT ENABLE Double-bit Flash Fault's until after the exception
     * handlers have been loaded and the PTs are loaded or else there's a
     * chance that multi-bit errors could prevent our PT loading scheme from
     * loading anything. */
    pt_load_init();

    /* Initialize MCU-specific exceptions, which must happen
     * after Flash init and PT init */
    Exceptions_Init();

    /* Erase the app selection flash. We need to get rid of this so the user specifies
     * app they want to select each time when booting. To be safe, delay for the
     * maximum duration needed to erase a PFlash sector.
     * Note: Safest to do this step after PT loading, which accesses PFlash */
    erase_app_selection_sector_startup();
    timer32_delay_ms(130UL);

    /* Initialize GladOS and Active Object constructors */
    GLADOS_Init();
    ActiveObjects_Init();

    Startup_Checks();

    Scrub_Init();

    while (1)
    {
        /* Poll and post any interrupt events, this allows the OS to
         * operate within the single, main thread.
         * Note: Having the interrupt events prior to the timers are processed
         * adds a race condition associated with a transfer completion
         * immediately prior to a timeout. AOs must handle this case if there
         * is reuse of the same timer in different states. The reason this race
         * condition is left in is to allow for a "tie goes to the runner"
         * since other higher priority AOs may add delay that may end up timing
         * out when the transfer already completed milliseconds before it. */
        PushInterruptEvents();

        /* Get and Update the current time */
        timer64_get_tick(&curr_tick_upper, &curr_tick_lower);
        max_data_unix_time_update(curr_tick_upper, curr_tick_lower);

        /* Check and process timer events that may have been triggered */
        GLADOS_Process_Timers(timer_list, sizeof(timer_list)/sizeof(timer_list[0]), curr_tick_lower);

        /* Check and process Active Object events */
        event_processed = GLADOS_Process_AOs(ao_list, sizeof(ao_list)/sizeof(ao_list[0]));

        /* If no Active Object events were processed, perform an Idle frame */
        if (!event_processed)
        {
            idle_tick_start = timer32_get_tick();
            IdleFrame();
            XTIME_UPDATE(&Xdata.os_idle_xtime, idle_tick_start);

            /* Calculate and store the full utilization time, which is the time
             * since the end of the last Idle frame */
            XTIME_UPDATE(&Xdata.os_full_util_xtime, last_idle_tick);
            last_idle_tick = timer32_get_tick();

            /* Accumulate the total ticks that occur while idle-ing for CPU utilization calc */
            cpu_util_idle_ticks += timer32_get_duration_ticks(curr_tick_lower, last_idle_tick);
        }

        /* Perform CPU Utilization calculation if an arbitrary number of ticks have elapsed */
        cpu_util_dur_ticks = timer32_get_duration_ticks(cpu_util_start_tick, curr_tick_lower);
        if (cpu_util_dur_ticks > 5000000UL)
        {
            /* Perform the calculation by finding the ratio of MCU ticks spent idle-ing
             * and the total number of ticks that have elapsed.
             *
             * MCU Utilization Percent = 100 * (1 - (idle ticks / total cpu ticks))
             */
            MaxData.fsw_cpu_util = (uint8_t)((100UL * (cpu_util_dur_ticks - cpu_util_idle_ticks)) / cpu_util_dur_ticks);

            /* Set the new start tick and reset the number of idle ticks */
            cpu_util_start_tick = curr_tick_lower;
            cpu_util_idle_ticks = 0UL;
        }

        /* Record the Min/Max execution time for the OS frame */
        XTIME_UPDATE(&Xdata.os_frame_xtime, curr_tick_lower);
    }

    return 0;
}


static void Board_Init(void)
{
    status_t status = STATUS_SUCCESS;

    /* Bootloader has set each SRAM_L location with ECC write generation enabled,
     * now it is time to enable ECC Read checking as well upon immediate entry into
     * the application. Do for SRAM_L and SRAM_U. */
    MCM->LMDR[0] = MCM_LMDR_CF0(3UL);
    MCM->LMDR[1] = MCM_LMDR_CF0(3UL);

    /* Ensure we avoid errata e9005 */
    S32_SCB->ACTLR |= S32_SCB_ACTLR_DISDEFWBUF_MASK;

    /* Initialize the clock manager, which always returns STATUS_SUCCESS */
    status = CLOCK_SYS_Init(g_clockManConfigsArr, CLOCK_MANAGER_CONFIG_CNT, g_clockManCallbacksArr, CLOCK_MANAGER_CALLBACK_CNT);
    DEV_ASSERT(status == STATUS_SUCCESS);

    /* Update the clock configuration, failures are development related so an assert suffices */
    status = CLOCK_SYS_UpdateConfiguration(0U, CLOCK_MANAGER_POLICY_AGREEMENT);
    DEV_ASSERT(status == STATUS_SUCCESS);

    /* Initialize the MCU pins and muxing, which always returns STATUS_SUCCESS */
    status = PINS_DRV_Init(NUM_OF_CONFIGURED_PINS, g_pin_mux_InitConfigArr);
    DEV_ASSERT(status == STATUS_SUCCESS);

    /* Initialize the PIT used for overall firmware timing */
    timer64_init();

    /* Set Initial active low GPIOs */
    GPIO_SetOutput(GPIO_WDT_TRIG_nCLR_EN, 1U);
    GPIO_SetOutput(GPIO_RFT_nRST_EN, 1U);
    GPIO_SetOutput(GPIO_HEATER_nRST_EN, 1U);
    GPIO_SetOutput(GPIO_PFCV_DAC_nRST_EN, 1U);

    /* Perform first WDT pet. Assert that the WDT pet output
     * pin is low just in case it was not initialized that
     * way by the pins driver.
	 * Note: POR to first WDT pet is 170ms nominally */
    GPIO_SetOutput(GPIO_WDT_PET, 0);
    timer32_delay_us(1U);
    GPIO_PetWDT();

    /* Hold the RFT efuse in reset to force it to hold a "short",
     * just long enough to allow for the the Thr Ctrl to be powered on.
     * This is a SW workaround for the large inrush current the boards
     * have, which tricks the RFT efuse into thinking it is a short. */
    GPIO_SetOutput(GPIO_RFT_nRST_EN, false);
    timer32_delay_us(2000UL);
    GPIO_SetOutput(GPIO_RFT_nRST_EN, true);

    /* Initialize eDMA driver, which always returns STATUS_SUCCESS */
    status = EDMA_DRV_Init(&dmaController0_State, &dmaController0_InitConfig0, edmaChnStateArray, edmaChnConfigArray, EDMA_CONFIGURED_CHANNELS_COUNT);
    DEV_ASSERT(status == STATUS_SUCCESS);

    /* Note: If debugging DMA, you may set the following register to
     * halt DMA in debug and halt on error
     * DMA->CR |= (DMA_CR_EDBG_MASK | DMA_CR_HOE_MASK) */

    /* Initialize the LPUART drivers, which always returns STATUS_SUCCESS */
    status = LPUART_DRV_Init(INST_LPUART0, &lpuart0_State, &lpuart0_InitConfig0);
    DEV_ASSERT(status == STATUS_SUCCESS);
    status = LPUART_DRV_Init(INST_LPUART1, &lpuart1_State, &lpuart1_InitConfig0);
    DEV_ASSERT(status == STATUS_SUCCESS);
    status = LPUART_DRV_Init(INST_LPUART2, &lpuart2_State, &lpuart2_InitConfig0);
    DEV_ASSERT(status == STATUS_SUCCESS);

    /* The performance boost using flash look-aheads does not help us since the
     * App runs out of SRAM and is probably safer to just have disabled */
    MSCM->OCMDR[0] |= MSCM_OCMDR_OCM1(0x3U);
    MSCM->OCMDR[1] |= MSCM_OCMDR_OCM1(0x3U);

    /* Initialize Program Flash, failures are development related so an assert suffices */
    status = initialize_flash();
    DEV_ASSERT(status == STATUS_SUCCESS);

    /* SPI initialization failures can error if the LPSPI module indicates it is busy
     * via the SR[MBF] flag, which indicates that the SPI module has been initialized
     * and is active. Analysis indicates that this error will never arise out of an MCU
     * cold start since the SR[MBF] flag has a reset value of 0 (not busy). As long as
     * this init routine is executed only once per LPSPI module, then only development
     * related errors can arise and assert checks suffice. */

    /* Initialize NAND Flash SPI bus
     * Manually set the callback parameter since Processor Expert doesn't allow us to there
     * NOTE: 512B blocking SPI transfers require clock >5MHz for hard-coded timeouts */
    DEV_ASSERT(lpspiCom0_MasterConfig0.bitsPerSec >= 5000000UL);
    lpspiCom0_MasterConfig0.callbackParam = (void *)&spi0_callback_send_dma_done;
    status = LPSPI_DRV_MasterInit(LPSPICOM0, &lpspiCom0State, &lpspiCom0_MasterConfig0);
    DEV_ASSERT(status == STATUS_SUCCESS);

    /* Currently if ADC SPI xfer is halted for debugging it
     * leads to a SPI error. Setting CR[DBGEN] prevents these errors. */
    LPSPI0->CR |= LPSPI_CR_DBGEN_MASK;

    /* Initialize all CS pins to active low */
    status = LPSPI_DRV_SetPcs(LPSPICOM0, LPSPI_PCS0, LPSPI_ACTIVE_LOW);
    DEV_ASSERT(status == STATUS_SUCCESS);

    /* Initialize FRAM SPI bus
     * Manually set the callback parameter since Processor Expert doesn't allow us to there
     * NOTE: 512B blocking SPI transfers require clock >5MHz for hard-coded timeouts */
    DEV_ASSERT(lpspiCom1_MasterConfig0.bitsPerSec >= 5000000UL);
    lpspiCom1_MasterConfig0.callbackParam = (void *)&spi1_callback_send_dma_done;
    status = LPSPI_DRV_MasterInit(LPSPICOM1, &lpspiCom1State, &lpspiCom1_MasterConfig0);
    DEV_ASSERT(status == STATUS_SUCCESS);

    /* Currently if ADC SPI xfer is halted for debugging it
     * leads to a SPI error. Setting CR[DBGEN] prevents these errors. */
    LPSPI1->CR |= LPSPI_CR_DBGEN_MASK;

    /* Initialize all CS pins to active low */
    status = LPSPI_DRV_SetPcs(LPSPICOM1, LPSPI_PCS0, LPSPI_ACTIVE_LOW);
    DEV_ASSERT(status == STATUS_SUCCESS);

    /* Initialize ADC SPI bus */
    status = LPSPI_DRV_MasterInit(LPSPICOM2, &lpspiCom2State, &lpspiCom2_MasterConfig0);
    DEV_ASSERT(status == STATUS_SUCCESS);

    /* Currently if ADC SPI xfer is halted for debugging it
     * leads to a SPI error. Setting CR[DBGEN] prevents these errors. */
    LPSPI2->CR |= LPSPI_CR_DBGEN_MASK;

    /* 5us delay between transfers is sufficient since it's greater than Tconv (4us) */
    status = LPSPI_DRV_MasterSetDelay(LPSPICOM2, 100UL, 1UL, 1UL);
    DEV_ASSERT(status == STATUS_SUCCESS);

    /* Initialize all CS pins to active low */
    status = LPSPI_DRV_SetPcs(LPSPICOM2, LPSPI_PCS1, LPSPI_ACTIVE_LOW);
    DEV_ASSERT(status == STATUS_SUCCESS);
    status = LPSPI_DRV_SetPcs(LPSPICOM2, LPSPI_PCS0, LPSPI_ACTIVE_LOW);
    DEV_ASSERT(status == STATUS_SUCCESS);

    (void)status;  /* Removes unused compiler warning */

    return;
}


static void Data_Init(void)
{
    max_data_init();

    return;
}


static void ActiveObjects_Init(void)
{
    MaxAO_init();
    ErrAO_init();
    DaqCtlAO_init();
    RftAO_init();
    TlmAO_init();
    TxAO_init();
    DataAO_init();
    CmdAO_init();
    RxScAO_init();
    RxGseAO_init();
    WdtAO_init();

    /* Enter the initial states for each Active Object */
    GLADOS_Initialize_AOs(ao_list, sizeof(ao_list)/sizeof(ao_list[0]));

    return;
}


static void PushInterruptEvents(void)
{
    Exceptions_PushInterrupts();
    DaqCtlAO_push_interrupts();
    RftAO_push_interrupts();
    TxAO_push_interrupts();
    DataAO_push_interrupts();
    RxScAO_push_interrupts();
    RxGseAO_push_interrupts();
    return;
}


/* This function serves to report any detected startup errors
 * since this expects to be called after AOs (and ErrAO) has
 * been initialized and ready to receive errors */
static void Startup_Checks(void)
{
    /* Record the current RCM Reset Status from the sticky bits
     * and reset the sticky bits after they have been read */
    MaxData.rcm_value = RCM->SSRS;
    RCM->SSRS |= RCM->SSRS;

    MaxData.wdt_rst_trig = (bool)GPIO_GetInput(GPIO_WDT_RESET);

    /* If RCM does not indicate a typical POR (0x82) or Debug session (0x420),
     * then check for the supported reset conditions. */
    if ((MaxData.rcm_value != (RCM_SSRS_SPOR_MASK | RCM_SSRS_SLVD_MASK)) &&
        (MaxData.rcm_value != (RCM_SSRS_SSW_MASK  | RCM_SSRS_SWDOG_MASK)))
    {
        if (MaxData.rcm_value == RCM_SSRS_SPIN_MASK)
        {
            /* Reset was due to external WDT if both the External WDT and Reset pins are set. */
            report_error_uint(&AO_Err, Errors.WatchdogTrip, SEND_TO_SPACECRAFT, MaxData.rcm_value);
        }
        else
        {
            /* Unexpected reset condition */
            report_error_uint(&AO_Err, Errors.McuUnexpectedReset, SEND_TO_SPACECRAFT, MaxData.rcm_value);
        }
    }

    /* Always clear the WDT trigger to prep for the next reset event */
    GPIO_SetOutput(GPIO_WDT_TRIG_nCLR_EN, 0U);
    timer32_delay_us(1UL);
    GPIO_SetOutput(GPIO_WDT_TRIG_nCLR_EN, 1U);

    /* Lock down Golden Image area unless PT says otherwise */
    if (PT->pt_end != GOLD_IMG_LOAD_PWD)
    {
        mcuflash_set_fprot_full();
    }

    /* Once the Active Objects have been initialized, any
     * PT load-related errors can now be logged */
    pt_load_report_any_errors();

    /* Removing bits of precision here is expected since all PT items
     * are standardized to 4B but this particular value is only 2B */
    MaxData.top_serial_number = (uint16_t)PT->top_serial_number;

    /* This must be set after all the AO's have gone through initialization and their ENTRY states */
    MaxData.startup_complete = true;

    return;
}


static void IdleFrame(void)
{
#ifndef MAXIMUS_NDEBUG
    bool os_print_data_exists = false;

    GLADOS_PRINT_HAS_CHARS(&os_print_data_exists);

    if (os_print_data_exists)
    {
        TxAO_SendMsg_OSPrint();
    }
#endif

    Scrub_MemoryProcess();

    return;
}

/*! @} */
