/* Copyright (c) 2017-2020 by Phase Four, Inc. All Rights Reserved. Confidential and Proprietary.
 *
 * pt_load.c
 */

#include "pt_load.h"
#include "mcu_types.h"
#include "max_data.h"
#include "crc.h"
#include "timer64.h"
#include "MCUflash.h"
#include "gpio.h"
#include "p4_err.h"
#include "ErrAO.h"
#include "default_pt.h"
#include "string.h"


static Cmd_Err_Code_t pt_load_error;

static void erase_runtime_table(uint32_t app_pt_full_size);
static void copy_table_to_runtime_table(uint32_t pt_img_addr, uint32_t pt_img_full_length);
static void use_default_pt(void);
static void use_app_pt(uint32_t pt_img_addr);
static void use_runtime_pt(void);


/* There are several Parameter Tables (PT):
 *  1. Runtime PT - intermediate buffer of the current App's PT
 *  2. App PT     - stored adjacent to the loaded Application
 *  3. Default PT - stored with the App itself, PT Core is loaded at App elaboration
 *
 *
 * PTs are generally validated through the following means:
 *  1. Checking PT type matches that in the code
 *     Note: This validates that the PT is intended for the code that is running
 *  2. Checking PT length matches that in the compiled code
 *     Note: This adds an extra check to ensure that the PT and code are in
 *     agreement on the PT format, since it's possible for a PT to be valid but
 *     have a different structure (e.g. older PT version for the same HW).
 *  3. Checking PT CRC matches what is specified in the header
 *     Note: This validates both the CRC and length of the image header match
 *
 *
 * Steps in selecting the appropriate PT
 *  1. Validate that App PT is not empty and has valid image header info
 *      A. If invalid, then use Default PT
 *      B. If valid, validate Runtime PT
 *          i.  If valid, use Runtime PT
 *          ii. If invalid, check App PT CRC (last step of App PT validation)
 *              a. If invalid, App PT and Runtime PT are both invalid, use Default PT
 *              b. If valid, copy App PT to runtime table and re-validate Runtime PT
 *                  1. If valid, use Runtime PT
 *                  2. If invalid or copy to runtime fails, use App PT
 *
 *
 * If using Default PT, the following will happen:
 *  1. Log SC error
 *  2. GET_SW_INFO for current PT image points to the loaded App header
 *  3. Prevent Prop Inventory calculation
 *  4. Prevent IDLE/THRUST since PTs determine CC driver values
 *
 * If using App PT, the following will happen:
 *  1. Log warning error
 *  2. Prevent SW updates to the App PT
 */
void pt_load_init(void)
{
    /* The function expects MaxData to have been initialized */
    DEV_ASSERT(MaxData.time_sync_lpit_lo != 0UL);

    enum Constants { STR_MATCH = 0 };

    uint32_t act_img_crc;
    uint32_t pt_app_img_addr;
    Img_Header_Rec_t *pt_run_img_hdr;
    Img_Header_Rec_t *pt_app_img_hdr;

    pt_load_error = ERROR_NONE;

    /* Get the location of the current App PT */
    if (MaxData.app_select == 1U)
    {
        pt_app_img_addr = PT_LOAD_APP1_IMG_ADDR;
    }
    else if (MaxData.app_select == 2U)
    {
        pt_app_img_addr = PT_LOAD_APP2_IMG_ADDR;
    }
    else
    {
        /* If the App is 3, then use the Golden App
         * If the App is invalid, then still set to
         * Golden App for now but use Default PT */
        pt_app_img_addr = PT_LOAD_GOLD_IMG_ADDR;
    }

    /* Get access to both the App PT and Runtime PT image headers */
    pt_app_img_hdr = (Img_Header_Rec_t *)pt_app_img_addr;
    pt_run_img_hdr = (Img_Header_Rec_t *)PT_LOAD_RUN_IMG_HDR_ADDR;

    /* 1. Validate that App PT is not empty and has valid image header info
     *
     * Validate image type and length matches that of the compiled code.
     * Mismatch of the image type indicates the *PT* is not made for this
     * SW App. Mismatch of the length indicates that the *version* of the
     * PT was not made for this SW App. These checks also ensure that
     * there exists at least a table adjacent to the SW App and that it
     * is not (completely) all 0xFFs. It also prevents a CRC check using
     * a crazy length like 0xFFFFFFFF.
     *
     * Note: Only the header is checked at this point since this is part
     * of the design. If integrity of the App PT is invalid but it is
     * valid in the Tuntime PT, then we will want to proceed. */
    if ((MaxData.app_select != 1U) && (MaxData.app_select != 2U) && (MaxData.app_select != 3U))
    {
        /* 1.A. If invalid, then use Default PT */

        /* If the App doesn't know who it is, then we don't either. Use Default PT. */
        pt_load_error = create_error_uint(Errors.UsingDefaultPtInvalidAppSel, MaxData.app_select);
        use_default_pt();
    }
    else if (pt_app_img_hdr->img_type != PARAM_TABLE_IMG_TYPE_ID)
    {
        /* 1.A. If invalid, then use Default PT */
        pt_load_error = create_error_uint(Errors.UsingDefaultPtTypeMismatch, pt_app_img_hdr->img_type);
        use_default_pt();
    }
    else if (pt_app_img_hdr->img_length != PARAM_TABLE_IMG_DATA_SIZE)
    {
        /* 1.A. If invalid, then use Default PT */
        pt_load_error = create_error_uint(Errors.UsingDefaultPtLengthMismatch, pt_app_img_hdr->img_length);
        use_default_pt();
    }
    else
    {
        /* 1.B. If valid, validate Runtime PT
         *
         * Check that:
         *  1. The image type and length match the compiled code
         *     Note: We re-check these since the Runtime PT header may need
         *     to be fixed/re-programmed in order to read the correct data
         *     via the GET_SW_INFO command.
         *  2. The App PT CRC matches the Runtime PT CRC.
         *
         *  These checks also ensure that the Runtime PT is not obviously
         *  empty (all 0xFFs) and means it is within the realm of possibility
         *  that the App PT is already loaded in the Runtime PT.
         */
        if ((pt_run_img_hdr->img_type   == PARAM_TABLE_IMG_TYPE_ID)   &&
            (pt_run_img_hdr->img_length == PARAM_TABLE_IMG_DATA_SIZE) &&
            (pt_run_img_hdr->img_crc    == pt_app_img_hdr->img_crc)   &&
            (pt_run_img_hdr->img_crc    == crc32_compute(0xFFFFFFFFUL,
                    (uint8_t *)PT_LOAD_RUN_IMG_DATA_ADDR, pt_run_img_hdr->img_length, true)) &&
            (STR_MATCH == memcmp(&pt_run_img_hdr->img_ver[0],
                                 &pt_app_img_hdr->img_ver[0],
                                 sizeof(pt_app_img_hdr->img_ver))))
        {
            /* 1.B.i. If valid, use runtime PT */
            MaxData.pt_crc = pt_app_img_hdr->img_crc;
            use_runtime_pt();
        }
        else
        {
            /* 1.B.ii. If invalid, check App PT CRC (last step of App PT validation) */

            act_img_crc = crc32_compute(0xFFFFFFFFUL,
                    (uint8_t *)(pt_app_img_addr + IMG_HDR_MAX_SIZE), pt_app_img_hdr->img_length, true);

            if (pt_app_img_hdr->img_crc != act_img_crc)
            {
                /* 1.B.ii.a. If invalid, App PT and Runtime PT are both invalid, use Default PT */
                pt_load_error = create_error_uint(Errors.UsingDefaultPtCrcMismatch, act_img_crc);
                use_default_pt();
            }
            else
            {
                /* 1.B.ii.b. If valid, copy App PT to runtime table and re-validate Runtime PT */

                /* Erase the runtime table
                 * Notes:
                 *  1) Ignore returned status since it's possible that even a failure to erase may still
                 *     be programmable. Ultimately the programming and re-checking integrity will determine
                 *     failure to program. Ignoring the status also permits a second chance for integrity of the
                 *     runtime table since we are here since it failed the first time.
                 *  2) The App PT image length is used since it may be larger than the space currently used
                 *     by the Runtime PT. Not using the App PT length for the erase could potentially lead
                 *     to corruption from writing over data that is already there. This has been witnessed
                 *     to lead to hard faults and rolling resets. */
                erase_runtime_table(pt_app_img_hdr->img_length + IMG_HDR_MAX_SIZE);

                /* Copy the current App PT to the runtime PT area */
                copy_table_to_runtime_table(pt_app_img_addr, pt_app_img_hdr->img_length + IMG_HDR_MAX_SIZE);

                /* Validate that the copy was performed correctly. Use the App image length here in case
                 * the erase/program failed and the Runtime PT image length is corrupted or was never
                 * set and remains some huge number like 0xFFFFFFFF. */
                act_img_crc = crc32_compute(0xFFFFFFFFUL,
                                            (uint8_t *)PT_LOAD_RUN_IMG_DATA_ADDR,
                                            pt_app_img_hdr->img_length,
                                            true);

                MaxData.pt_crc = pt_app_img_hdr->img_crc;

                /* Validating the calculated CRC in the runtime PT against the App PT
                 * ensures that data is safe for use, so no need for checking the length
                 * and image type a second time. */
                if ((pt_app_img_hdr->img_type   == pt_run_img_hdr->img_type)   &&
                    (pt_app_img_hdr->img_length == pt_run_img_hdr->img_length) &&
                    (pt_app_img_hdr->img_crc    == act_img_crc))
                {
                    /* 1.B.ii.b.1. If valid, use Runtime PT */
                    use_runtime_pt();
                }
                else
                {
                    /* 1.B.ii.b.2. If invalid or copy to runtime fails, use App PT */
                    pt_load_error = create_error_uint(Errors.UsingAppPt, pt_app_img_addr);
                    use_app_pt(pt_app_img_addr);
                }
            }
        }
    }

    /* This development check ensures any un-handled cases,
     * which do not initialize the PT pointers, are caught. */
    DEV_ASSERT(MaxData.using_default_pt || MaxData.using_app_pt || MaxData.using_runtime_pt);

    /* Also validate the PTs are 4-byte aligned */
    DEV_ASSERT(((uint32_t)PT_Default % 4U) == 0U);
    DEV_ASSERT(((uint32_t)PT % 4U) == 0U);

    return;
}


void pt_load_report_any_errors(void)
{
    if (MaxData.using_default_pt)
    {
        /* Default PT table will notify spacecraft since it will be unable to
         * enter into any IDLE/THRUST ops. */
        report_error_uint(&AO_Err, pt_load_error.reported_error, SEND_TO_SPACECRAFT, pt_load_error.aux_data);
    }
    else if (MaxData.using_app_pt)
    {
        report_error_uint(&AO_Err, pt_load_error.reported_error, DO_NOT_SEND_TO_SPACECRAFT, pt_load_error.aux_data);
    }

    return;
}


static void erase_runtime_table(uint32_t app_pt_full_size)
{
    /* Sanity check that the compiled Default PT size is not too large */
    DEV_ASSERT(PARAM_TABLE_IMG_DATA_SIZE <= (IMG_MAX_SIZE - IMG_HDR_MAX_SIZE));

    uint32_t i;
    uint32_t num_of_flash_sectors;
    status_t status = STATUS_SUCCESS;

    /* Use integer math to round up the total number of flash sectors */
    num_of_flash_sectors = (app_pt_full_size + (ERASE_SECTOR_SIZE - 1U)) / ERASE_SECTOR_SIZE;

    /* Strobe the WDT to allow for sufficient time to finish the next operation,
     * however delay at least 27ms to ensure we are within the WDT pet window
     * or else it might reset us prematurely. */
    timer32_delay_ms(30UL);

    for (i = 0UL; i < num_of_flash_sectors; ++i)
    {
        /* Pet the WDT since clearing a max of 128KB could take
         * a little more 4 seconds. */
        GPIO_PetWDT();

        /* This operation is a non-blocking call, therefore we must wait the maximum
         * duration of 130ms for the operation to complete before erasing the next black
         * Note: Returned status may indicate it is busy clearing, because, well, duh.
         * However, we proceed anyways to clear the next block after waiting 130ms
         * and we do not pay attention to this parameter. We only really care about the
         * returned status if it indicates a bad macro value. */
        status = erase_flash_sector(PT_LOAD_RUN_IMG_ADDR + (i * ERASE_SECTOR_SIZE), ERASE_SECTOR_SIZE);
        DEV_ASSERT(status != STATUS_ERROR);
        timer32_delay_ms(130UL);
    }

    /* Avoids compiler warning about unused variable */
    (void)status;

    return;
}


/* Note: pt_img_full_length includes header and data */
static void copy_table_to_runtime_table(uint32_t pt_img_addr, uint32_t pt_img_full_length)
{
    /* Since programming is a blocking call, the amount is split up based on the
     * worst-case programming time (225us per 8B) and the frequency in which we want
     * to strobe the WDT (4Hz). This gets us to about 1 strobe per 1024 writes,
     * with a total of 1024*8 bytes. We should strobe the WDT once per 8192B block. */

    enum Constants { BYTES_PER_BLOCK = 8192U };

    uint32_t i;
    uint32_t curr_app_tbl_addr = pt_img_addr;
    uint32_t curr_run_tbl_addr = PT_LOAD_RUN_IMG_ADDR;
    status_t status = STATUS_SUCCESS;

    /* Round up to the nearest 8 bytes since programming flash must occur in
     * multiples of 8 bytes. Writing unused junk data at the end of the image
     * never hurt nobody. */
    uint32_t total_prog_length = (pt_img_full_length + 0x7UL) & (~0x7UL);

    uint32_t num_of_blocks = total_prog_length / BYTES_PER_BLOCK;
    uint32_t last_write_length = total_prog_length - (num_of_blocks * BYTES_PER_BLOCK);

    /* Strobe the WDT to allow for sufficient time to finish the next operation,
     * however delay at least 27ms to ensure we are within the WDT pet window
     * or else it might reset us prematurely. */
    timer32_delay_ms(30UL);

    for (i = 0UL; (i < num_of_blocks) && (status == STATUS_SUCCESS); ++i)
    {
        /* Pet the WDT since a full block could be anywhere between 92-230ms */
        GPIO_PetWDT();

        /* Program the current block.
         * Note: Programming PFlash is a blocking call. No need to check if program
         * was successful since this is done later in the final CRC check. */
        status = write_flash(curr_run_tbl_addr, BYTES_PER_BLOCK, (uint8_t *)curr_app_tbl_addr);

        curr_app_tbl_addr += BYTES_PER_BLOCK;
        curr_run_tbl_addr += BYTES_PER_BLOCK;
    }

    /* Pet the WDT since a full block could be anywhere between 92-230ms */
    GPIO_PetWDT();

    /* Perform the last write if there is any data left
     * Note: Programming PFlash is a blocking call. No need to check if program
     * was successful since this is done later in the final CRC check. */
    status = write_flash(curr_run_tbl_addr, last_write_length, (uint8_t *)curr_app_tbl_addr);

    (void)status; /* Remove unused parameter compiler warning */

    return;
}


static void use_default_pt(void)
{
    MaxData.using_default_pt = true;
    MaxData.pt_select = 3U;

    /* Prop Inventory table has no initial value, always default to the golden image */
    PT_PropInv = (ParamTable_PropInv_t *)PT_LOAD_GOLD_PROP_INV_ADDR;

    /* Core table already points to the default table, which happens at elaboration */

    DEV_ASSERT(PT_PropInv);

    return;
}

static void use_app_pt(uint32_t pt_img_addr)
{
    MaxData.using_app_pt = true;
    MaxData.pt_select = 2U;

    /* Set the Prop Inventory and Core Tables to use the provided PT address */
    PT_PropInv = (ParamTable_PropInv_t *)(pt_img_addr + IMG_HDR_MAX_SIZE);
    PT = (ParamTable_Core_t *)(pt_img_addr + IMG_HDR_MAX_SIZE + PARAM_TABLE_PROP_INV_SIZE);

    DEV_ASSERT(PT_PropInv);
    DEV_ASSERT(PT);

    return;
}

static void use_runtime_pt(void)
{
    MaxData.using_runtime_pt = true;
    MaxData.pt_select = 1U;

    /* Set the Prop Inventory and Core Tables to use the Runtime PT */
    PT_PropInv = (ParamTable_PropInv_t *)PT_LOAD_RUN_PROP_INV_ADDR;
    PT = (ParamTable_Core_t *)PT_LOAD_RUN_CORE_ADDR;

    DEV_ASSERT(PT_PropInv);
    DEV_ASSERT(PT);

    return;
}

