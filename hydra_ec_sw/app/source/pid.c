/* Copyright (c) 2017-2020 by Phase Four, Inc. All Rights Reserved. Confidential and Proprietary.
 *
 * pid.c
 */

#include "pid.h"
#include "mcu_types.h"
#include <math.h>


void PID_Init(Pid_t *pid, float kp, float ki, float kd, float dt_sec, float i_limit, float min, float max)
{
    DEV_ASSERT(pid);
    DEV_ASSERT(isfinite(kp));
    DEV_ASSERT(isfinite(ki));
    DEV_ASSERT(isfinite(kd));
    DEV_ASSERT(isfinite(dt_sec));
    DEV_ASSERT(isfinite(i_limit));
    DEV_ASSERT(isfinite(min));
    DEV_ASSERT(isfinite(max));
    DEV_ASSERT(dt_sec > 0.0f);
    DEV_ASSERT(min < max);

    pid->kp = kp;
    pid->kd = kd;
    pid->ki = ki;
    pid->dt_sec = dt_sec;
    pid->i_limit = i_limit;
    pid->min = min;
    pid->max = max;
    pid->first_comp = true;
    pid->last_input = 0.0f;
    pid->integral = 0.0f;

    return;
}


void PID_ResetError(Pid_t *pid)
{
    DEV_ASSERT(pid);

    pid->first_comp = true;
    pid->last_input = 0.0f;
    pid->integral = 0.0f;

    return;
}


float PID_Calculate(Pid_t *pid, float setpoint, float curr_input)
{
    DEV_ASSERT(pid);
    DEV_ASSERT(pid->dt_sec > 0.0f);
    DEV_ASSERT(isfinite(setpoint));
    DEV_ASSERT(isfinite(curr_input));

    float error;
    float d_input;
    float p_out;
    float i_out;
    float d_out;
    float output;

    error = setpoint - curr_input;

    /* There is no last_input if it is the first computation */
    if (pid->first_comp)
    {
        pid->first_comp = false;
        d_input = 0.0f;
    }
    else
    {
        d_input = curr_input - pid->last_input;
    }

    /* Calculate the proportional term */
    p_out = pid->kp * error;


    /* Calculate the integral term */
    pid->integral += pid->ki * error * pid->dt_sec;

    /* Coerce integral term to avoid integral windup */
    if (pid->integral > pid->i_limit)
    {
        pid->integral = pid->i_limit;
    }
    else if (pid->integral < (-1.0f * pid->i_limit))
    {
        pid->integral = (-1.0f * pid->i_limit);
    }

    i_out =  pid->integral;


    /* Calculate the derivative term */
    d_out = (-1.0f) * pid->kd * d_input / pid->dt_sec;

    /* Get the new value */
    output = p_out + i_out + d_out;

    /* Coerce the new value to the min and max bounds */
    if (output > pid->max)
    {
        output = pid->max;
    }
    else if (output < pid->min)
    {
        output = pid->min;
    }

    /* Update the PID state */
    pid->last_input = curr_input;

    return output;
}
