/* Copyright (c) 2017-2020 by Phase Four, Inc. All Rights Reserved. Confidential and Proprietary.
 *
 * aux_driver.c
 */
#include "aux_driver.h"
#include "gpio.h"
#include "string.h"
#include "WdtAO.h"
#include <stdio.h>
#include <string.h>

extern uint32_t __MAX_AUX_NAME_HEAD_START;
#define NEW_LINE_DELIMITER_SIZE 1U

static running_morse_code_container_type morse_code_letter_holder;
static morse_code_string_tracker_type morse_string_tracker;
static morse_code_tracker_type morse_code_high_level;



static morse_code_letter_type morse_code_alphabet_legend [TOTAL_MORSE_ALPHABET_LETTERS] = {
        {'a',{Dot,Dash,Null,Null}},
        {'b',{Dash,Dot,Dot,Dot}},
        {'c',{Dash,Dot,Dash,Dot}},
        {'d',{Dash,Dot,Dot,Null}},
        {'e',{Dot,Null,Null,Null}},
        {'f',{Dot,Dot,Dash,Dot}},
        {'g',{Dash,Dash,Dot,Null}},
        {'h',{Dot,Dot,Dot,Dot}},
        {'i',{Dot,Dot,Null,Null}},
        {'j',{Dot,Dash,Dash,Dash}},
        {'k',{Dash,Dot,Dash,Null}},
        {'l',{Dot,Dash,Dot,Dot}},
        {'m',{Dash,Dash,Null,Null}},
        {'n',{Dash,Dot,Null,Null}},
        {'o',{Dash,Dash,Dash,Null}},
        {'p',{Dot,Dash,Dash,Dot}},
        {'q',{Dash,Dash,Dot,Dash}},
        {'r',{Dot,Dash,Dot,Null}},
        {'s',{Dot,Dot,Dot,Null}},
        {'t',{Dash,Null,Null,Null}},
        {'u',{Dot,Dot,Dash,Null}},
        {'v',{Dot,Dot,Dot,Dash}},
        {'w',{Dot,Dash,Dash,Null}},
        {'x',{Dash,Dot,Dot,Dash}},
        {'y',{Dash,Dot,Dash,Dash}},
        {'z',{Dash,Dash,Dot,Dot}},
        {' ',{Null,Null,Null,Null}}
  };

void turn_on_all_leds(void)
{
GPIO_SetOutput(GPIO_LED3,true);
GPIO_SetOutput(GPIO_LED2,true);
GPIO_SetOutput(GPIO_LED1,true);
GPIO_SetOutput(GPIO_LED0,true);

return;
    }

void turn_off_all_leds(void)
{
GPIO_SetOutput(GPIO_LED3,false);
GPIO_SetOutput(GPIO_LED2,false);
GPIO_SetOutput(GPIO_LED1,false);
GPIO_SetOutput(GPIO_LED0,false);

return;
    }



bool run_morse_letter(running_morse_code_container_type *morse_letter)
{

    bool letter_finished = false;
    uint8_t current_command = morse_letter->morse_info[morse_letter->morse_letter_info_index];
    if (current_command != Null)
    {
        if (morse_letter->break_between_morse_componet == true)
        {
            turn_off_all_leds();
            morse_letter->break_between_morse_componet = false;

        }
        /* If the next set is a dot go in this statement */
        else if (current_command == Dot)
        {
            turn_on_all_leds();

            morse_letter->morse_letter_info_index++;
            current_command = morse_letter->morse_info[morse_letter->morse_letter_info_index];
            if (current_command != Null)
            {
                morse_letter->break_between_morse_componet = true; /* We finished a dot*/
            }
        }
        else
        {
            /* It has to be a dash now. It can only be a null, dot, or dash */
            turn_on_all_leds();
            morse_letter->current_morse_dot_count++;
            if (morse_letter->current_morse_dot_count == 3)
            {
                morse_letter->morse_letter_info_index++;
                current_command = morse_letter->morse_info[morse_letter->morse_letter_info_index];

                if (current_command != Null)
                {
                    morse_letter->break_between_morse_componet = true; /* We finished a dash*/
                }
                morse_letter->current_morse_dot_count = 0;
            }

        }
    }

    /* If the current command is a Null then we are done with this letter. Once there is one null there will only be null's after.*/
    /* Also if there is a index of 4 then we are done. Morse code can only have up to 4 distinct morse events (dot or dash).*/
    if ((current_command == Null) || (morse_letter->morse_letter_info_index == 4) )
    {
        /* Reseting letter */
        morse_letter->morse_letter_info_index = 0;
        morse_letter->current_morse_dot_count= 0;
        morse_letter->break_between_morse_componet = false;
        letter_finished = true;
    }
return letter_finished;
}


morse_code_letter_type find_morse_letter(char letter_arg)
{
    morse_code_letter_type output_letter_object = {'*', {Null,Null,Null,Null}};

    /*Determining if is a capital letter or a lower case letter */
    if (((uint32_t) letter_arg >= (uint32_t)'A') && ((uint32_t)letter_arg <= (uint32_t)'Z')){
        output_letter_object =morse_code_alphabet_legend[letter_arg - 'A']; /* Normalizing the value to a 0 index array. Since 'A' is the lowest value, subtract by that to normalize to 0.*/
    }
    else if (((uint32_t) letter_arg >= (uint32_t)'a') && ((uint32_t)letter_arg <= (uint32_t)'z')){
        output_letter_object =morse_code_alphabet_legend[letter_arg - 'a']; /* Normalizing the value to a 0 index array. Since 'a' is the lowest value, subtract by that to normalize to 0.*/
    }
    else if (letter_arg == ' '){
        output_letter_object = morse_code_alphabet_legend[26];
    }


    return output_letter_object;
}




bool run_morse_code_string(char * morse_string, uint8_t string_length){


    char morse_string_complete = false;

    /* If we need a new character prepare get and prep for that character*/
    if (morse_string_tracker.new_letter == true)
    {
        morse_code_letter_type morse_code_letter_object = find_morse_letter(morse_string[morse_string_tracker.letter_index]);
        morse_code_letter_holder.alphabetic_letter= morse_code_letter_object.alphabetic_letter;
        morse_string_complete = prep_morse_code_char(&morse_code_letter_object,string_length);

        /* If we searched and we determine the string is complete lets return and get the next string.*/
        if (morse_string_complete == true)
        {
            return morse_string_complete;
        }

        morse_string_tracker.new_letter = false;
    }


    /* Main meat of the function starts here. We are determining if we are going to run part of a morse character or are we breaking between characters */
    if (morse_string_tracker.morse_break_flag == false)
    {
        bool letter_finished =  run_morse_letter(&morse_code_letter_holder);
        if (letter_finished == true)
        {
            /* After a character has been displayed in morse code wait for 3 'pulses' before starting the next character.*/
            morse_string_tracker.morse_break_flag = true;
            morse_string_tracker.morse_break_count = 0U;
            morse_string_tracker.morse_break_total = 3U;
            letter_finished = true;
        }
    }
    /* If we are not running a morse_code section then we are breaking between characters from either a finished character or a ' ' character was sent */
    else
    {
        morse_string_tracker.morse_break_count++;
        turn_off_all_leds();
        if ( morse_string_tracker.morse_break_count ==  morse_string_tracker.morse_break_total)
        {   /* We are done with our break lets get a new char*/
            morse_string_tracker.morse_break_flag = false;
            morse_string_tracker.letter_index++;
            morse_string_tracker.new_letter = true;
            /* If we are done with this string lets report a success and go get a new string.*/
            if (morse_string_tracker.letter_index >= string_length)
            {
                morse_string_tracker.letter_index = 0U;
                morse_string_complete =  true;
            }
        }

    }

    return morse_string_complete;
}

bool prep_morse_code_char(morse_code_letter_type* morse_code_letter_object, uint8_t string_length)
{

    char morse_string_complete = false;
    /* If it is equal to * then it could not find it lets skip to the next letter. */
    if (morse_code_letter_object->alphabetic_letter == '*')
    {
        morse_string_tracker.letter_index++;
        if (morse_string_tracker.letter_index == string_length)
        {
            morse_string_tracker.letter_index = 0;
            morse_string_complete = true;
        }

    }
    /* If there is a space we break for 5 'pulses' Before starting the next character.*/
    else if (morse_code_letter_object->alphabetic_letter == ' ')
    {
        if (morse_string_tracker.morse_break_flag == false)
        {

            morse_string_tracker.morse_break_flag = true;
            morse_string_tracker.morse_break_count = 0U;
            morse_string_tracker.morse_break_total = 5U;
        }
    }
    /* Then we have a normal Character lets copy it over and go to the next session.*/
    else
    {
        memcpy(morse_code_letter_holder.morse_info, morse_code_letter_object->morse_info, sizeof(uint8_t) * 4);
    }
    return morse_string_complete;
}

void run_morse_code()
{
    bool name_setup_complete = true;

    /* Get the new string's length so it can be used in the function.*/
    if (morse_code_high_level.new_name_flag == true)
    {
        morse_code_high_level.employee_name_string_size = find_string_size(morse_code_high_level.next_employee_point);

        /* If the length is 0 something happened lets skip this string.*/
        if ( morse_code_high_level.employee_name_string_size == 0)
        {
            name_setup_complete = false;
        }


    }

    if (name_setup_complete == true)
    {
        morse_code_high_level.new_name_flag = run_morse_code_string(morse_code_high_level.next_employee_point, morse_code_high_level.employee_name_string_size); /* Run a part of the morse sequence from a input of a string. The output indicates if the string is done*/
    }

    /*Check if we are done with the string, if we are select the next one.*/
    if (morse_code_high_level.new_name_flag == true)
    {
        morse_code_high_level.next_employee_point = morse_code_high_level.next_employee_point + morse_code_high_level.employee_name_string_size + NEW_LINE_DELIMITER_SIZE; /* Adding the new line delimiter sets it up for the next name.*/
        /* IF the next character starting is a 0 "/0" or if the amount of bytes we went through was above 8 KB stop turning on the LEDS.*/
        if (('\0' == (char) *morse_code_high_level.next_employee_point) ||(((uint32_t)morse_code_high_level.next_employee_point - (uint32_t)&__MAX_AUX_NAME_HEAD_START)> 0x2000))
        {
            stop_aux_driver(false);
        }
    }
    return;
}


/* Find the next size for the next string */
uint8_t find_string_size(char * morse_string)
{
    uint8_t string_size = 0;

    for (uint8_t x=0; x < CHAR_PER_EMPLOYEE; x++)
    {
        if (morse_string[x] == '\n')
        {
            string_size = x;
            break;
        }
    }
    return string_size;

}
void start_aux_driver()
{
    turn_off_all_leds();
    WdtAO_Enable_WDT_LED(false);
    WdtAO_Enable_Aux_Driver(true);
    morse_code_high_level.employee_name_string_size=0;
    morse_code_high_level.next_employee_point = (char*)&__MAX_AUX_NAME_HEAD_START;
    morse_code_high_level.new_name_flag=true;
    morse_string_tracker.morse_break_count=0;
    morse_string_tracker.morse_break_total=0;
    morse_string_tracker.letter_index=0;
    morse_string_tracker.morse_break_flag = false;
    morse_string_tracker.new_letter = true;
    morse_code_letter_holder.alphabetic_letter= '*';
    morse_code_letter_holder.current_morse_dot_count= 0;
    morse_code_letter_holder.morse_letter_info_index= 0;
    morse_code_letter_holder.break_between_morse_componet= false;
    morse_code_letter_holder.morse_info[0] = Null;
    morse_code_letter_holder.morse_info[1] = Null;
    morse_code_letter_holder.morse_info[2] = Null;
    morse_code_letter_holder.morse_info[3] = Null;
}

void stop_aux_driver(bool turn_off_leds)
{
    if (turn_off_leds == false){
        turn_on_all_leds();
    }
    else{
    turn_off_all_leds();
    }

    WdtAO_Enable_WDT_LED(true);
    WdtAO_Enable_Aux_Driver(false);
    morse_code_high_level.employee_name_string_size=0;
    morse_code_high_level.next_employee_point = (char*)&__MAX_AUX_NAME_HEAD_START;
    morse_code_high_level.new_name_flag=true;
    morse_string_tracker.morse_break_count=0;
    morse_string_tracker.morse_break_total=0;
    morse_string_tracker.letter_index=0;
    morse_string_tracker.morse_break_flag = false;
    morse_string_tracker.new_letter = true;
    morse_code_letter_holder.alphabetic_letter= '*';
    morse_code_letter_holder.current_morse_dot_count= 0;
    morse_code_letter_holder.morse_letter_info_index= 0;
    morse_code_letter_holder.break_between_morse_componet= false;
    morse_code_letter_holder.morse_info[0] = Null;
    morse_code_letter_holder.morse_info[1] = Null;
    morse_code_letter_holder.morse_info[2] = Null;
    morse_code_letter_holder.morse_info[3] = Null;

}

