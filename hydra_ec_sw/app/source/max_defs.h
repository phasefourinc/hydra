/* Copyright (c) 2017-2020 by Phase Four, Inc. All Rights Reserved. Confidential and Proprietary.
 *
 * max_defs.h
 */

#ifndef SOURCE_MAX_DEFS_H_
#define SOURCE_MAX_DEFS_H_

/* Various 16-bit addresses used by the system */
#define MAX_ADDR_PCU             0x0U
#define MAX_ADDR_RFT             0x1U
#define MAX_ADDR_SC              0xEU

#define MAX_CLANK_TX_MASK(addr)  (1U << addr)

/* PCU expects messages intended for both PCU and RFT */
#define MAX_CLANK_RX_MASK_PCU    (MAX_CLANK_TX_MASK(MAX_ADDR_RFT) | MAX_CLANK_TX_MASK(MAX_ADDR_PCU))

/* RFT expects messages only intended for the RFT */
#define MAX_CLANK_RX_MASK_RFT    (MAX_CLANK_TX_MASK(MAX_ADDR_RFT))

/* This determines the slowest response time for a received command */
#define MAX_CMD_TIMEOUT_US       500000UL

/* Configure timeout for receiving a message between 5ms and 30ms. Timing
 * selected based on 265B max transfer at 1Mbps and 115200bps respectively. */
#define MAX_RX_PKT_TIMEOUT_US    30000UL

#endif /* SOURCE_MAX_DEFS_H_ */
