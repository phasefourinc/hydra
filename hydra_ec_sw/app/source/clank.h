/* Copyright (c) 2017-2020 by Phase Four, Inc. All Rights Reserved. Confidential and Proprietary.
 *
 * clank.h
 */

#ifndef SOURCE_CLANK_H_
#define SOURCE_CLANK_H_

/*!
 * @addtogroup Clank
 * @{
 *
 * @file
 * @brief
 * This file defines Clank protocol API.
 *
 * @note The implementation supports both Big and Little Endian systems.
 */

#include "mcu_types.h"

/*! Clank header information format */
typedef struct Clank_Hdr_tag
{
    uint8_t SyncByte;
    uint8_t Src_Address;
    uint16_t Trans_Mask;
    uint8_t Pkt_Seq_Cnt;
    uint16_t MsgID;
    uint8_t Pkt_DataLen;
} BYTE_PACKED Clank_Hdr_t;


#define CLANK_SYNC_VER0         0x55U
#define CLANK_MAX_SRC_ADDR      0x0FU
#define CLANK_TRANS_MASK        0x1000U
#define CLANK_MSG_ID            0x05U
#define CLANK_INVALID_MSG_ID    0U
#define CLANK_MAX_CLANK_ADDR    15U

#define CLANK_HDR_SIZE          (sizeof(Clank_Hdr_t))
#define CLANK_MAX_DATA_SIZE     255U
#define CLANK_CRC_SIZE          2U
#define CLANK_DELIM_SIZE        2U
#define CLANK_MAX_MSG_SIZE      (CLANK_HDR_SIZE + CLANK_MAX_DATA_SIZE + CLANK_CRC_SIZE + CLANK_DELIM_SIZE)
#define CLANK_RESP_MSG_ID(x)    (x | 0x8000U)  /* Set MSb of msg ID to indicate it is an Ack messasge */


/*!
 * @brief Scans the provided buffer for a sync byte, which signifies the start
 *        of a message.  If found, the start index is returned. If no sync byte
 *        is found, then -1 is returned.
 *
 * @pre @p buf is not NULL
 * @pre @p buf contains at least buf_length number of bytes
 * @pre @p out_start_idx is not NULL
 *
 * @param[in] buf         Pointer to the buffer in which to search
 * @param[in] buf_length  The number of bytes to search
 * @return                Returns start index if found, otherwies returns -1
 */
int16_t Clank_GetStartIdx(uint8_t *buf, uint16_t buf_length);


/*!
 * @brief Retrieves the header information structure from the provided message.
 *
 * @pre @p message_in is not NULL
 * @pre @p hdr is not NULL
 *
 * @param message_in[in]  Pointer to the start of a Clank message
 * @param hdr[out]        Pointer to the resulting header structure
 */
void Clank_GetHeader(uint8_t *message_in, Clank_Hdr_t *hdr);


/*!
 * @brief Validates the Clank protocol header.
 *
 * @pre @p hdr is not NULL
 *
 * @param hdr[in]  Pointer to the clank header structure
 * @return         True if valid, false if invalid
 */
bool Clank_CheckHeaderInfo(const Clank_Hdr_t *hdr);


/* Checks to see if the current clank address is contained within the transmit mask */
/*!
 * @brief Checks if the specified clank address exists in the clank transmit
 *        mask. This is useful for validating that the received message was
 *        intended for the recipient.
 *
 * @param hdr[in]        Pointer to the clank header structure
 * @param rx_clank_addr  The receive clank address to check
 * @return               True if exists in the transmit mask, false otherwise
 */
bool Clank_CheckTxMask(const Clank_Hdr_t *hdr, uint8_t rx_clank_addr);


/*!
 * @brief Validates the CRC for a Clank message
 *
 * @pre @p message_in is not NULL
 *
 * @param message_in[in]  Pointer to the start of a Clank message
 * @return                True if valid, false if invalid
 */
bool Clank_CheckCrc(uint8_t *message_in);


/*!
 * @brief Formats a Clank message using the provided header and data
 *        information.
 *
 * @pre @p message_out is not NULL
 * @pre @p message_out size must be capable of storing the largest Clank packet
 * @pre @p message_len_out is not NULL
 * @pre @p data_buf is not NULL
 *
 * @param message_out[out]      Pointer to where to store the resulting Clank message
 * @param message_len_out[out]  Pointer to where to store the resulting message size
 * @param is_master             True if a master transmission, otherwise false for slave
 * @param src_id                The source address
 * @param dest_id               The destination address
 * @param pkt_seq_cnt           The packet sequence count
 * @param msg_id                The message ID
 * @param data_buf[in]          Pointer to the payload data
 * @param data_size             The payload data size
 */
void Clank_CreateMessage(uint8_t *message_out,
                         uint16_t *message_len_out,
                         bool is_master,
                         uint8_t src_id,
                         uint16_t dest_id,
                         uint8_t pkt_seq_cnt,
                         uint16_t msg_id,
                         uint8_t *data_buf,
                         uint8_t data_size);

/*! @} */

#endif /* SOURCE_CLANK_H_ */
