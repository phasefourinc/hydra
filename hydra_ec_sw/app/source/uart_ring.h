/* Copyright (c) 2017-2020 by Phase Four, Inc. All Rights Reserved. Confidential and Proprietary.
 *
 * uart_ring.h
 */

#ifndef SOURCE_UART_RING_H_
#define SOURCE_UART_RING_H_

#include "mcu_types.h"

/* Global invariants:
 *  - Implementation does not check for UART statuses, these should be
 *    captured in interrupt callback functions elsewhere to handle any
 *    UART-related errors that occur.
 */

/* Since none of the approaches worked, I decided to try and bypass the
 * SDK and implement a custom DMA transfer.  The idea is to set up a
 * DMA TCD to fill the entire ring buffer with a single transfer. Upon
 * transfer completion, the DMA would use scatter-gather to repeatedly
 * load the exact same TCD. This is nice because the DMA is constantly
 * receiving in the background without any interrupt intervention, allowing
 * the tail to simply follow the Major Iteration count of the TCD. Also,
 * it has the added benefit of not being affected by interrupts. So adding
 * a DMA TCD completion interrupt occurs completely in parallel and likely
 * means this approach has a good chance of working when integrated with
 * a bunch of other peripherals. It has the nice debugging side benefit of
 * not ruining a transfer if adding a breakpoint in the interrupt.
 * Where as the other approaches are susceptible to long interrupts at high
 * rates leading to RX Overruns. The challenge is how to handle buffer overruns.
 *
 * The overrun workaround attempted was to monitor a count that starts at the
 * ring buffer size and increases whenever a DMA transfer completes and
 * decreases each time data is read from the ring buffer. This allows for
 * the count to be monitored within a range: 0 < count < 2*ring_size.  The
 * count should never get below 0 since that implies that more data was read
 * than exists in the ring buffer.  The region of interest is to check for
 * the count being < 2*ring_size, because going beyond the region indicates
 * that the Tail has exceeded the Head. This approach was attempted but did
 * not work out too well due to the DMA interrupt periodically being "skipped"
 * for an unknown reason. Unclear of the root cause but the gut feeling was
 * that it had something to do with the DMA interaction with the interrupt
 * MCU interrupt handler. After lots of testing using the DMA TCD approach,
 * no Rx issues that would indicate a buffer overrun have been apparent. As such,
 * it seems safer to just remove this overrun detection altogether and let the
 * SW report malformed packets whenever the SW us unable to respond fast enough.
 */

typedef struct UartRing_Rec_tag
{
    LPUART_Type *lpuart;
    uint8_t dma_rx_ch;
    uint8_t *ring_buf;
    uint16_t ring_size;
    uint16_t head_i;
    uint16_t tail_i;
    uint8_t *read_buf;
    uint16_t read_size;
    uint8_t dma_rx_err_sem;
    bool read_ongoing;
    bool is_active;
} UartRing_Rec_t;


void UartRing_Init(UartRing_Rec_t *ring_rec,
                   LPUART_Type *lpuart,
                   uint8_t dma_rx_ch,
                   uint8_t *ring_buf,
                   uint16_t ring_size);

void UartRing_StartBuffer(UartRing_Rec_t *ring_rec);
void UartRing_StopBuffer(UartRing_Rec_t *ring_rec);
void UartRing_ResetBuffer(UartRing_Rec_t *ring_rec);
bool UartRing_Process(UartRing_Rec_t *ring_rec);
uint16_t UartRing_BytesAvailable(const UartRing_Rec_t *ring_rec);
void UartRing_ReadBuffer(UartRing_Rec_t *ring_rec, uint8_t *read_buf, uint16_t bytes_requested);

#endif /* SOURCE_UART_RING_H_ */
