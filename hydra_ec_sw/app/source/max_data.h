/* Copyright (c) 2017-2020 by Phase Four, Inc. All Rights Reserved. Confidential and Proprietary.
 *
 * max_data.h
 */

#ifndef SOURCE_MAX_DATA_H_
#define SOURCE_MAX_DATA_H_

#include "mcu_types.h"

typedef enum
{
    MAX_LOW_PWR   = 0x1,
    MAX_IDLE      = 0x2,
    MAX_THRUST    = 0x4,
    MAX_MANUAL    = 0x7,
    MAX_SW_UPDATE = 0x8,
    MAX_BIST      = 0xB,
    MAX_WILD_WEST = 0xD,
    MAX_SAFE      = 0xF
} MaxState_t;


typedef enum
{
    DATA_LOUNGE         = 0x0,
    DATA_RECORD         = 0x3,
    DATA_ERASE_NAND     = 0x5,
    DATA_DUMP_MCU       = 0x6,
    DATA_DUMP_TLM       = 0x9,
    DATA_UP_CMD_FAIL    = 0xA,
    DATA_UP_CMD         = 0xC,
    DATA_UP_FLSH_CLR    = 0xD,
    DATA_UP_FLSH_PRG    = 0xE,
    DATA_UP_CMD_SUCCESS = 0xF,
} DataMode_t;


typedef struct MaxData_tag
{
    /* Holds the current unix time, 0 at reset until new unix time is received */
    uint32_t unix_time_us_hi;
    uint32_t unix_time_us_lo;

    /* Holds the timestamp of the last received time sync value,
     * used to calculate the current unix time since the last commanded value */
    uint32_t time_sync_lpit_hi;           /* 0xFFFFFFFF at startup */
    uint32_t time_sync_lpit_lo;           /* 0xFFFFFFFF at startup */
    uint32_t last_unix_time_sync_us_hi;
    uint32_t last_unix_time_sync_us_lo;

    bool max_busy_flag;
    bool needs_reset_flag;
    uint8_t prop_status;

    MaxState_t state;
    float prop_density;
    float thrust_rf_vset_fb;
    float thrust_mdot_fb;
    float thrust_dur_sec_fb;
    uint16_t cmd_acpt_cnt;
    uint16_t cmd_rjct_cnt;

    float e3v3;
    float ipfcv;
    float ibus;
    float ebus;
    float ihpiso;
    float pprop;
    float irft;
    float e12v;
    float tadc0;

    float tprop;
    float iheater;
    float plowp;
    float e5v;
    float e2v5;
    float mfs_mdot;
    float tmfs;
    float tadc1;

    uint32_t  rft_fault;
    uint32_t  heater_fault;
    uint32_t  hpiso_fault;
    uint32_t  pfcv_fault;

    uint8_t  bl_select;
    uint8_t  app_select;
    uint8_t  pt_select;

    uint8_t next_app_select;

    uint32_t bl_crc;
    uint32_t app_crc;
    uint32_t pt_crc;

    bool     heat_ctrl_en_fb;
    bool     heater_en_fb;
    bool     hpiso_en_fb;
    bool     flow_ctrl_en_fb;

    float    heat_ctrl_temp_fb;
    float    heater_setpt_ma_fb;
    float    hpiso_setpt_ma_fb;
    float    flow_ctrl_mdot_fb;
    float    desired_mdot_v_fb;

    uint32_t rcm_value;
    bool     wdt_rst_trig;
    bool     wdt_tog_sel;
    uint8_t  fsw_cpu_util;

    bool     rec_fram_en;
    bool     rec_nand_en;
    uint8_t  rec_bank_sel;
    DataMode_t data_mode;

    uint32_t rec_curr_fram_addr;
    uint32_t rec_curr_nand_addr;
    uint32_t rec_nand_tlm_cnt[2];         /* Count rolls over at 497 days */
    uint16_t rec_nand_cycle_cnt;
    bool rec_bank0_has_data;
    bool rec_bank1_has_data;

    float   pfcv_ma_fb;

    uint32_t tc_comm_fail_cnt;
    uint16_t tc_comm_fail_cum_cnt;

    bool using_runtime_pt;
    bool using_default_pt;
    bool using_app_pt;

    uint32_t ecc_sram_1bit_cnt;
    uint32_t daq_consec_err_cnt;
    uint32_t rx_crc_fail_cnt;

    bool startup_complete;
    bool thrust_has_ignited;
    bool thrust_steady_state;
    bool thrust_in_hot_fire;
    bool thrust_in_cold_fire;    /* Available for future use. Set properly but unused by the Application */

    float thrust_puff_mdot_fb;
    float thrust_wf_gen_freq_fb;
    uint32_t thrust_puff_dur_us_fb;
    uint32_t thrust_ignite_us_fb;
    float thrust_ignite_vset_fb;
    uint32_t thrust_pp_ramp_us_fb;
    bool thrust_adv_cmd_rcvd;

#ifdef DUMMY_LOAD_STUBS
    uint8_t stub_idle_cnt;
    uint8_t stub_max_heater_dur_250ms;
    float stub_heat_temp_inc;
    float stub_heater_setpt_mA;
    float stub_ttank_temp_setpoint;
    float stub_plowp_setpoint;
    uint32_t stub_thrust_anomaly_sec_cnt;
    uint32_t stub_curr_thrust_sec_cnt;
    uint32_t stub_needs_reset_sec_cnt;
    uint32_t stub_curr_low_pwr_sec_cnt;
    uint32_t stub_last_thrust_sec_cnt;
#endif

    uint16_t top_serial_number;

    bool heater_connected;
    bool mfs_connected;
    bool tc_connected;

} MaxData_t;

extern MaxData_t MaxData;


void max_data_init(void);
void max_data_unix_time_update(uint32_t curr_tick_upper, uint32_t curr_tick_lower);
uint64_t get_unix_timestamp_micro(void);
float max_srand_float_tadc0(void);
float max_srand_float_tadc1(void);
float max_coerce(float input);

#endif /* SOURCE_MAX_DATA_H_ */
