/* Copyright (c) 2017-2020 by Phase Four, Inc. All Rights Reserved. Confidential and Proprietary.
 *
 * pid.h
 */

#ifndef SOURCE_PID_H_
#define SOURCE_PID_H_

#include "mcu_types.h"


/*! Structure holding all gains and values necessary for PID control */
typedef struct Pid_tag
{
    float kp;            /* Proportional Gain */
    float ki;            /* Integral Gain */
    float kd;            /* Differential Gain */
    float dt_sec;
    float i_limit;
    float min;
    float max;
    float last_input;
    float integral;
    bool first_comp;
} Pid_t;


/*!
 * @brief  Initializes the PID structure to hold the values used to perform
 *         Proportional, Integral, and Differential control. This includes
 *         configuring the appropriate gain (kx) values for the desired
 *         system. If a system only wishes to have PD control, then the
 *         integral gain (ki) should be set to zero.
 *
 * @pre @p is not NULL
 * @pre all float parameter values are finite
 * @pre @p dt is greater than 0.0
 * @pre @p min is less than @p max
 *
 * @param[out] pid  The pid structure to initialize
 * @param kp        Proportional gain constant
 * @param ki        Integral gain constant
 * @param kd        Differential gain constant
 * @param dt_ms     Change in time between samples of the current value in milliseconds
 * @param min       The min value of the system
 * @param max       The max value of the system
 */
void PID_Init(Pid_t *pid, float kp, float ki, float kd, float dt_ms, float i_limit, float min, float max);


/*!
 * @brief  Resets the error and integral components of the PID control
 *         system.
 *
 * @pre @p pid is not NULL
 *
 * @param pid  The pid structure to reset
 */
void PID_ResetError(Pid_t *pid);


/*!
 * @brief  Performs a PID control calculation to obtain the next value
 *         based on the provided PID constants, the current setpoint,
 *         and current value. The output is coerced to a min or max value
 *         should the control system return a value beyond the range.
 *
 * @note   This function expects @p setpoint and @p curr_value to be
 *         valid floating point values. Therefore their validity should
 *         be checked prior to calling this function.
 *
 * @pre @p pid is not NULL and initialized
 * @pre @p setpoint is finite
 * @pre @p curr_value is finite
 *
 * @param pid         The pid structure to use for control
 * @param setpoint    The desired setpoint
 * @param curr_value  The current sampled value
 * @return            The new value
 */
float PID_Calculate(Pid_t *pid, float setpoint, float curr_value);


#endif /* SOURCE_PID_H_ */
