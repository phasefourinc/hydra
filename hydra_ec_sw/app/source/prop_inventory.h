/* Copyright (c) 2017-2020 by Phase Four, Inc. All Rights Reserved. Confidential and Proprietary.
 *
 * prop_inventory.h
 */

#ifndef SOURCE_PROP_INVENTORY_H_
#define SOURCE_PROP_INVENTORY_H_

#include "mcu_types.h"

#define PADUA_N_LOW_PSI       30UL
#define PADUA_N_HIGH_PSI      25UL

float T_hat(uint32_t p, float x);
float eval_chevy_chase_lowp(uint32_t n, const float (*c_2darr)[31][31], float x1, float x2);
float eval_chevy_chase_highp(uint32_t n, const float (*c_2darr)[26][26], float x1, float x2);
float calculate_density_lowp(float pressure, float temp);
float calculate_density_highp(float pressure, float temp);

#endif /* SOURCE_PROP_INVENTORY_H_ */
