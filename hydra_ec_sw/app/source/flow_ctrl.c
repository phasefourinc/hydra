/* Copyright (c) 2017-2020 by Phase Four, Inc. All Rights Reserved. Confidential and Proprietary.
 *
 * flow_ctrl.c
 */

#include "flow_ctrl.h"
#include "mcu_types.h"
#include "pid.h"
#include "default_pt.h"
#include "DaqCtlAO.h"
#include "ErrAO.h"
#include "ad768x.h"
#include "pid.h"
#include "max_data.h"
#include "p4_err.h"
#include "math.h"
#include "float.h"

static Pid_t Pid_FlowCtrl = {0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, false};

static uint32_t get_idx_of_closest_val(float val, const float *vals_arr, uint32_t vals_arr_length);
static float get_desired_mdot_v(float desired_mdot_sccm, float rtd2_in_degC);


void flow_ctrl_init(void)
{
    PID_Init(&Pid_FlowCtrl, PT->mdot_pid_kp, PT->mdot_pid_ki, PT->mdot_pid_kd,
                 ((float)PT->mdot_pid_period_ms / 1000.0f), PT->mdot_pid_i_limit, PT->mdot_min, PT->mdot_max);

    return;
}


void flow_ctrl_enable(bool enable)
{
    Cmd_Err_Code_t err;

    if (enable && (!MaxData.flow_ctrl_en_fb))
    {
        /* If flow control is being enabled from being off,
         * then we'll want to command the PFCV to an initial
         * setpoint beyond the cracking current to permit the
         * PID to have direct control over it.
         *
         * Note: Any returned errors are PT-related dev errors */
        err = DaqCtlAO_Pfcv_SetCurrent(PT->mdot_flow_ctrl_init_pfcv_mA);
        DEV_ASSERT(err.reported_error == Errors.StatusSuccess);
        (void)err;  /* Remove unused variable compiler warning */

        /* Reset the PID parameters */
        PID_ResetError(&Pid_FlowCtrl);
    }

    MaxData.flow_ctrl_en_fb = enable;

    return;
}

Cmd_Err_Code_t flow_ctrl_setpt(float mdot_setpt)
{
    Cmd_Err_Code_t error_code = create_error_float(Errors.CmdInvalidMdotSetpt, mdot_setpt);

    if ((isfinite(mdot_setpt))                   &&
        (mdot_setpt >= PT->thrust_cold_mdot_min) &&
        (mdot_setpt <= PT->thrust_cold_mdot_max))
    {
        error_code = ERROR_NONE;
        MaxData.flow_ctrl_mdot_fb = mdot_setpt;
    }

    return error_code;
}


void flow_ctrl_iterate(void)
{
    float desired_mdot_v;
    float next_mdot_v;
    Cmd_Err_Code_t error;

    desired_mdot_v = get_desired_mdot_v(MaxData.flow_ctrl_mdot_fb, MaxData.tmfs);

    next_mdot_v = PID_Calculate(&Pid_FlowCtrl, desired_mdot_v, Adc1.ch_voltage[IN6]) + Adc1.ch_voltage[IN6];
    MaxData.desired_mdot_v_fb = next_mdot_v;

    MaxData.pfcv_ma_fb = calibrate(next_mdot_v, &PT->mdot_to_pfcv_mA_cal[0], sizeof(PT->mdot_to_pfcv_mA_cal)/sizeof(float));

    error = DaqCtlAO_Pfcv_SetCurrent(MaxData.pfcv_ma_fb);
    if (error.reported_error != Errors.StatusSuccess)
    {
        /* Error setting the flow rate, while they may impact a thrust event for this frame,
         * are not as critical since steps in the Thrust sequence perform checks to protect
         * against an invalid flow rate (e.g. right before ignition). Also once in a thrust,
         * other FDIR protects the system. It is sufficient to log this error as a warning. */
        report_error_uint(&AO_DaqCtl, error.reported_error, DO_NOT_SEND_TO_SPACECRAFT, error.aux_data);
    }

    return;
}


/* This function expects the flow rate cals to go from lowest to highest */
float get_current_mdot(float mfs_in_volt, UNUSED float rtd2_in_volt)
{
    /* These values are linearly converted from a 16-bit ADC channel,
     * therefore any issues are going to be development ones. No need
     * to check for these errors manually. */
    DEV_ASSERT(isfinite(mfs_in_volt));
    DEV_ASSERT(isfinite(rtd2_in_volt));

    enum Constants
    {
        TEMP_SCALE_POLY_CNT     = sizeof(PT->mdot_idx0_temp_scale_cal)/sizeof(float),
        IDX_LIST_CNT            = sizeof(PT->temp_scale_mdot_idx_list)/sizeof(float),
    };

    /* Do a binary search using the ideal Temp to Vdesired conversion equations to back-calc
     * the mdot. Do a brute force method by finding the 2 closest Vdesired equations and
     * then linearly interpolating for the actual setpoint mdot */

    float lower_curve_sccm;
    float upper_curve_sccm;
    float act_mdot_sccm;
    float perc;

    /* This god-forsaken line creates an array pointer that points to an
     * array of temp scale cal arrays. Initialize it from the address of
     * first temp scale cal array. This allow users to specify in the PT
     * the temp scale calibrations for as many flow rates as they want
     * but requires that each temp scale calibration array is defined
     * consecutively. */
    const float (*mdot_idx_temp_scale_cal)[IDX_LIST_CNT][TEMP_SCALE_POLY_CNT] = (const float (*)[IDX_LIST_CNT][TEMP_SCALE_POLY_CNT])&PT->mdot_idx0_temp_scale_cal;

    uint32_t curr_i;
    float prev_mdot_v = 0.0f;
    float rtd2_in_degC = AD768x_CalibrateChannel(&Adc1, IN7);  /* RTD2_VOUT */
    float curr_mdot_v = calibrate(rtd2_in_degC, &(*mdot_idx_temp_scale_cal)[0][0], TEMP_SCALE_POLY_CNT);

    /* Work our way through all of the mdot voltage temp calibration curves until the
     * condition is satisfied prev_mdot_v < mfs_in_volt <= curr_mdot_v */
    for (curr_i = 1UL; curr_i < IDX_LIST_CNT; ++curr_i)
    {
        prev_mdot_v = curr_mdot_v;
        curr_mdot_v = calibrate(rtd2_in_degC, &(*mdot_idx_temp_scale_cal)[curr_i][0], TEMP_SCALE_POLY_CNT);

        if (mfs_in_volt <= curr_mdot_v)
        {
            /* Exit loop without incrementing curr_i */
            break;
        }
    }

    /* If searching the entire list of curves and the condition was never satisfied,
     * then it means that the mdot voltage is greater than anything that was calibrated
     * for. In this case, we'll want to use the last two curves to linear interpolate
     */
    if (curr_i >= IDX_LIST_CNT)
    {
        curr_i = IDX_LIST_CNT - 1UL;
    }

    /* Coerce value in the case when the actual is below the bounds of our curves
     * or in the case of unexpected values. Equality check helps avoid div-by-zero situations. */
    if ((mfs_in_volt <= prev_mdot_v) || (curr_mdot_v <= 0.0f) || (fabsf(curr_mdot_v - prev_mdot_v) <= FLT_EPSILON))
    {
        act_mdot_sccm = PT->temp_scale_mdot_idx_list[curr_i - 1UL];
    }
    else
    {
        /* Linearly interpolate between the two mdot ideal readings to get actual SCCM.
         * In the case where the measured flow is beyond the mdot ideal readings, then
         * this case also handles that using the same interpolation mechanism except
         * percent will be >100% */

        /* Use the ascending ordered list of supported SCCM to grab the SCCM definitions
         * for each curve. The actual SCCM will be somewhere between these two SCCMs */
        lower_curve_sccm = PT->temp_scale_mdot_idx_list[curr_i - 1UL];
        upper_curve_sccm = PT->temp_scale_mdot_idx_list[curr_i];

        /* Use linear interpolation to get the percentage between the two SCCMs such that
         * 0% = lower_curve_sccm and 100% = upper_curve_sccm */
        perc = (mfs_in_volt - prev_mdot_v) / (curr_mdot_v - prev_mdot_v);

        /* Apply the percentage to the curve sccm definitions */
        act_mdot_sccm = (perc * (upper_curve_sccm - lower_curve_sccm)) + lower_curve_sccm;
    }

    return act_mdot_sccm;
}


/* Get desired flow rate voltage by searching for the closest mdot cal curve based
 * on the desired sccm and then plugging in the current temperature to the calibration */
static float get_desired_mdot_v(float desired_mdot_sccm, float rtd2_in_degC)
{
    /* These values are linearly converted from a 16-bit ADC channel,
     * therefore any issues are going to be development ones. No need
     * to check for these errors manually. */
    DEV_ASSERT(isfinite(desired_mdot_sccm));
    DEV_ASSERT(isfinite(rtd2_in_degC));

    enum Constants
    {
        TEMP_SCALE_POLY_CNT     = sizeof(PT->mdot_idx0_temp_scale_cal)/sizeof(float),
        IDX_LIST_CNT            = sizeof(PT->temp_scale_mdot_idx_list)/sizeof(float),
    };

    uint32_t cal_idx;
    float new_mfs_mdot;

    /* This god-forsaken line creates an array pointer that points to an
     * array of temp scale cal arrays. Initialize it from the address of
     * first temp scale cal array. This allow users to specify in the PT
     * the temp scale calibrations for as many flow rates as they want
     * but requires that each temp scale calibration array is defined
     * consecutively. */
    const float (*mdot_idx_temp_scale_cal)[IDX_LIST_CNT][TEMP_SCALE_POLY_CNT] = (const float (*)[IDX_LIST_CNT][TEMP_SCALE_POLY_CNT])&PT->mdot_idx0_temp_scale_cal;

    /* Select the right temperature scale calibration coefficients based on the
     * desired flow rate setpoint */
    cal_idx = get_idx_of_closest_val(desired_mdot_sccm, &PT->temp_scale_mdot_idx_list[0], IDX_LIST_CNT);

    /* Calculate the desired MFS voltage using RTD2 temp and the selected
     * temp scale calibration coeffs */
    new_mfs_mdot = calibrate(rtd2_in_degC, &(*mdot_idx_temp_scale_cal)[cal_idx][0], TEMP_SCALE_POLY_CNT);

    return new_mfs_mdot;
}


/* This function calculates the difference of each value in the vals_arr from
 * the specified val, and returns the index with the smallest difference. */
static uint32_t get_idx_of_closest_val(float val, const float *vals_arr, uint32_t vals_arr_length)
{
    DEV_ASSERT(vals_arr);

    uint32_t i;
    float curr_diff = fabsf(vals_arr[0] - val);

    /* Start by assuming the first value in the array has the cloests match */
    uint32_t smallest_i = 0;
    float smallest_diff = curr_diff;

    /* Compare each of the other values in the array, checking for the
     * smallest difference and updating the index of the smallest difference
     * along the way. */
    for (i = 1UL; i < vals_arr_length; ++i)
    {
        curr_diff = fabsf(vals_arr[i] - val);
        if (curr_diff < smallest_diff)
        {
            smallest_i = i;
            smallest_diff = curr_diff;
        }
    }

    return smallest_i;
}
