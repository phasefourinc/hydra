/* Copyright (c) 2017-2020 by Phase Four, Inc. All Rights Reserved. Confidential and Proprietary.
 *
 * aux_driver.h
 */

#ifndef SOURCE_AUX_DRIVER_H_
#define SOURCE_AUX_DRIVER_H_
#include "mcu_types.h"
typedef enum {Null, Dot, Dash} morse_code_types;

/* This structure contains the morse code letter that is currently being ran */
typedef struct running_morse_code_container_type{
    char alphabetic_letter; /* The letter the code is to represent */
    uint8_t current_morse_dot_count; /* The amount of times the timer has been on this index of the letter. (1 for dot, 3 for dashes)*/
    uint8_t morse_letter_info_index; /*The index of the morse_letter_info we are in  (between 1 - 4) */
    bool break_between_morse_componet;  /*To determine if a dot or dash finished if it is we need to break of one dot.*/
    uint8_t  morse_info  [4];
} BYTE_PACKED running_morse_code_container_type;


/* This structure is the format to determine what is the morse code pattern for a given letter.*/
typedef struct morse_code_letter_type{
    char alphabetic_letter; /* The letter the code is to represent */
    uint8_t  morse_info  [4];
} BYTE_PACKED morse_code_letter_type;

/*This structure is to keep track at a character level where we are in the string*/
typedef struct  morse_code_string_tracker_type {
        uint16_t morse_break_count;
        uint16_t morse_break_total;
        uint16_t letter_index;
        bool morse_break_flag;
        bool new_letter;

}BYTE_PACKED morse_code_string_tracker_type;

typedef struct morse_code_tracker_type {

    char * next_employee_point;
    uint8_t employee_name_string_size;
    bool new_name_flag;
}BYTE_PACKED morse_code_tracker_type;

#define NUM_OF_EMPLOYEES 50
#define CHAR_PER_EMPLOYEE 50
#define TOTAL_MORSE_ALPHABET_LETTERS 27

morse_code_letter_type find_morse_letter(char letter_arg);
bool run_morse_letter(running_morse_code_container_type *morse_letter);
uint8_t find_string_size(char * morse_string);

void start_aux_driver(void);
void stop_aux_driver(bool turn_off_leds);

void turn_off_all_leds(void);
void turn_on_all_leds(void);
void run_morse_code(void);
bool run_morse_code_string(char * morse_string, uint8_t string_length);
bool prep_morse_code_char(morse_code_letter_type* morse_code_letter_object, uint8_t string_length);


#endif /* SOURCE_AUX_DRIVER_H_ */
