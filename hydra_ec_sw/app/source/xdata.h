/* Copyright (c) 2018-2020 by Phase Four, Inc. All Rights Reserved. Confidential and Proprietary.
 *
 * xdata.h
 */

#ifndef XDATA_CODE_XDATA_H_
#define XDATA_CODE_XDATA_H_

#include "mcu_types.h"
#include "timer64.h"

#define XTIME_UPDATE(xdata_addr, start_tick)    xdata_update(xdata_addr, timer32_get_duration_ticks(start_tick, timer32_get_tick()));
#define XDATA_UPDATE(xdata_addr, new_data)      xdata_update(xdata_addr, new_data);

typedef struct
{
    uint32_t min;
    uint32_t max;
} Data_Min_Max_t;

typedef struct
{
    Data_Min_Max_t os_frame_xtime;
    Data_Min_Max_t os_idle_xtime;
    Data_Min_Max_t os_full_util_xtime;
    Data_Min_Max_t scrub_xtime;
    Data_Min_Max_t scrub_ecc_1bit_cnt;

} Thruster_Exe_Data_t;


extern Thruster_Exe_Data_t Xdata;


static inline void xdata_update(Data_Min_Max_t *stored_data, uint32_t value)
{
    if((value < stored_data->min) || (stored_data->min == 0U))
    {
        stored_data->min = value;
    }
    if (value > stored_data->max || (stored_data->max == 0U))
    {
        stored_data->max = value;
    }
    return;
}


#endif /* XDATA_CODE_XDATA_H_ */
