/* Copyright (c) 2017-2020 by Phase Four, Inc. All Rights Reserved. Confidential and Proprietary.
 *
 * fidr.c
 */

#include "fidr.h"
#include "mcu_types.h"
#include "ErrAO.h"
#include "max_data.h"
#include "rft_data.h"
#include "default_pt.h"
#include "stdio.h"

#define FLOAT_EQUAL_TOLERANCE    0.1f

/* Arguments for the FIDR functions
 * Each of these arguments are custom selected relative to the type of function being called on.
 * This gives a lot of flexibility on making a specific function to determine a specific error condition.
 * To determine what each of these functions mean look at the argument type data struct.
 *
 * NOTE: These are initialized in the fdir init function
 */
two_equality_arg_type EcEbusHigh_args;
two_equality_arg_type EcEbusLow_args;
two_equality_arg_type EcIbusHigh_args;
two_equality_arg_type EcE12vHigh_args;
two_equality_arg_type EcE12vLow_args;
two_equality_arg_type EcE5vHigh_args;
two_equality_arg_type EcE5vLow_args;

two_equality_arg_type EcHeaterActive_args;

two_eq_u32_arg_type EcHeaterFuse_args;
two_eq_u32_arg_type EcHpisoFuse_args;
two_eq_u32_arg_type EcPfcvFuse_args;
two_eq_u32_arg_type EcRftFuse_args;

two_equality_arg_type TcE12vHigh_args;
two_equality_arg_type TcE12vLow_args;
two_equality_arg_type TcE5vHigh_args;
two_equality_arg_type TcE5vLow_args;
two_tlm_eq_aux_arg_type TcEvbLow_args;
two_equality_arg_type TcIhvdHigh_args;
two_equality_arg_type TcIhvdLow_args;

linear_eq_arg_type TcIhvdLow2_args;

two_tlm_eq_aux_arg_type EcFlowHigh_args;
two_equality_arg_type EcFlowLow_args;
two_equality_arg_type EcPpropHigh_args;

two_equality_arg_type TcTfbInHigh_args;
two_equality_arg_type TcTfbOutHigh_args;
two_equality_arg_type TcTfetHigh_args;
two_equality_arg_type TcTcoilHigh_args;
two_equality_arg_type TcTchokeHigh_args;
two_equality_arg_type TcTplHigh_args;
two_equality_arg_type TcTmatchHigh_args;

two_eq_u32_arg_type EcNoTcComm_args;
two_eq_u32_arg_type EcNoDaqComm_args;

two_eq_bool_arg_type EcTcAborted_args;


enum FDIR_Record_Idx
{
    EC_EBUS_HIGH = 0,
    EC_EBUS_LOW,
    EC_IBUS_HIGH,
    EC_E12V_HIGH,
    EC_E12V_LOW,
    EC_E5V_HIGH,
    EC_E5V_LOW,

    EC_HEATER_ACTIVE,

    EC_HEATER_FUSE,
    EC_HPISO_FUSE,
    EC_PFCV_FUSE,
    EC_RFT_FUSE,

    TC_E12V_HIGH,
    TC_E12V_LOW,
    TC_E5V_HIGH,
    TC_E5V_LOW,
    TC_VSET_LOW,
    TC_IHVD_HIGH,
    TC_IHVD_LOW,

    TC_IHVD_LOW2,

    EC_FLOW_HIGH,
    EC_FLOW_LOW,
    EC_PPROP_HIGH,

    TC_TFB_IN_HIGH,
    TC_TFB_OUT_HIGH,
    TC_TFET_HIGH,
    TC_TCOIL_HIGH,
    TC_TCHOKE_HIGH,
    TC_TPL_HIGH,
    TC_TMATCH_HIGH,

    EC_NO_TC_COMM,
    EC_NO_DAQ_COMM,
    EC_TC_ABORTED,

    TOTAL_FDIR_RECORD_CNT
};


/* FIDR table.
 * This is the table that is used to determine what error goes with what function and what arguments to put in said function.
 *
 *                      Error to trigger,       pre condition func,    function to determine FIDR,    Arguments to be used in function, FIDR Enable
 */
FDIR_Record FDIR_Table [] =
{
    {(const err_type**) &Errors.EcEbusHigh,     no_pre_func,            greater_than_func,            (void*) &EcEbusHigh_args,     true },
    {(const err_type**) &Errors.EcEbusLow,      no_pre_func,            less_than_func,               (void*) &EcEbusLow_args,      true },
    {(const err_type**) &Errors.EcIbusHigh,     no_pre_func,            greater_than_func,            (void*) &EcIbusHigh_args,     true },
    {(const err_type**) &Errors.EcE12vHigh,     no_pre_func,            greater_than_func,            (void*) &EcE12vHigh_args,     true },
    {(const err_type**) &Errors.EcE12vLow,      no_pre_func,            less_than_func,               (void*) &EcE12vLow_args,      true },
    {(const err_type**) &Errors.EcE5vHigh,      no_pre_func,            greater_than_func,            (void*) &EcE5vHigh_args,      true },
    {(const err_type**) &Errors.EcE5vLow,       no_pre_func,            less_than_func,               (void*) &EcE5vLow_args,       true },

    {(const err_type**) &Errors.EcHeaterActive, is_state_not_in_idle,   greater_than_func,            (void*) &EcHeaterActive_args, true },

    {(const err_type**) &Errors.EcHeaterFuse,   no_pre_func,            greater_than_func_u32,        (void*) &EcHeaterFuse_args,   true },
    {(const err_type**) &Errors.EcHpisoFuse,    no_pre_func,            greater_than_func_u32,        (void*) &EcHpisoFuse_args,    true },
    {(const err_type**) &Errors.EcPfcvFuse,     no_pre_func,            greater_than_func_u32,        (void*) &EcPfcvFuse_args,     true },
    {(const err_type**) &Errors.EcRftFuse,      no_pre_func,            greater_than_func_u32,        (void*) &EcRftFuse_args,      true },

    {(const err_type**) &Errors.TcE12vHigh,     is_tc_connected,        greater_than_func,            (void*) &TcE12vHigh_args,     true },
    {(const err_type**) &Errors.TcE12vLow,      is_tc_connected,        less_than_func,               (void*) &TcE12vLow_args,      true },
    {(const err_type**) &Errors.TcE5vHigh,      is_tc_connected,        greater_than_func,            (void*) &TcE5vHigh_args,      true },
    {(const err_type**) &Errors.TcE5vLow,       is_tc_connected,        less_than_func,               (void*) &TcE5vLow_args,       true },
    {(const err_type**) &Errors.TcVsetLow,      is_thrust_steady_state, tlmvtlm_less_than_offset_func,(void*) &TcEvbLow_args,       true },
    {(const err_type**) &Errors.TcIhvdHigh,     no_pre_func,            greater_than_func,            (void*) &TcIhvdHigh_args,     true },
    {(const err_type**) &Errors.TcIhvdLow,      is_thrust_steady_state, less_than_func,               (void*) &TcIhvdLow_args,      true },

    {(const err_type**) &Errors.TcIhvdLow,      is_thrust_steady_state, linear_eq_less_than_func,     (void*) &TcIhvdLow2_args,     true },

    {(const err_type**) &Errors.EcFlowHigh,     is_post_ignition,       tlmvtlm_greater_than_offset_func, (void*) &EcFlowHigh_args, true },
    {(const err_type**) &Errors.EcFlowLow,      is_post_ignition,       less_than_func,               (void*) &EcFlowLow_args,      true },
    {(const err_type**) &Errors.EcPpropHigh,    no_pre_func,            greater_than_func,            (void*) &EcPpropHigh_args,    true },

    {(const err_type**) &Errors.TcTfbInHigh,    is_tc_connected,        greater_than_func,            (void*) &TcTfbInHigh_args,    true },
    {(const err_type**) &Errors.TcTfbOutHigh,   is_tc_connected,        greater_than_func,            (void*) &TcTfbOutHigh_args,   true },
    {(const err_type**) &Errors.TcTfetHigh,     is_tc_connected,        greater_than_func,            (void*) &TcTfetHigh_args,     true },
    {(const err_type**) &Errors.TcTcoilHigh,    is_tc_connected,        greater_than_func,            (void*) &TcTcoilHigh_args,    true },
    {(const err_type**) &Errors.TcTchokeHigh,   is_tc_connected,        greater_than_func,            (void*) &TcTchokeHigh_args,   true },
    {(const err_type**) &Errors.TcTplHigh,      has_rft_and_tpl,        greater_than_func,            (void*) &TcTplHigh_args,      true },
    {(const err_type**) &Errors.TcTmatchHigh,   has_rft_and_tmatch,     greater_than_func,            (void*) &TcTmatchHigh_args,   true },

    {(const err_type**) &Errors.EcNoTcComm,     in_thrust_hot_fire,     greater_than_func_u32,        (void*) &EcNoTcComm_args,     true },
    {(const err_type**) &Errors.EcNoDaqComm,    no_pre_func,            greater_than_func_u32,        (void*) &EcNoDaqComm_args,    true },
    {(const err_type**) &Errors.EcTcAborted,    is_post_ignition,       eq_func_bool,                 (void*) &EcTcAborted_args,    true }
};


#define FDIR_INIT_2EQ(idx, tlm_data_item, en, prsis_sec, lim) do {                                               \
        fdir_idx_cnt[idx]++;                                                                                     \
        FDIR_Table[idx].enabled = en;                                                                            \
        ((two_equality_arg_type *)FDIR_Table[idx].args)->super.persistence_amount = prsis_sec / FRAME_PERIOD_MS; \
        ((two_equality_arg_type *)FDIR_Table[idx].args)->super.curr_persist_cnt   = 0UL;                         \
        ((two_equality_arg_type *)FDIR_Table[idx].args)->comparison_value         = lim;                         \
        ((two_equality_arg_type *)FDIR_Table[idx].args)->tlm_data                 = &tlm_data_item; } while(0)


/* WARNING: May be issues due to float to u32 conversion for the limit parameter.
 *          If float is a decimal it will round down, but is considered
 *          undefined behavior if float lim is > 0xFFFFFFFE. This is an extreme
 *          edge case we don't expect to see in the parameter table so just
 *          documenting here. */
#define FDIR_INIT_2EQ_U32(idx, tlm_data_item, en, prsis_sec, lim) do {                                         \
        fdir_idx_cnt[idx]++;                                                                                   \
        FDIR_Table[idx].enabled = en;                                                                          \
        ((two_eq_u32_arg_type *)FDIR_Table[idx].args)->super.persistence_amount = prsis_sec / FRAME_PERIOD_MS; \
        ((two_eq_u32_arg_type *)FDIR_Table[idx].args)->super.curr_persist_cnt   = 0UL;                         \
        ((two_eq_u32_arg_type *)FDIR_Table[idx].args)->comparison_value         = (uint32_t)lim;               \
        ((two_eq_u32_arg_type *)FDIR_Table[idx].args)->tlm_data                 = &tlm_data_item; } while(0)


#define FDIR_INIT_2TLM_AUX(idx, tlm1, tlm2, en, prsis_sec, lim) do {                                               \
        fdir_idx_cnt[idx]++;                                                                                       \
        FDIR_Table[idx].enabled = en;                                                                              \
        ((two_tlm_eq_aux_arg_type *)FDIR_Table[idx].args)->super.persistence_amount = prsis_sec / FRAME_PERIOD_MS; \
        ((two_tlm_eq_aux_arg_type *)FDIR_Table[idx].args)->super.curr_persist_cnt = 0UL;                           \
        ((two_tlm_eq_aux_arg_type *)FDIR_Table[idx].args)->tlm_data1 = &tlm1;                                      \
        ((two_tlm_eq_aux_arg_type *)FDIR_Table[idx].args)->tlm_data2 = &tlm2;                                      \
        ((two_tlm_eq_aux_arg_type *)FDIR_Table[idx].args)->aux_data  = lim; } while(0)


#define FDIR_INIT_LINEAR_EQ(idx, tlm1, lim_tlm2, en, prsis_sec, limit_c0, limit_c1) do {                      \
        fdir_idx_cnt[idx]++;                                                                                  \
        FDIR_Table[idx].enabled = en;                                                                         \
        ((linear_eq_arg_type *)FDIR_Table[idx].args)->super.persistence_amount = prsis_sec / FRAME_PERIOD_MS; \
        ((linear_eq_arg_type *)FDIR_Table[idx].args)->super.curr_persist_cnt = 0UL;                           \
        ((linear_eq_arg_type *)FDIR_Table[idx].args)->tlm_data1 = &tlm1;                                      \
        ((linear_eq_arg_type *)FDIR_Table[idx].args)->lim_tlm_data2 = &lim_tlm2;                              \
        ((linear_eq_arg_type *)FDIR_Table[idx].args)->lim_c0 = limit_c0;                                      \
        ((linear_eq_arg_type *)FDIR_Table[idx].args)->lim_c1 = limit_c1; } while(0)


/* WARNING: May be issues due to float to u32 conversion for the limit parameter.
 *          If float is a decimal it will round down, but is considered
 *          undefined behavior if float lim is > 0xFFFFFFFE. This is an extreme
 *          edge case we don't expect to see in the parameter table so just
 *          documenting here. */
#define FDIR_INIT_2EQ_BOOL(idx, tlm_data_item, en, prsis_sec, lim) do {                                         \
        fdir_idx_cnt[idx]++;                                                                                   \
        FDIR_Table[idx].enabled = en;                                                                          \
        ((two_eq_bool_arg_type *)FDIR_Table[idx].args)->super.persistence_amount = prsis_sec / FRAME_PERIOD_MS; \
        ((two_eq_bool_arg_type *)FDIR_Table[idx].args)->super.curr_persist_cnt   = 0UL;                         \
        ((two_eq_bool_arg_type *)FDIR_Table[idx].args)->comparison_value         = (uint32_t)lim;               \
        ((two_eq_bool_arg_type *)FDIR_Table[idx].args)->tlm_data                 = &tlm_data_item; } while(0)


void fidr_init(void)
{
    /* Validate that the enumerated index has the same count as the FDIR Record */
    DEV_ASSERT(TOTAL_FDIR_RECORD_CNT == ((sizeof(FDIR_Table)/sizeof(FDIR_Table[0]))));

    uint8_t fdir_idx_cnt[TOTAL_FDIR_RECORD_CNT] = {0U};
    uint32_t i;

    FDIR_INIT_2EQ(EC_EBUS_HIGH,       MaxData.ebus,         PT->fdir_enabl_ec_ebus_high,     PT->fdir_prsis_ec_ebus_high,       PT->fdir_limit_ec_ebus_high);
    FDIR_INIT_2EQ(EC_EBUS_LOW,        MaxData.ebus,         PT->fdir_enabl_ec_ebus_low,      PT->fdir_prsis_ec_ebus_low,        PT->fdir_limit_ec_ebus_low);
    FDIR_INIT_2EQ(EC_IBUS_HIGH,       MaxData.ibus,         PT->fdir_enabl_ec_ibus_high,     PT->fdir_prsis_ec_ibus_high,       PT->fdir_limit_ec_ibus_high);
    FDIR_INIT_2EQ(EC_E12V_HIGH,       MaxData.e12v,         PT->fdir_enabl_ec_e12v_high,     PT->fdir_prsis_ec_e12v_high,       PT->fdir_limit_ec_e12v_high);
    FDIR_INIT_2EQ(EC_E12V_LOW,        MaxData.e12v,         PT->fdir_enabl_ec_e12v_low,      PT->fdir_prsis_ec_e12v_low,        PT->fdir_limit_ec_e12v_low);
    FDIR_INIT_2EQ(EC_E5V_HIGH,        MaxData.e5v,          PT->fdir_enabl_ec_e5v_high,      PT->fdir_prsis_ec_e5v_high,        PT->fdir_limit_ec_e5v_high);
    FDIR_INIT_2EQ(EC_E5V_LOW,         MaxData.e5v,          PT->fdir_enabl_ec_e5v_low,       PT->fdir_prsis_ec_e5v_low,         PT->fdir_limit_ec_e5v_low);

    FDIR_INIT_2EQ(EC_HEATER_ACTIVE,   MaxData.iheater,      PT->fdir_enabl_ec_heater_active, PT->fdir_prsis_ec_heater_active,   PT->fdir_limit_ec_heater_active);

    FDIR_INIT_2EQ_U32(EC_HEATER_FUSE, MaxData.heater_fault, PT->fdir_enabl_ec_heater_fuse,   PT->fdir_prsis_ec_heater_fuse,     PT->fdir_limit_ec_heater_fuse);
    FDIR_INIT_2EQ_U32(EC_HPISO_FUSE,  MaxData.hpiso_fault,  PT->fdir_enabl_ec_hpiso_fuse,    PT->fdir_prsis_ec_hpiso_fuse,      PT->fdir_limit_ec_hpiso_fuse);
    FDIR_INIT_2EQ_U32(EC_PFCV_FUSE,   MaxData.pfcv_fault,   PT->fdir_enabl_ec_pfcv_fuse,     PT->fdir_prsis_ec_pfcv_fuse,       PT->fdir_limit_ec_pfcv_fuse);
    FDIR_INIT_2EQ_U32(EC_RFT_FUSE,    MaxData.rft_fault,    PT->fdir_enabl_ec_rft_fuse,      PT->fdir_prsis_ec_rft_fuse,        PT->fdir_limit_ec_rft_fuse);

    FDIR_INIT_2EQ(TC_E12V_HIGH,       TcData.e12v,          PT->fdir_enabl_tc_e12v_high,     PT->fdir_prsis_tc_e12v_high,       PT->fdir_limit_tc_e12v_high);
    FDIR_INIT_2EQ(TC_E12V_LOW,        TcData.e12v,          PT->fdir_enabl_tc_e12v_low,      PT->fdir_prsis_tc_e12v_low,        PT->fdir_limit_tc_e12v_low);
    FDIR_INIT_2EQ(TC_E5V_HIGH,        TcData.e5v,           PT->fdir_enabl_tc_e5v_high,      PT->fdir_prsis_tc_e5v_high,        PT->fdir_limit_tc_e5v_high);
    FDIR_INIT_2EQ(TC_E5V_LOW,         TcData.e5v,           PT->fdir_enabl_tc_e5v_low,       PT->fdir_prsis_tc_e5v_low,         PT->fdir_limit_tc_e5v_low);

    FDIR_INIT_2TLM_AUX(TC_VSET_LOW,   TcData.evb, TcData.pp_vset_fb, PT->fdir_enabl_tc_vset_low, PT->fdir_prsis_tc_vset_low,    PT->fdir_limit_tc_vset_low);

    FDIR_INIT_2EQ(TC_IHVD_HIGH,       TcData.pvb,           PT->fdir_enabl_tc_ihvd_high,     PT->fdir_prsis_tc_ihvd_high,       PT->fdir_limit_tc_ihvd_high);
    FDIR_INIT_2EQ(TC_IHVD_LOW,        TcData.ivb,           PT->fdir_enabl_tc_ihvd_low,      PT->fdir_prsis_tc_ihvd_low,        PT->fdir_limit_tc_ihvd_low);

    FDIR_INIT_LINEAR_EQ(TC_IHVD_LOW2, TcData.ivb, TcData.evb, PT->fdir_enabl_tc_ihvd_low2, PT->fdir_prsis_tc_ihvd_low2, PT->fdir_limit_tc_ihvd_low2_c0, PT->fdir_limit_tc_ihvd_low2_c1);

    FDIR_INIT_2TLM_AUX(EC_FLOW_HIGH,  MaxData.mfs_mdot, MaxData.thrust_mdot_fb, PT->fdir_enabl_ec_flow_high, PT->fdir_prsis_ec_flow_high, PT->fdir_limit_ec_flow_high);
    FDIR_INIT_2EQ(EC_FLOW_LOW,        MaxData.mfs_mdot,     PT->fdir_enabl_ec_flow_low,      PT->fdir_prsis_ec_flow_low,        PT->fdir_limit_ec_flow_low);
    FDIR_INIT_2EQ(EC_PPROP_HIGH,      MaxData.pprop,        PT->fdir_enabl_ec_pprop_high,    PT->fdir_prsis_ec_pprop_high,      PT->fdir_limit_ec_pprop_high);

    FDIR_INIT_2EQ(TC_TFB_IN_HIGH,     TcData.tfb_in,        PT->fdir_enabl_tc_tfb_in_high,   PT->fdir_prsis_tc_tfb_in_high,     PT->fdir_limit_tc_tfb_in_high);
    FDIR_INIT_2EQ(TC_TFB_OUT_HIGH,    TcData.tfb_out,       PT->fdir_enabl_tc_tfb_out_high,  PT->fdir_prsis_tc_tfb_out_high,    PT->fdir_limit_tc_tfb_out_high);
    FDIR_INIT_2EQ(TC_TFET_HIGH,       TcData.tfet,          PT->fdir_enabl_tc_tfet_high,     PT->fdir_prsis_tc_tfet_high,       PT->fdir_limit_tc_tfet_high);
    FDIR_INIT_2EQ(TC_TCOIL_HIGH,      TcData.tcoil,         PT->fdir_enabl_tc_tcoil_high,    PT->fdir_prsis_tc_tcoil_high,      PT->fdir_limit_tc_tcoil_high);
    FDIR_INIT_2EQ(TC_TCHOKE_HIGH,     TcData.tchoke,        PT->fdir_enabl_tc_tchoke_high,   PT->fdir_prsis_tc_tchoke_high,     PT->fdir_limit_tc_tchoke_high);
    FDIR_INIT_2EQ(TC_TPL_HIGH,        TcData.tpl,           PT->fdir_enabl_tc_tpl_high,      PT->fdir_prsis_tc_tpl_high,        PT->fdir_limit_tc_tpl_high);
    FDIR_INIT_2EQ(TC_TMATCH_HIGH,     TcData.tmatch,        PT->fdir_enabl_tc_tmatch_high,   PT->fdir_prsis_tc_tmatch_high,     PT->fdir_limit_tc_tmatch_high);

    FDIR_INIT_2EQ_U32(EC_NO_TC_COMM,  MaxData.tc_comm_fail_cnt,   PT->fdir_enabl_ec_no_tc_comm,  PT->fdir_prsis_ec_no_tc_comm,  PT->fdir_limit_ec_no_tc_comm);
    FDIR_INIT_2EQ_U32(EC_NO_DAQ_COMM, MaxData.daq_consec_err_cnt, PT->fdir_enabl_ec_no_daq_comm, PT->fdir_prsis_ec_no_daq_comm, PT->fdir_limit_ec_no_daq_comm);
    FDIR_INIT_2EQ_BOOL(EC_TC_ABORTED, TcData.gpio_wf_en_fb,       PT->fdir_enabl_ec_tc_aborted,  PT->fdir_prsis_ec_tc_aborted,  PT->fdir_limit_ec_tc_aborted);

    /* Each macro increments the idx cnt. Check these
     * to ensure that all items in the FDIR table have
     * been initialized once. This is a dev check only,
     * therefore DEV_ASSERT should suffice. */
    for (i = 0UL; i < TOTAL_FDIR_RECORD_CNT; ++i)
    {
        DEV_ASSERT(fdir_idx_cnt[i] == 1U);
    }

    return;
}

/*
 * CURRENTLY NOT USED. THIS IS A IDEA TO HAVE A GENEREAL FUNCTION TO DO ALL FIDR CHECK> IT MIGHT BE NOT AS FAST HOWEVER.
 * STILL UP IN THE AIR IF THIS WILL HAVE SOME USE.
 *
 *
bool equality_function(FDIR_Record record)

{
    bool passed_eqaulity;
    switch (record.arguments.two_equality_arg.equality_value)
    {
        case EQUAL:
            if (TLM_DATA(record) == COMPARISION_VALUE(record))
            {
                passed_eqaulity = true;
            }
            else{
                passed_eqaulity = false;
            }
            break;

        case NOT_EQUAL:
            if (TLM_DATA(record) != COMPARISION_VALUE(record))
            {
                passed_eqaulity = true;
            }
            else{
                passed_eqaulity = false;
            }

            break;
        case GREATER_THAN:
            if (TLM_DATA(record) >COMPARISION_VALUE(record))
            {
                passed_eqaulity = true;
            }
            else{
                passed_eqaulity = false;
            }

            break;
        case LESS_THAN:
            if (TLM_DATA(record) <COMPARISION_VALUE(record))
            {
                passed_eqaulity = true;
            }
            else{
                passed_eqaulity = false;
            }
            break;
        case GREATER_THAN_OR_EQUAL_TO:
            if (TLM_DATA(record) >=COMPARISION_VALUE(record))
            {
                passed_eqaulity = true;
            }
            else{
                passed_eqaulity = false;
            }
            break;
        case LESS_THAN_OR_EQUAL_TO:
            if (TLM_DATA(record)<=COMPARISION_VALUE(record))
             {
                 passed_eqaulity = true;
             }
             else{
                 passed_eqaulity = false;
             }
             break;
    }

    if (passed_eqaulity ==true)
    {
    _report_fidr_error((err_type *)*(record.error_fidr),SEND_TO_SPACECRAFT,(float)*record.arguments.two_equality_arg.tlm_data);
    return true;
    }
    return false;

}
*/

/*Comparison Function: Greater Than Function
 *  This function compares a value from memory to see if it is greater than a known float value.
 *  If the check is true it then gets checked for persistence.
 *  If it pass persistence it will log a error to the Error AO
 */
bool greater_than_func(FDIR_Record *record)
{
    bool error_logged = false;
    two_equality_arg_type *arguments = (two_equality_arg_type*) record->args;
    if ((float)*arguments->tlm_data > arguments->comparison_value)
    {
        error_logged = check_persistence_and_log_error(record,(float)*arguments->tlm_data);
    }
    else
    {
        arguments->super.curr_persist_cnt = ZERO_COUNT;
    }
    return error_logged;

}


/*Comparison Function: Greater than Or Equal To Function
 *  This function compares a value from memory to see if it is greater than or equal to a known float value.
 *  If the check is true it then gets checked for persistence.
 *  If it pass persistence it will log a error to the Error AO
 */
bool greater_than_eq_func(FDIR_Record *record)
{
    bool error_logged = false;
    two_equality_arg_type *arguments = (two_equality_arg_type*) record->args;
    if ((float)*arguments->tlm_data >=arguments->comparison_value)
    {
        error_logged = check_persistence_and_log_error(record,(float)*arguments->tlm_data);
    }
    else
    {
        arguments->super.curr_persist_cnt = ZERO_COUNT;
    }
    return error_logged;

}


/*Comparison Function: Less Than Function
 *  This function compares a value from memory to see if it is less than to a known float value.
 *  If the check is true it then gets checked for persistence.
 *  If it pass persistence it will log a error to the Error AO
 */
bool less_than_func(FDIR_Record *record)
{

    two_equality_arg_type *arguments = (two_equality_arg_type*) record->args;
    bool error_logged = false;
    if ((float)*arguments->tlm_data <arguments->comparison_value)
    {
        error_logged = check_persistence_and_log_error(record,(float)*arguments->tlm_data);
    }
    else{
        arguments->super.curr_persist_cnt = ZERO_COUNT;
    }
    return error_logged;

}


/*Comparison Function: Less than Or Equal To Function
 *  This function compares a value from memory to see if it is less than or equal to a known float value.
 *  If the check is true it then gets checked for persistence.
 *  If it pass persistence it will log a error to the Error AO
 */
bool less_than_eq_func(FDIR_Record *record)
{
    bool error_logged = false;
    two_equality_arg_type *arguments = (two_equality_arg_type*) record->args;
    if ((float)*arguments->tlm_data <=arguments->comparison_value)
    {
        error_logged = check_persistence_and_log_error(record,(float)*arguments->tlm_data);
    }
    else
    {
        arguments->super.curr_persist_cnt = ZERO_COUNT;
    }
    return error_logged;

}


/*Comparison Function: Equal Comparison Function
 *  This function compares a value from memory to see if it is equal to a known float value.
 *  If the check is true it then gets checked for persistence.
 *  If it pass persistence it will log a error to the Error AO
 */
bool equal_comp_func(FDIR_Record *record)
{
    bool error_logged = false;
    two_equality_arg_type *arguments = (two_equality_arg_type*) record->args;
    if ((float)*arguments->tlm_data <= arguments->comparison_value +(arguments->comparison_value*FLOAT_EQUAL_TOLERANCE)  && (float)*arguments->tlm_data >= arguments->comparison_value -(arguments->comparison_value*FLOAT_EQUAL_TOLERANCE))
    {
        error_logged = check_persistence_and_log_error(record,(float)*arguments->tlm_data);
    }
    else
    {
        arguments->super.curr_persist_cnt = ZERO_COUNT;
    }
    return error_logged;

}


/*Comparison Function: Not Equal Comparison Function
 *  This function compares a value from memory to see if it is not equal to a known float value.
 *  If the check is true it then gets checked for persistence.
 *  If it pass persistence it will log a error to the Error AO
 */
bool not_equal_comp_func(FDIR_Record *record)
{
    bool error_logged = false;
    two_equality_arg_type *arguments = (two_equality_arg_type*) record->args;
    if ((float)*arguments->tlm_data > arguments->comparison_value +(arguments->comparison_value*FLOAT_EQUAL_TOLERANCE)  || (float)*arguments->tlm_data < arguments->comparison_value -(arguments->comparison_value*FLOAT_EQUAL_TOLERANCE))
    {
        error_logged = check_persistence_and_log_error(record,(float)*arguments->tlm_data);
    }
    else
    {
        arguments->super.curr_persist_cnt = ZERO_COUNT;
    }
    return error_logged;

}


/*Comparison Function of two telemetry: Greater Than Function
 *  This function compares a value from memory to see if it is greater than another value from memory.
 *  This can be used to compare two adc values for example.
 *  If the check is true it then gets checked for persistence.
 *  If it pass persistence it will log a error to the Error AO
 */
bool tlmvtlm_greater_than_func(FDIR_Record *record)
{
    two_tlm_eqaulity_arg_type *arguments = (two_tlm_eqaulity_arg_type*)record->args;
    bool error_logged = false;
    float aux_data;
    if ((float)*arguments->tlm_data1 > (float)*arguments->tlm_data2)
    {
        if (arguments->aux_data_select == ARG1)
        {
            aux_data = (float)*arguments->tlm_data1;
        }
        else
        {
            aux_data = (float)*arguments->tlm_data2;
        }
        error_logged = check_persistence_and_log_error(record, aux_data);
    }
    else
    {
        arguments->super.curr_persist_cnt = ZERO_COUNT;
    }
    return error_logged;

}


/* Comparison Function of two telemetry: Less Than with Offset Function
 *  This function compares a TLM1 item if it is less than TLM2 + Aux_Data_Offset.
 *  This can be used to compare two adc values for example.
 *  If the check is true it then gets checked for persistence.
 *  If it pass persistence it will log a error to the Error AO
 */
bool tlmvtlm_less_than_offset_func(FDIR_Record *record)
{
    two_tlm_eq_aux_arg_type *arguments = (two_tlm_eq_aux_arg_type*)record->args;
    bool error_logged = false;
    float cmp_val = *arguments->tlm_data2 + arguments->aux_data;

    if (*arguments->tlm_data1 < cmp_val)
    {
        error_logged = check_persistence_and_log_error(record, cmp_val);
    }
    else
    {
        arguments->super.curr_persist_cnt = ZERO_COUNT;
    }
    return error_logged;

}


/* Comparison Function of two telemetry: Greater Than with Offset Function
 *  This function compares a TLM1 item if it is greater than TLM2 + Aux_Data_Offset.
 *  This can be used to compare two adc values for example.
 *  If the check is true it then gets checked for persistence.
 *  If it pass persistence it will log a error to the Error AO
 */
bool tlmvtlm_greater_than_offset_func(FDIR_Record *record)
{
    two_tlm_eq_aux_arg_type *arguments = (two_tlm_eq_aux_arg_type*)record->args;
    bool error_logged = false;
    float cmp_val = *arguments->tlm_data2 + arguments->aux_data;

    if (*arguments->tlm_data1 > cmp_val)
    {
        error_logged = check_persistence_and_log_error(record, cmp_val);
    }
    else
    {
        arguments->super.curr_persist_cnt = ZERO_COUNT;
    }
    return error_logged;

}


/*Comparison Function: Greater Then Function
 *  This function compares a value from memory(casted to uint32_t) to see if it is greater then a known uint32 value.
 *  If the check is true it then gets checked for persistence.
 *  If it pass persistence it will log a error to the Error AO
 */
bool greater_than_func_u32(FDIR_Record *record)
{
    bool error_logged = false;
    two_eq_u32_arg_type *arguments = (two_eq_u32_arg_type*) record->args;
    if ((uint32_t)*arguments->tlm_data > arguments->comparison_value)
    {
        error_logged = check_persistence_and_log_error(record,(float)*arguments->tlm_data);
    }
    else
    {
        arguments->super.curr_persist_cnt = ZERO_COUNT;
    }
    return error_logged;

}


/*Comparison Function: Bool equal function
 *  This function compares a boolean value from memory(casted to uint32_t) to see if it equals uint32_t casted boolean value.
 *  If the check is true it then gets checked for persistence.
 *  If it pass persistence it will log a error to the Error AO
 */
bool eq_func_bool(FDIR_Record *record)
{
    bool error_logged = false;
    two_eq_bool_arg_type *arguments = (two_eq_bool_arg_type*) record->args;
    if ((uint32_t)*arguments->tlm_data == arguments->comparison_value)
    {
        error_logged = check_persistence_and_log_error(record,(float)*arguments->tlm_data);
    }
    else
    {
        arguments->super.curr_persist_cnt = ZERO_COUNT;
    }
    return error_logged;
}


/* Comparison Function: Linear equation less than function
 *  This function compares a TLM1 item if it is less than TLM2*LIM_C1 + LIM_C0
 *  This can be used to compare two adc values for example.
 *  If the check is true it then gets checked for persistence.
 *  If it pass persistence it will log a error to the Error AO
 */
bool linear_eq_less_than_func(FDIR_Record *record)
{
    linear_eq_arg_type *arguments = (linear_eq_arg_type*)record->args;
    bool error_logged = false;
    float cmp_val = ((*arguments->lim_tlm_data2) * arguments->lim_c1) + arguments->lim_c0;

    if ((*arguments->tlm_data1) < cmp_val)
    {
        error_logged = check_persistence_and_log_error(record, cmp_val);
    }
    else
    {
        arguments->super.curr_persist_cnt = ZERO_COUNT;
    }
    return error_logged;

}


/*Check Persistence Function
 *  This function is a lower level function that is used if the fidr requires persistence.
 *  This functions uses the arguments to check if the current persistence value is equal to the persistence limit specified from initaliziation.
 *  If it is the current persistence value and the persistence limit are the same it will log the fidr error.
 *  If it is not the same it will just increment by one.
 *
 *  NOTE: That if it goes beyond the persistence value, it will never report another error.
 *  It will report another error once the count is cleared by having the condition that cause fidr cleared.
 */

bool check_persistence_and_log_error(FDIR_Record *record, float aux_data)
{
    two_equality_arg_type *arguments = (two_equality_arg_type*) record->args;
    bool error_logged = false;

    /* Check for exact equality since no errors should be reported if the
     * fault remains active without having been cleared first */
    if (arguments->super.curr_persist_cnt == arguments->super.persistence_amount)
    {
        _report_fidr_error((err_type *)*(record->error_fidr), SEND_TO_SPACECRAFT, aux_data);
        error_logged = true;
    }

    if (arguments->super.curr_persist_cnt < 0xFFFFFFFFUL)
    {
        ++arguments->super.curr_persist_cnt;
    }
    return error_logged;

}


/*This is the main function that runs all of FIDR
 * This function first checks if the FIDR is enabled.
 * If it is, it will check the FIDR the condition to see if it passed or failed.
 * */

void run_fidr(void)
{
    uint32_t i= 0UL;
    FDIR_Record *fdir_rec;

    if (MaxData.state != MAX_WILD_WEST)
    {
        for (i = 0UL; i < TOTAL_FDIR_RECORD_CNT; ++i)
        {
            fdir_rec = &FDIR_Table[i];
            if ((fdir_rec->enabled==true) && (fdir_rec->prefunc() == true))
            {
                fdir_rec->fidr_func(fdir_rec);
            }
            /* If we are not in the pre-function then lets reset the persistence count*/
            else{
                fdir_arg_base_t *fdir_arg_base = (fdir_arg_base_t *)fdir_rec->args;
                fdir_arg_base->curr_persist_cnt = ZERO_COUNT;

            }
        }
    }

    return;
}


bool fidr_has_active_fault(void)
{
    uint32_t i= 0UL;
    bool in_fdir = false;
    FDIR_Record *fdir_rec;
    fdir_arg_base_t *fdir_arg_base;

    if (MaxData.state != MAX_WILD_WEST)
    {
        for (i = 0UL; (i < TOTAL_FDIR_RECORD_CNT) && (!in_fdir); ++i)
        {
            fdir_rec = &FDIR_Table[i];
            fdir_arg_base = (fdir_arg_base_t *)fdir_rec->args;

            /* Check active FDIR items for a persistence count that is not cleared */
            in_fdir = (fdir_rec->enabled) && (fdir_arg_base->curr_persist_cnt > fdir_arg_base->persistence_amount);
        }
    }

    return in_fdir;
}


void fidr_log_all_persistant_faults(void)
{
    uint32_t i= 0UL;
    bool in_fdir = false;
    FDIR_Record *fdir_rec;
    fdir_arg_base_t *fdir_arg_base;

    reported_error_record current_error;
    current_error.aux_data = 0UL;
    current_error.glados_state = MaxData.state;
    current_error.send_to_spacecraft = SEND_TO_SPACECRAFT;

    for (i = 0UL; (i < TOTAL_FDIR_RECORD_CNT); ++i)
    {
        fdir_rec = &FDIR_Table[i];
        fdir_arg_base = (fdir_arg_base_t *)fdir_rec->args;

        /* Check active FDIR items for a persistence count that is not cleared */
        in_fdir = (fdir_rec->enabled) && (fdir_arg_base->curr_persist_cnt > fdir_arg_base->persistence_amount);

        if (in_fdir)
        {
            current_error.err_type_pointer = (err_type *)*(fdir_rec->error_fidr);
            write_telem_record(current_error);
            write_sram_record(current_error);
        }
    }

    return;
}


bool no_pre_func(void)
{
    return true;
}


bool is_state_not_in_idle(void)
{
    if ((MaxData.state == MAX_IDLE) || (MaxData.state == MAX_MANUAL) ||
        (MaxData.state == MAX_WILD_WEST) || (MaxData.state == MAX_BIST))
    {
        return false;
    }
    return true;
}

bool is_state_manual(void)
{
    return (MaxData.state == MAX_MANUAL);
}

bool is_state_sw_update(void)
{
    return (MaxData.state == MAX_SW_UPDATE);
}

bool is_state_in_thrust(void)
{
    return (MaxData.state == MAX_THRUST);
}


bool is_heater_connected(void)
{
    return MaxData.heater_connected;
}

bool is_mfs_connected(void)
{
    return MaxData.mfs_connected;
}

bool is_post_ignition(void)
{
    return MaxData.thrust_has_ignited;
}

bool is_thrust_steady_state(void)
{
    return MaxData.thrust_steady_state;
}

bool is_tc_connected(void)
{
    return MaxData.tc_connected;
}

bool has_rft_and_tpl(void)
{
    return (MaxData.tc_connected && (TcData.tpl < 400.0f));
}

bool has_rft_and_tmatch(void)
{
    return (MaxData.tc_connected && (TcData.tmatch < 400.0f));
}

bool in_thrust_hot_fire(void)
{
    return MaxData.thrust_in_hot_fire;
}

