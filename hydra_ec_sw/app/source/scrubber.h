/* Copyright (c) 2017-2020 by Phase Four, Inc. All Rights Reserved. Confidential and Proprietary.
 *
 * scrubber.h
 */

#ifndef SOURCE_SCRUBBER_H_
#define SOURCE_SCRUBBER_H_

/* Global invariants:
 *  - Do not optimize this module as it may optimize the memory reads
 *
 * Notes:
 *  - When radiation testing, the SCRUB_U32_PER_CYCLE should be increased
 *    to permit faster ECC detection
 */
#include "mcu_types.h"

/* The number of bytes to scrub each cycle. This value should exactly
 * divide into the start and stop addresses. Doing it this way, while
 * restricting some flexibility, gains in performance.
 *
 * Notes:
 *  - 128  ->  50us per idle frame, 50ms to complete cycle
 *  - 1024 -> 300us per idle frame, 25ms to complete cycle
 *  - More than 1024 does not evenly divide into memory region
 */
#define SCRUB_U32_PER_CYCLE    128U

#define SCRUB_MEM_START        0x1FFE0000UL
#define SCRUB_MEM_STOP         0x2001F000UL

extern bool scrub_active;
extern uint32_t scrub_ecc_1bit_cnt;

void Scrub_Init(void);
void Scrub_MemoryProcess(void);

static inline bool Scrub_IsActive(void)
{
    return scrub_active;
}

static inline void Scrub_IncEccCnt(void)
{
    ++scrub_ecc_1bit_cnt;
    return;
}

#endif /* SOURCE_SCRUBBER_H_ */
