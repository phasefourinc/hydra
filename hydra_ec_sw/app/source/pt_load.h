/* Copyright (c) 2017-2020 by Phase Four, Inc. All Rights Reserved. Confidential and Proprietary.
 *
 * pt_load.h
 */

#ifndef SOURCE_PT_LOAD_H_
#define SOURCE_PT_LOAD_H_


/* Global invariants:
 *  - PFlash ECC double-bit faults disabled during PT loading, since leaving
 *    this fault enabled could potentially prevent the PT loading scheme from
 *    ever fixing an nbit error.
 *
 */

#include "default_pt.h"
#include "max_info_driver.h"
#include "p4_err.h"

#define PT_LOAD_GOLD_IMG_ADDR         PARM_TABLE_GOLD
#define PT_LOAD_GOLD_IMG_HDR_ADDR     (PT_LOAD_GOLD_IMG_ADDR)
#define PT_LOAD_GOLD_IMG_DATA_ADDR    (PT_LOAD_GOLD_IMG_HDR_ADDR + IMG_HDR_MAX_SIZE)
#define PT_LOAD_GOLD_PROP_INV_ADDR    (PT_LOAD_GOLD_IMG_DATA_ADDR)
#define PT_LOAD_GOLD_CORE_ADDR        (PT_LOAD_GOLD_IMG_DATA_ADDR + PARAM_TABLE_PROP_INV_SIZE)

#define PT_LOAD_APP1_IMG_ADDR         PARM_TABLE_APP1
#define PT_LOAD_APP1_IMG_HDR_ADDR     (PT_LOAD_APP1_IMG_ADDR)
#define PT_LOAD_APP1_IMG_DATA_ADDR    (PT_LOAD_APP1_IMG_HDR_ADDR + IMG_HDR_MAX_SIZE)
#define PT_LOAD_APP1_PROP_INV_ADDR    (PT_LOAD_APP1_IMG_DATA_ADDR)
#define PT_LOAD_APP1_CORE_ADDR        (PT_LOAD_APP1_IMG_DATA_ADDR + PARAM_TABLE_PROP_INV_SIZE)

#define PT_LOAD_APP2_IMG_ADDR         PARM_TABLE_APP2
#define PT_LOAD_APP2_IMG_HDR_ADDR     (PT_LOAD_APP2_IMG_ADDR)
#define PT_LOAD_APP2_IMG_DATA_ADDR    (PT_LOAD_APP2_IMG_HDR_ADDR + IMG_HDR_MAX_SIZE)
#define PT_LOAD_APP2_PROP_INV_ADDR    (PT_LOAD_APP2_IMG_DATA_ADDR)
#define PT_LOAD_APP2_CORE_ADDR        (PT_LOAD_APP2_IMG_DATA_ADDR + PARAM_TABLE_PROP_INV_SIZE)

#define PT_LOAD_RUN_IMG_ADDR          PARM_TABLE_RUN
#define PT_LOAD_RUN_IMG_HDR_ADDR      (PT_LOAD_RUN_IMG_ADDR)
#define PT_LOAD_RUN_IMG_DATA_ADDR     (PT_LOAD_RUN_IMG_HDR_ADDR + IMG_HDR_MAX_SIZE)
#define PT_LOAD_RUN_PROP_INV_ADDR     (PT_LOAD_RUN_IMG_DATA_ADDR)
#define PT_LOAD_RUN_CORE_ADDR         (PT_LOAD_RUN_IMG_DATA_ADDR + PARAM_TABLE_PROP_INV_SIZE)


/* This function expects to be called at startup, after MaxData initialization
 * but before Active Objects are initialized */
void pt_load_init(void);

/* This function expects to be called at a time after the error reporting
 * mechanisms have been initialized. */
void pt_load_report_any_errors(void);

#endif /* SOURCE_PT_LOAD_H_ */
