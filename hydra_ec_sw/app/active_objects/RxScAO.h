/* Copyright (c) 2017-2020 by Phase Four, Inc. All Rights Reserved. Confidential and Proprietary.
 *
 * RxScAO.h
 */

#ifndef ACTIVE_OBJECTS_RXSCAO_H_
#define ACTIVE_OBJECTS_RXSCAO_H_

/* Global invariants:
 *  - No constants that might affect reception of data should be placed in the PT, lest run
 *    the risk of having a valid PT loaded and then the user is unable to communicate.
 */

#include "mcu_types.h"
#include "glados.h"
#include "uart_ring.h"

#define RXSCAO_PRIORITY        10UL
#define RXSCAO_FIFO_SIZE       6UL


/* Each of these must be unique values for the entire system */
enum RxScAO_event_names
{
    EVT_TMR_RXSC_TIMEOUT = (RXSCAO_PRIORITY << 16U),
    EVT_TMR_RXSC_CMD_TIMEOUT,
    EVT_UART0_RX_DATA,
    EVT_RXSC_REINIT,
    EVT_RXSC_START_RX,
    EVT_RXSC_CMD_RCVD,
    EVT_TMR_RXSC_DROPOUT_DELAY
};

extern GLADOS_AO_t AO_RxSc;
extern GLADOS_Timer_t Tmr_RxScTimeout;
extern GLADOS_Timer_t Tmr_RxScCmdTimeout;
extern GLADOS_Timer_t Tmr_RxScDropoutDelay;

void RxScAO_init(void);
void RxScAO_push_interrupts(void);
void RxScAO_state(GLADOS_AO_t *this_ao, GLADOS_Event_t *event);
void RxScAO_state_rx_reinit(GLADOS_AO_t *this_ao, GLADOS_Event_t *event);
void RxScAO_state_rx_start(GLADOS_AO_t *this_ao, GLADOS_Event_t *event);
void RxScAO_state_rx_data(GLADOS_AO_t *this_ao, GLADOS_Event_t *event);
void RxScAO_state_rx_data_payload(GLADOS_AO_t *this_ao, GLADOS_Event_t *event);
void RxScAO_state_rx_cmd_ack(GLADOS_AO_t *this_ao, GLADOS_Event_t *event);


#endif /* ACTIVE_OBJECTS_RXSCAO_H_ */
