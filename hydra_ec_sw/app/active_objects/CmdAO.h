/* Copyright (c) 2017-2020 by Phase Four, Inc. All Rights Reserved. Confidential and Proprietary.
 *
 * CmdAO.h
 */

#ifndef ACTIVE_OBJECTS_CMDAO_H_
#define ACTIVE_OBJECTS_CMDAO_H_

#include "mcu_types.h"
#include "glados.h"
#include "max_data.h"
#include "p4_err.h"

/* Global Invariants:
 *  - CmdAO must be higher priority than RxScAO and RxGseAO
 */

/* Define the Cmd state masks such that they are 1U << state value.
 * Note: Lowest bit should not be set to handle instances when
 * the state is incorrectly set to zero */
#define LOW_PWR_STATE           (1U << (uint8_t)MAX_LOW_PWR)
#define IDLE_STATE              (1U << (uint8_t)MAX_IDLE)
#define THRUST_STATE            (1U << (uint8_t)MAX_THRUST)
#define MANUAL_STATE            (1U << (uint8_t)MAX_MANUAL)
#define SW_UPDATE_STATE         (1U << (uint8_t)MAX_SW_UPDATE)
#define BIST_STATE              (1U << (uint8_t)MAX_BIST)
#define WILD_WEST_STATE         (1U << (uint8_t)MAX_WILD_WEST)
#define SAFE_STATE              (1U << (uint8_t)MAX_SAFE)

#define ALL_STATES              0xFFFFU
#define MAN_WW_STATES           (MANUAL_STATE  | WILD_WEST_STATE)
#define MAN_WW_UP_STATES        (MAN_WW_STATES | SW_UPDATE_STATE)
#define LP_MAN_WW_STATES        (LOW_PWR_STATE | MAN_WW_STATES)
#define LP_IDLE_MAN_WW_STATES   (IDLE_STATE    | LP_MAN_WW_STATES)
#define ALL_BUT_THRUST_STATES   (ALL_STATES    & (~THRUST_STATE))


#define DATA_CMD_MASK           0x0020U


/* Function pointer for all commands to use */
typedef Cmd_Err_Code_t (*Cmd_FuncPtr)(uint8_t *args);

typedef struct
{
    uint16_t cmd_id;        /* Command ID */
    uint8_t params_size;    /* Number of command arguments */
    bool is_request;        /* Determines if the command transitions to a CMD request state after it executes */
    uint16_t valid_states;  /* bit number set if state number if valid for command */
    Cmd_FuncPtr cmd_func_ptr;
} CmdMgr_Cmd_Table_t;


#define CMDAO_PRIORITY        9UL
#define CMDAO_FIFO_SIZE       6UL

/* Each of these must be unique values for the entire system */
enum CmdAO_event_names
{
    EVT_CMD_MAX_TO___BASE    = (CMDAO_PRIORITY << 16U),
    EVT_CMD_MAX_TO_IDLE      = EVT_CMD_MAX_TO___BASE + MAX_IDLE,
    EVT_CMD_MAX_TO_THRUST    = EVT_CMD_MAX_TO___BASE + MAX_THRUST,
    EVT_CMD_MAX_TO_SW_UPDATE = EVT_CMD_MAX_TO___BASE + MAX_SW_UPDATE,
    EVT_CMD_MAX_TO_MANUAL    = EVT_CMD_MAX_TO___BASE + MAX_MANUAL,
    EVT_CMD_MAX_TO_BIST      = EVT_CMD_MAX_TO___BASE + MAX_BIST,
    EVT_CMD_MAX_TO_WILD_WEST = EVT_CMD_MAX_TO___BASE + MAX_WILD_WEST,
    EVT_CMD_MAX_TO_SAFE      = EVT_CMD_MAX_TO___BASE + MAX_SAFE,
    EVT_CMD_PROCESS_DONE     = ((CMDAO_PRIORITY << 16U) + 256U),
    EVT_CMD_TLM_REQUEST,
    EVT_CMD_TLM_RFT_REQUEST,
    EVT_TMR_CMD_TIMEOUT,
    EVT_CMD_ACK,
    EVT_CMD_ACK_NO_INC,
    EVT_CMD_ACK_W_SEND,
    EVT_CMD_NACK,
    EVT_CMD_NACK_W_SEND,
    EVT_CMD_RQST_COMPLETE,
    EVT_CMD_START_RECORDING,
    EVT_CMD_DUMP_MEM,
    EVT_CMD_DUMP_TLM,
    EVT_CMD_UPDATE_PRELOAD,
    EVT_CMD_UPDATE_STAGE,
    EVT_CMD_UPDATE_PROGRAM,
    EVT_CMD_SET_REC_BANK0,
    EVT_CMD_SET_REC_BANK1,
    EVT_CMD_DATA_ABORT,
    EVT_CMD_CLEAR_NAND_FULL,
    EVT_CMD_CLEAR_REC_TLM,
    EVT_CMD_CUST_NEXT_APP_ERASE,
};

typedef struct CmdAO_Data_tag
{
    uint8_t port;
    uint8_t seq_id;
    uint8_t src_id;
    uint16_t dest_mask;
    uint16_t msg_id;
    uint8_t *data_ptr;
    uint8_t data_size;

} BYTE_PACKED CmdAO_Data_t;

typedef struct
{
    Cmd_Err_Code_t err_code;
} BYTE_PACKED CmdAO_NackData_t;


extern GLADOS_AO_t AO_Cmd;
GLADOS_Timer_t Tmr_CmdTimeout;
GLADOS_Timer_t Tmr_CmdCustomAppTimer;
void CmdAO_init(void);
void CmdAO_state(GLADOS_AO_t *this_ao, GLADOS_Event_t *event);
void CmdAO_state_cmd_process(GLADOS_AO_t *this_ao, GLADOS_Event_t *event);
void CmdAO_state_request(GLADOS_AO_t *this_ao, GLADOS_Event_t *event);
void CmdAO_state_request_state(GLADOS_AO_t *this_ao, GLADOS_Event_t *event);
void CmdAO_state_request_norm(GLADOS_AO_t *this_ao, GLADOS_Event_t *event);
void CmdAO_state_request_pass_thru(GLADOS_AO_t *this_ao, GLADOS_Event_t *event);

void CmdAO_push_nack_w_send_err(GLADOS_AO_t *src_ao, Cmd_Err_Code_t err_code);

Cmd_Err_Code_t cmd_hdl_clr_errors(uint8_t *args);
Cmd_Err_Code_t cmd_hdl_set_utime(uint8_t *args);
Cmd_Err_Code_t cmd_hdl_set_heat_setpt(uint8_t *args);
Cmd_Err_Code_t cmd_hdl_set_heat_ctl_en(uint8_t *args);
Cmd_Err_Code_t cmd_hdl_set_flow_ctl_en(uint8_t *args);
Cmd_Err_Code_t cmd_hdl_set_flow_rate(uint8_t *args);
Cmd_Err_Code_t cmd_hdl_set_heat_i(uint8_t *args);
Cmd_Err_Code_t cmd_hdl_set_hpiso_i(uint8_t *args);
Cmd_Err_Code_t cmd_hdl_set_pfcv_i(uint8_t *args);
Cmd_Err_Code_t cmd_hdl_get_tlm(uint8_t *args);
Cmd_Err_Code_t cmd_hdl_set_auto_tlm(uint8_t *args);
Cmd_Err_Code_t cmd_hdl_clr_rec_tlm(uint8_t *args);
Cmd_Err_Code_t cmd_hdl_dump_mcu(uint8_t *args);
Cmd_Err_Code_t cmd_hdl_dump_tlm(uint8_t *args);
Cmd_Err_Code_t cmd_hdl_set_rec_tlm_en(uint8_t *args);
Cmd_Err_Code_t cmd_hdl_set_rec_nand_en(uint8_t *args);
Cmd_Err_Code_t cmd_hdl_set_rec_fram_en(uint8_t *args);
Cmd_Err_Code_t cmd_hdl_set_rec_bank(uint8_t *args);
Cmd_Err_Code_t cmd_hdl_abort_data_op(uint8_t *args);
Cmd_Err_Code_t cmd_hdl_sw_up_preload(uint8_t *args);
Cmd_Err_Code_t custom_cmd_hdl_sw_up_stage(uint8_t *args, uint8_t param_length);
Cmd_Err_Code_t cmd_hdl_sw_up_program(uint8_t *args);
Cmd_Err_Code_t cmd_hdl_set_wdi_en(uint8_t *args);
Cmd_Err_Code_t cmd_hdl_set_rft_rst_en(uint8_t *args);
Cmd_Err_Code_t cmd_hdl_set_heater_en(uint8_t *args);
Cmd_Err_Code_t cmd_hdl_set_heater_rst_en(uint8_t *args);
Cmd_Err_Code_t cmd_hdl_set_hpiso_en(uint8_t *args);
Cmd_Err_Code_t cmd_hdl_set_pfcv_dac_rst_en(uint8_t *args);
Cmd_Err_Code_t cmd_hdl_debug_leds(uint8_t *args);
Cmd_Err_Code_t cmd_hdl_config_thrust_adv(uint8_t *args);
Cmd_Err_Code_t cmd_hdl_heater_pwm_raw(uint8_t *args);
Cmd_Err_Code_t cmd_hdl_hpiso_pwm_raw(uint8_t *args);
Cmd_Err_Code_t cmd_hdl_pfcv_dac_raw(uint8_t *args);
Cmd_Err_Code_t cmd_hdl_get_tlm_rft(UNUSED uint8_t *args);

Cmd_Err_Code_t cmd_hdl_stp_wtd(uint8_t *args);

Cmd_Err_Code_t cmd_hdl_prg_flsh (uint8_t *args);
Cmd_Err_Code_t cmd_hdl_clr_flsh (uint8_t *args);

Cmd_Err_Code_t cmd_hdl_clr_errors_advanced(uint8_t *args);
Cmd_Err_Code_t cmd_hdl_get_sw_info(uint8_t *args);
Cmd_Err_Code_t cmd_hdl_write_max_hdr_info(uint8_t *args);
Cmd_Err_Code_t cmd_hdl_read_max_hdr_info( uint8_t *args);
Cmd_Err_Code_t cmd_hdl_erase_max_hdr_info(UNUSED uint8_t *args);

Cmd_Err_Code_t cmd_hdl_set_auto_status(uint8_t *args);
Cmd_Err_Code_t cmd_hdl_config_thrust(uint8_t *args);

Cmd_Err_Code_t cmd_hdl_test_thrust_anomaly(uint8_t *args);
Cmd_Err_Code_t cmd_hdl_test_needs_reset(uint8_t *args);
Cmd_Err_Code_t cmd_hdl_dft_sram_ecc_1bit(uint8_t *args);
Cmd_Err_Code_t cmd_hdl_dft_pt_sell_your_soul(uint8_t *args);
Cmd_Err_Code_t cmd_hdl_dft_pt_set(uint8_t *args);
Cmd_Err_Code_t cmd_hdl_set_next_app(uint8_t *args);

Cmd_Err_Code_t cmd_hdl_clr_count_log(uint8_t *args);
Cmd_Err_Code_t cmd_hdl_clr_err_telm(uint8_t *args);
Cmd_Err_Code_t cmd_hdl_clr_sram_rec(uint8_t *args);
Cmd_Err_Code_t cmd_hdl_clr_flash_rec(uint8_t *args);
Cmd_Err_Code_t cmd_hdl_clr_everything_rec(uint8_t *args);
Cmd_Err_Code_t cmd_hdl_wrt_err(uint8_t *args);
Cmd_Err_Code_t cmd_hdl_trnsf_flash(uint8_t *args);
Cmd_Err_Code_t cmd_hdl_wrt_err_bulk(uint8_t *args);
Cmd_Err_Code_t cmd_hdl_aux_driving_cmd(uint8_t *args);


#endif /* ACTIVE_OBJECTS_CMDAO_H_ */
