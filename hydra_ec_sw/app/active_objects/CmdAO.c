/* Copyright (c) 2017-2020 by Phase Four, Inc. All Rights Reserved. Confidential and Proprietary.
 *
 * CmdAO.c
 */

/* Global invariants:
 *  - All commands in the command table are sorted from smallest number ID to largest
 *  - No constants that might affect reception of data (e.g. CMD timeout) should be
 *    placed in the PT, lest run the risk of having a valid PT loaded and then the
 *    user is unable to communicate.
 */

#include "aux_driver.h"
#include "CmdAO.h"
#include "mcu_types.h"
#include "glados.h"
#include "ErrAO.h"
#include "RftAO.h"
#include "TxAO.h"
#include "RxScAO.h"
#include "RxGseAO.h"
#include "TlmAO.h"
#include "WdtAO.h"
#include "max_data.h"
#include "p4_cmd.h"
#include "MaxwellAO.h"
#include "DaqCtlAO.h"
#include "DataAO.h"
#include "gpio.h"
#include "max_info_driver.h"
#include "fidr.h"
#include "clank.h"
#include "default_pt.h"
#include "eim_driver.h"
#include "crc.h"
#include "fidr.h"
#include "max_defs.h"
#include "math.h"
#include "string.h"


/* Active Object public interface */

GLADOS_AO_t AO_Cmd;

/* Active Object timers */
GLADOS_Timer_t Tmr_CmdTimeout;

/* Active Object private variables */
static GLADOS_Event_t cmd_fifo[CMDAO_FIFO_SIZE];
static GLADOS_Event_t evt_cmd_rqst_complete;
static CmdAO_Data_t cmd_data;
static P4Cmd_Id_t cmd_id;

static int32_t get_cmd_table_idx(uint16_t command_id);


/* IMPORTANT: Command Table must be sorted from lowest Command ID number to highest
 * NOTE: Commands with a NULL handle function are custom commands handled uniquely */
static const CmdMgr_Cmd_Table_t Cmd_Table[] =
{
    /* Cmd ID,     Params Size,  Rqst Cmd, Valid States,                 Handle Function Pointer */
    { IDLE,                 0U,   false,     ALL_STATES,                 NULL },
    { THRUST,               0U,   false,     ALL_STATES,                 NULL },
    { BIST,                 0U,   false,     ALL_STATES,                 NULL },
    { GET_STATUS,           0U,   false,     ALL_STATES,                 NULL },
    { CONFIG_THRUST,        12U,  false,     ALL_BUT_THRUST_STATES,      &cmd_hdl_config_thrust },
    { SET_UTIME,            8U,   false,     ALL_STATES,                 &cmd_hdl_set_utime },
    { CLR_ERRORS,           0U,   false,     LP_IDLE_MAN_WW_STATES,      &cmd_hdl_clr_errors },

    { SET_AUTO_STATUS,      5U,   false,     ALL_STATES,                 &cmd_hdl_set_auto_status },
    { GET_SW_INFO,          1U,   true,      LP_IDLE_MAN_WW_STATES,      &cmd_hdl_get_sw_info },
    { SET_NEXT_APP,         1U,   true,      LP_MAN_WW_STATES,           &cmd_hdl_set_next_app },
    { ABORT,                0U,   false,     ALL_STATES,                 NULL },

    /* Test-related commands */
    { WRT_ERR,              3U,   false,     MAN_WW_STATES,              &cmd_hdl_wrt_err },
    { WRT_ERR_ERRAO_BLK,    0U,   false,     MAN_WW_STATES,              &cmd_hdl_wrt_err_bulk },
    { WRT_FLSH,             0U,   false,     MAN_WW_STATES,              &cmd_hdl_prg_flsh },
    { TRN_SRAM_FLASH,       0U,   false,     MAN_WW_STATES,              &cmd_hdl_trnsf_flash },
    { TEST_THRUST_ANOMALY,  4U,   false,     LP_IDLE_MAN_WW_STATES,      &cmd_hdl_test_thrust_anomaly },
    { TEST_NEEDS_RESET,     4U,   false,     LP_IDLE_MAN_WW_STATES,      &cmd_hdl_test_needs_reset },


    { DFT_SRAM_ECC_1BIT,    1U,   false,     MAN_WW_STATES,              &cmd_hdl_dft_sram_ecc_1bit },
    { DFT_PT_SELL_YOUR_SOUL,0U,   false,     WILD_WEST_STATE,            &cmd_hdl_dft_pt_sell_your_soul },
    { DFT_PT_SET,           8U,   false,     WILD_WEST_STATE,            &cmd_hdl_dft_pt_set },
    { AUX_ENABLE_CMD,       0U,   false,     MAN_WW_STATES,              &cmd_hdl_aux_driving_cmd },

    { SET_HPISO_EN,         1U,   false,     MAN_WW_STATES,              &cmd_hdl_set_hpiso_en },
    { SET_HPISO_I,          4U,   false,     MAN_WW_STATES,              &cmd_hdl_set_hpiso_i },
    { SET_HPISO_PWM_RAW,    2U,   false,     MAN_WW_STATES,              &cmd_hdl_hpiso_pwm_raw },
    { SET_HEATER_EN,        1U,   false,     MAN_WW_STATES,              &cmd_hdl_set_heater_en },
    { SET_HEAT_I,           4U,   false,     MAN_WW_STATES,              &cmd_hdl_set_heat_i },
    { SET_HEATER_PWM_RAW,   2U,   false,     MAN_WW_STATES,              &cmd_hdl_heater_pwm_raw },
    { SET_HEATER_RST_EN,    1U,   false,     MAN_WW_STATES,              &cmd_hdl_set_heater_rst_en },
    { SET_HEAT_SETPT,       4U,   false,     MAN_WW_STATES,              &cmd_hdl_set_heat_setpt },
    { SET_HEAT_CTL_ALG_EN,  1U,   false,     MAN_WW_STATES,              &cmd_hdl_set_heat_ctl_en },
    { SET_PFCV_I,           4U,   false,     MAN_WW_STATES,              &cmd_hdl_set_pfcv_i },
    { SET_PFCV_DAC_RAW,     2U,   false,     MAN_WW_STATES,              &cmd_hdl_pfcv_dac_raw },
    { SET_PFCV_DAC_RST_EN,  1U,   false,     MAN_WW_STATES,              &cmd_hdl_set_pfcv_dac_rst_en },
    { SET_FLOW_RATE,        4U,   false,     MAN_WW_STATES,              &cmd_hdl_set_flow_rate },
    { SET_FLOW_CTL_ALG_EN,  5U,   false,     MAN_WW_STATES,              &cmd_hdl_set_flow_ctl_en },
    { SET_RFT_RST_EN,       1U,   false,     MAN_WW_STATES,              &cmd_hdl_set_rft_rst_en },
    { SET_WDI_EN,           1U,   false,     MAN_WW_STATES,              &cmd_hdl_set_wdi_en },
    { SET_DEBUG_LEDS,       1U,   false,     MAN_WW_STATES,              &cmd_hdl_debug_leds },

    { CONFIG_THRUST_ADV,    36U,  false,     MAN_WW_STATES,              &cmd_hdl_config_thrust_adv },

    { SET_MAX_HRD_INFO,     129U, false,     LP_IDLE_MAN_WW_STATES,      &cmd_hdl_write_max_hdr_info },
    { ERASE_MAX_HRD_INFO,   0U,   false,     LP_IDLE_MAN_WW_STATES,      &cmd_hdl_erase_max_hdr_info },

    { MANUAL,               0U,   false,     ALL_STATES,                 NULL },
    { SW_UPDATE,            0U,   false,     ALL_STATES,                 NULL },
    { WILD_WEST,            0U,   false,     ALL_STATES,                 NULL },
    { SW_UP_PRELOAD,        12U,  true,      SW_UPDATE_STATE,            &cmd_hdl_sw_up_preload },
    { SW_UP_STAGE,          4U,   true,      SW_UPDATE_STATE,            NULL },
    { SW_UP_PROGRAM,        12U,  true,      SW_UPDATE_STATE,            &cmd_hdl_sw_up_program },
    { GET_TLM,              0U,   true,      ALL_STATES,                 &cmd_hdl_get_tlm },
    { GET_TLM_RFT_CACHED,   0U,   true,      ALL_STATES,                 &cmd_hdl_get_tlm_rft },
    { SET_AUTO_TLM,         5U,   false,     ALL_STATES,                 &cmd_hdl_set_auto_tlm },
    { SET_REC_BANK,         1U,   false,     MAN_WW_STATES,              &cmd_hdl_set_rec_bank },
    { SET_REC_NAND_EN,      1U,   false,     MAN_WW_STATES,              &cmd_hdl_set_rec_nand_en },
    { SET_REC_FRAM_EN,      1U,   false,     MAN_WW_STATES,              &cmd_hdl_set_rec_fram_en },
    { ENBL_REC_TLM,         0U,   true,      MAN_WW_STATES,              &cmd_hdl_set_rec_tlm_en },
    { DUMP_TLM,             2U,   true,      LP_MAN_WW_STATES,           &cmd_hdl_dump_tlm },

    { ABORT_DATA_OP,        0U,   false,     MAN_WW_UP_STATES,           &cmd_hdl_abort_data_op },
    { CLR_REC_TLM,          0U,   true,      LP_MAN_WW_STATES,           &cmd_hdl_clr_rec_tlm },
    { CLEAR_ERRORS_ADVANCED, 1U,  false,     LP_IDLE_MAN_WW_STATES,      &cmd_hdl_clr_errors_advanced },
    { DUMP_MCU,             5U,   true,      LP_MAN_WW_STATES,           &cmd_hdl_dump_mcu },
    { STOP_WDT,             0U,   false,     ALL_STATES,                 &cmd_hdl_stp_wtd },
    { GET_MAX_HRD_INFO,     1U,   true,      LP_IDLE_MAN_WW_STATES,      &cmd_hdl_read_max_hdr_info }
};


/* Active Object state definitions */

void CmdAO_init(void)
{
    /* Initialize the Active Object with both its Event FIFO and initial state function */
    GLADOS_AO_Init(&AO_Cmd, CMDAO_PRIORITY, cmd_fifo, CMDAO_FIFO_SIZE, &CmdAO_state);

    return;
}

void CmdAO_state(GLADOS_AO_t *this_ao, GLADOS_Event_t *event)
{
    DEV_ASSERT(this_ao);
    DEV_ASSERT(event);

    switch(event->name)
    {
        /* GLADOS_ENTRY/GLADOS_EXIT optional event for each state */
        case GLADOS_ENTRY:
            /* Configure timeout for receiving the message to 5ms. Selected based
             * on max 265B transfer at 1Mbps rate */
            GLADOS_Timer_Init(&Tmr_CmdTimeout, MAX_CMD_TIMEOUT_US, EVT_TMR_CMD_TIMEOUT);
            GLADOS_Timer_Subscribe_AO(&Tmr_CmdTimeout, this_ao);

            /* This timer value is expected to be overwritten by the feature using it */
            GLADOS_Timer_Init(&Tmr_CmdCustomAppTimer, 1000000UL, EVT_TMR_CMD_TIMEOUT);
            GLADOS_Timer_Subscribe_AO(&Tmr_CmdCustomAppTimer, this_ao);

            /* Create a self event used as a consistent way to signal the
             * restarting of the Rx process */
            GLADOS_Event_New(&evt_cmd_rqst_complete, EVT_CMD_RQST_COMPLETE);

            GLADOS_STATE_TRAN_TO_CHILD(&CmdAO_state_cmd_process);
            break;

        case GLADOS_EXIT:
            break;

        case EVT_MAX_STATE_BUSY:
        case EVT_MAX_STATE_IN_IDLE:
        case EVT_MAX_STATE_IN_THRUST:
        case EVT_MAX_STATE_IN_MANUAL:
        case EVT_MAX_STATE_IN_BIST:
        case EVT_MAX_STATE_IN_WILD_WEST:
        case EVT_MAX_STATE_IN_SW_UPDATE:
            report_error_uint(&AO_Cmd, Errors.UnexpectedTransition , DO_NOT_SEND_TO_SPACECRAFT, 0UL);
            break;
        case EVT_RFT_ACK:
        case EVT_RFT_NACK:
            break;
        case EVT_MAX_STATE_IN_SAFE:
        case EVT_MAX_STATE_IN_LOW_PWR:
            /* Ignore state changes that occur due to FDIR or critical errors */
            break;

        default:
            /* Top level state, skip undefined Events */
            break;
    }

    return;
}


void CmdAO_state_cmd_process(GLADOS_AO_t *this_ao, GLADOS_Event_t *event)
{
    DEV_ASSERT(this_ao);
    DEV_ASSERT(event);

    int32_t cmd_idx;
    Cmd_Err_Code_t error_code;
    bool cmd_ackd = false;

    switch(event->name)
    {
        /* GLADOS_ENTRY/GLADOS_EXIT optional event for each state */
        case GLADOS_ENTRY:
            break;

        case GLADOS_EXIT:
            break;

        case EVT_RXGSE_CMD_RCVD:
        case EVT_RXSC_CMD_RCVD:
            DEV_ASSERT(event->has_data);
            DEV_ASSERT(event->data_size == sizeof(CmdAO_Data_t));

            /* Store a copy of the command data for all internal states to use.
             * Note: This pointer relies on CmdAO being of a higher priority
             * than the Rx AOs in order to preserve this data until the next
             * command received is pushed by an Rx AO. */
            cmd_data = *(CmdAO_Data_t *)&event->data[0];

            cmd_idx = get_cmd_table_idx(cmd_data.msg_id);

            /* Detect if the command is a pass through command intended for the RFT */
            if ((cmd_data.dest_mask & (1U << (uint16_t)MAX_ADDR_RFT)) != 0U)
            {
                /* Indicate that no Ack/Nack is necessary yet since the response
                 * is to be initiated by another AO */
                cmd_ackd = false;

                /* Use the data_ptr to get the beginning of the entire Clank packet,
                 * (start of the header). Use the data_size to get the size of
                 * the entire Clank packet, which includes header and CRC size. */
                RftAO_push_cmd_request(this_ao, RFT_CMD_PASSTHRU, 0U, cmd_data.data_ptr - CLANK_HDR_SIZE,
                                           (uint16_t)cmd_data.data_size + CLANK_HDR_SIZE + CLANK_CRC_SIZE, cmd_data.port);

                /* Treat pass through commands like any normal command request to another AO */
                GLADOS_STATE_TRAN_TO_SIBLING(&CmdAO_state_request);
                GLADOS_STATE_TRAN_TO_CHILD(&CmdAO_state_request_pass_thru);
            }
            /* Check if command exists */
            else if (cmd_idx < 0)
            {
                cmd_ackd = true;
                MaxData.cmd_rjct_cnt++;
                report_error_uint(&AO_Cmd, Errors.CmdMsgIdNotFound , DO_NOT_SEND_TO_SPACECRAFT, cmd_data.msg_id);
                TxAO_SendMsg_Ack(this_ao, (TxPort_t)cmd_data.port,(uint8_t)cmd_data.src_id, cmd_data.seq_id, cmd_data.msg_id, Errors.CmdMsgIdNotFound->error_id);
            }
            /* Check if command is defined for the current state */
            else if (0U == (Cmd_Table[cmd_idx].valid_states & (1U << MaxData.state)))
            {
                cmd_ackd = true;
                MaxData.cmd_rjct_cnt++;
                report_error_uint(&AO_Cmd, Errors.CmdMsgIdStateNotSupported , DO_NOT_SEND_TO_SPACECRAFT, (uint32_t)MaxData.state);
                TxAO_SendMsg_Ack(this_ao, (TxPort_t)cmd_data.port, (uint8_t)cmd_data.src_id, cmd_data.seq_id, cmd_data.msg_id, Errors.CmdMsgIdStateNotSupported->error_id);
            }
            /* Handle commands with defined function handles */
            else if (Cmd_Table[cmd_idx].cmd_func_ptr != NULL)
            {
                /* Check if all parameters exist. This is checked here rather than one level up since
                 * the parameter check does not work for custom commands that have sent variable data. */
                if (Cmd_Table[cmd_idx].params_size != cmd_data.data_size)
                {
                    cmd_ackd = true;
                    MaxData.cmd_rjct_cnt++;
                    report_error_uint(&AO_Cmd, Errors.CmdMsgIdInvalidDataSize , DO_NOT_SEND_TO_SPACECRAFT, Cmd_Table[cmd_idx].params_size);
                    TxAO_SendMsg_Ack(this_ao, (TxPort_t)cmd_data.port,(uint8_t)cmd_data.src_id, cmd_data.seq_id, cmd_data.msg_id, Errors.CmdMsgIdInvalidDataSize->error_id);
                }
                else
                {
                    /* Call the function and pass in the command arguments */
                    error_code = Cmd_Table[cmd_idx].cmd_func_ptr(&cmd_data.data_ptr[0]);

                    /* If a request command to another AO, then transition to the CmdAO request
                     * state and await a response from the appropriate AO. No Ack is returned
                     * to the sender until the appropriate AO responds. */
                    if ((error_code.reported_error == Errors.StatusSuccess) && (Cmd_Table[cmd_idx].is_request))
                    {
                        cmd_ackd = false;
                        GLADOS_STATE_TRAN_TO_SIBLING(&CmdAO_state_request);
                        GLADOS_STATE_TRAN_TO_CHILD(&CmdAO_state_request_norm);
                    }
                    /* If not a request command, then report an Ack if no error. */
                    else if (error_code.reported_error == Errors.StatusSuccess)
                    {
                        MaxData.cmd_acpt_cnt++;
                        /* Transmits no error */
                        cmd_ackd = true;
                        TxAO_SendMsg_Ack(this_ao, (TxPort_t)cmd_data.port,(uint8_t)cmd_data.src_id, cmd_data.seq_id, cmd_data.msg_id, error_code.reported_error->error_id);
                    }
                    else
                    {
                        /* Error occurred in the handle function, send Nack back */
                        MaxData.cmd_rjct_cnt++;
                        cmd_ackd = true;
                        report_error_uint(&AO_Cmd, (err_type*) error_code.reported_error , DO_NOT_SEND_TO_SPACECRAFT, error_code.aux_data);
                        TxAO_SendMsg_Ack(this_ao, (TxPort_t)cmd_data.port,(uint8_t)cmd_data.src_id, cmd_data.seq_id, cmd_data.msg_id, error_code.reported_error->error_id);
                    }
                }
            }
            /* If the command is defined within the table and does not have a handle function,
             * then treat these commands as a custom command */
            else
            {
                cmd_id = Cmd_Table[cmd_idx].cmd_id;

                switch(cmd_id)
                {
                    case ABORT:
                    case IDLE:
                    case THRUST:
                    case MANUAL:
                    case BIST:
                    case WILD_WEST:
                    case SW_UPDATE:
                        if (Cmd_Table[cmd_idx].params_size != cmd_data.data_size)
                        {
                            cmd_ackd = true;
                            MaxData.cmd_rjct_cnt++;
                            report_error_uint(&AO_Cmd, Errors.CmdMsgIdInvalidDataSize , DO_NOT_SEND_TO_SPACECRAFT, Cmd_Table[cmd_idx].params_size);
                            TxAO_SendMsg_Ack(this_ao, (TxPort_t)cmd_data.port,(uint8_t)cmd_data.src_id, cmd_data.seq_id, cmd_data.msg_id, Errors.CmdMsgIdInvalidDataSize->error_id);
                        }
                        else
                        {
                            /* All commands that forward their requests to another AO do not send an ACK
                             * since the AO will signal the appropriate ACK */
                            cmd_ackd = false;
                            GLADOS_AO_PUSH(&AO_Maxwell, EVT_CMD_MAX_TO___BASE + (uint32_t)(0xFUL & ((uint32_t)cmd_id)));
                            GLADOS_STATE_TRAN_TO_SIBLING(&CmdAO_state_request);
                            GLADOS_STATE_TRAN_TO_CHILD(&CmdAO_state_request_state);
                        }
                        break;

                    case GET_STATUS:
                        /* Command is always accepted, however do not increment accept counter */
                        cmd_ackd = true;
                        TxAO_SendMsg_Ack(this_ao, (TxPort_t)cmd_data.port,(uint8_t)cmd_data.src_id, cmd_data.seq_id, cmd_data.msg_id, Errors.StatusSuccess->error_id);
                        break;

                    case SW_UP_STAGE:
                        error_code = custom_cmd_hdl_sw_up_stage(&cmd_data.data_ptr[0], cmd_data.data_size);
                        if (error_code.reported_error->error_id != 0U)
                        {
                            cmd_ackd = true;
                            MaxData.cmd_rjct_cnt++;
                            report_error_uint(&AO_Cmd, (err_type*) error_code.reported_error , DO_NOT_SEND_TO_SPACECRAFT,  error_code.aux_data);
                            TxAO_SendMsg_Ack(this_ao, (TxPort_t)cmd_data.port, (uint8_t)cmd_data.src_id, cmd_data.seq_id, cmd_data.msg_id, error_code.reported_error->error_id);
                        }
                        else
                        {
                            cmd_ackd = false;
                            GLADOS_STATE_TRAN_TO_SIBLING(&CmdAO_state_request);
                            GLADOS_STATE_TRAN_TO_CHILD(&CmdAO_state_request_norm);
                        }
                        break;

                    default:
                        /* This case is hit if the command exists in the command table, but
                         * does not yet have a defined command handle function or is not
                         * handled as a request command. */
                        /* Transmits Ack if no error, Nack with error code otherwise */
                        cmd_ackd = true;
                        report_error_uint(&AO_Cmd, Errors.MsgIdFunctionNotImplemented , DO_NOT_SEND_TO_SPACECRAFT, cmd_data.msg_id);
                        TxAO_SendMsg_Ack(this_ao, (TxPort_t)cmd_data.port, (uint8_t)cmd_data.src_id, cmd_data.seq_id, cmd_data.msg_id, Errors.MsgIdFunctionNotImplemented->error_id);
                        break;
                }
            }

            /* Signal to the appropriate Rx AO that the Rx buffer can be used
             * to receive the next command.  If the command was not yet ack'd,
             * then do not free up the Rx AO as this will be done upon
             * completion of the command request. */
            if (cmd_ackd && (cmd_data.port == PORT_GSE))
            {
                GLADOS_AO_PUSH(&AO_RxGse, EVT_CMD_PROCESS_DONE);
            }
            else if (cmd_ackd)
            {
                GLADOS_AO_PUSH(&AO_RxSc, EVT_CMD_PROCESS_DONE);
            }

            break;

        default:
            GLADOS_STATE_CHECK_PARENT(&CmdAO_state);
            break;
    }

    return;
}


void CmdAO_state_request(GLADOS_AO_t *this_ao, GLADOS_Event_t *event)
{
    DEV_ASSERT(this_ao);
    DEV_ASSERT(event);

    CmdAO_Data_t *tmp_cmd_data;

    switch(event->name)
    {
        /* GLADOS_ENTRY/GLADOS_EXIT optional event for each state */
        case GLADOS_ENTRY:
            GLADOS_Timer_EnableOnce(&Tmr_CmdTimeout);
            break;

        case GLADOS_EXIT:
            GLADOS_Timer_Disable(&Tmr_CmdTimeout);

            /* Signal to the appropriate Rx AO that the Rx buffer can be used
             * to receive the next command */
            if (cmd_data.port == PORT_GSE)
            {
                GLADOS_AO_PUSH(&AO_RxGse, EVT_CMD_PROCESS_DONE);
            }
            else
            {
                GLADOS_AO_PUSH(&AO_RxSc, EVT_CMD_PROCESS_DONE);
            }
            break;

        case EVT_RXGSE_CMD_RCVD:
        case EVT_RXSC_CMD_RCVD:
            /* If we get another command in the middle of processing, send Nack.
             * This should not happen since the Rx AOs are lower in priority and
             * are not freed up to read from their Rx buffers until signaled by
             * this AO that processing has completed with the current command. */
            MaxData.cmd_rjct_cnt++;
            tmp_cmd_data = (CmdAO_Data_t *)&event->data[0];
            report_error_uint(&AO_Cmd, Errors.CmdProcessingPreviousCommand , DO_NOT_SEND_TO_SPACECRAFT, 0UL);
            TxAO_SendMsg_Ack(this_ao, (TxPort_t)tmp_cmd_data->port,(uint8_t)tmp_cmd_data->src_id, tmp_cmd_data->seq_id, tmp_cmd_data->msg_id, Errors.CmdProcessingPreviousCommand->error_id);

            break;

        case EVT_TMR_CMD_TIMEOUT:
            /* Send Nack */
            MaxData.cmd_rjct_cnt++;
            report_error_uint(&AO_Cmd, Errors.CurrentProcessedCommandTimedout , DO_NOT_SEND_TO_SPACECRAFT, 0UL);
            TxAO_SendMsg_Ack(this_ao, (TxPort_t)cmd_data.port,(uint8_t)cmd_data.src_id, cmd_data.seq_id, cmd_data.msg_id, Errors.CurrentProcessedCommandTimedout->error_id);

            GLADOS_STATE_TRAN_TO_SIBLING(&CmdAO_state_cmd_process);
            break;

        case EVT_CMD_RQST_COMPLETE:
            GLADOS_STATE_TRAN_TO_SIBLING(&CmdAO_state_cmd_process);
            break;

        default:
            GLADOS_STATE_CHECK_PARENT(&CmdAO_state);
            break;
    }

    return;
}


void CmdAO_state_request_state(GLADOS_AO_t *this_ao, GLADOS_Event_t *event)
{
    DEV_ASSERT(this_ao);
    DEV_ASSERT(event);

    switch(event->name)
    {
        /* GLADOS_ENTRY/GLADOS_EXIT optional event for each state */
        case GLADOS_ENTRY:
            break;

        case GLADOS_EXIT:
            break;

        /* EVT_MAX_STATE_IN_LOW_PWR is not expected since transitions
         * must always go through SAFE */
        case EVT_MAX_STATE_IN_SAFE:
        case EVT_MAX_STATE_IN_IDLE:
        case EVT_MAX_STATE_IN_THRUST:
        case EVT_MAX_STATE_IN_MANUAL:
        case EVT_MAX_STATE_IN_BIST:
        case EVT_MAX_STATE_IN_WILD_WEST:
        case EVT_MAX_STATE_IN_SW_UPDATE:
            /* If state mismatches the commanded, then send NACK */
            if ((event->name & 0xFUL) != (uint32_t)(cmd_id & 0xFUL))
            {
                MaxData.cmd_rjct_cnt++;
                report_error_uint(&AO_Cmd, Errors.CmdStateTransitionFail, DO_NOT_SEND_TO_SPACECRAFT, 0UL);
                TxAO_SendMsg_Ack(this_ao, (TxPort_t)cmd_data.port,(uint8_t)cmd_data.src_id, cmd_data.seq_id, cmd_data.msg_id,  Errors.CmdStateTransitionFail->error_id);
            }
            /* Otherwise, send ACK */
            else
            {
                MaxData.cmd_acpt_cnt++;
                /* If state mismatches the commanded, then send NACK */
                TxAO_SendMsg_Ack(this_ao, (TxPort_t)cmd_data.port,
                                    (uint8_t)cmd_data.src_id, cmd_data.seq_id, cmd_data.msg_id, 0U);
            }
            GLADOS_AO_PUSH_EVT_SELF(&evt_cmd_rqst_complete);
            break;

        case EVT_MAX_STATE_INVALID:
            MaxData.cmd_rjct_cnt++;
            /*Send error for invalid for state to Error AO */

            report_error_uint(&AO_Cmd, Errors.CmdInvalidStateTransition , DO_NOT_SEND_TO_SPACECRAFT, 0UL);
            TxAO_SendMsg_Ack(this_ao, (TxPort_t)cmd_data.port, (uint8_t)cmd_data.src_id, cmd_data.seq_id, cmd_data.msg_id,  Errors.CmdInvalidStateTransition->error_id);

            /* Transition back to process the next command */
            GLADOS_AO_PUSH_EVT_SELF(&evt_cmd_rqst_complete);
            break;

        case EVT_MAX_STATE_BUSY:
            MaxData.cmd_rjct_cnt++;

            /*Send error for state busy to Error AO */
            report_error_uint(&AO_Cmd, Errors.CmdCurrentStateIsBusy , DO_NOT_SEND_TO_SPACECRAFT, 0UL);
            TxAO_SendMsg_Ack(this_ao, (TxPort_t)cmd_data.port,(uint8_t)cmd_data.src_id,
                                 cmd_data.seq_id, cmd_data.msg_id, Errors.CmdCurrentStateIsBusy->error_id);

            /* Transition back to process the next command */
            GLADOS_AO_PUSH_EVT_SELF(&evt_cmd_rqst_complete);
            break;

        case EVT_MAX_ERR_ACTIVE:
            MaxData.cmd_rjct_cnt++;

            /* The MaxAO handles any logging the specific error, so only report the NACK */
            TxAO_SendMsg_Ack(this_ao, (TxPort_t)cmd_data.port,(uint8_t)cmd_data.src_id,
                                 cmd_data.seq_id, cmd_data.msg_id, Errors.CmdRejectedErrorActive->error_id);

            /* Transition back to process the next command */
            GLADOS_AO_PUSH_EVT_SELF(&evt_cmd_rqst_complete);
            break;

        default:
            GLADOS_STATE_CHECK_PARENT(&CmdAO_state_request);
            break;
    }

    return;
}


void CmdAO_state_request_norm(GLADOS_AO_t *this_ao, GLADOS_Event_t *event)
{
    DEV_ASSERT(this_ao);
    DEV_ASSERT(event);

    CmdAO_NackData_t *nack_data;
    RftAO_Intern_Data_t *rft_intern_data;

    switch(event->name)
    {
        /* GLADOS_ENTRY/GLADOS_EXIT optional event for each state */
        case GLADOS_ENTRY:
            break;

        case GLADOS_EXIT:
            break;

        /* Return a NACK with the error data */
        case EVT_CMD_NACK_W_SEND:
            DEV_ASSERT(event->has_data);
            DEV_ASSERT(event->data_size == sizeof(CmdAO_NackData_t));

            nack_data = (CmdAO_NackData_t *)&event->data[0];

            /* Errors aren't logged, the status response should suffice. */

            /* Send Nack */
            TxAO_SendMsg_Ack(this_ao, (TxPort_t)cmd_data.port, (uint8_t)cmd_data.src_id,
                                 cmd_data.seq_id, cmd_data.msg_id, nack_data->err_code.reported_error->error_id);

            MaxData.cmd_rjct_cnt++;
            GLADOS_AO_PUSH_EVT_SELF(&evt_cmd_rqst_complete);
            break;

        case EVT_RFT_NACK:
            DEV_ASSERT(event->has_data);
            DEV_ASSERT(event->data_size == sizeof(CmdAO_NackData_t));

            nack_data = (CmdAO_NackData_t *)&event->data[0];
            report_error_uint(&AO_Cmd, (err_type*) nack_data->err_code.reported_error, DO_NOT_SEND_TO_SPACECRAFT, nack_data->err_code.aux_data);

            /* Send Nack. Note that the Aux data is transmitted as the error code
             * since it contains the error status returned from the RFT. */
            TxAO_SendMsg_Ack(this_ao, (TxPort_t)cmd_data.port, (uint8_t)cmd_data.src_id,
                                 cmd_data.seq_id, cmd_data.msg_id, nack_data->err_code.aux_data);

            MaxData.cmd_rjct_cnt++;
            GLADOS_AO_PUSH_EVT_SELF(&evt_cmd_rqst_complete);
            break;

        case EVT_CMD_NACK:
            /* Log error for denial of request because the AO is busy */
            report_error_uint(&AO_Cmd, Errors.CmdCurrentStateIsBusy , DO_NOT_SEND_TO_SPACECRAFT, 0UL);
            MaxData.cmd_rjct_cnt++;
            GLADOS_AO_PUSH_EVT_SELF(&evt_cmd_rqst_complete);
            break;

        case EVT_CMD_ACK_W_SEND:
        /* Commands that are not pass-thru but are internally initiated by the CmdAO
         * to the RFT need to initiate an Ack message. */
        case EVT_RFT_ACK:
            /* Send Ack */
            TxAO_SendMsg_Ack(this_ao, (TxPort_t)cmd_data.port, (uint8_t)cmd_data.src_id, cmd_data.seq_id, cmd_data.msg_id, 0U);
            MaxData.cmd_acpt_cnt++;
            GLADOS_AO_PUSH_EVT_SELF(&evt_cmd_rqst_complete);
            break;

        case EVT_RFT_ACK_DATA:
            DEV_ASSERT(event->has_data);
            DEV_ASSERT(event->data_size == sizeof(RftAO_Intern_Data_t));

            rft_intern_data = (RftAO_Intern_Data_t *)&event->data[0];

            TxAO_SendMsg_Data(&AO_Cmd, (TxPort_t)cmd_data.port, (uint8_t)cmd_data.src_id, cmd_data.seq_id,
                                  cmd_data.msg_id, rft_intern_data->data_addr, rft_intern_data->data_length);

            MaxData.cmd_acpt_cnt++;
            GLADOS_AO_PUSH_EVT_SELF(&evt_cmd_rqst_complete);
            break;

        case EVT_CMD_ACK:
            MaxData.cmd_acpt_cnt++;
            GLADOS_AO_PUSH_EVT_SELF(&evt_cmd_rqst_complete);
            break;

        case EVT_CMD_ACK_NO_INC:
            GLADOS_AO_PUSH_EVT_SELF(&evt_cmd_rqst_complete);
            break;
        case EVT_CMD_CUST_NEXT_APP_ERASE:
             cmd_hdl_set_next_app(Tmr_CmdCustomAppTimer.tmr_evt.data);
            break;

        default:
            GLADOS_STATE_CHECK_PARENT(&CmdAO_state_request);
            break;
    }

    return;
}

void CmdAO_state_request_pass_thru(GLADOS_AO_t *this_ao, GLADOS_Event_t *event)
{
    DEV_ASSERT(this_ao);
    DEV_ASSERT(event);

    switch(event->name)
    {
        /* GLADOS_ENTRY/GLADOS_EXIT optional event for each state */
        case GLADOS_ENTRY:
            break;

        case GLADOS_EXIT:
            break;

        case EVT_RFT_ACK:
            MaxData.cmd_acpt_cnt++;
            GLADOS_AO_PUSH_EVT_SELF(&evt_cmd_rqst_complete);
            break;

        case EVT_RFT_NACK:
            /* Do not send ACK/NACK for pass thru commands since RftAO already handled it */
            MaxData.cmd_rjct_cnt++;
            /* No need to push an error since RftAO already handled it too */
            GLADOS_AO_PUSH_EVT_SELF(&evt_cmd_rqst_complete);
            break;

        case EVT_TMR_CMD_TIMEOUT:
            /* Do not send ACK/NACK for pass thru commands in the event of a timeout */
            MaxData.cmd_rjct_cnt++;
            report_error_uint(&AO_Cmd, Errors.CurrentProcessedCommandTimedout, DO_NOT_SEND_TO_SPACECRAFT, 0UL);
            GLADOS_AO_PUSH_EVT_SELF(&evt_cmd_rqst_complete);
            break;

        default:
            GLADOS_STATE_CHECK_PARENT(&CmdAO_state_request);
            break;
    }

    return;
}



void CmdAO_push_nack_w_send_err(GLADOS_AO_t *src_ao, Cmd_Err_Code_t err_code)
{
    GLADOS_Event_t evt;
    CmdAO_NackData_t nack_data = { .err_code = err_code };

    GLADOS_Event_New_Data(&evt, EVT_CMD_NACK_W_SEND, (uint8_t *)&nack_data, sizeof(CmdAO_NackData_t));
    GLADOS_AO_PushEvt(src_ao, &AO_Cmd, &evt);

    return;
}

/* Performs binary search of table until command ID is found */
static int32_t get_cmd_table_idx(uint16_t command_id)
{

    int32_t num_of_rows = sizeof(Cmd_Table) / sizeof(CmdMgr_Cmd_Table_t);
    int32_t first_i = 0L;
    int32_t last_i = num_of_rows - 1L;
    int32_t middle_i;

    int32_t found_idx = -1L;    /* Holds the matching index, -1 if not found */

    while (first_i <= last_i)
    {
        middle_i = (first_i + last_i) / 2U;

        if (command_id < Cmd_Table[middle_i].cmd_id)
        {
            last_i = middle_i - 1U;
        }
        else if (command_id == Cmd_Table[middle_i].cmd_id)
        {
            found_idx = middle_i;
            break;
        }
        else
        {
            first_i = middle_i + 1U;
        }
    }

    return found_idx;
}


Cmd_Err_Code_t cmd_hdl_clr_errors(UNUSED uint8_t *args)
{
    DEV_ASSERT(args);
    RftAO_push_cmd_request(&AO_Cmd, RFT_CMD_SILENT, CLR_ERRORS, cmd_data.data_ptr, cmd_data.data_size, cmd_data.port);
    Cmd_Err_Code_t error_code = ERROR_NONE;
    uint8_t clear_all_err_arg = 0x0F;
    GLADOS_Event_t evt;
    GLADOS_Event_New_Data(&evt, EVT_CLEAR_ERRORS, &clear_all_err_arg, 1UL);
    GLADOS_AO_PushEvt(&AO_Cmd, &AO_Err, &evt);
    MaxData.cmd_acpt_cnt = 0U;
    MaxData.cmd_rjct_cnt = 0U;

    return error_code;
}

Cmd_Err_Code_t cmd_hdl_clr_errors_advanced(uint8_t *args)
{
    DEV_ASSERT(args);
    Cmd_Err_Code_t error_code = ERROR_NONE;

    GLADOS_Event_t evt;
    GLADOS_Event_New_Data(&evt, EVT_CLEAR_ERRORS, args, 1UL);
    GLADOS_AO_PushEvt(&AO_Cmd, &AO_Err, &evt);
    MaxData.cmd_acpt_cnt = 0U;
    MaxData.cmd_rjct_cnt = 0U;

    return error_code;
}

Cmd_Err_Code_t cmd_hdl_set_utime(uint8_t *args)
{
    DEV_ASSERT(args);
    RftAO_push_cmd_request(&AO_Cmd, RFT_CMD_SILENT, SET_UTIME, cmd_data.data_ptr, cmd_data.data_size, cmd_data.port);
    Cmd_Err_Code_t error_code = ERROR_NONE;

    /* Record the current tick when the unix time was received.  This tick is used
     * to calculate the number of ticks that have elapsed since this sync.  The
     * time that has elapsed in msec is then added to the last Unix Time received
     * to get the current Unix Time */
    timer64_get_tick(&MaxData.time_sync_lpit_hi, &MaxData.time_sync_lpit_lo);

    /* Set the last received 64-bit Unix Time stamp in msec */
    MaxData.last_unix_time_sync_us_hi = ((uint32_t)args[0] << 24U) +
                                        ((uint32_t)args[1] << 16U) +
                                        ((uint32_t)args[2] << 8U)  +
                                        ((uint32_t)args[3]);

    MaxData.last_unix_time_sync_us_lo = ((uint32_t)args[4] << 24U) +
                                        ((uint32_t)args[5] << 16U) +
                                        ((uint32_t)args[6] << 8U)  +
                                        ((uint32_t)args[7]);
    return error_code;
}


Cmd_Err_Code_t cmd_hdl_set_heat_setpt(uint8_t *args)
{
    DEV_ASSERT(args);

    Cmd_Err_Code_t error_code = ERROR_NONE;

    uint32_t float_bytes = ((uint32_t)args[0] << 24U) +
                           ((uint32_t)args[1] << 16U) +
                           ((uint32_t)args[2] << 8U)  +
                           ((uint32_t)args[3]);

    error_code = DaqCtlAO_HeatCtl_SetTemp(BYTES_TO_F32(float_bytes));

    return error_code;
}


Cmd_Err_Code_t cmd_hdl_set_heat_ctl_en(uint8_t *args)
{
    DEV_ASSERT(args);
    Cmd_Err_Code_t error_code = ERROR_NONE;
    DaqCtlAO_HeatCtl_Enable((bool)args[0]);
    return error_code;
}


Cmd_Err_Code_t cmd_hdl_set_flow_ctl_en(uint8_t *args)
{
    DEV_ASSERT(args);

    Cmd_Err_Code_t error_code = ERROR_NONE;
    bool enable = (bool)args[0];

    uint32_t float_bytes = ((uint32_t)args[1] << 24U) +
                           ((uint32_t)args[2] << 16U) +
                           ((uint32_t)args[3] << 8U)  +
                           ((uint32_t)args[4]);
    if (enable)
    {
        error_code = DaqCtlAO_FlowCtl_Set(BYTES_TO_F32(float_bytes));
        if (error_code.reported_error == Errors.StatusSuccess)
        {
            DaqCtlAO_FlowCtl_Enable(true);
        }
    }
    else
    {
        /* If users wish to have a specific flow rate with flow control
         * disabled, then they must send a mdot set command afterward */
        DaqCtlAO_FlowCtl_Enable(false);
        MaxData.flow_ctrl_mdot_fb = 0.0f;
    }

    return error_code;
}


Cmd_Err_Code_t cmd_hdl_set_flow_rate(uint8_t *args)
{
    DEV_ASSERT(args);

    Cmd_Err_Code_t error_code = ERROR_NONE;

    uint32_t float_bytes = ((uint32_t)args[0] << 24U) +
                           ((uint32_t)args[1] << 16U) +
                           ((uint32_t)args[2] << 8U)  +
                           ((uint32_t)args[3]);

     error_code = DaqCtlAO_Pfcv_Set(BYTES_TO_F32(float_bytes));

    return error_code;
}


Cmd_Err_Code_t cmd_hdl_set_heat_i(uint8_t *args)
{
    DEV_ASSERT(args);

    uint32_t float_bytes = ((uint32_t)args[0] << 24U) +
                           ((uint32_t)args[1] << 16U) +
                           ((uint32_t)args[2] << 8U)  +
                           ((uint32_t)args[3]);

    Cmd_Err_Code_t error_code = DaqCtlAO_Heater_Set(BYTES_TO_F32(float_bytes));

    return error_code;
}


Cmd_Err_Code_t cmd_hdl_set_hpiso_i(uint8_t *args)
{
    DEV_ASSERT(args);
    Cmd_Err_Code_t error_code = ERROR_NONE;

    uint32_t float_bytes = ((uint32_t)args[0] << 24U) +
                           ((uint32_t)args[1] << 16U) +
                           ((uint32_t)args[2] << 8U)  +
                           ((uint32_t)args[3]);

    float i_mA = BYTES_TO_F32(float_bytes);

    error_code = DaqCtlAO_HighPIso_Set(i_mA);

    return error_code;
}


Cmd_Err_Code_t cmd_hdl_set_pfcv_i(uint8_t *args)
{
    DEV_ASSERT(args);
    Cmd_Err_Code_t error_code = ERROR_NONE;

    uint32_t float_bytes = ((uint32_t)args[0] << 24U) +
                           ((uint32_t)args[1] << 16U) +
                           ((uint32_t)args[2] << 8U)  +
                           ((uint32_t)args[3]);

    error_code = DaqCtlAO_Pfcv_SetCurrent(BYTES_TO_F32(float_bytes));

    return error_code;
}



Cmd_Err_Code_t cmd_hdl_get_tlm(UNUSED uint8_t *args)
{
    DEV_ASSERT(args);
    Cmd_Err_Code_t error_code = ERROR_NONE;


    /* cmd_data is a private variable and has the current info for the command */
    TlmAO_RequestTlm(&AO_Cmd, cmd_data.port, cmd_data.seq_id, cmd_data.src_id, cmd_data.msg_id);

    return error_code;
}


Cmd_Err_Code_t cmd_hdl_set_auto_tlm(uint8_t *args)
{
    DEV_ASSERT(args);
    Cmd_Err_Code_t error_code = ERROR_NONE;


    uint8_t auto_en = args[0];
    uint32_t period_ms = ((uint32_t)args[1] << 24U) +
                         ((uint32_t)args[2] << 16U) +
                         ((uint32_t)args[3] << 8U)  +
                         ((uint32_t)args[4]);

    if (auto_en == 0U)
    {
        TlmAO_AutoTlmEnable(cmd_data.src_id, cmd_data.port, false);
    }
    else
    {
        error_code = TlmAO_AutoTlmPeriod(period_ms);
        if (error_code.reported_error == Errors.StatusSuccess)
        {
            TlmAO_AutoTlmEnable(cmd_data.src_id, cmd_data.port, true);
        }
    }

    return error_code;
}


Cmd_Err_Code_t cmd_hdl_clr_rec_tlm(UNUSED uint8_t *args)
{
    Cmd_Err_Code_t error_code = ERROR_NONE;

    /* Pushing the event always succeeds, however since it is a request
     * command, let the DataAO handle responding if the command is
     * accepted or rejected. */
    GLADOS_AO_PushEvtName(&AO_Cmd, &AO_Data, EVT_CMD_CLEAR_REC_TLM);

    return error_code;
}


Cmd_Err_Code_t cmd_hdl_dump_mcu(uint8_t *args)
{
    DEV_ASSERT(args);

    enum Constants
    {
        ACK_STATUS_LENGTH      = 2,
        MCU_PFLASH_START_ADDR  = 0x0,
        MCU_PFLASH_END_ADDR    = 0x200000,
        MCU_FLEXNVM_START_ADDR = 0x10000000,
        MCU_FLEXNVM_END_ADDR   = 0x10080000,
        MCU_SRAM_START_ADDR    = 0x1FFE0000,
        MCU_SRAM_END_ADDR      = 0x2001F000,
        MCU_PERIPH_START_ADDR  = 0x40000000,
        MCU_PERIPH_END_ADDR    = 0x40100000
    };

    /* Convert from BE to LE */
    uint32_t mcu_addr = ((uint32_t)args[0] << 24U) +
                        ((uint32_t)args[1] << 16U) +
                        ((uint32_t)args[2] << 8U)  +
                        ((uint32_t)args[3]);

    uint32_t data_len = (uint32_t)args[4];

    /* Starting with error, normal operation should cast to status successful */
    Cmd_Err_Code_t error_code = create_error_uint(Errors.CmdInvalidMcuDataDumpSize,  data_len);

    /* Validate the address and length fall within one of the valid
     * memory regions */
    if (( (mcu_addr <  MCU_PFLASH_END_ADDR)                 &&
          ((mcu_addr + data_len) <= MCU_PFLASH_END_ADDR))      ||
        ( (mcu_addr >= MCU_FLEXNVM_START_ADDR)              &&
          (mcu_addr <  MCU_FLEXNVM_END_ADDR)                &&
          ((mcu_addr + data_len) >  MCU_FLEXNVM_START_ADDR) &&
          ((mcu_addr + data_len) <= MCU_FLEXNVM_END_ADDR))     ||
        ( (mcu_addr >= MCU_SRAM_START_ADDR)                 &&
          (mcu_addr <  MCU_SRAM_END_ADDR)                   &&
          ((mcu_addr + data_len) >  MCU_SRAM_START_ADDR)    &&
          ((mcu_addr + data_len) <= MCU_SRAM_END_ADDR))        ||
        ( (mcu_addr >= MCU_PERIPH_START_ADDR)               &&
          (mcu_addr <  MCU_PERIPH_END_ADDR)                 &&
          ((mcu_addr + data_len) >  MCU_PERIPH_START_ADDR)  &&
          ((mcu_addr + data_len) <= MCU_PERIPH_END_ADDR)))
    {
        /* Validate that the length of data requested fits within a clank payload */
        if (data_len <= (TXAO_CLANK_MAX_XFER_SIZE - ACK_STATUS_LENGTH))
        {
             error_code = ERROR_NONE;

            /* Initiate the transmit */
            TxAO_SendMsg_Data_wAck(&AO_Cmd, (TxPort_t)cmd_data.port, cmd_data.src_id, cmd_data.seq_id,
                                       cmd_data.msg_id, mcu_addr, data_len);

            /* Pushing this event to self serves as a way to avoid returning a status message for this
             * specific command. How this works is that this command is configured as a 'request command'
             * and transitions into this state immediately after returning from this function, which does
             * not transmit a status TLM packet as a result and awaits an ACK/NACK from the requesting
             * state. In this case, there is no requesting state and, instead, an ACK (without send) is
             * pushed here to indicate that the command was executed and is ready to receive the next
             * message. */
            GLADOS_AO_PushEvtName(&AO_Cmd, &AO_Cmd, EVT_CMD_ACK);
        }
    }

    return error_code;
}


Cmd_Err_Code_t cmd_hdl_dump_tlm(uint8_t *args)
{
    DEV_ASSERT(args);
    Cmd_Err_Code_t error_code = ERROR_NONE;

    uint8_t mem_type = args[0];
    uint8_t mem_bank = args[1];

    /* Validate that parameters indicate dumping from
     * either FRAM (0xA) or NAND (0xB) and that a valid
     * bank is selected (2 - dump both banks 0 and 1) */
    if (((mem_type == DATAAO_MEMTYPE_FRAM) || (mem_type == DATAAO_MEMTYPE_NAND)) &&
        ((mem_bank <= 2U)))
    {
        DataAO_DumpTlmRequest(&AO_Cmd, cmd_data.port, cmd_data.seq_id, cmd_data.src_id, cmd_data.msg_id, mem_type, mem_bank);
    }
    /*Checking which part of the condition is wrong for a more precise error */
    else if ((mem_type != DATAAO_MEMTYPE_FRAM) && (mem_type != DATAAO_MEMTYPE_NAND))
    {
        error_code = create_error_uint(Errors.CmdTlmDumpInvalidMemChipSelected,  mem_type);
    }
    else {
        error_code = create_error_uint(Errors.CmdTlmDumpInvalidBankSelected,  mem_bank);
    }

    return error_code;
}


Cmd_Err_Code_t cmd_hdl_set_rec_tlm_en(UNUSED uint8_t *args)
{
    DEV_ASSERT(args);
    Cmd_Err_Code_t error_code = ERROR_NONE;

    GLADOS_AO_PushEvtName(&AO_Cmd, &AO_Data, EVT_CMD_START_RECORDING);

    return error_code;
}


Cmd_Err_Code_t cmd_hdl_set_rec_nand_en(uint8_t *args)
{
    DEV_ASSERT(args);
    Cmd_Err_Code_t error_code = ERROR_NONE;
    DataAO_SetRecord_Nand((bool)args[0]);
    return error_code;
}


Cmd_Err_Code_t cmd_hdl_set_rec_fram_en(uint8_t *args)
{
    DEV_ASSERT(args);
    Cmd_Err_Code_t error_code = ERROR_NONE;
    DataAO_SetRecord_Fram((bool)args[0]);
    return error_code;
}


Cmd_Err_Code_t cmd_hdl_set_rec_bank(uint8_t *args)
{
    DEV_ASSERT(args);
    Cmd_Err_Code_t error_code = ERROR_NONE;
    DataAO_SetRecord_Bank(args[0]);
    return error_code;
}


Cmd_Err_Code_t cmd_hdl_abort_data_op(UNUSED uint8_t *args)
{
    Cmd_Err_Code_t error_code = ERROR_NONE;

    /* Note: This command always succeeds and therefore does not
     * need to be a request command. */
    GLADOS_AO_PushEvtName(&AO_Cmd, &AO_Data, EVT_CMD_DATA_ABORT);
    return error_code;
}


Cmd_Err_Code_t cmd_hdl_sw_up_preload(uint8_t *args)
{
    DEV_ASSERT(args);

    Cmd_Err_Code_t error_code = ERROR_NONE;
    GLADOS_Event_t evt;
    DataAO_PreloadData_t preld_data;

    /* Convert from BE to LE */
    uint32_t pflash_addr = ((uint32_t)args[0] << 24U) +
                           ((uint32_t)args[1] << 16U) +
                           ((uint32_t)args[2] << 8U)  +
                           ((uint32_t)args[3]);

    uint32_t length      = ((uint32_t)args[4] << 24U) +
                           ((uint32_t)args[5] << 16U) +
                           ((uint32_t)args[6] << 8U)  +
                           ((uint32_t)args[7]);

    uint32_t fram_offset = ((uint32_t)args[8] << 24U) +
                           ((uint32_t)args[9] << 16U) +
                           ((uint32_t)args[10] << 8U) +
                           ((uint32_t)args[11]);

    /* All addresses intended to be for the RFT must have this mask in its
     * address space.  If not, then it is intended for the Prop Ctrl.
     * Otherwise if there is a mixed address space, then return an error. */
    if (((pflash_addr & RFT_VIRTUAL_ADDR_MASK) == RFT_VIRTUAL_ADDR_MASK) &&
        ((fram_offset & RFT_VIRTUAL_ADDR_MASK) == RFT_VIRTUAL_ADDR_MASK))
    {
        /* Step in and mask out the virtual RFT addr. Setting the full byte
         * rather than just the MSnibble to zero is okay since the address
         * space does not extend beyond 0x0020_0000 */
        args[0] = 0U;    /* Clear virtual addr for pflash_addr param */
        args[8] = 0U;    /* Clear virtual addr for fram_offset param */

        RftAO_push_cmd_request(&AO_Cmd, RFT_CMD_INTERNAL, SW_UP_PRELOAD, cmd_data.data_ptr, cmd_data.data_size, cmd_data.port);
    }
    /* No RFT virtual addresses means it is intended for Prop Ctrl */
    else if (((pflash_addr & RFT_VIRTUAL_ADDR_MASK) == 0x0) &&
             ((fram_offset & RFT_VIRTUAL_ADDR_MASK) == 0x0))
    {
        preld_data.pflash_addr = pflash_addr;
        preld_data.length      = length;
        preld_data.fram_offset = fram_offset;

        GLADOS_Event_New_Data(&evt, EVT_CMD_UPDATE_PRELOAD, (uint8_t *)&preld_data, sizeof(DataAO_PreloadData_t));
        GLADOS_AO_PushEvt(&AO_Cmd, &AO_Data, &evt);
    }
    else
    {
        /* Virtual address space mismatch amongst parameters */
        error_code = create_error_uint(Errors.CmdRftVirtualAddrMismatch, 0UL);
    }

    return error_code;
}


Cmd_Err_Code_t custom_cmd_hdl_sw_up_stage(uint8_t *args, uint8_t payload_length)
{
    DEV_ASSERT(args);

    enum Constants { MIN_DATA_LENGTH = 4U };

    Cmd_Err_Code_t error_code = ERROR_NONE;
    GLADOS_Event_t evt;
    DataAO_StageData_t stage_data;

    /* Convert from BE to LE */
    uint32_t fram_offset = ((uint32_t)args[0] << 24U) +
                           ((uint32_t)args[1] << 16U) +
                           ((uint32_t)args[2] << 8U)  +
                           ((uint32_t)args[3]);

    /* Staging must have at least 1 byte to stage for the given address */
    if (payload_length <= MIN_DATA_LENGTH)
    {
        error_code = create_error_uint(Errors.CmdSwUpdateBelowMinDataAmount,  payload_length);
    }
    /* All addresses intended to be for the RFT must have this mask in its
     * address space.  If not, then it is intended for the Prop Ctrl. */
    else if ((fram_offset & RFT_VIRTUAL_ADDR_MASK) == RFT_VIRTUAL_ADDR_MASK)
    {
        /* Step in and mask out the virtual RFT addr. Setting the full byte
         * rather than just the MSnibble to zero is okay since the address
         * space does not extend beyond 0x0020_0000 */
        args[0] = 0U;    /* Clear virtual addr for fram_offset param */

        RftAO_push_cmd_request(&AO_Cmd, RFT_CMD_INTERNAL, SW_UP_STAGE, cmd_data.data_ptr, cmd_data.data_size, cmd_data.port);
    }
    else
    {
        /* Command is intended for the Prop Controller */
        stage_data.fram_offset = fram_offset;
        /* The data pointer points to the payload data just after the fram_offset parameter */
        stage_data.data_ptr    = (uint32_t)&args[MIN_DATA_LENGTH];
        stage_data.length      = (uint32_t)payload_length - (uint32_t)MIN_DATA_LENGTH;

        GLADOS_Event_New_Data(&evt, EVT_CMD_UPDATE_STAGE, (uint8_t *)&stage_data, sizeof(DataAO_StageData_t));
        GLADOS_AO_PushEvt(&AO_Cmd, &AO_Data, &evt);
    }

    return error_code;
}


Cmd_Err_Code_t cmd_hdl_sw_up_program(uint8_t *args)
{
    DEV_ASSERT(args);

    Cmd_Err_Code_t error_code = ERROR_NONE;
    GLADOS_Event_t evt;
    DataAO_ProgData_t prog_data;

    /* Convert from BE to LE */
    uint32_t pflash_addr = ((uint32_t)args[0] << 24U) +
                           ((uint32_t)args[1] << 16U) +
                           ((uint32_t)args[2] << 8U)  +
                           ((uint32_t)args[3]);

    uint32_t length      = ((uint32_t)args[4] << 24U) +
                           ((uint32_t)args[5] << 16U) +
                           ((uint32_t)args[6] << 8U)  +
                           ((uint32_t)args[7]);

    uint32_t crc         = ((uint32_t)args[8] << 24U) +
                           ((uint32_t)args[9] << 16U) +
                           ((uint32_t)args[10] << 8U) +
                           ((uint32_t)args[11]);

    /* All addresses intended to be for the RFT must have this mask in its
     * address space.  If not, then it is intended for the Prop Ctrl. */
    if ((pflash_addr & RFT_VIRTUAL_ADDR_MASK) == RFT_VIRTUAL_ADDR_MASK)
    {
        /* Step in and mask out the virtual RFT addr. Setting the full byte
         * rather than just the MSnibble to zero is okay since the address
         * space does not extend beyond 0x0020_0000 */
        args[0] = 0U;    /* Clear virtual addr for pflash_addr param */

        RftAO_push_cmd_request(&AO_Cmd, RFT_CMD_INTERNAL, SW_UP_PROGRAM, cmd_data.data_ptr, cmd_data.data_size, cmd_data.port);
    }
    else
    {
        /* Command is intended for the Prop Controller */
        prog_data.pflash_addr = pflash_addr;
        prog_data.length      = length;
        prog_data.crc         = crc;

        GLADOS_Event_New_Data(&evt, EVT_CMD_UPDATE_PROGRAM, (uint8_t *)&prog_data, sizeof(DataAO_ProgData_t));
        GLADOS_AO_PushEvt(&AO_Cmd, &AO_Data, &evt);
    }

    return error_code;
}


Cmd_Err_Code_t cmd_hdl_set_wdi_en(uint8_t *args)
{
    DEV_ASSERT(args);
    Cmd_Err_Code_t error_code = ERROR_NONE;
    GPIO_SetOutput(GPIO_WDT_PET, (bool)args[0]);
    return error_code;
}


Cmd_Err_Code_t cmd_hdl_set_rft_rst_en(uint8_t *args)
{
    DEV_ASSERT(args);
    Cmd_Err_Code_t error_code = ERROR_NONE;
    DaqCtlAO_RFT_ResetEnable((bool)args[0]);
    return error_code;
}


Cmd_Err_Code_t cmd_hdl_set_heater_en(uint8_t *args)
{
    DEV_ASSERT(args);
    Cmd_Err_Code_t error_code = ERROR_NONE;
    DaqCtlAO_Heater_Enable((bool)args[0]);
    return error_code;
}


Cmd_Err_Code_t cmd_hdl_set_heater_rst_en(uint8_t *args)
{
    DEV_ASSERT(args);
    Cmd_Err_Code_t error_code = ERROR_NONE;
    GPIO_SetOutput(GPIO_HEATER_nRST_EN, (bool)args[0]);
    return error_code;
}


Cmd_Err_Code_t cmd_hdl_set_hpiso_en(uint8_t *args)
{
    DEV_ASSERT(args);
    Cmd_Err_Code_t error_code = ERROR_NONE;

    if (args[0] == 0U)
    {
        /* Always permit disabling the HPISO valve */
        DaqCtlAO_HighPIso_Enable(false);
    }
    else
    {
        DaqCtlAO_HighPIso_Enable(true);
    }

    return error_code;
}


Cmd_Err_Code_t cmd_hdl_set_pfcv_dac_rst_en(uint8_t *args)
{
    DEV_ASSERT(args);
    Cmd_Err_Code_t error_code = ERROR_NONE;
    GPIO_SetOutput(GPIO_PFCV_DAC_nRST_EN, (bool)args[0]);
    return error_code;
}


Cmd_Err_Code_t cmd_hdl_debug_leds(uint8_t *args)
{
    DEV_ASSERT(args);
    Cmd_Err_Code_t error_code = ERROR_NONE;
    GPIO_SetLEDs(args[0]);
    return error_code;
}


Cmd_Err_Code_t cmd_hdl_config_thrust_adv(uint8_t *args)
{
    DEV_ASSERT(args);

    Cmd_Err_Code_t error_rec = ERROR_NONE;

    uint32_t float_bytes_rf_vset = ((uint32_t)args[0] << 24U) +
                                   ((uint32_t)args[1] << 16U) +
                                   ((uint32_t)args[2] << 8U)  +
                                   ((uint32_t)args[3]);

    uint32_t float_bytes_mdot_sccm = ((uint32_t)args[4] << 24U) +
                                     ((uint32_t)args[5] << 16U) +
                                     ((uint32_t)args[6] << 8U)  +
                                     ((uint32_t)args[7]);

    uint32_t float_bytes_dur_sec = ((uint32_t)args[8]  << 24U) +
                                   ((uint32_t)args[9]  << 16U) +
                                   ((uint32_t)args[10] << 8U)  +
                                   ((uint32_t)args[11]);

    uint32_t float_bytes_puff_mdot = ((uint32_t)args[12]  << 24U) +
                                     ((uint32_t)args[13]  << 16U) +
                                     ((uint32_t)args[14]  << 8U)  +
                                     ((uint32_t)args[15]);

    uint32_t float_bytes_wf_gen_freq = ((uint32_t)args[16]  << 24U) +
                                       ((uint32_t)args[17]  << 16U) +
                                       ((uint32_t)args[18]  << 8U)  +
                                       ((uint32_t)args[19]);

    uint32_t float_bytes_puff_sec = ((uint32_t)args[20]  << 24U) +
                                    ((uint32_t)args[21]  << 16U) +
                                    ((uint32_t)args[22]  << 8U)  +
                                    ((uint32_t)args[23]);

    uint32_t float_bytes_ignite_sec = ((uint32_t)args[24]  << 24U) +
                                      ((uint32_t)args[25]  << 16U) +
                                      ((uint32_t)args[26]  << 8U)  +
                                      ((uint32_t)args[27]);

    uint32_t float_bytes_ignite_vset = ((uint32_t)args[28]  << 24U) +
                                       ((uint32_t)args[29]  << 16U) +
                                       ((uint32_t)args[30]  << 8U)  +
                                       ((uint32_t)args[31]);

    uint32_t float_bytes_pp_ramp_sec = ((uint32_t)args[32]  << 24U) +
                                        ((uint32_t)args[33]  << 16U) +
                                        ((uint32_t)args[34]  << 8U)  +
                                        ((uint32_t)args[35]);

    MaxData.thrust_rf_vset_fb     = BYTES_TO_F32(float_bytes_rf_vset);
    MaxData.thrust_mdot_fb        = BYTES_TO_F32(float_bytes_mdot_sccm);
    MaxData.thrust_dur_sec_fb     = BYTES_TO_F32(float_bytes_dur_sec);
    MaxData.thrust_puff_mdot_fb   = BYTES_TO_F32(float_bytes_puff_mdot);
    MaxData.thrust_wf_gen_freq_fb = BYTES_TO_F32(float_bytes_wf_gen_freq);
    MaxData.thrust_puff_dur_us_fb = (uint32_t)(BYTES_TO_F32(float_bytes_puff_sec) * 1000000.0f);
    MaxData.thrust_ignite_us_fb   = (uint32_t)(BYTES_TO_F32(float_bytes_ignite_sec) * 1000000.0f);
    MaxData.thrust_ignite_vset_fb = BYTES_TO_F32(float_bytes_ignite_vset);
    MaxData.thrust_pp_ramp_us_fb = (uint32_t)(BYTES_TO_F32(float_bytes_pp_ramp_sec) * 1000000.0f);

    MaxData.thrust_adv_cmd_rcvd   = true;

    return error_rec;
}


Cmd_Err_Code_t cmd_hdl_heater_pwm_raw(uint8_t *args)
{
    DEV_ASSERT(args);
    uint16_t duty_cycle_cnt = ((uint16_t)args[0] << 8U) + (uint16_t)args[1];
    Cmd_Err_Code_t error_code = DaqCtlAO_Heater_SetPwmRaw(duty_cycle_cnt);
    return error_code;
}


Cmd_Err_Code_t cmd_hdl_hpiso_pwm_raw(uint8_t *args)
{
    DEV_ASSERT(args);
    Cmd_Err_Code_t error_code = ERROR_NONE;
    uint16_t duty_cycle_cnt = ((uint16_t)args[0] << 8U) + (uint16_t)args[1];
    error_code = DaqCtlAO_HighPIso_SetPwmRaw(duty_cycle_cnt);
    return error_code;
}


Cmd_Err_Code_t cmd_hdl_pfcv_dac_raw(uint8_t *args)
{
    DEV_ASSERT(args);
    Cmd_Err_Code_t error_code = ERROR_NONE;
    uint16_t dac_code = ((uint16_t)args[0] << 8U) + (uint16_t)args[1];
    error_code = DaqCtlAO_Pfcv_SetDacRaw(dac_code);
    return error_code;
}


Cmd_Err_Code_t cmd_hdl_get_tlm_rft(UNUSED uint8_t *args)
{
    DEV_ASSERT(args);
    Cmd_Err_Code_t error_code = ERROR_NONE;
    /* cmd_data is a private variable and has the current info for the command */
    TlmAO_RequestTlm_Rft(&AO_Cmd, cmd_data.port, cmd_data.seq_id, cmd_data.src_id, cmd_data.msg_id);
    return error_code;
}


Cmd_Err_Code_t cmd_hdl_stp_wtd(UNUSED uint8_t *args)
{
    Cmd_Err_Code_t error_code = ERROR_NONE;
    WdtAO_Stop_WDT(&AO_Cmd);
    return error_code;
}


Cmd_Err_Code_t cmd_hdl_prg_flsh(UNUSED uint8_t *args)
{
    Cmd_Err_Code_t error_code = ERROR_NONE;
    GLADOS_AO_PushEvtName(&AO_Cmd, &AO_Err, EVT_WRITE_FLASH);
    return error_code;
}


Cmd_Err_Code_t cmd_hdl_wrt_err_bulk(UNUSED uint8_t *args)
{
    Cmd_Err_Code_t error_code = ERROR_NONE;
    uint8_t real_data [6]={0x22,0x33,0x77,0x88,0x99,0xAA};
    GLADOS_Event_t evt;
    GLADOS_Event_New_Data(&evt, EVT_REPORT_ERR_BULK,real_data,6);

    GLADOS_AO_PushEvt(&AO_Cmd, &AO_Err, &evt);

    return error_code;
}


Cmd_Err_Code_t cmd_hdl_wrt_err(uint8_t *args)
{
    Cmd_Err_Code_t error_code = ERROR_NONE;
    static uint16_t increment=0;
    uint8_t spaceraft_visable = args[2];
    uint16_t msg_id = ((uint16_t)args[0] << 8U) + (uint16_t)args[1];
    err_type* error_sent = (err_type*) Errors.StatusSuccess;
    switch(msg_id){
        case 1:
            error_sent = (err_type*) Errors.EmbeddedTimeout;
            break;
        case 2:
            error_sent = (err_type*)Errors.NotEnoughBytes;
            break;
        case 3:
            error_sent =  (err_type*) Errors.MalformedHeader;
            break;
        case 4:
            error_sent = (err_type*) Errors.SyncBytesError;
            break;
        case 5:
            error_sent = (err_type*) Errors.SourceAddressError;
            break;
        case 6:
            error_sent = (err_type*) Errors.TransmitMaskError ;
            break;
        case 7:
            error_sent = (err_type*) Errors.PktDataLenError ;
            break;
        case 8:
            error_sent = (err_type*) Errors.ComRxCrcError;
            break;
        case 9:
            error_sent = (err_type*) Errors.MsgIdFunctionNotImplemented ;
            break;
        case 10:
            error_sent = (err_type*) Errors.CurrentProcessedCommandTimedout;
            break;
        case 11:
            error_sent = (err_type*) Errors.UnexpectedTransition;
            break;
        case 12:
            error_sent = (err_type*) Errors.Ad568SpiSetFailed;
            break;
        case 13:
            error_sent = (err_type*) Errors.MdotSetPointOutOfBounds ;
            break;
        case 14:
            error_sent = (err_type*) Errors.InvalidTxParameters;
            break;
        case 15:
            error_sent = (err_type*) Errors.IntermediateTxQueueHasNoData;
            break;
        case 16:
            error_sent = (err_type*) Errors.TxAoIsBusy;
            break;
        case 17:
            error_sent = (err_type*) Errors.TlmTmrTimeout;
            break;
        case 18:
            error_sent = (err_type*) Errors.InvalidTelemtryEventRequest;
            break;
        case 19:
            error_sent = (err_type*) Errors.RxScReciviedJunkData;
            break;
        case 20:
            error_sent = (err_type*) Errors.ReceivedMalformClankPacket;
            break;
        case 21:
            error_sent = (err_type*) Errors.ReceivedRxscTimeout;
            break;
        case 22:
            error_sent = (err_type*) Errors.MismatchCrc;
            break;
        case 23:
            error_sent = (err_type*) Errors.GseRxCircularBufferOverrun;
            break;
        case 24:
            error_sent = (err_type*) Errors.EvtCmdDumpTlmInvalidSize;
            break;
        case 25:
            error_sent = (err_type*) Errors.EvtDataOpTimeout;
            break;
        case 26:
            error_sent = (err_type*) Errors.CmdInvalidDurationSetpt;
            break;
        case 27:
            error_sent = (err_type*) Errors.CmdDataModeIsBusy;
            break;
        case 28:
            error_sent = (err_type*) Errors.CmdUpdateStageInvalidParam;
            break;
        case 29:
            error_sent = (err_type*) Errors.CmdUpdatePreloadInvalidParam;
            break;
        case 30:
            error_sent = (err_type*) Errors.CmdUpdateProgramInvalidParam;
            break;
        case 31:
             error_sent = (err_type*) Errors.CmdDataAbortDueToSafe;
             break;
    };

    report_error_uint(&AO_Cmd, error_sent,spaceraft_visable,0x56789ABCUL+increment);

    increment = increment + 1;
    return error_code;
}


Cmd_Err_Code_t cmd_hdl_trnsf_flash(UNUSED uint8_t *args)
{
    Cmd_Err_Code_t error_code = ERROR_NONE;
    GLADOS_AO_PushEvtName(&AO_Cmd, &AO_Err, EVT_TRANSFER_TO_NVM);
    return error_code;
}


Cmd_Err_Code_t cmd_hdl_get_sw_info(uint8_t *args)
{
    bool is_rft_virtual = false;
    Cmd_Err_Code_t error_code = ERROR_NONE;
    uint8_t sw_application_selection=args[0];

    /* Check if the command is requesting an RFT virtual */

    if ((sw_application_selection & 0xF0U) == 0xF0UL)
    {
        /* Mask out the RFT virtual portion of the parameter */
        sw_application_selection &= 0x0FUL;
        is_rft_virtual = true;
    }

    if (is_rft_virtual)
    {
        /* Overwrite the parameter and forward to the RFT */
        args[0] = sw_application_selection;
        RftAO_push_cmd_request(&AO_Cmd, RFT_CMD_INTERNAL, GET_SW_INFO, cmd_data.data_ptr, cmd_data.data_size, cmd_data.port);
    }
    else
    {
        error_code = update_flash_hdr(sw_application_selection); /*Changes the header_address global variable in max_info_driver  :( to specify the correct address to send the correct software information on */
        if (error_code.reported_error->error_id==STATUS_SUCCESS)
        {

            TxAO_SendMsg_Data_wAck(&AO_Cmd, (TxPort_t)cmd_data.port, (uint8_t)cmd_data.src_id,
                                   cmd_data.seq_id, cmd_data.msg_id, header_address, sizeof(App_Header_Rec_t));

            /* Push Ack to self so we can accept and return from being a request command */
            GLADOS_AO_PushEvtName(&AO_Cmd, &AO_Cmd, EVT_CMD_ACK);
        }
    }

    return error_code;
}

Cmd_Err_Code_t cmd_hdl_write_max_hdr_info(uint8_t *args)
{
    DEV_ASSERT(args);
    Cmd_Err_Code_t error_code = write_maxwell_hrd_record(&AO_Cmd,(char*)args,129);
    return error_code;
}


Cmd_Err_Code_t cmd_hdl_read_max_hdr_info(uint8_t *args)
{
    DEV_ASSERT(args);

    uint8_t mcu_dump_args[5];

    Cmd_Err_Code_t error_code = calculate_maxwell_hrd_record_address(args[0] ,mcu_dump_args);
    if (error_code.reported_error->error_id==STATUS_SUCCESS){
        error_code = cmd_hdl_dump_mcu(mcu_dump_args);
    }
    return error_code;
}


Cmd_Err_Code_t cmd_hdl_erase_max_hdr_info(UNUSED uint8_t *args)
{
    Cmd_Err_Code_t error_code = ERROR_NONE;
    erase_max_hrd_record(&AO_Cmd);
    return error_code;
}


Cmd_Err_Code_t cmd_hdl_set_auto_status(uint8_t *args)
{
    DEV_ASSERT(args);
    Cmd_Err_Code_t error_code = ERROR_NONE;

    uint8_t auto_en = args[0];
    uint32_t period_ms = ((uint32_t)args[1] << 24U) +
                         ((uint32_t)args[2] << 16U) +
                         ((uint32_t)args[3] << 8U)  +
                         ((uint32_t)args[4]);

    if (auto_en == 0U)
    {
        TlmAO_AutoTlmEnable(cmd_data.src_id, cmd_data.port, false);
    }
    else
    {
        error_code = TlmAO_AutoStatusPeriod(period_ms);
        if (error_code.reported_error == Errors.StatusSuccess)
        {
            TlmAO_AutoStatusEnable(cmd_data.src_id, cmd_data.port, true);
        }
    }

    return error_code;
}


Cmd_Err_Code_t cmd_hdl_config_thrust(uint8_t *args)
{
    DEV_ASSERT(args);

    uint32_t float_bytes_rf_vset = ((uint32_t)args[0] << 24U) +
                                   ((uint32_t)args[1] << 16U) +
                                   ((uint32_t)args[2] << 8U)  +
                                   ((uint32_t)args[3]);

    uint32_t float_bytes_mdot_sccm = ((uint32_t)args[4] << 24U) +
                                     ((uint32_t)args[5] << 16U) +
                                     ((uint32_t)args[6] << 8U)  +
                                     ((uint32_t)args[7]);

    uint32_t float_bytes_dur_sec = ((uint32_t)args[8]  << 24U) +
                                   ((uint32_t)args[9]  << 16U) +
                                   ((uint32_t)args[10] << 8U)  +
                                   ((uint32_t)args[11]);

    float rf_vset   = BYTES_TO_F32(float_bytes_rf_vset);
    float mdot_sccm = BYTES_TO_F32(float_bytes_mdot_sccm);
    float dur_sec   = BYTES_TO_F32(float_bytes_dur_sec);

    Cmd_Err_Code_t error_rec = MaxAO_ValidateThrustParams(rf_vset, mdot_sccm, dur_sec);

    if (error_rec.reported_error == Errors.StatusSuccess)
    {
        MaxData.thrust_rf_vset_fb = rf_vset;
        MaxData.thrust_mdot_fb    = mdot_sccm;
        MaxData.thrust_dur_sec_fb = dur_sec;
        MaxData.thrust_adv_cmd_rcvd = false;
    }

    return error_rec;
}


Cmd_Err_Code_t cmd_hdl_test_thrust_anomaly(uint8_t *args)
{
    DEV_ASSERT(args);
    Cmd_Err_Code_t error_rec = ERROR_NONE;

#ifdef DUMMY_LOAD_STUBS
    MaxData.stub_thrust_anomaly_sec_cnt = ((uint32_t)args[0] << 24U) +
                                          ((uint32_t)args[1] << 16U) +
                                          ((uint32_t)args[2] << 8U)  +
                                          ((uint32_t)args[3]);
#endif

    return error_rec;
}


Cmd_Err_Code_t cmd_hdl_test_needs_reset(uint8_t *args)
{
    DEV_ASSERT(args);
    Cmd_Err_Code_t error_rec = ERROR_NONE;

#ifdef DUMMY_LOAD_STUBS
    MaxData.stub_curr_low_pwr_sec_cnt = 0U;
    MaxData.stub_needs_reset_sec_cnt = ((uint32_t)args[0] << 24U) +
                                       ((uint32_t)args[1] << 16U) +
                                       ((uint32_t)args[2] << 8U)  +
                                       ((uint32_t)args[3]);
#endif

    return error_rec;
}


Cmd_Err_Code_t cmd_hdl_dft_sram_ecc_1bit(uint8_t *args)
{
    DEV_ASSERT(args);

    enum Constants { SRAM_DUMMY_ADDR = 0x1FFFFFF0 };

    Cmd_Err_Code_t error_rec = ERROR_NONE;
    uint8_t num_of_reads = args[0];

    uint8_t i;
    uint32_t dummy_read;

    /* Initialize address used to test */
    *(uint32_t *)SRAM_DUMMY_ADDR = 0U;

    EIM_DRV_Init(INST_EIM1, EIM_CHANNEL_COUNT0, eim1_ChannelConfig0);

    for (i = 0U; i < num_of_reads; ++i)
    {
        /* Read any address on RAM  */
        /* Enable read region Ram (0x1FFF8000 - 0x20006FFF) when debug equal Flash */
        dummy_read = *(uint32_t *)SRAM_DUMMY_ADDR;
    }

    /* Removes compiler warning */
    (void)dummy_read;

    /* Deinit EIM module */
    EIM_DRV_Deinit(INST_EIM1);

    return error_rec;
}


Cmd_Err_Code_t cmd_hdl_dft_pt_sell_your_soul(UNUSED uint8_t *args)
{
    DEV_ASSERT(args);
    DEV_ASSERT(((uint32_t)PT_Default % 4U) == 0U);
    DEV_ASSERT(((uint32_t)PT % 4U) == 0U);

    enum Constants { PT_SYS_MODE_MASK = 0xF0 };

    Cmd_Err_Code_t error_rec = ERROR_NONE;
    Img_Header_Rec_t *img_hdr;
    uint32_t act_img_crc;

    /* Sell your soul is intended as a way to adjust PT values
     * while the FSW is running. It is entered by performing
     * the following steps:
     * 1. Determine the PT that is in use
     *    a. If default table is in use, then nothing more to
     *       do since we've already sold our soul
     * 2. Integrity check the PT (unless if it's default PT)
     * 3. Copy over the PT to the default table
     */

    /* If PT is loaded from either Runtime or App tables, then perform the CRC and copying.
     * Note: Redo if already in SYS since it can reset with a valid PT config. */
    if ((MaxData.pt_select == 1U) ||
        (MaxData.pt_select == 2U))
    {
        /* Start from the PT core and work backward to the image header */
        img_hdr = (Img_Header_Rec_t *)(((uint32_t)PT) - PARAM_TABLE_PROP_INV_SIZE - PARAM_TABLE_IMG_HDR_SIZE);

        act_img_crc = crc32_compute(0xFFFFFFFFUL, (uint8_t *)((uint32_t)img_hdr + IMG_HDR_MAX_SIZE),
                                        img_hdr->img_length, true);

        if (img_hdr->img_crc == act_img_crc)
        {
            /* Copy the PT into the default PT and point the current PT there */
            memcpy((uint8_t *)PT_Default, (uint8_t *)PT, PARAM_TABLE_CORE_SIZE);
            PT = PT_Default;
            MaxData.pt_select |= PT_SYS_MODE_MASK;
        }
        else
        {
            error_rec = create_error_uint(Errors.CmdNoSoulToSell, act_img_crc);
        }
    }
    else
    {
        MaxData.pt_select |= PT_SYS_MODE_MASK;
    }

    return error_rec;
}

Cmd_Err_Code_t cmd_hdl_dft_pt_set(uint8_t *args)
{
    DEV_ASSERT(args);
    DEV_ASSERT(((uint32_t)PT_Default % 4U) == 0U);

    uint32_t *pt_value;

    Cmd_Err_Code_t error_rec = ERROR_NONE;

    uint32_t pt_offset = ((uint32_t)args[0] << 24U) +
                         ((uint32_t)args[1] << 16U) +
                         ((uint32_t)args[2] << 8U)  +
                         ((uint32_t)args[3]);

    uint32_t raw_value = ((uint32_t)args[4] << 24U) +
                         ((uint32_t)args[5] << 16U) +
                         ((uint32_t)args[6] << 8U)  +
                         ((uint32_t)args[7]);

    /* Since the PT core includes the Prop Inventory table at the
     * very beginning, subtract that table size from the offset */
    pt_offset -= PARAM_TABLE_PROP_INV_SIZE;

    /* All values in the PT are 4-byte aligned, so check for this
     * and also validate that the offset does not exceed the table
     * size or rollover a u32 */
    if (((pt_offset % 4U) != 0U)              ||
        ((pt_offset > PARAM_TABLE_CORE_SIZE)) ||
        ((pt_offset + sizeof(uint32_t)) >= PARAM_TABLE_CORE_SIZE))
    {
        error_rec = create_error_uint(Errors.CmdWishNotGranted, pt_offset);
    }
    else
    {
        /* Copying over a raw value should handle both u32 and f32 */
        pt_value = (uint32_t *)(((uint32_t)PT_Default) + pt_offset);
        *pt_value = raw_value;

        /* Reinitialize some items due to the new PT
         * Note: Not everything adjusted in this mode will take effect, only items that
         * are sampled at runtime will. PT items that are only used at startup will need
         * to be reinitialized here if adjustment in this mode is desired. */
        fidr_init();
        DaqCtrlAO_InitDrivers();
    }

    return error_rec;
}

Cmd_Err_Code_t cmd_hdl_set_next_app(uint8_t *args)
{
    DEV_ASSERT(args);

    static uint8_t command_state = 0;
    Cmd_Err_Code_t error_code = ERROR_NONE;

    if ((args[0] >= 1U) && (args[0] <= 3U))
    {
        if (command_state == 0)
        {
            /* Set slower frame rate and increase the RFT request timeout
             * to permit faster, more reliable select app ops. These are
             * returned when set next app command is completed. */
            DaqCtlAO_SetDaqRate(PT->max_swup_daq_period_us);
            RftAO_adjust_request_timeout(PT->max_swup_rft_timeout_us);

            RftAO_push_cmd_request(&AO_Cmd, RFT_CMD_SILENT, SET_NEXT_APP, cmd_data.data_ptr, cmd_data.data_size, cmd_data.port);


            status_t status = erase_app_selection_sector(&AO_Cmd);
            if (status != STATUS_SUCCESS)
            {
                /* Set error code, which does not transition to the request state */
                error_code = create_error_uint(Errors.FlashEraseFailed, (uint32_t)status);
            }
            else
            {
                command_state = 1;
                GLADOS_Timer_Disable(&Tmr_CmdCustomAppTimer);

                /* Set up the timer, worst case timing for sector erase is 130ms */
                GLADOS_Timer_Init(&Tmr_CmdCustomAppTimer, 130000UL, EVT_CMD_CUST_NEXT_APP_ERASE);
                Tmr_CmdCustomAppTimer.tmr_evt.data[0] = args[0];
                Tmr_CmdCustomAppTimer.tmr_evt.data_size = 1;
                Tmr_CmdCustomAppTimer.tmr_evt.has_data = true;
                GLADOS_Timer_Subscribe_AO(&Tmr_CmdCustomAppTimer, &AO_Cmd);
                GLADOS_Timer_EnableOnce(&Tmr_CmdCustomAppTimer);
                TxAO_SendMsg_Ack(&AO_Cmd, (TxPort_t)cmd_data.port,(uint8_t)cmd_data.src_id, cmd_data.seq_id, cmd_data.msg_id, error_code.reported_error->error_id);
            }
        }
        else if (command_state == 1)
        {
            /* Return to normal rates */
            DaqCtlAO_SetDaqRate(DAQCTLAO_PERIOD_US);
            RftAO_adjust_request_timeout(PT->rft_rqst_timeout_us);

            GLADOS_Timer_Disable(&Tmr_CmdCustomAppTimer);
            error_code = write_next_app_select(&AO_Cmd, args[0]);
            GLADOS_AO_PushEvtName(&AO_Cmd, &AO_Cmd, EVT_CMD_ACK);
            command_state = 0;


        }
    }
    else
    {
        error_code = create_error_uint(Errors.CmdInvalidParameter, args[0]);
    }

    return error_code;
}

Cmd_Err_Code_t cmd_hdl_aux_driving_cmd(UNUSED uint8_t *args)
{
    DEV_ASSERT(args);
    Cmd_Err_Code_t error_rec = ERROR_NONE;
    start_aux_driver();

    return error_rec;
}

