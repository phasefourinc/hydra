/* Copyright (c) 2017-2020 by Phase Four, Inc. All Rights Reserved. Confidential and Proprietary.
 *
 * TlmAO.c
 */


#include "TlmAO.h"
#include "mcu_types.h"
#include "glados.h"
#include "DaqCtlAO.h"
#include "CmdAO.h"
#include "TxAO.h"
#include "p4_tlm.h"
#include "max_data.h"
#include "rft_data.h"
#include "clank.h"
#include "p4_cmd.h"
#include "max_info_driver.h"
#include "DataAO.h"
#include "ErrAO.h"
#include "default_pt.h"
#include "string.h"

GLADOS_AO_t AO_Tlm;

/* Active Object timers */
GLADOS_Timer_t Tmr_TlmTimeout;
GLADOS_Timer_t Tmr_TlmAuto;

/* Active Object private variables */
static GLADOS_Event_t tlm_fifo[TLMAO_FIFO_SIZE];

static uint8_t tlm_daq_buf[TLMAO_MAX_REC_TLM_SIZE] = {0};
static uint8_t tlm_cmd_buf[TLMAO_MAX_REC_TLM_SIZE] = {0};
static uint8_t tlm_cmd2_buf[TLMAO_MAX_REC_TLM_SIZE] = {0};

static uint8_t auto_tlm_uart_port = PORT_SC;
static uint8_t auto_tlm_dest_id   = 0xFFU;
static uint8_t tlm_cnt_seq_id = 0U;

static void append_debug_to_tlm(P4Tlm_TcTlm_t *tc_tlm);
static void create_tlm_payload(P4Tlm_EcTlm_t *tlm_pkt);



/* Active Object state definitions */


void TlmAO_init(void)
{
    /* Initialize the Active Object with both its Event FIFO and initial state function */
    GLADOS_AO_Init(&AO_Tlm, TLMAO_PRIORITY, tlm_fifo, TLMAO_FIFO_SIZE, &TlmAO_state);

    return;
}


void TlmAO_state(GLADOS_AO_t *this_ao, GLADOS_Event_t *event)
{
    DEV_ASSERT(this_ao);
    DEV_ASSERT(event);

    switch(event->name)
    {
        /* GLADOS_ENTRY/GLADOS_EXIT optional event for each state */
        case GLADOS_ENTRY:
            /* Arbitrary number as this value is set via command */
            GLADOS_Timer_Init(&Tmr_TlmAuto, 1000000UL, EVT_TMR_AUTO_TLM);
            GLADOS_Timer_Subscribe_AO(&Tmr_TlmAuto, this_ao);

            /* Note: A full 2 TLM transfer should complete within 50ms for 115200 */
            GLADOS_Timer_Init(&Tmr_TlmTimeout, PT->tlm_xfer_timeout_us, EVT_TMR_TLM_TIMEOUT);
            GLADOS_Timer_Subscribe_AO(&Tmr_TlmTimeout, this_ao);

            GLADOS_STATE_TRAN_TO_CHILD(&TlmAO_state_idle);
            break;

        case GLADOS_EXIT:
            break;

        default:
            /* Top level state, skip undefined Events */
            break;
    }

    return;
}


void TlmAO_state_idle(GLADOS_AO_t *this_ao, GLADOS_Event_t *event)
{
    DEV_ASSERT(this_ao);
    DEV_ASSERT(event);

    GLADOS_Event_t evt;
    DataAO_RecordData_t tlm_ready_data;
    TlmAO_Rqst_Data_t *tlm_rqst;
    Cmd_Err_Code_t error;

    switch(event->name)
    {
        /* GLADOS_ENTRY/GLADOS_EXIT optional event for each state */
        case GLADOS_ENTRY:
            break;

        case GLADOS_EXIT:
            break;

        case EVT_DAQ_DATA_READY:
            /* Create the PCU TLM payload into the beginning of TLM Daq buffer.
             * Note: The RFT TLM payload should have already been stored into
             * the TLM Daq buffer, indexed to be just after the PCU TLM. */
            create_tlm_payload((P4Tlm_EcTlm_t *)&tlm_daq_buf[0]);

            /* Append PCU debug data to the RFT TLM packet */
            append_debug_to_tlm((P4Tlm_TcTlm_t *)&tlm_daq_buf[TLMAO_MAX_TLM_DATA]);

            tlm_ready_data.buf = &tlm_daq_buf[0];
            tlm_ready_data.length = TLMAO_MAX_REC_TLM_SIZE;  /* Note: currently unused since hardcoded on DataAO side */

            GLADOS_Event_New_Data(&evt, EVT_TLM_PKT_READY, (uint8_t *)&tlm_ready_data, sizeof(DataAO_RecordData_t));
            GLADOS_AO_PUSH_EVT(&AO_Data, &evt);
            break;

        /* Data: uart_port, src_id, seq_id, msg_id */
        case EVT_TMR_AUTO_TLM:
            /* Copy DAQ Buffer to a CMD Buffer to protect the TLM data during transmission */
            memcpy(&tlm_cmd_buf[0], &tlm_daq_buf[0], sizeof(tlm_cmd_buf));

            /* Queue the transmit of the auto TLM packet. First one is sent as standard data
             * since the second packet (below) is the one that should trigger TxAO to send a
             * EVT_TX_TLM_DONE back to TlmAO to indicate a full transfer as completed. */
            TxAO_SendMsg_Data(this_ao, auto_tlm_uart_port, auto_tlm_dest_id, tlm_cnt_seq_id,
                    RX_AUTO_TLM_PCU, (uint32_t)&tlm_cmd_buf[0], TLMAO_MAX_TLM_DATA);

            ++tlm_cnt_seq_id;

            /* Queue the RFT TLM packet separately */
            TxAO_SendMsg_Tlm(this_ao, auto_tlm_uart_port, auto_tlm_dest_id, tlm_cnt_seq_id,
                    RX_AUTO_TLM_RFT, (uint32_t)&tlm_cmd_buf[TLMAO_MAX_TLM_DATA], TLMAO_MAX_TLM_DATA);

            /* For auto TLM, start at 0 and increment by one on each auto TLM transfer, let
             * the uint8_t handle the rollover back to 0 */
            ++tlm_cnt_seq_id;

            GLADOS_STATE_TRAN_TO_CHILD(&TlmAO_state_idle_xfer_tlm);
            break;

        case EVT_TMR_AUTO_STATUS:
            TxAO_SendMsg_Ack(this_ao, auto_tlm_uart_port, auto_tlm_dest_id, tlm_cnt_seq_id, RX_AUTO_STATUS_PCU, 0U);

            /* For auto TLM, start at 0 and increment by one on each auto TLM transfer, let
             * the uint8_t handle the rollover back to 0 */
            ++tlm_cnt_seq_id;

            /* No need to transition to the IDLE_XFER_TLM state since status messages do not
             * require a buffer to remain coherent until transmission has completed. Status
             * can be transmitted whenever and wherever and TxAO can handle it. */
            break;

        case EVT_CMD_TLM_REQUEST:
            if (!event->has_data || (event->data_size != sizeof(TlmAO_Rqst_Data_t)))
            {
                error = create_error_uint(Errors.TlmInvalidRequestData, 0U);
                CmdAO_push_nack_w_send_err(this_ao, error);
            }
            else
            {
                tlm_rqst = (TlmAO_Rqst_Data_t *)&event->data[0];

                /* Copy DAQ Buffer to a CMD Buffer to protect the TLM data during transmission.
                 * Note: Simply copying over the TLM buffer does not increment the command
                 * acpt counter for the current command. It's prob just simpler to leave this
                 * as is rather than updating the acpt count or error data since this could
                 * very easily lead to data mismatches between what is returned at time x and
                 * what is recorded at time x. Therefore, we expect GET_TLM to indicate an
                 * increased cmd_acpt_cnt upon the next status/TLM packet rather than this one. */
                memcpy(&tlm_cmd_buf[0], &tlm_daq_buf[0], TLMAO_MAX_TLM_DATA);

                TxAO_SendMsg_Tlm(this_ao, tlm_rqst->port, tlm_rqst->src_id, tlm_rqst->seq_id,
                                     tlm_rqst->msg_id, (uint32_t)&tlm_cmd_buf[0], TLMAO_MAX_TLM_DATA);

                /* Signal command completion but also to not increment the command accept
                 * counter, which avoids use cases where high rate polling for TLM spams
                 * the accept counter. */
                GLADOS_AO_PUSH(&AO_Cmd, EVT_CMD_ACK_NO_INC);

                GLADOS_STATE_TRAN_TO_CHILD(&TlmAO_state_idle_xfer_tlm);
            }
            break;

        case EVT_CMD_TLM_RFT_REQUEST:
            if (!event->has_data || (event->data_size != sizeof(TlmAO_Rqst_Data_t)))
            {
                error = create_error_uint(Errors.TlmInvalidRequestData, 0U);
                CmdAO_push_nack_w_send_err(this_ao, error);
            }
            else
            {
                tlm_rqst = (TlmAO_Rqst_Data_t *)&event->data[0];

                /* Copy DAQ Buffer to a CMD Buffer to protect the TLM data during transmission.
                 * Note: The tlm_daq_buf expects to remain coherent at the time the data is
                 * copied to the command buffer due to the restriction that an RFT pass thru
                 * command cannot happen simultaneously as an RFT TLM request */
                memcpy(&tlm_cmd_buf[TLMAO_MAX_TLM_DATA], &tlm_daq_buf[TLMAO_MAX_TLM_DATA], TLMAO_MAX_TLM_DATA);

                TxAO_SendMsg_Tlm(this_ao, tlm_rqst->port, tlm_rqst->src_id, tlm_rqst->seq_id,
                                     tlm_rqst->msg_id, (uint32_t)&tlm_cmd_buf[TLMAO_MAX_TLM_DATA], TLMAO_MAX_TLM_DATA);

                /* Signal command completion but also to not increment the command accept
                 * counter, which avoids use cases where high rate polling for TLM spams
                 * the accept counter. */
                GLADOS_AO_PUSH(&AO_Cmd, EVT_CMD_ACK_NO_INC);

                GLADOS_STATE_TRAN_TO_CHILD(&TlmAO_state_idle_xfer_tlm);
            }
            break;

        case EVT_TX_TLM_DONE:
            /* Any extras from TxAO ignore as we have already timed out */
            break;

        default:
            GLADOS_STATE_CHECK_PARENT(&TlmAO_state);
            break;
    }

    return;
}


void TlmAO_state_idle_xfer_tlm(GLADOS_AO_t *this_ao, GLADOS_Event_t *event)
{
    DEV_ASSERT(this_ao);
    DEV_ASSERT(event);

    static int8_t tlm_xfer_cnt;
    static bool sent_second_tlm;

    TlmAO_Rqst_Data_t *tlm_rqst;
    Cmd_Err_Code_t error;

    switch(event->name)
    {
        /* GLADOS_ENTRY/GLADOS_EXIT optional event for each state */
        case GLADOS_ENTRY:
            tlm_xfer_cnt = 1;
            sent_second_tlm = false;
            GLADOS_Timer_EnableOnce(&Tmr_TlmTimeout);
            break;

        case GLADOS_EXIT:
            GLADOS_Timer_Disable(&Tmr_TlmTimeout);
            break;

        case EVT_TMR_AUTO_TLM:
        case EVT_TMR_AUTO_STATUS:
            /* Ignore since TLM transfer is already underway */
            break;

        case EVT_CMD_TLM_REQUEST:
            if (!event->has_data || (event->data_size != sizeof(TlmAO_Rqst_Data_t)))
            {
                error = create_error_uint(Errors.TlmInvalidRequestData, 0U);
                CmdAO_push_nack_w_send_err(this_ao, error);
            }
            else
            {
                tlm_rqst = (TlmAO_Rqst_Data_t *)&event->data[0];

                /* Copy DAQ Buffer to a CMD Buffer to protect the TLM data during transmission.
                 * Note: Simply copying over the TLM buffer does not increment the command
                 * acpt counter for the current command. It's prob just simpler to leave this
                 * as is rather than updating the acpt count or error data since this could
                 * very easily lead to data mismatches between what is returned at time x and
                 * what is recorded at time x. Therefore, we expect GET_TLM to indicate an
                 * increased cmd_acpt_cnt upon the next status/TLM packet rather than this one. */
                memcpy(&tlm_cmd2_buf[0], &tlm_daq_buf[0], TLMAO_MAX_TLM_DATA);

                TxAO_SendMsg_Tlm(this_ao, tlm_rqst->port, tlm_rqst->src_id, tlm_rqst->seq_id,
                                     tlm_rqst->msg_id, (uint32_t)&tlm_cmd2_buf[0], TLMAO_MAX_TLM_DATA);

                /* Delay sending the ACK to CMD AO since this is a special case that only
                 * expects to be used during testing, which is with Auto TLM enabled and
                 * asking for TLM at the same time. In this instance, it is okay to delay
                 * returning the ACK until after the message transmission has completed.
                 * This, in a way, throttles the number of TLM packets that can be requested
                 * since CmdAO will not accept the next command (even if it is a GET_TLM) until
                 * the TxAO has indicated transmission has completed.
                 *
                 * Delaying the ACK to CMD AO occurs through the use of a flag
                 * that will send the ACK after the TLM has completed. */
                sent_second_tlm = true;
                ++tlm_xfer_cnt;

                /* Stay in this state */
            }
            break;

        case EVT_CMD_TLM_RFT_REQUEST:
            if (!event->has_data || (event->data_size != sizeof(TlmAO_Rqst_Data_t)))
            {
                error = create_error_uint(Errors.TlmInvalidRequestData, 0U);
                CmdAO_push_nack_w_send_err(this_ao, error);
            }
            else
            {
                tlm_rqst = (TlmAO_Rqst_Data_t *)&event->data[0];

                /* Copy DAQ Buffer to a CMD Buffer to protect the TLM data during transmission.
                 * Note: The tlm_daq_buf expects to remain coherent at the time the data is
                 * copied to the command buffer due to the restriction that an RFT pass thru
                 * command cannot happen simultaneously as an RFT TLM request */
                memcpy(&tlm_cmd2_buf[TLMAO_MAX_TLM_DATA], &tlm_daq_buf[TLMAO_MAX_TLM_DATA], TLMAO_MAX_TLM_DATA);

                TxAO_SendMsg_Data(this_ao, tlm_rqst->port, tlm_rqst->src_id, tlm_rqst->seq_id,
                                     tlm_rqst->msg_id, (uint32_t)&tlm_cmd2_buf[TLMAO_MAX_TLM_DATA], TLMAO_MAX_TLM_DATA);

                /* Delay sending the ACK to CMD AO since this is a special case that only
                 * expects to be used during testing, which is with Auto TLM enabled and
                 * asking for TLM at the same time. In this instance, it is okay to delay
                 * returning the ACK until after the message transmission has completed.
                 * This, in a way, throttles the number of TLM packets that can be requested
                 * since CmdAO will not accept the next command (even if it is a GET_TLM) until
                 * the TxAO has indicated transmission has completed.
                 *
                 * Delaying the ACK to CMD AO occurs through the use of a binary semaphore
                 * that will send the ACK after the TLM has completed. */
                sent_second_tlm = true;
                ++tlm_xfer_cnt;

                /* Stay in this state */
            }
            break;


        case EVT_TMR_TLM_TIMEOUT:
            if (sent_second_tlm)
            {
                /* If it's a secondary TLM, then a NACK needs to be transmitted */
                error = create_error_uint(Errors.TlmTmrTimeout, Tmr_TlmTimeout.duration_us);
                CmdAO_push_nack_w_send_err(this_ao, error);
            }
            else
            {
                /* If for some reason the EVT_TX_TLM_DONE did not return from TxAO, then
                 * log the error internally */
                report_error_uint(this_ao, Errors.TlmTmrTimeout, DO_NOT_SEND_TO_SPACECRAFT, Tmr_TlmTimeout.duration_us);
            }
            GLADOS_STATE_TRAN_TO_PARENT(&TlmAO_state_idle);
            break;

        case EVT_TX_TLM_DONE:
            --tlm_xfer_cnt;

            /* No other TLM to be transmitted! */
            if (tlm_xfer_cnt <= 0)
            {
                /* If a secondary TLM packet was transmitted, then forward the ACK. */
                if (sent_second_tlm)
                {
                    GLADOS_AO_PUSH(&AO_Cmd, EVT_CMD_ACK_NO_INC);
                }
                GLADOS_STATE_TRAN_TO_PARENT(&TlmAO_state_idle);
            }
            break;

        default:
            GLADOS_STATE_CHECK_PARENT(&TlmAO_state_idle);
            break;
    }

    return;
}


/* Public Helper Functions */


Cmd_Err_Code_t TlmAO_AutoTlmPeriod(uint32_t period_ms)
{
    bool timer_is_enabled;
    Cmd_Err_Code_t error_code = create_error_uint(Errors.CmdInvalidAutoTlmRange, period_ms);

    if ((period_ms >= (PT->tlm_auto_tlm_min_period_us/1000UL)) &&
        (period_ms <= (PT->tlm_auto_tlm_max_period_us/1000UL)))
    {
        error_code = create_error_uint(Errors.StatusSuccess, 0UL);

        timer_is_enabled = Tmr_TlmAuto.enabled;
        GLADOS_Timer_Disable(&Tmr_TlmAuto);
        GLADOS_Timer_Set(&Tmr_TlmAuto, period_ms * 1000UL);
        if (timer_is_enabled)
        {
            GLADOS_Timer_EnableContinuous(&Tmr_TlmAuto);
        }
    }

    return error_code;
}


void TlmAO_AutoTlmEnable(uint8_t dest_id, uint8_t uart_port, bool enable)
{
    /* Destination ID is already checked upon reception for validity. But
     * if dest_id is invalid or not configured (i.e. > 0xF), this should
     * transmit 0 as the transmit mask and will be an invalid Clank packet. */
    DEV_ASSERT(dest_id < CLANK_MAX_SRC_ADDR);

    auto_tlm_dest_id   = dest_id;
    auto_tlm_uart_port = (uart_port & (uint8_t)PORT_GSE);

    if (enable)
    {
        /* Same timer is used for auto TLM and auto status
         * since they both cannot be running simultaneously */
        Tmr_TlmAuto.tmr_evt.name = EVT_TMR_AUTO_TLM;
        GLADOS_Timer_EnableContinuous(&Tmr_TlmAuto);
    }
    else
    {
        GLADOS_Timer_Disable(&Tmr_TlmAuto);
    }

    return;
}


Cmd_Err_Code_t TlmAO_AutoStatusPeriod(uint32_t period_ms)
{
    bool timer_is_enabled;
    Cmd_Err_Code_t error_code = create_error_uint(Errors.CmdInvalidAutoTlmRange, period_ms);

    if ((period_ms >= (PT->tlm_auto_stat_min_period_us/1000UL)) &&
        (period_ms <= (PT->tlm_auto_stat_max_period_us/1000UL)))
    {
        error_code = create_error_uint(Errors.StatusSuccess, 0UL);

        timer_is_enabled = Tmr_TlmAuto.enabled;
        GLADOS_Timer_Disable(&Tmr_TlmAuto);
        GLADOS_Timer_Set(&Tmr_TlmAuto, period_ms * 1000UL);
        if (timer_is_enabled)
        {
            GLADOS_Timer_EnableContinuous(&Tmr_TlmAuto);
        }
    }

    return error_code;
}


void TlmAO_AutoStatusEnable(uint8_t dest_id, uint8_t uart_port, bool enable)
{
    /* Destination ID is already checked upon reception for validity. But
     * if dest_id is invalid or not configured (i.e. > 0xF), this should
     * transmit 0 as the transmit mask and will be an invalid Clank packet. */
    DEV_ASSERT(dest_id < CLANK_MAX_SRC_ADDR);

    auto_tlm_dest_id   = dest_id;
    auto_tlm_uart_port = (uart_port & (uint8_t)PORT_GSE);

    if (enable)
    {
        /* Same timer is used for auto TLM and auto status
         * since they both cannot be running simultaneously */
        Tmr_TlmAuto.tmr_evt.name = EVT_TMR_AUTO_STATUS;
        GLADOS_Timer_EnableContinuous(&Tmr_TlmAuto);
    }
    else
    {
        GLADOS_Timer_Disable(&Tmr_TlmAuto);
    }

    return;
}


void TlmAO_RequestTlm(GLADOS_AO_t *ao_src, uint8_t port, uint8_t seq_id, uint8_t src_id, uint16_t msg_id)
{
    GLADOS_Event_t evt;

    TlmAO_Rqst_Data_t tlm_request =
    {
        .port   = port,
        .seq_id = seq_id,
        .src_id = src_id,
        .msg_id = msg_id
    };

    GLADOS_Event_New_Data(&evt, EVT_CMD_TLM_REQUEST, (uint8_t *)&tlm_request, sizeof(TlmAO_Rqst_Data_t));
    GLADOS_AO_PushEvt(ao_src, &AO_Tlm, &evt);

    return;
}


void TlmAO_RequestTlm_Rft(GLADOS_AO_t *ao_src, uint8_t port, uint8_t seq_id, uint8_t src_id, uint16_t msg_id)
{
    GLADOS_Event_t evt;

    TlmAO_Rqst_Data_t tlm_request =
    {
        .port   = port,
        .seq_id = seq_id,
        .src_id = src_id,
        .msg_id = msg_id
    };

    GLADOS_Event_New_Data(&evt, EVT_CMD_TLM_RFT_REQUEST, (uint8_t *)&tlm_request, sizeof(TlmAO_Rqst_Data_t));
    GLADOS_AO_PushEvt(ao_src, &AO_Tlm, &evt);

    return;
}


void TlmAO_DaqTlm_StoreRftTlm(const uint8_t *rft_tlm_msg_buf)
{
    DEV_ASSERT(rft_tlm_msg_buf);

    /* Copy the payload data starting after the Clank header into the
     * TLM Daq buffer just after the PCU TLM */
    memcpy(&tlm_daq_buf[TLMAO_MAX_TLM_DATA], &rft_tlm_msg_buf[CLANK_HDR_SIZE], TLMAO_MAX_TLM_DATA);

    /* Append PCU debug data to the RFT TLM packet.
     * Note: We do this here because the entire RFT TLM is copied over, which allows for the size
     * of the debug area of the RFT TLM to vary in size transparently. But doing so has the side
     * effect of clearing any debug data. Appending this ensures that the debug area is always
     * populated with the appropriate data. Not doing this leads to a race condition wherein, for
     * a 50sec or so period of time, the Auto TLM timing and DAQ Frame timing would alias and
     * return zeroed debug data. */
    append_debug_to_tlm((P4Tlm_TcTlm_t *)&tlm_daq_buf[TLMAO_MAX_TLM_DATA]);

    return;
}


static void append_debug_to_tlm(P4Tlm_TcTlm_t *tc_tlm)
{
    DEV_ASSERT(tc_tlm);

    /* The unix timestamp for the upper u32 that represents Jan 1, 2019 */
    enum Constants { UNIX_TIME_US_HI_2019JAN1 = 0x57E5B };

    /* If the Prop Ctrl indicates that it is set (set later than 2019), then
     * always replace the RFT timestamp with the Prop Ctrl timestmap for
     * consistent TLM and recording purposes. */
    if (MaxData.unix_time_us_hi > UNIX_TIME_US_HI_2019JAN1)
    {
        tc_tlm->tc_status.tc_unix_time_us_hi = REV32(MaxData.unix_time_us_hi);
        tc_tlm->tc_status.tc_unix_time_us_lo = REV32(MaxData.unix_time_us_lo);
    }

    return;
}


static void create_tlm_payload(P4Tlm_EcTlm_t *tlm_pkt)
{
    DEV_ASSERT(tlm_pkt);
    DEV_ASSERT(sizeof(P4Tlm_EcTlm_t) <= TLMAO_MAX_TLM_DATA);

    float tmp;

    tlm_pkt->ec_status.ec_ack_status        = 0U;
    tlm_pkt->ec_status.sc_ec_err1_code      = REV16(telemetry_record.spacecraft_err_id[0]);
    tlm_pkt->ec_status.sc_ec_err2_code      = REV16(telemetry_record.spacecraft_err_id[1]);
    tlm_pkt->ec_status.sc_ec_err3_code      = REV16(telemetry_record.spacecraft_err_id[2]);
    tlm_pkt->ec_status.ec_state             = (uint8_t)MaxData.state;
    tlm_pkt->ec_status.ec_busy_flag         = (uint8_t)(MaxData.max_busy_flag || TcData.tc_busy);
    tlm_pkt->ec_status.ec_needs_reset       = (uint8_t)MaxData.needs_reset_flag;
    tlm_pkt->ec_status.sc_tprop             = REV32(F32_TO_BYTES(MaxData.tprop));
    tmp = max_coerce(MaxData.pprop);
    tlm_pkt->ec_status.sc_pprop             = REV32(F32_TO_BYTES(tmp));
    tmp = max_coerce(MaxData.plowp);
    tlm_pkt->ec_status.sc_plowp             = REV32(F32_TO_BYTES(tmp));
    tmp = max_coerce(MaxData.mfs_mdot);
    tlm_pkt->ec_status.sc_mfs_mdot          = REV32(F32_TO_BYTES(tmp));
    tmp = max_coerce(TcData.ehvd);
    tlm_pkt->ec_status.sc_ehvd              = REV32(F32_TO_BYTES(tmp));
    tmp = max_coerce(TcData.ivb);
    tlm_pkt->ec_status.sc_ihvd              = REV32(F32_TO_BYTES(tmp));
    tlm_pkt->ec_status.sc_tliner            = REV32(F32_TO_BYTES(TcData.tpl));
    tlm_pkt->ec_status.sc_tfet              = REV32(F32_TO_BYTES(TcData.tfet));
    tlm_pkt->ec_status.ec_thrust_rf_vset_fb = REV32(F32_TO_BYTES(MaxData.thrust_rf_vset_fb));
    tlm_pkt->ec_status.ec_thrust_mdot_fb    = REV32(F32_TO_BYTES(MaxData.thrust_mdot_fb));
    tlm_pkt->ec_status.ec_thrust_dur_sec_fb = REV32(F32_TO_BYTES(MaxData.thrust_dur_sec_fb));
    tlm_pkt->ec_status.sc_tcoil             = REV32(F32_TO_BYTES(TcData.tcoil));
    tlm_pkt->ec_status.sc_sn                = REV16(MaxData.top_serial_number);
    tlm_pkt->ec_status.ec_app_select        = (uint8_t)MaxData.app_select;
    tlm_pkt->ec_status.ec_acpt_cnt          = REV16(MaxData.cmd_acpt_cnt);
    tlm_pkt->ec_status.ec_rjct_cnt          = REV16(MaxData.cmd_rjct_cnt);
    tlm_pkt->ec_status.ec_unix_time_us_hi   = REV32(MaxData.unix_time_us_hi);
    tlm_pkt->ec_status.ec_unix_time_us_lo   = REV32(MaxData.unix_time_us_lo);

    tlm_pkt->ec_e3v3         = REV32(F32_TO_BYTES(MaxData.e3v3));
    tlm_pkt->ec_ipfcv        = REV32(F32_TO_BYTES(MaxData.ipfcv));
    tlm_pkt->ec_ibus         = REV32(F32_TO_BYTES(MaxData.ibus));
    tlm_pkt->ec_ebus         = REV32(F32_TO_BYTES(MaxData.ebus));
    tlm_pkt->ec_ihpiso       = REV32(F32_TO_BYTES(MaxData.ihpiso));
    tlm_pkt->ec_pprop        = REV32(F32_TO_BYTES(MaxData.pprop));
    tlm_pkt->ec_irft         = REV32(F32_TO_BYTES(MaxData.irft));
    tlm_pkt->ec_e12v         = REV32(F32_TO_BYTES(MaxData.e12v));
    tlm_pkt->ec_tadc0        = REV32(F32_TO_BYTES(MaxData.tadc0));

    tlm_pkt->ec_tprop        = REV32(F32_TO_BYTES(MaxData.tprop));
    tlm_pkt->ec_iheater      = REV32(F32_TO_BYTES(MaxData.iheater));
    tlm_pkt->ec_plowp        = REV32(F32_TO_BYTES(MaxData.plowp));
    tlm_pkt->ec_e5v          = REV32(F32_TO_BYTES(MaxData.e5v));
    tlm_pkt->ec_e2v5         = REV32(F32_TO_BYTES(MaxData.e2v5));
    tlm_pkt->ec_mfs_mdot     = REV32(F32_TO_BYTES(MaxData.mfs_mdot));
    tlm_pkt->ec_tmfs         = REV32(F32_TO_BYTES(MaxData.tmfs));
    tlm_pkt->ec_tadc1        = REV32(F32_TO_BYTES(MaxData.tadc1));

    tlm_pkt->ec_rft_fault    = (uint8_t)MaxData.rft_fault;
    tlm_pkt->ec_heater_fault = (uint8_t)MaxData.heater_fault;
    tlm_pkt->ec_hpiso_fault  = (uint8_t)MaxData.hpiso_fault;
    tlm_pkt->ec_pfcv_fault   = (uint8_t)MaxData.pfcv_fault;

    tlm_pkt->ec_bl_select    = (uint8_t)MaxData.bl_select;
    tlm_pkt->ec_pt_select    = (uint8_t)MaxData.pt_select;

    tlm_pkt->ec_bl_crc       = REV32(MaxData.bl_crc);
    tlm_pkt->ec_app_crc      = REV32(MaxData.app_crc);
    tlm_pkt->ec_pt_crc       = REV32(MaxData.pt_crc);

    tlm_pkt->tlm_err_rec.powerup_err_code = REV16(telemetry_record.latest_power_up_err);
    tlm_pkt->tlm_err_rec.err1.err_code    = REV16(telemetry_record.internal_errors[0].err_id);
    tlm_pkt->tlm_err_rec.err1.err_data    = REV32(telemetry_record.internal_errors[0].aux_data);
    tlm_pkt->tlm_err_rec.err2.err_code    = REV16(telemetry_record.internal_errors[1].err_id);
    tlm_pkt->tlm_err_rec.err2.err_data    = REV32(telemetry_record.internal_errors[1].aux_data);
    tlm_pkt->tlm_err_rec.err3.err_code    = REV16(telemetry_record.internal_errors[2].err_id);
    tlm_pkt->tlm_err_rec.err3.err_data    = REV32(telemetry_record.internal_errors[2].aux_data);
    tlm_pkt->tlm_err_rec.total_err_cnt    = REV16(telemetry_record.total_err_count);

    tlm_pkt->ec_heat_ctrl_en_fb    = (uint8_t)MaxData.heat_ctrl_en_fb;
    tlm_pkt->ec_heater_en_fb       = (uint8_t)MaxData.heater_en_fb;
    tlm_pkt->ec_hpiso_en_fb        = (uint8_t)MaxData.hpiso_en_fb;
    tlm_pkt->ec_flow_ctrl_en_fb    = (uint8_t)MaxData.flow_ctrl_en_fb;

    tlm_pkt->ec_heat_ctrl_temp_fb  = REV32(F32_TO_BYTES(MaxData.heat_ctrl_temp_fb));
    tlm_pkt->ec_heater_setpt_ma_fb = REV32(F32_TO_BYTES(MaxData.heater_setpt_ma_fb));
    tlm_pkt->ec_hpiso_setpt_ma_fb  = REV32(F32_TO_BYTES(MaxData.hpiso_setpt_ma_fb));
    tlm_pkt->ec_flow_ctrl_mdot_fb  = REV32(F32_TO_BYTES(MaxData.flow_ctrl_mdot_fb));
    tlm_pkt->ec_desired_mdot_v_fb  = REV32(F32_TO_BYTES(MaxData.desired_mdot_v_fb));

    tlm_pkt->ec_pfcv_ma_fb         = REV32(F32_TO_BYTES(MaxData.pfcv_ma_fb));

    tlm_pkt->ec_rcm_value          = REV32(MaxData.rcm_value);
    tlm_pkt->ec_wdt_rst_trig       = (uint8_t)MaxData.wdt_rst_trig;
    tlm_pkt->ec_wdt_toggle_sel     = (uint8_t)MaxData.wdt_tog_sel;

    tlm_pkt->ec_data_mode          = (uint8_t)MaxData.data_mode;
    tlm_pkt->ec_rec_nand_en_fb     = (uint8_t)MaxData.rec_nand_en;

    tlm_pkt->ec_rec_nand_tlm_cnt   = REV32(MaxData.rec_nand_tlm_cnt[0]);
    tlm_pkt->ec_pprop_raw          = REV16(Adc0.raw_dma_ch_arr[IN5]);
    tlm_pkt->ec_tprop_raw          = REV16(Adc1.raw_dma_ch_arr[IN0]);
    tlm_pkt->ec_plowp_raw          = REV16(Adc1.raw_dma_ch_arr[IN3]);
    tlm_pkt->ec_mfs_mdot_raw       = REV16(Adc1.raw_dma_ch_arr[IN6]);
    tlm_pkt->ec_tmfs_raw           = REV16(Adc1.raw_dma_ch_arr[IN7]);
    tlm_pkt->ec_ecc_sram_1bit_cnt  = REV32(MaxData.ecc_sram_1bit_cnt);
    tlm_pkt->ec_tc_comm_fail_cnt   = REV16(MaxData.tc_comm_fail_cum_cnt);
    tlm_pkt->ec_fsw_cpu_util       = (uint8_t)MaxData.fsw_cpu_util;

    tlm_pkt->ec_heater_connected   = (uint8_t)MaxData.heater_connected;
    tlm_pkt->ec_mfs_connected      = (uint8_t)MaxData.mfs_connected;
    tlm_pkt->ec_tc_connected       = (uint8_t)MaxData.tc_connected;

    return;
}
