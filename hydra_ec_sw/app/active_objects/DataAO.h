/* Copyright (c) 2017-2020 by Phase Four, Inc. All Rights Reserved. Confidential and Proprietary.
 *
 * DataAO.h
 */

#ifndef ACTIVE_OBJECTS_DATAAO_H_
#define ACTIVE_OBJECTS_DATAAO_H_

/*
 * Global invariants:
 *  - The CRC32 algorithm can validate a 128KB buffer within 10ms
 */

#include "mcu_types.h"
#include "glados.h"

#define DATAAO_MEMTYPE_FRAM    0xAU
#define DATAAO_MEMTYPE_NAND    0xBU

#define DATAAO_PRIORITY        8UL
#define DATAAO_FIFO_SIZE       6UL

extern GLADOS_AO_t AO_Data;
extern GLADOS_Timer_t Tmr_DataAux;
extern GLADOS_Timer_t Tmr_DataTimeout;
extern bool spi0_callback_send_dma_done;
extern bool spi1_callback_send_dma_done;


/*Currently the events the watchdog timer has is pet the watchdog and stop the watchdog.*/
enum DataAO_event_names
{
    EVT_DATA_OP_DONE = (DATAAO_PRIORITY << 16U),
    EVT_DATA_CLEAR_NAND,
    EVT_DATA_RECORD_NAND,
    EVT_DATA_DMA_NAND_DONE,
    EVT_DATA_DMA_FRAM_DONE,
    EVT_TMR_DATA_AUX_DONE,
    EVT_TMR_DATA_TIMEOUT,
    EVT_DATA_DUMP_START,
    EVT_DATA_DUMP_NEXT_BANK,
    EVT_DATA_DUMP_NEXT_TLM,
    EVT_DATA_CRC_ERROR,
};

typedef struct
{
    uint8_t *buf;
    uint16_t length;
} BYTE_PACKED DataAO_RecordData_t;

typedef struct
{
    uint8_t port;
    uint8_t seq_id;
    uint8_t src_id;
    uint16_t msg_id;
    uint8_t mem_type;
    uint8_t bank;
} BYTE_PACKED DataAO_DumpTlm_RqstData_t;

typedef struct
{
    uint32_t pflash_addr;
    uint32_t length;
    uint32_t fram_offset;
} BYTE_PACKED DataAO_PreloadData_t;

typedef struct
{
    uint32_t fram_offset;
    uint32_t data_ptr;
    uint32_t length;
} BYTE_PACKED DataAO_StageData_t;


typedef struct
{
    uint32_t pflash_addr;
    uint32_t length;
    uint32_t crc;
} BYTE_PACKED DataAO_ProgData_t;


void DataAO_init(void);
void DataAO_push_interrupts(void);
void DataAO_state(GLADOS_AO_t *this_ao, GLADOS_Event_t *event);
void DataAO_state_build_bb_map(GLADOS_AO_t *this_ao, GLADOS_Event_t *event);
void DataAO_state_lounge(GLADOS_AO_t *this_ao, GLADOS_Event_t *event);
void DataAO_state_record(GLADOS_AO_t *this_ao, GLADOS_Event_t *event);
void DataAO_state_record_abort(GLADOS_AO_t *this_ao, GLADOS_Event_t *event);
void DataAO_state_record_clear_nand(GLADOS_AO_t *this_ao, GLADOS_Event_t *event);
void DataAO_state_record_write_nand(GLADOS_AO_t *this_ao, GLADOS_Event_t *event);
void DataAO_state_dump_tlm(GLADOS_AO_t *this_ao, GLADOS_Event_t *event);
void DataAO_state_dump_tlm_nand_read(GLADOS_AO_t *this_ao, GLADOS_Event_t *event);
void DataAO_state_dump_tlm_fram_read(GLADOS_AO_t *this_ao, GLADOS_Event_t *event);
void DataAO_state_update_cmd(GLADOS_AO_t *this_ao, GLADOS_Event_t *event);
void DataAO_state_update_cmd_success(GLADOS_AO_t *this_ao, GLADOS_Event_t *event);
void DataAO_state_update_cmd_fail(GLADOS_AO_t *this_ao, GLADOS_Event_t *event);
void DataAO_state_preload(GLADOS_AO_t *this_ao, GLADOS_Event_t *event);
void DataAO_state_stage(GLADOS_AO_t *this_ao, GLADOS_Event_t *event);
void DataAO_state_fram_validate(GLADOS_AO_t *this_ao, GLADOS_Event_t *event);
void DataAO_state_pflash_erase(GLADOS_AO_t *this_ao, GLADOS_Event_t *event);
void DataAO_state_pflash_program(GLADOS_AO_t *this_ao, GLADOS_Event_t *event);
void DataAO_state_pflash_validate(GLADOS_AO_t *this_ao, GLADOS_Event_t *event);

void DataAO_SetRecord_Fram(bool enable);
void DataAO_SetRecord_Nand(bool enable);
void DataAO_SetRecord_Bank(uint8_t bank_num);
void DataAO_DumpTlmRequest(GLADOS_AO_t *ao_src,
                           uint8_t port,
                           uint8_t seq_id,
                           uint8_t src_id,
                           uint16_t msg_id,
                           uint8_t mem_type,
                           uint8_t bank);

void fram_metadata_write(uint8_t bank, uint8_t meta1_8bit, uint32_t meta2_24bit);
void fram_metadata_read(uint8_t bank, uint8_t *meta1_8bit, uint32_t *meta2_24bit);

#endif /* ACTIVE_OBJECTS_DATAAO_H_ */
