/* Copyright (c) 2017-2020 by Phase Four, Inc. All Rights Reserved. Confidential and Proprietary.
 *
 * ErrAO.h
 */

#ifndef ACTIVE_OBJECTS_ERRAO_H_
#define ACTIVE_OBJECTS_ERRAO_H_

#include "glados.h"
#include "mcu_types.h"
#include "p4_err.h"


/* Log Record Offset*/
/*This data is located first in sram and then is saved in Flash as archival data*/
#define CRR_TIME_RECORD_BYTE_OFFSET 0X00               /*decimal = 0   The current MCU time of the reported error offset.*/
#define ERR_ID_RECORD_BYTE_OFFSET 0X08                 /*decimal = 8   The Error ID of the reported error offset. */
#define AUX_DATA_RECORD_BYTE_OFFSET 0X0A               /*decimal = 10  Auxiliary data of the reported error offset. */
#define CRR_STATE_RECORD_BYTE_OFFSET 0X0E              /*decimal = 14  State of the state machine when the error was reported.*/


#define RECORD_OFFSET 0x10      /*decimal = 16  This offset is used to jump over record entries from the Might be useful for the off by 1 in the sram overflow */


#define ERRAO_PRIORITY        2UL
#define ERRAO_FIFO_SIZE       15UL

#define CLEAR_TYPE_INDEX 0U
#define ERASE_MEM_OPTIONS 4U

#define ERROR_RECORD_SECTOR_1_ADDRESS 0x000F0000
#define ERROR_RECORD_SECTOR_2_ADDRESS 0x000F1000
#define ERROR_RECORD_SECTOR_2_LIMIT 0x000F2000


#define TOTAL_AMOUNT_OF_RECORDS_IN_ERROR_SRAM 256U
#define SRAM_ERROR_RECORD_SIZE 16U
#define TOTAL_AMOUNT_OF_RECORDS_ERR_FREQ_COUNT 128U
#define TOTAL_TLM_ERR_COUNT 3U
#define TOTAL_TLM_SPC_ERR_COUNT 3U
#define LAST_ERR_TLM_SHIFT_BUFFER_AMNT 2U
#define FIRST_END_TLM_ERR_INDEX 3U


/* Each of these must be unique values for the entire system */

#define SEND_TO_SPACECRAFT        0xFFU
#define DO_NOT_SEND_TO_SPACECRAFT 0x00U


#define ONE_MIN_IN_US 0x3938700


#define FLASH_SECTOR_ONE_75_PERCENT_BOUNDRY ERROR_RECORD_SECTOR_2_ADDRESS -0x400
#define FLASH_SECTOR_TWO_75_PERCENT_BOUNDRY ERROR_RECORD_SECTOR_2_LIMIT -0x400



enum ErrAO_event_names
{
    EVT_ERR_PROCESS_DONE = (ERRAO_PRIORITY << 16U),
    EVT_TMR_ERR_TIMEOUT,
    EVT_CLEAR_ERRORS,
    EVT_TMR_FLASH_ERASE_DONE,
    EVT_TRANSFER_TO_NVM,
    EVT_CLEAR_FLASH,
    EVT_WRITE_FLASH,
    EVT_NVM_PREPARED,
    EVT_SAVE_FLASH,
    EVT_REPORT_ERR,
    EVT_REPORT_ERR_BULK,
    EVT_RUN_FIDR,
    START_FIDR_RUN,
    EVT_ERR_MAX_TO_SAFE
};


enum overflow_status
{ NO_OVERFLOW = 0,
   OVERFLOW
    };


/*ALWAYS UPDATE ERASE_MEM_OPTIONS! IF ADDING A OPTION*/
enum erase_option
{ CLEAR_COUNT_LOG = 1,
   CLEAR_SRAM_TELM = 2,
   CLEAR_SRAM_ERROR_LOG=4,
   CLEAR_FLASH=8,
    };

typedef struct reported_error_record{
err_type *err_type_pointer;
uint32_t aux_data;
uint8_t glados_state;
uint8_t send_to_spacecraft;
} BYTE_PACKED reported_error_record ;


typedef struct telm_error_info{
    uint16_t err_id;
    uint32_t aux_data;
}BYTE_PACKED telm_error_info ;



typedef struct telm_err_struct {
uint16_t latest_power_up_err;
telm_error_info internal_errors[TOTAL_TLM_ERR_COUNT];
uint16_t most_freq_err_id;
uint16_t most_freq_err_amt;
uint16_t total_err_count;
uint16_t spacecraft_err_id[TOTAL_TLM_SPC_ERR_COUNT];
} BYTE_PACKED telm_err_struct;


typedef struct log_rec_error_struct{
uint64_t mcu_time;
uint16_t error_id;
uint32_t aux_data;
uint8_t glados_state;
uint8_t reserved;
} BYTE_PACKED log_rec_error_struct;

typedef struct log_rec_header_struct{
uint32_t curr_sram_address;
uint32_t curr_flash_address;
uint16_t sram_error_record_count;
uint16_t freq_unique_error_count;
uint16_t bootloader_error_id;
uint16_t reserved;
} BYTE_PACKED log_rec_header_struct;


/* This Extern Variables are used for other systems if they are needed Currently they are not needed as of now.*/
extern log_rec_error_struct log_record_sram [TOTAL_AMOUNT_OF_RECORDS_IN_ERROR_SRAM];
extern telm_err_struct telemetry_record;


extern GLADOS_AO_t AO_Err;
extern GLADOS_Timer_t Tmr_errAOTimeout;
extern GLADOS_Timer_t Tmr_errAOFidr;

void ErrAO_init(void);
void ErrAO_state(GLADOS_AO_t *this_ao, GLADOS_Event_t *event);


void ErrAO_CLR_Flash(GLADOS_AO_t *this_ao, GLADOS_Event_t *event);
void ErrAO_TRNS_NVM(GLADOS_AO_t *this_ao, GLADOS_Event_t *event);
void write_sram_record(reported_error_record current_error);
void write_telem_record(reported_error_record current_error);

void ErrAO_ERASE_SECTOR(GLADOS_AO_t *this_ao, GLADOS_Event_t *event);
void ErrAO_ERASE_SECTOR_PROACTIVE(GLADOS_AO_t *this_ao, GLADOS_Event_t *event);
uint8_t sram_record_overflow_check(void);
void insert_telm_packet(reported_error_record current_error);
void insert_telm_packet_spacecraft_visable(uint16_t current_error_id);
void log_error(reported_error_record current_error);
void clear_error_ao_sram(void);


void report_error_float(GLADOS_AO_t *ao_src, const err_type *err_sent, uint8_t send_to_spacecraft, float aux_data);
void report_error_uint(GLADOS_AO_t *ao_src, const err_type *err_sent, uint8_t send_to_spacecraft, uint32_t aux_data);
void _report_fidr_error(err_type *err_sent, uint8_t send_to_spacecraft, float aux_data);
err_type* find_bootloader_errors(uint16_t err_id);
bool error_filtration_check(err_type * reported_error);
void clear_error_log(void);
uint32_t search_for_current_flash_address(void);
status_t add_errors_in_flash(uint32_t start_address, uint32_t finish_address);
status_t attempt_write_flash_ao_twice(uint32_t flash_size, uint32_t start_address);
status_t err_log_and_nvm_xfer(err_type *err, uint32_t aux_data);
#endif /* ACTIVE_OBJECTS_CMDAO_H_ */
