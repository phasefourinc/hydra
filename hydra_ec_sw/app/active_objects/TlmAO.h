/* Copyright (c) 2017-2020 by Phase Four, Inc. All Rights Reserved. Confidential and Proprietary.
 *
 * TlmAO.h
 */

#ifndef ACTIVE_OBJECTS_TLMAO_H_
#define ACTIVE_OBJECTS_TLMAO_H_

#include "mcu_types.h"
#include "glados.h"
#include "p4_err.h"

/* This is maximum amount of TLM bytes per TLM packet. Clank supports
 * up to 255 bytes of payload data, but 254 is easier to use. */
#define TLMAO_MAX_TLM_DATA        254U

/* Even though the MAX TLM Data is 508 due to CLANK 254 transfer
 * restrictions, it makes sense to have an actual TLM buffer size
 * the size of an external memory block (512) since this permits
 * the DataAO to utilize the 4 extra bytes at the end as misc
 * bytes (e.g. for CRC) prior to writing to external memory. */
#define TLMAO_MAX_REC_TLM_SIZE    512U


#define TLMAO_PRIORITY            6UL
#define TLMAO_FIFO_SIZE           6UL

/* Each of these must be unique values for the entire system */
enum TlmAO_event_names
{
    EVT_TMR_AUTO_TLM = (TLMAO_PRIORITY << 16U),
    EVT_TMR_AUTO_STATUS,
    EVT_TMR_TLM_TIMEOUT,
    EVT_TLM_AUTO_REQUEST,
    EVT_TLM_PKT_READY
};

typedef struct TlmAO_Rqst_Data_tag
{
    uint8_t port;
    uint8_t seq_id;
    uint8_t src_id;
    uint16_t msg_id;
} BYTE_PACKED TlmAO_Rqst_Data_t;


extern GLADOS_AO_t AO_Tlm;
extern GLADOS_Timer_t Tmr_TlmAuto;
extern GLADOS_Timer_t Tmr_TlmTimeout;

void TlmAO_init(void);
void TlmAO_state(GLADOS_AO_t *this_ao, GLADOS_Event_t *event);
void TlmAO_state_idle(GLADOS_AO_t *this_ao, GLADOS_Event_t *event);
void TlmAO_state_idle_xfer_tlm(GLADOS_AO_t *this_ao, GLADOS_Event_t *event);

/* Public Helper Functions */
Cmd_Err_Code_t TlmAO_AutoTlmPeriod(uint32_t period_ms);
void TlmAO_AutoTlmEnable(uint8_t dest_id, uint8_t uart_port, bool enable);
Cmd_Err_Code_t TlmAO_AutoStatusPeriod(uint32_t period_ms);
void TlmAO_AutoStatusEnable(uint8_t dest_id, uint8_t uart_port, bool enable);
void TlmAO_RequestTlm(GLADOS_AO_t *ao_src, uint8_t port, uint8_t seq_id, uint8_t src_id, uint16_t msg_id);
void TlmAO_RequestTlm_Rft(GLADOS_AO_t *ao_src, uint8_t port, uint8_t seq_id, uint8_t src_id, uint16_t msg_id);
void TlmAO_DaqTlm_StoreRftTlm(const uint8_t *rft_tlm_msg_buf);

#endif /* ACTIVE_OBJECTS_TLMAO_H_ */
