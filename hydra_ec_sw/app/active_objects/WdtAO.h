/*Copyright (c) 2018-2019 by Phase Four, Inc. All Rights Reserved. Confidential and Proprietary.
 * 
 * WdtAO.h
 */

#ifndef ACTIVE_OBJECTS_WDTAO_H_
#define ACTIVE_OBJECTS_WDTAO_H_

#include "glados.h"
#include "mcu_types.h"

#define WDT_PRIORITY        13UL
#define WDT_FIFO_SIZE       16UL

/* Leave in so we can see a WDT strobe if loaded on a dev board */
#define EVB_DEBUG_LED_PORT      PTE
#define EVB_DEBUG_LED_BLUE_PIN  23U

extern GLADOS_AO_t AO_Wdt;
extern GLADOS_Timer_t Tmr_4HzPetWtd;
extern GLADOS_Timer_t Tmr_WtdAux;

/*Currently the events the watchdog timer has is pet the watchdog and stop the watchdog.*/
enum WtdAO_event_names
{
    EVT_4HZ_TMR_PET_WTDG = (WDT_PRIORITY << 16U),  /*This is triggered via timer at a given frequency to toggle a pin to "pet" the watchdog.*/
    EVT_STP_WTDG_TMR,
    RUN_MORSE,
};

void WdtAO_init(void);
/*This the only state of the watchdog timer.*/
void WdtAO_state_idle(GLADOS_AO_t *this_ao, GLADOS_Event_t *event);


/*This is a public function that can be used to stop the watchdog. This can be used in program or via a command.
This will stop the watchdog and thus reset the processor.*/
void WdtAO_Stop_WDT(GLADOS_AO_t *src_ao);
void WdtAO_Enable_WDT_LED(bool enable);
void WdtAO_Enable_Aux_Driver(bool enable);
#endif /* ACTIVE_OBJECTS_WDTAO_H_ */
