/* Copyright (c) 2017-2020 by Phase Four, Inc. All Rights Reserved. Confidential and Proprietary.
 *
 * RftAO.h
 */

#ifndef ACTIVE_OBJECTS_RFTAO_H_
#define ACTIVE_OBJECTS_RFTAO_H_

/* Global invariants:
 *  - RFT request commands should be spaced with at least 10ms between them or
 *    else the commands may be corrupted
 *  - The RFT Rx buffer is only guaranteed coherent in this AO. If accessing data
 *    from any other AO then it is possible that the DMA may be stepping on its
 *    toes. This restriction requires any accesses to this data to be performed
 *    upon completion of the transfer, such as grabbing the latest RftData or
 *    copying the data into a TLM buffer for PCU TLM and data recording.
 *  - Tmr_RftRqstTimeout does not utilize a following continuous enable function:
 *    GLADOS_Timer_EnableContinuous
 */

#include "mcu_types.h"
#include "glados.h"
#include "p4_cmd.h"

#define RFT_VIRTUAL_ADDR_MASK    0xF0000000UL

#define RFTAO_PRIORITY           5UL
#define RFTAO_FIFO_SIZE          6UL

/* Each of these must be unique values for the entire system */
enum RftAO_event_names
{
    EVT_RFT_INIT_XFER = (RFTAO_PRIORITY << 16U),
    EVT_RFT_SEND_MSG,
    EVT_RFT_RETRY_XFER,
    EVT_MAX_RFT_ABORT,
    EVT_TMR_RFT_COMM_TIMEOUT,
    EVT_UART2_TX_ERR,
    EVT_UART2_RX_DATA,
    EVT_UART2_RX_ERR,
    EVT_RFT_NACK,
    EVT_RFT_ACK,
    EVT_RFT_ACK_DATA,
    EVT_RFT_TLM_READY
};


typedef enum
{
    RFT_CMD_PASSTHRU = 0,
    RFT_CMD_SILENT,
    RFT_CMD_INTERNAL
} Rft_Cmd_t;


typedef struct
{
    GLADOS_AO_t *ao_src;
    uint8_t      cmd_type;      /* Determines the type of command queued for RFT transmission */
    uint8_t     *buf;           /* The start of the entire raw Clank packet if cmd_type is
                                pass thru, otherwise start of payload data buf */
    uint16_t     buf_length;    /* Length of buf */
    uint16_t     msg_id;        /* The message ID for non-pass thru cmds */
    uint8_t      rx_port;       /* The Rx port (SC/GSE) to transmit to for pass thru cmds */
} BYTE_PACKED RftAO_Cmd_Rqst_t;


typedef struct
{
    uint32_t     data_addr;     /* The start of the returned RFT data buffer */
    uint16_t     data_length;   /* The number of bytes returned in RFT data */
} BYTE_PACKED RftAO_Intern_Data_t;


extern GLADOS_AO_t AO_Rft;
extern GLADOS_Timer_t Tmr_RftRqstTimeout;

void RftAO_init(void);
void RftAO_push_interrupts(void);
void RftAO_state(GLADOS_AO_t *this_ao, GLADOS_Event_t *event);
void RftAO_state_idle(GLADOS_AO_t *this_ao, GLADOS_Event_t *event);
void RftAO_state_busy(GLADOS_AO_t *this_ao, GLADOS_Event_t *event);
void RftAO_state_busy_pass_thru(GLADOS_AO_t *this_ao, GLADOS_Event_t *event);
void RftAO_state_busy_silent(GLADOS_AO_t *this_ao, GLADOS_Event_t *event);
void RftAO_state_busy_internal(GLADOS_AO_t *this_ao, GLADOS_Event_t *event);

void RftAO_push_cmd_request(GLADOS_AO_t *ao_src, Rft_Cmd_t cmd_type, P4Cmd_Id_t msg_id,
                                uint8_t *buf, uint16_t buf_length, uint8_t rx_port);
void RftAO_adjust_request_timeout(uint32_t duration_us);

#endif /* ACTIVE_OBJECTS_RFTAO_H_ */
