/* Copyright (c) 2017-2020 by Phase Four, Inc. All Rights Reserved. Confidential and Proprietary.
 *
 * DataAO.c
 */


#include "DataAO.h"
#include "mcu_types.h"
#include "glados.h"
#include "MaxwellAO.h"
#include "ErrAO.h"
#include "CmdAO.h"
#include "TlmAO.h"
#include "TxAO.h"
#include "cy15.h"
#include "tc58.h"
#include "crc.h"
#include "p4_cmd.h"
#include "bad_block.h"
#include "clank.h"
#include "max_info_driver.h"
#include "MCUflash.h"
#include "gpio.h"
#include "default_pt.h"
#include "string.h"


/* Redefine these so it's easier to remember the correct port */
#define LPSPICOM1_FRAM           LPSPICOM1
#define LPSPICOM0_NAND           LPSPICOM0

#define BANK0                    0U
#define BANK1                    1U
#define BANK_HAS_DATA_KEY        0xDU
#define BANK_HAS_ERR_KEY         0xEU

#define FRAM_REC_MAX_PKT_SIZE    (TLMAO_MAX_REC_TLM_SIZE - 2U)  /* Max size is based on 508B TLM data and 2B CRC, last 2B are extra */
#define FRAM_BANK_MAX_SIZE       0x00040000UL               /* 256KB FRAM Bank Size */
#define FRAM_BANK_MAX_TLM_CNT    (FRAM_BANK_MAX_SIZE / FRAM_REC_MAX_PKT_SIZE)  /* This equates to the last 4B of each bank as unused */
#define FRAM_ADDR_BANK0_START    0xA0000000UL
#define FRAM_ADDR_BANK0_END      (FRAM_ADDR_BANK0_START + ((FRAM_BANK_MAX_SIZE / FRAM_REC_MAX_PKT_SIZE) * FRAM_REC_MAX_PKT_SIZE))
#define FRAM_ADDR_BANK1_START    (FRAM_ADDR_BANK0_START + FRAM_BANK_MAX_SIZE)
#define FRAM_ADDR_BANK1_END      (FRAM_ADDR_BANK1_START + ((FRAM_BANK_MAX_SIZE / FRAM_REC_MAX_PKT_SIZE) * FRAM_REC_MAX_PKT_SIZE))

#define NAND_REC_MAX_PKT_SIZE    TLMAO_MAX_REC_TLM_SIZE         /* Use full 512B since last 2B should always be written 0xFFFF for bad block determination */
#define NAND_BANK_MAX_SIZE       0x04000000UL               /* 64MB NAND Flash Bank Size */
#define NAND_BANK_MAX_TLM_CNT    (NAND_BANK_MAX_SIZE / NAND_REC_MAX_PKT_SIZE)
#define NAND_ADDR_BANK0_START    0xB0000000UL
#define NAND_ADDR_BANK0_END      (NAND_ADDR_BANK0_START + NAND_BANK_MAX_SIZE)
#define NAND_ADDR_BANK1_START    (NAND_ADDR_BANK0_START + NAND_BANK_MAX_SIZE)
#define NAND_ADDR_BANK1_END      (NAND_ADDR_BANK1_START + NAND_BANK_MAX_SIZE)

#define UPDATE_MAX_FRAM_XFER     CY15_BLOCK_SIZE
#define UPDATE_PRELD_ADDR_END    0x00100000UL    /* First 1MB in PFlash can be preloaded */
#define UPDATE_PROG_ADDR_BEGIN   GOLDEN_IMAGE_FLASH_HDR_RECORD
#define UPDATE_PROG_ADDR_END     0x00100000UL    /* This ensures Runtime PT, Error table, etc. located >0x100000 are not corrupted */
#define UPDATE_MAX_LOAD_SIZE     (APP_HDR_MAX_SIZE + (128UL*1024UL))  /* Max size of the header and app image */
#define UPDATE_MAX_STAGE_SIZE    250UL    /* Determined by max Clank payload size minus 4 bytes for addr data */
#define IS_128KB_ALIGNED(x)      ((x & ((128UL*1024UL)-1UL)) == 0UL)

typedef enum
{
    NAND_PKT,
    NAND_BLOCK
} NandAddr_t;

GLADOS_AO_t AO_Data;

/* Active Object timers */
GLADOS_Timer_t Tmr_DataAux;
GLADOS_Timer_t Tmr_DataTimeout;

bool spi0_callback_send_dma_done = false;
bool spi1_callback_send_dma_done = false;


/* Active Object private variables */
static GLADOS_Event_t dataao_fifo[TLMAO_FIFO_SIZE];

static GLADOS_Event_t data_clear_nand_evt;
static GLADOS_Event_t data_record_nand_evt;

static uint8_t  dao_next_bank = 0U;
static uint32_t dao_curr_nand_area_start;
static uint32_t dao_curr_nand_area_stop;
static uint16_t dao_nand_minor_cnt = 0U;
static uint16_t dao_nand_major_cnt = 0U;

static uint32_t dao_dump_curr_addr;
static int32_t  dao_dump_tlm_cnt;
static uint8_t *dao_dump_rx_buf;
static uint8_t  dao_dump_seq_cnt    = 0U;
static uint32_t dao_cmd_pflash_addr = 0UL;
static uint32_t dao_cmd_fram_offset = 0UL;
static uint32_t dao_cmd_length      = 0UL;
static uint32_t dao_cmd_crc         = 0UL;
static uint32_t dao_cmd_data_ptr    = 0UL;

static DataAO_DumpTlm_RqstData_t dao_dumptlm_rqst;

static uint8_t spi0_xfer_done = 0U;
static uint8_t spi1_xfer_done = 0U;

static void get_next_nand_addr(NandAddr_t addr_type);
static bool sw_update_hdr_valid(App_Header_Rec_t *img_hdr, uint32_t exp_addr, uint32_t exp_length, uint32_t exp_crc);


void LPSPI0_TxIdleCallback(UNUSED void *driverState, UNUSED spi_event_t event, void *userData)
{
    bool *send_dma_done = (bool *)userData;

    if (*send_dma_done)
    {
        ++spi0_xfer_done;
    }
    return;
}


void LPSPI1_TxIdleCallback(UNUSED void *driverState, UNUSED spi_event_t event, void *userData)
{
    bool *send_dma_done = (bool *)userData;

    if (*send_dma_done)
    {
        ++spi1_xfer_done;
    }
    return;
}


/* Active Object state definitions */


void DataAO_init(void)
{
    /* Initialize the Active Object with both its Event FIFO and initial state function */
    GLADOS_AO_Init(&AO_Data, DATAAO_PRIORITY, dataao_fifo, DATAAO_FIFO_SIZE, &DataAO_state);

    return;
}


void DataAO_push_interrupts(void)
{
    /* If the callback was called, then decrement semaphore and post event */
    if (spi0_xfer_done > 0U)
    {
        --spi0_xfer_done;
        GLADOS_AO_PushEvtName(&AO_Data, &AO_Data, EVT_DATA_DMA_NAND_DONE);
    }
    if (spi1_xfer_done > 0U)
    {
        --spi1_xfer_done;
        GLADOS_AO_PushEvtName(&AO_Data, &AO_Data, EVT_DATA_DMA_FRAM_DONE);
    }

    return;
}


void DataAO_state(GLADOS_AO_t *this_ao, GLADOS_Event_t *event)
{
    DEV_ASSERT(this_ao);
    DEV_ASSERT(event);

    status_t status;
    uint8_t metadata1;
    uint32_t metadata2;

    switch(event->name)
    {
        /* GLADOS_ENTRY/GLADOS_EXIT optional event for each state */
        case GLADOS_ENTRY:
            /* Init timers with arbitrary period value, their actual values are
             * configured upon each usage of the timer. */
            GLADOS_Timer_Init(&Tmr_DataAux, 100000UL, EVT_TMR_DATA_AUX_DONE);
            GLADOS_Timer_Subscribe_AO(&Tmr_DataAux, this_ao);

            GLADOS_Timer_Init(&Tmr_DataTimeout, 100000UL, EVT_TMR_DATA_TIMEOUT);
            GLADOS_Timer_Subscribe_AO(&Tmr_DataTimeout, this_ao);

            GLADOS_Event_New(&data_clear_nand_evt, EVT_DATA_CLEAR_NAND);
            GLADOS_Event_New(&data_record_nand_evt, EVT_DATA_RECORD_NAND);

            /* Initialize FRAM */
            status = CY15_FRAM_Init(LPSPICOM1_FRAM, LPSPI_PCS0);
            /* NOTE: Could hit this assert if loaded on wrong hardware */
            DEV_ASSERT(status == STATUS_SUCCESS);
            if (status != STATUS_SUCCESS)
            {
                report_error_uint(&AO_Data, Errors.DataaoFramInitFail, DO_NOT_SEND_TO_SPACECRAFT, 0UL);
            }

            /* Initialize NAND Flash */
            status = TC58_Flash_Init(LPSPICOM0_NAND, LPSPI_PCS0);
            DEV_ASSERT(status == STATUS_SUCCESS);
            if (status != STATUS_SUCCESS)
            {
                report_error_uint(&AO_Data, Errors.DataaoNandInitFail, DO_NOT_SEND_TO_SPACECRAFT, 0UL);
            }

            /* Get recorded TLM counts and data from FRAM meta data stored at the end of each FRAM bank.
             * Note: The reason keys are used to determine if data exists for each bank instead of, say, a
             * boolean variable is because it guards against 0x00 and 0xFF values that may have been
             * incorrectly written to those locations */
            metadata1 = 0U;
            metadata2 = 0UL;
            fram_metadata_read(BANK1, &metadata1, &metadata2);


            MaxData.rec_bank1_has_data = (metadata1 == BANK_HAS_DATA_KEY);

            if (metadata2 > NAND_BANK_MAX_TLM_CNT)
            {
                MaxData.rec_nand_tlm_cnt[BANK1] = NAND_BANK_MAX_TLM_CNT;
            }
            else
            {
                MaxData.rec_nand_tlm_cnt[BANK1] = metadata2;
            }

            metadata1 = 0U;
            metadata2 = 0UL;
            fram_metadata_read(BANK0, &metadata1, &metadata2);
            MaxData.rec_bank0_has_data = (metadata1 == BANK_HAS_DATA_KEY) || (metadata1 == BANK_HAS_ERR_KEY);

            /* If data exists in Bank0, retain it until commanded cleared */
            if (metadata1 == BANK_HAS_ERR_KEY)
            {
                MaxData.rec_bank_sel = BANK1;
            }
            else
            {
                MaxData.rec_bank_sel = BANK0;
            }

            if (metadata2 > NAND_BANK_MAX_TLM_CNT)
            {
                MaxData.rec_nand_tlm_cnt[BANK0] = NAND_BANK_MAX_TLM_CNT;
            }
            else
            {
                MaxData.rec_nand_tlm_cnt[BANK0] = metadata2;
            }

            GLADOS_STATE_TRAN_TO_CHILD(&DataAO_state_lounge);
            break;

        case GLADOS_EXIT:
            break;

        case EVT_TLM_PKT_READY:
            /* Ignore TLM packets if not recording */
            break;

        case EVT_CMD_START_RECORDING:
        case EVT_CMD_DUMP_MEM:
        case EVT_CMD_DUMP_TLM:
        case EVT_CMD_UPDATE_PRELOAD:
        case EVT_CMD_UPDATE_STAGE:
        case EVT_CMD_UPDATE_PROGRAM:
        case EVT_CMD_SET_REC_BANK0:
        case EVT_CMD_SET_REC_BANK1:
            CmdAO_push_nack_w_send_err(this_ao, create_error_uint(Errors.CmdDataModeIsBusy, 0UL));
            break;

        case EVT_CMD_DATA_ABORT:
            /* No need to return ACK since command always succeeds */
        case EVT_MAX_DATA_ABORT:
            GLADOS_STATE_TRAN_TO_CHILD(&DataAO_state_lounge);
            break;

        case EVT_ERR_MAX_TO_SAFE:
            /* If FDIR was detected, then move recording to the next bank to preserve BANK0
             * until errors are cleared */
            DataAO_SetRecord_Bank(BANK1);
            break;

        case EVT_CLEAR_ERRORS:
            /* If errors are cleared, then revert recording back to BANK0 */
            DataAO_SetRecord_Bank(BANK0);
            break;

        default:
            /* Top level state, skip undefined Events */
            break;
    }

    return;
}


void DataAO_state_lounge(GLADOS_AO_t *this_ao, GLADOS_Event_t *event)
{
    DEV_ASSERT(this_ao);
    DEV_ASSERT(event);

    switch(event->name)
    {
        /* GLADOS_ENTRY/GLADOS_EXIT optional event for each state */
        case GLADOS_ENTRY:
            MaxData.data_mode = DATA_LOUNGE;
            GPIO_SetOutput(GPIO_LED1, 0U);
            GPIO_SetOutput(GPIO_LED2, 0U);
            GPIO_SetOutput(GPIO_LED3, 0U);
            break;

        case GLADOS_EXIT:
            break;

        case EVT_CMD_START_RECORDING:
            GLADOS_AO_PUSH(&AO_Cmd, EVT_CMD_ACK_W_SEND);
            GLADOS_STATE_TRAN_TO_SIBLING(&DataAO_state_record);
            break;

        case EVT_MAX_START_RECORDING:
            GLADOS_STATE_TRAN_TO_SIBLING(&DataAO_state_record);
            break;

        case EVT_MAX_STATE_IN_SW_UPDATE:
            /* Clear data in bank 1 of FRAM since that's where the data is staged */
            MaxData.rec_bank1_has_data = false;
            MaxData.rec_nand_tlm_cnt[BANK1] = 0UL;
            fram_metadata_write(BANK1, 0U, 0UL);

            /* Note that in the rare chance that the metadata at the very end of BANK1
             * is overwritten in the load process (during a full image) and the MCU be
             * reset, it could indicate that recorded data exists in BANK1 and it would
             * permit TLM dumping. It's easier to simply allow this edge case to exist
             * rather than handle all the cases required to ensure that metadata is
             * re-cleared when leaving SW Update state (e.g. via cmd and due to FDIR).
             * The worst that would happen is that the TLM dump would be started but no
             * data would be dumped since all the CRCs would fail due to it just being
             * junk data. And in the astronomical chance that the CRC did match and a
             * dumped packet sneaks through, then there's no way that the unix time
             * would be valid for storing to InfluxDB (right?). I dunno, seems very
             * unlikely, but maybe I'm just a betting man (or lazy). */

            GLADOS_STATE_TRAN_TO_SIBLING(&DataAO_state_update_cmd);
            break;

        case EVT_CMD_CLEAR_REC_TLM:
            GLADOS_AO_PUSH(&AO_Cmd, EVT_CMD_ACK_W_SEND);

            /* Clear all SW-related recording data. This is all that
             * is necessary since any actual clearing of data in
             * NAND and FRAM happens during data recording (i.e. gets
             * overwritten in real time). So no need for long delays to
             * clear logged data. Pretty cool ey? */
            MaxData.rec_bank_sel = BANK0;
            MaxData.rec_nand_tlm_cnt[BANK0] = 0UL;
            MaxData.rec_nand_tlm_cnt[BANK1] = 0UL;
            MaxData.rec_nand_cycle_cnt = 0U;

            /* Clear any recording-related data in FRAM meta data */
            fram_metadata_write(BANK0, 0U, 0UL);
            fram_metadata_write(BANK1, 0U, 0UL);
            break;

        case EVT_CMD_DUMP_TLM:
            if (!event->has_data || (event->data_size != sizeof(DataAO_DumpTlm_RqstData_t)))
            {
                /* Let CmdAO handle sending NACK since unclear if event was corrupt or unexpected */
                CmdAO_push_nack_w_send_err(this_ao, create_error_uint(Errors.EvtCmdDumpTlmInvalidSize, event->data_size));
            }
            else
            {
                /* Copy event data over into a private variable */
                dao_dumptlm_rqst = *(DataAO_DumpTlm_RqstData_t *)&event->data[0];

                /* Inform CmdAO that command was accepted but no need to send an ACK
                 * as this is handled here */
                GLADOS_AO_PUSH(&AO_Cmd, EVT_CMD_ACK);
                TxAO_SendMsg_Ack(this_ao, dao_dumptlm_rqst.port, dao_dumptlm_rqst.src_id,
                                 dao_dumptlm_rqst.seq_id, dao_dumptlm_rqst.msg_id, Errors.StatusSuccess->error_id);

                /* Start the dump */
                GLADOS_STATE_TRAN_TO_SIBLING(&DataAO_state_dump_tlm);
            }
            break;

        case EVT_TLM_PKT_READY:
            /* Ignore TLM packets when not recording */
            break;

        default:
            GLADOS_STATE_CHECK_PARENT(&DataAO_state);
            break;
    }

    return;
}


void DataAO_state_record(GLADOS_AO_t *this_ao, GLADOS_Event_t *event)
{
    DEV_ASSERT(this_ao);
    DEV_ASSERT(event);

    /* Used to handoff the TLM packet to record between TlmAO and DataAO */
    static DataAO_RecordData_t *record_data;

    uint16_t tlm_crc;
    status_t status;

    switch(event->name)
    {
        /* GLADOS_ENTRY/GLADOS_EXIT optional event for each state */
        case GLADOS_ENTRY:
            MaxData.data_mode = DATA_RECORD;
            GPIO_SetOutput(GPIO_LED3, 1U);

            MaxData.rec_bank_sel = dao_next_bank & 0x1U;

            /* Reset recording parameters based on the bank in use.
             * Note: This means that once a record session has ended,
             * it cannot be resumed. To do so requires updating this
             * state as well as any others that may use these parameters. */
            if (MaxData.rec_bank_sel == BANK0)
            {
                MaxData.rec_curr_fram_addr      = FRAM_ADDR_BANK0_START;

                /* Set the current NAND area start to the NEXT block address, which is
                 * necessary since the next block selection algorithm requires this. */
                MaxData.rec_curr_nand_addr      = NAND_ADDR_BANK0_START;
                dao_curr_nand_area_start        = NAND_ADDR_BANK0_START + TC58_BLOCK_SIZE;
                dao_curr_nand_area_stop         = NAND_ADDR_BANK0_END;
                MaxData.rec_nand_tlm_cnt[BANK0] = 0UL;
            }
            else
            {
                MaxData.rec_curr_fram_addr      = FRAM_ADDR_BANK1_START;

                /* Set the current NAND area start to the NEXT block address, which is
                 * necessary since the next block selection algorithm requires this. */
                MaxData.rec_curr_nand_addr      = NAND_ADDR_BANK1_START;
                dao_curr_nand_area_start        = NAND_ADDR_BANK1_START + TC58_BLOCK_SIZE;
                dao_curr_nand_area_stop         = NAND_ADDR_BANK1_END;
                MaxData.rec_nand_tlm_cnt[BANK1] = 0UL;
            }
            dao_nand_minor_cnt = 0U;
            dao_nand_major_cnt = 0U;

            /* Halt any DMA SPI operations in case there are some pending */
            spi0_callback_send_dma_done = false;
            TC58_Flash_AbortTransfer(LPSPICOM0_NAND);
            spi1_callback_send_dma_done = false;
            CY15_FRAM_AbortTransfer(LPSPICOM1_FRAM);

            /* Prior to initiating recording, do a blocking write to the bank's FRAM metadata
             * to indicate data exists and the worst-case amount of TLM recorded to NAND. Doing
             * so allows for the system to know that data is recorded even in the event that
             * power is pulled or reset interrupts recording. Otherwise the appropriate TLM count
             * will be updated upon completion of recording.
             *
             * Note: If this step runs into a SPI busy assert, it likely means that the FRAM or NAND
             * is in use still from some other operation (not sure what though unless if commands are
             * sent quickly). An option in that case is to have a hard delay of 100us to allow for
             * any parallel SPI DMA transfers to complete prior to writing metadata. */
            fram_metadata_write(MaxData.rec_bank_sel, BANK_HAS_DATA_KEY, NAND_BANK_MAX_TLM_CNT);

            /* Push a self-event to clear the first block of NAND Flash.
             * Note: A bad block existing in the very first block of the NAND Flash bank
             * is handled by continuing forward. The first step is to erase the block,
             * which it will fail, and then the next valid block will be found after that.
             * Handling bad blocks here doesn't seem necessary here since the edge case is
             * handled by the rest of the AO.
             * Note: Self-event is necessary to ensure this block is cleared prior to any
             * EVT_TLM_PKT_READY messages trying to sneak in before NAND is ready. */
            GLADOS_AO_PUSH_EVT_SELF(&data_clear_nand_evt);
            break;

        case GLADOS_EXIT:
            break;

        case EVT_CMD_DATA_ABORT:
            /* No need to return ACK since command always succeeds */
        case EVT_MAX_DATA_ABORT:
            GLADOS_STATE_TRAN_TO_SIBLING(&DataAO_state_record_abort);
            break;


        case EVT_DATA_CLEAR_NAND:
            GLADOS_STATE_TRAN_TO_CHILD(&DataAO_state_record_clear_nand);
            break;

        /* Data: buf, length */
        case EVT_TLM_PKT_READY:
            DEV_ASSERT(event->has_data);
            DEV_ASSERT(event->data_size == sizeof(DataAO_RecordData_t));

            record_data = (DataAO_RecordData_t *)&event->data[0];

            /* Calculate the packet CRC for the maximum data to be logged */
            tlm_crc = crc16_compute(0xFFFFU, record_data->buf, TLMAO_MAX_REC_TLM_SIZE - 4U);

            /* Store the CRC16 just after the TLM packet in big-endian format.
             * The TLM buffer should have room for the 2 CRC bytes.  Also add
             * 2 bytes of F's to complete the 512B minimum for NAND Flash.
             * Keeping these last 2 bytes F's as if the memory is cleared is
             * necessary since the design checks for byte 2047 of a page to be
             * cleared, which indicates that the NAND block is valid as opposed
             * to bad (0x00) */
            record_data->buf[TLMAO_MAX_REC_TLM_SIZE-4U] = (uint8_t)((tlm_crc >> 8U) & 0xFFU);
            record_data->buf[TLMAO_MAX_REC_TLM_SIZE-3U] = (uint8_t)(tlm_crc & 0xFFU);
            record_data->buf[TLMAO_MAX_REC_TLM_SIZE-2U] = 0xFFU;
            record_data->buf[TLMAO_MAX_REC_TLM_SIZE-1U] = 0xFFU;

            /* Handle when recording to FRAM is enabled */
            if (MaxData.rec_fram_en)
            {
                /* Although we don't expect there to be transfers at this point, always start
                 * by aborting any DMA transfers that may be occurring in parallel.
                 * Note: Ensure that callbacks are disabled prior to abort transfer or else
                 * it sends a DMA done event. */
                spi1_callback_send_dma_done = false;
                CY15_FRAM_AbortTransfer(LPSPICOM1_FRAM);

                /* Initiate the non-blocking DMA transfer for data and CRC, do not overwrite last
                 * 2 bytes of the 512B buffer since this may be used for other things in FRAM
                 * for miscellaneous use.
                 * Note: No need to enable DMA done event callback since nothing needs to happen
                 * after the write completes */
                status  = CY15_FRAM_SetWEL(LPSPICOM1_FRAM, LPSPI_PCS0, true);
                status |= CY15_FRAM_WriteBlock_NonBlocking(LPSPICOM1_FRAM, LPSPI_PCS0, MaxData.rec_curr_fram_addr,
                                                           FRAM_REC_MAX_PKT_SIZE, record_data->buf);
                if (status != STATUS_SUCCESS)
                {
                    report_error_uint(&AO_Data, Errors.DataaoFramWriteFail, DO_NOT_SEND_TO_SPACECRAFT, MaxData.rec_curr_fram_addr);
                }

                /* Increment to the next FRAM address and handle rollovers
                 * based on the recording bank */
                MaxData.rec_curr_fram_addr += FRAM_REC_MAX_PKT_SIZE;

                if ((MaxData.rec_bank_sel == BANK0) &&
                    (MaxData.rec_curr_fram_addr >= FRAM_ADDR_BANK0_END))
                {
                    MaxData.rec_curr_fram_addr = FRAM_ADDR_BANK0_START;
                }
                else if ((MaxData.rec_bank_sel == BANK1) &&
                         (MaxData.rec_curr_fram_addr >= FRAM_ADDR_BANK1_END))
                {
                    MaxData.rec_curr_fram_addr = FRAM_ADDR_BANK1_START;
                }
            }

            /* Handle when recording to NAND Flash is enabled */
            if (MaxData.rec_nand_en)
            {
                GLADOS_AO_PUSH_EVT_SELF(&data_record_nand_evt);
            }
            break;

        case EVT_DATA_RECORD_NAND:
            /* Although we don't expect there to be transfers at this point, always start
             * by aborting any DMA transfers that may be occurring in parallel.
             * Note: Ensure that callbacks are disabled prior to abort transfer or else
             * it sends a DMA done event. */
            spi0_callback_send_dma_done = false;
            TC58_Flash_AbortTransfer(LPSPICOM0_NAND);

            /* Determine if last program was successful */
            status = TC58_Flash_LastProgStatus(LPSPICOM0_NAND, LPSPI_PCS0);
            if (status != STATUS_SUCCESS)
            {
                report_error_uint(&AO_Data, Errors.DataaoNandLastProgFail, DO_NOT_SEND_TO_SPACECRAFT, MaxData.rec_curr_nand_addr);
            }

            /* Initiate the non-blocking DMA transfer to NAND Flash.
             *
             * Writing to NAND Flash has a few steps. First one must enable the Write Enable Latch,
             * wait for it to complete, set the bytes in the Internal Buffer. At a later time,
             * load the page into the cell.
             *
             * Cascade the commands within the if statement to allow for a failure in any one of
             * these to back out of the entire operation. If this is not done and the SPI port
             * is not functioning, then each of these commands will execute and timeout, adding
             * long blocking delays. */

            status = STATUS_ERROR;
            if ((STATUS_SUCCESS == TC58_Flash_SetWEL(LPSPICOM0_NAND, LPSPI_PCS0, true))       &&
                (STATUS_SUCCESS == TC58_Flash_WELEnableWait(LPSPICOM0_NAND, LPSPI_PCS0, 2UL)))
            {
                /* Enable SPI complete callback */
                spi0_callback_send_dma_done = true;
                status = TC58_Flash_SetInternalBuf_NonBlocking(LPSPICOM0_NAND, LPSPI_PCS0,
                                                               TC58_FLASH_COL(MaxData.rec_curr_nand_addr),
                                                               NAND_REC_MAX_PKT_SIZE, record_data->buf);
            }

            if (status != STATUS_SUCCESS)
            {
                spi0_callback_send_dma_done = false;
                report_error_uint(&AO_Data, Errors.DataaoFramWriteFail, DO_NOT_SEND_TO_SPACECRAFT, MaxData.rec_curr_nand_addr);
            }
            else
            {
                /* While the bytes are being written to the Internal Buffer, transition to
                 * the next step that will program the data into the cell */
                GLADOS_STATE_TRAN_TO_CHILD(&DataAO_state_record_write_nand);
            }
            break;

        case EVT_DATA_DMA_NAND_DONE:
        case EVT_DATA_DMA_FRAM_DONE:
            /* Ignore transfer completion events when writing here */
            break;

        default:
            GLADOS_STATE_CHECK_PARENT(&DataAO_state);
            break;
    }

    return;
}


void DataAO_state_record_abort(GLADOS_AO_t *this_ao, GLADOS_Event_t *event)
{
    DEV_ASSERT(this_ao);
    DEV_ASSERT(event);

    uint32_t tlm_cnts;

    switch(event->name)
    {
        /* GLADOS_ENTRY/GLADOS_EXIT optional event for each state */
        case GLADOS_ENTRY:
            /* Reset the cycle count */
            MaxData.rec_nand_cycle_cnt = 0U;

            /* Halt any DMA SPI operations in case there are some pending */
            spi1_callback_send_dma_done = false;
            spi0_callback_send_dma_done = false;

            /* Wait one or more frames to allow for the last of the recording
             * to complete prior to storing the recording metadata to FRAM */
            GLADOS_Timer_Set(&Tmr_DataAux, PT->tmr_rec_metadata_delay_us);
            GLADOS_Timer_EnableOnce(&Tmr_DataAux);
            break;

        case GLADOS_EXIT:
            /* Handles case when ABORT command is received during this state */
            GLADOS_Timer_Disable(&Tmr_DataAux);

            break;

        case EVT_TMR_DATA_AUX_DONE:
            /* Update the recording meta data at the end of the current bank in FRAM.
             * Note: This banks on the bank being unchanged for the duration of rec session. */
            tlm_cnts = MaxData.rec_nand_tlm_cnt[MaxData.rec_bank_sel];
            if (tlm_cnts > NAND_BANK_MAX_TLM_CNT)
            {
                tlm_cnts = NAND_BANK_MAX_TLM_CNT;
            }

            fram_metadata_write(MaxData.rec_bank_sel, BANK_HAS_DATA_KEY, tlm_cnts);

            GLADOS_STATE_TRAN_TO_SIBLING(&DataAO_state_lounge);
            break;

        case EVT_DATA_DMA_NAND_DONE:
        case EVT_DATA_DMA_FRAM_DONE:
            /* Ignore transfer completion events when writing here */
            break;

        default:
            GLADOS_STATE_CHECK_PARENT(&DataAO_state);
            break;
    }

    return;
}


void DataAO_state_record_clear_nand(GLADOS_AO_t *this_ao, GLADOS_Event_t *event)
{
    DEV_ASSERT(this_ao);
    DEV_ASSERT(event);

    static bool nand_xfer_error;

    status_t status;

    switch(event->name)
    {
        /* GLADOS_ENTRY/GLADOS_EXIT optional event for each state */
        case GLADOS_ENTRY:
            /* Although we don't expect there to be transfers at this point, always start
             * by aborting any DMA transfers that may be occurring in parallel.
             * Note: Ensure that callbacks are disabled prior to abort transfer or else
             * it sends a DMA done event. */
            spi0_callback_send_dma_done = false;
            TC58_Flash_AbortTransfer(LPSPICOM0_NAND);

            /* At this point, the current address is one that does not exist in the bad block map */
            status = TC58_Flash_BlockErase(LPSPICOM0_NAND, LPSPI_PCS0, MaxData.rec_curr_nand_addr);

            /* SPI errors are handled on the other side of the erase timer. This causes
             * the recording to NAND Flash to delay until the FSW can get a valid block
             * erased. This ensures data recording coherency and handles cases where the SPI
             * is completely disconnected in a simple way without bogging down the system
             * with repeated retries.
             * Note: Do not mark SPI-related errors as bad blocks since they're separate issues! */
            nand_xfer_error = (status != STATUS_SUCCESS);

            GLADOS_Timer_Set(&Tmr_DataAux, PT->tmr_nand_clear_dur_us);
            GLADOS_Timer_EnableOnce(&Tmr_DataAux);
            break;

        case GLADOS_EXIT:
            GLADOS_Timer_Disable(&Tmr_DataAux);
            break;

        case EVT_TMR_DATA_AUX_DONE:
            /* Check if a SPI error occurred on the erase command */
            if (nand_xfer_error)
            {
                report_error_uint(&AO_Data, Errors.DataaoNandEraseFail, DO_NOT_SEND_TO_SPACECRAFT, MaxData.rec_curr_nand_addr);

                /* Retry the erase to the same NAND Flash address block */
                GLADOS_AO_PUSH_EVT_SELF(&data_clear_nand_evt);
            }
            else
            {
                /* Check if last erase status has succeeded */
                status = TC58_Flash_LastEraseStatus(LPSPICOM0_NAND, LPSPI_PCS0);
                if (status == STATUS_SUCCESS)
                {
                    /* Successful erase indicates that the next block is ready for recording */
                    GLADOS_STATE_TRAN_TO_PARENT(&DataAO_state_record);
                }
                else
                {
                    /* Erase failure indicates a bad block.  Put it on the naughty list!
                     * Note: Even if it really is not a bad block and failed for other reasons,
                     * it is easier to treat it as such. It's possible to reclaim this block
                     * since the bad block map is recreated upon each power cycle. */
                    bad_block_mark(MaxData.rec_curr_nand_addr);

                    /* Since the current address is marked bad, update MaxData.rec_curr_nand_addr to the
                     * next valid NAND block address */
                    get_next_nand_addr(NAND_BLOCK);

                    /* Note: we are not logging errors for bad blocks that are found, as these
                     * are expected to be discovered throughout the lifetime of the system */

                    /* Attempt to clear the new NAND Flash address block */
                    GLADOS_AO_PUSH_EVT_SELF(&data_clear_nand_evt);
                }
            }
            break;

        case EVT_DATA_DMA_NAND_DONE:
            /* Ignore this event since the timer manages when the erase actually completes */
            break;

        case EVT_DATA_RECORD_NAND:
            /* Ignore additional TLM requests to NAND Flash if already busy */
            break;

        default:
            GLADOS_STATE_CHECK_PARENT(&DataAO_state_record);
            break;
    }

    return;
}


void DataAO_state_record_write_nand(GLADOS_AO_t *this_ao, GLADOS_Event_t *event)
{
    DEV_ASSERT(this_ao);
    DEV_ASSERT(event);

    status_t status;

    switch(event->name)
    {
        /* GLADOS_ENTRY/GLADOS_EXIT optional event for each state */
        case GLADOS_ENTRY:
            GLADOS_Timer_Set(&Tmr_DataTimeout, PT->tmr_nand_write_dur_us);
            GLADOS_Timer_EnableOnce(&Tmr_DataTimeout);
            break;

        case GLADOS_EXIT:
            GLADOS_Timer_Disable(&Tmr_DataTimeout);
            GLADOS_Timer_Disable(&Tmr_DataAux);
            break;

        case EVT_DATA_DMA_NAND_DONE:
            /* Disable timeout timer after SPI comm has completed */
            GLADOS_Timer_Disable(&Tmr_DataTimeout);

            /* Disable transfer completion callbacks for blocking calls */
            spi0_callback_send_dma_done = false;

            /* The write to the Internal Buffer has completed, now it's time
             * to initiate the command to load it into the cell. This
             * process takes a max of 500us to complete. */
            status = TC58_Flash_LoadCell(LPSPICOM0_NAND, LPSPI_PCS0, MaxData.rec_curr_nand_addr);

            if (status != STATUS_SUCCESS)
            {
                report_error_uint(&AO_Data, Errors.DataaoNandLoadFail, DO_NOT_SEND_TO_SPACECRAFT, MaxData.rec_curr_nand_addr);
            }
            else
            {
                MaxData.rec_nand_tlm_cnt[MaxData.rec_bank_sel] += 1UL;
            }

            /* Enable Aux timer to wait for the max 500us for the load to complete.
             * Note: It appears this implementation is safe from any race conditions
             * that may screw up nand erase. The following condition is NOT possible:
             *  - EVT_TMR_DATA_TIMEOUT pushed immediately after EVT_DATA_DMA_NAND_DONE
             *  - EVT_TMR_DATA_AUX_DONE pushed immediately after EVT_TMR_DATA_TIMEOUT
             *  - Erase thinks it completed early */
            GLADOS_Timer_Set(&Tmr_DataAux, PT->tmr_nand_prog_dur_us);
            GLADOS_Timer_EnableOnce(&Tmr_DataAux);
            break;

        case EVT_TMR_DATA_TIMEOUT:
            /* If a page program fails and times out, it's possible that programs to the
             * rest of the flash block will also fail due to the following restriction:
             * pages within a block must be programmed in order. The implication is that
             * there could be many of these errors that will occur for the block until it
             * reaches a new block. For this reasoning, we do not log any errors and will
             * lose recorded data for the block, which amounts to 2.5 seconds of data max
             * (poof! gone). This is okay because long-term data recording is a nicety.
             *
             * Fall through (yes on purpose) */
        case EVT_TMR_DATA_AUX_DONE:
            /* Update MaxData.rec_curr_nand_addr with the next NAND address for recording */
            get_next_nand_addr(NAND_PKT);

            /* If the currend nand address is on a block boundary, transition to
             * perform a block erase */
            if (TC58_ON_BLOCK_BOUNDARY(MaxData.rec_curr_nand_addr))
            {
                /* Clear the new NAND Flash address block before the next TLM packet */
                GLADOS_AO_PUSH_EVT_SELF(&data_clear_nand_evt);
            }
            else
            {
                /* Otherwise return to the parent and await the next TLM packet */
                GLADOS_STATE_TRAN_TO_PARENT(&DataAO_state_record);
            }
            break;

        case EVT_DATA_RECORD_NAND:
            /* Ignore new NAND recording when one is already underway */
            break;

        default:
            GLADOS_STATE_CHECK_PARENT(&DataAO_state_record);
            break;
    }

    return;
}


void DataAO_state_dump_tlm(GLADOS_AO_t *this_ao, GLADOS_Event_t *event)
{
    DEV_ASSERT(this_ao);
    DEV_ASSERT(event);

    static uint8_t banks_to_dump;
    static uint8_t dump_curr_bank;

    bool valid_block_found;
    uint32_t bank_end_addr;

    switch(event->name)
    {
        /* GLADOS_ENTRY/GLADOS_EXIT optional event for each state */
        case GLADOS_ENTRY:
            MaxData.data_mode = DATA_DUMP_TLM;
            GPIO_SetOutput(GPIO_LED2, 1U);

            switch (dao_dumptlm_rqst.bank)
            {
                case 2U:
                    banks_to_dump = 2U;
                    dump_curr_bank = 0U;
                    break;
                case 1U:
                    banks_to_dump = 1U;
                    dump_curr_bank = 1U;
                    break;
                default:
                case 0U:
                    banks_to_dump = 1U;
                    dump_curr_bank = 0U;
                    break;
            }

            GLADOS_AO_PUSH(&AO_Data, EVT_DATA_DUMP_START);
            break;

        case GLADOS_EXIT:
            break;

        case EVT_DATA_DUMP_START:
        case EVT_DATA_DUMP_NEXT_BANK:
            /* If there are no more banks to dump */
            if (banks_to_dump == 0U)
            {
                /* Dump complete, get back to lounge (don't forget to wipe) */
                TxAO_SendMsg_Ack(&AO_Data, dao_dumptlm_rqst.port, dao_dumptlm_rqst.src_id,
                                     dao_dump_seq_cnt, DUMP_TLM_DONE, 0U);
                ++dao_dump_seq_cnt;
                GLADOS_STATE_TRAN_TO_SIBLING(&DataAO_state_lounge);
            }
            /* Otherwise, setup the next bank start and stop addresses for a dumping */
            else
            {
                if (dao_dumptlm_rqst.mem_type == DATAAO_MEMTYPE_NAND)
                {
                    dao_dump_curr_addr = NAND_ADDR_BANK0_START + (dump_curr_bank * NAND_BANK_MAX_SIZE);
                    dao_dump_tlm_cnt = (int32_t)MaxData.rec_nand_tlm_cnt[dump_curr_bank];

                    /* If the number of TLM packets is greater than the number that can fit in the bank space,
                     * then set the count to the max for the bank. */
                    if (dao_dump_tlm_cnt > (int32_t)NAND_BANK_MAX_TLM_CNT)
                    {
                        dao_dump_tlm_cnt = (int32_t)NAND_BANK_MAX_TLM_CNT;
                    }
                }
                else
                {
                    /* Use FRAM as default since it is much smaller and much easier to work with
                     * and therefore less risky to use should the mem_type be incorrect. Always
                     * attempt to dump the maximum TLM count since FRAM fills after a few seconds. */
                    dao_dump_curr_addr = FRAM_ADDR_BANK0_START + (dump_curr_bank * FRAM_BANK_MAX_SIZE);
                    dao_dump_tlm_cnt = (int32_t)FRAM_BANK_MAX_TLM_CNT;
                }

                /* After the bank start and end addresses have been computed, get settings for next bank */
                --banks_to_dump;
                ++dump_curr_bank;

                GLADOS_AO_PUSH(&AO_Data, EVT_DATA_DUMP_NEXT_TLM);
            }
            break;

        case EVT_DATA_DUMP_NEXT_TLM:
        case EVT_TX_DUMP_NEXT:
            GPIO_ToggleOutput(GPIO_LED3);

            /* Check if there is any more TLM packets left to dump */
            if (dao_dump_tlm_cnt <= 0L)
            {
                /* Dump of the current bank has completed or has no data in it */
                GLADOS_AO_PUSH(&AO_Data, EVT_DATA_DUMP_NEXT_BANK);
            }
            else if (dao_dumptlm_rqst.mem_type == DATAAO_MEMTYPE_NAND)
            {
                /* Get the end addr of the current bank rather than the end recorded address since
                 * there are an unknown number of bad blocks between the current address and the
                 * end of the bank.
                 * Note: Current bank was already incremented preemptively so subtract one here */
                bank_end_addr = NAND_ADDR_BANK0_END + ((dump_curr_bank - 1U) * NAND_BANK_MAX_SIZE);

                /* If the current address is beyond the bank end, then this bank is complete */
                if (dao_dump_curr_addr >= bank_end_addr)
                {
                    GLADOS_AO_PUSH(&AO_Data, EVT_DATA_DUMP_NEXT_BANK);
                }
                /* If the new address falls into a new NAND block, check/get the next valid block */
                else if (TC58_ON_BLOCK_BOUNDARY(dao_dump_curr_addr))
                {

                    /* Consult the bad block map for the next valid block in the NAND Flash chip */
                    valid_block_found = bad_block_get_next_valid_addr(&dao_dump_curr_addr,
                                                                      dao_dump_curr_addr,
                                                                      bank_end_addr);
                    if (!valid_block_found)
                    {
                        /* If no valid block is found, then nothing left to dump for the current bank */
                        GLADOS_AO_PUSH(&AO_Data, EVT_DATA_DUMP_NEXT_BANK);
                    }
                    else
                    {
                        DEV_ASSERT(AO_Data.state_fn_ptr != DataAO_state_dump_tlm_nand_read);
                        GLADOS_STATE_TRAN_TO_CHILD(&DataAO_state_dump_tlm_nand_read);
                    }
                }
                else
                {
                    DEV_ASSERT(AO_Data.state_fn_ptr != DataAO_state_dump_tlm_nand_read);
                    GLADOS_STATE_TRAN_TO_CHILD(&DataAO_state_dump_tlm_nand_read);
                }
            }
            else
            {
                DEV_ASSERT(AO_Data.state_fn_ptr != DataAO_state_dump_tlm_fram_read);
                GLADOS_STATE_TRAN_TO_CHILD(&DataAO_state_dump_tlm_fram_read);
            }
            break;

        case EVT_TMR_DATA_TIMEOUT:
            /* Ignore timer events that trigger just after the data transfer has completed */
            break;

        case EVT_DATA_DMA_NAND_DONE:
        case EVT_DATA_DMA_FRAM_DONE:
            /* Ignore DMA transfer completions just after a timeout that has already triggered */
            break;

        default:
            GLADOS_STATE_CHECK_PARENT(&DataAO_state);
            break;
    }

    return;
}


void DataAO_state_dump_tlm_nand_read(GLADOS_AO_t *this_ao, GLADOS_Event_t *event)
{
    DEV_ASSERT(this_ao);
    DEV_ASSERT(event);

    status_t status;

    switch(event->name)
    {
        /* GLADOS_ENTRY/GLADOS_EXIT optional event for each state */
        case GLADOS_ENTRY:
            /* Although we don't expect there to be transfers at this point, always start
             * by aborting any DMA transfers that may be occurring in parallel.
             * Note: Ensure that callbacks are disabled prior to abort transfer or else
             * it sends a DMA done event. */
            spi0_callback_send_dma_done = false;
            TC58_Flash_AbortTransfer(LPSPICOM0_NAND);

            /* Reading NAND Flash requires that the page cell be read into the internal buffer
             * prior to accesses.  First check if the data is on a page boundary to see if that
             * extra step needs to be done. */
            status = STATUS_SUCCESS;
            if (TC58_ON_PAGE_BOUNDARY(dao_dump_curr_addr))
            {
                status |= TC58_Flash_ReadCell(LPSPICOM0_NAND, LPSPI_PCS0, dao_dump_curr_addr);

                /* Fixed delay for simplicity
                 * Note: Delay less than 3 with 20MHz SPI does not work with DEV_ASSERTs removed */
                status |= TC58_Flash_BusyWait(LPSPICOM0_NAND, LPSPI_PCS0, 3U);
            }

            if (status == STATUS_SUCCESS)
            {
                /* Enable callbacks prior to sending non-blocking call */
                spi0_callback_send_dma_done = true;

                /* Read the data using a non-blocking call. Read full 512B even though it is less
                 * since we want to include the CRC bytes and in case we want to validate that the
                 * last two bytes are 0xFFFF, which is used for bad block determination. */
                status = TC58_Flash_GetInternalBuf_NonBlocking(LPSPICOM0_NAND,
                                                               LPSPI_PCS0,
                                                               TC58_FLASH_COL(dao_dump_curr_addr),
                                                               TLMAO_MAX_REC_TLM_SIZE,
                                                               &dao_dump_rx_buf);
            }

            if (status != STATUS_SUCCESS)
            {
                /* If something goes awry, abort transfer and move onto the next TLM.
                 * If the Read Cell command fails, then this is not the end
                 * of the world because the next 3 dumped TLM will either be old data
                 * that has already been dumped, or will fail the CRC and filtered
                 * out anyway. */
                spi0_callback_send_dma_done = false;
                TC58_Flash_AbortTransfer(LPSPICOM0_NAND);
                report_error_uint(&AO_Data, Errors.DataaoNandReadFail, DO_NOT_SEND_TO_SPACECRAFT, dao_dump_curr_addr);
                GLADOS_STATE_TRAN_TO_PARENT(&DataAO_state_dump_tlm);
                GLADOS_AO_PUSH(&AO_Data, EVT_DATA_DUMP_NEXT_TLM);
            }

            /* Get the next nand address to dump, which ensures that this continues to increment
             * even in the event of a failed transfer or a timeout. Ensure that it falls on a
             * 512-byte boundary. */
            dao_dump_curr_addr += NAND_REC_MAX_PKT_SIZE;
            dao_dump_curr_addr &= ~(NAND_REC_MAX_PKT_SIZE - 1UL);

            /* Each entry into this state decrements the total TLM count, which prevents
             * the possibility of getting stuck in the dumping TLM state FOR-EV-ER! */
            --dao_dump_tlm_cnt;

            /* Initiate a DMA timeout in case the transfer never completes */
            GLADOS_Timer_Set(&Tmr_DataTimeout, PT->tmr_nand_read_dur_us);
            GLADOS_Timer_EnableOnce(&Tmr_DataTimeout);
            break;

        case GLADOS_EXIT:
            GLADOS_Timer_Disable(&Tmr_DataTimeout);
            break;

        case EVT_TX_DUMP_NEXT:
            /* Should never get here, but if we do then ignore it since either timeout
             * or reception of the next TLM packet will initiate another transfer.
             * This is compiled out in released code. */
            DEV_ASSERT(0);
            break;

        case EVT_TMR_DATA_TIMEOUT:
            GLADOS_STATE_TRAN_TO_PARENT(&DataAO_state_dump_tlm);
            GLADOS_AO_PUSH(&AO_Data, EVT_DATA_DUMP_NEXT_TLM);
            break;

        case EVT_DATA_DMA_NAND_DONE:
            spi0_callback_send_dma_done = false;

            /* Check the returned data for the bad block marker, which is returned
             * in the last byte of the 512B packet  */
            if (dao_dump_rx_buf[BB_COL_CHECK_OFFSET_TLM] != BB_VALID_BLOCK_VALUE)
            {
                /* If the block is invalid, then mark the bad block and increment to the next block.
                 * You say: "But mommay, shouldn't the bad block map be built alreaday?"
                 * We say: "Not necessarily, if MCU is reset and TLM dump is initiated. Now eat your peas." */
                bad_block_mark(dao_dump_curr_addr);

                /* Skip the current block. Ensure that the next block falls on a block boundary */
                dao_dump_curr_addr += TC58_BLOCK_SIZE;
                dao_dump_curr_addr &= ~(TC58_BLOCK_SIZE - 1UL);

                /* JK! We didn't want to decrement the TLM count just yet since the block was bad.
                 * Adding it back. */
                ++dao_dump_tlm_cnt;

                /* Initiate the dump of the next TLM packet since TxAO is not sent anything. */
                GLADOS_AO_PUSH(&AO_Data, EVT_DATA_DUMP_NEXT_TLM);
            }
            /* Check CRC for the full packet and 2 CRC bytes, which evaluates to zero if a match.
             * This also handles instances when the page is erased since it will fail the CRC. */
            else if (0x0000U != crc16_compute(0xFFFFU, dao_dump_rx_buf, TLMAO_MAX_REC_TLM_SIZE - 2U))
            {
                /* This was entered due to a CRC mismatch caused by corrupt data, SEU, or cleared data.
                 * Skip this TLM. Dump the next TLM to keep going since TxAO was not sent anything.
                 * Note: Will hit this case if the bad block map was not yet generated and a TLM dump
                 * requests bad block data. */
                GLADOS_AO_PUSH(&AO_Data, EVT_DATA_DUMP_NEXT_TLM);
            }
            else
            {
                /* Send the first as data transfer and second as a dump transfer since we want the
                 * second transfer to signal back to the DataAO when the transfer has completed */
                TxAO_SendMsg_Data(&AO_Data, dao_dumptlm_rqst.port, dao_dumptlm_rqst.src_id, dao_dump_seq_cnt,
                        RX_DUMP_TLM_PCU, (uint32_t)&dao_dump_rx_buf[0], TLMAO_MAX_TLM_DATA);
                ++dao_dump_seq_cnt;

                TxAO_SendMsg_Dump(&AO_Data, dao_dumptlm_rqst.port, dao_dumptlm_rqst.src_id, dao_dump_seq_cnt,
                        RX_DUMP_TLM_RFT, (uint32_t)&dao_dump_rx_buf[TLMAO_MAX_TLM_DATA], TLMAO_MAX_TLM_DATA);
                ++dao_dump_seq_cnt;
            }

            /* Transition back and await the signal to dump the next TLM packet */
            GLADOS_STATE_TRAN_TO_PARENT(&DataAO_state_dump_tlm);
            break;

        default:
            GLADOS_STATE_CHECK_PARENT(&DataAO_state_dump_tlm);
            break;
    }

    return;
}


void DataAO_state_dump_tlm_fram_read(GLADOS_AO_t *this_ao, GLADOS_Event_t *event)
{
    DEV_ASSERT(this_ao);
    DEV_ASSERT(event);

    status_t status;

    switch(event->name)
    {
        /* GLADOS_ENTRY/GLADOS_EXIT optional event for each state */
        case GLADOS_ENTRY:
            /* Although we don't expect there to be transfers at this point, always start
             * by aborting any DMA transfers that may be occurring in parallel.
             * Note: Ensure that callbacks are disabled prior to abort transfer or else
             * it sends a DMA done event. */
            spi1_callback_send_dma_done = false;
            CY15_FRAM_AbortTransfer(LPSPICOM1_FRAM);

            /* Enable callbacks prior to sending non-blocking call */
            spi1_callback_send_dma_done = true;

            /* Read the data using a non-blocking call. Read full 512B even though it is less
             * since we want to include the CRC bytes and in case we want to validate that the
             * last two bytes are 0xFFFF, which is used for bad block determination. */
            status = CY15_FRAM_ReadBlock_NonBlocking(LPSPICOM1_FRAM,
                                                     LPSPI_PCS0,
                                                     dao_dump_curr_addr,
                                                     FRAM_REC_MAX_PKT_SIZE,
                                                     &dao_dump_rx_buf);

            if (status != STATUS_SUCCESS)
            {
                /* If something is awry, abort transfer and move onto the next TLM */
                spi0_callback_send_dma_done = false;
                CY15_FRAM_AbortTransfer(LPSPICOM1_FRAM);
                report_error_uint(&AO_Data, Errors.DataaoFramReadFail, DO_NOT_SEND_TO_SPACECRAFT, dao_dump_curr_addr);
                GLADOS_STATE_TRAN_TO_PARENT(&DataAO_state_dump_tlm);
                GLADOS_AO_PUSH(&AO_Data, EVT_DATA_DUMP_NEXT_TLM);
            }

            dao_dump_curr_addr += FRAM_REC_MAX_PKT_SIZE;

            /* Each entry into this state decrements the total TLM count, which prevents
             * the possibility of getting stuck in the dumping TLM state FOR-EV-ER! */
            --dao_dump_tlm_cnt;

            /* Initiate a DMA timeout in case the transfer never completes */
            GLADOS_Timer_Set(&Tmr_DataTimeout, PT->tmr_fram_xfer_timeout_us);
            GLADOS_Timer_EnableOnce(&Tmr_DataTimeout);
            break;

        case GLADOS_EXIT:
            GLADOS_Timer_Disable(&Tmr_DataTimeout);
            break;

        case EVT_TX_DUMP_NEXT:
            /* Should never get here, but if we do then ignore it since either timeout
             * or reception of the next TLM packet will initiate another transfer.
             * This is compiled out in released code. */
            DEV_ASSERT(0);
            break;

        case EVT_TMR_DATA_TIMEOUT:
            GLADOS_STATE_TRAN_TO_PARENT(&DataAO_state_dump_tlm);
            GLADOS_AO_PUSH(&AO_Data, EVT_DATA_DUMP_NEXT_TLM);
            break;

        case EVT_DATA_DMA_FRAM_DONE:
            spi1_callback_send_dma_done = false;

            /* Check CRC for the full packet and 2 CRC bytes, which evaluates to zero if a match.
             * This also handles instances when the page is erased since it will fail the CRC. */
            if (0x0000U != crc16_compute(0xFFFFU, dao_dump_rx_buf, FRAM_REC_MAX_PKT_SIZE))
            {
                /* This was entered due to a CRC mismatch caused by corrupt data, SEU, or cleared data.
                 * Skip this TLM. Dump the next TLM to keep going since TxAO was not sent anything. */
                GLADOS_AO_PUSH(&AO_Data, EVT_DATA_DUMP_NEXT_TLM);
            }
            else
            {
                /* Send the first as data transfer and second as a dump transfer since we want the
                 * second transfer to signal back to the DataAO when the transfer has completed */
                TxAO_SendMsg_Data(&AO_Data, dao_dumptlm_rqst.port, dao_dumptlm_rqst.src_id, dao_dump_seq_cnt,
                        RX_DUMP_TLM_PCU, (uint32_t)&dao_dump_rx_buf[0], TLMAO_MAX_TLM_DATA);
                ++dao_dump_seq_cnt;

                TxAO_SendMsg_Dump(&AO_Data, dao_dumptlm_rqst.port, dao_dumptlm_rqst.src_id, dao_dump_seq_cnt,
                        RX_DUMP_TLM_RFT, (uint32_t)&dao_dump_rx_buf[TLMAO_MAX_TLM_DATA], TLMAO_MAX_TLM_DATA);
                ++dao_dump_seq_cnt;
            }
            GLADOS_STATE_TRAN_TO_PARENT(&DataAO_state_dump_tlm);
            break;

        default:
            GLADOS_STATE_CHECK_PARENT(&DataAO_state_dump_tlm);
            break;
    }

    return;
}


void DataAO_state_update_cmd(GLADOS_AO_t *this_ao, GLADOS_Event_t *event)
{
    DEV_ASSERT(this_ao);
    DEV_ASSERT(event);

    DataAO_PreloadData_t *preload_data;
    DataAO_StageData_t *stage_data;
    DataAO_ProgData_t *prog_data;

    switch(event->name)
    {
        /* GLADOS_ENTRY/GLADOS_EXIT optional event for each state */
        case GLADOS_ENTRY:
            MaxData.data_mode = DATA_UP_CMD;
            GPIO_SetOutput(GPIO_LED1, 1U);
            GPIO_SetOutput(GPIO_LED2, 0U);
            GPIO_SetOutput(GPIO_LED3, 0U);
            break;

        case GLADOS_EXIT:
            break;

        case EVT_CMD_UPDATE_PRELOAD:
            DEV_ASSERT(event->has_data);
            DEV_ASSERT(event->data_size == sizeof(DataAO_PreloadData_t));

            preload_data = (DataAO_PreloadData_t *)&event->data[0];

            dao_cmd_pflash_addr = preload_data->pflash_addr;
            dao_cmd_length      = preload_data->length;
            dao_cmd_fram_offset = preload_data->fram_offset;

            /* Validate alignment, length, and region, taking into account possible addr rollover */
            if ((dao_cmd_length >  0UL)                                            &&
                (dao_cmd_length <= UPDATE_MAX_LOAD_SIZE)                           &&
                (dao_cmd_fram_offset < UPDATE_MAX_LOAD_SIZE)                       &&
                ((dao_cmd_fram_offset + dao_cmd_length) <= UPDATE_MAX_LOAD_SIZE)   &&
                (dao_cmd_pflash_addr <  UPDATE_PRELD_ADDR_END)                     &&
                ((dao_cmd_pflash_addr + dao_cmd_length) <= UPDATE_PRELD_ADDR_END))
            {
                /* The FRAM staging occurs in BANK1 */
                dao_cmd_fram_offset += FRAM_ADDR_BANK1_START;

                /* Transition to Preload state */
                GLADOS_STATE_TRAN_TO_SIBLING(&DataAO_state_preload);
            }
            else
            {
                /* Otherwise invalid, send NACK */
                CmdAO_push_nack_w_send_err(this_ao, create_error_uint(Errors.CmdUpdatePreloadInvalidParam, 0UL));
            }
            break;

        case EVT_CMD_UPDATE_STAGE:
            DEV_ASSERT(event->has_data);
            DEV_ASSERT(event->data_size == sizeof(DataAO_StageData_t));

            stage_data = (DataAO_StageData_t *)&event->data[0];

            dao_cmd_fram_offset = stage_data->fram_offset;
            dao_cmd_data_ptr    = stage_data->data_ptr;
            dao_cmd_length      = stage_data->length;

            /* Validate the staging params and that it does not exceed the load boundary */
            if ((dao_cmd_data_ptr != 0UL)                    &&
                (dao_cmd_length > 0UL)                       &&
                (dao_cmd_length <= UPDATE_MAX_STAGE_SIZE)    &&
                (dao_cmd_fram_offset < UPDATE_MAX_LOAD_SIZE) &&
                ((dao_cmd_fram_offset + dao_cmd_length) <= UPDATE_MAX_LOAD_SIZE))
            {
                /* The FRAM staging occurs in BANK1 */
                dao_cmd_fram_offset += FRAM_ADDR_BANK1_START;

                /* Transition to Stage state */
                GLADOS_STATE_TRAN_TO_SIBLING(&DataAO_state_stage);
                /* Only send ACK after the stage has completed */
            }
            else
            {
                /* Otherwise invalid, send NACK */
                CmdAO_push_nack_w_send_err(this_ao, create_error_uint(Errors.CmdUpdateStageInvalidParam, 0UL));
            }
            break;

        case EVT_CMD_UPDATE_PROGRAM:
            DEV_ASSERT(event->has_data);
            DEV_ASSERT(event->data_size == sizeof(DataAO_ProgData_t));

            prog_data = (DataAO_ProgData_t *)&event->data[0];

            dao_cmd_pflash_addr = prog_data->pflash_addr;
            dao_cmd_length      = prog_data->length;
            dao_cmd_crc         = prog_data->crc;

            if ((MaxData.using_app_pt) && (MaxData.app_select == 1U) &&
                     (dao_cmd_pflash_addr >= PARM_TABLE_APP1) && (dao_cmd_pflash_addr < (PARM_TABLE_APP1 + IMG_MAX_SIZE)))
            {
                /* Do not permit loads to the App PT if it's currently in use */
                CmdAO_push_nack_w_send_err(this_ao, create_error_uint(Errors.CmdUpdateProgramApp1PtInUse, 0UL));
            }
            else if ((MaxData.using_app_pt) && (MaxData.app_select == 2U) &&
                     (dao_cmd_pflash_addr >= PARM_TABLE_APP2) && (dao_cmd_pflash_addr < (PARM_TABLE_APP2 + IMG_MAX_SIZE)))
            {
                /* Do not permit loads to the App PT if it's currently in use */
                CmdAO_push_nack_w_send_err(this_ao, create_error_uint(Errors.CmdUpdateProgramApp2PtInUse, 0UL));
            }
            /* Validate alignment, length, and region, taking into account possible addr rollover */
            /* Note: CRC32 implementation requires that the length fall on a 4B boundary */
            else if ((IS_128KB_ALIGNED(dao_cmd_pflash_addr))                            &&
                     (dao_cmd_length >  0UL)                                            &&
                     (dao_cmd_length <= UPDATE_MAX_LOAD_SIZE)                           &&
                     ((dao_cmd_length % 4U) == 0UL)                                     &&
                     ((dao_cmd_pflash_addr + dao_cmd_length) >  UPDATE_PROG_ADDR_BEGIN) &&
                     ((dao_cmd_pflash_addr + dao_cmd_length) <= UPDATE_PROG_ADDR_END))
            {
                /* Transition to begin the programming sequence */
                GLADOS_STATE_TRAN_TO_SIBLING(&DataAO_state_fram_validate);
                /* Only send ACK after the staged image has completed */
            }
            else
            {
                /* Otherwise invalid, send NACK */
                CmdAO_push_nack_w_send_err(this_ao, create_error_uint(Errors.CmdUpdateProgramInvalidParam, 0UL));
            }
            break;

        case EVT_TMR_DATA_TIMEOUT:
            /* Ignore any timeouts that are posted after an operation has completed */
            break;

        default:
            GLADOS_STATE_CHECK_PARENT(&DataAO_state);
            break;
    }

    return;
}


void DataAO_state_update_cmd_success(GLADOS_AO_t *this_ao, GLADOS_Event_t *event)
{
    DEV_ASSERT(this_ao);
    DEV_ASSERT(event);

    switch(event->name)
    {
        /* GLADOS_ENTRY/GLADOS_EXIT optional event for each state */
        case GLADOS_ENTRY:
            MaxData.data_mode = DATA_UP_CMD_SUCCESS;
            GPIO_SetLEDs(0xFU);

            /* Place this here since it's possible that other things
             * could be busy when just sitting idle in SW_UPDATE */
            MaxData.max_busy_flag = false;
            break;

        default:
            GLADOS_STATE_CHECK_PARENT(&DataAO_state_update_cmd);
            break;
    }

    return;
}


void DataAO_state_update_cmd_fail(GLADOS_AO_t *this_ao, GLADOS_Event_t *event)
{
    DEV_ASSERT(this_ao);
    DEV_ASSERT(event);

    switch(event->name)
    {
        /* GLADOS_ENTRY/GLADOS_EXIT optional event for each state */
        case GLADOS_ENTRY:
            MaxData.data_mode = DATA_UP_CMD_FAIL;
            GPIO_SetOutput(GPIO_LED2, 1U);

            /* Place this here since it's possible that other things
             * could be busy when just sitting idle in SW_UPDATE */
            MaxData.max_busy_flag = false;

            /* Note: The error that occurred leading into this state
             * has already been logged */
            break;

        default:
            GLADOS_STATE_CHECK_PARENT(&DataAO_state_update_cmd);
            break;
    }

    return;
}


void DataAO_state_preload(GLADOS_AO_t *this_ao, GLADOS_Event_t *event)
{
    DEV_ASSERT(this_ao);
    DEV_ASSERT(event);

    status_t status;

    switch(event->name)
    {
        /* GLADOS_ENTRY/GLADOS_EXIT optional event for each state */
        case GLADOS_ENTRY:
        case EVT_DATA_DMA_FRAM_DONE:
            GLADOS_Timer_Disable(&Tmr_DataTimeout);

            status = STATUS_SUCCESS;

            /* First time through, dao_cmd_fram_offset and dao_cmd_pflash_addr
             * have already been initialized with the command parameters.
             * Rest of the times through, these params are updated upon completion
             * of each transfer to FRAM. */

            if (dao_cmd_length == 0UL)
            {
                /* The preload transfer to FRAM has completed */
                GLADOS_AO_PUSH(&AO_Cmd, EVT_CMD_ACK_W_SEND);
                GLADOS_STATE_TRAN_TO_SIBLING(&DataAO_state_update_cmd);
            }
            else if (dao_cmd_length < UPDATE_MAX_FRAM_XFER)
            {
                /* Transfer the last data to FRAM */

                /* Initiate the non-blocking DMA transfer for data */
                spi1_callback_send_dma_done = false;
                status  = CY15_FRAM_SetWEL(LPSPICOM1_FRAM, LPSPI_PCS0, true);
                spi1_callback_send_dma_done = true;
                status |= CY15_FRAM_WriteBlock_NonBlocking(LPSPICOM1_FRAM, LPSPI_PCS0, dao_cmd_fram_offset,
                                                           dao_cmd_length, (uint8_t *)dao_cmd_pflash_addr);

                dao_cmd_length       = 0UL;
                dao_cmd_fram_offset += UPDATE_MAX_FRAM_XFER;
                dao_cmd_pflash_addr += UPDATE_MAX_FRAM_XFER;
            }
            else
            {
                /* Transfer the next block of data to FRAM */

                /* Initiate the non-blocking DMA transfer for data */
                spi1_callback_send_dma_done = false;
                status  = CY15_FRAM_SetWEL(LPSPICOM1_FRAM, LPSPI_PCS0, true);
                spi1_callback_send_dma_done = true;
                status |= CY15_FRAM_WriteBlock_NonBlocking(LPSPICOM1_FRAM, LPSPI_PCS0, dao_cmd_fram_offset,
                                                           UPDATE_MAX_FRAM_XFER, (uint8_t *)dao_cmd_pflash_addr);

                dao_cmd_length      -= UPDATE_MAX_FRAM_XFER;
                dao_cmd_fram_offset += UPDATE_MAX_FRAM_XFER;
                dao_cmd_pflash_addr += UPDATE_MAX_FRAM_XFER;
            }

            if (status != STATUS_SUCCESS)
            {
                report_error_uint(&AO_Data, Errors.DataaoFramWriteFail, DO_NOT_SEND_TO_SPACECRAFT, dao_cmd_fram_offset);
            }

            GLADOS_Timer_Set(&Tmr_DataTimeout, PT->tmr_fram_xfer_timeout_us);
            GLADOS_Timer_EnableOnce(&Tmr_DataTimeout);
            break;

        case GLADOS_EXIT:
            /* Halt posting of the EVT_DATA_DMA_FRAM_DONE event and disable timer */
            spi1_callback_send_dma_done = false;
            GLADOS_Timer_Disable(&Tmr_DataTimeout);
            break;

        case EVT_TMR_DATA_TIMEOUT:
            /* Abort the transfer, send NACK, and record error */
            CY15_FRAM_AbortTransfer(LPSPICOM1_FRAM);
            CmdAO_push_nack_w_send_err(this_ao, create_error_uint(Errors.EvtDataOpTimeout, Tmr_DataTimeout.duration_us));
            GLADOS_STATE_TRAN_TO_SIBLING(&DataAO_state_update_cmd);
            break;

        case EVT_MAX_DATA_ABORT:
            /* Handle this event specifically (rather than leaving it to the parent state)
             * since the NACK must be returned. This logs an error as well, which may double
             * up the error but it at least indicates that a command was cancelled due to
             * a FDIR safe-ing mechanism. */
            CmdAO_push_nack_w_send_err(this_ao, create_error_uint(Errors.CmdDataAbortDueToSafe, 0UL));
            CY15_FRAM_AbortTransfer(LPSPICOM1_FRAM);
            GLADOS_STATE_TRAN_TO_SIBLING(&DataAO_state_lounge);
            break;

        default:
            GLADOS_STATE_CHECK_PARENT(&DataAO_state);
            break;
    }

    return;
}


void DataAO_state_stage(GLADOS_AO_t *this_ao, GLADOS_Event_t *event)
{
    DEV_ASSERT(this_ao);
    DEV_ASSERT(event);

    status_t status;

    switch(event->name)
    {
        /* GLADOS_ENTRY/GLADOS_EXIT optional event for each state */
        case GLADOS_ENTRY:
            GPIO_ToggleOutput(GPIO_LED3);

            /* Initiate the non-blocking DMA transfer for data */
            spi1_callback_send_dma_done = false;
            status  = CY15_FRAM_SetWEL(LPSPICOM1_FRAM, LPSPI_PCS0, true);
            spi1_callback_send_dma_done = true;
            status |= CY15_FRAM_WriteBlock_NonBlocking(LPSPICOM1_FRAM, LPSPI_PCS0, dao_cmd_fram_offset,
                                                       dao_cmd_length, (uint8_t *)dao_cmd_data_ptr);

            if (status != STATUS_SUCCESS)
            {
                report_error_uint(&AO_Data, Errors.DataaoFramWriteFail, DO_NOT_SEND_TO_SPACECRAFT, dao_cmd_fram_offset);
            }

            GLADOS_Timer_Set(&Tmr_DataTimeout, PT->tmr_fram_xfer_timeout_us);
            GLADOS_Timer_EnableOnce(&Tmr_DataTimeout);
            break;

        case GLADOS_EXIT:
            GPIO_SetOutput(GPIO_LED3, 1U);

            /* Halt posting of the EVT_DATA_DMA_FRAM_DONE event and disable timer */
            spi1_callback_send_dma_done = false;
            GLADOS_Timer_Disable(&Tmr_DataTimeout);
            break;

        case EVT_DATA_DMA_FRAM_DONE:
            GLADOS_Timer_Disable(&Tmr_DataTimeout);
            dao_cmd_length = 0UL;
            GLADOS_AO_PUSH(&AO_Cmd, EVT_CMD_ACK_W_SEND);
            GLADOS_STATE_TRAN_TO_SIBLING(&DataAO_state_update_cmd);
            break;

        case EVT_TMR_DATA_TIMEOUT:
            /* Abort the transfer, send NACK */
            CY15_FRAM_AbortTransfer(LPSPICOM1_FRAM);
            CmdAO_push_nack_w_send_err(this_ao, create_error_uint(Errors.EvtDataOpTimeout, Tmr_DataTimeout.duration_us));
            GLADOS_STATE_TRAN_TO_SIBLING(&DataAO_state_update_cmd);
            break;

        case EVT_MAX_DATA_ABORT:
            /* Handle this event specifically (rather than leaving it to the parent state)
             * since the NACK must be returned. This logs an error as well, which may double
             * up the error but it at least indicates that a command was cancelled due to
             * a FDIR safe-ing mechanism. */
            CmdAO_push_nack_w_send_err(this_ao, create_error_uint(Errors.CmdDataAbortDueToSafe, 0UL));
            CY15_FRAM_AbortTransfer(LPSPICOM1_FRAM);
            GLADOS_STATE_TRAN_TO_SIBLING(&DataAO_state_lounge);
            break;

        default:
            GLADOS_STATE_CHECK_PARENT(&DataAO_state);
            break;
    }

    return;
}


void DataAO_state_fram_validate(GLADOS_AO_t *this_ao, GLADOS_Event_t *event)
{
    DEV_ASSERT(this_ao);
    DEV_ASSERT(event);

    static uint32_t curr_crc;
    static uint8_t *curr_data_ptr;
    static uint32_t curr_length;
    static uint32_t curr_fram_offset;
    static bool has_sent_ack_mutex;

    status_t status;
    App_Header_Rec_t *hdr;

    switch(event->name)
    {
        /* GLADOS_ENTRY/GLADOS_EXIT optional event for each state */
        case GLADOS_ENTRY:
            has_sent_ack_mutex = false;
            GPIO_SetOutput(GPIO_LED1, 1U);
            GPIO_SetOutput(GPIO_LED2, 0U);
            GPIO_SetOutput(GPIO_LED3, 0U);

            /* Restart the CRC */
            curr_crc = 0xFFFFFFFFUL;

            /* Use another variable to hold the current length since the
             * dao_cmd_length should remain static to retain the initial
             * program command parameters */
            curr_length = dao_cmd_length;

            /* The start of staging begins at FRAM BANK1, so start there */
            curr_fram_offset = FRAM_ADDR_BANK1_START;

            /*  Start by reading the header. Initiate the non-blocking DMA transfer for data. */
            spi1_callback_send_dma_done = true;
            status = CY15_FRAM_ReadBlock_NonBlocking(LPSPICOM1_FRAM, LPSPI_PCS0, curr_fram_offset,
                                                     APP_HDR_MAX_SIZE, &curr_data_ptr);
            if (status != STATUS_SUCCESS)
            {
                report_error_uint(&AO_Data, Errors.DataaoFramReadFail, DO_NOT_SEND_TO_SPACECRAFT, curr_fram_offset);
            }

            GLADOS_Timer_Set(&Tmr_DataTimeout, PT->tmr_fram_xfer_timeout_us);
            GLADOS_Timer_EnableOnce(&Tmr_DataTimeout);
            break;

        case GLADOS_EXIT:
            /* Halt posting of the EVT_DATA_DMA_FRAM_DONE event and disable timer */
            spi1_callback_send_dma_done = false;
            GLADOS_Timer_Disable(&Tmr_DataTimeout);
            break;

        case EVT_DATA_DMA_FRAM_DONE:
            GLADOS_Timer_Disable(&Tmr_DataTimeout);

            /* If it's the first read, then it contains the flash header */
            if (curr_fram_offset == FRAM_ADDR_BANK1_START)
            {
                /* Grab the CRC and length within the header and validate that it matches what is expected */
                hdr = (App_Header_Rec_t *)curr_data_ptr;

                if (!sw_update_hdr_valid(hdr, dao_cmd_pflash_addr, dao_cmd_length, dao_cmd_crc))
                {
                    /* If CRC or length mismatch what is expected, then fail out */
                    CmdAO_push_nack_w_send_err(this_ao, create_error_uint(Errors.CmdUpdateProgramStageInvalid, hdr->app_record.img_crc));
                    has_sent_ack_mutex = true;
                    GLADOS_STATE_TRAN_TO_SIBLING(&DataAO_state_update_cmd);
                    /* Note: Do not go to the FAIL state since a NACK is returned,
                     * indicating that the command was rejected. Technically the
                     * SW update only starts after successfully validating that the
                     * header CRC and length match */
                }
                else
                {
                    /* Success! Transition to the next update step */
                    GLADOS_AO_PUSH(&AO_Cmd, EVT_CMD_ACK_W_SEND);
                    has_sent_ack_mutex = true;
                    MaxData.max_busy_flag = true;

                    /* The first read was of the header, only increment by that */
                    curr_length      -= APP_HDR_MAX_SIZE;
                    curr_fram_offset += APP_HDR_MAX_SIZE;

                    /* CRC is not performed on the header, read next block from FRAM.
                     * Note: Reading beyond the last address of FRAM rolls over to the beginning,
                     * which works for this implementation since all reads from FRAM can be read
                     * using the same block size without special consideration for the last read,
                     * which may only be a couple of bytes. The dao_cmd_length is used in a way
                     * that will not include this extra rollover data that may have been read. */
                    spi1_callback_send_dma_done = true;
                    status = CY15_FRAM_ReadBlock_NonBlocking(LPSPICOM1_FRAM, LPSPI_PCS0, curr_fram_offset,
                                                             UPDATE_MAX_FRAM_XFER, &curr_data_ptr);
                    if (status != STATUS_SUCCESS)
                    {
                        /* This is an internal error may be useful. This failure will ultimately
                         * lead to an failed CRC computation and SC-visible error logged. */
                        report_error_uint(&AO_Data, Errors.DataaoFramReadFail, DO_NOT_SEND_TO_SPACECRAFT, curr_fram_offset);
                    }

                    GLADOS_Timer_Set(&Tmr_DataTimeout, PT->tmr_fram_xfer_timeout_us);
                    GLADOS_Timer_EnableOnce(&Tmr_DataTimeout);
                }
            }
            /* If the previous read was the last one */
            else if (curr_length < UPDATE_MAX_FRAM_XFER)
            {
                /* Only accumulate the CRC for the last of the data. This function
                 * handles the case when curr_length is zero. */
                curr_crc = crc32_compute(curr_crc, curr_data_ptr, curr_length, true);

                /* Check the CRC and transition to the appropriate state */
                if (curr_crc != dao_cmd_crc)
                {
                    /* Command has already been ACKd, now it's on the SW to report errors to the SC */
                    report_error_uint(&AO_Data, Errors.DataaoUpProgStagedCrcFail, SEND_TO_SPACECRAFT, curr_crc);

                    /* Failed CRC, return to base camp */
                    GLADOS_STATE_TRAN_TO_SIBLING(&DataAO_state_update_cmd);
                    GLADOS_STATE_TRAN_TO_CHILD(&DataAO_state_update_cmd_fail);
                }
                else
                {
                    GLADOS_STATE_TRAN_TO_SIBLING(&DataAO_state_pflash_erase);
                }
            }
            else
            {
                /* Continue to accumulate the CRC for the full block read from FRAM */
                curr_crc = crc32_compute(curr_crc, curr_data_ptr, UPDATE_MAX_FRAM_XFER, false);

                curr_length      -= UPDATE_MAX_FRAM_XFER;
                curr_fram_offset += UPDATE_MAX_FRAM_XFER;

                /* Initiate the next full block read from FRAM
                 * Note: Reading beyond the last address of FRAM rolls over to the beginning,
                 * which works for this implementation since all reads from FRAM can be read
                 * using the same block size without special consideration for the last read,
                 * which may only be a couple of bytes. The dao_cmd_length is used in a way
                 * that will not include this extra rollover data that may have been read. */
                spi1_callback_send_dma_done = true;
                status = CY15_FRAM_ReadBlock_NonBlocking(LPSPICOM1_FRAM, LPSPI_PCS0, curr_fram_offset,
                                                         UPDATE_MAX_FRAM_XFER, &curr_data_ptr);
                if (status != STATUS_SUCCESS)
                {
                    /* This is an internal error may be useful. This failure will ultimately
                     * lead to an failed CRC computation and SC-visible error logged. */
                    report_error_uint(&AO_Data, Errors.DataaoFramReadFail, DO_NOT_SEND_TO_SPACECRAFT, curr_fram_offset);
                }

                GLADOS_Timer_Set(&Tmr_DataTimeout, PT->tmr_fram_xfer_timeout_us);
                GLADOS_Timer_EnableOnce(&Tmr_DataTimeout);
            }
            break;

        case EVT_TMR_DATA_TIMEOUT:
            /* Note: There is a race condition where it's possible that the DMA transfer
             * completes just before this event is pushed to the AO. Which would initiate
             * the next transfer but the very next event will be this timeout, causing the
             * exiting of this state prematurely. This is not ideal, but still okay since the
             * DMA transfer should never be anywhere near the timeout time. And if it does,
             * then it deserves to be aborted.
             *
             * On that note, this race condition could potentially affect other states that
             * might be transitioned to as a result of the last DMA transfer completing. The
             * current transitions from this state to others are accounted for (i.e. to
             * update_cmd and pflash_erase) and don't use the EVT_TMR_DATA_TIMEOUT event.
             * However any new states that may be transitioned to from this state must take
             * this into account. However that is true for any AO design that uses a timeout
             * paradigm. But just restating here for clarity. */

            /* Abort transfer, send NACK, and record error */
            CY15_FRAM_AbortTransfer(LPSPICOM1_FRAM);

            /* This state handles sending the ACK/NACK to the SC, however don't double-send
             * if it has already been done, record an error otherwise. */
            if (!has_sent_ack_mutex)
            {
                CmdAO_push_nack_w_send_err(this_ao, create_error_uint(Errors.DataaoUpProgStagedCrcTimeout, curr_fram_offset));
                has_sent_ack_mutex = true;
            }
            else
            {
                /* This is an internal error may be useful. This failure will ultimately
                 * lead to an failed CRC computation and SC-visible error logged. */
                report_error_uint(&AO_Data, Errors.DataaoUpProgStagedCrcTimeout, SEND_TO_SPACECRAFT, curr_fram_offset);
            }

            GLADOS_STATE_TRAN_TO_SIBLING(&DataAO_state_update_cmd);
            GLADOS_STATE_TRAN_TO_CHILD(&DataAO_state_update_cmd_fail);
            break;

        case EVT_MAX_DATA_ABORT:
            /* Handle this event specifically (rather than leaving it to the parent state)
             * since the NACK may need to be sent. This logs an error as well, which may double
             * up the error but it at least indicates that a command was cancelled due to
             * a FDIR safe-ing mechanism. */
            CY15_FRAM_AbortTransfer(LPSPICOM1_FRAM);

            /* This state handles sending the ACK/NACK to the SC, however don't double-send
             * if it has already been done, record an error otherwise. */
            if (!has_sent_ack_mutex)
            {
                CmdAO_push_nack_w_send_err(this_ao, create_error_uint(Errors.CmdDataAbortDueToSafe, 0UL));
                has_sent_ack_mutex = true;
            }
            else
            {
                report_error_uint(&AO_Data, Errors.CmdDataAbortDueToSafe, SEND_TO_SPACECRAFT, 0UL);
            }

            GLADOS_STATE_TRAN_TO_SIBLING(&DataAO_state_lounge);
            break;

        default:
            GLADOS_STATE_CHECK_PARENT(&DataAO_state);
            break;
    }

    return;
}


void DataAO_state_pflash_erase(GLADOS_AO_t *this_ao, GLADOS_Event_t *event)
{
    DEV_ASSERT(this_ao);
    DEV_ASSERT(event);

    static uint32_t curr_pflash_addr;
    static uint32_t end_pflash_addr;

    status_t status;

    switch(event->name)
    {
        /* GLADOS_ENTRY/GLADOS_EXIT optional event for each state */
        case GLADOS_ENTRY:
            MaxData.data_mode = DATA_UP_FLSH_CLR;

            /* Note: At this stage, the address and length parameters have
             * already been validated for appropriate ranges and byte
             * boundaries so assert checking ones pertinent to this state
             * is sufficient. */
            DEV_ASSERT((dao_cmd_pflash_addr & (ERASE_SECTOR_SIZE - 1)) == 0UL);

            /* Use another variable to hold the current address since the
             * dao_cmd_pflash_addr should remain static to retain the initial
             * program command parameters */
            curr_pflash_addr = dao_cmd_pflash_addr;

            /* Get end address, rounding up to the next PFlash sector */
            end_pflash_addr  = dao_cmd_pflash_addr + dao_cmd_length + (ERASE_SECTOR_SIZE - 1UL);
            end_pflash_addr &= ~(ERASE_SECTOR_SIZE - 1UL);

            /* Manually push the first event to self to get into the erasing, which
             * removes the need to handle the same actions in multiple places */
            GLADOS_AO_PUSH(&AO_Data, EVT_TMR_DATA_AUX_DONE);
            break;

        case GLADOS_EXIT:
            GLADOS_Timer_Disable(&Tmr_DataAux);
            break;

        case EVT_TMR_DATA_AUX_DONE:
            /* Check if all sectors of the pflash region have been erased */
            if (curr_pflash_addr >= end_pflash_addr)
            {
                /* Once complete, move on to programming the image */
                GLADOS_STATE_TRAN_TO_SIBLING(&DataAO_state_pflash_program);
            }
            else
            {
                /* Erase the next PFlash sector
                 * Note: There are some instances where the erase operation returns busy
                 * immediately when there is nothing left to erase. If there is something
                 * to erase, then it returns STATUS_SUCCESS. Given this behavior, we only
                 * STATUS_ERROR is the only status left, and even that is just rechecking
                 * the inputs into the function, which already happened upon entry to start
                 * the program. For this reason, check STATUS_ERROR for development only. */
                status = erase_flash_sector((uint32_t )curr_pflash_addr, ERASE_SECTOR_SIZE);
                DEV_ASSERT(status != STATUS_ERROR);
                (void)status;  /* Avoids compiler warning */

                curr_pflash_addr += ERASE_SECTOR_SIZE;

                /* Wait a fixed set of time for the PFlash sector to erase */
                GLADOS_Timer_Set(&Tmr_DataAux, PT->tmr_pflash_sector_erase_us);
                GLADOS_Timer_EnableOnce(&Tmr_DataAux);
            }
            break;

        default:
            GLADOS_STATE_CHECK_PARENT(&DataAO_state);
            break;
    }

    return;
}


void DataAO_state_pflash_program(GLADOS_AO_t *this_ao, GLADOS_Event_t *event)
{
    DEV_ASSERT(this_ao);
    DEV_ASSERT(event);

    static uint32_t curr_prog_pflash_addr;
    static uint32_t curr_prog_length;
    static uint32_t curr_prog_fram_offset;
    static uint8_t *curr_prog_data_ptr;

    status_t status;

    switch(event->name)
    {
        /* GLADOS_ENTRY/GLADOS_EXIT optional event for each state */
        case GLADOS_ENTRY:
            MaxData.data_mode = DATA_UP_FLSH_PRG;

            curr_prog_pflash_addr = dao_cmd_pflash_addr;
            curr_prog_length      = dao_cmd_length;
            curr_prog_fram_offset = FRAM_ADDR_BANK1_START;

            /* Manually push the first event to self to get into the FRAM read, which
             * removes the need to handle the same actions in multiple places */
            GLADOS_AO_PUSH(&AO_Data, EVT_TMR_DATA_AUX_DONE);
            break;

        case GLADOS_EXIT:
            spi1_callback_send_dma_done = false;
            GLADOS_Timer_Disable(&Tmr_DataTimeout);
            GLADOS_Timer_Disable(&Tmr_DataAux);
            break;

        case EVT_TMR_DATA_TIMEOUT:
            CY15_FRAM_AbortTransfer(LPSPICOM1_FRAM);
            report_error_uint(&AO_Data, Errors.DataaoUpProgFlashTimeout, SEND_TO_SPACECRAFT, curr_prog_fram_offset);
            GLADOS_STATE_TRAN_TO_SIBLING(&DataAO_state_update_cmd);
            GLADOS_STATE_TRAN_TO_CHILD(&DataAO_state_update_cmd_fail);
            break;

        case EVT_TMR_DATA_AUX_DONE:
            if (curr_prog_length == 0UL)
            {
                /* Programming has completed, go to CRC image validation step */
                GLADOS_STATE_TRAN_TO_SIBLING(&DataAO_state_pflash_validate);
            }
            else
            {
                /* Initiate the next full block read from FRAM
                 * Note: Reading beyond the last address of FRAM rolls over to the beginning,
                 * which works for this implementation since all reads from FRAM can be read
                 * using the same block size without special consideration for the last read,
                 * which may only be a couple of bytes. The dao_cmd_length is used in a way
                 * that will not include this extra rollover data that may have been read. */
                spi1_callback_send_dma_done = true;
                status = CY15_FRAM_ReadBlock_NonBlocking(LPSPICOM1_FRAM, LPSPI_PCS0, curr_prog_fram_offset,
                                                         UPDATE_MAX_FRAM_XFER, &curr_prog_data_ptr);
                if (status != STATUS_SUCCESS)
                {
                    /* This is an internal error may be useful. This failure will ultimately
                     * lead to a timeout and SC-visible error logged. */
                    report_error_uint(&AO_Data, Errors.DataaoFramReadFail, DO_NOT_SEND_TO_SPACECRAFT, curr_prog_fram_offset);
                }

                GLADOS_Timer_Set(&Tmr_DataTimeout, PT->tmr_fram_xfer_timeout_us);
                GLADOS_Timer_EnableOnce(&Tmr_DataTimeout);
            }
            break;

        case EVT_DATA_DMA_FRAM_DONE:
            GLADOS_Timer_Disable(&Tmr_DataTimeout);
            if (curr_prog_length <= UPDATE_MAX_FRAM_XFER)
            {
                /* Round up to the nearest 8 bytes since programming flash must occur in
                 * multiples of 8 bytes. Writing unused junk data at the end of the image
                 * never hurt nobody. */
                curr_prog_length += 0x7;
                curr_prog_length &= ~0x7;

                /* This is the last program
                 * Note: Programming PFlash is a blocking call. No need to check if program
                 * was successful since this is done later in the final CRC check. */
                status = write_flash_ao(&AO_Data, (uint32_t )curr_prog_pflash_addr, curr_prog_length, curr_prog_data_ptr);

                curr_prog_pflash_addr += curr_prog_length;
                curr_prog_fram_offset += curr_prog_length;
                curr_prog_length       = 0UL;
            }
            else
            {
                /* This is the last program
                 * Note: Programming PFlash is a blocking call. No need to check if program
                 * was successful since this is done later in the final CRC check. */
                status = write_flash_ao(&AO_Data, (uint32_t )curr_prog_pflash_addr, UPDATE_MAX_FRAM_XFER, curr_prog_data_ptr);

                curr_prog_pflash_addr += UPDATE_MAX_FRAM_XFER;
                curr_prog_fram_offset += UPDATE_MAX_FRAM_XFER;
                curr_prog_length      -= UPDATE_MAX_FRAM_XFER;
            }

            if (status != STATUS_SUCCESS)
            {
                report_error_uint(&AO_Data, Errors.DataaoUpProgFlashFail, SEND_TO_SPACECRAFT, curr_prog_pflash_addr);
                GLADOS_STATE_TRAN_TO_SIBLING(&DataAO_state_update_cmd);
                GLADOS_STATE_TRAN_TO_CHILD(&DataAO_state_update_cmd_fail);
            }
            else
            {
                /* This is delay out of courtesy, allowing other AOs to execute */
                GLADOS_Timer_Set(&Tmr_DataAux, PT->tmr_pflash_prog_delay_us);
                GLADOS_Timer_EnableOnce(&Tmr_DataAux);
            }
            break;

        default:
            GLADOS_STATE_CHECK_PARENT(&DataAO_state);
            break;
    }

    return;
}


void DataAO_state_pflash_validate(GLADOS_AO_t *this_ao, GLADOS_Event_t *event)
{
    DEV_ASSERT(this_ao);
    DEV_ASSERT(event);

    static uint32_t curr_crc;
    static uint32_t crc_length1;
    static uint32_t crc_length2;

    uint32_t curr_pflash_addr;
    App_Header_Rec_t *hdr;

    switch(event->name)
    {
        /* GLADOS_ENTRY/GLADOS_EXIT optional event for each state */
        case GLADOS_ENTRY:
            /* Expect to spend 10-20ms in this state. */

            /* Note: At this stage, the address and length parameters have
             * already been validated for appropriate ranges and byte
             * boundaries so assert checking ones pertinent to this state
             * is sufficient. */
            /* Validate length since we don't want to wait forever for the CRC calc to finish */
            DEV_ASSERT(dao_cmd_length > APP_HDR_MAX_SIZE);
            DEV_ASSERT(dao_cmd_length <= UPDATE_MAX_LOAD_SIZE);
            DEV_ASSERT((dao_cmd_length % 4U) == 0U);

            /* Make the CRC for the first section roughly half. Since the CRC32 implementation
             * requires the length to be divisible by four, we round up to the nearest number
             * divisible by 4. */
            crc_length1 = (dao_cmd_length - APP_HDR_MAX_SIZE) / 2UL;
            crc_length1 = (crc_length1 + 3UL) & (~0x3UL);
            crc_length2 = (dao_cmd_length - APP_HDR_MAX_SIZE) - crc_length1;

            /* CRC does not include the Application header, so skip */
            curr_pflash_addr = dao_cmd_pflash_addr + APP_HDR_MAX_SIZE;

            curr_crc = crc32_compute(0xFFFFFFFFUL, (uint8_t *)curr_pflash_addr, crc_length1, false);

            /* This is delay out of courtesy, allowing other AOs to execute */
            GLADOS_Timer_Set(&Tmr_DataAux, PT->tmr_pflash_prog_delay_us);
            GLADOS_Timer_EnableOnce(&Tmr_DataAux);
            break;

        case GLADOS_EXIT:
            break;

        case EVT_TMR_DATA_AUX_DONE:
            curr_pflash_addr = dao_cmd_pflash_addr + APP_HDR_MAX_SIZE + crc_length1;

            curr_crc = crc32_compute(curr_crc, (uint8_t *)curr_pflash_addr, crc_length2, true);

            /* Grab the CRC and length within the header and validate that it matches what is expected */
            hdr = (App_Header_Rec_t *)dao_cmd_pflash_addr;

            /* Validate the the CRC matches what was command, as well as the header info */
            if ((curr_crc                   != dao_cmd_crc) ||
                (hdr->app_record.img_crc    != dao_cmd_crc) ||
                (hdr->app_record.img_length != (dao_cmd_length - APP_HDR_MAX_SIZE)))
            {
                /* Doh! After all that, the upload failed */
                report_error_uint(&AO_Data, Errors.DataaoUpProgFinalCrcFail, SEND_TO_SPACECRAFT, curr_crc);
                GLADOS_STATE_TRAN_TO_SIBLING(&DataAO_state_update_cmd);
                GLADOS_STATE_TRAN_TO_CHILD(&DataAO_state_update_cmd_fail);
            }
            else
            {
                /* Upload has completed! Wow it really worked */
                GLADOS_STATE_TRAN_TO_SIBLING(&DataAO_state_update_cmd);
                GLADOS_STATE_TRAN_TO_CHILD(&DataAO_state_update_cmd_success);
            }

            break;

        default:
            GLADOS_STATE_CHECK_PARENT(&DataAO_state);
            break;
    }

    return;
}


void DataAO_SetRecord_Fram(bool enable)
{
    MaxData.rec_fram_en = enable;
    return;
}

void DataAO_SetRecord_Nand(bool enable)
{
    MaxData.rec_nand_en = enable;
    return;
}


void DataAO_SetRecord_Bank(uint8_t bank_num)
{
    /* Do not disturb the current bank setting since it is
     * possible it could be in use. Bank adjustments latched
     * upon entry into the next fresh recording event */
    dao_next_bank = bank_num & 0x1U;
    return;
}

void DataAO_DumpTlmRequest(GLADOS_AO_t *ao_src,
                           uint8_t port,
                           uint8_t seq_id,
                           uint8_t src_id,
                           uint16_t msg_id,
                           uint8_t mem_type,
                           uint8_t bank)
{
    GLADOS_Event_t evt;

    DataAO_DumpTlm_RqstData_t dumptlm_rqst =
    {
        .port   = port,
        .seq_id = seq_id,
        .src_id = src_id,
        .msg_id = msg_id,
        .mem_type = mem_type,
        .bank = bank
    };

    GLADOS_Event_New_Data(&evt, EVT_CMD_DUMP_TLM, (uint8_t *)&dumptlm_rqst, sizeof(DataAO_DumpTlm_RqstData_t));
    GLADOS_AO_PushEvt(ao_src, &AO_Data, &evt);

    return;
}


/* NOTE: User MUST ensure that no other FRAM SPI operations are currently in progress
 * prior to calling this function, or else it will end its transfer. */
void fram_metadata_write(uint8_t bank, uint8_t meta1_8bit, uint32_t meta2_24bit)
{
    enum Constants { METADATA_SIZE = 4U };

    status_t status;
    uint8_t write_buf[METADATA_SIZE] = {0U};
    uint32_t addr = (bank == BANK0) ? FRAM_ADDR_BANK0_END : FRAM_ADDR_BANK1_END;

    /* Only perform the read if no other non-blocking DMA SPI transfers to FRAM are in progress */
    if (spi1_callback_send_dma_done == false)
    {
        write_buf[0] = meta1_8bit;
        write_buf[1] = (uint8_t)((meta2_24bit >> 16U) & 0xFFUL);
        write_buf[2] = (uint8_t)((meta2_24bit >> 8U)  & 0xFFUL);
        write_buf[3] = (uint8_t)((meta2_24bit)        & 0xFFUL);

        status  = CY15_FRAM_SetWEL(LPSPICOM1_FRAM, LPSPI_PCS0, true);
        status |= CY15_FRAM_WriteBlock(LPSPICOM1_FRAM, LPSPI_PCS0, addr, 4U, &write_buf[0]);

        /* Assume that the write was successful, which is okay since having this metadata is
         * a non-critical function of the software. Cast to void to avoid compiler warning.
         * NOTE: If hitting this with STATUS_BUSY, the reigning theory is that the DMA is still
         * performing an operation (e.g. recording) that is preventing this from running since
         * the DMA operates in parallel and may not have completed.  Also initiating a DMA SPI
         * abort may have associated delays that may get caught here, so initiating a DMA SPI
         * abort immediately prior to calling this function does not work well. */
        DEV_ASSERT(status == STATUS_SUCCESS);
        (void)status;
    }
    else
    {
        /* Log a busy FRAM error for internal purposes */
        report_error_uint(&AO_Data, Errors.DataaoFramMetadataWriteFail, DO_NOT_SEND_TO_SPACECRAFT, 0UL);
    }

    return;
}


/* NOTE: User MUST ensure that no other FRAM SPI operations are currently in progress
 * prior to calling this function, or else it will end its transfer. */
void fram_metadata_read(uint8_t bank, uint8_t *meta1_8bit, uint32_t *meta2_24bit)
{
    enum Constants { METADATA_SIZE = 4U };

    status_t status;
    uint8_t *fram_metadata = NULL;
    uint32_t addr = (bank == BANK0) ? FRAM_ADDR_BANK0_END : FRAM_ADDR_BANK1_END;

    /* Only perform the read if no other non-blocking DMA SPI transfers to FRAM
     * are in progress */
    if (spi1_callback_send_dma_done == false)
    {
        /* Perform a blocking SPI read. */
        status = CY15_FRAM_ReadBlock(LPSPICOM1_FRAM, LPSPI_PCS0, addr, 4U, &fram_metadata);

        /* Only update the values if reading the data was successful */
        if (status == STATUS_SUCCESS)
        {
            *meta1_8bit = fram_metadata[0];
            *meta2_24bit = ((uint32_t)fram_metadata[1] << 16U) +
                           ((uint32_t)fram_metadata[2] << 8U)  +
                           ((uint32_t)fram_metadata[3]);
        }
    }

    return;
}


/* This function handles the data recording algorithm for the NAND Flash and determines
 * the next NAND address for data recording to use given the current major/minor loop
 * iterations and the bad block map.
 *
 * The idea behind the algorithm is to reduce NAND wear while also serving as useful
 * long-term storage that permits debugging for long-term thrust events (>20 minutes).
 *
 * Next NAND Flash address algorithm:
 *  - Increment by the TLM size (512) to the next TLM address.
 *
 *  - If the address falls into a new nand block, things get interesting...
 *     - Check current block for validity in the bad block map and get the next
 *       valid block address if invalid.
 *
 *       In most cases, the next valid address is found and the current address
 *       is updated and the algorithm is done here. The checks below handle other
 *       conditions...
 *
 *     - If a valid block is found and the address has rolled over, then
 *        - Increment nand cycle count
 *        - Increment the minor iteration count
 *        - If minor iteration count is > 511
 *           - Increment major iteration count
 *           - Set minor iteration equal to the major iteration
 *        - Update nand area start to BANKx_START + ((minor iteration + 1) * Block size))
 *          Note: We add 1 to the minor iteration since it will be the next start address
 *                upon rollover.
 *
 * *    - If no valid block is found
 *         - Increment the major iteration count
 *         - Set the minor iteration to the major iteration count
 *         - Update nand area start to BANKx_START + ((minor iteration + 1) * Block size))
 * *         Note: We add 1 to the minor iteration since it will be the "next" start address
 *                 upon rollover.
 *
 *      - If cycle count is greater than PT->max_nand_cycle_cnt
 *         - Halt nand recording
 *         - Record error
 *
 * Note: If a bad block exists at the start of a minor loop, the algorithm assumes that it
 *       is writable even if it is not actually written to (because bad_block_get_next_valid_addr
 *       handles this). This behavior is admissible to keep the algorithm... simple(r), but
 *       means that the recorded snapshot for the iteration may not be preserved upon the next
 *       minor/major loop.
 */

static void get_next_nand_addr(NandAddr_t addr_type)
{
    bool valid_block_found;
    uint32_t prev_addr;

    if (addr_type == NAND_BLOCK)
    {
        /* Get the next nand address block.
         * Ensure that it falls on a block boundary. */
        MaxData.rec_curr_nand_addr += TC58_BLOCK_SIZE;
        MaxData.rec_curr_nand_addr &= ~(TC58_BLOCK_SIZE - 1UL);
    }
    else
    {
        /* Getting the next nand address to record to is simple enough.
         * Ensure that it falls on a 512-byte boundary. */
        MaxData.rec_curr_nand_addr += NAND_REC_MAX_PKT_SIZE;
        MaxData.rec_curr_nand_addr &= ~(NAND_REC_MAX_PKT_SIZE - 1UL);
    }

    /* If the new address falls into a new NAND block, perform the recording algorithm.
     * Note: A better defensive programming strategy would be to check for a rollover of
     * the block rather than on the boundary. However this is easier, faster and is safe as
     * long as incrementing the nand address guarantees it to eventually hit the boundary */
    if (TC58_ON_BLOCK_BOUNDARY(MaxData.rec_curr_nand_addr))
    {
        prev_addr = MaxData.rec_curr_nand_addr;

        /* Consult the bad block map for the next valid block in the NAND Flash chip */
        valid_block_found = bad_block_get_next_valid_addr(&MaxData.rec_curr_nand_addr,
                                                          dao_curr_nand_area_start,
                                                          dao_curr_nand_area_stop);

        /* If a valid block is found, check for address rollover
         * Note: If no valid block exists, then MaxData.rec_curr_nand_addr is equal to prev_addr */
        if (MaxData.rec_curr_nand_addr < prev_addr)
        {
            ++MaxData.rec_nand_cycle_cnt;
            ++dao_nand_minor_cnt;

            /* Check for major iteration loop completion */
            if (dao_nand_minor_cnt > (uint16_t)PT->nand_max_minor_cnt)
            {
                ++dao_nand_major_cnt;
                dao_nand_minor_cnt = dao_nand_major_cnt;
            }

            /* Since consulting the bad block map automatically selects the next valid block for
             * address rollovers off the end of the bank, the start address should be set to the
             * start address of the NEXT minor iteration. Otherwise the rollover will select the
             * bank of the current minor iteration that we are trying to preserve until the next
             * major iteration.  */
            dao_curr_nand_area_start = (NAND_ADDR_BANK0_START + (MaxData.rec_bank_sel * NAND_BANK_MAX_SIZE)) +
                                                                ((dao_nand_minor_cnt + 1UL) * TC58_BLOCK_SIZE);
        }

        /* If no valid block is found, increment to the major iteration to increase
         * the NAND Flash address range.
         * Note: In the edge case where no valid blocks exists in the entire NAND flash,
         * the major count increases until it hits the max cycle count before halting recording */
        if (!valid_block_found)
        {
            ++MaxData.rec_nand_cycle_cnt;
            ++dao_nand_major_cnt;
            dao_nand_minor_cnt = dao_nand_major_cnt;

            /* Since consulting the bad block map automatically selects the next valid block for
             * address rollovers off the end of the bank, the start address should be set to the
             * start address of the NEXT minor iteration. Otherwise the rollover will select the
             * bank of the current minor iteration that we are trying to preserve until the next
             * major iteration.  */
            dao_curr_nand_area_start = (NAND_ADDR_BANK0_START + (MaxData.rec_bank_sel * NAND_BANK_MAX_SIZE)) +
                                       ((dao_nand_minor_cnt + 1UL) * TC58_BLOCK_SIZE);
        }

        /* Check if the maximum number of cycles has been hit. This halts recording
         * to NAND in these instances:
         *  1) If too much wear (program/erase cycles) occur
         *  2) If bad block map indicates no blocks are available to continue recording
         */
        if (MaxData.rec_nand_cycle_cnt >= (uint16_t)PT->nand_max_cycle_cnt)
        {
            MaxData.rec_nand_en = false;
        }
    }
}


static bool sw_update_hdr_valid(App_Header_Rec_t *img_hdr, uint32_t exp_addr, uint32_t exp_length, uint32_t exp_crc)
{
    enum Constants { STR_MATCH = 0 };

    bool hdr_valid = false;

    /* Validate image CRC and length matches the expected */
    if ((img_hdr->app_record.img_crc    != exp_crc) ||
        (img_hdr->app_record.img_length != (exp_length - APP_HDR_MAX_SIZE)))
    {
        hdr_valid = false;
    }

    /* Validate the load is intended for the product, e.g. Max-1 vs Max-2 */
    else if (STR_MATCH != memcmp(&img_hdr->app_record.product_version_type[0],
                                 &app_hdr_rec->app_record.product_version_type[0],
                                 sizeof(img_hdr->app_record.product_version_type)))
    {
        hdr_valid = false;
    }

    /* Validate the load is intended for the hardware, e.g. Prop Ctrl vs Thr Ctrl */
    else if (STR_MATCH != memcmp(&img_hdr->app_record.hardware_version_type[0],
                                 &app_hdr_rec->app_record.hardware_version_type[0],
                                 sizeof(img_hdr->app_record.hardware_version_type)))
    {
        hdr_valid = false;
    }

    /* If load is to an Executable, it must match one of the proper addresses */
    else if (img_hdr->app_record.img_type == APP_IMG_TYPE_ID)
    {
        /* Only check for Golden Image if loading is enabled */
        if ((PT->pt_end == GOLD_IMG_LOAD_PWD) &&
            (exp_addr   == GOLDEN_IMAGE_FLASH_HDR_RECORD))
        {
            hdr_valid = true;
        }
        else if ((exp_addr == APP_1_FLASH_HDR_RECORD) ||
                 (exp_addr == APP_2_FLASH_HDR_RECORD))
        {
            hdr_valid = true;
        }
    }

    /* If load is to a Parameter table, it must match one of the proper addresses */
    else if (img_hdr->app_record.img_type == PARAM_TABLE_IMG_TYPE_ID)
    {
        /* Only check for Golden Image if loading is enabled */
        if ((PT->pt_end == GOLD_IMG_LOAD_PWD) &&
            (exp_addr   == PARM_TABLE_GOLD))
        {
            hdr_valid = true;
        }
        else if ((exp_addr == PARM_TABLE_APP1) ||
                 (exp_addr == PARM_TABLE_APP2))
        {
            hdr_valid = true;
        }
    }

    return hdr_valid;
}
