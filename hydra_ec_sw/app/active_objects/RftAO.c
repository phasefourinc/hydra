/* Copyright (c) 2017-2020 by Phase Four, Inc. All Rights Reserved. Confidential and Proprietary.
 *
 * RftAO.c
 */


#include "RftAO.h"
#include "mcu_types.h"
#include "glados.h"
#include "clank.h"
#include "p4_cmd.h"
#include "p4_tlm.h"
#include "max_data.h"
#include "rft_data.h"
#include "DaqCtlAO.h"
#include "CmdAO.h"
#include "TlmAO.h"
#include "TxAO.h"
#include "max_info_driver.h"
#include "default_pt.h"
#include "max_defs.h"
#include "string.h"
#include "math.h"

#define RFT_GET_HW_INFO_SIZE     (CLANK_HDR_SIZE + CLANK_CRC_SIZE + 2U + sizeof(Max_Hardware_Rec_t)) /* Includes 2-byte ACK_STATUS */
#define RFT_TLM_SIZE             (CLANK_HDR_SIZE + CLANK_CRC_SIZE + TLMAO_MAX_TLM_DATA)
#define RFT_GET_SW_INFO_SIZE     (CLANK_HDR_SIZE + CLANK_CRC_SIZE + 2U + sizeof(App_Header_Rec_t))   /* Includes 2-byte ACK_STATUS */
#define RFT_STATUS_SIZE          (CLANK_HDR_SIZE + CLANK_CRC_SIZE + P4TLM_TC_STATUS_PAYLOAD_SIZE)
#define RFT_CMD_BUF_SIZE         CLANK_MAX_MSG_SIZE   /* 24 if only fitting a full GET_TLM command bytes */
#define RFT_COMM_FAIL_CNT_MAX    0xFFFFUL
#define RFTAO_XFER_FIFO_SIZE     4U


/* Active Object public interface */

GLADOS_AO_t AO_Rft;
GLADOS_Timer_t Tmr_RftRqstTimeout;


/* Active Object private variables */
static GLADOS_Event_t rftao_fifo[RFTAO_FIFO_SIZE];

static RftAO_Cmd_Rqst_t *rftao_cmd_rqst_ptr;                    /* Pointer to the current RFT request on the Xfer FIFO */
static uint8_t rftao_tx_buf[RFT_CMD_BUF_SIZE] = {0U};           /* RFT Tx buffer for non-pass thru commands */
static uint16_t rftao_tx_buf_length = 0U;                       /* RFT Tx buffer length for non-pass thru commands */
static uint8_t rftao_retry_cnt = 0U;                            /* Tracks the number of RFT retries per transfer request */
static uint32_t rftao_curr_seq_id = 0U;                         /* Clank Sequence ID incremented upon each transfer */
static uint8_t rftao_rx_buf[CLANK_MAX_MSG_SIZE] = {0U};         /* RFT Rx buffer for all transfers */
static uint8_t rftao_pass_thru_buf[CLANK_MAX_MSG_SIZE] = {0U};  /* Holds RFT responses to be forwarded to the spacecraft */

/* Use a separate FIFO for holding the RFT command requests. This FIFO
 * is not to be confused with the RFT AO FIFO, which is used to store
 * all events for the RFT AO. */
static GLADOS_Event_t rftao_xfer_fifo_buf[RFTAO_XFER_FIFO_SIZE] = { {0} };
static GLADOS_Fifo_t rftao_xfer_fifo = { 0 };
static GLADOS_Event_t rftao_init_xfer_evt;

static uint8_t uart2_dma_tx_err  = 0U;
static uint8_t uart2_dma_rx_data = 0U;
static uint8_t uart2_dma_rx_err  = 0U;


/* Normal UART Tx Callback ops sees UART_EVENT_TX_EMPTY and then
 * later UART_EVENT_END_TRANSFER */
static void LPUART2_TxCallback(UNUSED void *driverState, uart_event_t event, UNUSED void *userData)
{
    if (event == UART_EVENT_ERROR)
    {
        ++uart2_dma_tx_err;
    }

    return;
}


/* Normal UART Tx Callback ops sees UART_EVENT_RX_FULL and then
 * later UART_EVENT_END_TRANSFER */
static void LPUART2_RxCallback(UNUSED void *driverState, uart_event_t event, UNUSED void *userData)
{
    if (event == UART_EVENT_END_TRANSFER)
    {
        ++uart2_dma_rx_data;
    }
    else if (event == UART_EVENT_ERROR)
    {
        /* Capture all other errors (e.g. UART Rx overrun, framing, parity, noise)
         * in this semaphore. If the user wishes to have additional error precision,
         * they may read the error flags from the STAT register. */
        ++uart2_dma_rx_err;
    }

    return;
}


static uint32_t get_rft_msg_id_response_length(RftAO_Cmd_Rqst_t *cmd_rqst);
static uint16_t parse_rft_ack_status(const uint8_t *rft_msg_buf);
static void parse_rft_tlm(const uint8_t *rft_tlm_buf);


/* Active Object state definitions */

void RftAO_init(void)
{
    /* Initialize the Active Object with both its Event FIFO and initial state function */
    GLADOS_AO_Init(&AO_Rft, RFTAO_PRIORITY, rftao_fifo, RFTAO_FIFO_SIZE, &RftAO_state);

    return;
}


void RftAO_push_interrupts(void)
{
    if (uart2_dma_tx_err > 0U)
    {
        --uart2_dma_tx_err;
        GLADOS_AO_PushEvtName(&AO_Rft, &AO_Rft, EVT_UART2_TX_ERR);
    }
    if (uart2_dma_rx_data > 0U)
    {
        --uart2_dma_rx_data;
        GLADOS_AO_PushEvtName(&AO_Rft, &AO_Rft, EVT_UART2_RX_DATA);
    }
    if (uart2_dma_rx_err > 0U)
    {
        --uart2_dma_rx_err;
        GLADOS_AO_PushEvtName(&AO_Rft, &AO_Rft, EVT_UART2_RX_ERR);
    }
    return;
}


void RftAO_state(GLADOS_AO_t *this_ao, GLADOS_Event_t *event)
{
    DEV_ASSERT(this_ao);
    DEV_ASSERT(event);

    bool success;

    switch(event->name)
    {
        /* GLADOS_ENTRY/GLADOS_EXIT optional event for each state */
        case GLADOS_ENTRY:
            GLADOS_Timer_Init(&Tmr_RftRqstTimeout, PT->rft_rqst_timeout_us, EVT_TMR_RFT_COMM_TIMEOUT);
            GLADOS_Timer_Subscribe_AO(&Tmr_RftRqstTimeout, this_ao);

            /* Set the Tx callback to handle UART transmit errors. The returned callback
             * is unused */
            (void)LPUART_DRV_InstallTxCallback(INST_LPUART2, &LPUART2_TxCallback, NULL);

            /* Set the Rx callback to handle UART receive completions as well as any
             * Rx errors. The returned callback is unused. */
            (void)LPUART_DRV_InstallRxCallback(INST_LPUART2, &LPUART2_RxCallback, NULL);

            /* Initialize a done event for all states to use to transition
             * back to TX_IDLE.  This event exists on the heap since Self Events
             * must not exist on the stack */
            GLADOS_Event_New(&rftao_init_xfer_evt, EVT_RFT_INIT_XFER);

            /* Initialize the Xfer FIFO used to buffer multiple RFT command requests */
            GLADOS_Fifo_Init(&rftao_xfer_fifo, RFTAO_XFER_FIFO_SIZE, &rftao_xfer_fifo_buf[0]);

            /* Initialize the system into a SAFE state */
            GLADOS_STATE_TRAN_TO_CHILD(&RftAO_state_idle);
            break;

        case GLADOS_EXIT:
            break;

        case EVT_RFT_SEND_MSG:
            /* No need to check event data validity here since it is handled
             * in the RFT busy state */

            /* Push the Tx event to the back of the intermediate Tx queue */
            success = GLADOS_Fifo_PushTail(&rftao_xfer_fifo, event);
            if (!success)
            {
                report_error_uint(this_ao, Errors.RftaoInterqFull, DO_NOT_SEND_TO_SPACECRAFT, rftao_xfer_fifo.count);
            }

            /* Transmission already in progress, so pushing to the Tx queue
             * is all that is needed here */
            break;

        case EVT_UART2_TX_ERR:
            /* Note: No logging of errors is necessary. Let the source AO
             * decide what to do when it receives an EVT_RFT_NACK */

            if (MaxData.tc_comm_fail_cnt < RFT_COMM_FAIL_CNT_MAX)
            {
                ++MaxData.tc_comm_fail_cnt;
            }

            /* Always returns STATUS_SUCCESS */
            (void)LPUART_DRV_AbortReceivingData(INST_LPUART2);
            (void)LPUART_DRV_AbortSendingData(INST_LPUART2);

            /* Data in rftao_cmd_rqst_ptr is still valid since the data
             * has not yet been popped from the FIFO. This occurs during
             * the BUSY GLADOS_EXIT transition, which happens next. */
            GLADOS_AO_PUSH(rftao_cmd_rqst_ptr->ao_src, EVT_RFT_NACK);

            GLADOS_STATE_TRAN_TO_CHILD(&RftAO_state_idle);
            break;

        case EVT_UART2_RX_ERR:
            /* Note: No logging of errors is necessary. Let the source AO
             * decide what to do when it receives an EVT_RFT_NACK */

            if (MaxData.tc_comm_fail_cnt < RFT_COMM_FAIL_CNT_MAX)
            {
                ++MaxData.tc_comm_fail_cnt;
            }

            /* Always returns STATUS_SUCCESS */
            (void)LPUART_DRV_AbortReceivingData(INST_LPUART2);
            (void)LPUART_DRV_AbortSendingData(INST_LPUART2);

            /* Data in rftao_cmd_rqst_ptr is still valid since the data
             * has not yet been popped from the FIFO. This occurs during
             * the BUSY GLADOS_EXIT transition, which happens next. */
            GLADOS_AO_PUSH(rftao_cmd_rqst_ptr->ao_src, EVT_RFT_NACK);

            GLADOS_STATE_TRAN_TO_CHILD(&RftAO_state_idle);
            break;

        case EVT_TMR_RFT_COMM_TIMEOUT:
            /* Note: No logging of errors is necessary. Let the source AO
             * decide what to do when it receives an EVT_RFT_NACK */

            if (MaxData.tc_comm_fail_cnt < RFT_COMM_FAIL_CNT_MAX)
            {
                ++MaxData.tc_comm_fail_cnt;
            }

            /* Always returns STATUS_SUCCESS */
            (void)LPUART_DRV_AbortReceivingData(INST_LPUART2);
            (void)LPUART_DRV_AbortSendingData(INST_LPUART2);

            GLADOS_AO_PUSH(rftao_cmd_rqst_ptr->ao_src, EVT_RFT_NACK);

            GLADOS_STATE_TRAN_TO_CHILD(&RftAO_state_idle);
            break;

        default:
            /* Top level state, skip undefined Events */
            break;
    }

    return;
}


void RftAO_state_idle(GLADOS_AO_t *this_ao, GLADOS_Event_t *event)
{
    DEV_ASSERT(this_ao);
    DEV_ASSERT(event);

    bool success;
    bool evt_exists;
    GLADOS_Event_t *tx_evt;

    switch(event->name)
    {
        /* GLADOS_ENTRY/GLADOS_EXIT optional event for each state */
        case GLADOS_ENTRY:
            /* If a request was waiting in the queue, then perform that request */
            evt_exists = GLADOS_Fifo_PeekHead(&rftao_xfer_fifo, &tx_evt);
            if (evt_exists)
            {
                /* Allow for a slight pause between transfers to give other AOs
                 * a chance to do their thing. If this was a self-evt, this would
                 * also be pushing up against the maximum number of self-evts
                 * permitted per AO processing frame. */
                GLADOS_AO_PUSH(this_ao, EVT_RFT_INIT_XFER);
            }
            break;

        case GLADOS_EXIT:
            break;

        case EVT_RFT_INIT_XFER:
            rftao_retry_cnt = 0U;
            GLADOS_STATE_TRAN_TO_SIBLING(&RftAO_state_busy);
            break;

        case EVT_RFT_SEND_MSG:
            /* No need to check event data validity here since it is handled
             * in the RFT busy state */

            /* Push the Tx event to the back of the intermediate Tx queue */
            success = GLADOS_Fifo_PushTail(&rftao_xfer_fifo, event);
            if (!success)
            {
                report_error_uint(this_ao, Errors.RftaoInterqFull, DO_NOT_SEND_TO_SPACECRAFT, rftao_xfer_fifo.count);
            }

            GLADOS_AO_PUSH_EVT_SELF(&rftao_init_xfer_evt);
            break;

        case EVT_UART2_TX_ERR:
        case EVT_UART2_RX_ERR:
            /* Ignore additional errors here since the error was already
             * captured and the transfer cancelled */
        case EVT_TMR_RFT_COMM_TIMEOUT:
        case EVT_UART2_RX_DATA:
            /* Ignore timeouts and data receptions that occurred simultaneously */
            break;

        default:
            GLADOS_STATE_CHECK_PARENT(&RftAO_state);
            break;
    }

    return;
}


void RftAO_state_busy(GLADOS_AO_t *this_ao, GLADOS_Event_t *event)
{
    DEV_ASSERT(this_ao);
    DEV_ASSERT(event);

    bool evt_exists;
    GLADOS_Event_t *tx_evt;

    switch(event->name)
    {
        /* GLADOS_ENTRY/GLADOS_EXIT optional event for each state */
        case GLADOS_ENTRY:
        case EVT_RFT_RETRY_XFER:
            evt_exists = GLADOS_Fifo_PeekHead(&rftao_xfer_fifo, &tx_evt);

            /* Order matters.  We expect an event to exist if we get to this state, but if
             * it does not exist or the event data within is invalid then throw error */
            if ((!evt_exists) || (!tx_evt->has_data) || (tx_evt->data_size != sizeof(RftAO_Cmd_Rqst_t)))
            {
                report_error_uint(this_ao, Errors.RftaoInterqNoData, DO_NOT_SEND_TO_SPACECRAFT,
                                     ((uint32_t)tx_evt->has_data << 8U) + (uint32_t)tx_evt->data_size);
                GLADOS_STATE_TRAN_TO_SIBLING(&RftAO_state_idle);
            }
            else
            {
                /* Get a pointer to the transfer parameters. This pointer points
                 * to the top item of the intermediate transfer FIFO, therefore
                 * the item in that FIFO should not be popped until the transfer
                 * has completed. */
                rftao_cmd_rqst_ptr = (RftAO_Cmd_Rqst_t *)&tx_evt->data[0];

                switch(rftao_cmd_rqst_ptr->cmd_type)
                {
                    case RFT_CMD_PASSTHRU:
                        GLADOS_STATE_TRAN_TO_CHILD(&RftAO_state_busy_pass_thru);
                        break;

                    case RFT_CMD_SILENT:
                        GLADOS_STATE_TRAN_TO_CHILD(&RftAO_state_busy_silent);
                        break;

                    case RFT_CMD_INTERNAL:
                    default:
                        GLADOS_STATE_TRAN_TO_CHILD(&RftAO_state_busy_internal);
                        break;
                }
            }
            break;

        case GLADOS_EXIT:
            GLADOS_Timer_Disable(&Tmr_RftRqstTimeout);

            /* Transfer has completed, pop the transfer record and params */
            GLADOS_Fifo_PopHead(&rftao_xfer_fifo);
            break;

        default:
            GLADOS_STATE_CHECK_PARENT(&RftAO_state);
            break;
    }

    return;
}


void RftAO_state_busy_pass_thru(GLADOS_AO_t *this_ao, GLADOS_Event_t *event)
{
    DEV_ASSERT(this_ao);
    DEV_ASSERT(event);

    status_t status;
    uint16_t ack_status;
    uint32_t exp_rx_length;
    Clank_Hdr_t rft_msg_clank_hdr;

    switch(event->name)
    {
        /* GLADOS_ENTRY/GLADOS_EXIT optional event for each state */
        case GLADOS_ENTRY:
            exp_rx_length = get_rft_msg_id_response_length(rftao_cmd_rqst_ptr);
            DEV_ASSERT(exp_rx_length <= RFT_TLM_SIZE);

            /* Note: Manual flushing of the Rx FIFO does not appear to be necessary
             * since the DMA, when enabled, should only be triggered upon new data.
             * That said, there is nothing in the LPUART driver that manually flushes
             * the Rx FIFO via FIFO[RXFLUSH], so it's something to keep in mind if
             * issues arise. There have not been any issues so it seems this is okay. */

            /* Start the Rx DMA with the expected response size based on the msg ID */
            memset(&rftao_rx_buf[0], 0, sizeof(rftao_rx_buf));
            status = LPUART_DRV_ReceiveData(INST_LPUART2,
                                            &rftao_rx_buf[0],
                                            exp_rx_length);

            /* Start the Tx DMA with the queued RFT command request parameters.
             * For pass thrus, the buffer points to the beginning of the full
             * Clank packet. */
            DEV_ASSERT(rftao_cmd_rqst_ptr->buf);
            status |= LPUART_DRV_SendData(INST_LPUART2,
                                          rftao_cmd_rqst_ptr->buf,
                                          rftao_cmd_rqst_ptr->buf_length);

            if (status != STATUS_SUCCESS)
            {
                report_error_uint(this_ao, Errors.RftaoUartBusy, DO_NOT_SEND_TO_SPACECRAFT,
                                     ((uint32_t)rftao_cmd_rqst_ptr->cmd_type << 16U) + (uint32_t)rftao_cmd_rqst_ptr->msg_id);
                /* Allow for Timeout to occur to recover and attempt any retries before
                 * returning to IDLE */
            }

            GLADOS_Timer_EnableOnce(&Tmr_RftRqstTimeout);
            break;

        case GLADOS_EXIT:
            break;

        case EVT_TMR_RFT_COMM_TIMEOUT:
            /* Note: Logging of errors is not necessary since the source AO
             * decides what to do when it receives an EVT_RFT_NACK, however
             * this insight might be helpful to differentiate between a
             * timeout from invalid CRC. */

            /* Always returns STATUS_SUCCESS */
            (void)LPUART_DRV_AbortReceivingData(INST_LPUART2);
            (void)LPUART_DRV_AbortSendingData(INST_LPUART2);

            /* Retry the transfer until it has reached the max */
            ++rftao_retry_cnt;
            if (rftao_retry_cnt >= (uint8_t)PT->rft_rqst_attempts_max)
            {
                GLADOS_AO_PUSH(rftao_cmd_rqst_ptr->ao_src, EVT_RFT_NACK);
                /* Note: No actual response message is sent back to the
                 * spacecraft port since it is treated as an unresponsive RFT */

                /* Pass through command failed, return to the RFT idle state */
                GLADOS_STATE_TRAN_TO_PARENT(&RftAO_state_busy);
                GLADOS_STATE_TRAN_TO_SIBLING(&RftAO_state_idle);
            }
            else
            {
                GLADOS_AO_PUSH(this_ao, EVT_RFT_RETRY_XFER);
            }
            break;

        case EVT_UART2_RX_DATA:
            GLADOS_Timer_Disable(&Tmr_RftRqstTimeout);

            /* At this point, the full header should have been received into the Rx buffer */
            Clank_GetHeader(&rftao_rx_buf[0], &rft_msg_clank_hdr);

            /* Validate clank formatting. Pass thru commands don't validate the
             * header transmit mask since the origin is unknown. */
            if ((false == Clank_CheckHeaderInfo(&rft_msg_clank_hdr))               ||
                (false == Clank_CheckCrc(&rftao_rx_buf[0])))
            {
                /* Retry the transfer until it has reached the max */
                ++rftao_retry_cnt;
                if (rftao_retry_cnt >= (uint8_t)PT->rft_rqst_attempts_max)
                {
                    report_error_uint(this_ao, Errors.RftaoXferInvalidPkt, DO_NOT_SEND_TO_SPACECRAFT,
                                          ((uint32_t)rftao_cmd_rqst_ptr->cmd_type << 16U) + (uint32_t)rftao_cmd_rqst_ptr->msg_id);
                    GLADOS_AO_PUSH(rftao_cmd_rqst_ptr->ao_src, EVT_RFT_NACK);
                    /* Note: No actual response message is sent back to the
                     * spacecraft port since it is treated as an unresponsive RFT */

                    /* Pass through command failed, return to the RFT idle state */
                    GLADOS_STATE_TRAN_TO_PARENT(&RftAO_state_busy);
                    GLADOS_STATE_TRAN_TO_SIBLING(&RftAO_state_idle);
                }
                else
                {
                    GLADOS_AO_PUSH(this_ao, EVT_RFT_RETRY_XFER);
                }
            }
            else
            {
                /* Check if the RFT ack status and use this to determine if the command
                 * was accepted or rejected. Signal this to the CmdAO so it may update
                 * its acpt/rjct counter accordingly but do not have CmdAO send an actual
                 * Ack/Nack message as this AO takes care of that for forwarding the RFT
                 * response to Tx AO. */
                ack_status = parse_rft_ack_status(&rftao_rx_buf[0]);
                if (ack_status != 0U)
                {
                    /* If clank packet is valid but status is rejected, then do not log error
                     * as the reason for the rejection is included in the returned ACK status
                     * passsed back to the source */
                    GLADOS_AO_PUSH(rftao_cmd_rqst_ptr->ao_src, EVT_RFT_NACK);
                }
                else
                {
                    GLADOS_AO_PUSH(rftao_cmd_rqst_ptr->ao_src, EVT_RFT_ACK);
                }

                /* Hold in intermediate RFT request pass through response buffer while
                 * it is transmitted.
                 * Note: If this buffer is altered before it can be transmitted, then
                 * data will likely be corrupted. To ensure this does not happen via
                 * defensive programming techniques requires a lot of extra design that
                 * is not really necessary for something that can more easily be handled
                 * with a commanding restriction of at least 10ms between RFT commands. */
                memcpy(&rftao_pass_thru_buf[0], &rftao_rx_buf[0], sizeof(rftao_pass_thru_buf));

                exp_rx_length = get_rft_msg_id_response_length(rftao_cmd_rqst_ptr);
                DEV_ASSERT(exp_rx_length <= RFT_TLM_SIZE);
                TxAO_SendMsg_Raw(this_ao, (TxPort_t)rftao_cmd_rqst_ptr->rx_port, (uint32_t)&rftao_pass_thru_buf[0], exp_rx_length);

                /* Return to the RFT idle state */
                GLADOS_STATE_TRAN_TO_PARENT(&RftAO_state_busy);
                GLADOS_STATE_TRAN_TO_SIBLING(&RftAO_state_idle);
            }
            break;

        default:
            GLADOS_STATE_CHECK_PARENT(&RftAO_state_busy);
            break;
    }

    return;
}


void RftAO_state_busy_silent(GLADOS_AO_t *this_ao, GLADOS_Event_t *event)
{
    DEV_ASSERT(this_ao);
    DEV_ASSERT(event);

    status_t status;
    uint16_t ack_status;
    uint32_t exp_rx_length;
    Clank_Hdr_t rft_msg_clank_hdr;

    switch(event->name)
    {
        /* GLADOS_ENTRY/GLADOS_EXIT optional event for each state */
        case GLADOS_ENTRY:
            Clank_CreateMessage(&rftao_tx_buf[0],
                                &rftao_tx_buf_length,
                                true,    /* Acts as the Master over the Thruster Controller */
                                MAX_ADDR_PCU,
                                MAX_ADDR_RFT,
                                rftao_curr_seq_id++,
                                rftao_cmd_rqst_ptr->msg_id,
                                rftao_cmd_rqst_ptr->buf,
                                rftao_cmd_rqst_ptr->buf_length);

            exp_rx_length = get_rft_msg_id_response_length(rftao_cmd_rqst_ptr);
            DEV_ASSERT(exp_rx_length <= RFT_TLM_SIZE);

            /* Note: Manual flushing of the Rx FIFO does not appear to be necessary
             * since the DMA, when enabled, should only be triggered upon new data.
             * That said, there is nothing in the LPUART driver that manually flushes
             * the Rx FIFO via FIFO[RXFLUSH], so it's something to keep in mind if
             * issues arise. There have not been any issues so it seems this is okay. */

            /* Start the Rx DMA with the expected response size based on the msg ID */
            memset(&rftao_rx_buf[0], 0, sizeof(rftao_rx_buf));
            status = LPUART_DRV_ReceiveData(INST_LPUART2,
                                            &rftao_rx_buf[0],
                                            exp_rx_length);

            /* Start the Tx DMA with the queued RFT command request parameters */
            status |= LPUART_DRV_SendData(INST_LPUART2,
                                          &rftao_tx_buf[0],
                                          rftao_tx_buf_length);

            if (status != STATUS_SUCCESS)
            {
                report_error_uint(this_ao, Errors.RftaoUartBusy, DO_NOT_SEND_TO_SPACECRAFT,
                                     ((uint32_t)rftao_cmd_rqst_ptr->cmd_type << 16U) + (uint32_t)rftao_cmd_rqst_ptr->msg_id);
                /* Allow for Timeout to occur to recover and return to IDLE */
            }

            GLADOS_Timer_EnableOnce(&Tmr_RftRqstTimeout);
            break;

        case GLADOS_EXIT:
            break;

        case EVT_UART2_RX_DATA:
            GLADOS_Timer_Disable(&Tmr_RftRqstTimeout);

            /* At this point, the full header should have been received into the Rx buffer */
            Clank_GetHeader(&rftao_rx_buf[0], &rft_msg_clank_hdr);

            ack_status = parse_rft_ack_status(&rftao_rx_buf[0]);

            /* Validate clank formatting. Order matters since we must validate header
             * and data integrity before validating the ACK_STATUS within the message. */
            if ((false == Clank_CheckHeaderInfo(&rft_msg_clank_hdr))                        ||
                (false == Clank_CheckTxMask(&rft_msg_clank_hdr, MAX_ADDR_PCU)) ||
                (false == Clank_CheckCrc(&rftao_rx_buf[0]))                                 ||
                (ack_status != 0U))
            {
                /* Each failed data acquisition of the RFT TLM is considered a comm
                 * failure. This heartbeat occurs very often, so ensure that this
                 * count does not roll over. This is used with FDIR to determine if
                 * RFT is connected. */
                if (MaxData.tc_comm_fail_cnt < RFT_COMM_FAIL_CNT_MAX)
                {
                    ++MaxData.tc_comm_fail_cnt;
                }
                ++MaxData.tc_comm_fail_cum_cnt;
            }
            /* If the silent internal command was TLM data, process it, otherwise
             * ignore any other successful command response. */
            else if (rft_msg_clank_hdr.Pkt_DataLen == TLMAO_MAX_TLM_DATA)
            {
                /* RFT TLM has successfully been received! */
                MaxData.tc_comm_fail_cnt = 0UL;

                /* Get RFT TLM into MaxData for FDIR and general use */
                parse_rft_tlm(&rftao_rx_buf[0]);

                /* Store the RFT TLM payload into the DAQ TLM buffer.
                 * Note: Copying received RFT TLM from this buffer MUST occur in this
                 * AO since it is only coherent in this moment. Otherwise it may be
                 * in use for UART transfers at any time. */
                TlmAO_DaqTlm_StoreRftTlm(&rftao_rx_buf[0]);

                /* Note: The DaqCtrlAO has a hard timer for 4ms before moving forward with
                 * whatever data it has, whether or not data is stale or new from this
                 * current sample. */
            }

            /* Return to the RFT idle state */
            GLADOS_STATE_TRAN_TO_PARENT(&RftAO_state_busy);
            GLADOS_STATE_TRAN_TO_SIBLING(&RftAO_state_idle);
            break;

        default:
            GLADOS_STATE_CHECK_PARENT(&RftAO_state_busy);
            break;
    }

    return;
}


void RftAO_state_busy_internal(GLADOS_AO_t *this_ao, GLADOS_Event_t *event)
{
    DEV_ASSERT(this_ao);
    DEV_ASSERT(event);

    status_t status;
    bool is_valid_msg;
    bool is_rft_data_cmd;
    uint16_t ack_status;
    uint32_t exp_rx_length;
    Clank_Hdr_t rft_msg_clank_hdr;
    Cmd_Err_Code_t error_code;
    GLADOS_Event_t evt;
    RftAO_Intern_Data_t rft_intern_data;

    switch(event->name)
    {
        /* GLADOS_ENTRY/GLADOS_EXIT optional event for each state */
        case GLADOS_ENTRY:
            Clank_CreateMessage(&rftao_tx_buf[0],
                                &rftao_tx_buf_length,
                                true,    /* Acts as the Master over the Thruster Controller */
                                MAX_ADDR_PCU,
                                MAX_ADDR_RFT,
                                rftao_curr_seq_id++,
                                rftao_cmd_rqst_ptr->msg_id,
                                rftao_cmd_rqst_ptr->buf,
                                rftao_cmd_rqst_ptr->buf_length);

            exp_rx_length = get_rft_msg_id_response_length(rftao_cmd_rqst_ptr);
            DEV_ASSERT(exp_rx_length <= RFT_TLM_SIZE);

            /* Note: Manual flushing of the Rx FIFO does not appear to be necessary
             * since the DMA, when enabled, should only be triggered upon new data.
             * That said, there is nothing in the LPUART driver that manually flushes
             * the Rx FIFO via FIFO[RXFLUSH], so it's something to keep in mind if
             * issues arise. There have not been any issues so it seems this is okay. */

            /* Start the Rx DMA with the expected response size based on the msg ID */
            memset(&rftao_rx_buf[0], 0, sizeof(rftao_rx_buf));
            status = LPUART_DRV_ReceiveData(INST_LPUART2,
                                            &rftao_rx_buf[0],
                                            exp_rx_length);

            /* Start the Tx DMA with the queued RFT command request parameters */
            status |= LPUART_DRV_SendData(INST_LPUART2,
                                          &rftao_tx_buf[0],
                                          rftao_tx_buf_length);

            if (status != STATUS_SUCCESS)
            {
                report_error_uint(this_ao, Errors.RftaoUartBusy, DO_NOT_SEND_TO_SPACECRAFT,
                                     ((uint32_t)rftao_cmd_rqst_ptr->cmd_type << 16U) + (uint32_t)rftao_cmd_rqst_ptr->msg_id);
                /* Allow for Timeout to occur to recover and return to IDLE */
            }

            GLADOS_Timer_EnableOnce(&Tmr_RftRqstTimeout);
            break;

        case GLADOS_EXIT:
            break;

        case EVT_TMR_RFT_COMM_TIMEOUT:
            /* Note: No logging of errors is necessary. Let the source AO
             * decide what to do when it receives an EVT_RFT_NACK */

            /* Always returns STATUS_SUCCESS */
            (void)LPUART_DRV_AbortReceivingData(INST_LPUART2);
            (void)LPUART_DRV_AbortSendingData(INST_LPUART2);

            /* Retry the transfer until it has reached the max */
            ++rftao_retry_cnt;
            if (rftao_retry_cnt >= (uint8_t)PT->rft_rqst_attempts_max)
            {
                /* Let source AO decide how to deal with the error */
                error_code = create_error_uint(Errors.RftInternCommTimeout, Errors.RftInternCommTimeout->error_id);
                GLADOS_Event_New_Data(&evt, EVT_RFT_NACK, (uint8_t *)&error_code, sizeof(CmdAO_NackData_t));
                GLADOS_AO_PUSH_EVT(rftao_cmd_rqst_ptr->ao_src, &evt);

                /* Pass through command failed, return to the RFT idle state */
                GLADOS_STATE_TRAN_TO_PARENT(&RftAO_state_busy);
                GLADOS_STATE_TRAN_TO_SIBLING(&RftAO_state_idle);
            }
            else
            {
                GLADOS_AO_PUSH(this_ao, EVT_RFT_RETRY_XFER);
            }
            break;

        case EVT_UART2_RX_DATA:
            GLADOS_Timer_Disable(&Tmr_RftRqstTimeout);

            /* At this point, the full header should have been received into the Rx buffer */
            Clank_GetHeader(&rftao_rx_buf[0], &rft_msg_clank_hdr);

            is_valid_msg  = Clank_CheckHeaderInfo(&rft_msg_clank_hdr);
            is_valid_msg &= Clank_CheckTxMask(&rft_msg_clank_hdr, MAX_ADDR_PCU);
            is_valid_msg &= Clank_CheckCrc(&rftao_rx_buf[0]);

            /* One-off to handle the fact that GET_SW_INFO just returns raw data and
             * does not return an ack_status */
            if (rft_msg_clank_hdr.MsgID == CLANK_RESP_MSG_ID(GET_SW_INFO))
            {
                ack_status = 0U;
                is_rft_data_cmd = true;
            }
            else
            {
                ack_status = parse_rft_ack_status(&rftao_rx_buf[0]);
                is_rft_data_cmd = false;
            }

            /* Validate clank formatting. Order matters since we must validate header
             * and data integrity before validating the ACK_STATUS within the message. */
            if ((!is_valid_msg) || (ack_status != 0U))
            {
                /* Retry the transfer until it has reached the max */
                ++rftao_retry_cnt;
                if (rftao_retry_cnt >= (uint8_t)PT->rft_rqst_attempts_max)
                {
                    /* If the message is invalid, the ack_status data may be invalid and cannot
                     * be trusted. Use the error id for the Ack status in these instances.
                     * Otherwise, use the returned Nack status as the error data. */
                    if (!is_valid_msg)
                    {
                        ack_status = Errors.RftInternCommInvalidPkt->error_id;
                    }

                    /* Let the source AO decide how to handle the error */
                    error_code = create_error_uint(Errors.RftInternCommInvalidPkt, ack_status);
                    GLADOS_Event_New_Data(&evt, EVT_RFT_NACK, (uint8_t *)&error_code, sizeof(CmdAO_NackData_t));
                    GLADOS_AO_PUSH_EVT(rftao_cmd_rqst_ptr->ao_src, &evt);

                    /* Pass through command failed, return to the RFT idle state */
                    GLADOS_STATE_TRAN_TO_PARENT(&RftAO_state_busy);
                    GLADOS_STATE_TRAN_TO_SIBLING(&RftAO_state_idle);
                }
                else
                {
                    GLADOS_AO_PUSH(this_ao, EVT_RFT_RETRY_XFER);
                }
            }
            /* RFT message was successfully received */
            else
            {
                /* If the RFT does not return data, then returning a simple ACK
                 * to the initiating AO works nicely */
                if (!is_rft_data_cmd)
                {
                    GLADOS_AO_PUSH(rftao_cmd_rqst_ptr->ao_src, EVT_RFT_ACK);
                }
                /* Handle specific cases where the RFT returns data */
                else
                {
                    /* Hold in intermediate RFT request pass through response buffer while
                     * it is transmitted.
                     * Note: If this buffer is altered before it can be transmitted, then
                     * data will likely be corrupted. To ensure this does not happen via
                     * defensive programming techniques requires a lot of extra design that
                     * is not really necessary for something that can more easily be handled
                     * with a commanding restriction of at least 10ms between RFT commands. */
                    memcpy(&rftao_pass_thru_buf[0], &rftao_rx_buf[0], sizeof(rftao_pass_thru_buf));

                    exp_rx_length = get_rft_msg_id_response_length(rftao_cmd_rqst_ptr);

                    /* Return the data start location (not the message start location) and the
                     * data length to the AO that initiated the transfer. */
                    rft_intern_data.data_addr   = (uint32_t)&rftao_pass_thru_buf[CLANK_HDR_SIZE];
                    rft_intern_data.data_length = exp_rx_length - (CLANK_HDR_SIZE + CLANK_CRC_SIZE);
                    DEV_ASSERT(rft_intern_data.data_length <= RFT_TLM_SIZE);

                    GLADOS_Event_New_Data(&evt, EVT_RFT_ACK_DATA, (uint8_t *)&rft_intern_data, sizeof(RftAO_Intern_Data_t));
                    GLADOS_AO_PUSH_EVT(rftao_cmd_rqst_ptr->ao_src, &evt);
                }

                /* Return to the RFT idle state */
                GLADOS_STATE_TRAN_TO_PARENT(&RftAO_state_busy);
                GLADOS_STATE_TRAN_TO_SIBLING(&RftAO_state_idle);
            }
            break;

        default:
            GLADOS_STATE_CHECK_PARENT(&RftAO_state_busy);
            break;
    }

    return;
}


void RftAO_push_cmd_request(GLADOS_AO_t *ao_src, Rft_Cmd_t cmd_type, P4Cmd_Id_t msg_id,
                                uint8_t *buf, uint16_t buf_length, uint8_t rx_port)
{
    /* NOTES:
     * 1) rx_port only used for pass_thru commands that require the returned message
     *    to be transmitted as is. Internal commands do not use this rx_port parameter.
     * 2) buf as NULL is an acceptable value */
    DEV_ASSERT(ao_src);

    GLADOS_Event_t evt;

    RftAO_Cmd_Rqst_t rft_cmd_rqst =
    {
        .ao_src       = ao_src,
        .cmd_type     = (uint8_t)cmd_type,
        .buf          = buf,
        .buf_length   = buf_length,
        .msg_id       = (uint16_t)msg_id,
        .rx_port      = rx_port
    };

    GLADOS_Event_New_Data(&evt, EVT_RFT_SEND_MSG, (uint8_t *)&rft_cmd_rqst, sizeof(RftAO_Cmd_Rqst_t));
    GLADOS_AO_PushEvt(ao_src, &AO_Rft, &evt);

    return;
}


/* Get the state of the timeout timer, since if it
 * is enabled, we will want to reenable after adjusting
 * the timer duration.
 *
 * Note: This implementation requires that Tmr_RftRqstTimeout
 * is only enabled once (GLADOS_Timer_EnableOnce) and
 * never continuously (GLADOS_Timer_EnableContinuous) */
void RftAO_adjust_request_timeout(uint32_t duration_us)
{
    DEV_ASSERT(Tmr_RftRqstTimeout.is_continuous == false);

    bool rft_rqst_enabled = Tmr_RftRqstTimeout.enabled;

    GLADOS_Timer_Disable(&Tmr_RftRqstTimeout);
    GLADOS_Timer_Set(&Tmr_RftRqstTimeout, duration_us);

    if (rft_rqst_enabled)
    {
        /* Reenable the timer if it was already enabled */
        GLADOS_Timer_EnableOnce(&Tmr_RftRqstTimeout);
    }

    return;
}


static uint32_t get_rft_msg_id_response_length(RftAO_Cmd_Rqst_t *cmd_rqst)
{
    DEV_ASSERT(cmd_rqst);
    uint16_t msg_id;
    uint32_t msg_length;
    Clank_Hdr_t hdr;

    /* Pass thru commands do not have a specified msg_id within
     * them, therefore the Clank packet must be parsed for it .*/
    if (cmd_rqst->cmd_type == RFT_CMD_PASSTHRU)
    {
        Clank_GetHeader(cmd_rqst->buf, &hdr);
        msg_id = hdr.MsgID;
    }
    else
    {
        msg_id = cmd_rqst->msg_id;
    }

    /* Get the expected response size based on msg ID */
    if (msg_id == GET_TLM)
    {
        msg_length = RFT_TLM_SIZE;
    }
    else if (msg_id == GET_SW_INFO)
    {
        msg_length = RFT_GET_SW_INFO_SIZE;
    }
    else if (msg_id == GET_MAX_HRD_INFO)
    {
        msg_length = RFT_GET_HW_INFO_SIZE;
    }
    else
    {
        msg_length = RFT_STATUS_SIZE;
    }
    return msg_length;
}

static uint16_t parse_rft_ack_status(const uint8_t *rft_msg_buf)
{
    DEV_ASSERT(rft_msg_buf);

    P4Tlm_TcStatus_t *status = (P4Tlm_TcStatus_t *)&rft_msg_buf[CLANK_HDR_SIZE];

    return REV16(status->tc_ack_status);
}


static void parse_rft_tlm(const uint8_t *rft_tlm_buf)
{
    DEV_ASSERT(rft_tlm_buf);
    DEV_ASSERT(sizeof(P4Tlm_TcTlm_t) <= TLMAO_MAX_TLM_DATA);

    uint32_t f32_tmp;
    P4Tlm_TcTlm_t *tc_tlm = (P4Tlm_TcTlm_t *)&rft_tlm_buf[CLANK_HDR_SIZE];

    TcData.ack_status       = REV16(tc_tlm->tc_status.tc_ack_status);
    TcData.sc_err1_code     = REV16(tc_tlm->tc_status.sc_tc_err1_code);
    TcData.sc_err2_code     = REV16(tc_tlm->tc_status.sc_tc_err2_code);
    TcData.sc_err3_code     = REV16(tc_tlm->tc_status.sc_tc_err3_code);
    TcData.state            = (uint8_t)tc_tlm->tc_status.tc_state;
    TcData.tc_busy          = (uint8_t)tc_tlm->tc_status.tc_busy;
    TcData.needs_reset_flag = (uint8_t)tc_tlm->tc_status.tc_needs_reset;
    TcData.app_select       = (uint8_t)tc_tlm->tc_status.tc_app_select;
    TcData.cmd_acpt_cnt     = REV16(tc_tlm->tc_status.tc_acpt_cnt);
    TcData.cmd_rjct_cnt     = REV16(tc_tlm->tc_status.tc_rjct_cnt);
    TcData.unix_time_us_hi  = REV32(tc_tlm->tc_status.tc_unix_time_us_hi);
    TcData.unix_time_us_lo  = REV32(tc_tlm->tc_status.tc_unix_time_us_lo);

    TcData.powerup_err_code = REV16(tc_tlm->tlm_err_rec.powerup_err_code);
    TcData.err1_code        = REV16(tc_tlm->tlm_err_rec.err1.err_code);
    TcData.err1_data        = REV32(tc_tlm->tlm_err_rec.err1.err_data);
    TcData.err2_code        = REV16(tc_tlm->tlm_err_rec.err2.err_code);
    TcData.err2_data        = REV32(tc_tlm->tlm_err_rec.err2.err_data);
    TcData.err3_code        = REV16(tc_tlm->tlm_err_rec.err3.err_code);
    TcData.err3_data        = REV32(tc_tlm->tlm_err_rec.err3.err_data);
    TcData.err_cnt          = REV16(tc_tlm->tlm_err_rec.total_err_cnt);

    TcData.bl_select        = (uint8_t)tc_tlm->tc_bl_select;
    TcData.pt_select        = (uint8_t)tc_tlm->tc_pt_select;

    TcData.bl_crc           = REV32(tc_tlm->tc_bl_crc);
    TcData.app_crc          = REV32(tc_tlm->tc_app_crc);
    TcData.pt_crc           = REV32(tc_tlm->tc_pt_crc);

    TcData.rcm_value        = REV32(tc_tlm->tc_rcm_value);
    TcData.wdt_rst_trig     = (bool)tc_tlm->tc_wdt_rst_trig;
    TcData.wdt_rst_tog_sel  = (bool)tc_tlm->tc_wdt_rst_tog_sel;
    TcData.gpio_wdi_fb      = (bool)tc_tlm->tc_wdi_fb;
    TcData.fsw_cpu_util     = (uint8_t)tc_tlm->tc_fsw_cpu_util;

    f32_tmp = REV32(tc_tlm->tc_tfet);
    TcData.tfet             = BYTES_TO_F32(f32_tmp);
    f32_tmp = REV32(tc_tlm->tc_tchoke);
    TcData.tchoke           = BYTES_TO_F32(f32_tmp);
    f32_tmp = REV32(tc_tlm->tc_tcoil);
    TcData.tcoil            = BYTES_TO_F32(f32_tmp);

    f32_tmp = REV32(tc_tlm->tc_tic);
    TcData.tic              = BYTES_TO_F32(f32_tmp);

#ifdef DUMMY_LOAD_STUBS
    tc_tlm->tc_tpl = REV32(F32_TO_BYTES(TcData.tpl));
#else
    f32_tmp = REV32(tc_tlm->tc_tpl);
    TcData.tpl              = BYTES_TO_F32(f32_tmp);
#endif
    f32_tmp = REV32(tc_tlm->tc_tmatch);
    TcData.tmatch           = BYTES_TO_F32(f32_tmp);
    f32_tmp = REV32(tc_tlm->tc_ehvd);
    TcData.ehvd             = BYTES_TO_F32(f32_tmp);
    f32_tmp = REV32(tc_tlm->tc_ihvd);
    TcData.ihvd             = BYTES_TO_F32(f32_tmp);
    f32_tmp = REV32(tc_tlm->tc_tadc0);
    TcData.tadc0            = BYTES_TO_F32(f32_tmp);
    f32_tmp = REV32(tc_tlm->tc_ebus);
    TcData.ebus             = BYTES_TO_F32(f32_tmp);
    f32_tmp = REV32(tc_tlm->tc_e12v);
    TcData.e12v             = BYTES_TO_F32(f32_tmp);
    f32_tmp = REV32(tc_tlm->tc_e5v);
    TcData.e5v              = BYTES_TO_F32(f32_tmp);
    f32_tmp = REV32(tc_tlm->tc_evb);
    TcData.evb              = BYTES_TO_F32(f32_tmp);
    f32_tmp = REV32(tc_tlm->tc_ibus);
    TcData.ibus             = BYTES_TO_F32(f32_tmp);
    f32_tmp = REV32(tc_tlm->tc_i12v);
    TcData.i12v             = BYTES_TO_F32(f32_tmp);
    f32_tmp = REV32(tc_tlm->tc_ivb);
    TcData.ivb              = BYTES_TO_F32(f32_tmp);
    f32_tmp = REV32(tc_tlm->tc_tfb_in);
    TcData.tfb_in           = BYTES_TO_F32(f32_tmp);
    f32_tmp = REV32(tc_tlm->tc_tfb_out);
    TcData.tfb_out          = BYTES_TO_F32(f32_tmp);
    f32_tmp = REV32(tc_tlm->tc_tfram);
    TcData.tfram            = BYTES_TO_F32(f32_tmp);
    f32_tmp = REV32(tc_tlm->tc_tmcu);
    TcData.tmcu             = BYTES_TO_F32(f32_tmp);

    f32_tmp = REV16(tc_tlm->tc_pwm_pp_vset_raw_fb);
    TcData.pwm_pp_vset_raw_fb = BYTES_TO_F32(f32_tmp);
    f32_tmp = REV32(tc_tlm->tc_pp_vset_fb);
    TcData.pp_vset_fb       = BYTES_TO_F32(f32_tmp);
    f32_tmp = REV32(tc_tlm->tc_wav_gen_freq_fb);
    TcData.wav_gen_freq_fb  = BYTES_TO_F32(f32_tmp);

    TcData.ecc_sram_1bit_cnt = REV32(tc_tlm->tc_ecc_sram_1bit_cnt);

    TcData.data_mode        = (DataMode_t)tc_tlm->tc_data_mode;
    TcData.gpio_wf_en_fb    = (bool)tc_tlm->tc_wf_en_fb;
    TcData.gpio_pp_en_fb    = (bool)tc_tlm->tc_pp_en_fb;

    TcData.inverter_connected_flag    = (bool)tc_tlm->tc_inv_connected;
    TcData.rft_connected_flag    = (bool)tc_tlm->tc_rft_connected;
    TcData.hp_connected_flag     = (bool)tc_tlm->tc_hp_connected;

    TcData.pvb = TcData.evb * TcData.ivb;

    return;
}
