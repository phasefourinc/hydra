/* Copyright (c) 2017-2020 by Phase Four, Inc. All Rights Reserved. Confidential and Proprietary.
 *
 * DaqCtlAO.c
 */

#include "DaqCtlAO.h"
#include "mcu_types.h"
#include "glados.h"
#include "TlmAO.h"
#include "MaxwellAO.h"
#include "ErrAO.h"
#include "RftAO.h"
#include "edma_adc.h"
#include "ad768x.h"
#include "ad568x.h"
#include "flexio_spi3.h"
#include "max_data.h"
#include "gpio.h"
#include "pwm_load_driver.h"
#include "pid.h"
#include "timer64.h"
#include "p4_cmd.h"
#include "p4_err.h"
#include "default_pt.h"
#include "math.h"
#include "prop_inventory.h"
#include "p4_err.h"
#include "string.h"
#include "flow_ctrl.h"

/* Heater
 *
 * Nominal is 12V with resistance of 9.6ohms (+/-1ohm)
 *
 * High Power Isolation Valve
 *
 * Nominal is 12V with resistance of 80-85ohms
 *
 * Spike is 150mA for 50ms and then hold is >75mA.  Since the bus voltage
 * can swing from 22V-36V, it's safest to have a hold voltage of around 9V,
 * which equates to about 109mA.
 *
 * Recommended Spike: 158mA
 * Recommended Hold:  108mA
 *
 *
 * Low Power Isolation Valve
 *
 * Nominal is 12V with resistance of 285ohms (range unknown by Clipper)
 *
 * Spike is 42mA for 50ms and then hold is >21mA.  Ratings for the
 * Clippard is 10% of nominal 12V, which at 285ohms equates to
 * 37.8mA to 46.2mA for actuation and max current respectively.
 * Since the bus voltage can swing from 22V-36V, it is safest to have
 * the hold voltage around 9V, which equates to about 31mA.
 *
 * Recommended Spike: 45mA
 * Recommended Hold:  30mA
 * */

#define HEATER_NOMINAL_BUS_V    28.0f
#define HEATER_MAX_CURRENT_A    1.25f

#define LPF_NUM_OF_POINTS    7UL

typedef struct
{
    float arr[LPF_NUM_OF_POINTS];
    float sum;
    uint32_t curr_idx;    /* index of most recent data point */
    bool is_init;

} LPF_t;


float low_pass_filter(LPF_t *lpf, float curr_value);

LPF_t lpf_ttank = { {0.0f}, 0.0f, 0UL, 0 };
LPF_t lpf_propmass = { {0.0f}, 0.0f, 0UL, 0 };

/* Active Object public interface */

GLADOS_AO_t AO_DaqCtl;

/* Active Object timers */
GLADOS_Timer_t Tmr_DaqStart;
GLADOS_Timer_t Tmr_DaqRftTimeout;


/* Active Object private variables */
static GLADOS_Event_t daqctl_fifo[DAQCTLAO_FIFO_SIZE];

AD768x_t Adc0 = { 0, 0, 0, {0} };
AD768x_t Adc1 = { 0, 0, 0, {0} };

static AD568x_t Dac0 = { 0 };

static Pwm_t Pwm_Heater = {0};
static Pwm_t Pwm_HiPIso = {0};

static Pid_t Pid_HeatCtrl = {0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, false};

static volatile uint8_t dma_adc_tx_err  = 0U;
static volatile uint8_t dma_adc_rx_done = 0U;
static volatile uint8_t dma_adc_rx_err  = 0U;

static bool started_heating = false;
static uint8_t heating_completed_cnt = 0U;
static uint32_t flow_ctrl_ms_cnt     = 0U;
static uint32_t heater_ctrl_ms_cnt   = 0U;
static uint32_t prop_inv_calc_ms_cnt = 0U;
static uint32_t heat_ctrl_hold_cnt   = 0UL;
static float heat_ctrl_next_temp     = 0.0f;
static float heat_ctrl_max_power     = 0.0f;

static void DmaAdc_RxCallback(UNUSED void *parameter, edma_chn_status_t event)
{
    if (event == EDMA_CHN_NORMAL)
    {
        ++dma_adc_rx_done;
    }
    else
    {
        ++dma_adc_rx_err;
    }

    return;
}


static void DmaAdc_TxCallback(UNUSED void *parameter, edma_chn_status_t event)
{
    if (event != EDMA_CHN_NORMAL)
    {
        ++dma_adc_tx_err;
    }

    return;
}


/* Active Object state definitions */


void DaqCtlAO_init(void)
{
    /* Initialize the Active Object with both its Event FIFO and initial state function */
    GLADOS_AO_Init(&AO_DaqCtl, DAQCTLAO_PRIORITY, daqctl_fifo, DAQCTLAO_FIFO_SIZE, &DaqCtlAO_state);

    return;
}


void DaqCtlAO_push_interrupts(void)
{
    if (dma_adc_rx_done > 0U)
    {
        --dma_adc_rx_done;
        GLADOS_AO_PushEvtName(&AO_DaqCtl, &AO_DaqCtl, EVT_DAQ_DMA_COMPLETE);
    }
    if (dma_adc_rx_err > 0U)
    {
        --dma_adc_rx_err;
        report_error_uint(&AO_DaqCtl, Errors.SpiAdcDmaRxOverrun, DO_NOT_SEND_TO_SPACECRAFT, 0U);
    }
    if (dma_adc_tx_err > 0U)
    {
        --dma_adc_tx_err;
        report_error_uint(&AO_DaqCtl, Errors.SpiAdcDmaTxUnderrun, DO_NOT_SEND_TO_SPACECRAFT, 0U);
    }
    return;
}



void DaqCtlAO_state(GLADOS_AO_t *this_ao, GLADOS_Event_t *event)
{
    DEV_ASSERT(this_ao);
    DEV_ASSERT(event);

    status_t status;

    switch(event->name)
    {
        /* GLADOS_ENTRY/GLADOS_EXIT optional event for each state */
        case GLADOS_ENTRY:
            /* Configure DAQ rate, this also determines the overall system frame rate */
            GLADOS_Timer_Init(&Tmr_DaqStart, DAQCTLAO_PERIOD_US, EVT_TMR_DAQ_START);
            GLADOS_Timer_Subscribe_AO(&Tmr_DaqStart, this_ao);
            GLADOS_Timer_EnableContinuous(&Tmr_DaqStart);

            /* 4ms is selected for RFT response timeout which allows for any mal-packet
             * retransmissions to complete successfully but permits 1-2ms to handle TLM
             * data processing and control algorithms, leaving 4-5ms as frame buffer. */
            GLADOS_Timer_Init(&Tmr_DaqRftTimeout, 4000UL, EVT_TMR_DAQ_RFT_TIMEOUT);
            GLADOS_Timer_Subscribe_AO(&Tmr_DaqRftTimeout, this_ao);

            /* Initialize the heater control temperature setpoint here to allow for it to
             * tank the new setting after the PT has been selected and loaded. */
            MaxData.heat_ctrl_temp_fb = PT->heater_ctl_setpt_degC;
            heat_ctrl_next_temp       = PT->heater_ctl_setpt_degC;

            /* Initialize the DMA process that is to perform the autonomous ADC transfers */
            EdmaAdc_Init(&DmaAdc_TxCallback, &DmaAdc_RxCallback);

            DaqCtrlAO_InitDrivers();

            /* Initialize the PFCV DAC driver */
            AD568x_Init(&Dac0, INST_FLEXIO_SPI3, &flexio_spi3_MasterConfig0, PT->pfcv_max_mA, PT->pfcv_mA_to_dac_cal);

            /* The generic PWM constant current driver uses max_load_v and load_ohms params, obtained from the heater
             * load, to prevent exceeding a max current limit. For the heater specifically, even though it is a current
             * driver, the power going into the heater is actually what matters. Otherwise the fluctuating unregulated
             * bus voltage at the same max current leads to fluctuating power draws that affects heating rate. To normalize
             * heating rates, a max power is defined and managed external to the PWM driver and then the PWM driver is
             * configured to an internal max current (max_load_v/load_ohms) that cannot be exceeded regardless of power.
             *
             * Use the heater PT values to determine the max power at a nominal bus voltage and initialize the PWM driver
             * so its max_load_v equates to an internal current limit of HEATER_MAX_CURRENT_A */
            heat_ctrl_max_power   = (PT->heater_pwm_max_load_v / PT->heater_pwm_load_ohms) * HEATER_NOMINAL_BUS_V;
            status  = PWM_Load_Init(&Pwm_Heater, INST_FLEXTIMER_PWM1, 2U, &flexTimer_pwm1_InitConfig,
                                       &flexTimer_pwm1_PwmConfig, &PT->heater_mA_to_pwm_cal, PT->heater_pwm_load_ohms,
                                       (PT->heater_pwm_load_ohms * HEATER_MAX_CURRENT_A));
            if (status != STATUS_SUCCESS)
            {
                report_error_uint(&AO_DaqCtl, Errors.PwmDriverInitFail, DO_NOT_SEND_TO_SPACECRAFT, INST_FLEXTIMER_PWM1);
            }

            /* Initialize the HPISO constant current driver */
            status = PWM_Load_Init(&Pwm_HiPIso, INST_FLEXTIMER_PWM3, 0U, &flexTimer_pwm3_InitConfig,
                                       &flexTimer_pwm3_PwmConfig, &PT->hpiso_mA_to_pwm_cal, PT->hpiso_pwm_load_ohms, PT->hpiso_pwm_max_load_v);
            if (status != STATUS_SUCCESS)
            {
                report_error_uint(&AO_DaqCtl, Errors.PwmDriverInitFail, DO_NOT_SEND_TO_SPACECRAFT, INST_FLEXTIMER_PWM3);
            }

            GLADOS_STATE_TRAN_TO_CHILD(&DaqCtlAO_state_idle);
            break;

        case GLADOS_EXIT:
            break;

        default:
            /* Top level state, skip undefined Events */
            break;
    }

    return;
}


void DaqCtlAO_state_idle(GLADOS_AO_t *this_ao, GLADOS_Event_t *event)
{
    DEV_ASSERT(this_ao);
    DEV_ASSERT(event);

    status_t status;
    Cmd_Err_Code_t error;
    float prop_mass;
    float ttank_filtered;

    switch(event->name)
    {
        /* GLADOS_ENTRY/GLADOS_EXIT optional event for each state */
        case GLADOS_ENTRY:
            break;

        case GLADOS_EXIT:
            break;

        case EVT_TMR_DAQ_START:
            /* Add duration that has elapsed to each of the counters
             * Note: Counts rolling over is dandy */
            flow_ctrl_ms_cnt     += DAQCTLAO_PERIOD_US / 1000UL;
            heater_ctrl_ms_cnt   += DAQCTLAO_PERIOD_US / 1000UL;
            prop_inv_calc_ms_cnt += DAQCTLAO_PERIOD_US / 1000UL;

            /* If still in RFT_DATA state, then we have a frame overrun */
            DEV_ASSERT(this_ao->state_fn_ptr == &DaqCtlAO_state_idle);

            status = EdmaAdc_EndTransfer();
            if (status != STATUS_SUCCESS)
            {
                report_error_uint(&AO_DaqCtl, Errors.SpiAdcDmaError, DO_NOT_SEND_TO_SPACECRAFT, (uint32_t)status);
            }

            EdmaAdc_StartTransfer();

            GLADOS_STATE_TRAN_TO_CHILD(&DaqCtlAO_state_idle_rft_data);
            break;

        case EVT_RFT_NACK:
        case EVT_RFT_ACK:
            /* Currently status from the RFT transfer is not used since
             * DAQ Ctrl AO will go ahead with whatever data exists in
             * the TLM DAQ buffer, even if it's stale */
            break;

        case EVT_DAQ_DMA_COMPLETE:
            status = EdmaAdc_EndTransfer();

            if (status != STATUS_SUCCESS)
            {
                if (MaxData.daq_consec_err_cnt < 0xFFFFFFFFUL)
                {
                    ++MaxData.daq_consec_err_cnt;
                }
                report_error_uint(&AO_DaqCtl, Errors.SpiAdcDmaError, DO_NOT_SEND_TO_SPACECRAFT, (uint32_t)status); /*TODO Why is there a error here? The FDIR should take care of this case. Seems redundant. */
            }
            else
            {
                /* Reset the consecutive DAQ ADC error counter */
                MaxData.daq_consec_err_cnt = 0UL;

                AD768x_RawToVoltage_AllChannels(&Adc0);
                AD768x_RawToVoltage_AllChannels(&Adc1);

#ifdef DUMMY_LOAD_STUBS
                /* Following devices are stubbed
                 *  - IPFCV
                 *  - IHPISO
                 *  - PTANK
                 *  - TRTD4
                 *  - PLOWP
                 *  - MFS_MDOT
                 *  - TRTD2
                 */
                MaxData.e3v3     = AD768x_CalibrateChannel(&Adc0, IN0);  /* 3V3_SENSE */
                MaxData.ibus     = AD768x_CalibrateChannel(&Adc0, IN2);  /* BOARD_ISENSE */
                MaxData.ebus     = AD768x_CalibrateChannel(&Adc0, IN3);  /* BUS_SENSE */
                MaxData.irft     = AD768x_CalibrateChannel(&Adc0, IN6);  /* RFT_ISENSE */
                MaxData.e12v     = AD768x_CalibrateChannel(&Adc0, IN7);  /* 12V_SENSE */
                MaxData.tadc0    = AD768x_CalibrateChannel(&Adc0, TEMP); /* ADC0_TEMP */
                MaxData.iheater  = AD768x_CalibrateChannel(&Adc1, IN1);  /* TANK_ISENSE */
                MaxData.e5v      = AD768x_CalibrateChannel(&Adc1, IN4);  /* 5V_SENSE */
                MaxData.e2v5     = AD768x_CalibrateChannel(&Adc1, IN5);  /* 2V5_REF_SENSE */
                MaxData.tadc1    = AD768x_CalibrateChannel(&Adc1, TEMP); /* ADC1_TEMP */
                MaxData.heater_connected = true;
                MaxData.mfs_connected = true;
#else
                MaxData.e3v3     = AD768x_CalibrateChannel(&Adc0, IN0);  /* 3V3_SENSE */
                MaxData.ipfcv    = AD768x_CalibrateChannel(&Adc0, IN1);  /* PFCV_ISENSE */
                MaxData.ibus     = AD768x_CalibrateChannel(&Adc0, IN2);  /* BOARD_ISENSE */
                MaxData.ebus     = AD768x_CalibrateChannel(&Adc0, IN3);  /* BUS_SENSE */
                MaxData.ihpiso   = AD768x_CalibrateChannel(&Adc0, IN4);  /* HPISO_ISENSE */
                MaxData.pprop    = AD768x_CalibrateChannel(&Adc0, IN5);  /* HPD_VOUT */
                MaxData.irft     = AD768x_CalibrateChannel(&Adc0, IN6);  /* RFT_ISENSE */
                MaxData.e12v     = AD768x_CalibrateChannel(&Adc0, IN7);  /* 12V_SENSE */
                MaxData.tadc0    = AD768x_CalibrateChannel(&Adc0, TEMP); /* ADC0_TEMP */

                MaxData.tprop    = AD768x_CalibrateChannel(&Adc1, IN0);  /* RTD4_VOUT */
                MaxData.iheater  = AD768x_CalibrateChannel(&Adc1, IN1);  /* TANK_ISENSE */
                                                                         /* CH2 UNUSED */
                MaxData.plowp    = AD768x_CalibrateChannel(&Adc1, IN3);  /* LPD_VOUT */
                MaxData.e5v      = AD768x_CalibrateChannel(&Adc1, IN4);  /* 5V_SENSE */
                MaxData.e2v5     = AD768x_CalibrateChannel(&Adc1, IN5);  /* 2V5_REF_SENSE */
                MaxData.mfs_mdot = get_current_mdot(Adc1.ch_voltage[IN6], Adc1.ch_voltage[IN7]);
                MaxData.tmfs     = AD768x_CalibrateChannel(&Adc1, IN7);  /* RTD2_VOUT */
                MaxData.tadc1    = AD768x_CalibrateChannel(&Adc1, TEMP); /* ADC1_TEMP */

                /* The TTANK value reads extremely low degC values when not connected */
                MaxData.heater_connected = (MaxData.tprop > -100.0f);

                /* The MFS temp value reads extremely high degC values when not connected */
                MaxData.mfs_connected = (MaxData.tmfs < 200.0f);
#endif
                /* 10+ consecutive comm errors with the Thr Ctrl indicate no connection */
                MaxData.tc_connected = (MaxData.tc_comm_fail_cnt < 10U);

                if ((flow_ctrl_ms_cnt >= PT->mdot_pid_period_ms) && (MaxData.flow_ctrl_en_fb))
                {
                    /* Reset count (and resync if it had rolled over) */
                    flow_ctrl_ms_cnt = 0UL;

                    /* For each PID iteration, gather data used to determine flow stability,
                     * which can be used for validating stability */
                    (void)DaqCtlAO_FlowCtrl_Stability(false);

                    flow_ctrl_iterate();
                }
                /* If flow control is disabled, then there is no need to reassert a flow rate */

                /* Do prop inventory every 100 ms unless there are no valid Prop Inv constants to use */
                if ((prop_inv_calc_ms_cnt >= PT->prop_inv_calc_period_ms) && (!MaxData.using_default_pt))
                {
                    /* Reset count (and resync if it had rolled over) */
                    prop_inv_calc_ms_cnt = 0UL;

                    /* Checking if the inputs (RTD4 and ptank) to determine what state prop inventory is in.
                     * The Proper values are Temp: 25 - 65 celcius, Pressure: 1 -3000 pisa
                     * Else if the input values are Temp: 25 - 65 celcius, Pressure: 1 -3000 pisa. Then it is in Low resolution mode.
                     * Else we can not calculate prop inventory due to normalizing bounds of Temperature (25 - 65) and Pressure (1 - 3000) so it is not valid.
                     */
                    MaxData.prop_status = get_prop_mass_status();

                    if (MaxData.prop_status != PROP_NOT_VALID_MODE)
                    {
                        prop_mass = 0.0f;
                        if (MaxData.pprop < CHEBYSHEV_CAlC_SWITCH_PRESSURE_VAL)
                        {
                            prop_mass = calculate_density_lowp(MaxData.pprop, MaxData.tprop);
                        }
                        else
                        {
                            prop_mass = calculate_density_highp(MaxData.pprop, MaxData.tprop);
                        }

                        /* Verifying the prop _mass calculation as a sanity check to make sure the value we have calculated are in the expected bounds.*/
                        if ((prop_mass <= LOWEST_PROP_MASS_VALUE) || (prop_mass >= HIGHEST_PROP_MASS_VALUE))
                        {
                            /* The value is out of bounds. There seems to be a error in the implementation of the density calculation or the checking of the sensor values */
                            /* Regardless lets throw a error up so we can see this in development/testing */
                            report_error_float(&AO_DaqCtl, (err_type*) Errors.PropInvInvalidPropCalculation , DO_NOT_SEND_TO_SPACECRAFT, MaxData.prop_density);
                            /*Since the value is goofy it is not valid. */
                            MaxData.prop_status = PROP_NOT_VALID_MODE;
                        }
                        else {
                            MaxData.prop_density= low_pass_filter(&lpf_propmass, prop_mass);
                        }

                    }
                }

                /* Perform heater control if enabled */
                if ((heater_ctrl_ms_cnt >= PT->heater_pid_period_ms) && (MaxData.heat_ctrl_en_fb))
                {
                    /* Reset count (and resync if it had rolled over) */
                    heater_ctrl_ms_cnt = 0UL;

                    /* Reduce TTANK noise from creating positive feedback in the PID */
                    ttank_filtered = low_pass_filter(&lpf_ttank, MaxData.tprop);

                    /* Feed in the current temp to the PID controller and scale it to
                     * the desired current setpoint
                     *
                     * The temp to i_mA scale plant function effectively amplifies the role that the current
                     * temperature has on the next setpoint. It also affects the default current when the setpoint
                     * has been reached. Since there is no cooling mechanism, having some minimum current allows
                     * the PID to have more control since it can at least back off on the pedal. */
                    MaxData.heater_setpt_ma_fb = PT->heater_temp2mA_scale * (ttank_filtered +
                                                     PID_Calculate(&Pid_HeatCtrl, heat_ctrl_next_temp, ttank_filtered));

                    /* Coerce to the maximum permitted heater bus power if necessary, which can
                     * vary depending on what the unregulated bus voltage is currently sampled at. */
                    if ((((MaxData.heater_setpt_ma_fb / 1000.0f) * MaxData.ebus) > heat_ctrl_max_power) &&
                        (fpclassify(MaxData.ebus) != FP_ZERO))
                    {
                        MaxData.heater_setpt_ma_fb = (heat_ctrl_max_power / MaxData.ebus) * 1000.0f;
                    }

                    /* This function clamps the desired setpoint and checks for finiteness before setting */
                    error = DaqCtlAO_Heater_Set(MaxData.heater_setpt_ma_fb);

                    if (error.reported_error != Errors.StatusSuccess)
                    {
                        /* Error setting the current heater control setpoint value, missing one
                         * is not critical since FDIR protects the system in parallel as a backup
                         * mechanism should too many of these occur at a bad time. The backup to
                         * the backup is that the heater efuse can trip. It is sufficient to log
                         * this error as a warning. */
                        report_error_uint(&AO_DaqCtl, error.reported_error, DO_NOT_SEND_TO_SPACECRAFT, error.aux_data);
                    }

                    /* This conditional is active only if a hold heater setpoint is active. Heat to
                     * the intermediate heat_ctrl_next_temp setpoint and do not update to the final
                     * heater control temperature until the hold has elapsed. Subtract 1C to allow for
                     * a buffer in case PID has steady-state error and cannot fully reach the setpoint.
                     * Once the hold has been entered, then let the hold perpetuate for the hard
                     * duration. A hold count above the size of the low pass filter ensures a single,
                     * railed ttank transient does not enter the hold prematurely. */
                    if ((heat_ctrl_next_temp < MaxData.heat_ctrl_temp_fb) &&
                        ((ttank_filtered > (heat_ctrl_next_temp - 1.0f)) || (heat_ctrl_hold_cnt > LPF_NUM_OF_POINTS)))
                    {
                        /* Continuously resetting the error prevents the integral term from winding
                         * up and adding more overshoot during the hold period, allowing for the
                         * Kp term to dominate until the holding period has completed. */
                        PID_ResetError(&Pid_HeatCtrl);

                        /* Each hold count equates to one count of the heater control period.
                         * E.g. If heater control period is 1sec, then each count is 1sec. */
                        ++heat_ctrl_hold_cnt;
                        if (heat_ctrl_hold_cnt > PT->heater_ctl_hold_cnt)
                        {
                            /* Holding period has completed, set heater ctrl to final setpoint */
                            heat_ctrl_next_temp = MaxData.heat_ctrl_temp_fb;
                        }
                    }

                    /* Indicate to the Maxwell AO that IDLE heating has completed if it
                     * has either reached the default IDLE temp or if it has reached
                     * its desired heater control temp setpoint. This permits for heater
                     * control values below the default IDLE complete setpt to also permit
                     * transitioning to THRUST, which is likely needed for testing.
                     *
                     * Note: Only send if DaqCtlAO_HeaterCtlSet() has been called to set
                     * started_heating, which ensures that this event only occurs once
                     * or else we would likely flood Maxwell AO with these events. */
                    if (started_heating &&
                            ((MaxData.tprop >= PT->heater_idle_complete_degC) ||
                             (MaxData.tprop >= MaxData.heat_ctrl_temp_fb)))
                    {
                        ++heating_completed_cnt;

                        /* Filter out transient RTD4 samples by requiring multiple readings */
                        if (heating_completed_cnt >= PT->heater_ctl_done_sample_cnt)
                        {
                            started_heating = false;
                            heating_completed_cnt = 0U;
                            heat_ctrl_hold_cnt = 0UL;
                            GLADOS_AO_PUSH(&AO_Maxwell, EVT_DAQ_PROP_HEATED);
                        }
                    }
                    else
                    {
                        heating_completed_cnt = 0U;
                    }
                }
                else if (!MaxData.heat_ctrl_en_fb)
                {
                    /* If heater control is disabled, then reassert the current
                     * heater enable and PWM driver settings to permit custom
                     * control of these functions.
                     * Note: Returned status will be successful because they are set using
                     * values that were previously successful. Assert statement suffices. */
                    DaqCtlAO_Heater_Enable(MaxData.heater_en_fb);
                    error = DaqCtlAO_Heater_Set(MaxData.heater_setpt_ma_fb);
                    DEV_ASSERT(error.reported_error == Errors.StatusSuccess);
                }
            }

            /* No matter what, sample fault lines */
            MaxData.rft_fault    = GPIO_GetInput(GPIO_RFT_FAULT);
            MaxData.heater_fault = GPIO_GetInput(GPIO_HEATER_FAULT);
            MaxData.hpiso_fault  = GPIO_GetInput(GPIO_HPISO_FAULT);
            MaxData.pfcv_fault   = GPIO_GetInput(GPIO_PFCV_FAULT);
            MaxData.wdt_rst_trig = GPIO_GetInput(GPIO_WDT_RESET);
            MaxData.wdt_tog_sel  = GPIO_GetInput(GPIO_WDT_TOGGLE_SEL);

            /* Reassert other PWM drivers each DAQ cycle. This is important for
             * radiation as well as ensuring that bus voltage does not sneak away
             * and close an already open valve since the PWM Set functions account
             * for the latest bus voltage.
             * Note: Returned status will be successful because they are set using
             * values that were previously successful. Assert statement suffices. */
            DaqCtlAO_HighPIso_Enable(MaxData.hpiso_en_fb);
            error = DaqCtlAO_HighPIso_Set(MaxData.hpiso_setpt_ma_fb);
            DEV_ASSERT(error.reported_error == Errors.StatusSuccess);

            /* Wait until all RFT data has been received */
            break;

        default:
            GLADOS_STATE_CHECK_PARENT(&DaqCtlAO_state);
            break;
    }

    return;
}


void DaqCtlAO_state_idle_rft_data(GLADOS_AO_t *this_ao, GLADOS_Event_t *event)
{
    DEV_ASSERT(this_ao);
    DEV_ASSERT(event);

    switch(event->name)
    {
        /* GLADOS_ENTRY/GLADOS_EXIT optional event for each state */
        case GLADOS_ENTRY:
            GLADOS_Timer_EnableOnce(&Tmr_DaqRftTimeout);
            /* Push start of RFT data gathering and wait for completion */
            RftAO_push_cmd_request(this_ao, RFT_CMD_SILENT, GET_TLM, NULL, 0U, 0U);
            break;

        case GLADOS_EXIT:
            GLADOS_Timer_Disable(&Tmr_DaqRftTimeout);
            GLADOS_AO_PUSH(&AO_Tlm, EVT_DAQ_DATA_READY);
            break;

        case EVT_TMR_DAQ_RFT_TIMEOUT:
            /* Use the RFT status sampling timeout to determine when to move onto
             * TLM parsing. This keeps timing consistent whether or not the Thr Ctrl
             * is connected. */
            GLADOS_STATE_TRAN_TO_PARENT(&DaqCtlAO_state_idle);
            break;

        default:
            GLADOS_STATE_CHECK_PARENT(&DaqCtlAO_state_idle);
            break;
    }

    return;
}



/* Public Helper Functions */


void DaqCtrlAO_InitDrivers(void)
{
    /* Configure both ADCs with a 5V Vref and point them to the appropriate location
     * within the eDMA Rx buffer where each ADC's data will be when Rx is complete. */
    AD768x_Init(&Adc0, &edma_adc_rx[EDMA_ADC0_CH0_IDX], &PT->adc0_ch0_cal_e3v3,  5.0f);
    AD768x_Init(&Adc1, &edma_adc_rx[EDMA_ADC1_CH0_IDX], &PT->adc1_ch0_cal_trtd4, 5.0f);

    PID_Init(&Pid_HeatCtrl, PT->heater_pid_kp, PT->heater_pid_ki, PT->heater_pid_kd,
                 ((float)PT->heater_pid_period_ms / 1000.0f), PT->heater_pid_i_limit, PT->heater_degC_min, PT->heater_degC_max);

    flow_ctrl_init();

    return;
}

void DaqCtlAO_RFT_FuseReset(void)
{
    /* Clear the fault condition stored in the D-flipflop */
    GPIO_SetOutput(GPIO_RFT_nRST_EN, false);
    timer32_delay_us(2000UL);
    GPIO_SetOutput(GPIO_RFT_nRST_EN, true);

    MaxData.rft_fault = GPIO_GetInput(GPIO_RFT_FAULT);

    return;
}


void DaqCtlAO_RFT_ResetEnable(bool enable)
{
    /* Clear the fault condition stored in the D-flipflop */
    GPIO_SetOutput(GPIO_RFT_nRST_EN, enable);
    return;
}


/* Note that enabling and disabling the heater directly
 * while Heater Control is enabled may be overridden
 * in the next frame */
void DaqCtlAO_Heater_Enable(bool enable)
{
    MaxData.heater_en_fb = enable;
    GPIO_SetOutput(GPIO_HEATER_EN, enable);
    return;
}


/* Heater has a range of 0-3.956A at 36V.  However we only want to run the
 * heater at a max of 1.25A (12V at 9.6ohms)
 *
 * Note: Setting heater current value directly while Heater Control is enabled
 * may be overridden in the next frame */
Cmd_Err_Code_t DaqCtlAO_Heater_Set(float i_mA)
{

    /* Validate that Pwm_Heater structure has been properly initialized */
    DEV_ASSERT(Pwm_Heater.state.ftmClockSource != FTM_CLOCK_SOURCE_NONE);
    Cmd_Err_Code_t error_code;
    status_t status;

    /* Configure the bus voltage used for the PWM driver right before setting
     * the driver since the bus voltage can fluctuate anywhere from 22-36V */
    PWM_Load_SetVBus(&Pwm_Heater, MaxData.ebus);

    status = PWM_Load_SetCurrent(&Pwm_Heater, i_mA);
    if (status == STATUS_SUCCESS)
    {
        error_code = create_error_uint((err_type *)Errors.StatusSuccess, 0UL);

        MaxData.heater_setpt_ma_fb = i_mA;
    }
    else
    {
        /* Range error */
        error_code = create_error_float((err_type *)Errors.CmdSetHeaterCurrentOutOfBounds, i_mA);
    }

    return error_code;
}


Cmd_Err_Code_t DaqCtlAO_Heater_SetPwmRaw(uint16_t duty_cycle_cnt)
{
    /* Validate that Pwm_Heater structure has been properly initialized */
    DEV_ASSERT(Pwm_Heater.state.ftmClockSource != FTM_CLOCK_SOURCE_NONE);

    Cmd_Err_Code_t error_code = create_error_uint((err_type *)Errors.CmdInvalidHeaterPwmRawInvalidDutyCycle, duty_cycle_cnt);

    float desired_v;
    status_t status;

    /* Configure the bus voltage used for the PWM driver right before setting
     * the driver since the bus voltage can fluctuate anywhere from 22-36V */
    PWM_Load_SetVBus(&Pwm_Heater, MaxData.ebus);

    desired_v = Pwm_Heater.bus_v * ((float)duty_cycle_cnt / (float)FTM_MAX_DUTY_CYCLE);

    status = PWM_Load_SetDriverRaw(&Pwm_Heater, duty_cycle_cnt);
    if (status == STATUS_SUCCESS)
    {
        error_code = create_error_uint((err_type *)Errors.StatusSuccess, 0UL);

        /* Set the setpt current as the duty cycle percentage */
        MaxData.heater_setpt_ma_fb = (desired_v / Pwm_Heater.load_ohms) * 1000.0f;
    }

    return error_code;
}


void DaqCtlAO_Heater_FuseReset(void)
{
    /* Clear the fault condition stored in the D-flipflop */
    GPIO_SetOutput(GPIO_HEATER_nRST_EN, false);
    timer32_delay_us(25UL);
    GPIO_SetOutput(GPIO_HEATER_nRST_EN, true);

    /* Allow for sufficient time from D-flipflop clearing to
     * the value showing up on the MCU pin */
    timer32_delay_us(1UL);
    MaxData.heater_fault = GPIO_GetInput(GPIO_HEATER_FAULT);

    return;
}


void DaqCtlAO_HeatCtl_Enable(bool enable)
{
    Cmd_Err_Code_t error_code;

    MaxData.heat_ctrl_en_fb = enable;
    started_heating = enable;
    heating_completed_cnt = 0U;
    heat_ctrl_hold_cnt = 0UL;

    /* Automatically turn off as a safety
     * mechanism. Users that wish to have
     * Heater Control disabled but the heater
     * on must reassert it. */
    if (!enable)
    {
        PID_ResetError(&Pid_HeatCtrl);

        /* Disable the Heater PWM current driver. Returns error
         * code if value is out of range, which 0.0 is not and
         * therefore we check with an assert statement. */
        DaqCtlAO_Heater_Enable(false);
        error_code = DaqCtlAO_Heater_Set(0.0f);
        DEV_ASSERT(error_code.reported_error->error_id == 0U);
        (void)error_code;  /* Removes unused compiler warning */
    }
    else
    {
        /* Do not reset PID error in case heating is reasserted.
         * Disabling resets the PID error so it should be all set. */

        /* If an intermediate hold temp is configured and the the
         * current temperature is less than the hold temp, then set
         * the next heater setpoint to the hold temp. Otherwise, set
         * to the final target temp. */
        if (MaxData.tprop < (MaxData.heat_ctrl_temp_fb - PT->heater_ctl_hold_diff_degC))
        {
            heat_ctrl_next_temp = MaxData.heat_ctrl_temp_fb - PT->heater_ctl_hold_diff_degC;
        }
        else
        {
            heat_ctrl_next_temp = MaxData.heat_ctrl_temp_fb;
        }

        /* Set the enable, but don't worry, the PWM will be
         * set in the heater control loop */
        DaqCtlAO_Heater_Enable(true);

        /* Initialize the count to ensure that the heater ctrl
         * will always start upon the next frame */
        heater_ctrl_ms_cnt = PT->heater_pid_period_ms;
    }

    return;
}


Cmd_Err_Code_t DaqCtlAO_HeatCtl_SetTemp(float setpt_degC)
{
    Cmd_Err_Code_t error_code = create_error_float(Errors.CmdInvalidHeaterCtrlSetpt, setpt_degC);

    if (isfinite(setpt_degC))
    {
        error_code = ERROR_NONE;

        /* Documenting a behavior:
         * If heater control is already underway, setting this command will
         * bypass the intermediate heat_ctrl_next_temp setpt and go straight to
         * the heat_ctrl_temp_fb setpoint. If this is undesired, then be sure to
         * set the heater control setpoint first before enabling the control. */
        MaxData.heat_ctrl_temp_fb = setpt_degC;
        heat_ctrl_next_temp = setpt_degC;
    }

    return error_code;
}


void DaqCtlAO_HighPIso_Enable(bool enable)
{
    MaxData.hpiso_en_fb = enable;
    GPIO_SetOutput(GPIO_HPISO_EN, enable);
    return;
}


Cmd_Err_Code_t DaqCtlAO_HighPIso_Spike(void)
{
    Cmd_Err_Code_t error_code = DaqCtlAO_HighPIso_Set(PT->hpiso_pwm_spike_mA);
    return error_code;
}

Cmd_Err_Code_t DaqCtlAO_HighPIso_Hold(void)
{
    Cmd_Err_Code_t error_code = DaqCtlAO_HighPIso_Set(PT->hpiso_pwm_hold_mA);
    return error_code;
}


void DaqCtlAO_HighPIso_Close(void)
{
    /* Disable PWM DAC signal. Returns error code if value is
     * out of range, which 0.0 is not and therefore we check
     * with an assert statement */
    Cmd_Err_Code_t error_code = DaqCtlAO_HighPIso_Set(0.0f);
    DEV_ASSERT(error_code.reported_error->error_id == 0U);
    (void)error_code;  /* Removes unused compiler warning */
    return;
}


Cmd_Err_Code_t DaqCtlAO_HighPIso_Set(float i_mA)
{
    /* Validate that Pwm_HiPIso structure has been properly initialized */
    DEV_ASSERT(Pwm_HiPIso.state.ftmClockSource != FTM_CLOCK_SOURCE_NONE);
    Cmd_Err_Code_t error_code = ERROR_NONE;
    status_t status;


    /* Configure the bus voltage used for the PWM driver right before setting
     * the driver since the bus voltage can fluctuate anywhere from 22-36V */
    PWM_Load_SetVBus(&Pwm_HiPIso, MaxData.ebus);

    status = PWM_Load_SetCurrent(&Pwm_HiPIso, i_mA);
    if (status == STATUS_SUCCESS)
    {
        MaxData.hpiso_setpt_ma_fb = i_mA;
    }
    else
    {
        error_code = create_error_float(Errors.CmdSetHpisoCurrentOutOfBounds, i_mA);
    }

    return error_code;
}


Cmd_Err_Code_t DaqCtlAO_HighPIso_SetPwmRaw(uint16_t duty_cycle_cnt)
{
    /* Validate that Pwm_Heater structure has been properly initialized */
    DEV_ASSERT(Pwm_HiPIso.state.ftmClockSource != FTM_CLOCK_SOURCE_NONE);

    Cmd_Err_Code_t error_code = {(err_type*) Errors.CmdInvalidHpisoPwmRawInvalidDutyCycle, duty_cycle_cnt};
    float desired_v;
    status_t status;

    /* Configure the bus voltage used for the PWM driver right before setting
     * the driver since the bus voltage can fluctuate anywhere from 22-36V */
    PWM_Load_SetVBus(&Pwm_HiPIso, MaxData.ebus);

    desired_v = Pwm_HiPIso.bus_v * ((float)duty_cycle_cnt / (float)FTM_MAX_DUTY_CYCLE);

    status = PWM_Load_SetDriverRaw(&Pwm_HiPIso, duty_cycle_cnt);
    if (status == STATUS_SUCCESS)
    {
        error_code = create_error_uint(Errors.StatusSuccess, 0UL);

        /* Set the setpt current as the duty cycle percentage */
        MaxData.hpiso_setpt_ma_fb = (desired_v / Pwm_HiPIso.load_ohms) * 1000.0f;
    }

    return error_code;
}


void DaqCtlAO_FlowCtl_Enable(bool enable)
{
    flow_ctrl_enable(enable);

    /* Always reset flow control ms count. This has no effect when disabling
     * since flow control will be disabled anyway. Also this does not affect
     * the old flow control algorithm since it does flow control every frame.
     * Setting to zero is necessary for newer code, however, which does flow
     * control at a slower rate */
    flow_ctrl_ms_cnt = 0U;

    return;
}


Cmd_Err_Code_t DaqCtlAO_FlowCtl_Set(float mdot_setpt)
{
    return flow_ctrl_setpt(mdot_setpt);
}


void DaqCtlAO_Pfcv_DacResetToClose(void)
{
    MaxData.flow_ctrl_mdot_fb = 0.0f;
    MaxData.pfcv_ma_fb = 0.0f;
    MaxData.desired_mdot_v_fb = 0.0f;

    GPIO_SetOutput(GPIO_PFCV_DAC_nRST_EN, false);
    timer32_delay_us(1UL);
    GPIO_SetOutput(GPIO_PFCV_DAC_nRST_EN, true);
    return;
}


void DaqCtlAO_Pfcv_DacAbort(void)
{
    AD568x_XferAbort(&Dac0);
    return;
}

Cmd_Err_Code_t DaqCtlAO_Pfcv_Set(float mdot_setpt)
{
    Cmd_Err_Code_t error = ERROR_NONE;

    if ((!isfinite(mdot_setpt))     ||
        (mdot_setpt < PT->mdot_min) ||
        (mdot_setpt > PT->mdot_max))
    {
        error = create_error_float(Errors.MdotSetPointOutOfBounds, mdot_setpt);
    }
    else
    {
        /* Place FlexIO SPI into a known idle state before transferring */
        DaqCtlAO_Pfcv_DacAbort();

        if (fpclassify(mdot_setpt) == FP_ZERO)
        {
            /* Bypass calibration for setpoints of zero to avoid setting the
             * PFCV right at the cracking current, ensuring it closes fully */
            MaxData.pfcv_ma_fb = 0.0f;
        }
        else
        {
            /* Calibrate the new mdot setpoint to a PFCV current in mA */
            MaxData.pfcv_ma_fb = calibrate(mdot_setpt, &PT->mdot_to_pfcv_mA_cal[0], sizeof(PT->mdot_to_pfcv_mA_cal)/sizeof(float));
        }

        /* Command the PFCV with the current in mA */
        error = DaqCtlAO_Pfcv_SetCurrent(MaxData.pfcv_ma_fb);
        if (error.reported_error == Errors.StatusSuccess)
        {
            MaxData.desired_mdot_v_fb = mdot_setpt;
        }
    }

    return error;
}


/* Note: This function sets the value but it is not reflected in the
 *       mdot feedback */
Cmd_Err_Code_t DaqCtlAO_Pfcv_SetCurrent(float i_mA)
{
    DEV_ASSERT(isfinite(i_mA));
    Cmd_Err_Code_t error_code = create_error_float(Errors.Ad568SpiSetFailed, i_mA);
    status_t status = STATUS_ERROR;

    if (isfinite(i_mA))
    {
        /* Place FlexIO SPI into a known idle state before transferring */
        DaqCtlAO_Pfcv_DacAbort();

        /* Set the DAC Write Control register each time to ensure no
         * radiation hit. Doing this also simplifies resetting the DAC
         * since we guarantee to configure the DAC properly upon each
         * write. */
        status = AD568x_SetVal_Blocking(&Dac0, i_mA);
        if (status == STATUS_SUCCESS)
        {
            error_code = ERROR_NONE;
            MaxData.pfcv_ma_fb = i_mA;
        }
    }

    return error_code;
}


/* Note: This function sets the value but it is not reflected in the
 *       mdot or PFCV current feedback */
Cmd_Err_Code_t DaqCtlAO_Pfcv_SetDacRaw(uint16_t code)
{
    Cmd_Err_Code_t error_code = {(err_type *)Errors.Ad568SpiSetFailed, code};
    status_t status = STATUS_ERROR;

    /* Place FlexIO SPI into a known idle state before transferring */
    DaqCtlAO_Pfcv_DacAbort();

    /* Set the DAC Write Control register each time to ensure no
     * radiation hit. Doing this also simplifies resetting the DAC
     * since we guarantee to configure the DAC properly upon each
     * write. */
    status  = AD568x_SetControlReg_Blocking(&Dac0, AD568X_WCR_GAIN_BIT);
    status |= AD568x_XferRaw_Blocking(&Dac0, AD568X_WDI, code);

    if (status == STATUS_SUCCESS)
    {
        /* Clear this feedback item to indicate that it is not in use */
        MaxData.desired_mdot_v_fb = 0.0f;
        error_code = ERROR_NONE;
    }

    return error_code;
}


float DaqCtlAO_FlowCtrl_Stability(bool reset)
{

    static float min_reading = 0.0f;
    static float max_reading = 0.0f;

    if (reset)
    {
        min_reading = MaxData.ipfcv;
        max_reading = MaxData.ipfcv;
    }
    else
    {
        /* If the period has been reached, grab the latest value
         * and determine if it is either a new max or min */
        if (MaxData.ipfcv < min_reading)
        {
            min_reading = MaxData.ipfcv;
        }
        else if (MaxData.ipfcv > max_reading)
        {
            max_reading = MaxData.ipfcv;
        }
    }

    return (max_reading - min_reading);
}


void DaqCtlAO_SetDaqRate(uint32_t period_us)
{
    /* Coerce to max DAQ rate */
    if (period_us < DAQCTLAO_PERIOD_US)
    {
        period_us = DAQCTLAO_PERIOD_US;
    }

    /* WARNING: Do NOT set the timer while it is running
     * as there is a race condition. */
    GLADOS_Timer_Disable(&Tmr_DaqStart);
    GLADOS_Timer_Set(&Tmr_DaqStart, period_us);
    GLADOS_Timer_EnableContinuous(&Tmr_DaqStart);

    return;
}


uint8_t get_prop_mass_status(void)
{
    uint8_t prop_mass_status = PROP_NOT_VALID_MODE;

    /*Validate  Temperatures */
    /* Every cycle, do a check to see if the prop mass is valid */
    if (MaxData.pprop >= PROP_MASS_PRESSURE_MIN && MaxData.pprop <= PROP_MASS_PRESSURE_MAXIMUM)
    {

        if (MaxData.tprop >= PROP_MASS_TEMP_MINIMUM && MaxData.tprop <= PROP_MASS_TEMP_MAXIMUM)
        {
            if (MaxData.tprop >= PT->heater_prop_mass_valid_degC && MaxData.tprop <= PROP_MASS_TEMP_MAXIMUM)
            {
                prop_mass_status = HIGH_RESOLUTION_MODE;
            }
            else
            {
                prop_mass_status = LOW_RESOLUTION_MODE;
            }
        }

    }
    return prop_mass_status;
}




float low_pass_filter(LPF_t *lpf, float curr_value)
{
    DEV_ASSERT(lpf);
    DEV_ASSERT(LPF_NUM_OF_POINTS > 0UL);

    uint32_t i;

    /* Skip calculation if not a normal float value */
    if (isfinite(curr_value))
    {
        if (!lpf->is_init)
        {
            /* Initialize entire array to the first data point */
            for (i = 0UL; i < LPF_NUM_OF_POINTS; ++i)
            {
                lpf->arr[i] = curr_value;
            }

            lpf->curr_idx = 0UL;
            lpf->sum = curr_value * (float)LPF_NUM_OF_POINTS;
            lpf->is_init = true;
        }
        else
        {
            /* Get the index of the oldest data point, this will also
             * be where the index gets incremented to next. The modulo
             * operation captures if the index must circle back around. */
            lpf->curr_idx = (lpf->curr_idx + 1UL) % LPF_NUM_OF_POINTS;
        }

        lpf->sum -= lpf->arr[lpf->curr_idx];
        lpf->sum += curr_value;
        lpf->arr[lpf->curr_idx] = curr_value;
    }

    return (lpf->sum / ((float)LPF_NUM_OF_POINTS));
}


float calibrate(float pre_cal_value, const float *cal_coeff_arr, uint8_t cal_size)
{
    DEV_ASSERT(cal_coeff_arr);
    DEV_ASSERT(cal_size < 10U);  /* Really, you need more than 9th order -__- */

    uint32_t i;
    float cal_value = 0.0f;
    float x = pre_cal_value;
    float xn = pre_cal_value;

    /* Calibrate the value: C0 + C1x + C2x^2 + ... */
    cal_value = cal_coeff_arr[0];
    for (i = 1UL; i < cal_size; ++i)
    {
        cal_value += cal_coeff_arr[i] * xn;
        xn *= x;
    }

    return cal_value;
}
