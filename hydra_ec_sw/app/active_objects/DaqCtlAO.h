/* Copyright (c) 2017-2020 by Phase Four, Inc. All Rights Reserved. Confidential and Proprietary.
 *
 * DaqCtlAO.h
 */

#ifndef ACTIVE_OBJECTS_DAQCTLAO_H_
#define ACTIVE_OBJECTS_DAQCTLAO_H_

#include "mcu_types.h"
#include "glados.h"
#include "p4_err.h"
#include "ad768x.h"

/* Configure the data acquisition rate to 10ms period (100Hz) system frame rate
 *
 * Note: Placing this value into a PT is not an easy or reliable thing to do. The system has been
 * designed around this value and it is unclear what other dependencies or assumptions have been
 * based off of it. Also there would need to be additional checking added to validate the PT is
 * not trying to set something that will kill the system. Not to mention all the additional
 * testing that would be required to validate setting of this value via PT is safe. */
#define DAQCTLAO_PERIOD_US       10000UL

#define DAQCTLAO_PRIORITY        3UL
#define DAQCTLAO_FIFO_SIZE       6UL

/*This #define is a pressure constant that is used to determine what chebychev function to use.
 * If the pressure is below this value, it will use the low pressure Chebyshev. If above it will use the high pressure Chebyshev.*/
#define CHEBYSHEV_CAlC_SWITCH_PRESSURE_VAL 975.0
#define PROP_MASS_PRESSURE_MIN 1
#define PROP_MASS_PRESSURE_MAXIMUM 3000
#define PROP_MASS_TEMP_MINIMUM 25.0f
#define PROP_MASS_TEMP_MAXIMUM 65.0f

/*Modes of the prop status*/
#define HIGH_RESOLUTION_MODE 2U
#define LOW_RESOLUTION_MODE  1U
#define PROP_NOT_VALID_MODE  0U

/* These values were determined through analysis that the prop mass value should not exceed these maximum or minimum.
 * These values were determined with the following constraints.
 * Temp: 25 - 65 Celsius
 * Pressure: 1 - 3000 PISA
 *
 * NOTE these value are not the min or max rather it is the min and max with margin to make them nice round numbers.
 * This is just to make sure the mass prop calculation is not grossly off.
 */
#define LOWEST_PROP_MASS_VALUE 0.0f
#define HIGHEST_PROP_MASS_VALUE 2500.0f


/* Each of these must be unique values for the entire system */
enum DaqCtlAO_event_names
{
    EVT_TMR_DAQ_START = (DAQCTLAO_PRIORITY << 16U),
    EVT_TMR_DAQ_RFT_TIMEOUT,
    EVT_DAQ_DMA_COMPLETE,
    EVT_DAQ_DATA_READY,
    EVT_DAQ_PROP_HEATED,
};

extern GLADOS_AO_t AO_DaqCtl;
extern GLADOS_Timer_t Tmr_DaqStart;
extern GLADOS_Timer_t Tmr_DaqRftTimeout;

/* Making raw ADC values visible to other functions that handle TLM */
extern AD768x_t Adc0;
extern AD768x_t Adc1;

void DaqCtlAO_init(void);
void DaqCtlAO_push_interrupts(void);
void DaqCtlAO_state(GLADOS_AO_t *this_ao, GLADOS_Event_t *event);
void DaqCtlAO_state_idle(GLADOS_AO_t *this_ao, GLADOS_Event_t *event);
void DaqCtlAO_state_idle_rft_data(GLADOS_AO_t *this_ao, GLADOS_Event_t *event);


/* Public Helper Functions */

void DaqCtrlAO_InitDrivers(void);
void DaqCtlAO_RFT_FuseReset(void);
void DaqCtlAO_RFT_ResetEnable(bool enable);

void DaqCtlAO_Heater_Enable(bool enable);
Cmd_Err_Code_t DaqCtlAO_Heater_Set(float i_mA);
Cmd_Err_Code_t DaqCtlAO_Heater_SetPwmRaw(uint16_t duty_cycle_cnt);
void DaqCtlAO_Heater_FuseReset(void);
void DaqCtlAO_HeatCtl_Enable(bool enable);
Cmd_Err_Code_t DaqCtlAO_HeatCtl_SetTemp(float setpt_degC);

void DaqCtlAO_HighPIso_Enable(bool enable);
Cmd_Err_Code_t DaqCtlAO_HighPIso_Spike(void);
Cmd_Err_Code_t DaqCtlAO_HighPIso_Hold(void);
void DaqCtlAO_HighPIso_Close(void);
Cmd_Err_Code_t DaqCtlAO_HighPIso_Set(float i_mA);
Cmd_Err_Code_t DaqCtlAO_HighPIso_SetPwmRaw(uint16_t duty_cycle_cnt);

void DaqCtlAO_FlowCtl_Enable(bool enable);
Cmd_Err_Code_t DaqCtlAO_FlowCtl_Set(float mdot_setpt);
void DaqCtlAO_Pfcv_DacResetToClose(void);
void DaqCtlAO_Pfcv_DacAbort(void);
Cmd_Err_Code_t DaqCtlAO_Pfcv_Set(float mdot_setpt);
Cmd_Err_Code_t DaqCtlAO_Pfcv_SetCurrent(float i_mA);
Cmd_Err_Code_t DaqCtlAO_Pfcv_SetDacRaw(uint16_t code);

/* Returns the difference between the min and max sccm. The user is
 * expected to call this function multiple times and then utilize
 * the result when ready The reset parameter resets the min and max
 * sccm readings to the current reading. */
float DaqCtlAO_FlowCtrl_Stability(bool reset);

void DaqCtlAO_SetDaqRate(uint32_t period_us);

/* Function to determine if Prop-mass is valid */
uint8_t get_prop_mass_status(void);


float calibrate(float pre_cal_value, const float *cal_coeff_arr, uint8_t cal_size);

#endif /* ACTIVE_OBJECTS_DAQCTLAO_H_ */
