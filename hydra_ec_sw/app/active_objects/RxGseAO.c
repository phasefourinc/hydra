/* Copyright (c) 2017-2020 by Phase Four, Inc. All Rights Reserved. Confidential and Proprietary.
 *
 * RxGseAO.c
 */


#include "RxGseAO.h"
#include "mcu_types.h"
#include "glados.h"
#include "lpuart_driver.h"
#include "clank.h"
#include "ErrAO.h"
#include "TxAO.h"
#include "CmdAO.h"
#include "uart_ring.h"
#include "edma_uart1.h"
#include "default_pt.h"
#include "max_defs.h"
#include "string.h"


/* Store a full message and extra header for initial junk data */
#define RXGSE_MAX_BUF_SIZE   (CLANK_MAX_MSG_SIZE + CLANK_HDR_SIZE)

/* Active Object public interface */

GLADOS_AO_t AO_RxGse;

/* Active Object timers */
GLADOS_Timer_t Tmr_RxGseTimeout;
GLADOS_Timer_t Tmr_RxGseCmdTimeout;
GLADOS_Timer_t Tmr_RxGseDropoutDelay;

static UartRing_Rec_t uart1_ring_buf_rec = {0};


/* Active Object private variables */

static GLADOS_Event_t rxgseao_fifo[RXGSEAO_FIFO_SIZE];
static GLADOS_Event_t rxgse_reinit_evt;

static Clank_Hdr_t rxgse_clank_hdr = {0};

static uint8_t rxgse_buf[RXGSE_MAX_BUF_SIZE] = {0U};
static int16_t rxgse_start_idx = 0U;

static uint8_t uart1_rx_err_sem= 0U;
static uint32_t uart1_rx_err_data = 0UL;


void Uart1_RxCallback(UNUSED void *driverState, uart_event_t event, UNUSED void *userData)
{
    if (event == UART_EVENT_ERROR)
    {
        ++uart1_rx_err_sem;
        uart1_rx_err_data = LPUART1->STAT;
    }

    return;
}


/* Active Object state definitions */

void RxGseAO_init(void)
{
    /* Initialize the Active Object with both its Event FIFO and initial state function */
    GLADOS_AO_Init(&AO_RxGse, RXGSEAO_PRIORITY, rxgseao_fifo, RXGSEAO_FIFO_SIZE, &RxGseAO_state);

    return;
}


void RxGseAO_push_interrupts(void)
{
    if (true == UartRing_Process(&uart1_ring_buf_rec))
    {
        GLADOS_AO_PushEvtName((GLADOS_AO_t *)&uart1_ring_buf_rec, &AO_RxGse, EVT_UART1_RX_DATA);
    }
    if (uart1_ring_buf_rec.dma_rx_err_sem > 0U)
    {
        --uart1_ring_buf_rec.dma_rx_err_sem;

        /* NXP LPUART IRQ driver may halt the DMA receiving since it
         * was created to support their version of the DMA receiving,
         * also the DMA Channel state may be placed into an error
         * state that needs to be cleared for the SDK drivers to work.
         * Therefore, we re-enable the DMA receiving here
         * in the event of an error and re-init reception. */
        UartRing_StopBuffer(&uart1_ring_buf_rec);
        EdmaUart1_Init();
        UartRing_ResetBuffer(&uart1_ring_buf_rec);
        UartRing_StartBuffer(&uart1_ring_buf_rec);

        /* Only report the error upon first detection. Subsequent errors
         * that are detected and have not completed the isolation dropout
         * delay are not logged.
         * Note: Grabbing the DMA Error Status reg outside of the interrupt that it
         * occurred may change from when it actually gets logged but this should
         * suffice for supplementary error data. */
        if (!Tmr_RxGseDropoutDelay.enabled)
        {
            report_error_uint(&AO_RxGse, Errors.Uart1RxDmaError, DO_NOT_SEND_TO_SPACECRAFT, DMA->ES);
        }

        /* Restart the timer even if it has already been kicked off.
         * This acts as a filter to isolate a UART bus error that
         * is still active. Only after the UART bus error has been
         * cleared will receiving resume. */
        GLADOS_Timer_EnableOnce(&Tmr_RxGseDropoutDelay);
    }
    if (uart1_rx_err_sem > 0U)
    {
        --uart1_rx_err_sem;

        /* NXP LPUART IRQ driver halts the DMA receiving since it
         * was created to support their version of the DMA receiving,
         * however we have our own custom one that does not expect to
         * be halted. Therefore, we re-enable the DMA receiving here
         * in the event of an error and re-init reception. */
        UartRing_StopBuffer(&uart1_ring_buf_rec);
        EdmaUart1_Init();
        UartRing_ResetBuffer(&uart1_ring_buf_rec);
        UartRing_StartBuffer(&uart1_ring_buf_rec);

        /* Only report the error upon first detection. Subsequent errors
         * that are detected and have not completed the isolation dropout
         * delay are not logged */
        if (!Tmr_RxGseDropoutDelay.enabled)
        {
            report_error_uint(&AO_RxGse, Errors.Uart1RxError, DO_NOT_SEND_TO_SPACECRAFT, uart1_rx_err_data);
            uart1_rx_err_data = 0UL;
        }

        /* Restart the timer even if it has already been kicked off.
         * This acts as a filter to isolate a UART bus error that
         * is still active. Only after the UART bus error has been
         * cleared will receiving resume. */
        GLADOS_Timer_EnableOnce(&Tmr_RxGseDropoutDelay);
    }

    return;
}


void RxGseAO_state(GLADOS_AO_t *this_ao, GLADOS_Event_t *event)
{
    DEV_ASSERT(this_ao);
    DEV_ASSERT(event);

    switch(event->name)
    {
        /* GLADOS_ENTRY/GLADOS_EXIT optional event for each state */
        case GLADOS_ENTRY:
            /* This determines the slowest response time for a received command */
            GLADOS_Timer_Init(&Tmr_RxGseTimeout, MAX_RX_PKT_TIMEOUT_US, EVT_TMR_RXGSE_TIMEOUT);
            GLADOS_Timer_Subscribe_AO(&Tmr_RxGseTimeout, this_ao);

            GLADOS_Timer_Init(&Tmr_RxGseCmdTimeout, MAX_CMD_TIMEOUT_US, EVT_TMR_RXGSE_CMD_TIMEOUT);
            GLADOS_Timer_Subscribe_AO(&Tmr_RxGseCmdTimeout, this_ao);

            /* This determines the isolation delay of a UART Rx error before considering
             * the error unique. This is necessary as UART errors are likely to occur
             * very rapidly. UART errors that occur within 10ms are all considered 1 error. */
            GLADOS_Timer_Init(&Tmr_RxGseDropoutDelay, PT->rxgse_dropout_delay_us, EVT_TMR_RXGSE_DROPOUT_DELAY);
            GLADOS_Timer_Subscribe_AO(&Tmr_RxGseDropoutDelay, this_ao);

            /* Create a self event used as a consistent way to signal the
             * restarting of the Rx process */
            GLADOS_Event_New(&rxgse_reinit_evt, EVT_RXGSE_REINIT);

            (void)LPUART_DRV_InstallRxCallback(INST_LPUART1, &Uart1_RxCallback, NULL);

            EdmaUart1_Init();

            UartRing_Init(&uart1_ring_buf_rec, LPUART1, EDMA_UART1_RX_CH_NUM,
                                  &edma_uart1_ring_buffer[0], sizeof(edma_uart1_ring_buffer));

            UartRing_StartBuffer(&uart1_ring_buf_rec);

            GLADOS_STATE_TRAN_TO_CHILD(&RxGseAO_state_rx_reinit);
            break;

        case GLADOS_EXIT:
            break;

        case EVT_RXGSE_REINIT:
        case EVT_TMR_RXGSE_DROPOUT_DELAY:
            GLADOS_STATE_TRAN_TO_CHILD(&RxGseAO_state_rx_reinit);
            break;

        default:
            /* Top level state, skip undefined Events */
            break;
    }

    return;
}


void RxGseAO_state_rx_reinit(GLADOS_AO_t *this_ao, GLADOS_Event_t *event)
{
    DEV_ASSERT(this_ao);
    DEV_ASSERT(event);

    switch(event->name)
    {
        /* GLADOS_ENTRY/GLADOS_EXIT optional event for each state */
        case GLADOS_ENTRY:
            /* Push a reinit event to self while allowing re-arbitration of AOs.
             * The reason for having this separate state is that this state will
             * catch edge conditions.
             *
             * E.g. If a timeout occurs, and immediately afterward the UART Rx interrupt
             * signals it has received data. The AO FIFO has 2 events (timeout, uart_rx).
             * The timeout event is handled and transitions to this state, posting
             * EVT_RXGSE_START_RX. The FIFO has 2 events again (uart_rx, start_rx).
             * The UART Rx event will be ignored here and next start Rx event kicks off
             * a new receive. */
            GLADOS_AO_PUSH(this_ao, EVT_RXGSE_START_RX);
            break;

        case GLADOS_EXIT:
            break;

        case EVT_UART1_RX_DATA:
        case EVT_TMR_RXGSE_TIMEOUT:
        case EVT_TMR_RXGSE_CMD_TIMEOUT:
            /* Ignore these since they may occur here in edge cases */
            break;

        case EVT_RXGSE_START_RX:
            GLADOS_STATE_TRAN_TO_SIBLING(&RxGseAO_state_rx_start);
            break;

        default:
            GLADOS_STATE_CHECK_PARENT(&RxGseAO_state);
            break;
    }

    return;
}


void RxGseAO_state_rx_start(GLADOS_AO_t *this_ao, GLADOS_Event_t *event)
{
    DEV_ASSERT(this_ao);
    DEV_ASSERT(event);

    static uint8_t junk_cnt = 0U;

    int16_t start_idx;

    switch(event->name)
    {
        /* GLADOS_ENTRY/GLADOS_EXIT optional event for each state */
        case GLADOS_ENTRY:
            memset(&rxgse_buf[0], 0, sizeof(rxgse_buf));
            memset(&rxgse_clank_hdr, 0, sizeof(Clank_Hdr_t));
            UartRing_ReadBuffer(&uart1_ring_buf_rec, &rxgse_buf[0], CLANK_HDR_SIZE);
            /* Wait for Ring Buffer to signal UART Rx event */
            break;

        case GLADOS_EXIT:
            break;

        case EVT_UART1_RX_DATA:
            start_idx = Clank_GetStartIdx(&rxgse_buf[0], CLANK_HDR_SIZE);

            /* If no start byte is detected */
            if (start_idx < 0)
            {
                junk_cnt++;
                if (junk_cnt > 2U)
                {
                    junk_cnt = 0U;
                    report_error_uint(this_ao, Errors.SyncBytesError, SEND_TO_SPACECRAFT, 0UL);
                }

                /* Restart the read */
                GLADOS_AO_PUSH_EVT_SELF(&rxgse_reinit_evt);
            }
            else
            {
                junk_cnt = 0U;

                /* Store the start of the message in the rxgse_buf */
                rxgse_start_idx = start_idx;

                /* Move to the next state to receive the rest of the header */
                GLADOS_STATE_TRAN_TO_SIBLING(&RxGseAO_state_rx_data);
            }
            break;

        default:
            GLADOS_STATE_CHECK_PARENT(&RxGseAO_state);
            break;
    }

    return;
}


void RxGseAO_state_rx_data(GLADOS_AO_t *this_ao, GLADOS_Event_t *event)
{
    DEV_ASSERT(this_ao);
    DEV_ASSERT(event);

    bool valid_hdr;

    switch(event->name)
    {
        /* GLADOS_ENTRY/GLADOS_EXIT optional event for each state */
        case GLADOS_ENTRY:
            GLADOS_Timer_EnableOnce(&Tmr_RxGseTimeout);

            /* Read the rest of the header, if header is already read then still proceed and
             * have the ring buffer signal that the data read has completed */
            UartRing_ReadBuffer(&uart1_ring_buf_rec, &rxgse_buf[CLANK_HDR_SIZE], rxgse_start_idx);

            /* Wait for Ring Buffer to signal UART Rx event */
            break;

        case GLADOS_EXIT:
            GLADOS_Timer_Disable(&Tmr_RxGseTimeout);
            break;

        case EVT_TMR_RXGSE_TIMEOUT:
            report_error_uint(this_ao, Errors.NotEnoughBytes, DO_NOT_SEND_TO_SPACECRAFT, 0UL);
            TxAO_SendMsg_Ack(this_ao, PORT_GSE, rxgse_clank_hdr.Src_Address, rxgse_clank_hdr.Pkt_Seq_Cnt,
                                                    rxgse_clank_hdr.MsgID, Errors.NotEnoughBytes->error_id);

            /* Restart the read */
            GLADOS_AO_PUSH_EVT_SELF(&rxgse_reinit_evt);
            break;

        case EVT_UART1_RX_DATA:
            /* At this point, the full header should have been received into the rxgse_buf
             * starting at rxgse_start_idx */
            Clank_GetHeader(&rxgse_buf[rxgse_start_idx], &rxgse_clank_hdr);

            valid_hdr = Clank_CheckHeaderInfo(&rxgse_clank_hdr);
            if (!valid_hdr)
            {
                /* If the header is invalid, send a NACK */
                report_error_uint(this_ao, Errors.MalformedHeader, DO_NOT_SEND_TO_SPACECRAFT, 0UL);
                TxAO_SendMsg_Ack(this_ao, PORT_GSE, rxgse_clank_hdr.Src_Address, rxgse_clank_hdr.Pkt_Seq_Cnt,
                                                        rxgse_clank_hdr.MsgID, Errors.MalformedHeader->error_id);

                /* Restart the read */
                GLADOS_AO_PUSH_EVT_SELF(&rxgse_reinit_evt);
            }
            else
            {
                /* Otherwise, transition to receive the payload data */
                GLADOS_STATE_TRAN_TO_CHILD(&RxGseAO_state_rx_data_payload);
            }
            break;

        default:
            GLADOS_STATE_CHECK_PARENT(&RxGseAO_state);
            break;
    }

    return;
}


void RxGseAO_state_rx_data_payload(GLADOS_AO_t *this_ao, GLADOS_Event_t *event)
{
    DEV_ASSERT(this_ao);
    DEV_ASSERT(event);

    bool crc_match;
    GLADOS_Event_t cmd_evt;
    CmdAO_Data_t cmd_data;

    switch(event->name)
    {
        /* GLADOS_ENTRY/GLADOS_EXIT optional event for each state */
        case GLADOS_ENTRY:
            /* At this point, the full Clank header has been received and validated in rxgse_buf
             * with the message starting at rxgse_start_idx
             *
             * Read the payload data and 2 CRC bytes, the offset into the rxgse_buf is exactly a clank
             * header after the message start index.
             *
             * Note: The requested data length is guaranteed to be less than the size of the ring buffer
             * because Pkt_DataLen is an 8-bit value and ring buffer is guaranteed to be larger than that */
            UartRing_ReadBuffer(&uart1_ring_buf_rec,
                                    &rxgse_buf[rxgse_start_idx + CLANK_HDR_SIZE], rxgse_clank_hdr.Pkt_DataLen + 2UL);
            /* Wait for Ring Buffer to signal UART Rx event */
            break;

        case GLADOS_EXIT:
            break;

        case EVT_UART1_RX_DATA:
            GLADOS_Timer_Disable(&Tmr_RxGseTimeout);

            crc_match = Clank_CheckCrc(&rxgse_buf[rxgse_start_idx]);

            /* Check the transmit mask here after the full packet has been received, that way
             * the full message can be ignored if it was not intended for this address. This
             * removes the need to handle any junk payload data in that scenario. Also do this
             * check prior to the CRC since only the intended destination hardware should
             * be responding to an invalid CRC. */
            if ((rxgse_clank_hdr.Trans_Mask & MAX_CLANK_RX_MASK_PCU) == 0x0000U)
            {
                /* In a multi-thruster system on the same bus, it is desirable to completely
                 * ignore messages intended for another thruster. However, this is currently
                 * undesirable since a multi-thruster system is not yet in the immediate future.
                 * For a good product, it is better to ensure we respond in the case that noise
                 * accidentally flips the transmit mask. */
                TxAO_SendMsg_Ack(this_ao, PORT_GSE, rxgse_clank_hdr.Src_Address, rxgse_clank_hdr.Pkt_Seq_Cnt,
                                                        rxgse_clank_hdr.MsgID, Errors.TransmitMaskError->error_id);

                /* Restart the read */
                GLADOS_AO_PUSH_EVT_SELF(&rxgse_reinit_evt);
            }
            else if (!crc_match)
            {
                ++MaxData.rx_crc_fail_cnt;
                report_error_uint(this_ao, Errors.ComRxCrcError, DO_NOT_SEND_TO_SPACECRAFT, MaxData.rx_crc_fail_cnt);
                TxAO_SendMsg_Ack(this_ao, PORT_GSE, rxgse_clank_hdr.Src_Address, rxgse_clank_hdr.Pkt_Seq_Cnt,
                                                        rxgse_clank_hdr.MsgID, Errors.ComRxCrcError->error_id);

                /* Restart the read */
                GLADOS_AO_PUSH_EVT_SELF(&rxgse_reinit_evt);
            }
            else
            {
                /* Prepare a command event
                 * Note: Rx AO must keep data untouched in the Rx buffer until
                 * CmdAO signals that it has completed processes */
                cmd_data.port      = (uint8_t)PORT_GSE;
                cmd_data.seq_id    = rxgse_clank_hdr.Pkt_Seq_Cnt;
                cmd_data.src_id    = rxgse_clank_hdr.Src_Address;
                cmd_data.dest_mask   = rxgse_clank_hdr.Trans_Mask;
                cmd_data.msg_id    = rxgse_clank_hdr.MsgID;
                cmd_data.data_ptr  = (uint8_t *)&rxgse_buf[rxgse_start_idx + CLANK_HDR_SIZE];
                cmd_data.data_size = rxgse_clank_hdr.Pkt_DataLen;

                /* Send command event to CmdAO */
                GLADOS_Event_New_Data(&cmd_evt, EVT_RXGSE_CMD_RCVD, (uint8_t *)&cmd_data, sizeof(CmdAO_Data_t));
                GLADOS_AO_PUSH_EVT(&AO_Cmd, &cmd_evt);

                /* Transition to a state to await a signal from CmdAO that indicates
                 * it has completed processing (and sending Ack/Nack) */
                GLADOS_STATE_TRAN_TO_PARENT(&RxGseAO_state_rx_data);
                GLADOS_STATE_TRAN_TO_SIBLING(&RxGseAO_state_rx_cmd_ack);
            }
            break;

        default:
            GLADOS_STATE_CHECK_PARENT(&RxGseAO_state_rx_data);
            break;
    }

    return;
}


void RxGseAO_state_rx_cmd_ack(GLADOS_AO_t *this_ao, GLADOS_Event_t *event)
{
    DEV_ASSERT(this_ao);
    DEV_ASSERT(event);

    switch(event->name)
    {
        /* GLADOS_ENTRY/GLADOS_EXIT optional event for each state */
        case GLADOS_ENTRY:
            /* Initiate timeout timer while waiting for Cmd AO
             * to finish processing */
            GLADOS_Timer_EnableOnce(&Tmr_RxGseCmdTimeout);
            break;

        case GLADOS_EXIT:
            GLADOS_Timer_Disable(&Tmr_RxGseCmdTimeout);
            break;

        case EVT_TMR_RXGSE_TIMEOUT:
            /* Ignore this event since it could occur in an edge case */
            break;

        case EVT_TMR_RXGSE_CMD_TIMEOUT:
            /* Do not send error Error AO nor NACK to Tx AO since this
             * is the responsibility of the Cmd AO at this point */
        case EVT_CMD_PROCESS_DONE:
            /* Restart the read */
            GLADOS_AO_PUSH_EVT_SELF(&rxgse_reinit_evt);
            break;

        default:
            GLADOS_STATE_CHECK_PARENT(&RxGseAO_state);
            break;
    }

    return;
}
