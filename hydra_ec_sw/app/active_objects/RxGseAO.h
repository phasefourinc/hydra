/* Copyright (c) 2017-2020 by Phase Four, Inc. All Rights Reserved. Confidential and Proprietary.
 *
 * RxGseAO.h
 */

#ifndef ACTIVE_OBJECTS_RXGSEAO_H_
#define ACTIVE_OBJECTS_RXGSEAO_H_

/* Global invariants:
 *  - No constants that might affect reception of data should be placed in the PT, lest run
 *    the risk of having a valid PT loaded and then the user is unable to communicate.
 */

#include "mcu_types.h"
#include "glados.h"
#include "uart_ring.h"

#define RXGSEAO_PRIORITY        11UL
#define RXGSEAO_FIFO_SIZE       6UL


/* Each of these must be unique values for the entire system */
enum RxGseAO_event_names
{
    EVT_TMR_RXGSE_TIMEOUT = (RXGSEAO_PRIORITY << 16U),
    EVT_TMR_RXGSE_CMD_TIMEOUT,
    EVT_UART1_RX_DATA,
    EVT_RXGSE_REINIT,
    EVT_RXGSE_START_RX,
    EVT_RXGSE_CMD_RCVD,
    EVT_TMR_RXGSE_DROPOUT_DELAY
};

extern GLADOS_AO_t AO_RxGse;
extern GLADOS_Timer_t Tmr_RxGseTimeout;
extern GLADOS_Timer_t Tmr_RxGseCmdTimeout;
extern GLADOS_Timer_t Tmr_RxGseDropoutDelay;

void RxGseAO_init(void);
void RxGseAO_push_interrupts(void);
void RxGseAO_state(GLADOS_AO_t *this_ao, GLADOS_Event_t *event);
void RxGseAO_state_rx_reinit(GLADOS_AO_t *this_ao, GLADOS_Event_t *event);
void RxGseAO_state_rx_start(GLADOS_AO_t *this_ao, GLADOS_Event_t *event);
void RxGseAO_state_rx_data(GLADOS_AO_t *this_ao, GLADOS_Event_t *event);
void RxGseAO_state_rx_data_payload(GLADOS_AO_t *this_ao, GLADOS_Event_t *event);
void RxGseAO_state_rx_cmd_ack(GLADOS_AO_t *this_ao, GLADOS_Event_t *event);

#endif /* ACTIVE_OBJECTS_RXGSEAO_H_ */
