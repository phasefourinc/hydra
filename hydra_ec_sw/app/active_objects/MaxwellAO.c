/* Copyright (c) 2017-2020 by Phase Four, Inc. All Rights Reserved. Confidential and Proprietary.
 *
 * MaxwellAO.c
 */

#include "MaxwellAO.h"
#include "mcu_types.h"
#include "glados.h"
#include "ErrAO.h"
#include "CmdAO.h"
#include "DaqCtlAO.h"
#include "RftAO.h"
#include "DataAO.h"
#include "max_data.h"
#include "rft_data.h"
#include "ad768x.h"
#include "default_pt.h"
#include "fidr.h"
#include "math.h"
#include "aux_driver.h"
#include "bist.h"

#define MAXAO_RFT_MAX_PARAM_SIZE    12U
#define SEC_PER_HOUR                3600.0f
#define PP_VSET_MARGIN              5.0f /*The margin we compare against to see if the push-pull voltage is the correct range we asked it to be */
#define PP_RAMP2_DELAY_US           1000000UL  /* Additional delay to permit PP Ramp 2 to complete */

GLADOS_AO_t AO_Maxwell;
GLADOS_Timer_t Tmr_MaxSafeSeq;
GLADOS_Timer_t Tmr_MaxThrustSeq;
GLADOS_Timer_t Tmr_MaxThrustTimeout;

#ifdef DUMMY_LOAD_STUBS
#include "rft_data.h"
GLADOS_Timer_t Tmr_MaxStub1sec;
float pos_or_neg = -1.0f;
#define HEAT_STUB_TOLERANCE 0.750f
#define MFS_FLOW_STUB_TOLERANCE 0.03f
#define PLOWP_STUB_TOLERANCE .200f
#endif

/* Active Object public interface */


/* Active Object private variables */
static GLADOS_Event_t max_fifo[MAXAO_FIFO_SIZE];

static uint8_t maxao_rft_params[MAXAO_RFT_MAX_PARAM_SIZE] = {0U};

static uint32_t thrust_dur_hrs_cnt = 0UL;
static uint32_t curr_thrust_dur_hrs_cnt = 0UL;

/* Active Object state definitions */

void MaxAO_init(void)
{
    /* Initialize the Active Object with both its Event FIFO and initial state function */
    GLADOS_AO_Init(&AO_Maxwell, MAXAO_PRIORITY, max_fifo, MAXAO_FIFO_SIZE, &MaxAO_state);

    return;
}


void MaxAO_state(GLADOS_AO_t *this_ao, GLADOS_Event_t *event)
{
    DEV_ASSERT(this_ao);
    DEV_ASSERT(event);

    switch(event->name)
    {
        /* GLADOS_ENTRY/GLADOS_EXIT optional event for each state */
        case GLADOS_ENTRY:
            /* Configure the abort sequence timer to 40ms for each step */
            GLADOS_Timer_Init(&Tmr_MaxSafeSeq, PT->max_safe_seq_delay_us, EVT_TMR_NEXT_SAFE_SEQ);
            GLADOS_Timer_Subscribe_AO(&Tmr_MaxSafeSeq, this_ao);

            /* Arbitrary number here since set by each sequence accordingly before use */
            GLADOS_Timer_Init(&Tmr_MaxThrustSeq, 1000000UL, EVT_TMR_NEXT_THR_SEQ);
            GLADOS_Timer_Subscribe_AO(&Tmr_MaxThrustSeq, this_ao);

            /* Arbitrary number here since set by thrust config command before use */
            GLADOS_Timer_Init(&Tmr_MaxThrustTimeout, 1000000UL, EVT_TMR_MAX_THRUST_TIMEOUT);
            GLADOS_Timer_Subscribe_AO(&Tmr_MaxThrustTimeout, this_ao);

#ifdef DUMMY_LOAD_STUBS
            GLADOS_Timer_Init(&Tmr_MaxStub1sec, 250000UL, EVT_TMR_MAX_STUB_250MS);
            GLADOS_Timer_Subscribe_AO(&Tmr_MaxStub1sec, this_ao);
            GLADOS_Timer_EnableContinuous(&Tmr_MaxStub1sec);
#endif

            /* Initialize the system into a SAFE state */
            GLADOS_STATE_TRAN_TO_CHILD(&MaxAO_state_safe);
            break;

        case GLADOS_EXIT:
            break;

        case EVT_CMD_MAX_TO_IDLE:
        case EVT_CMD_MAX_TO_THRUST:
        case EVT_CMD_MAX_TO_MANUAL:
        case EVT_CMD_MAX_TO_WILD_WEST:
        case EVT_CMD_MAX_TO_SW_UPDATE:
            /* Signal to CmdAO that the requested state transition is invalid */
            GLADOS_AO_PUSH(&AO_Cmd, EVT_MAX_STATE_INVALID);
            break;

        case EVT_ERR_MAX_TO_SAFE:
        case EVT_CMD_MAX_TO_SAFE:
            GLADOS_STATE_TRAN_TO_CHILD(&MaxAO_state_safe);
            break;

#ifdef DUMMY_LOAD_STUBS
        case EVT_TMR_MAX_STUB_250MS:
            /* Update stubbed values unless overridden */

            pos_or_neg *= -1.0f;
            update_nominal_telm_stubs();

            /* These numbers come from a calculation of pressure increase for a density of 1500*/
            /* For information on the calculation go here https://docs.google.com/spreadsheets/d/1l8JJqytCEUqOK2CWQYfAjgZFSmwtgUqx6ud_yyM2RbY/edit#gid=553813290 */
            MaxData.pprop = (float)23.7f*MaxData.tprop + 526;


            ++MaxData.stub_curr_low_pwr_sec_cnt;
            if (MaxData.stub_curr_low_pwr_sec_cnt > MaxData.stub_needs_reset_sec_cnt)
            {
                MaxData.needs_reset_flag = true;
            }


            TcData.tpl += (27.0f  - TcData.tpl) / 192.0f; /* It will ramp up to 31 and stay there. Was divided by 48 but since we increase by 4 times it is now 192.*/
            break;
#endif

        default:
            /* Top level state, skip undefined Events */
            break;
    }

    return;
}


void MaxAO_state_safe(GLADOS_AO_t *this_ao, GLADOS_Event_t *event)
{
    DEV_ASSERT(this_ao);
    DEV_ASSERT(event);

    static uint8_t safe_seq_cnt = 0U;

    uint16_t error_code;

    switch(event->name)
    {
        /* GLADOS_ENTRY/GLADOS_EXIT optional event for each state */
        case GLADOS_ENTRY:
            safe_seq_cnt = 0U;
            MaxData.state = MAX_SAFE;
            MaxData.max_busy_flag  = true;
            GLADOS_AO_PUSH(&AO_Cmd, EVT_MAX_STATE_IN_SAFE);

            /* Signal to RftAO to disable all actuators immediately due to sensitive
             * high voltage electronics. This also helps ensure that thrust events
             * are ended in a timely manner. */
            RftAO_push_cmd_request(this_ao, RFT_CMD_INTERNAL, ABORT, NULL, 0U, 0U);

            GLADOS_Timer_Set(&Tmr_MaxSafeSeq, PT->max_safe_seq_delay_us);
            GLADOS_Timer_EnableContinuous(&Tmr_MaxSafeSeq);
            break;

        case GLADOS_EXIT:
            GLADOS_Timer_Disable(&Tmr_MaxSafeSeq);
            break;

        case EVT_RFT_ACK:
        case EVT_RFT_NACK:
            /* No error is logged in the event that the RFT ABORT command fails after the
             * retry attempts. Doing so avoids any recursive errors due to failed comm and
             * does not re-report errors when the RFT is not connected. */
            break;

        case EVT_TMR_NEXT_SAFE_SEQ:
            /* Increment to the next sequence */
            ++safe_seq_cnt;

            switch(safe_seq_cnt)
            {
                case 1U:
                default:
                    /* The first step in the SAFE sequence.
                     * Note: Use this case as default to catch unexpectedly
                     * large counts. If it is large, restart the sequence. */
                    DEV_ASSERT(safe_seq_cnt == 1U);
                    safe_seq_cnt = 1U;

                    /* Reassert RftAO to disable all actuators */
                    RftAO_push_cmd_request(this_ao, RFT_CMD_INTERNAL, ABORT, NULL, 0U, 0U);

                    /* Disable Heaters and Heater Control SW mechanism */
                    DaqCtlAO_HeatCtl_Enable(false);

                    break;

                case 2U:

                    /* Disable Flow Control SW mechanism */
                    DaqCtlAO_FlowCtl_Enable(false);

                    /* Reset the DAC FlexIO SPI transfer and
                     * toggle DAC reset pin, returns DAC to 2.5V
                     * internal reference and output 0V. This is
                     * more reliable than commanding over SPI and
                     * ensures that FlexIO SPI always gets into a
                     * known state. */
                    DaqCtlAO_Pfcv_DacAbort();
                    DaqCtlAO_Pfcv_DacResetToClose();
                    timer32_delay_us(1UL);
                    DaqCtlAO_Pfcv_SetCurrent(0.0f);

                    break;

                case 3U:
                    /* Close High Pressure Isolation Valve */
                    DaqCtlAO_HighPIso_Enable(false);
                    DaqCtlAO_HighPIso_Close();

#ifdef DUMMY_LOAD_STUBS
                    MaxData.plowp = 31.3f; /* Was 34.5 */
                    MaxData.stub_plowp_setpoint = 31.3f;
                    MaxData.mfs_mdot = MaxData.flow_ctrl_mdot_fb;
#endif


                    /* Reset Fault Fuses that are set now that the full system
                     * has been safed. If the fault condition is still not cleared,
                     * then they will be triggered. */
                    if (MaxData.heater_fault)
                    {
                        DaqCtlAO_Heater_FuseReset();
                    }

                    if (MaxData.rft_fault)
                    {
                        DaqCtlAO_RFT_FuseReset();
                    }

                    /* Abort any data recording, dumping, updating in progress */
                    GLADOS_AO_PUSH(&AO_Data, EVT_MAX_DATA_ABORT);
                    DataAO_SetRecord_Nand(false);

                    /* Transfer Errors to Flash via ErrAO */
                    GLADOS_AO_PushEvtName(&AO_Maxwell, &AO_Err, EVT_TRANSFER_TO_NVM);

                    /* Transition to LOW_PWR state */
                    GLADOS_STATE_TRAN_TO_SIBLING(&MaxAO_state_low_pwr);
                    break;
            }

            /* Avoids compiler warning */
            (void)error_code;
            break;

        case EVT_CMD_MAX_TO_SAFE:
            /* Already safe-ing, signal this to CmdAO */
            GLADOS_AO_PUSH(&AO_Cmd, EVT_MAX_STATE_IN_SAFE);
            break;

        case EVT_ERR_MAX_TO_SAFE:
            /* Ignore Errors signaling to go to safe since already safe-ing */
            break;

        default:
            GLADOS_STATE_CHECK_PARENT(&MaxAO_state);
            break;
    }

    return;
}


void MaxAO_state_low_pwr(GLADOS_AO_t *this_ao, GLADOS_Event_t *event)
{
    DEV_ASSERT(this_ao);
    DEV_ASSERT(event);

    switch(event->name)
    {
        /* GLADOS_ENTRY/GLADOS_EXIT optional event for each state */
        case GLADOS_ENTRY:
            MaxData.state = MAX_LOW_PWR;
            MaxData.max_busy_flag  = false;
            GLADOS_AO_PUSH(&AO_Cmd, EVT_MAX_STATE_IN_LOW_PWR);
            break;

        case GLADOS_EXIT:
            break;

        case EVT_CMD_MAX_TO_IDLE:
            if (MaxData.using_default_pt || fidr_has_active_fault())
            {
                /* If using the default PT (likely with incorrect calibrations)
                 * or a fault is active, reject the command and do not transition */
                GLADOS_AO_PUSH(&AO_Cmd, EVT_MAX_ERR_ACTIVE);
            }
            else
            {
                GLADOS_STATE_TRAN_TO_SIBLING(&MaxAO_state_idle);
            }
            break;

        case EVT_CMD_MAX_TO_MANUAL:
            GLADOS_STATE_TRAN_TO_SIBLING(&MaxAO_state_manual);
            break;

        case EVT_CMD_MAX_TO_SW_UPDATE:
            GLADOS_STATE_TRAN_TO_SIBLING(&MaxAO_state_sw_update);
            break;

        case EVT_CMD_MAX_TO_BIST:
            GLADOS_STATE_TRAN_TO_SIBLING(&MaxAO_state_bist);
            break;

        default:
            GLADOS_STATE_CHECK_PARENT(&MaxAO_state);
            break;
    }

    return;
}


void MaxAO_state_sw_update(GLADOS_AO_t *this_ao, GLADOS_Event_t *event)
{
    DEV_ASSERT(this_ao);
    DEV_ASSERT(event);

    switch(event->name)
    {
        /* GLADOS_ENTRY/GLADOS_EXIT optional event for each state */
        case GLADOS_ENTRY:
            MaxData.state = MAX_SW_UPDATE;
            MaxData.max_busy_flag  = false;

            /* Set slower frame rate and increase the RFT request timeout
             * to permit faster, more reliable SW loading ops. These are
             * returned when SW_UPDATE is exited. */
            DaqCtlAO_SetDaqRate(PT->max_swup_daq_period_us);
            RftAO_adjust_request_timeout(PT->max_swup_rft_timeout_us);

            /* Ensure that the Thr Ctrl follows suit since a load could occur through
             * internal commanding. Doing it for SW_UPDATE seems to be okay since it
             * can only exit through ABORT which syncs the Thr Ctrl every time. */
            RftAO_push_cmd_request(this_ao, RFT_CMD_INTERNAL, SW_UPDATE, NULL, 0U, 0U);

            GLADOS_AO_PUSH(&AO_Cmd, EVT_MAX_STATE_IN_SW_UPDATE);
            GLADOS_AO_PUSH(&AO_Data, EVT_MAX_STATE_IN_SW_UPDATE);
            break;

        case GLADOS_EXIT:
            /* Return to normal rates */
            DaqCtlAO_SetDaqRate(DAQCTLAO_PERIOD_US);
            RftAO_adjust_request_timeout(PT->rft_rqst_timeout_us);
            break;

        case EVT_CMD_MAX_TO_SW_UPDATE:
            GLADOS_AO_PUSH(&AO_Cmd, EVT_MAX_STATE_IN_SW_UPDATE);
            break;

        case EVT_RFT_ACK:
        case EVT_RFT_NACK:
            /* Ignore any RFT NACK responses in the event that it is not connected.
             * If the user attempts to load to the Thr Ctrl through the prop ctrl
             * and it is not connected, then the other commands will handle sending
             * a NACK back to the spacecraft. */
            break;

        default:
            GLADOS_STATE_CHECK_PARENT(&MaxAO_state);
            break;
    }

    return;
}


void MaxAO_state_bist(GLADOS_AO_t *this_ao, GLADOS_Event_t *event)
{
    DEV_ASSERT(this_ao);
    DEV_ASSERT(event);

    static uint8_t bist_seq_cnt = 0U;

    switch(event->name)
    {
        /* GLADOS_ENTRY/GLADOS_EXIT optional event for each state */
        case GLADOS_ENTRY:
            bist_seq_cnt = 0U;
            MaxData.state = MAX_BIST;
            MaxData.max_busy_flag  = true;
            GLADOS_AO_PUSH(&AO_Cmd, EVT_MAX_STATE_IN_BIST);
            GLADOS_AO_PUSH(&AO_Maxwell, EVT_TMR_NEXT_THR_SEQ);
            break;

        case GLADOS_EXIT:
            GLADOS_Timer_Disable(&Tmr_MaxThrustTimeout);
            GLADOS_Timer_Disable(&Tmr_MaxThrustSeq);
            break;

        case EVT_RFT_ACK:
        case EVT_RFT_NACK:
            /* Successful RFT comm commands are welcome here at any time as there is nothing
             * left to do. Unsuccessful RFT comm commands are also ignored since the individual
             * BIST tests handle checking of the command's success. Also, aborting here due to
             * an error would skip the rest of BIST, which is undesirable. */
            break;

        case EVT_TMR_MAX_THRUST_TIMEOUT:
            report_error_uint(&AO_Maxwell, Errors.BistTimeout, SEND_TO_SPACECRAFT, 0UL);
            GLADOS_STATE_TRAN_TO_SIBLING(&MaxAO_state_safe);
            break;

        case EVT_TMR_NEXT_THR_SEQ:
            /* Increment to the next sequence */
            ++bist_seq_cnt;

            switch(bist_seq_cnt)
            {
#ifdef DUMMY_LOAD_STUBS
                case 1U:
                    /* Use a nominal delay to "execute" the heater test */
                    GLADOS_Timer_Set(&Tmr_MaxThrustSeq, 6000000);
                    GLADOS_Timer_EnableOnce(&Tmr_MaxThrustSeq);
                    break;

                case 2U:
                    /* Provide an example for testing. If bus voltage falls too low it will cause the BIST to fail and abort. */
                    if (MaxData.ebus < 25.0f)
                    {
                        err_type *bist_err = (err_type *)Errors.BistInternEcFail;
                        report_error_float(&AO_Maxwell, bist_err, SEND_TO_SPACECRAFT, MaxData.ebus);
                    }

                    /* Delay for 25 seconds */
                    GLADOS_Timer_Set(&Tmr_MaxThrustSeq, 25000000);
                    GLADOS_Timer_EnableOnce(&Tmr_MaxThrustSeq);
                    break;

                case 3U:
                default:
                    /* No more BIST steps to execute. Transition to SAFE state. */
                    GLADOS_STATE_TRAN_TO_SIBLING(&MaxAO_state_safe);
                    break;
#else
                case 1U:
                    /* Kick off the heater test here since it permits customers to
                     * easily test the highest power draw scenario of the logic bus
                     * immediately after BIST command reception */
                    bist_heater_test(PARTA);
                    GLADOS_Timer_Set(&Tmr_MaxThrustSeq, PT->bist_seq1_delay_us);
                    GLADOS_Timer_EnableOnce(&Tmr_MaxThrustSeq);
                    break;

                case 2U:
                    bist_heater_test(PARTB);
                    GLADOS_Timer_Set(&Tmr_MaxThrustSeq, PT->bist_seq2_delay_us);
                    GLADOS_Timer_EnableOnce(&Tmr_MaxThrustSeq);
                    break;

                case 3U:
                    bist_internal_ec_test();
                    GLADOS_Timer_Set(&Tmr_MaxThrustSeq, PT->bist_seq3_delay_us);
                    GLADOS_Timer_EnableOnce(&Tmr_MaxThrustSeq);
                    break;

                case 4U:
                    bist_internal_tc_test();
                    GLADOS_Timer_Set(&Tmr_MaxThrustSeq, PT->bist_seq4_delay_us);
                    GLADOS_Timer_EnableOnce(&Tmr_MaxThrustSeq);
                    break;

                case 5U:
                    bist_memory_test();
                    GLADOS_Timer_Set(&Tmr_MaxThrustSeq, PT->bist_seq5_delay_us);
                    GLADOS_Timer_EnableOnce(&Tmr_MaxThrustSeq);
                    break;

                case 6U:
                    bist_push_pull_test(PARTA);
                    GLADOS_Timer_Set(&Tmr_MaxThrustSeq, PT->bist_seq6_delay_us);
                    GLADOS_Timer_EnableOnce(&Tmr_MaxThrustSeq);
                    break;

                case 7U:
                    bist_push_pull_test(PARTB);
                    GLADOS_Timer_Set(&Tmr_MaxThrustSeq, PT->bist_seq7_delay_us);
                    GLADOS_Timer_EnableOnce(&Tmr_MaxThrustSeq);
                    break;

                case 8U:
                    bist_push_pull_test(PARTC);
                    GLADOS_Timer_Set(&Tmr_MaxThrustSeq, PT->bist_seq8_delay_us);
                    GLADOS_Timer_EnableOnce(&Tmr_MaxThrustSeq);
                    break;

                case 9U:
                    bist_push_pull_test(PARTD);
                    GLADOS_Timer_Set(&Tmr_MaxThrustSeq, PT->bist_seq9_delay_us);
                    GLADOS_Timer_EnableOnce(&Tmr_MaxThrustSeq);
                    break;

                case 10U:
                    bist_waveform_test(PARTA);
                    GLADOS_Timer_Set(&Tmr_MaxThrustSeq, PT->bist_seq10_delay_us);
                    GLADOS_Timer_EnableOnce(&Tmr_MaxThrustSeq);
                    break;

                case 11U:
                    bist_waveform_test(PARTB);
                    GLADOS_Timer_Set(&Tmr_MaxThrustSeq, PT->bist_seq11_delay_us);
                    GLADOS_Timer_EnableOnce(&Tmr_MaxThrustSeq);
                    break;

                case 12U:
                    bist_waveform_test(PARTC);
                    GLADOS_Timer_Set(&Tmr_MaxThrustSeq, PT->bist_seq12_delay_us);
                    GLADOS_Timer_EnableOnce(&Tmr_MaxThrustSeq);
                    break;

                case 13U:
                    bist_pfcv_test(PARTA);
                    GLADOS_Timer_Set(&Tmr_MaxThrustSeq, PT->bist_seq13_delay_us);
                    GLADOS_Timer_EnableOnce(&Tmr_MaxThrustSeq);
                    break;

                case 14U:
                    bist_pfcv_test(PARTB);
                    GLADOS_Timer_Set(&Tmr_MaxThrustSeq, PT->bist_seq14_delay_us);
                    GLADOS_Timer_EnableOnce(&Tmr_MaxThrustSeq);
                    break;

                case 15U:
                    bist_lp_blowout_test(PARTA);
                    GLADOS_Timer_Set(&Tmr_MaxThrustSeq, PT->bist_seq15_delay_us);
                    GLADOS_Timer_EnableOnce(&Tmr_MaxThrustSeq);
                    break;

                case 16U:
                default:
                    bist_lp_blowout_test(PARTB);
                    /* No more BIST steps to execute. Transition to SAFE state. */
                    GLADOS_STATE_TRAN_TO_SIBLING(&MaxAO_state_safe);
                    break;
#endif
            }
            break;

        default:
            GLADOS_STATE_CHECK_PARENT(&MaxAO_state);
            break;
    }

    return;
}


void MaxAO_state_manual(GLADOS_AO_t *this_ao, GLADOS_Event_t *event)
{
    DEV_ASSERT(this_ao);
    DEV_ASSERT(event);

    Cmd_Err_Code_t error_rec;

    switch(event->name)
    {
        /* GLADOS_ENTRY/GLADOS_EXIT optional event for each state */
        case GLADOS_ENTRY:
            MaxData.state = MAX_MANUAL;
            MaxData.max_busy_flag = false;
            GLADOS_AO_PUSH(&AO_Cmd, EVT_MAX_STATE_IN_MANUAL);
            break;

        case GLADOS_EXIT:
            break;

        case EVT_CMD_MAX_TO_MANUAL:
            GLADOS_AO_PUSH(&AO_Cmd, EVT_MAX_STATE_IN_MANUAL);
            break;

        case EVT_CMD_MAX_TO_IDLE:
            if (MaxData.using_default_pt || fidr_has_active_fault())
            {
                /* If using the default PT (likely with incorrect calibrations)
                 * or a fault is active, reject the command and do not transition */
                GLADOS_AO_PUSH(&AO_Cmd, EVT_MAX_ERR_ACTIVE);
            }
            else
            {
                GLADOS_STATE_TRAN_TO_SIBLING(&MaxAO_state_idle);
            }
            break;

        case EVT_CMD_MAX_TO_WILD_WEST:
            GLADOS_STATE_TRAN_TO_SIBLING(&MaxAO_state_wild_west);
            break;

        case EVT_CMD_MAX_TO_THRUST:
            error_rec = MaxAO_ValidateThrustParams(MaxData.thrust_rf_vset_fb,
                                                   MaxData.thrust_mdot_fb,
                                                   MaxData.thrust_dur_sec_fb);

            if ((MaxData.using_default_pt)                     ||
                (fidr_has_active_fault())                      ||
                (error_rec.reported_error != Errors.StatusSuccess))
            {
                /* If a fault is active, reject the command and
                 * do not transition out of this state */
                GLADOS_AO_PUSH(&AO_Cmd, EVT_MAX_ERR_ACTIVE);
            }
            else
            {
                GLADOS_STATE_TRAN_TO_SIBLING(&MaxAO_state_thrust);
            }
            break;

        default:
            GLADOS_STATE_CHECK_PARENT(&MaxAO_state);
            break;
    }

    return;
}


void MaxAO_state_wild_west(GLADOS_AO_t *this_ao, GLADOS_Event_t *event)
{
    DEV_ASSERT(this_ao);
    DEV_ASSERT(event);

    switch(event->name)
    {
        /* GLADOS_ENTRY/GLADOS_EXIT optional event for each state */
        case GLADOS_ENTRY:
            /* FDIR uses max_state to disable FDIR checking */
            MaxData.state = MAX_WILD_WEST;
            MaxData.max_busy_flag  = false;
            GLADOS_AO_PUSH(&AO_Cmd, EVT_MAX_STATE_IN_WILD_WEST);
            break;

        case GLADOS_EXIT:
            /* FDIR uses max_state to re-enable FDIR checking */
            break;

        case EVT_CMD_MAX_TO_WILD_WEST:
            GLADOS_AO_PUSH(&AO_Cmd, EVT_MAX_STATE_IN_WILD_WEST);
            break;

        case EVT_CMD_MAX_TO_IDLE:
            /* No validating for active errors, we are coming from
             * the Wild West after all */
            GLADOS_STATE_TRAN_TO_SIBLING(&MaxAO_state_idle);
            break;

        case EVT_CMD_MAX_TO_MANUAL:
            GLADOS_STATE_TRAN_TO_SIBLING(&MaxAO_state_manual);
            break;

        case EVT_CMD_MAX_TO_THRUST:
            /* No validating for active errors, we are coming from
             * the Wild West after all */
            GLADOS_STATE_TRAN_TO_SIBLING(&MaxAO_state_thrust);
            break;

        default:
            GLADOS_STATE_CHECK_PARENT(&MaxAO_state);
            break;
    }

    return;
}


void MaxAO_state_idle(GLADOS_AO_t *this_ao, GLADOS_Event_t *event)
{
    DEV_ASSERT(this_ao);
    DEV_ASSERT(event);

    switch(event->name)
    {
        /* GLADOS_ENTRY/GLADOS_EXIT optional event for each state */
        case GLADOS_ENTRY:
            MaxData.state = MAX_IDLE;
            MaxData.max_busy_flag  = true;
            GLADOS_AO_PUSH(&AO_Cmd, EVT_MAX_STATE_IN_IDLE);

            DataAO_SetRecord_Nand(false);
            GLADOS_AO_PUSH(&AO_Data, EVT_MAX_START_RECORDING);

#ifdef DUMMY_LOAD_STUBS
            /* We are heating this up to the max to simulate heating rather then using a PID control */
            DaqCtlAO_Heater_Set(MaxData.stub_heater_setpt_mA);
            DaqCtlAO_Heater_Enable(true); /* Heater Enable True */


            MaxData.heat_ctrl_temp_fb = PT->heater_ctl_setpt_degC;
            MaxData.stub_max_heater_dur_250ms = 240;
            MaxData.stub_heat_temp_inc = (float)(MaxData.heat_ctrl_temp_fb - MaxData.tprop) / (float)MaxData.stub_max_heater_dur_250ms;
            MaxData.stub_ttank_temp_setpoint = MaxData.heat_ctrl_temp_fb;
            static uint16_t heating_completed_cnt = 0;

#else
            /* Enable Heater Control */
            DaqCtlAO_HeatCtl_Enable(true);

            /* Start Aux Driver */
            start_aux_driver();
#endif
            break;

        case GLADOS_EXIT:
            stop_aux_driver(true);
            break;

        case EVT_CMD_MAX_TO_IDLE:
            GLADOS_AO_PUSH(&AO_Cmd, EVT_MAX_STATE_IN_IDLE);
            break;

        case EVT_CMD_MAX_TO_MANUAL:
            GLADOS_STATE_TRAN_TO_SIBLING(&MaxAO_state_manual);
            break;

        case EVT_CMD_MAX_TO_THRUST:
            GLADOS_AO_PUSH(&AO_Cmd, EVT_MAX_STATE_BUSY);
            break;

#ifdef DUMMY_LOAD_STUBS
        case EVT_TMR_MAX_STUB_250MS:


            TcData.tpl += (27.0f  - TcData.tpl) / 192.0f;

            /*Update plowp Stub */
            MaxData.plowp = add_stub_noise(MaxData.plowp, MaxData.stub_plowp_setpoint, PLOWP_STUB_TOLERANCE);


            ++MaxData.stub_curr_low_pwr_sec_cnt;
            if (MaxData.stub_curr_low_pwr_sec_cnt > MaxData.stub_needs_reset_sec_cnt)
            {
                MaxData.needs_reset_flag = true;
            }

            /* Prop is primed after the preset number of seconds has elapsed
             * Note: This count does not reset, which simulates that the
             * prop only needs to be conditioned once on each reset. */
            if (MaxData.stub_idle_cnt < MaxData.stub_max_heater_dur_250ms)
            {
                ++MaxData.stub_idle_cnt;
                MaxData.tprop += MaxData.stub_heat_temp_inc;
                MaxData.tmfs  += MaxData.stub_heat_temp_inc;
            }
            /* These numbers come from a calculation of pressure increase for a density of 1500*/
            /* For information on the calculation go here https://docs.google.com/spreadsheets/d/1l8JJqytCEUqOK2CWQYfAjgZFSmwtgUqx6ud_yyM2RbY/edit#gid=553813290 */
            MaxData.pprop = (float)23.7f*MaxData.tprop + 526;


            /* This checks the condition if we need to go to idle ready;*/
            if(((MaxData.tprop >= PT->heater_idle_complete_degC) || (MaxData.tprop >= MaxData.heat_ctrl_temp_fb)))
            {
                ++heating_completed_cnt;

                /* Filter out transient RTD4 samples by requiring multiple readings */
                if (heating_completed_cnt >= PT->heater_ctl_done_sample_cnt)
                {
                    heating_completed_cnt = 0U;
                    GLADOS_STATE_TRAN_TO_CHILD(&MaxAO_state_idle_ready);
                }
            }

            break;
#endif
        case EVT_DAQ_PROP_HEATED:
            GLADOS_STATE_TRAN_TO_CHILD(&MaxAO_state_idle_ready);
            break;

        default:
            GLADOS_STATE_CHECK_PARENT(&MaxAO_state);
            break;
    }

    return;
}


void MaxAO_state_idle_ready(GLADOS_AO_t *this_ao, GLADOS_Event_t *event)
{
    DEV_ASSERT(this_ao);
    DEV_ASSERT(event);

    Cmd_Err_Code_t error_rec;

    switch(event->name)
    {
        /* GLADOS_ENTRY/GLADOS_EXIT optional event for each state */
        case GLADOS_ENTRY:
            MaxData.max_busy_flag = false;

#ifdef DUMMY_LOAD_STUBS
            DaqCtlAO_Heater_Set(0.0f);
            DaqCtlAO_Heater_Enable(false);
#endif
            break;

        case GLADOS_EXIT:
            break;

        case EVT_CMD_MAX_TO_THRUST:
            error_rec = MaxAO_ValidateThrustParams(MaxData.thrust_rf_vset_fb,
                                                   MaxData.thrust_mdot_fb,
                                                   MaxData.thrust_dur_sec_fb);

            /* Note: No check for loaded default PT because it is redundant
             * since IDLE should never be reached if default PT is active */
            if ((fidr_has_active_fault())                      ||
                (error_rec.reported_error != Errors.StatusSuccess))
            {
                /* If a fault is active, reject the command and
                 * do not transition out of this state */
                GLADOS_AO_PUSH(&AO_Cmd, EVT_MAX_ERR_ACTIVE);
            }
            else
            {
                GLADOS_STATE_TRAN_TO_PARENT(&MaxAO_state_idle);
                GLADOS_STATE_TRAN_TO_SIBLING(&MaxAO_state_thrust);
            }
            break;

#ifdef DUMMY_LOAD_STUBS
        case EVT_TMR_MAX_STUB_250MS:


            /* Prop is primed after the preset number of seconds has elapsed
             * Note: This count does not reset, which simulates that the
             * prop only needs to be conditioned once on each reset. */
            if (MaxData.stub_idle_cnt < MaxData.stub_max_heater_dur_250ms)
            {
                ++MaxData.stub_idle_cnt;
                MaxData.tprop += MaxData.stub_heat_temp_inc;
                MaxData.tmfs  += MaxData.stub_heat_temp_inc;
                MaxData.plowp = add_stub_noise(MaxData.plowp, MaxData.stub_plowp_setpoint, PLOWP_STUB_TOLERANCE);
            }
            else
            {
                update_nominal_telm_stubs();

            }


            /* These numbers come from a calculation of pressure increase for a density of 1500*/
            /* For information on the calculation go here https://docs.google.com/spreadsheets/d/1l8JJqytCEUqOK2CWQYfAjgZFSmwtgUqx6ud_yyM2RbY/edit#gid=553813290 */
            MaxData.pprop = (float)23.7f*MaxData.tprop + 526;

            TcData.tpl += (27.0f  - TcData.tpl) / 192.0f;

            ++MaxData.stub_curr_low_pwr_sec_cnt;
            if (MaxData.stub_curr_low_pwr_sec_cnt > MaxData.stub_needs_reset_sec_cnt)
            {
                MaxData.needs_reset_flag = true;
            }

            break;
#endif

        default:
            GLADOS_STATE_CHECK_PARENT(&MaxAO_state_idle);
            break;
    }

    return;
}


void MaxAO_state_thrust(GLADOS_AO_t *this_ao, GLADOS_Event_t *event)
{
    DEV_ASSERT(this_ao);
    DEV_ASSERT(event);

    static uint8_t thrust_seq_cnt = 0U;

    Cmd_Err_Code_t error_code;
    uint32_t raw_bytes;
    uint32_t rft_nack_err_data;
    CmdAO_NackData_t *rft_nack_data;
    float tmp;
    float leftover_sec_f32;
    float max_diff_ipfcv_A;

    switch(event->name)
    {
        /* GLADOS_ENTRY/GLADOS_EXIT optional event for each state */
        case GLADOS_ENTRY:

            /* NOTE: Validation of Thrust Parameters occurs during the command
             * to set them as well as in the states that transition to this
             * function to double-check. It is done there because if it fails,
             * the SW should stay in that state rather than having to transition
             * to THRUST and then back. */

            thrust_seq_cnt = 0U;
            curr_thrust_dur_hrs_cnt = 0UL;
            MaxData.state = MAX_THRUST;
            MaxData.max_busy_flag = true;
            GLADOS_AO_PUSH(&AO_Cmd, EVT_MAX_STATE_IN_THRUST);

            /* If advanced thrust parameters were not configured
             * via the CONFIG_THRUST_ADV, default to PT */
            if (MaxData.thrust_adv_cmd_rcvd == false)
            {
                MaxData.thrust_puff_mdot_fb   = PT->max_ignite_flow_setpt_sccm;
                MaxData.thrust_wf_gen_freq_fb = PT->max_thrust_wf_gen_freq_hz;
                MaxData.thrust_puff_dur_us_fb = PT->max_thrust_seq6_delay_us;
                MaxData.thrust_ignite_us_fb   = PT->max_thrust_seq8_delay_us;
                MaxData.thrust_ignite_vset_fb = PT->max_thrust_push_prep_volt;
                MaxData.thrust_pp_ramp_us_fb  = PT->max_thrust_pp_ramp_time_us;
            }

            /* This is considered sequence 0 in the thrust sequence */

            /* Enabling long-term recording */
             DataAO_SetRecord_Nand(true);

            /* Disable Heaters and Heater Control SW mechanism */
             DaqCtlAO_HeatCtl_Enable(false);

            GLADOS_AO_PUSH(&AO_Data, EVT_MAX_START_RECORDING);

            GLADOS_Timer_Set(&Tmr_MaxThrustSeq, PT->max_thrust_seq0_delay_us);
            GLADOS_Timer_EnableOnce(&Tmr_MaxThrustSeq);

#ifdef DUMMY_LOAD_STUBS

            /* Though the timer reset below seems redundant, this is used to reset the 1 second timer.
            This makes the timer more deterministic and reduces the odds the timer stub will miss thrust cases 4 and 5 in the thrust sequence*/
            GLADOS_Timer_EnableContinuous(&Tmr_MaxStub1sec);
#endif
            break;

        case GLADOS_EXIT:
            MaxData.thrust_rf_vset_fb     = 0.0f;
            MaxData.thrust_mdot_fb        = 0.0f;
            MaxData.thrust_dur_sec_fb     = 0.0f;
            MaxData.thrust_puff_mdot_fb   = 0.0f;
            MaxData.thrust_wf_gen_freq_fb = 0.0f;
            MaxData.thrust_puff_dur_us_fb = 0UL;
            MaxData.thrust_ignite_us_fb   = 0UL;
            MaxData.thrust_ignite_vset_fb = 0.0f;
            MaxData.thrust_pp_ramp_us_fb  = 0UL;
            MaxData.thrust_adv_cmd_rcvd   = false;
            MaxData.thrust_has_ignited    = false;
            MaxData.thrust_steady_state   = false;
            MaxData.thrust_in_hot_fire    = false;
            MaxData.thrust_in_cold_fire   = false;

            GLADOS_Timer_Disable(&Tmr_MaxThrustTimeout);
            GLADOS_Timer_Disable(&Tmr_MaxThrustSeq);

#ifdef DUMMY_LOAD_STUBS


            /* Keep stub timer going after first thrust in order to allow for
             * temps to come back down in low power mode */


            /* Though the timer reset below seems redundant, this is used to reset the 1 second timer.
            So we can sync a good exit after a abort or a successful hotfire */
            GLADOS_Timer_EnableContinuous(&Tmr_MaxStub1sec);
#endif
            break;

        case EVT_RFT_ACK:
            /* Successful RFT comm commands are welcome here at any time */
            break;

        case EVT_RFT_NACK:
            rft_nack_err_data = thrust_seq_cnt;

            /* If the NACK has error aux_data, which could be an internal error code
             * or a NACK status code, then include that 16-bit code in the 2 MSBs of
             * the error data to help troubleshoot RFT comm issues. */
            if (event->has_data && (event->data_size == sizeof(CmdAO_NackData_t)))
            {
                rft_nack_data = (CmdAO_NackData_t *)&event->data[0];
                rft_nack_err_data |= ((rft_nack_data->err_code.aux_data & 0xFFFFUL) << 16U);
            }

            /* This error aborts the thrust operation */
            report_error_uint(&AO_Maxwell, Errors.ThrustRftCommFail, SEND_TO_SPACECRAFT, rft_nack_err_data);
            break;

        case EVT_TMR_MAX_THRUST_TIMEOUT:
            /* Timer is enabled continuously to ensure that curr_thrust_dur_hrs_cnt keeps incrementing
             * even if weird conditions arise to allow us to leave this state eventually no matter what.
             * This assumes that, however, that thrust_dur_hrs_cnt is not set to its max value. Consider
             * reducing this assert to a more reasonable value rather than 490k years. */
            DEV_ASSERT(thrust_dur_hrs_cnt < 0xFFFFFFFFUL);

            ++curr_thrust_dur_hrs_cnt;
            if (thrust_dur_hrs_cnt == 0UL)
            {
                /* If the entire thrust duration fit within one span of the timer,
                 * then we're done here */
                GLADOS_STATE_TRAN_TO_SIBLING(&MaxAO_state_safe);
            }
            else if (curr_thrust_dur_hrs_cnt > thrust_dur_hrs_cnt)
            {
                /* If the leftover time has completed, we're done here */
                GLADOS_STATE_TRAN_TO_SIBLING(&MaxAO_state_safe);
            }
            else if (curr_thrust_dur_hrs_cnt == thrust_dur_hrs_cnt)
            {
                /* The number of hour iterations have been counted, now set the timer
                 * for the leftover time, which is less than 1 hr.
                 * Note: No div by zero here since thrust_dur_hrs_cnt is checked for 0 above
                 * and no neg float to uint32_t cast error since thrust_dur_sec_fb > 0.0 */
                leftover_sec_f32 = MaxData.thrust_dur_sec_fb / (SEC_PER_HOUR * ((float)thrust_dur_hrs_cnt));
                GLADOS_Timer_Disable(&Tmr_MaxThrustTimeout);
                GLADOS_Timer_Set(&Tmr_MaxThrustTimeout, (uint32_t)(leftover_sec_f32 * 1000000.0f));
                GLADOS_Timer_EnableContinuous(&Tmr_MaxThrustTimeout);
            }
            /* Else, there is still at least one more hour to go. Keep going. */
            break;

        case EVT_CMD_MAX_TO_THRUST:
            /* Do not accept another Thrust command while one is already underway */
            GLADOS_AO_PUSH(&AO_Cmd, EVT_MAX_STATE_BUSY);
            break;

        case EVT_TMR_NEXT_THR_SEQ:
            /* Increment to the next sequence */
            ++thrust_seq_cnt;

            switch(thrust_seq_cnt)
            {
                case 1U:
                default:
                    /* The first step in the Thrust sequence.
                     * Note: Use this case as default to catch unexpectedly
                     * large counts. If it is large, restart the sequence. */
                    DEV_ASSERT(thrust_seq_cnt == 1U);
                    thrust_seq_cnt = 1U;

                    /* Spiking HPISO */
                    /*Open early to allow unknown fluidic state and variable line length to reach steady-state before flow control.*/
                    DaqCtlAO_HighPIso_Enable(true);
                    error_code = DaqCtlAO_HighPIso_Spike();
                    if (error_code.reported_error != Errors.StatusSuccess)
                    {
                        /* This error aborts the thrust operation */
                        report_error_uint(&AO_Maxwell, Errors.ThrustHpisoSpikeFail, SEND_TO_SPACECRAFT, error_code.aux_data);
                    }
                     /* Holding current for spike until next sequence*/
                    GLADOS_Timer_Set(&Tmr_MaxThrustSeq, PT->max_thrust_seq1_delay_us);
                    GLADOS_Timer_EnableOnce(&Tmr_MaxThrustSeq);
                    break;

                case 2U:
                    /* Holding HPISO */
                    error_code = DaqCtlAO_HighPIso_Hold();
                    if (error_code.reported_error != Errors.StatusSuccess)
                    {
                        /* This error aborts the thrust operation */
                        report_error_uint(&AO_Maxwell, Errors.ThrustHpisoHoldFail, SEND_TO_SPACECRAFT, error_code.aux_data);
                    }

                    GLADOS_Timer_Set(&Tmr_MaxThrustSeq, PT->max_thrust_seq2_delay_us);
                    GLADOS_Timer_EnableOnce(&Tmr_MaxThrustSeq);
                    break;

                case 3U:
                    /* If we are hot firing set the waveform to the correct frequency*/
                    if (MaxData.thrust_rf_vset_fb > 0.0f)
                    {
                        /* Signals to FDIR that a hot fire is underway so begin monitoring now. */
                        MaxData.thrust_in_hot_fire = true;

                        /* Command the RFT with the expected WF oscillator frequency */
                        raw_bytes = F32_TO_BYTES(MaxData.thrust_wf_gen_freq_fb);
                        maxao_rft_params[0] = (uint8_t)((raw_bytes >> 24U)& 0xFFUL);
                        maxao_rft_params[1] = (uint8_t)((raw_bytes >> 16U)& 0xFFUL);
                        maxao_rft_params[2] = (uint8_t)((raw_bytes >> 8U)& 0xFFUL);
                        maxao_rft_params[3] = (uint8_t)((raw_bytes) & 0xFFUL);
                        RftAO_push_cmd_request(this_ao, RFT_CMD_INTERNAL,RFT_SET_WAV_GEN_FREQ, &maxao_rft_params[0], 4U, 0U);
                    }
                    else
                    {
                        /* Signals to FDIR and system that a cold gas is underway */
                        MaxData.thrust_in_cold_fire = true;
                    }
                    
                    GLADOS_Timer_Set(&Tmr_MaxThrustSeq, PT->max_thrust_seq3_delay_us);
                    GLADOS_Timer_EnableOnce(&Tmr_MaxThrustSeq);
                    break;

                case 4U:
                    /* Hot Fire Sequence Only*/
                    if (MaxData.thrust_in_hot_fire)
                    {
                        /* Validate WF Generator Frequency is set. This should be an exact match,
                         * with the thrust parameter (advanced), however floats make it tricky so
                         * this just checks for within 1% of the expected value. */
                        if ((TcData.wav_gen_freq_fb > (1.01f * MaxData.thrust_wf_gen_freq_fb)) || (TcData.wav_gen_freq_fb < (0.99f * MaxData.thrust_wf_gen_freq_fb)))
                        {
                            /* This error aborts the thrust operation */
                            report_error_float(&AO_Maxwell, Errors.ThrustRftWfGenFreqFail, SEND_TO_SPACECRAFT, TcData.wav_gen_freq_fb);
                        }

                        /* Send RFT command to Enable RF Waveform generator*/
                        maxao_rft_params[0] = 0x01U;
                        RftAO_push_cmd_request(this_ao, RFT_CMD_INTERNAL, RFT_SET_WF_EN, &maxao_rft_params[0], 1U, 0U);
                    }

                    /* Waiting for the thruster controller to set the wf generator */
                    GLADOS_Timer_Set(&Tmr_MaxThrustSeq,  PT->max_thrust_seq4_delay_us);
                    GLADOS_Timer_EnableOnce(&Tmr_MaxThrustSeq);
                    break;

                case 5U:
                    if (MaxData.thrust_in_hot_fire)
                    {
                        /* Validate that the WF Generator is set to enable */
                        if (TcData.gpio_wf_en_fb == false)
                        {
                            /* This error aborts the thrust operation */
                            report_error_uint(&AO_Maxwell, Errors.ThrustWaveformEnableFail, SEND_TO_SPACECRAFT, TcData.gpio_wf_en_fb);
                        }

                        /* Command the RFT to set the Push Pull Prep voltage. This value comes from a
                         * PT table and it is the starting point for the thruster controller ramp. */
                        raw_bytes = F32_TO_BYTES(MaxData.thrust_ignite_vset_fb);
                        maxao_rft_params[0] = (uint8_t)((raw_bytes >> 24U)  & 0xFFUL);
                        maxao_rft_params[1] = (uint8_t)((raw_bytes >> 16U) & 0xFFUL);
                        maxao_rft_params[2] = (uint8_t)((raw_bytes >> 8U) & 0xFFUL);
                        maxao_rft_params[3] = (uint8_t)((raw_bytes) & 0xFFUL);
                        RftAO_push_cmd_request(this_ao, RFT_CMD_INTERNAL, RFT_SET_PP_VSET, &maxao_rft_params[0], 4U, 0U);
                    }

                    /* Waiting for the the thruster controller's pp_vset to settle at the prep voltage */
                    GLADOS_Timer_Set(&Tmr_MaxThrustSeq, PT->max_thrust_seq5_delay_us);
                    GLADOS_Timer_EnableOnce(&Tmr_MaxThrustSeq);
                    break;

                case 6U:
                    /* Enable flow control, 90% flow after 3 seconds
                     * Set manually since value has already been validated twice. */
                    DaqCtlAO_FlowCtl_Enable(true);

                    if (MaxData.thrust_in_hot_fire)
                    {
                        /* Validate that the Push Pull voltage is within the appropriate tolerance */
                        if ((TcData.pp_vset_fb < (MaxData.thrust_ignite_vset_fb - PP_VSET_MARGIN)) ||
                            (TcData.pp_vset_fb > (MaxData.thrust_ignite_vset_fb + PP_VSET_MARGIN)))
                        {
                            /* This error aborts the thrust operation */
                            report_error_float(&AO_Maxwell, Errors.ThrustRftIgnitePpVsetCmdFail, SEND_TO_SPACECRAFT, TcData.pp_vset_fb);
                        }

                        /* For hot fire, set to the ignition puff flow rate. */
                        MaxData.flow_ctrl_mdot_fb = MaxData.thrust_puff_mdot_fb;
                    }
                    else
                    {
                        /* For cold gas, set to the commanded flow rate. */
                        MaxData.flow_ctrl_mdot_fb = MaxData.thrust_mdot_fb;
                    }

                    /* Delay until flow is sufficient to ignite the plenum */
                    GLADOS_Timer_Set(&Tmr_MaxThrustSeq, MaxData.thrust_puff_dur_us_fb);
                    GLADOS_Timer_EnableOnce(&Tmr_MaxThrustSeq);
                    break;

                case 7U:
                    /* Reinitialize parameters used for checking flow stability, which
                     * occurs each flow control PID cycle. At a later time, the result
                     * returned from this function will contain the maximum PFCV current
                     * deviation over the period starting from this point in time. */
                    (void)DaqCtlAO_FlowCtrl_Stability(true);

                    /* Waiting for flow control to check stability */
                    GLADOS_Timer_Set(&Tmr_MaxThrustSeq, PT->max_thrust_seq7_delay_us);
                    GLADOS_Timer_EnableOnce(&Tmr_MaxThrustSeq);
                    break;

                case 8U:
                    /* Validate Flow Stability by checking the range in current since we started looking */
                    max_diff_ipfcv_A = DaqCtlAO_FlowCtrl_Stability(false);
                    if (max_diff_ipfcv_A > PT->max_thrust_stable_ipfcv_A)
                    {
                        /* This error aborts the thrust operation */
                        report_error_float(&AO_Maxwell, Errors.ThrustFlowUnstable, SEND_TO_SPACECRAFT, max_diff_ipfcv_A);
                    }

                    /* If a hot fire, check flow and enable push pull for ignition */
                    if (MaxData.thrust_in_hot_fire)
                    {
                        /* At a bare minimum, validate we're flowing something */
                        if (MaxData.mfs_mdot < PT->max_thrust_flow_min_sccm)
                        {
                            /* This error aborts the thrust operation */
                            report_error_float(&AO_Maxwell, Errors.ThrustInaccurateFlow, SEND_TO_SPACECRAFT, MaxData.mfs_mdot);
                        }

                        /* If flow tolerance check is enabled, validate we're flowing within the expected bounds */
                        if ((PT->max_thrust_flow_tol_en)  &&
                            ((MaxData.mfs_mdot > ((1.0f + PT->max_thrust_flow_tol_pcnt)  * MaxData.thrust_puff_mdot_fb)) ||
                             (MaxData.mfs_mdot < ((1.0f - PT->max_thrust_flow_tol_pcnt) * MaxData.thrust_puff_mdot_fb))))

                        {
                            /* This error aborts the thrust operation */
                            report_error_float(&AO_Maxwell, Errors.ThrustInaccurateFlow, SEND_TO_SPACECRAFT, MaxData.mfs_mdot);
                        }

                        /* We are hot firing set the pp EN enable */
                        /* Sending the thruster controller PPU enable at the low voltage setpoint and start radiating  */
                        maxao_rft_params[0] = 0x01U; /* 0x01 is means enable is true */
                        /* Let's light this candle stick */
                        RftAO_push_cmd_request(this_ao, RFT_CMD_INTERNAL, RFT_SET_PP_EN, &maxao_rft_params[0], 1U, 0U);

                        /* Continue the thrust sequence if we are hot firing, fight on. If not, this is the end of the road for the cold gas.*/
                        GLADOS_Timer_Set(&Tmr_MaxThrustSeq, MaxData.thrust_ignite_us_fb); /* We wait until thrust_ignite_us_fb. It can be change in advance thrust config. */
                        GLADOS_Timer_EnableOnce(&Tmr_MaxThrustSeq);
                    }

                    /* At this point, cold gas is flowing at >90% of the specified rate */
                    /* And if we are hot firing, we set the enable for a hot fire so we should be thrusting. */
                    /* Start counting down thrust timer */

                    /* Note: Thrust duration already checked for validity prior to this */
                    DEV_ASSERT(isfinite(MaxData.thrust_dur_sec_fb));
                    DEV_ASSERT(MaxData.thrust_dur_sec_fb > 0.0f);

                    /* Determine if the thrust can fit into a single timer, otherwise, split
                     * up into timer increments. The rest of this implementation is handled
                     * in the timeout case. */
                    if (MaxData.thrust_dur_sec_fb <= SEC_PER_HOUR)
                    {
                        thrust_dur_hrs_cnt = 0UL;
                        GLADOS_Timer_Set(&Tmr_MaxThrustTimeout, (uint32_t)(MaxData.thrust_dur_sec_fb * 1000000.0f));
                    }
                    else
                    {
                        /* Cast to u32 okay since thrust_dur_sec_fb is > 0.0 */
                        thrust_dur_hrs_cnt = ((uint32_t)MaxData.thrust_dur_sec_fb) / ((uint32_t)SEC_PER_HOUR);
                        GLADOS_Timer_Set(&Tmr_MaxThrustTimeout, (uint32_t)(SEC_PER_HOUR * 1000000.0f));
                    }

                    /* Use the Thrust timer now to finish off the thrust event */
                    GLADOS_Timer_EnableContinuous(&Tmr_MaxThrustTimeout);
                    break;

                case 9U:
                    /* Validate that the Push Pull was set enabled */
                    if (TcData.gpio_pp_en_fb != 1UL)
                    {
                        /* This error aborts the thrust operation */
                        report_error_uint(&AO_Maxwell, Errors.ThrustPushPullEnableFail, SEND_TO_SPACECRAFT, TcData.gpio_pp_en_fb);
                    }

                    /* Validate that the Push Pull voltage is within the appropriate tolerance */
                    if ((TcData.evb < (MaxData.thrust_ignite_vset_fb - PP_VSET_MARGIN)) ||
                        (TcData.evb > (MaxData.thrust_ignite_vset_fb + PP_VSET_MARGIN)))
                    {
                        /* This error aborts the thrust operation */
                        report_error_float(&AO_Maxwell, Errors.ThrustRftIgnitePpVsetFail, SEND_TO_SPACECRAFT, TcData.evb);
                    }

                    if ((TcData.ivb < PT->max_ignite_level_low_ihvd_A) || (TcData.ivb > PT->max_ignite_level_ihvd_A))
                    {
                        /* This error aborts the thrust operation */
                        report_error_float(&AO_Maxwell, Errors.ThrustIgnitionFail, SEND_TO_SPACECRAFT, TcData.ivb);
                    }

                    GLADOS_Timer_Set(&Tmr_MaxThrustSeq, PT->max_thrust_seq9_delay_us);
                    GLADOS_Timer_EnableOnce(&Tmr_MaxThrustSeq);
                    break;

                case 10U:
                    /* Check evb and ivb parameters a second time before proceeding with the thrust sequence
                     * to ensure the values are stable.
                     *
                     * NOTE: We use ivb here instead of IHVD since IHVD is invalid for the initial units.
                     *
                     * Ignition occurs in a few steps:
                     *  1. ivb takes an DC offset increase due to the push pull being enabled (not yet ignited)
                     *  2. ivb maintains this level for a period of time (typically 300-400ms)
                     *  3. ivb ramps up to either (a) an ignition value or (b) up to an even higher non-ignition value
                     *
                     * Given these steps, the FSW must protect against the scenario when ignition (step 2) takes longer
                     * than expected. If the FSW were to sample an ivb value in mid ivb ramp (step 3) it could cause the
                     * FSW to think it is an ignition even though the system was actually just ramping up to the higher
                     * non-ignition ivb value. Checking twice ensures the value is stable and avoids this scenario. */

                    /* Validate that the Push Pull voltage is within the appropriate tolerance */
                    if ((TcData.evb < (MaxData.thrust_ignite_vset_fb - PP_VSET_MARGIN)) ||
                        (TcData.evb > (MaxData.thrust_ignite_vset_fb + PP_VSET_MARGIN)))
                    {
                        /* This error aborts the thrust operation */
                        report_error_float(&AO_Maxwell, Errors.ThrustRftIgnitePpVsetFail, SEND_TO_SPACECRAFT, TcData.evb);
                    }

                    if ((TcData.ivb < PT->max_ignite_level_low_ihvd_A) || (TcData.ivb > PT->max_ignite_level_ihvd_A))
                    {
                        /* This error aborts the thrust operation */
                        report_error_float(&AO_Maxwell, Errors.ThrustIgnitionFail, SEND_TO_SPACECRAFT, TcData.ivb);
                    }
                    else
                    {
                        /*If everything has been checked out and there are no errors then we are have ignited. */
                        /*Lets set max_data to true so we can enable post-ignition FDIR*/
                        MaxData.thrust_has_ignited = true;
                    }

                    /* Hard transition to the commanded flow rate. This will update the
                     * PID target setpoint used for DaqCtlAO flow control */
                    MaxData.flow_ctrl_mdot_fb = MaxData.thrust_mdot_fb;

                    /* Let the flow transition settle across a delay */
                    GLADOS_Timer_Set(&Tmr_MaxThrustSeq, PT->max_thrust_seq10_delay_us);
                    GLADOS_Timer_EnableOnce(&Tmr_MaxThrustSeq);
                    break;

                case 11U:
                    /* Flow is stabilized after delay, initiate a power ramp.
                     * Note: FDIR with post-ignition precondition are actively monitoring. */

                    /* If flow tolerance check is enabled, validate we're flowing within the expected bounds */
                    if ((PT->max_thrust_flow_tol_en) &&
                        ((MaxData.mfs_mdot > ((1.0f + PT->max_thrust_flow_tol_pcnt) * MaxData.thrust_mdot_fb)) ||
                         (MaxData.mfs_mdot < ((1.0f - PT->max_thrust_flow_tol_pcnt) * MaxData.thrust_mdot_fb))))
                    {
                        /* This error aborts the thrust operation */
                        report_error_float(&AO_Maxwell, Errors.ThrustInaccurateFlow, SEND_TO_SPACECRAFT, MaxData.mfs_mdot);
                    }

                    /* Command the RFT to set the Push Pull value and ramp.
                     * First input is the commanded thrust RF Vset, second comes from the PT or CONFIG_THRUST_ADV command. */
                     raw_bytes = F32_TO_BYTES(MaxData.thrust_rf_vset_fb);
                     maxao_rft_params[0] = (uint8_t)((raw_bytes >> 24U) & 0xFFUL);
                     maxao_rft_params[1] = (uint8_t)((raw_bytes >> 16U) & 0xFFUL);
                     maxao_rft_params[2] = (uint8_t)((raw_bytes >> 8U)  & 0xFFUL);
                     maxao_rft_params[3] = (uint8_t)((raw_bytes)        & 0xFFUL);

                    /* Push-Pull ramp command expects timing in milliseconds */
                     tmp = ((float)MaxData.thrust_pp_ramp_us_fb)/1000.0f;
                     raw_bytes = F32_TO_BYTES(tmp);
                     maxao_rft_params[4] = (uint8_t)((raw_bytes >> 24U) & 0xFFUL);
                     maxao_rft_params[5] = (uint8_t)((raw_bytes >> 16U) & 0xFFUL);
                     maxao_rft_params[6] = (uint8_t)((raw_bytes >> 8U)  & 0xFFUL);
                     maxao_rft_params[7] = (uint8_t)((raw_bytes)        & 0xFFUL);
                     RftAO_push_cmd_request(this_ao, RFT_CMD_INTERNAL, RFT_RAMP_PUSH_PULL, &maxao_rft_params[0], 8U, 0U);

                     /* Delay to permit the power ramp to complete. It includes an additional delay to
                      * account for transmission and execution delays on the Thr Ctrl. */
                    GLADOS_Timer_Set(&Tmr_MaxThrustSeq, PT->max_thrust_seq11_delay_us);
                    GLADOS_Timer_EnableOnce(&Tmr_MaxThrustSeq);
                    break;

                case 12U:
                    /* Validate that the Push Pull voltage is within the appropriate tolerance */
                    if ((TcData.evb < (MaxData.thrust_rf_vset_fb - PP_VSET_MARGIN)) ||
                        (TcData.evb > (MaxData.thrust_rf_vset_fb + PP_VSET_MARGIN)))
                    {
                        /* This error aborts the thrust operation */
                        report_error_float(&AO_Maxwell, Errors.ThrustRftPushPullFinalVsetFail, SEND_TO_SPACECRAFT, TcData.evb);
                    }

                    MaxData.thrust_steady_state = true;

                    GLADOS_Timer_Set(&Tmr_MaxThrustSeq, PT->max_thrust_seq12_delay_us);
                    GLADOS_Timer_EnableOnce(&Tmr_MaxThrustSeq);
                    break;

                case 13U:
                    /* Perform optional power compensation steps */
                    if (PT->max_thrust_pwr_step_enbl)
                    {
                        /* Prep the Thr Ctrl command to increment the voltage */
                        tmp = TcData.pp_vset_fb + PT->max_thrust_pwr_step_v_inc;
                        if (tmp > PT->max_thrust_pwr_step_v_max)
                        {
                            tmp = PT->max_thrust_pwr_step_v_max;
                        }
                        raw_bytes = F32_TO_BYTES(tmp);
                        maxao_rft_params[0] = (uint8_t)((raw_bytes >> 24U) & 0xFFUL);
                        maxao_rft_params[1] = (uint8_t)((raw_bytes >> 16U) & 0xFFUL);
                        maxao_rft_params[2] = (uint8_t)((raw_bytes >> 8U)  & 0xFFUL);
                        maxao_rft_params[3] = (uint8_t)((raw_bytes)        & 0xFFUL);

                        /* If there is a power low conditional check, do it before stepping power */
                        if (PT->max_thrust_pwr_low_ivb_enbl)
                        {
                            /* If the power falls too low, step the power */
                            if (TcData.ivb < PT->max_thrust_pwr_low_ivb_A)
                            {
                                /* Step the power */
                                RftAO_push_cmd_request(this_ao, RFT_CMD_INTERNAL, RFT_SET_PP_VSET, &maxao_rft_params[0], 4U, 0U);
                            }
                        }
                        else
                        {
                            /* Step the power */
                            RftAO_push_cmd_request(this_ao, RFT_CMD_INTERNAL, RFT_SET_PP_VSET, &maxao_rft_params[0], 4U, 0U);
                        }

                        /* If power compensation is continuous, then set the delay for the next run */
                        if (PT->max_thrust_pwr_cont_enbl)
                        {
                            /* Update the thrust sequence count to ensure it reenters this step in the sequence */
                            --thrust_seq_cnt;
                            GLADOS_Timer_Set(&Tmr_MaxThrustSeq, PT->max_thrust_pwr_cont_delay_us);
                            GLADOS_Timer_EnableOnce(&Tmr_MaxThrustSeq);
                        }
                    }
                    break;
            }

            /* Avoids compiler warning */
            (void)error_code;
            break;

#ifdef DUMMY_LOAD_STUBS
        case EVT_TMR_MAX_STUB_250MS:
            /* Update stubbed values depending on the thrust sequence step */
            pos_or_neg *= -1.0f;
            ++MaxData.stub_curr_thrust_sec_cnt;

            switch(thrust_seq_cnt)
            {
                case 2U:
                    /* Flow control update */
                    MaxData.plowp = 27.5f;
                    MaxData.stub_plowp_setpoint = 27.5f;
                case 6U:

                    MaxData.mfs_mdot = MaxData.flow_ctrl_mdot_fb; /* Mdot for puff */
                    break;

                case 9:
                    MaxData.mfs_mdot = MaxData.flow_ctrl_mdot_fb;  /* Mdot for hold for the fire duration */

                case 11U:
                    MaxData.pprop    -= 0.0023f;
                    MaxData.mfs_mdot = add_stub_noise(MaxData.mfs_mdot, MaxData.flow_ctrl_mdot_fb, MFS_FLOW_STUB_TOLERANCE); update_nominal_telm_stubs();
                    /* Adding mfs mdot noise when firing */
                    TcData.tpl += (100.0f - TcData.tpl) / 400.0f;

                    if (MaxData.stub_curr_thrust_sec_cnt > MaxData.stub_thrust_anomaly_sec_cnt)
                    {
                        MaxData.stub_thrust_anomaly_sec_cnt = 0xFFFFFFFFUL;
                        report_error_uint(this_ao, Errors.EcIbusHigh, SEND_TO_SPACECRAFT, 0UL);
                        GLADOS_AO_PUSH(this_ao, EVT_ERR_MAX_TO_SAFE);
                    }
                    break;

                default:
                    /* Continue to decrement the temperatures in
                     * the steps leading up to the burn */

                    update_nominal_telm_stubs();

                    MaxData.mfs_mdot = add_stub_noise(MaxData.mfs_mdot, MaxData.flow_ctrl_mdot_fb, MFS_FLOW_STUB_TOLERANCE); /* Adding mfs mdot noise when firing */
                    /* These numbers come from a calculation of pressure increase for a density of 1500*/
                    /* For information on the calculation go here https://docs.google.com/spreadsheets/d/1l8JJqytCEUqOK2CWQYfAjgZFSmwtgUqx6ud_yyM2RbY/edit#gid=553813290 */
                    MaxData.pprop = (float)23.7f*MaxData.tprop + 526;

                    /*
                    if (thrust_seq_cnt >6 && thrust_seq_cnt < 8){
                        MaxData.mfs_mdot += (MaxData.flow_ctrl_mdot_fb - MaxData.mfs_mdot) /1.5f;
                    }
                    if (thrust_seq_cnt >= 8){
                    MaxData.mfs_mdot  += pos_or_neg * 0.3f;
                    }
                    */
               /* MaxData.mfs_mdot += (MaxData.thrust_mdot_fb - MaxData.mfs_mdot) / 2.0f; */
                    break;
            }
            break;
#endif

        default:
            GLADOS_STATE_CHECK_PARENT(&MaxAO_state);
            break;
    }

    return;
}


void MaxAO_hard_safe(void)
{
    /* RFT can take care of itself, given it has its own protection mechanisms
     * and automatically safes after dropped comms. It is more important to move
     * forward with safing this system immediately without significant delays. */

    /* Disable Heaters and Heater Control SW mechanism */
    DaqCtlAO_HeatCtl_Enable(false);

    /* Disable Flow Control SW mechanism */
    DaqCtlAO_FlowCtl_Enable(false);

    /* Toggle DAC reset pin, returns DAC to 2.5V
     * internal reference and output 0V. This is
     * more reliable than commanding over SPI and
     * ensures that FlexIO SPI always gets into a
     * known state. */
    DaqCtlAO_Pfcv_DacResetToClose();

    /* Close High Pressure Isolation Valve */
    DaqCtlAO_HighPIso_Enable(false);
    DaqCtlAO_HighPIso_Close();

    /* Do not reset fuses */
    return;
}


Cmd_Err_Code_t MaxAO_ValidateThrustParams(float rf_vset, float mdot_set, float dur_sec)
{
    Cmd_Err_Code_t error_code = ERROR_NONE;

    /* If cold gas THRUST feature is enabled and if RF Vset is set to 0 for cold gas */
    if ((PT->thrust_cold_gas_enable == 1UL) &&
        (isfinite(rf_vset))                 &&
        (fpclassify(rf_vset) == FP_ZERO))
    {
        /* Validate cold gas bounds of the flow rate */
        if ((isfinite(mdot_set))                   &&
            (mdot_set >= PT->thrust_cold_mdot_min) &&
            (mdot_set <= PT->thrust_cold_mdot_max))
        {
            error_code = create_error_uint(Errors.StatusSuccess, 0UL);
        }
        else
        {
            error_code = create_error_float(Errors.MdotSetPointOutOfBounds, mdot_set);
        }
    }
    /* If hot fire THRUST feature is enabled and if RF Vset is valid */
    else if ((PT->thrust_hot_fire_enable == 1UL)     &&
             (isfinite(rf_vset))                     &&
             (rf_vset >= PT->thrust_hot_rf_vset_min) &&
             (rf_vset <= PT->thrust_hot_rf_vset_max))
    {
        /* Validate the mdot for the hot fire as well */
        if ((isfinite(mdot_set))                  &&
            (mdot_set >= PT->thrust_hot_mdot_min) &&
            (mdot_set <= PT->thrust_hot_mdot_max))
        {
            error_code = create_error_uint(Errors.StatusSuccess, 0UL);
        }
        else
        {
            error_code = create_error_float(Errors.MdotSetPointOutOfBounds, mdot_set);
        }
    }
    else
    {
        error_code = create_error_float(Errors.CmdInvalidRfVoltageSetpt, rf_vset);
    }

    /* Checking for zero duration ensures that thrust params have been
     * set prior to initiating the THRUST command */
    if ((!isfinite(dur_sec)) || (dur_sec <= 0.0f) || (dur_sec > PT->thrust_max_dur_sec))
    {
        error_code = create_error_float(Errors.CmdInvalidDurationSetpt, dur_sec);
    }

    return error_code;
}




#ifdef DUMMY_LOAD_STUBS
/*This function is to updates maxwell telemetry  by adding noise from the decimal portion of tadc0. */
void update_nominal_telm_stubs(void)
{
    /* Update Heater Stubs */
    MaxData.tprop = add_stub_noise(MaxData.tprop, MaxData.stub_ttank_temp_setpoint, HEAT_STUB_TOLERANCE);

    /*Update tmfs Stub */
    MaxData.tmfs = add_stub_noise(MaxData.tmfs, MaxData.stub_ttank_temp_setpoint, HEAT_STUB_TOLERANCE);

    /*Update plowp Stub */
    MaxData.plowp = add_stub_noise(MaxData.plowp, MaxData.stub_plowp_setpoint, PLOWP_STUB_TOLERANCE);

}



/*This function is to update ttank and the mfs_temp by adding noise from the decimal portion of tadc0. */
float add_stub_noise(float maxwell_telemetry, float tlm_ideal_value, float tolerance_range)
{
    static bool noise_selector = false; /* Used to determine the noise source. That way every stub telm is not the same... */
    float maxwell_noise = 0.0; /* Will be intialize in the if statement below */
   /* Selecting the noise that it will be used */
    if (noise_selector == false)
    {
        maxwell_noise = (float) max_srand_float_tadc0()/10.0f; /*Reducing the amount of noise by 10x */
        noise_selector = true;
    }
    else
    {
        maxwell_noise = (float) max_srand_float_tadc1()/10.0f; /*Reducing the amount of noise by 10x */
        noise_selector = false;
    }
    /* Add noise to the telemetry */
    maxwell_telemetry += pos_or_neg * maxwell_noise;

    /* Check if the telemetry is within expected bounds if cast it */
    if (maxwell_telemetry > (tlm_ideal_value + tolerance_range))
    {
        /*We are both doing trtd4 and two since they are affected by the same noise */
        /* The reason for the negative 2 is we need to undo the previous calculation and then go negative.*/
        maxwell_telemetry+= (float) -2.0f * maxwell_noise;
    }
    else if (maxwell_telemetry < (tlm_ideal_value - tolerance_range))
    {
        /*We are both doing trtd4 and two since they are affected by the same noise */
        /* The reason for the negative 2 is we need to undo the previous calculation and then go negative.*/
        maxwell_telemetry += (float) 2.0f * maxwell_noise;

    }
    /* None of the telemetry stubs should be 0 so if below zero cast to 0 */
    if (maxwell_telemetry < 0)
    {
        maxwell_telemetry = 0;
    }
    return maxwell_telemetry;
}
#endif
