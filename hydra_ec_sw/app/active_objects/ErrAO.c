 /* Copyright (c) 2017-2020 by Phase Four, Inc. All Rights Reserved. Confidential and Proprietary.
 *
 * ErrAO.c
 */

/* Future enhancements:
 *  - Optimize search algorithm in search_for_current_flash_address()
 */

#include "ErrAO.h"
#include "mcu_types.h"
#include "glados.h"
#include "MCUflash.h"
#include "timer64.h"
#include "max_data.h"
#include "xdata.h"
#include "p4_err.h"
#include "fidr.h"
#include "max_info_driver.h"
#include "MaxwellAO.h"
#include "DataAO.h"
#include "string.h"
#include "default_pt.h"

/* Active Object public interface */

GLADOS_AO_t AO_Err;

/* Active Object timers */
GLADOS_Timer_t Tmr_errAOFidr;
GLADOS_Timer_t Tmr_errAOTimeout;

/* Active Object private variables */
static GLADOS_Event_t err_fifo[ERRAO_FIFO_SIZE];
static GLADOS_Event_t erao_nvm_prep;

/*Error AO private variables */
static uint8_t sector_select=0;
static uint32_t erase_address=0;
static uint16_t total_numbers_recorded =0;
static err_type most_common_error = {0x000, 0x0000, 0x000, false, UNSIGNED_INTEGER}; /*Initialize value to default this does not matter because it will get replaced as soon as a error is reported.*/

/* Data defined locations */
log_rec_header_struct  __attribute__ ((section (".__LOG_REC_HEAD"))) log_rec_head; /* NOTE the record needs to be this location in SRAM specifically! The reason is the bootloader populates error information here. Changing it will require changes to the bootloader */
log_rec_error_struct  __attribute__ ((section (".__LOG_REC_SRAM"))) log_record_sram [TOTAL_AMOUNT_OF_RECORDS_IN_ERROR_SRAM];


/*log_rec_error_struct  __attribute__ ((section (".__LOG_REC_SRAM"))) log_record_flash_sector1 [TOTAL_AMOUNT_OF_RECORDS_IN_ERROR_SRAM];
log_rec_error_struct  __attribute__ ((section (".__LOG_REC_SRAM"))) log_record_flash_sector2 [TOTAL_AMOUNT_OF_RECORDS_IN_ERROR_SRAM]; */

telm_err_struct __attribute__ ((section (".__TElEM_REC"))) telemetry_record = {0}; /*Initialising the telemetry record to 0. Will get populated once errors are reported.*/


#define END_OF_SRAM_ERR_REC  ((uint32_t)(&log_record_sram[0]) + sizeof(log_record_sram))
#define ERROR_LIMIT_BEFORE_TIME_FILTER 10



/* Small functions to help internal process */

static void reset_current_address(void)
{
    log_rec_head.curr_sram_address = (uint32_t)log_record_sram;
}
static void set_error_record_count_to_zero(void)
{
    log_rec_head.sram_error_record_count = (uint16_t)0x0U;
}



/* Active Object state definitions */
void ErrAO_init(void)
{
    /* Initialize the Active Object with both its Event FIFO and initial state function */
    GLADOS_AO_Init(&AO_Err, ERRAO_PRIORITY, err_fifo, ERRAO_FIFO_SIZE,&ErrAO_state);
    return;
}



void initalize_error_ao_memory(void)
{
    uint32_t *previous_error_in_flash;

    clear_error_ao_sram();
    log_rec_head.curr_flash_address = search_for_current_flash_address();
    /* Checking if the Current Flash Address is outside the expected bounds */
    if ((log_rec_head.curr_flash_address < ERROR_RECORD_SECTOR_1_ADDRESS) || (log_rec_head.curr_flash_address > ERROR_RECORD_SECTOR_2_LIMIT))
    {
        /* If not report a error and set to the top */
        report_error_uint(&AO_Err, (err_type*) Errors.ErraoCurrentFlashAddressOutOfBounds, DO_NOT_SEND_TO_SPACECRAFT, log_rec_head.curr_flash_address);
        log_rec_head.curr_flash_address = ERROR_RECORD_SECTOR_1_ADDRESS;
    }

    /* Getting the last error that was reported from flash */
    /* If we are at the top of the first sector get the value from the ending of the last sector in flash */
    if (log_rec_head.curr_flash_address == ERROR_RECORD_SECTOR_1_ADDRESS)
    {
        previous_error_in_flash = ((uint32_t*)(ERROR_RECORD_SECTOR_2_LIMIT- SRAM_ERROR_RECORD_SIZE)); /* Getting the last error from the second sector */
    }
    else
    {
        previous_error_in_flash = (uint32_t *)(log_rec_head.curr_flash_address - SRAM_ERROR_RECORD_SIZE);
    }


    if (*previous_error_in_flash == 0xFFFFFFFF && *(previous_error_in_flash + 1) == 0xFFFFFFFF)
    {
        telemetry_record.latest_power_up_err = 0x0000; /* Setting the telemetry record value offset to 0x0000 since there is no previous error. */
    }
    else
    {
       uint8_t *error_pointer =  (void*)previous_error_in_flash;
       telemetry_record.latest_power_up_err = *(uint16_t*) (error_pointer + ERR_ID_RECORD_BYTE_OFFSET); /* Setting the telemetry record value to the error id */
    }
    if (log_rec_head.bootloader_error_id != 0)
    {
        err_type* bootloader_error = find_bootloader_errors(log_rec_head.bootloader_error_id);

        report_error_uint(&AO_Err, (err_type*) bootloader_error, SEND_TO_SPACECRAFT, app_hdr_rec->dna_record.bl_select);
    }
}


uint32_t search_for_current_flash_address(void)
{
  uint32_t current_address = ERROR_RECORD_SECTOR_1_ADDRESS;
  bool flash_blank = false;
  bool sector_one_blank = false;
  bool sector_two_blank = false;
  log_rec_error_struct* flash_address_pointer = (log_rec_error_struct*) ERROR_RECORD_SECTOR_1_ADDRESS; /* Pointer to the start of the first sector of flash for the error log. */
  log_rec_error_struct *  start_flash_address_sector_one = (log_rec_error_struct*) ERROR_RECORD_SECTOR_1_ADDRESS; /* Pointer to the start of the first sector of flash for the error log. */
  log_rec_error_struct *  start_flash_address_sector_two = (log_rec_error_struct*) ERROR_RECORD_SECTOR_2_ADDRESS; /* Pointer to the start of the second sector of flash for the error log. */



  /* Checking which sectors are blank. That way we can eliminate half the search time. */
  if (start_flash_address_sector_one->error_id == 0xFFFFU) {
    sector_one_blank = true;
  }

  if (start_flash_address_sector_two->error_id == 0xFFFFU) {
    sector_two_blank = true;
  }



/* If this condition fails that means they are both blank so we are going to send the current address which is currently  ERROR_RECORD_SECTOR_1_ADDRESS*/
  /*IF this condition passes then one of the sectors is not blank. Let's find it */

  if (sector_one_blank == true && sector_two_blank == true )
  {
      flash_address_pointer = (log_rec_error_struct*) ERROR_RECORD_SECTOR_1_ADDRESS;
  }
  else if (sector_one_blank == false  && sector_two_blank == true)
  {
      flash_address_pointer = (log_rec_error_struct*) ERROR_RECORD_SECTOR_1_ADDRESS;
  }
  else if (sector_one_blank == true  && sector_two_blank == false)
  {
      flash_address_pointer = (log_rec_error_struct*) ERROR_RECORD_SECTOR_2_ADDRESS;
  }
  /* If both sectors have info */
  else
  {
      flash_address_pointer = (log_rec_error_struct*)ERROR_RECORD_SECTOR_2_ADDRESS;
  }



    /* Iterate via linear search until we find the a record that is blank. That is our current blank location. TODO we can make this a faster search by splitting it in half and checking.*/
    while ((flash_address_pointer < (log_rec_error_struct *) ERROR_RECORD_SECTOR_2_LIMIT) && (flash_blank == false))
    {
        if (flash_address_pointer->error_id == 0xFFFFU)
        {
            flash_blank = true;
            current_address = (uint32_t)flash_address_pointer;
        }
        else
        {
            flash_address_pointer++;
        }
    }

    if (flash_blank == false)
    {
        report_error_uint(&AO_Err, (err_type *)Errors.ErraoCantDetermineCurrentFlashAddress, DO_NOT_SEND_TO_SPACECRAFT, 0UL);
    }

  return current_address;
}


/*This function's goal is to find the error that was detected from the Bootloader error location in sram.*/
err_type* find_bootloader_errors(uint16_t err_id)
{
     err_type* bootloader_error = (err_type*) Errors.InvalidBootloaderError;

    /*Double pointer math. Basically this is taking the pointer of the struct and dereferencing to the pointer of each of the err_types. From there it is derefrenced to the current error.  */
    /* This loop is also going through each error in the bootloader (currently  GeneralBootloaderError - EndOfBootloaderMain) and determining what error it found. If the error pointer  reaches the error after EndOfBootloaderMain it indicates that error was not there.
     * NOTE this only works if EndOfBootloaderMain is the last error for the bootloader. Currently for the forseeable future this will be true.
     */
    err_type **curr_err = (err_type **)&Errors.GeneralBootloaderError;
    bool found_error = false;
    /*Incrementing each error to see if the current bootloader error equals err_id*/
    while((*curr_err)<=Errors.EndOfBootloaderMain && found_error == false)
    {
        if ((*curr_err)->error_id == err_id)
        {
            found_error = true;
            bootloader_error = (err_type*)(*curr_err);
        }
        ++curr_err; /* Increment the error in the ERRORS by one.*/
    }

    return bootloader_error;
    }


void ErrAO_state(GLADOS_AO_t *this_ao, GLADOS_Event_t *event)
{
    DEV_ASSERT(this_ao);
    DEV_ASSERT(event);
    reported_error_record current_error;

    uint8_t option_select;

    switch(event->name)
    {
        /* GLADOS_ENTRY/GLADOS_EXIT optional event for each state */
        case GLADOS_ENTRY:
            initalize_error_ao_memory();

            /* Flash erases have worst-case timing of 130ms */
            GLADOS_Timer_Init(&Tmr_errAOTimeout, 130000UL, EVT_TMR_FLASH_ERASE_DONE);
            GLADOS_Timer_Subscribe_AO(&Tmr_errAOTimeout, this_ao);

            GLADOS_Timer_Init(&Tmr_errAOFidr, PT->fdir_startup_delay_us, START_FIDR_RUN);
            GLADOS_Timer_Subscribe_AO(&Tmr_errAOFidr, this_ao);
            GLADOS_Timer_EnableOnce(&Tmr_errAOFidr);

            /* Initialize the FDIR table based on the selected PT */
            fidr_init();

            break;

        case EVT_REPORT_ERR:
            if ((event->has_data==false) && (event->data_size != sizeof(reported_error_record)))
            {
                /* We should not have this happen, something is wrong with programming */
                DEV_ASSERT(0);
            }
            reported_error_record *myrecord = (reported_error_record *)&event->data;

            log_error(*myrecord);
            break;

         /* This case is a test case to validate functionality of writing a error to flash. That we can control when we are setting errors to flash */
         /* The data on here is purposely fake so we can identify it on our scripts */
        case EVT_WRITE_FLASH:
            current_error.err_type_pointer = (err_type *)Errors.StatusFailure;
            current_error.err_type_pointer->mcu_time = 0x3333333333333333;
            current_error.aux_data=0x77777777;
            current_error.glados_state=0x99;
            uint16_t count_w =0;
            while(count_w<TOTAL_AMOUNT_OF_RECORDS_IN_ERROR_SRAM){
                if (count_w<TOTAL_AMOUNT_OF_RECORDS_ERR_FREQ_COUNT){
                }
            write_sram_record(current_error);
            count_w++;
            }
            log_rec_head.curr_flash_address = 0x33333333;
            break;

        case EVT_CLEAR_FLASH:
            GLADOS_STATE_TRAN_TO_CHILD(&ErrAO_CLR_Flash);
            break;


        case EVT_CLEAR_ERRORS:
            if ((event->has_data==false) && (event->data_size != 1))
            {
                report_error_uint(this_ao, Errors.ErraoClrErrorsEvtNoData, DO_NOT_SEND_TO_SPACECRAFT,
                                     ((uint32_t)event->has_data << 8U) + (uint32_t)event->data_size);
            }
            else
            {
                option_select = 0U;
                while (option_select < ERASE_MEM_OPTIONS)
                {
                    switch (event->data[CLEAR_TYPE_INDEX] & (1 << option_select))
                    {
                        case CLEAR_COUNT_LOG:
                            clear_error_log();
                            break;

                        case CLEAR_SRAM_TELM:
                            memset(&telemetry_record, 0, sizeof(telemetry_record));
                            /* Re-add FDIR faults to the error log that are still active */
                            fidr_log_all_persistant_faults();
                            /* Indicate to DataAO that recording is to return to BANK0 */
                            GLADOS_AO_PUSH(&AO_Data, EVT_CLEAR_ERRORS);
                            break;

                        case CLEAR_SRAM_ERROR_LOG:
                            memset(log_record_sram, 0, TOTAL_AMOUNT_OF_RECORDS_IN_ERROR_SRAM* sizeof(log_rec_error_struct));
                            reset_current_address(); /*Set the log record address to the base address to reflect the erase process*/
                            set_error_record_count_to_zero(); /*Set the record count to 0 to reflect the erase process of the telem log in sram*/
                            break;

                        case CLEAR_FLASH:
                            GLADOS_STATE_TRAN_TO_CHILD(&ErrAO_CLR_Flash);
                            break;
                        default:
                            /*If it is in here it indicates that the command was not set for this iteration/cast of input.*/
                            break;
                    }
                    option_select++;
                }
            }
            break;
        case EVT_TRANSFER_TO_NVM:
            /* If we have no errors then do no initiate a transfer */
            if (log_rec_head.sram_error_record_count==0)
            {
                return;
            }
            else {
            GLADOS_STATE_TRAN_TO_CHILD(&ErrAO_TRNS_NVM);}
        break;
        case EVT_RUN_FIDR:
            run_fidr();
        
            break;
        case START_FIDR_RUN:
            GLADOS_Timer_Disable(&Tmr_errAOFidr);
            GLADOS_Timer_Init(&Tmr_errAOFidr, 10000UL, EVT_RUN_FIDR);
            GLADOS_Timer_Subscribe_AO(&Tmr_errAOFidr, this_ao);
            GLADOS_Timer_EnableContinuous(&Tmr_errAOFidr);
            break;
        case GLADOS_EXIT:
            break;

        default:
            /* Top level state, skip undefined Events */
            DEV_ASSERT(0);
            break;
    }

    return;
}



void ErrAO_CLR_Flash(GLADOS_AO_t *this_ao, GLADOS_Event_t *event){
    DEV_ASSERT(this_ao);
    DEV_ASSERT(event);
    status_t status = STATUS_SUCCESS;
    switch(event->name)
    {
        case GLADOS_ENTRY:
            erase_address = ERROR_RECORD_SECTOR_1_ADDRESS;
            erase_flash_sector_ao(&AO_Err,erase_address,FLASH_SECTOR_SIZE);
            GLADOS_Timer_EnableOnce(&Tmr_errAOTimeout);
            break;
        case EVT_TMR_FLASH_ERASE_DONE:
            GLADOS_Timer_Disable(&Tmr_errAOTimeout);
            if (erase_address==ERROR_RECORD_SECTOR_1_ADDRESS)
            {
                status=check_erase_flash_status(erase_address);
                if (status!=STATUS_SUCCESS)
                {
                    report_error_uint(this_ao, Errors.ErraoClrFlashFail, DO_NOT_SEND_TO_SPACECRAFT, (uint32_t)status);
                }
                erase_address=ERROR_RECORD_SECTOR_2_ADDRESS;
                erase_flash_sector_ao(&AO_Err,erase_address,FLASH_SECTOR_SIZE);
                GLADOS_Timer_EnableOnce(&Tmr_errAOTimeout);
            }
            else{
                status=check_erase_flash_status(erase_address);
                if (status!=STATUS_SUCCESS)
                {
                    report_error_uint(this_ao, Errors.ErraoClrFlashFail, DO_NOT_SEND_TO_SPACECRAFT, (uint32_t)status);
                }
                erase_address = ERROR_RECORD_SECTOR_1_ADDRESS; /*Erase finished successfully set back to original memory address. */
                log_rec_head.curr_flash_address = erase_address;

                GLADOS_STATE_TRAN_TO_PARENT(&ErrAO_state);
            }
            break;

        case EVT_TRANSFER_TO_NVM:
            /* Ignore, already busy doing Flash ops */
            report_error_uint(this_ao, Errors.FlashWriteFailedFlashStatusBusy, DO_NOT_SEND_TO_SPACECRAFT, (uint32_t)status);
            break;

        /* If CLEAR_ERRORS is received here, during another CLEAR_ERRORS command or during transition out of SAFE,
         * the expectation is for it to be handled by the parent to reinitiate the command. */

        default:
            GLADOS_STATE_CHECK_PARENT(&ErrAO_state);
            break;
    }
return;
}


void clear_error_ao_sram(void)
{
    reset_current_address(); /*Set the log record address to the base address since we are initializing on error ao memory*/
    set_error_record_count_to_zero(); /* Setting the error record count to zero since we are reseting the errors */
    log_rec_head.freq_unique_error_count = (uint16_t) 0x0U;
    memset(&log_record_sram[0],0,sizeof(log_record_sram)); /* Weird pointer setting. This dereferences the first element and then re-reference the address of the element. This makes it very explicit on what the address should be instead of letting the compiler decide what the address is.  */
    clear_error_log();
    return;}


/*This is used to clear the error logs in each of the error objects.
 *The error objects holds the count of the amount of errors that have been called in the power cycle, as well as the time of the last error reported.
 *This gets cleared because the values are used to filter out errors coming into the error AO.
 *This uses pointer arithmetic to clear the double pointers struct for each of the errors.
 *The math is taking the address of ERRORS and casting a double pointer to pointer to the first error in the struct.
 * */
void clear_error_log(void)
{
    uint16_t i;

    DEV_ASSERT(sizeof(Errors) % 4U == 0U);

    err_type **curr_err = (err_type **)&Errors;
    /*Double pointer math. Basically this is taking the pointer of the struct and dereferencing to the pointer of each of the err_types. From there it is derefrenced to the current error.  */
    for (i = 0UL; i < (sizeof(Errors) / sizeof(err_type *)); ++i, ++curr_err)
    {
        (*curr_err)->err_count = ZERO_ERRORS;
        (*curr_err)->mcu_time = NO_CURRENT_TIME;

    }
    return;
}


void log_error(reported_error_record current_error){

    total_numbers_recorded = total_numbers_recorded+1;

    if (log_rec_head.sram_error_record_count==TOTAL_AMOUNT_OF_RECORDS_IN_ERROR_SRAM)
    {
        log_rec_head.sram_error_record_count= (uint16_t) TOTAL_AMOUNT_OF_RECORDS_IN_ERROR_SRAM;
    }
    else if (log_rec_head.sram_error_record_count>TOTAL_AMOUNT_OF_RECORDS_IN_ERROR_SRAM)
    {
        DEV_ASSERT(0);
    }
    else {
        log_rec_head.sram_error_record_count= (uint16_t) log_rec_head.sram_error_record_count+1;
    }

    write_telem_record(current_error);
    write_sram_record(current_error);

    if (current_error.err_type_pointer->severity_level == SAFE)
    {
        GLADOS_AO_PushEvtName(&AO_Err, &AO_Maxwell, EVT_ERR_MAX_TO_SAFE);
        GLADOS_AO_PushEvtName(&AO_Err, &AO_Data, EVT_ERR_MAX_TO_SAFE);
    }

    return;
}


void write_telem_record(reported_error_record current_error)
{
    uint16_t current_error_id = current_error.err_type_pointer->error_id;
    /*Checking if the data is a unsigned integer or a float. So the data can be cast correctly in memory.*/
    uint8_t send_to_spacecraft = current_error.send_to_spacecraft;

    insert_telm_packet(current_error);

    telemetry_record.most_freq_err_id = (most_common_error).error_id ;
    telemetry_record.most_freq_err_amt = (most_common_error).err_count;
    telemetry_record.total_err_count++;

    if (send_to_spacecraft == SEND_TO_SPACECRAFT)
    {
        insert_telm_packet_spacecraft_visable(current_error_id);
    }

    return;
}

void insert_telm_packet_spacecraft_visable(uint16_t current_error_id)
{
    static bool spc_telem_record_full = false;
    uint8_t sp_err_pos = 0U; /* This is the spacecraft error position in the array. Used to find a spacecraft error slot that does not have a error. */

    /*Check if the spacecraft error full flag is set to true, while seeing if the first spacecraft error id is 0.
     * This condition only occurs if we had full spacecraft errors and then there was a command to erase telm errors.*/
    if (spc_telem_record_full == true && (telemetry_record.spacecraft_err_id[0] == 0x0000U))
    {
        spc_telem_record_full = false; /* We now know we had a clear error command so we are setting back to false. */
    }

    if (spc_telem_record_full == false)
    {
        /* Go through the spacecraft error array to find the empty one. */
        while (sp_err_pos <= TOTAL_TLM_SPC_ERR_COUNT)
        {
            if (telemetry_record.spacecraft_err_id[sp_err_pos] == 0x0000U)
            {
                telemetry_record.spacecraft_err_id[sp_err_pos] = current_error_id;
                break;
            }
            sp_err_pos++;
        }

        /* If this is the last Telm record we are writing that means the Telm Record is full */
        if (sp_err_pos >= (TOTAL_TLM_SPC_ERR_COUNT - 1))
        {
            spc_telem_record_full = true;
        }
    }

    /* Since all errors are full, shift over spacecraft telemetry item 2 to the 3rd slot and the new error will go into the 3rd slot */
    else
    {
        telemetry_record.spacecraft_err_id[1] = telemetry_record.spacecraft_err_id[2];
        telemetry_record.spacecraft_err_id[2] = current_error_id;
    }
    return;

}



void insert_telm_packet(reported_error_record current_error)
{
    static bool telem_record_full = false;
    uint8_t err_pos = 0U;
    /*Check if the telm error full flag is set to true, while seeing if the first error's id and aux data is set to 0 (This is done by adding them together).
     * This condition only occurs if we had full telm errors and then there was a error telm erase.*/

    if (telem_record_full == true && (telemetry_record.internal_errors[0].err_id == 0x0000U))
    {
        telem_record_full = false;
    }

    if (telem_record_full == false)
    {
        while (err_pos < TOTAL_TLM_ERR_COUNT)
        {
            if (telemetry_record.internal_errors[err_pos].err_id== 0x0000)
            {
                telemetry_record.internal_errors[err_pos].err_id = current_error.err_type_pointer->error_id;
                telemetry_record.internal_errors[err_pos].aux_data = current_error.aux_data;
                break;
            }
            err_pos++;
        }

        /* If this is the last Telm record we are writing that means the Telm Record is full */
        if (err_pos == (TOTAL_TLM_ERR_COUNT-1) )
        {
            telem_record_full =true;
        }
    }

    else
    {
        telemetry_record.internal_errors[1] = telemetry_record.internal_errors[2];
        /* Writing error id*/
        telemetry_record.internal_errors[2].err_id = current_error.err_type_pointer->error_id;
        /* Writing aux_data id*/
        telemetry_record.internal_errors[2].aux_data =current_error.aux_data;
        return;
    }

}




void write_sram_record( reported_error_record current_error)
{
    /* If it overflowed or if it is outof bounds go back to the start of log_record_sram this is a saftey check it may not be necessary*/
    if ((log_rec_head.curr_sram_address >=(uint32_t)(END_OF_SRAM_ERR_REC)) || (log_rec_head.curr_sram_address <(uint32_t)&log_record_sram[0]))
    {
        reset_current_address();
    }

     *(uint64_t*)(log_rec_head.curr_sram_address + CRR_TIME_RECORD_BYTE_OFFSET) =current_error.err_type_pointer->mcu_time;
     *(uint16_t*)(log_rec_head.curr_sram_address+ERR_ID_RECORD_BYTE_OFFSET) =current_error.err_type_pointer->error_id;
     *(uint32_t*)(log_rec_head.curr_sram_address+AUX_DATA_RECORD_BYTE_OFFSET) =current_error.aux_data;
     *(uint8_t*)(log_rec_head.curr_sram_address+CRR_STATE_RECORD_BYTE_OFFSET) =current_error.glados_state;
     log_rec_head.curr_sram_address = (uint32_t)log_rec_head.curr_sram_address+SRAM_ERROR_RECORD_SIZE;
     if ((log_rec_head.curr_sram_address >=(uint32_t)(END_OF_SRAM_ERR_REC)) || (log_rec_head.curr_sram_address <(uint32_t)log_record_sram))
     {
         reset_current_address();

     }

     return;
}


bool error_filtration_check(err_type * reported_error)
{

    bool record_error=false;
    uint32_t stop_tick_upper=0;
    uint32_t stop_tick_lower=0;
    uint64_t current_time = 0UL;
    uint64_t old_error_time = 0UL;

    /*There is a edge case here where if the mcu gets a unix timestamp it can greatly increase the current time.
     * If that is the case it can seem like it increased over a minute. This will trigger a accidental log if the current error was being filtered.
     * However this edge case is not that big of a deal.
     */

    timer64_get_tick( &stop_tick_upper, &stop_tick_lower);
    current_time = get_unix_timestamp_micro();
    old_error_time = (*reported_error).mcu_time;


    if ((*reported_error).err_count >=ERROR_LIMIT_BEFORE_TIME_FILTER)
       {

        if (current_time - old_error_time>ONE_MIN_IN_US){
             record_error = true;
         }
        else {
            record_error = false;
        }

       }
    else {
        record_error = true;
    }
   /* last_error_index = (*reported_error);  Might use for later determines what the last error was used.*/

    (*reported_error).mcu_time = current_time;
    (*reported_error).err_count = (*reported_error).err_count + 1;
    if (record_error == true)
    {
        if ((*reported_error).err_count > most_common_error.err_count)
        {
            most_common_error = (*reported_error);
        }
    }


    return record_error;
}



void ErrAO_TRNS_NVM(GLADOS_AO_t *this_ao, GLADOS_Event_t *event){
    DEV_ASSERT(this_ao);
    DEV_ASSERT(event);
    switch(event->name)
    {
        case GLADOS_ENTRY:
            /*Check if the flash is in the bounds*/
           if ((log_rec_head.curr_flash_address<ERROR_RECORD_SECTOR_1_ADDRESS) ||(log_rec_head.curr_flash_address>ERROR_RECORD_SECTOR_2_LIMIT))
           {
               report_error_uint(&AO_Err, (err_type*) Errors.ErraoCurrentFlashAddressOutOfBounds, DO_NOT_SEND_TO_SPACECRAFT, log_rec_head.curr_flash_address);
               log_rec_head.curr_flash_address = search_for_current_flash_address();
              DEV_ASSERT(0);
           }
           else if (log_rec_head.curr_flash_address<ERROR_RECORD_SECTOR_2_ADDRESS){
               /*We are in flash sector 1 now we need to determine if we have to clear flash or not to get room*/
               if (log_rec_head.curr_flash_address + (log_rec_head.sram_error_record_count*SRAM_ERROR_RECORD_SIZE)>ERROR_RECORD_SECTOR_2_ADDRESS){
                       /*We ran out of room we need to erase flash */
                   sector_select=2;
                   GLADOS_STATE_TRAN_TO_CHILD(&ErrAO_ERASE_SECTOR);
                 }
               else {
                   erao_nvm_prep.name=EVT_NVM_PREPARED;
                   GLADOS_AO_PUSH_EVT_SELF(&erao_nvm_prep); /*Need to figure out how to get to NVM prepared*/
               }

           }
           else{
               /*We are in flash sector 2 now we need to determine if we have to clear flash or not to get room*/
               if (log_rec_head.curr_flash_address + (log_rec_head.sram_error_record_count*SRAM_ERROR_RECORD_SIZE)>ERROR_RECORD_SECTOR_2_LIMIT){
                     /*We ran out of room we need to erase flash */
                   sector_select=1;
                   GLADOS_STATE_TRAN_TO_CHILD(&ErrAO_ERASE_SECTOR);
                    }
               else {
                   erao_nvm_prep.name=EVT_NVM_PREPARED;
                   GLADOS_AO_PUSH_EVT_SELF(&erao_nvm_prep); /*Need to figure out how to get to NVM prepared*/
               }
           }
            break;
        case EVT_NVM_PREPARED:{
         uint8_t overflow_status= sram_record_overflow_check();
         status_t status = STATUS_SUCCESS;
         uint32_t total_memory_usage=log_rec_head.sram_error_record_count*SRAM_ERROR_RECORD_SIZE;
         uint32_t base_sram_address;
         switch (overflow_status)
         {
             /*Setting up temp buffer based on the current SRAM scenrio*/
             case NO_OVERFLOW:
                 base_sram_address =  log_rec_head.curr_sram_address - total_memory_usage;
                 status =  add_errors_in_flash(base_sram_address, log_rec_head.curr_sram_address);
                 break;



                 /* This case handles if the SRAM overflows part of the way (Just went back around.) or completely overflowed (Every element is new) Due to how the algorithim works.
                     * It works with the assumption that total memory worst case can go up to 256 Errors.
                     * As of this writing this is true. If we get more then 256 errors it will currently stay at 256. Due to that is the total amount of errors we can record at at time.
                     *
                     * Since we can go only up to 256 errors we find that finding the length of how much data in each section is easy by the following equation.
                     * Top_Buffer_Size = Current_SRAM_Location (always a bigger number) - top_of_sram_bank. This will give us our top buffer in which is from the current location to the top of sram record bank before the overflow.
                     * Now that we have the Top buffer and the total amount of errors is constant we can simply calculate the bottom amount.
                     * Bottom_Buffer_Size = Total_Memory_Amount - Top_Buffer_Size. This would work because we know that Total_Memory_Amount = Top_Buffer_Size + Bottom_Buffer_Size.
                     *
                     * Now that we know the sizes and we know the hard positions of where the sram record borders are we can back calculate the base_sram_address
                     * base_sram_address = Top_of_telmetry_section (Lower down/ higher memory offset of any sram record) - bottom_buffer_size (how far up till we hit the top of the bottom_buffer_size)
                     * Since we have the base_sram_address we can simply unroll the loop and put everything in order.
                     *
                     * Since again that the total memory will be stuck at 256 this can work for part overfill or a complete overfill.
                     *
                     * To help your understanding look at the current memory map for the error AO. https://phasefour.atlassian.net/wiki/spaces/MAX/pages/503873590/Software+Design+Document+SDD
                     * */
             case OVERFLOW:{

                 uint32_t top_circular_data_size = log_rec_head.curr_sram_address - (uint32_t)log_record_sram;
                 uint32_t bottom_circular_data_size = total_memory_usage - top_circular_data_size;
                 base_sram_address = (uint32_t)((END_OF_SRAM_ERR_REC) - bottom_circular_data_size);
                 status = add_errors_in_flash(base_sram_address, END_OF_SRAM_ERR_REC);
                 status = add_errors_in_flash((uint32_t)log_record_sram,(uint32_t) log_rec_head.curr_sram_address); /*Log record_sram is the top of sram record bank */
                 break;}

             default:
                 DEV_ASSERT(0);
                 break;

         }

         if (status != STATUS_SUCCESS)
         {
             GLADOS_STATE_TRAN_TO_PARENT(&ErrAO_state); /* Flash failed go to parent.*/
         }

         /*If the flash address is over or under these addresses something went terribly wrong.*/
         else if ((log_rec_head.curr_flash_address < ERROR_RECORD_SECTOR_1_ADDRESS) || (log_rec_head.curr_flash_address > ERROR_RECORD_SECTOR_2_LIMIT))
         {
             sector_select = 1;
            GLADOS_STATE_TRAN_TO_CHILD(&ErrAO_ERASE_SECTOR);
            GLADOS_STATE_TRAN_TO_CHILD(&ErrAO_ERASE_SECTOR_PROACTIVE);
            log_rec_head.curr_flash_address = ERROR_RECORD_SECTOR_1_ADDRESS;
            set_error_record_count_to_zero();            /*Set the record count to 0 to reflect the erase process of the telem log in sram*/
            DEV_ASSERT(0);
         }

         /* If the current flash address is above the 75% line of sector one*/
         else if ((log_rec_head.curr_flash_address <= ERROR_RECORD_SECTOR_2_ADDRESS) && (log_rec_head.curr_flash_address >= FLASH_SECTOR_ONE_75_PERCENT_BOUNDRY))
         {
             sector_select = 2;
             GLADOS_STATE_TRAN_TO_CHILD(&ErrAO_ERASE_SECTOR);
             GLADOS_STATE_TRAN_TO_CHILD(&ErrAO_ERASE_SECTOR_PROACTIVE);
             set_error_record_count_to_zero(); /*Set the record count to 0 to reflect the erase process of the telem log in sram*/
         }
         /* If the current flash address is above the 75% line of sector two or at the start of the first sector.*/
         else if ((log_rec_head.curr_flash_address >= FLASH_SECTOR_TWO_75_PERCENT_BOUNDRY) || (log_rec_head.curr_flash_address == ERROR_RECORD_SECTOR_1_ADDRESS))
         {
             /*We ran out of room we need to erase flash */
             sector_select = 1;
             GLADOS_STATE_TRAN_TO_CHILD(&ErrAO_ERASE_SECTOR);
             GLADOS_STATE_TRAN_TO_CHILD(&ErrAO_ERASE_SECTOR_PROACTIVE);
             set_error_record_count_to_zero(); /*Set the record count to 0 to reflect the erase process of the telem log in sram*/
         }
         else{
             set_error_record_count_to_zero(); /*Set the record count to 0 to reflect the erase process of the telem log in sram*/
             GLADOS_STATE_TRAN_TO_PARENT(&ErrAO_state);
         }

        }
            break;

        case EVT_SAVE_FLASH:
            break;

        case EVT_TRANSFER_TO_NVM:
            break;

        case GLADOS_EXIT:
            break;

        default:
            GLADOS_STATE_CHECK_PARENT(&ErrAO_state);
            break;
    }
return;
}




/* It is assumed that start_address is "up" lower memory values and finish_address is a higher place in memory */
status_t add_errors_in_flash(uint32_t start_address, uint32_t finish_address)
{
    status_t status = STATUS_SUCCESS;
    if (finish_address < start_address)
    {
        DEV_ASSERT(0);
        status = STATUS_ERROR;
        return status;
    }
    uint32_t memory_total = finish_address - start_address;

    uint32_t current_remaining_flash = (((uint32_t)(ERROR_RECORD_SECTOR_2_LIMIT)) - log_rec_head.curr_flash_address);

    if (current_remaining_flash < memory_total)
    {
        if (current_remaining_flash != 0)
        {
            /* status = write_flash_ao(&AO_Err, CURRENT_FLASH_ADDRESS, current_remaining_flash, (uint32_t *)start_address);*/
            status = attempt_write_flash_ao_twice(current_remaining_flash, (start_address));
            /* If we could not write correctly to flash stop the process and report a error*/
            if (status != STATUS_SUCCESS)
            {
                return status;
            }
        }
        uint32_t final_amount_to_program_in_flash = memory_total - current_remaining_flash;
        log_rec_head.curr_flash_address = ERROR_RECORD_SECTOR_1_ADDRESS; /* We reached the end of flash we need to set it back to the beginning. At this point flash has been erased. */

        /* status = write_flash_ao(&AO_Err, CURRENT_FLASH_ADDRESS, final_amount_to_program_in_flash, (uint32_t *)(start_address+current_remaining_flash)); */
        status = attempt_write_flash_ao_twice(final_amount_to_program_in_flash, (start_address + current_remaining_flash));

        /* If we could not write correctly to flash, dont update the header because it was not written correctly. */
        if (status == STATUS_SUCCESS)
        {
            log_rec_head.curr_flash_address = log_rec_head.curr_flash_address + final_amount_to_program_in_flash; /* Updated the final difference right here*/
        }
    }

    else
    {
        /*   status = write_flash_ao(&AO_Err, CURRENT_FLASH_ADDRESS, memory_total, (uint32_t *)start_address); */
        status = attempt_write_flash_ao_twice(memory_total, start_address);

        /* If we could not write correctly to flash, dont update the header because it was not written correctly. */
        if (status == STATUS_SUCCESS)
        {
            log_rec_head.curr_flash_address = log_rec_head.curr_flash_address + memory_total;
        }
    }
    return status;
}

status_t attempt_write_flash_ao_twice(uint32_t flash_size, uint32_t start_address)
{
    uint8_t count = 0;
    status_t status = STATUS_ERROR;
    /*If writing was not succesful and there was no user generated errors  try again for 2 time total. */
while (count < 2 && ((status != STATUS_SUCCESS) && (status != STATUS_FLASH_WRITE_INVALID_ERASE_ADDRESS) && (status != STATUS_FLASH_WRITE_OVERLIMIT_WRITE_LENGTH)  && (status != STATUS_FLASH_WRITE_INVALID_STATUS_BUSY)))
  {
        status = write_flash_ao(&AO_Err, log_rec_head.curr_flash_address, flash_size, (uint8_t *)start_address);
        count++;
  }
if (count>= 2){
    DEV_ASSERT(0);
        }
  return status;
}


/* Note if the sram starts at the base address from a overflow. (The last error logged was at the end of sram) It views this as not a overflow.
 * Even though technically it went over and went to the beginning operationally it is the same as it started from the beginning.
 * It only is affected if the sram buffer overflowed and beyond in between transfers.
 */
uint8_t sram_record_overflow_check(void){
    uint32_t total_memory_usage=log_rec_head.sram_error_record_count*SRAM_ERROR_RECORD_SIZE;
    uint32_t base_sram_address = (uint32_t) log_rec_head.curr_sram_address - total_memory_usage;

    /*base_sram_address is a "lose"term. I am calculating the hypothetical base address if the circular buffer was rolled out.
     * So if the current address minus the total message size (since from a transfer) is under the starting address of log_record_sram (starting address of the circular buffer)
     * It means that there was more data then the current buffer can contain from the start of the buffer to the current address. The only way that is possible is through a overflow. */
    if (base_sram_address<(uint32_t)log_record_sram)
    {
        return OVERFLOW;
    }
    else {return NO_OVERFLOW;}

}

void ErrAO_ERASE_SECTOR(GLADOS_AO_t *this_ao, GLADOS_Event_t *event){
    DEV_ASSERT(this_ao);
    DEV_ASSERT(event);
    /*Check if there is data in Erase Sector*/
    status_t status = STATUS_SUCCESS;
    /* If there is data lets verify that the data is what we expect*/


    switch(event->name)
    {
        case GLADOS_ENTRY:{

            if (sector_select==1)
            {
                erase_address = ERROR_RECORD_SECTOR_1_ADDRESS; /*Set address to erase sector 1 */
            }
            else
            {
                erase_address = ERROR_RECORD_SECTOR_2_ADDRESS;  /*Set address to erase sector 2 */
            }

            sector_select=0;
            erase_flash_sector_ao(&AO_Err, erase_address,FLASH_SECTOR_SIZE);
            GLADOS_Timer_EnableOnce(&Tmr_errAOTimeout);
        }
            break;


        case EVT_TMR_FLASH_ERASE_DONE:
            GLADOS_Timer_Disable(&Tmr_errAOTimeout);

                status=check_erase_flash_status(erase_address);
                if (status!=STATUS_SUCCESS)
                {
                    report_error_uint(this_ao, Errors.ErraoEraseSectorFail, DO_NOT_SEND_TO_SPACECRAFT, (uint32_t)status);
                }
                /* TODO what if this fails? */
                GLADOS_STATE_TRAN_TO_PARENT(&ErrAO_TRNS_NVM);
                erao_nvm_prep.name=EVT_NVM_PREPARED;
                GLADOS_AO_PUSH_EVT_SELF(&erao_nvm_prep); /*Need to figure out how to get to NVM prepared*/
            break;

        case GLADOS_EXIT:
            GLADOS_Timer_Disable(&Tmr_errAOTimeout);
            break;

        default:
            GLADOS_STATE_CHECK_PARENT(&ErrAO_TRNS_NVM);
            break;
    }
return;
}

void ErrAO_ERASE_SECTOR_PROACTIVE(GLADOS_AO_t *this_ao, GLADOS_Event_t *event)
{
    DEV_ASSERT(this_ao);
    DEV_ASSERT(event);
    status_t status = STATUS_SUCCESS;
    switch(event->name)
     {
        case EVT_TMR_FLASH_ERASE_DONE:
            GLADOS_Timer_Disable(&Tmr_errAOTimeout);
                status=check_erase_flash_status(erase_address);
                if (status!=STATUS_SUCCESS)
                {
                    report_error_uint(this_ao, Errors.ErraoEraseSectorFail, DO_NOT_SEND_TO_SPACECRAFT, (uint32_t)status);
                }
                /* TODO what if this fails? */
                GLADOS_STATE_TRAN_TO_PARENT(&ErrAO_ERASE_SECTOR);
                GLADOS_STATE_TRAN_TO_PARENT(&ErrAO_TRNS_NVM);
                GLADOS_STATE_TRAN_TO_PARENT(&ErrAO_state);
            break;

        default:
            GLADOS_STATE_CHECK_PARENT(&ErrAO_ERASE_SECTOR);
            break;

    }
    return;
}






/* Public helper functions */
void report_error_uint(GLADOS_AO_t *ao_src, const err_type *err_sent, uint8_t send_to_spacecraft, uint32_t aux_data)
{
    DEV_ASSERT(ao_src);
    reported_error_record current_error;
    bool record_error_flag = error_filtration_check((err_type *)err_sent);
    if (record_error_flag)
    {
        current_error.err_type_pointer = (err_type *)err_sent;
        current_error.aux_data = aux_data;
        current_error.glados_state = MaxData.state;
        current_error.send_to_spacecraft = send_to_spacecraft;

        GLADOS_Event_t evt;
        GLADOS_Event_New_Data(&evt, EVT_REPORT_ERR,(uint8_t*)&current_error,sizeof(reported_error_record)); /* THIS WILL NOT WORK THE TIMING IS OFF WHEN MUTIPLE VALUES ARE SENT SEND THE TIME SEPARATELY*/
        GLADOS_AO_PushEvt(ao_src,&AO_Err,&evt);
    }

    return;
}



/* Public helper functions */
void report_error_float(GLADOS_AO_t *ao_src, const err_type *err_sent, uint8_t send_to_spacecraft, float aux_data)
{
    DEV_ASSERT(ao_src);
    reported_error_record current_error;
    bool record_error_flag = error_filtration_check((err_type *)err_sent);
    if (record_error_flag)
    {
        current_error.err_type_pointer = (err_type *)err_sent;
        current_error.aux_data = *(uint32_t*)&aux_data;
        current_error.glados_state = MaxData.state;
        current_error.send_to_spacecraft = send_to_spacecraft;

        GLADOS_Event_t evt;
        GLADOS_Event_New_Data(&evt, EVT_REPORT_ERR,(uint8_t*)&current_error,sizeof(reported_error_record)); /* THIS WILL NOT WORK THE TIMING IS OFF WHEN MUTIPLE VALUES ARE SENT SEND THE TIME SEPARATELY*/
        GLADOS_AO_PushEvt(ao_src,&AO_Err,&evt);
    }

    return;
}


void report_error_int(GLADOS_AO_t *ao_src, err_type *err_sent, uint8_t send_to_spacecraft, int aux_data)
{
    DEV_ASSERT(ao_src);
    reported_error_record current_error;
    bool record_error_flag = error_filtration_check(err_sent);
    if (record_error_flag){

        current_error.err_type_pointer=err_sent;
        current_error.aux_data = *(uint32_t*)&aux_data;
        current_error.glados_state = MaxData.state;
        current_error.send_to_spacecraft = send_to_spacecraft;

        GLADOS_Event_t evt;
        GLADOS_Event_New_Data(&evt, EVT_REPORT_ERR,(uint8_t*)&current_error,sizeof(reported_error_record)); /* THIS WILL NOT WORK THE TIMING IS OFF WHEN MUTIPLE VALUES ARE SENT SEND THE TIME SEPARATELY*/
        GLADOS_AO_PushEvt(ao_src,&AO_Err,&evt);
    }

    return;
}


status_t err_log_and_nvm_xfer(err_type *err, uint32_t aux_data)
{
    uint32_t err_start_addr = log_rec_head.curr_sram_address;
    uint32_t err_end_addr = log_rec_head.curr_sram_address + SRAM_ERROR_RECORD_SIZE;

    reported_error_record current_error =
    {
        .err_type_pointer = err,
        .aux_data = aux_data,
        .send_to_spacecraft = SEND_TO_SPACECRAFT,
        .glados_state = MaxData.state
    };

    write_telem_record(current_error);
    write_sram_record(current_error);

    return add_errors_in_flash(err_start_addr, err_end_addr);
}


/* This function is basically identical as the report_error function. The only difference is that it logs the error directly and does not go through the queue.*/
void  _report_fidr_error(err_type *err_sent, uint8_t send_to_spacecraft, float aux_data)
{
    reported_error_record current_error;
    bool record_error_flag = error_filtration_check(err_sent);
    if (record_error_flag){
        current_error.err_type_pointer=err_sent;
        /* Determining if it is a float or not.*/
        current_error.aux_data = *(uint32_t*)&aux_data;
        current_error.glados_state = MaxData.state;
        current_error.send_to_spacecraft = send_to_spacecraft;
        log_error(current_error);

    }

    return;

}


