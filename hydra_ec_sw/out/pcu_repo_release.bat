@echo off
rem Run by double-clicking
rem
rem Dependencies:
rem  - M: drive is mounted
rem  - The hydra_ec_sw has been built and post-build steps have been
rem    run to generate sw_version.txt, which is used for the
rem    folder version number
rem  - Connected to network with access to \\BENDER (10.0.0.250)

rem Get path of this file no matter where it is executed
set mypath=%~dp0

set sw_ver_file=M:\hydra_ec_sw\out\pcu_pflash\sw_version.txt
set bin_file=M:\hydra_ec_sw\out\pcu_pflash\pcu_pflash.bin
set bl_file=M:\hydra_ec_sw\out\all_bl_pcu_pt\all_bl_pcu_pt.srec
set map_file=M:\hydra_ec_sw\out\pcu_sram\hydra_ec_sw.map
set elf_file=M:\hydra_ec_sw\out\pcu_sram\hydra_ec_sw.elf
set rel_dir=\\10.0.0.250\Bender\phase_four_repo\firmware\hydra_ec_sw

rem Check for the existance of both files first
if exist "%bl_file%" (
    if exist "%sw_ver_file%" (
      goto RUN_SCRIPT
    ) else (
        @echo on
        start "" /wait cmd /c "echo %mypath%%~nx0: Aborting, no such file '%sw_ver_file%'&echo(&pause"
        rem echo "Cannot find file: %sw_ver_file%"
        goto EXIT
    )
) else (
    @echo on
    start "" /wait cmd /c "echo %mypath%%~nx0: Aborting, no such file '%bl_file%'&echo(&pause"
    rem echo "Cannot find file: %bl_file%"
    goto EXIT
)

:RUN_SCRIPT

rem Use the version (spit out from the build) to create a new directory
set /p sw_ver=<%sw_ver_file%
set ver_dir="%mypath:~0,-1%\%sw_ver%"
mkdir "%ver_dir%"

rem Copy the released file(s) into the directory along with the raw binary
copy "%bl_file%" "%ver_dir%\bl_full_%sw_ver%.srec"
copy "%bin_file%" "%ver_dir%\%sw_ver%.bin"
copy "%map_file%" "%ver_dir%\%sw_ver%.map"
copy "%elf_file%" "%ver_dir%\%sw_ver%.elf"

rem Copy the folder (removing the trailing "\") to the release folder
xcopy /S /I "%ver_dir%" "%rel_dir%\%sw_ver%"

:EXIT

@echo on
