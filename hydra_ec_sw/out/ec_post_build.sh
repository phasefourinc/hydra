# This script turns a SRAM build executable srecord into a set of images
# that follow the Phase Four image format, which are ultimately combined
# into a single loadable file that includes the Bootloader (Pri + Red)
# and Application images (Golden, App 1, App 2).
#
# This script expects to be called in the Eclipse post-build steps.
# Having in a single file is much easier to manage and track changes
# compared to having each in the .cproject file.
#
# Use by calling this bash file in the Eclipse post-build steps and passing in
# the project name:
#   1. Navigate to Project Properties->Settings->Build Steps->Post-build steps
#   2. Update to include calling the bash script like so:
#   arm-none-eabi-objcopy -v -O srec --srec-forceS3 "${BuildArtifactFileName}" "${BuildArtifactFileBaseName}.srec"; ../out/ec_post_build.sh "${BuildArtifactFileBaseName}";

# 4-byte code indicating the image header type
img_type_code=0x45584521 # EXE! in ASCII
hard_type_code=0x45433031 # EC01 in ASCII
prod_type_code=0x4D314232 # M1B2 in ASCII

# Project name passed in so cloning the FSW between projects
# does not require editing of this file
proj_name=$1

# This script expects to be called in the post-build steps,
# therefore these paths are relative to the build directory
out_dir="../out"
pt_dir="${out_dir}/pt_default"
sram_dir="${out_dir}/pcu_sram"
pflash_dir="${out_dir}/pcu_pflash"
bl_dir="${out_dir}/bl_pcu_full"
all_dir="${out_dir}/all_bl_pcu_pt"

# This file is generated from the pre-build script
pt_debug_srec="${pt_dir}/A00000_ec_default.srec"

bl_srec_dir="../../submodules/p4_ark/Bootloader/Out"
srec_cat_exe="M:/tools/srecord/srecord_1.63_win32/srec_cat.exe"

# Create all directories as necessary
mkdir -p ${bl_dir} ${pflash_dir} ${sram_dir} ${all_dir}

# Copy files from the build directory to the sram dir
cp *.srec *.map *.elf ${sram_dir}

# Get the latest git version to use
M:/tools/git/bin/git.exe describe --match "${proj_name}*" > "${pflash_dir}/sw_version.txt"

# Since the file has a \0a character indicating the end of the file, we will want to
# crop this character out when reading in sw_version.txt into srec_cat. To do this
# we grab the length of the file and store it to a varaible for later use.
#
# Get the word count and then split the returned string based on
# awk's default " " delimiter and grab the first item, which
# contains the char count and then subtract by 1
ver_char_count=`wc -c "${pflash_dir}/sw_version.txt" | awk '{print $1}'`
new_ver_char_count="$(($ver_char_count - 1))"

# Convert the ASCII file of the version count into an srecord, filling holes
# with 0x0 and converting to Little Endian
${srec_cat_exe} "${pflash_dir}/sw_version.txt" \
    -Binary -crop 0x0 ${new_ver_char_count} -fill 0x00 0x0 0x20 -byte-swap 4    \
    -Output "${pflash_dir}/sw_version.srec" -Motorola -address-length=4 -Line_Length 46 -Disable Data_Count 


# Get the word count and then split the returned string based on
# awk's default " " delimiter and grab the first item, which
# contains the char count and then subtract by 1
cred_char_count=`wc -c "${out_dir}/credits.txt" | awk '{print $1}'`
new_cred_char_count="$(($cred_char_count - 1))"

# Convert the ASCII file of the version count into an srecord, filling holes
# with 0x0 and converting to Little Endian
${srec_cat_exe} "${out_dir}/credits.txt" \
    -Binary -crop 0x0 ${new_cred_char_count} -fill 0x00 0x0 0x2000    \
    -Output "${pflash_dir}/credits.srec" -Motorola -address-length=4 -Line_Length 46 -Disable Data_Count 


# Use the SRAM executable to create a header and move both to the Golden Image location
#
# Image header format
#   0x00-0x03 - Image Type Code (4 bytes) EXE! PARM BOOT
#   0x04-0x07 - Image Length (4 bytes)
#   0x08-0x0B - Image Exe CRC32 (4 bytes)
#   0x0C-0x0F - Reserved (4 bytes)
#   0x10-0x2F - SW Version (32 bytes)
#   0x30-0xFF - Reserved  PC01 MX01
#
# Image Executable
#   0x100 - 0x1_FFFF
#
# Memory Map
#   0x0_0F00 - Bootloader 1 (Flash Header)
#   0x0_1F00 - Bootloader 2 (Flash Header)
#   0x4_0000 - Golden Exe Image
#   0x6_0000 - Golden PT Image
#   0x8_0000 - Application Exe Image 1
#   0xA_0000 - Application PT Image 1
#   0xC_0000 - Application Exe Image 2
#   0xE_0000 - Application PT Image 2
#
# This one's a doozie, so here's what's going on:
#  1. Calculate the CRC32 over the lower SRAM (cropped) and insert result 8 bytes into the
#     start of the image (0x1FFE_0008), crop to keep ONLY the CRC value, and offset to the
#     Golden Image address of 0x4_0000 (-0x1FFA_0000 offset from 0x1FFE_0000)
#  2. Reload the SRAM srecord for lower SRAM (cropped) and insert the length at 4 bytes into
#     the start of the image (0x1FFE_0004). Offset both length and full SRAM image to the
#     Golden Image address of (0x4_0000) and combine with the CRC result from Step 1.
#  3. Load the SW version value, cropping for a maximum size of 32 bytes, offset it into
#     the Golden Image header (0x4_0010), combine with data from previous steps
#  4. Generate a 4-byte value for the image type code and insert into the very beginning of
#     the Golden Image header (0x4_0000), combine with data from previous steps
#  5. Spit out into a final srec with appropriate formatting
${srec_cat_exe} \
    "${sram_dir}/${proj_name}.srec" -crop 0x1FFE0100 0x20000000                               \
        -CRC32_Little_Endian 0x1FFE0008 -crop 0x1FFE0008 0x1FFE000C -offset -0x1FFA0000 \
    "${sram_dir}/${proj_name}.srec" -crop 0x1FFE0100 0x20000000                               \
        -exclusive-length-L-e 0x1FFE0004 -offset -0x1FFA0000                            \
    "${pflash_dir}/sw_version.srec" -crop 0x0 0x20 -offset 0x40010                      \
    -generate 0x40000 0x40004 -constant-l-e ${img_type_code} 4                          \
    -generate 0x40030 0x40034 -constant-l-e ${hard_type_code} 4                         \
    -generate 0x40034 0x40038 -constant-l-e ${prod_type_code} 4                         \
    -Output "${pflash_dir}/pcu_pflash_golden.srec" -Motorola -address-length=4 -Line_Length 46 -Disable Data_Count

# Fill holes in the header with 0's to match that of the binary file output
${srec_cat_exe} \
    "${pflash_dir}/pcu_pflash_golden.srec" -Motorola -fill 0x00 0x40000 0x40100 \
    -Output "${pflash_dir}/pcu_pflash_golden.srec" -Motorola -address-length=4 -Line_Length 46 -Disable Data_Count

# Golden Executable -> Raw Binary (no addressing)
${srec_cat_exe} "${pflash_dir}/pcu_pflash_golden.srec" -offset -0x40000 \
    -Output "${pflash_dir}/pcu_pflash.bin" -Binary

# Golden Executable -> Application 1 Exe (addr 0x8_0000)
${srec_cat_exe} "${pflash_dir}/pcu_pflash_golden.srec" -offset +0x40000 \
    -Output "${pflash_dir}/pcu_pflash_app1.srec" -Motorola -address-length=4 -Line_Length 46 -Disable Data_Count

# Golden Executable -> Application 2 Exe (addr 0xC_0000)
${srec_cat_exe} "${pflash_dir}/pcu_pflash_golden.srec" -offset +0x80000 \
    -Output "${pflash_dir}/pcu_pflash_app2.srec" -Motorola -address-length=4 -Line_Length 46 -Disable Data_Count

# Combine the full Bootloader with all 3 Application images
#
# Note: ORDER IS IMPORTANT, even though data comes out the same,
# the header differs which can mess up the elf file when debugging in eclipse.
${srec_cat_exe}  \
    -generate 0x00101000 0x00101008 -constant-l-e 0xFFFFFFFF 4 \
    "${bl_srec_dir}/Final_Bootloader_Output.srec" \
    "${pflash_dir}/pcu_pflash_golden.srec"        \
    "${pflash_dir}/pcu_pflash_app1.srec"          \
    "${pflash_dir}/pcu_pflash_app2.srec"          \
    -Output "${bl_dir}/bl_pcu_full.srec" -Motorola -address-length=4 -Line_Length 46 -Disable Data_Count

# Combine the full Bootloader, all 3 Application Exe images, and all 3 PT images
${srec_cat_exe}  \
    "${bl_dir}/bl_pcu_full.srec"                                   \
    "${pt_debug_srec}"                                             \
    "${pflash_dir}/credits.srec" -crop 0x0 0x2000 -offset 0x102000 \
    -Output "${all_dir}/all_bl_pcu_pt.srec" -Motorola -address-length=4 -Line_Length 46 -Disable Data_Count

