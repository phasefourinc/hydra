/* ###################################################################
**     This component module is generated by Processor Expert. Do not modify it.
**     Filename    : flexio_spi3.h
**     Project     : hydra_ec_sw
**     Processor   : S32K148_144
**     Component   : flexio_spi
**     Version     : Component SDK_S32K1xx_15, Driver 01.00, CPU db: 3.00.000
**     Repository  : SDK_S32K1xx_15
**     Compiler    : GNU C Compiler
**     Date/Time   : 2022-04-29, 14:34, # CodeGen: 1
**     Contents    :
**         FLEXIO_DRV_InitDevice                 - status_t FLEXIO_DRV_InitDevice(uint32_t instance,flexio_device_state_t *...
**         FLEXIO_DRV_DeinitDevice               - status_t FLEXIO_DRV_DeinitDevice(uint32_t instance);
**         FLEXIO_DRV_Reset                      - status_t FLEXIO_DRV_Reset(uint32_t instance);
**         FLEXIO_SPI_DRV_MasterInit             - status_t FLEXIO_SPI_DRV_MasterInit(uint32_t instance,const...
**         FLEXIO_SPI_DRV_MasterDeinit           - status_t FLEXIO_SPI_DRV_MasterDeinit(flexio_spi_master_state_t * master);
**         FLEXIO_SPI_DRV_MasterSetBaudRate      - status_t FLEXIO_SPI_DRV_MasterSetBaudRate(flexio_spi_master_state_t *...
**         FLEXIO_SPI_DRV_MasterGetBaudRate      - status_t FLEXIO_SPI_DRV_MasterGetBaudRate(flexio_spi_master_state_t *...
**         FLEXIO_SPI_DRV_MasterTransfer         - status_t FLEXIO_SPI_DRV_MasterTransfer(flexio_spi_master_state_t *...
**         FLEXIO_SPI_DRV_MasterTransferBlocking - status_t FLEXIO_SPI_DRV_MasterTransferBlocking(flexio_spi_master_state_t *...
**         FLEXIO_SPI_DRV_MasterTransferAbort    - status_t FLEXIO_SPI_DRV_MasterTransferAbort(flexio_spi_master_state_t * master);
**         FLEXIO_SPI_DRV_MasterGetStatus        - status_t FLEXIO_SPI_DRV_MasterGetStatus(flexio_spi_master_state_t *...
**         FLEXIO_SPI_DRV_SlaveInit              - status_t FLEXIO_SPI_DRV_SlaveInit(uint32_t instance,const...
**         FLEXIO_SPI_DRV_SlaveDeinit            - static inline status_t FLEXIO_SPI_DRV_SlaveDeinit(flexio_spi_slave_state_t *...
**         FLEXIO_SPI_DRV_SlaveTransfer          - static inline status_t FLEXIO_SPI_DRV_SlaveTransfer(flexio_spi_slave_state_t...
**         FLEXIO_SPI_DRV_SlaveTransferBlocking  - static inline status_t FLEXIO_SPI_DRV_S...
**         FLEXIO_SPI_DRV_SlaveTransferAbort     - static inline status_t FLEXIO_SPI_DRV_S...
**         FLEXIO_SPI_DRV_SlaveGetStatus         - static inline status_t FLEXIO_SPI_DRV_SlaveGetStatus(flexio_spi_slave_state_t...
**         FLEXIO_SPI_DRV_MasterGetDefaultConfig - void FLEXIO_SPI_DRV_MasterGetDefaultConfig(flexio_spi_master_user_config_t *...
**         FLEXIO_SPI_DRV_SlaveGetDefaultConfig  - void FLEXIO_SPI_DRV_SlaveGetDefaultConfig(flexio_spi_slave_user_config_t *...
**
**     Copyright 1997 - 2015 Freescale Semiconductor, Inc. 
**     Copyright 2016-2017 NXP 
**     All Rights Reserved.
**     
**     THIS SOFTWARE IS PROVIDED BY NXP "AS IS" AND ANY EXPRESSED OR
**     IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
**     OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
**     IN NO EVENT SHALL NXP OR ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
**     INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
**     (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
**     SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
**     HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
**     STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
**     IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
**     THE POSSIBILITY OF SUCH DAMAGE.
** ###################################################################*/
/*!
** @file flexio_spi3.h
** @version 01.00
*/         
/*!
**  @addtogroup flexio_spi3_module flexio_spi3 module documentation
**  @{
*/         
#ifndef flexio_spi3_H
#define flexio_spi3_H

/* MODULE flexio_spi3
 *
 * @page misra_violations MISRA-C:2012 violations
 *
 * @section [global]
 * Violates MISRA 2012 Advisory Rule 2.5, Global macro not referenced.
 * The macros are defined to be used by application code.
 */

/* Include inherited beans */
#include "clockMan0.h"
#include "dmaController0.h"
#include "Cpu.h"
#include "flexio_spi_driver.h"

/*! @brief Device instance number */
#define INST_FLEXIO_SPI3 0

/*! @brief Master configuration declaration */
extern const flexio_spi_master_user_config_t flexio_spi3_MasterConfig0;
    
/*! @brief Slave configuration declaration */
extern const flexio_spi_slave_user_config_t flexio_spi3_SlaveConfig0;
    



#endif
/* ifndef flexio_spi3_H */
/*!
** @}
*/
/*
** ###################################################################
**
**     This file was created by Processor Expert 10.1 [05.21]
**     for the Freescale S32K series of microcontrollers.
**
** ###################################################################
*/
