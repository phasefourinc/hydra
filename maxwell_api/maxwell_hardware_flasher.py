import json

from maxapi.maxwell_information_processor import process_maxwell_record, read_hardware_maxwell_record
from maxapi.mr_maxwell import MrMaxwell, MaxConst

DEFAULT_BAUD_RATE = "1000000"
DEFAULT_COM_PORT = "COM4"
DEFAULT_DATASOURCE = "sandbox"
HOST = "nekone.phasefour.co"
DEFAULT_MEASUREMENT = "andrew_pcu_dev"

mr_max = MrMaxwell(DEFAULT_COM_PORT, DEFAULT_BAUD_RATE, DEFAULT_DATASOURCE, DEFAULT_MEASUREMENT)


def _verify_maxwell_information(output,field_name, part_number, revision, serial_number):
    if output["field_name"] != field_name:
        return False
    if output["part_number"] != part_number:
        return False
    if output["revision"] != revision:
        return False
    if output["serial_number"] != serial_number:
        return False
    return True


'''
This will write a record in the maxwell hardware folder.
This is done in a fairly full proof way. 
Since we are writing a record to flash, if there is a overlap on information we have to erase the overlap. 
However since we are writing to flash you have to erase the whole sector.
Thus the way this is done is first it reads the current flash information and confirm if there will be overlap.
If there is no overlap it will just write the data and confirm it is written successfully.
However, if the record we are writing to already exist it will erase all of the maxwell record headers 
It will then re write all the records and replace the overlaped one with the one that was requested to be written.
'''
def write_maxwell_hardware_record(field_name, part_number, revision, serial_number, offset_location, transmit_mask = MaxConst.PROP_CONTROLLER_ADDRESS ):
    current_flash_information = mr_max.read_all_maxwell_hardware_records(transmit_mask=transmit_mask)
    same_index = None
    record_found = False
    # Determining if there the value want to write already exist in flash.
    for index, dict_element in enumerate(current_flash_information):
        if dict_element["flash_offset"] == offset_location:
            same_index = index
            record_found = True
            break
    # If there is a overlap with what we want to write,
    # erase the whole flash and re write the flash without the record that caused the overlap.
    if record_found:
        del current_flash_information[same_index]
        mr_max.mass_erase_max_hrd_info(transmit_mask=transmit_mask)
        for dict_element in current_flash_information:
            byte_arr = process_maxwell_record(dict_element["field_name"], dict_element["part_number"], dict_element["revision"], dict_element["serial_number"], dict_element["flash_offset"])
            mr_max.set_max_hrd_info(byte_arr, transmit_mask=transmit_mask)

    # Write the new record
    byte_arr = process_maxwell_record(field_name, part_number, revision, serial_number, offset_location)
    mr_max.set_max_hrd_info(byte_arr, transmit_mask=transmit_mask)
    # Confirm if the written record was successful
    payload = mr_max.get_max_hrd_info(offset_location, transmit_mask= transmit_mask)
    output = read_hardware_maxwell_record(payload["raw_payload"], payload)
    if _verify_maxwell_information(output, field_name, part_number, revision, serial_number):
        print("Write Succeeded")
    else:
        print("Write Failed here is what was read from flash:\n" + str(output))
        print("\nYou put in the following\n"
               "field_name: " + str(field_name) + "\n" 
               "part_number: " + str(part_number) + "\n"
               "revision: " + str(revision) + "\n"
               "serial_number: " + str(serial_number) + "\n" 
               "offset_location: " + str(offset_location) + "\n")

    print("The current flash record")
    current_flash_information_new = mr_max.read_all_maxwell_hardware_records(transmit_mask=transmit_mask)
    current_flash_information_json = json.dumps(current_flash_information_new, indent=4, sort_keys=True)
    print(current_flash_information_json)



# Testing the write_maxwell_hardware_record
def test_example(transmit_mask):
    write_maxwell_hardware_record("Poop Head", "001 335-201", "Rev 1", "666-666", 0, transmit_mask=transmit_mask)
    print("\n\n NEXT SECTION \n\n")
    write_maxwell_hardware_record("How old are you", "001 335-201", "Rev 1", "123-123", 0, transmit_mask=transmit_mask)
    print("\n\n NEXT SECTION \n\n")
    write_maxwell_hardware_record("This is a rec", "001 335-201", "Rev 1", "999-999", 0, transmit_mask=transmit_mask)
    print("\n\n NEXT SECTION \n\n")
    write_maxwell_hardware_record("Another Record", "001 335-201", "Rev 1", "666-555", 13, transmit_mask=transmit_mask)



def write_prop_controller_hardware_record(field_name,rev_number, serial_number,offset):
    write_maxwell_hardware_record(field_name, "001277-202", rev_number, serial_number, offset, transmit_mask=MaxConst.PROP_CONTROLLER_ADDRESS)


def write_thrust_controller_hardware_record(field_name,rev_number, serial_number,offset):
    write_maxwell_hardware_record(field_name, "001335-202", rev_number, serial_number, offset, transmit_mask=MaxConst.THRUSTER_CONTROLLER_ADDRESS)


def main():
    mr_max.mass_erase_max_hrd_info(transmit_mask=MaxConst.PROP_CONTROLLER_ADDRESS)
    # write_prop_controller_hardware_record("Prop Controller", "Rev A", "A00093", 0) # Prop Controller Example
    # write_thrust_controller_hardware_record("Thruster Controller", "Rev A", "A00130", 0) # Thruster Controller Example





if __name__ == "__main__":
    main()