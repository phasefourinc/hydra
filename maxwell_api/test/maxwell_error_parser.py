import datetime
import pathlib
import csv
class MaxwellErrorParser ():
    def __init__(self):
        self.sram_record_bytes = 16


    def parse_sram_error_record(self ,byte_input, from_flash=False):
        parsed_error_record = {
            "MCU Time": None,
            "Error ID": None,
            "Aux Data": None,
            "Maxwell State": None
        }
        if len(byte_input) != 16:
            print("Error does not have the proper amount of bytes to parse")
            return parsed_error_record

        timestamp_blank = self.is_bytes_blank(byte_input,8, from_flash)
        if not timestamp_blank:
            parsed_error_record["MCU Time"] =  datetime.datetime.fromtimestamp(int( self._parse_error_bytes_together(byte_input, 0, 8) / 1000000.0))
        else:
            # print("MCU Time value is blank")
            parsed_error_record["MCU Time"] =  "Blank"
        parsed_error_record["Error ID"] = self._parse_error_bytes_together(byte_input, 8, 2)
        parsed_error_record["Aux Data"] = self._parse_error_bytes_together(byte_input, 10, 4)
        parsed_error_record["Maxwell State"] = self._parse_error_bytes_together(byte_input, 14, 1)
        return parsed_error_record

    def parse_bulk_sram_error_records(self, mutiple_bytes,from_flash = False):
        parsed_records = []
        if len(mutiple_bytes)%16 !=0:
            print("The byte amount you sent is not a multiple of error records.")
            return parsed_records
        number_of_records = int(len(mutiple_bytes)/16)
        count = 0
        for x in range(number_of_records):
            byte_input = mutiple_bytes[count:count + self.sram_record_bytes]
            parsed_records.append(self.parse_sram_error_record(byte_input,from_flash))
            count = count + self.sram_record_bytes
        return parsed_records



    def check_if_parsed_error_record_is_blank(self, error_record, type):
        if type == "sram":
            if error_record["MCU Time"] == "Blank" and error_record["Error ID"] == 0x00 and error_record["Aux Data"] == 0x00 and error_record["Maxwell State"] == 0x00:
                return True
            return False
        elif type == "flash":
            if error_record["MCU Time"] == "Blank" and error_record["Error ID"] == 0xFFFF and error_record[ "Aux Data"] == 0xFFFFFFFF and error_record["Maxwell State"] == 0xFF:
                return True
            return False
        else:
            print("Invalid type argument: valid values are 'sram' and 'flash'")
            return False



    def parse_tlm_error_record(self ,byte_input):
        parsed_error_tlm_record = {
            "Last Error Power Up": None,
            "First Error": {
                "Error ID": None,
                "Aux Data": None,
            },
            "Second Error": {
                "Error ID": None,
                "Aux Data": None,
            },
            "Third Error": {
                "Error ID": None,
                "Aux Data": None,
            },
            "Most Recent Error 1": {
                "Error ID": None,
                "Aux Data": None,
            },
            "Most Recent Error 2": {
                "Error ID": None,
                "Aux Data": None,
            },
            "Most Recent Error 3": {
                "Error ID": None,
                "Aux Data": None,
            },
            "Most Frequent Error ID": None,
            "Error ID For Spacecraft": None,
            "Total Error Count": None,
            "Spacecraft Error 1": None,
            "Spacecraft Error 2": None,
            "Spacecraft Error 3": None
        }

        if len(byte_input) != 50:
            print("Telm Error Packet does not have the proper amount of bytes to parse")
            return parsed_error_tlm_record

        parsed_error_tlm_record[ "Last Error Power Up"] = self._parse_error_bytes_together(byte_input, 0, 2)
        list_of_field_to_populate = ["First Error", "Second Error", "Third Error", "Most Recent Error 1", "Most Recent Error 2", "Most Recent Error 3"]
        count = 2
        for x in list_of_field_to_populate:
            parsed_error_tlm_record[x]["Error ID"] = self._parse_error_bytes_together(byte_input, count, 2)
            parsed_error_tlm_record[x]["Aux Data"] = self._parse_error_bytes_together(byte_input, count+2, 4)
            count = count + 6

        parsed_error_tlm_record["Most Frequent Error ID"] = self._parse_error_bytes_together(byte_input, 38, 2)
        parsed_error_tlm_record["Number of Errors from Most Freq. Error ID"] = self._parse_error_bytes_together(byte_input, 40, 2)
        parsed_error_tlm_record["Total Error Count"] = self._parse_error_bytes_together(byte_input, 42, 2)
        parsed_error_tlm_record["Spacecraft Error 1"] = self._parse_error_bytes_together(byte_input, 44, 2)
        parsed_error_tlm_record["Spacecraft Error 2"] = self._parse_error_bytes_together(byte_input, 46, 2)
        parsed_error_tlm_record["Spacecraft Error 3"] = self._parse_error_bytes_together(byte_input, 48, 2)


        return parsed_error_tlm_record




    def generate_sram_error_record_csv_report(self, csv_name, error_records, csv_mode = 'w'):
        final_csv_name = self._write_csv_name(csv_name)
        field_names = ["Error Number","MCU Time","Error ID","Aux Data","Maxwell State"]
        field_names = ["Error Number"]
        count = 1
        if error_records:
            if isinstance(error_records, list):
                field_names.extend(error_records[0].keys())
                with open(final_csv_name, mode=csv_mode, newline='') as csvfile:
                    csv_writer = csv.DictWriter(csvfile, fieldnames = field_names, extrasaction='ignore')
                    csv_writer.writeheader()
                    for dict_data in error_records:
                        dict_data["Error Number"] = count
                        csv_writer.writerow(dict_data)
                        count = count + 1
            elif isinstance(error_records, dict):
                field_names.extend(error_records.keys())
                with open(final_csv_name, mode=csv_mode, newline='') as csvfile:
                    csv_writer = csv.DictWriter(csvfile, fieldnames=field_names, extrasaction='ignore')
                    csv_writer.writeheader()
                    error_records["Error Number"] = 1
                    csv_writer.writerow(error_records)

            else:
                print("error_records not a valid input. Please input a dict or a list.")

        return


    def is_bytes_blank(self,payload, check_length,from_flash=False):
        if from_flash:
            blank_value = 0xFF
        else:
            blank_value = 0x00
        for x in range(check_length):
            if payload[x] != blank_value:
                return False
        return True
    def generate_tlm_error_record_csv_report(self, csv_name, error_records, csv_mode = 'w'):
        final_csv_name = self._write_csv_name(csv_name)

        with open(final_csv_name,  mode=csv_mode, newline='') as csvfile:  # Just use 'w' mode in 3.x
            w = csv.DictWriter(csvfile, error_records.keys())
            w.writeheader()
            w.writerow(error_records)




    def _write_csv_name(self,csv_name):
        pathlib.Path('Outputs').mkdir(parents=True, exist_ok=True)
        if ".csv" in csv_name:
            csv_name = csv_name.replace('.csv', '')
        return csv_name + ".csv"

    def _parse_error_bytes_together(self ,byte_input ,start_address ,num_of_bytes):
        parsed_data = 0
        for count in range(num_of_bytes):
            parsed_data = parsed_data + ((byte_input[start_address +count]) << (8 * (count)))  # Calculating the total value of the part.
        return parsed_data
