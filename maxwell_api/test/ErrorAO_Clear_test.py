from maxapi.mr_maxwell_b2 import MrMaxwell
from maxapi.max_cmd_id import MaxCmd
from maxwell_error_parser import MaxwellErrorParser
import time
import sys
DEFAULT_BAUD_RATE =  "1000000"
DEFAULT_COM_PORT = "COM4"
DEFAULT_DATASOURCE = "sandbox"
HOST = "nekone.phasefour.co"
DEFAULT_MEASUREMENT = "andrew_pcu_dev"
DEFAULT_HARDWARE = "Prop Controller" # Options are "Thruster Controller" and "Prop Controller"

mr_max = MrMaxwell(DEFAULT_COM_PORT, DEFAULT_BAUD_RATE, DEFAULT_DATASOURCE, DEFAULT_MEASUREMENT)
mr_max.update_default_interfacting_hardware(DEFAULT_HARDWARE)

WRT_ERR_ERRAO = MaxCmd.WRT_ERR
sram_start = 0x20000010
telm_start = 0x20001010
sram_record_length = 16
telm_record_length = 50


def clear_sram_test():
    print("Starting clear sram_test")
    mr_max.goto_manual()
    mr_max.clear_errors_advanced("Sram Error Record")
    for x in range(1, 31):
        send_bulk_error(x)

    dump_mcu_payload = get_all_record_errors()
    index = 0
    for x in range(256):
        is_blank = is_record_blank(dump_mcu_payload[index:index + sram_record_length], 8)
        if (is_blank == True):
            print("CLEAR SRAM TEST FAILED DID NOT WRITE ERRORS CORRECTLY")
            sys.exit()
        index = index + sram_record_length
    mr_max.clear_errors_advanced("Sram Error Record")
    time.sleep(.5)
    dump_mcu_payload = get_all_record_errors()
    index = 0
    for x in range(256):
        is_blank = is_record_blank(dump_mcu_payload[index:index + sram_record_length], 8)
        if (is_blank == False):
            print("CLEAR SRAM TEST FAILED")
            sys.exit()
        index = index + sram_record_length
    print("Clear SRAM Pass")


def clear_telm_test():
    parse_err = MaxwellErrorParser()
    mr_max.goto_manual()
    mr_max.clear_errors_advanced("Telemetry Error Record")
    print("Starting clear_telm_test")

    for x in range(1, 11):
        mr_max.send_maxwell_message(WRT_ERR_ERRAO, [0x00, x, 0xFF])

    dump_mcu_payload = mr_max.dump_mcu(telm_start, telm_record_length)["raw_payload"][2:]
    output = parse_err.parse_tlm_error_record(dump_mcu_payload)
    complete_errors = does_telm_have_errors(output)
    if (complete_errors == False):
        print("CLEAR TELM TEST FAILED DID NOT WRITE ERRORS CORRECTLY")
        sys.exit()

    mr_max.clear_errors_advanced("Telemetry Error Record")

    dump_mcu_payload = mr_max.dump_mcu(telm_start, telm_record_length)["raw_payload"][2:]
    output = parse_err.parse_tlm_error_record(dump_mcu_payload)
    is_blank = is_telm_packet_blank(output)
    if (is_blank == False):
        print("CLEAR TELM TEST FAILED DID NOT Erase ERRORS CORRECTLY")
        sys.exit()
    print("Clear Telm test Pass")


def parse_sram_records():
    mr_max.goto_manual()
    parse_err = MaxwellErrorParser()
    mr_max.send_maxwell_message(WRT_ERR_ERRAO, [0x00, 0x01, 0xFF])
    mr_max.send_maxwell_message(WRT_ERR_ERRAO, [0x00, 0x07, 0xFF])
    mr_max.send_maxwell_message(WRT_ERR_ERRAO, [0x00, 0x0A, 0xFF])
    mr_max.send_maxwell_message(WRT_ERR_ERRAO, [0x00, 0x015, 0xFF])
    mr_max.send_maxwell_message(WRT_ERR_ERRAO, [0x00, 0x010, 0xFF])
    mr_max.send_maxwell_message(WRT_ERR_ERRAO, [0x00, 0x03, 0xFF])
    mr_max.send_maxwell_message(WRT_ERR_ERRAO, [0x00, 0x011, 0xFF])
    payload = gather_errors_sram(sram_start, 7)
    output1 = parse_err.parse_bulk_sram_error_records(payload)
    print(output1)
    output2 = dict(output1[0])
    parse_err.generate_sram_error_record_csv_report("Errors_I_Discovered.csv", output1)
    parse_err.generate_sram_error_record_csv_report("ONE_ERRORI_Discovered.csv", output2)


def parse_tlm_records():
    parse_err = MaxwellErrorParser()
    mr_max.send_maxwell_message(WRT_ERR_ERRAO, [0x00, 0x01, 0xFF])
    mr_max.send_maxwell_message(WRT_ERR_ERRAO, [0x00, 0x07, 0xFF])
    mr_max.send_maxwell_message(WRT_ERR_ERRAO, [0x00, 0x0A, 0xFF])
    mr_max.send_maxwell_message(WRT_ERR_ERRAO, [0x00, 0x015, 0xFF])
    mr_max.send_maxwell_message(WRT_ERR_ERRAO, [0x00, 0x010, 0xFF])
    mr_max.send_maxwell_message(WRT_ERR_ERRAO, [0x00, 0x03, 0xFF])
    mr_max.send_maxwell_message(WRT_ERR_ERRAO, [0x00, 0x011, 0xFF])
    dump_mcu_payload = mr_max.dump_mcu(telm_start, telm_record_length)["raw_payload"][2:]
    output = parse_err.parse_tlm_error_record(dump_mcu_payload)
    print(output)
    parse_err.generate_tlm_error_record_csv_report("Telm_discover.csv", output)


def get_all_record_errors():
    payload = gather_errors_sram(sram_start, 256)
    return payload


def send_bulk_error(error_id):
    for x in range(9):
        mr_max.send_maxwell_message(WRT_ERR_ERRAO, [0x00, error_id, 0xFF])


def gather_errors_sram(start_sram, total_number_of_errors):
    number_of_full_mcu_requests = total_number_of_errors // 15  # integer division
    final_amount_of_errors_to_request = total_number_of_errors % 15
    final_payload = []
    memory_address = start_sram
    for x in range(number_of_full_mcu_requests):
        payload = mr_max.dump_mcu(memory_address, 15 * sram_record_length)
        memory_address = memory_address + sram_record_length * 15
        final_payload.extend(payload["raw_payload"][2:])

    if final_amount_of_errors_to_request != 0:
        payload = mr_max.dump_mcu(memory_address, final_amount_of_errors_to_request * sram_record_length)
        final_payload.extend(payload["raw_payload"][2:])

    return final_payload


def does_telm_have_errors(telm_dict):
    error_clear = False
    arr_of_tlm_errors = ["First Error","Second Error","Third Error","Most Recent Error 1","Most Recent Error 2","Most Recent Error 3"]
    for x in arr_of_tlm_errors:
        if (telm_dict[x]["Error ID"] == 0 )or (telm_dict[x]["Aux Data"] == 0):
            error_clear =  True
    if telm_dict["Most Frequent Error ID"] == 0:
        error_clear = True
    if telm_dict["Number of Errors from Most Freq. Error ID"] == 0:
        error_clear = True
    if telm_dict["Total Error Count"] == 0:
        error_clear = True
    if telm_dict["Spacecraft Error 1"] == 0:
        error_clear = True
    if telm_dict["Spacecraft Error 2"] == 0:
        error_clear = True
    if telm_dict["Spacecraft Error 3"] == 0:
        error_clear = True

    if error_clear == True:
        print("TELM HAS SOME ERRORS CLEAR")
        return False
    else:
        return True

def is_telm_packet_blank(telm_dict):
    error_clear = True
    arr_of_tlm_errors = ["First Error","Second Error","Third Error","Most Recent Error 1","Most Recent Error 2","Most Recent Error 3"]
    for x in arr_of_tlm_errors:
        if (telm_dict[x]["Error ID"] != 0 )or (telm_dict[x]["Aux Data"] != 0):
            error_clear = False
    if telm_dict["Most Frequent Error ID"] != 0:
        error_clear = False
    if telm_dict["Number of Errors from Most Freq. Error ID"] != 0:
        error_clear = False
    if telm_dict["Total Error Count"] != 0:
        error_clear = False
    if telm_dict["Spacecraft Error 1"] != 0:
        error_clear = False
    if telm_dict["Spacecraft Error 2"] != 0:
        error_clear = False
    if telm_dict["Spacecraft Error 3"] != 0:
        error_clear = False


    if error_clear == False:
        print("Some of the errors where not cleared")
        return False
    else:
        return True





def is_record_blank(payload, check_length):
    for x in range(check_length):
        if payload[x] != 0x00:
            print("Not Blank")
            return False
    print("Blank")
    return True


#NOTE IF YOU RUN THIS TEST MUTIPLE TIMES WITHOUT A POWER CYCLE IT WILL FAIL. DUE TO ERROR FILTERING POWER CYCLE MAXWELL EACH TIME YOU RUN.
def main():
    mr_max.clear_errors()
    mr_max.set_auto_tlm_enable(500)
    time.sleep(1)
    clear_sram_test()
    clear_telm_test()
    print("Test complete")


if __name__ == "__main__":
    main()
