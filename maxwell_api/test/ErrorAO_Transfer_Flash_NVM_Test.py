from maxapi.mr_maxwell_b2 import MrMaxwell
from maxapi.max_cmd_id import MaxCmd
from maxapi.max_errors import MaxErr
from maxwell_error_parser import MaxwellErrorParser
from chozo.power_supplies.maxwell_bk_precision_9201 import MaxwellBkPrecision9201
import time
import sys
import random

Power_Supply = MaxwellBkPrecision9201()
DEFAULT_BAUD_RATE = "1000000"
DEFAULT_COM_PORT = "COM4"
DEFAULT_DATASOURCE = "sandbox"
HOST = "nekone.phasefour.co"
DEFAULT_MEASUREMENT = "andrew_pcu_dev"
DEFAULT_HARDWARE = "Prop Controller"  # Options are "Thruster Controller" and "Prop Controller"
# Power_Supply.reset_power_supply(1, 1)
mr_max = MrMaxwell(DEFAULT_COM_PORT, DEFAULT_BAUD_RATE, DEFAULT_DATASOURCE, DEFAULT_MEASUREMENT)
mr_max.update_default_interfacting_hardware(DEFAULT_HARDWARE)
WRT_ERR_ERRAO = MaxCmd.WRT_ERR

flash_record_partition_one_address = 0x000F0000
flash_record_partition_two_address = 0x000F1000

sram_start = 0x20000010
telm_start = 0x20001010
sram_record_length = 16
telm_record_length = 50

# when we write a error we have to do it in a non ideal way.
# Basically when we send a index of writing a error (example 0 ) there is a look up of the type of error to send.
# Because we are going to be looking at the contents we need to know exactly what error we sent so we are making a lookup to mimic what is on the embedded system.

# We are putting Status_Success even though it is not techincally a error. The reason for this is to keep the index in sync with embedded system were EMBEDDED_TIMEOUT is equal to index 1.

write_error_dictonary = [{"Error ID": MaxErr.STATUS_SUCCESS}, {"Error ID": MaxErr.EMBEDDED_TIMEOUT},
                         {"Error ID": MaxErr.NOT_ENOUGH_BYTES}, {"Error ID": MaxErr.MALFORMED_HEADER},
                         {"Error ID": MaxErr.SYNC_BYTES_ERROR},

                         {"Error ID": MaxErr.SOURCE_ADDRESS_ERROR}, {"Error ID": MaxErr.TRANSMIT_MASK_ERROR},
                         {"Error ID": MaxErr.PKT_DATA_LEN_ERROR},

                         {"Error ID": MaxErr.COM_RX_CRC_ERROR}, {"Error ID": MaxErr.MSG_ID_FUNCTION_NOT_IMPLEMENTED},
                         {"Error ID": MaxErr.CURRENT_PROCESSED_COMMAND_TIMEDOUT},

                         {"Error ID": MaxErr.UNEXPECTED_TRANSITION}, {"Error ID": MaxErr.AD568_SPI_SET_FAILED},
                         {"Error ID": MaxErr.MDOT_SET_POINT_OUT_OF_BOUNDS},

                         {"Error ID": MaxErr.INVALID_TX_PARAMETERS},
                         {"Error ID": MaxErr.INTERMEDIATE_TX_QUEUE_HAS_NO_DATA}, {"Error ID": MaxErr.TX_AO_IS_BUSY},

                         {"Error ID": MaxErr.TLM_TMR_TIMEOUT}, {"Error ID": MaxErr.INVALID_TELEMTRY_EVENT_REQUEST},
                         {"Error ID": MaxErr.RX_SC_RECIVIED_JUNK_DATA},

                         {"Error ID": MaxErr.RECEIVED_MALFORM_CLANK_PACKET}, {"Error ID": MaxErr.RECEIVED_RXSC_TIMEOUT},
                         {"Error ID": MaxErr.MISMATCH_CRC},

                         {"Error ID": MaxErr.GSE_RX_CIRCULAR_BUFFER_OVERRUN},
                         {"Error ID": MaxErr.EVT_CMD_DUMP_TLM_INVALID_SIZE}, {"Error ID": MaxErr.EVT_DATA_OP_TIMEOUT},

                         {"Error ID": MaxErr.CMD_INVALID_DURATION_SETPT}, {"Error ID": MaxErr.CMD_DATA_MODE_IS_BUSY},
                         {"Error ID": MaxErr.CMD_UPDATE_STAGE_INVALID_PARAM},

                         {"Error ID": MaxErr.CMD_UPDATE_PRELOAD_INVALID_PARAM},
                         {"Error ID": MaxErr.CMD_UPDATE_PROGRAM_INVALID_PARAM},
                         {"Error ID": MaxErr.CMD_DATA_ABORT_DUE_TO_SAFE},
                         ]



def generate_unique_errors_from_write_error_dictonary(num_of_errors):
    if num_of_errors >31:
        print("Cant generate more unqiue errors then the maximum.")
    error_id_list = []
    error_id_list_offset = []
    # making a list of 10 unique errors
    while len(error_id_list_offset) < num_of_errors:
        error_offset = random.randint(1, len(write_error_dictonary) - 1)  # Generate a offset of the error for write_error_dictonary (1, 31)
        if not (error_offset in error_id_list_offset):
            error_id_list_offset.append(error_offset)
            error_id_list.append(write_error_dictonary[error_offset]["Error ID"])
    return error_id_list, error_id_list_offset
'''
This is a primitive test the goal is not to use this in the test chain but more of a basic tutorial test to reference more complex tests.
If all of the tests are failing this is the basic of the basic test. 
It sends one error and confirms that one error is in flash and all the other errors are blank.
'''
def primitive_test():
    print("Starting primitive test.")
    error_parser = MaxwellErrorParser()
    mr_max.goto_manual()
    mr_max.clear_errors()
    output = mr_max.send_maxwell_message(MaxCmd.WRT_ERR, [0x00, 0x1, 0xFF])
    if output["status"] != "STATUS_SUCCESS":
        print("Primitive Test Fail. Could not write error.")
        return False
    output = mr_max.send_maxwell_message(MaxCmd.TRN_SRAM_FLASH, [])
    if output["status"] != "STATUS_SUCCESS":
        print("Primitive Test Fail. Could not transfer flash.")
        return False
    parsed_errors = gather_and_parse_errors(flash_record_partition_one_address, 1, error_parser)
    if not confirm_errors_via_error_id(parsed_errors[0], write_error_dictonary[1]["Error ID"]):
        print("Primitive Test Fail. Reading from Flash did not get the same error. Looks like transfer failed.")
        return False


    Power_Supply.reset_power_supply(1, 1)
    # After resetting the supply we are going to confirm the startup error and then grab the whole error record and confirm they are the same.
    mr_max.set_utime()
    mr_max.goto_manual()
    if not confirm_last_error_in_flash(write_error_dictonary[1]["Error ID"]):
        print("Primitive Test Fail. We did not get the latest error from flash after powerup.")
        return False
    parsed_errors = get_and_parse_all_error_records("flash", error_parser)
    list_of_error_ids = [write_error_dictonary[1]["Error ID"]]

    list_of_error_ids.extend(write_blank_error_id_record(511, "flash"))

    if not confirm_errors_via_error_id(parsed_errors, list_of_error_ids):
        print("Primitive Test Fail. Reading the entire Flash record did not get the same error record as expected. Ethier of the flash values are wrong or there is was suppose to be blank and there was not.")
        return False

    print("Primitive Test Passed")
    return True


'''
This is basic test one
This Test does the following things:
   - Confirms basic functionality of writing error records into sram
   - Confirms basic functionality of writing sram errors into flash. By a explict command.
   - Confirms functionality of appending new errors in flash after previous errors are there.
   - Confirms algorithm of gathering the latest error in flash and putting that value it into telemetry.
   - Confirms errors stay in flash after power reset.
   
The high level procedure is:
    - Clear Errors
    - Specify a error to write
    - Write the same error 9 times (to avoid filtering)
    - Tell MCU to put errors in flash
    - Confirm they are in flash
    - Reset Board
    - Reconfirm they stayed in flash and confirm what the latest error is.
    - Repeat steps (2 -7) with a different error, and confirm the previous errors were not erased after the power reset.
'''

def  basic_test_one():
    print("Starting Basic Test One.")
    error_written_to_flash_offset_start = random.randint(1,len(write_error_dictonary)-1)
    error_written_to_flash_start = write_error_dictonary[error_written_to_flash_offset_start]["Error ID"]
    error_parser = MaxwellErrorParser()
    mr_max.goto_manual()
    mr_max.clear_errors()
    if not check_if_flash_is_blank():
        return False

    output = send_bulk_error(error_written_to_flash_offset_start)
    if not output:
        print("Basic Test One Failed. Could not write all of the errors error.")
        return False

    output = mr_max.send_maxwell_message(MaxCmd.TRN_SRAM_FLASH, [])
    if output["status"] != "STATUS_SUCCESS":
        print("Basic Test One Failed. Could not transfer flash.")
        return False

    parsed_errors = gather_and_parse_errors(flash_record_partition_one_address, 9, error_parser)
    list_of_error_ids = write_error_id_records(9,error_written_to_flash_start)
    if not confirm_errors_via_error_id(parsed_errors, list_of_error_ids):
        print("Basic Test One Failed. Reading from Flash did not get the same errors as written. Looks like transfer failed.")
        return False


    Power_Supply.reset_power_supply(1, 1)
    # After resetting the supply we are going to confirm the startup error and then grab the whole error record and confirm they are the same.

    mr_max.goto_manual()
    mr_max.set_utime()
    if not confirm_last_error_in_flash(error_written_to_flash_start):
        print("Basic Test One Failed. We did not get the latest error from flash after powerup.")
        return False
    parsed_errors = get_and_parse_all_error_records("flash", error_parser)
    list_of_error_ids = fill_out_complete_flash_record_error_ids(list_of_error_ids)

    if not confirm_errors_via_error_id(parsed_errors, list_of_error_ids):
        print("Basic Test One Failed. Reading the entire Flash record did not get the same error record as expected. Ethier of the flash values are wrong or there is was suppose to be blank and there was not.")
        return False

    # Run the same setup again but this time we are going to write a new set of errors.


    # Keep searching until we get a new error that was not the same as last time. This is so we can visuall compare how a new error comes in after a reset.
    error_written_to_flash_offset_end = error_written_to_flash_offset_start
    while error_written_to_flash_offset_end == error_written_to_flash_offset_start:
        error_written_to_flash_offset_end = random.randint(1,len(write_error_dictonary)-1)

    error_written_to_flash_end = write_error_dictonary[error_written_to_flash_offset_end]["Error ID"]

    output = send_bulk_error(error_written_to_flash_offset_end)
    if not output:
        print("Basic Test One Failed. Could not write all of the errors error.")
        return False

    output = mr_max.send_maxwell_message(MaxCmd.TRN_SRAM_FLASH, [])
    if output["status"] != "STATUS_SUCCESS":
        print("Basic Test One Failed. Could not transfer flash.")
        return False

    parsed_errors = gather_and_parse_errors(flash_record_partition_one_address, 18, error_parser)
    list_of_error_ids = write_error_id_records(9, error_written_to_flash_start)
    list_of_error_ids.extend(write_error_id_records(9, error_written_to_flash_end))
    if not confirm_errors_via_error_id(parsed_errors, list_of_error_ids):
        print("Basic Test One Failed. Reading from Flash did not get the same errors as written. Looks like transfer failed.")
        return False
    Power_Supply.reset_power_supply(1, 1)

    mr_max.goto_manual()
    mr_max.set_utime()
    if not confirm_last_error_in_flash(error_written_to_flash_end):
        print("Basic Test One Failed. We did not get the latest error from flash after powerup.")
        return False
    parsed_errors = get_and_parse_all_error_records("flash", error_parser)
    list_of_error_ids = fill_out_complete_flash_record_error_ids(list_of_error_ids)

    if not confirm_errors_via_error_id(parsed_errors, list_of_error_ids):
        print("Basic Test One Failed. Reading the entire Flash record did not get the same error record as expected. Ethier of the flash values are wrong or there is was suppose to be blank and there was not.")
        return False
    print("Basic Test One Passed")
    return True


def pick_latest_error_in_flash_test():
    print("Starting Latest Error In Flash Test.")
    error_id_list, error_id_list_offset = generate_unique_errors_from_write_error_dictonary(10)

    # We should now have a list of unique errors.

    error_parser = MaxwellErrorParser()
    mr_max.goto_manual()
    mr_max.clear_errors()

    if not check_if_flash_is_blank():
        print("Starting Latest Error In Flash Test Failed. When checking if the flash is blank it is found out it was not blank.")
        return False

    # In this loop we are confirming that the latest error function is working correctly.
    # This is done by sending one error writing to flash and then confirming the latest error has been updated.

    for index, error in enumerate(error_id_list):
        output = send_error(error_id_list_offset[index])
        if not output:
            print("Starting Latest Error In Flash Test Failed. Could not write a error. On index " + str(index))
            return False
        output = mr_max.send_maxwell_message(MaxCmd.TRN_SRAM_FLASH, [])
        if output["status"] != "STATUS_SUCCESS":
            print("Starting Latest Error In Flash Test Failed. Could not transfer flash. On index " +str(index))
            return False
        Power_Supply.reset_power_supply(.250, .750)
        mr_max.goto_manual()
        mr_max.set_utime()
        if not confirm_last_error_in_flash(error):
            print("Starting Latest Error In Flash Test Failed. We did not get the latest error from flash after powerup. On index "+ str(index))
            return False
        print("Latest Error Test passed for error "+ str(error))

    # After the previous test passed we are going to write all the errors again
    # And then do a final check in flash including verifying blanks.

    output = send_errors_via_error_id_list(error_id_list_offset) # We are resending all the errors to see if the check_latest gets update after mutiple sends.
    if not output:
        print("Starting Latest Error In Flash Test Failed. Could not write all of the unique errors.")
        return False

    output = mr_max.send_maxwell_message(MaxCmd.TRN_SRAM_FLASH, [])
    if output["status"] != "STATUS_SUCCESS":
        print("Starting Latest Error In Flash Test Failed. Could not transfer flash.")
        return False


    Power_Supply.reset_power_supply(1,1)
    # After resetting the supply we are going to confirm the startup error and then grab the whole error record and confirm they are the same.

    mr_max.goto_manual()
    mr_max.set_utime()
    # Confirm the last error is the last error we sent -1 is the last index of this list.
    if not confirm_last_error_in_flash(error_id_list[-1]):
        print("Starting Latest Error In Flash Test Failed. We did not get the latest error from flash after powerup.")
        return False

    parsed_errors = get_and_parse_all_error_records("flash", error_parser)
    error_id_list = error_id_list + error_id_list  # Combining the list because we sent the list twice.
    list_of_error_ids = fill_out_complete_flash_record_error_ids(error_id_list)

    if not confirm_errors_via_error_id(parsed_errors, list_of_error_ids):
        print("Starting Latest Error In Flash Test Failed. Reading the entire Flash record did not get the same error record as expected. Ethier of the flash values are wrong or there is was suppose to be blank and there was not.")
        return False

    print("Starting Latest Error In Flash Test Passed")
    return True



def  basic_test_two():
    print("Starting Basic Test Two.")
    unique_err_id_list, unique_err_id_list_offset = generate_unique_errors_from_write_error_dictonary(3)
    error_parser = MaxwellErrorParser()
    mr_max.goto_manual()
    mr_max.clear_errors()
    if not check_if_flash_is_blank():
        return False
    # Writing 3 bulk errors to prop controller.
    for error_offset in unique_err_id_list_offset:
        output = send_bulk_error(error_offset)
        if not output:
            print("Basic Test Two Failed. Could not write all of the errors error.")
            return False

    output = mr_max.goto_safe() # Going to safe. We should see the flash sent
    if output["status"] != "STATUS_SUCCESS":
        print("Basic Test Two Failed. Could not got to safe rug ro raggy")
        return False
    mr_max.goto_manual()
    parsed_errors = gather_and_parse_errors(flash_record_partition_one_address, 9*len(unique_err_id_list), error_parser)

    list_of_error_ids = []
    for error in unique_err_id_list:
        list_of_error_ids = list_of_error_ids +  write_error_id_records(9, error)
    if not confirm_errors_via_error_id(parsed_errors, list_of_error_ids):
        print("Basic Test Two Failed. Reading from Flash did not get the same errors as written. Looks like transfer failed.")
        return False

    Power_Supply.reset_power_supply(1, 1)
    # After resetting the supply we are going to confirm the startup error and then grab the whole error record and confirm they are the same.

    mr_max.goto_manual()
    mr_max.set_utime()
    if not confirm_last_error_in_flash(unique_err_id_list[-1]):
        print("Basic Test Two Failed. We did not get the latest error from flash after powerup.")
        return False
    parsed_errors = get_and_parse_all_error_records("flash", error_parser)
    list_of_error_ids = fill_out_complete_flash_record_error_ids(list_of_error_ids)

    if not confirm_errors_via_error_id(parsed_errors, list_of_error_ids):
        print("Basic Two Two Failed. Reading the entire Flash record did not get the same error record as expected. Ethier of the flash values are wrong or there is was suppose to be blank and there was not.")
        return False

    print("Basic Test Two Passed")
    return True

def basic_test_three_underflow_error_condition():

    print("Starting Basic Test Three.")
    error_parser = MaxwellErrorParser()
    mr_max.goto_manual()
    mr_max.clear_errors()
    if not check_if_flash_is_blank():
        return False
    #  randomly_select the amount of errors to send. It should be on a bigger scale though random.randint(10, len(write_error_dictonary) - 1)
    unique_err_id_list, unique_err_id_list_offset = generate_unique_errors_from_write_error_dictonary(17)
    error_amount = 0
    # We are writing for a total of 7 errors.
    # This for loop is here because we are sending the data 1 uinque error (9 error per unique error) at a time.
    # This is due to the fact that the current mcu has to keep a t 10 ms pulse to go through all the AO and writing to many errors will stop that.
    for index, error in enumerate(unique_err_id_list):
        output = send_bulk_error(unique_err_id_list_offset[index])
        if not output:
            print("Starting Latest Error In Flash Test Failed. Could not write a error. On index " + str(index))
            return False
        output = mr_max.send_maxwell_message(MaxCmd.TRN_SRAM_FLASH, [])
        if output["status"] != "STATUS_SUCCESS":
            print("Starting Latest Error In Flash Test Failed. Could not transfer flash. On index " +str(index))
            return False
        time.sleep(.150)
        error_amount = error_amount + 9
        print("Total errors: " + str(error_amount))



    output = mr_max.goto_safe() # Going to safe. We should send the sram errors to be saved into flash
    if output["status"] != "STATUS_SUCCESS":
        print("Basic Test Three Failed. Could not got to safe rug ro raggy")
        return False
    mr_max.goto_manual()
    parsed_errors = gather_and_parse_errors(flash_record_partition_one_address, len(unique_err_id_list)*9, error_parser)
    list_of_error_ids = []
    for error in unique_err_id_list:
        list_of_error_ids = list_of_error_ids + write_error_id_records(9, error)
    if not confirm_errors_via_error_id(parsed_errors, list_of_error_ids):
        print("Basic Test Three Failed. Reading from Flash did not get the same errors as written. Looks like transfer failed.")
        return False

    Power_Supply.reset_power_supply(1, 1)
    # After resetting the supply we are going to confirm the startup error and then grab the whole error record and confirm they are the same.

    mr_max.goto_manual()
    mr_max.set_utime()
    if not confirm_last_error_in_flash(unique_err_id_list[-1]):
        print("Basic Test Three Failed. We did not get the latest error from flash after powerup.")
        return False
    parsed_errors = get_and_parse_all_error_records("flash", error_parser)
    list_of_error_ids = fill_out_complete_flash_record_error_ids(list_of_error_ids)

    if not confirm_errors_via_error_id(parsed_errors, list_of_error_ids):
        print("Basic Test Three Failed. Reading the entire Flash record did not get the same error record as expected. Ethier of the flash values are wrong or there is was suppose to be blank and there was not.")
        return False

    print("Basic Test Three Passed")
    return True

def basic_test_four_overflow_error_condition():
    print("Starting Basic Test Four Overflow Conditions")
    error_parser = MaxwellErrorParser()
    mr_max.goto_manual()
    mr_max.clear_errors()
    if not check_if_flash_is_blank():
        return False

    num_of_errors = random.randint(29, len(write_error_dictonary) - 1)  # randomly select the number of errors we are sending to the error AO. Since
    unique_err_id_list, unique_err_id_list_offset = generate_unique_errors_from_write_error_dictonary(num_of_errors) # Create a unique error list based off of the number of errors we are creating.


    # We are writing for a total of 252  errors  This will prep for a overflow sram condtion which happends when more then 256 error are sent.
    # This for loop is here because we are sending the data 1 uinque error (9 error per unique error) at a time.
    # This is due to the fact that the current mcu has to keep a t 10 ms pulse to go through all the AO and writing to many errors will stop that.

    error_amount = 0
    list_of_error_ids = []
    for index, error in enumerate(unique_err_id_list[:28]):
        output = send_bulk_error(unique_err_id_list_offset[index])
        if not output:
            print("Basic Test 4 Overflow Condition Failed. Could not write a error. On index " + str(index))
            return False
        output = mr_max.send_maxwell_message(MaxCmd.TRN_SRAM_FLASH, [])
        if output["status"] != "STATUS_SUCCESS":
            print("Basic Test 4 Overflow Condition Failed. Could not transfer flash. On index " +str(index))
            return False
        list_of_error_ids = list_of_error_ids + write_error_id_records(9, error)
        time.sleep(.050)
        error_amount = error_amount + 9
        print("Total errors: " + str(error_amount))


    output = mr_max.goto_safe() # Going to safe. We should send the sram errors to be saved into flash
    if output["status"] != "STATUS_SUCCESS":
        print("Basic Test 4 Overflow Condition Failed.Could not got to safe rug ro raggy")
        return False
    mr_max.goto_manual()
    parsed_errors = gather_and_parse_errors(flash_record_partition_one_address, len(unique_err_id_list[:28])*9, error_parser)
    if not confirm_errors_via_error_id(parsed_errors, list_of_error_ids):
        print("Basic Test 4 Overflow Condition Failed. Reading from Flash did not get the same errors as written. Looks like transfer failed.")
        return False

    for index, error in enumerate(unique_err_id_list[28:], start=28):
        output = send_bulk_error(unique_err_id_list_offset[index])
        list_of_error_ids = list_of_error_ids + write_error_id_records(9, error)
        if not output:
            print("Basic Test 4 Overflow Condition Failed. Could not write a error. On index " + str(index))
            return False
        time.sleep(.050)
        error_amount = error_amount + 9
        print("Total errors: " + str(error_amount))


    output = mr_max.send_maxwell_message(MaxCmd.TRN_SRAM_FLASH, [])
    if output["status"] != "STATUS_SUCCESS":
        print("Basic Test 4 Overflow Condition Failed. Could not transfer flash.")
        return False

    sram_parsed_errors = get_and_parse_all_error_records("sram", error_parser)

    # Converting the total errors we sent to the what is currently held in sram.
    # From there we can compare this list with the current errors in sram.
    list_of_error_ids_sram = error_list_to_sram_record_representation(list_of_error_ids)
    # Lets confirm that the sram overflowed properly.
    if not confirm_errors_via_error_id(sram_parsed_errors, list_of_error_ids_sram):
        print("Basic Test Four Failed. Reading the entire Sram record did not get the same error record as expected. Ethier of the sram values are wrong or there is was suppose to be blank and there was not.")
        return False

    Power_Supply.reset_power_supply(1, 1)
    # After resetting the supply we are going to confirm the startup error and then grab the whole error record and confirm they are the same.

    mr_max.goto_manual()
    mr_max.set_utime()
    if not confirm_last_error_in_flash(unique_err_id_list[-1]):
        print("Basic Test Four Failed. We did not get the latest error from flash after powerup.")
        return False
    parsed_errors = get_and_parse_all_error_records("flash", error_parser)
    list_of_error_ids = fill_out_complete_flash_record_error_ids(list_of_error_ids)

    if not confirm_errors_via_error_id(parsed_errors, list_of_error_ids):
        print("Basic Test Four Failed. Reading the entire Flash record did not get the same error record as expected. Ethier of the flash values are wrong or there is was suppose to be blank and there was not.")
        return False
    print("Basic Test Four Passed")
    return True

def test_five_flash_overflow_condtion():
    print("Starting Test Five Flash Overflow Conditions")
    error_parser = MaxwellErrorParser()
    mr_max.goto_manual()
    mr_max.clear_errors()
    if not check_if_flash_is_blank():
        return False

    num_of_errors = random.randint(29, len(write_error_dictonary) - 1)  # randomly select the number of errors we are sending to the error AO. Since
    unique_err_id_list, unique_err_id_list_offset = generate_unique_errors_from_write_error_dictonary(31)  # Create 31 unique error ids.

    # We are writing for a total of 279  errors  This will prep for a Flash overflow condtion which happends first happens in flash address 1 when we write more then 512
    # This for loop is here because we are sending the data 1 uinque error (9 error per unique error) at a time.
    # This is due to the fact that the current mcu has to keep a t 10 ms pulse to go through all the AO and writing to many errors will stop that.
    status, list_of_error_ids, error_amount = write_and_send_bulk_errors_to_flash(unique_err_id_list=unique_err_id_list, unique_err_id_list_offset=unique_err_id_list_offset)
    if not status:
        return False
    # We are resetting the power supply to redo the filtering to send more errors.
    Power_Supply.reset_power_supply(1, 1)
    mr_max.goto_manual()
    mr_max.set_utime()

    status, list_of_error_ids, error_amount = write_and_send_bulk_errors_to_flash(unique_err_id_list=unique_err_id_list[:18], unique_err_id_list_offset=unique_err_id_list_offset, list_of_error_ids=list_of_error_ids, error_amount=error_amount)
    if not status:
        return False

    parsed_errors = get_and_parse_all_error_records("flash", error_parser)
    flash_list_of_error_ids = fill_out_complete_flash_record_error_ids(list_of_error_ids)

    if not confirm_errors_via_error_id(parsed_errors, flash_list_of_error_ids):
        print( "Test Five Flash Overflow Conditions Failed. Reading from Flash did not get the same errors as written. Looks like transfer failed.")
        return False

    # Now we have to trigger the overflow condition current we are at 441 errors. We need at least 71 errors. Which will be 8 bulk errors send.
    # We are sending all these errors at once which means that we might dev assert.
    # We are sending 8 bulk errors starting from 18 to 26 which will make it unqiue.
    for index, error in enumerate(unique_err_id_list[18:28],start=18):
        output = send_bulk_error(unique_err_id_list_offset[index])
        if not output:
            print("Test Five Flash Overflow Conditions. Could not write a error. On index " + str(index))
            return False
        list_of_error_ids = list_of_error_ids + write_error_id_records(9, error)
        time.sleep(.050)
        error_amount = error_amount + 9
        print("Total errors: " + str(error_amount))


    output = mr_max.goto_safe()  # Going to safe. We should send the sram errors to be saved into flash
    if output["status"] != "STATUS_SUCCESS":
        print("Basic Test 4 Overflow Condition Failed.Could not got to safe rug ro raggy")
        return False
    time.sleep(.150) # we are erasing so it will take some time.
    mr_max.goto_manual()


    list_of_error_ids =  error_list_to_flash_record_representation(list_of_error_ids) # converting the errors to make it look like what it would be in flash. From there we can do a direct comparison.
    parsed_errors = gather_and_parse_errors(flash_record_partition_one_address, len(list_of_error_ids),error_parser)
    if not confirm_errors_via_error_id(parsed_errors, list_of_error_ids):
        print( "Basic Test 4 Overflow Condition Failed. Reading from Flash did not get the same errors as written. Looks like transfer failed.")
        return False


    # for index, error in enumerate(unique_err_id_list[28:], start=28):
    #     output = send_bulk_error(unique_err_id_list_offset[index])
    #     list_of_error_ids = list_of_error_ids + write_error_id_records(9, error)
    #     if not output:
    #         print("Basic Test 4 Overflow Condition Failed. Could not write a error. On index " + str(index))
    #         return False
    #     time.sleep(.050)
    #     error_amount = error_amount + 9
    #     print("Total errors: " + str(error_amount))
    #
    # output = mr_max.send_maxwell_message(MaxCmd.TRN_SRAM_FLASH, [])
    # if output["status"] != "STATUS_SUCCESS":
    #     print("Basic Test 4 Overflow Condition Failed. Could not transfer flash.")
    #     return False
    #
    # sram_parsed_errors = get_and_parse_all_error_records("sram", error_parser)
    #
    # # Converting the total errors we sent to the what is currently held in sram.
    # # From there we can compare this list with the current errors in sram.
    # list_of_error_ids_sram = error_list_to_sram_record_representation(list_of_error_ids)
    # # Lets confirm that the sram overflowed properly.
    # if not confirm_errors_via_error_id(sram_parsed_errors, list_of_error_ids_sram):
    #     print(
    #         "Basic Test Four Failed. Reading the entire Sram record did not get the same error record as expected. Ethier of the sram values are wrong or there is was suppose to be blank and there was not.")
    #     return False
    #
    #
    # if not confirm_last_error_in_flash(unique_err_id_list[-1]):
    #     print("Basic Test Four Failed. We did not get the latest error from flash after powerup.")
    #     return False
    # parsed_errors = get_and_parse_all_error_records("flash", error_parser)
    # list_of_error_ids = fill_out_complete_flash_record_error_ids(list_of_error_ids)
    #
    # if not confirm_errors_via_error_id(parsed_errors, list_of_error_ids):
    #     print(
    #         "Basic Test Four Failed. Reading the entire Flash record did not get the same error record as expected. Ethier of the flash values are wrong or there is was suppose to be blank and there was not.")
    #     return False
    # print("Basic Test Four Passed")
    return True



def test_six_flash_75_percent_condtion():
    print("Starting Test Six Flash 75 Percent Check")
    error_parser = MaxwellErrorParser()
    mr_max.goto_manual()
    mr_max.clear_errors()
    if not check_if_flash_is_blank():
        return False


    unique_err_id_list, unique_err_id_list_offset = generate_unique_errors_from_write_error_dictonary(31)  # Create 31 unique error ids.

    # We are writing for a total of 279  errors  This will prep for a Flash overflow condtion which happends first happens in flash address 1 when we write more then 512
    # This for loop is here because we are sending the data 1 uinque error (9 error per unique error) at a time.
    # This is due to the fact that the current mcu has to keep a t 10 ms pulse to go through all the AO and writing to many errors will stop that.
    status, list_of_error_ids, error_amount = write_and_send_bulk_errors_to_flash(unique_err_id_list=unique_err_id_list, unique_err_id_list_offset=unique_err_id_list_offset)
    if not status:
        return False
    # We are resetting the power supply to redo the filtering to send more errors.
    Power_Supply.reset_power_supply(1, 1)
    mr_max.goto_manual()
    mr_max.set_utime()

    status, list_of_error_ids, error_amount = write_and_send_bulk_errors_to_flash(unique_err_id_list=unique_err_id_list[:18], unique_err_id_list_offset=unique_err_id_list_offset, list_of_error_ids=list_of_error_ids, error_amount=error_amount)
    if not status:
        return False

    output = mr_max.goto_safe()  # Going to safe. We should see the flash sent
    if output["status"] != "STATUS_SUCCESS":
        print("Basic Test Six Failed. Could not got to safe rug ro raggy")
        return False
    mr_max.goto_manual()
    list_of_error_ids = error_list_to_flash_record_representation(list_of_error_ids)  # converting the errors to make it look like what it would be in flash. From there we can do a direct comparison.
    parsed_errors = gather_and_parse_errors(flash_record_partition_one_address, len(list_of_error_ids), error_parser)
    if not confirm_errors_via_error_id(parsed_errors, list_of_error_ids):
        print("Basic Test 4 Overflow Condition Failed. Reading from Flash did not get the same errors as written. Looks like transfer failed.")
        return False
    #Randomly select the number of errors we are sending to the error AO.
    # We need to be in range so it triggers the 75% condtion and not the overflash condtion.
    # Since there is a 64 error difference and 7*9 =63 < 64 we can have up to 7 bulk errors. That is why the range is 19 - 25 because 25- 18 = 7
    num_of_errors = random.randint(19, 25)

    # Now we have to trigger the overflow condition current we are at 441 errors. We need at least 71 errors. Which will be 8 bulk errors send.
    print("Num of errors is: " + str(num_of_errors))
    for index, error in enumerate(unique_err_id_list[18:num_of_errors],start=18):
        output = send_bulk_error(unique_err_id_list_offset[index])
        if not output:
            print("Test Five Flash Overflow Conditions. Could not write a error. On index " + str(index))
            return False
        list_of_error_ids = list_of_error_ids + write_error_id_records(9, error)
        time.sleep(.050)
        error_amount = error_amount + 9
        print("Total errors: " + str(error_amount))

    output = mr_max.goto_safe()  # Going to safe. We should send the sram errors to be saved into flash
    if output["status"] != "STATUS_SUCCESS":
        print("Basic Test 4 Overflow Condition Failed.Could not got to safe rug ro raggy")
        return False
    time.sleep(.150) # we are erasing so it will take some time.
    mr_max.goto_manual()


    list_of_error_ids =  error_list_to_flash_record_representation(list_of_error_ids) # converting the errors to make it look like what it would be in flash. From there we can do a direct comparison.
    parsed_errors = gather_and_parse_errors(flash_record_partition_one_address, len(list_of_error_ids),error_parser)
    if not confirm_errors_via_error_id(parsed_errors, list_of_error_ids):
        print( "Basic Test 4 Overflow Condition Failed. Reading from Flash did not get the same errors as written. Looks like transfer failed.")
        return False


    Power_Supply.reset_power_supply(1, 1)
    mr_max.goto_manual()
    mr_max.set_utime()

    # Now that we triggered the second sector of 75% check. We are going to verify the first sector functionality.
    # We need want to send errors right up to the 75% error line and then send one error over. From there we can verify if the 75% sector worked for the second flash.
    # the magical number for the 1 sector is 192 as the crossing for the first sector.
    # However since we already went through a whole page of flash it is actually 512 + 192 = 704.
    # Since we do not want to trigger first we are going to send 703 errors and then send one error and verify it worked.

    #First part determine how many errors we need to send to hit 703
    next_error_amount_target = 703 # we want 703 to hit right above but not into the sector 1 75% condition with a full set of flash already.

    total_errors_to_send = next_error_amount_target - error_amount
    bulk_errors_needed_to_send = int(total_errors_to_send/9) # this will be the amount of bulk errors
    remainder_errors_needed_to_send = total_errors_to_send%9 # This is the left over errors we will send after.

    status, list_of_error_ids, error_amount = write_and_send_bulk_errors_to_flash(unique_err_id_list=unique_err_id_list[:bulk_errors_needed_to_send], unique_err_id_list_offset=unique_err_id_list_offset, list_of_error_ids=list_of_error_ids, error_amount=error_amount)
    if not status:
        return False

    for x in range(remainder_errors_needed_to_send):
        status = send_error(unique_err_id_list_offset[bulk_errors_needed_to_send+1])
        if not status:
            print("Test 6 Failed could not send error.")
            return False
        list_of_error_ids = list_of_error_ids + write_error_id_records(1, unique_err_id_list[bulk_errors_needed_to_send+1])
        error_amount = error_amount + 1

    output = mr_max.goto_safe()  # Going to safe. We should see the flash sent
    if output["status"] != "STATUS_SUCCESS":
        print("Basic Test Six Failed. Could not got to safe rug ro raggy")
        return False
    mr_max.goto_manual()

    # Now 703 errors have been sent. Lets Confirm that everything is as expected.

    parsed_errors = get_and_parse_all_error_records("flash", error_parser)
    flash_list_of_error_ids = error_list_to_flash_record_representation(list_of_error_ids)

    if not confirm_errors_via_error_id(parsed_errors, flash_list_of_error_ids):
        print( "Test Five Flash Overflow Conditions Failed. Reading from Flash did not get the same errors as written. Looks like transfer failed.")
        return False

    # Everything has been sent and verified up to 703 lets do 1 more error and trigger the 1st sector 75 condition.

    status = send_error(unique_err_id_list_offset[bulk_errors_needed_to_send+1]) # We can send another one since we know it was not fully sent.
    if not status:
        print("Test 6 Failed could not send error.")
        return False
    list_of_error_ids = list_of_error_ids + write_error_id_records(1, unique_err_id_list[bulk_errors_needed_to_send+1])  # We can send another one since we know it was not fully sent.
    error_amount = error_amount + 1

    output = mr_max.goto_safe()  # Going to safe. We should send the sram errors to be saved into flash
    if output["status"] != "STATUS_SUCCESS":
        print("Basic Test 4 Overflow Condition Failed.Could not got to safe rug ro raggy")
        return False
    time.sleep(.150)  # we are erasing so it will take some time.
    mr_max.goto_manual()


    # now we are good to check if it all worked!
    parsed_errors = get_and_parse_all_error_records("flash", error_parser)
    flash_list_of_error_ids = error_list_to_flash_record_representation(list_of_error_ids)  # converting the errors to make it look like what it would be in flash. From there we can do a direct comparison.
    if not confirm_errors_via_error_id(parsed_errors, flash_list_of_error_ids):
        print( "Basic Test 6 Failed. Reading from Flash did not get the same errors as written. Looks like transfer failed.")
        return False


    # The final test is to send 1 error at a time and send to flash over and over until we hit the 2nd 75% sector. This will simulate a more releastic scnerio.
    # To get over the 75% sector we have to do some math the second error target is 960 = (512 +(512-64(The 75_percent_offset))). To hit that target we are going to do some math with error amount.
    # Since we want to go over the sector

    # First we are going to reset to get rid of error filtering.
    Power_Supply.reset_power_supply(1, 1)
    mr_max.goto_manual()
    mr_max.set_utime()

    #First part determine how many errors we need to send to hit 960
    next_error_amount_target = 960

    total_errors_to_send = next_error_amount_target - error_amount
    bulk_errors_needed_to_send = int(total_errors_to_send/9) # this will be the amount of bulk errors
    remainder_errors_needed_to_send = total_errors_to_send%9 # This is the left over errors we will send after.


    # Now we are going to send errors one at a time and then reset.
    status, list_of_error_ids, error_amount = write_and_send_bulk_errors_to_flash_one_by_one(unique_err_id_list=unique_err_id_list[:bulk_errors_needed_to_send], unique_err_id_list_offset=unique_err_id_list_offset, list_of_error_ids=list_of_error_ids, error_amount=error_amount)
    if not status:
        return False

    for x in range(remainder_errors_needed_to_send):
        output = write_and_send_error_to_flash(unique_err_id_list_offset[bulk_errors_needed_to_send+1])
        if not output:
            print("Error")
            return False
        list_of_error_ids = list_of_error_ids + write_error_id_records(1, unique_err_id_list[bulk_errors_needed_to_send + 1])
        error_amount = error_amount + 1

    output = mr_max.goto_safe()  # Going to safe. We should see the flash sent
    if output["status"] != "STATUS_SUCCESS":
        print("Basic Test Six Failed. Could not got to safe rug ro raggy")
        return False
    mr_max.goto_manual()
    parsed_errors = get_and_parse_all_error_records("flash", error_parser)
    flash_list_of_error_ids = error_list_to_flash_record_representation(list_of_error_ids)  # converting the errors to make it look like what it would be in flash. From there we can do a direct comparison.
    if not confirm_errors_via_error_id(parsed_errors, flash_list_of_error_ids):
        print( "Basic Test 6 Failed. Reading from Flash did not get the same errors as written. Looks like transfer failed.")
        return False


    # OK so we did the final flash lets just roll over and make sure everything else works well.

    # First we are going to reset to get rid of error filtering.
    Power_Supply.reset_power_supply(1, 1)
    mr_max.goto_manual()
    mr_max.set_utime()

    #We are sending 8 bulk errors or 72 errors this should be enough to go over via flash
    bulk_errors_needed_to_send= 8
    status, list_of_error_ids, error_amount = write_and_send_bulk_errors_to_flash_one_by_one(unique_err_id_list=unique_err_id_list[:bulk_errors_needed_to_send], unique_err_id_list_offset=unique_err_id_list_offset, list_of_error_ids=list_of_error_ids, error_amount=error_amount)
    if not status:
        return False

    output = mr_max.goto_safe()  # Going to safe. We should see the flash sent
    if output["status"] != "STATUS_SUCCESS":
        print("Basic Test Six Failed. Could not got to safe rug ro raggy")
        return False
    parsed_errors = get_and_parse_all_error_records("flash", error_parser)
    flash_list_of_error_ids = error_list_to_flash_record_representation(
        list_of_error_ids)  # converting the errors to make it look like what it would be in flash. From there we can do a direct comparison.
    if not confirm_errors_via_error_id(parsed_errors, flash_list_of_error_ids):
        print(
            "Basic Test 6 Failed. Reading from Flash did not get the same errors as written. Looks like transfer failed.")
        return False

    print("Test Passed!")
    return True


def confirm_errors_via_error_id(list_of_errors, list_of_error_ids):
    max_err = MaxwellErrorParser()
    # if only one error is being checked a user might send a int type instead of a list. Lets convert it to a list.
    if isinstance(list_of_errors, int) or isinstance(list_of_errors, dict):
        error = list_of_errors
        list_of_errors = [error]
    # if only one error id is being checked a user might send a int type instead of a list. Lets convert it to a list.
    if isinstance(list_of_error_ids, int):
        error = list_of_error_ids
        list_of_error_ids = [error]
    if len(list_of_errors) != len(list_of_error_ids):
        print("There is a difference in length of the errors you want to compare with error id's it is comparing to. ")
        return False
    for x in range(len(list_of_errors)):
        if list_of_error_ids[x] == 0xFFFF:
            blank = max_err.check_if_parsed_error_record_is_blank(list_of_errors[x], "flash")
            if not blank:
                print("The " + str(x) + " error was suppose to be a blank error in flash (0xFFFF). It was not the error value was " + str( list_of_errors[x]))
                return False
        elif list_of_error_ids[x] == 0x0000:
            blank = max_err.check_if_parsed_error_record_is_blank(list_of_errors[x], "sram")
            if not blank:
                print("The " + str(x) + " error was suppose to be a blank error in sram (0x0000). It was not the error value was " + str(list_of_errors[x]))
                return False
        elif list_of_errors[x]["Error ID"] != list_of_error_ids[x]:
            print("Error Mismatch with ID")
            return False
    return True

def gather_all_sram_record_errors():
    payload = gather_errors(sram_start, 256)
    return payload


def gather_all_flash_record_errors():
    payload = gather_errors(flash_record_partition_one_address, 512)
    return payload


def get_and_parse_all_error_records(location_type, error_parser=None):
    parsed_errors = []
    raw_errors = []
    if error_parser is None:
        error_parser = MaxwellErrorParser()
    if location_type == "sram":
        raw_errors = gather_all_sram_record_errors()
        parsed_errors = error_parser.parse_bulk_sram_error_records(raw_errors)
    elif location_type == "flash":
        raw_errors = gather_all_flash_record_errors()
        parsed_errors = error_parser.parse_bulk_sram_error_records(raw_errors, from_flash=True)

    return parsed_errors


def gather_and_parse_all_flash_record_errors():
    payload = gather_all_flash_record_errors()

    return payload


def send_bulk_error(error_id):
    error_id_list = []
    for x in range(9):
        error_id_list.append(error_id)
    return send_errors_via_error_id_list(error_id_list)

def send_errors_via_error_id_list(error_id_list):
    final_status = True
    for error_id in error_id_list:
        output = mr_max.send_maxwell_message(WRT_ERR_ERRAO, [0x00, error_id, 0xFF])
        if output["status"] != "STATUS_SUCCESS":
            final_status = False
    return final_status


def send_error(error_id):
    output = mr_max.send_maxwell_message(WRT_ERR_ERRAO, [0x00, error_id, 0xFF])
    if output["status"] != "STATUS_SUCCESS":
        return False
    return True

def write_and_send_bulk_errors_to_flash(unique_err_id_list, unique_err_id_list_offset, enum_start=0, list_of_error_ids = None, error_amount=None ):
    if list_of_error_ids == None:
        list_of_error_ids = []
    if error_amount == None:
        error_amount = 0
    for index, error in enumerate(unique_err_id_list,start=enum_start):
        output = send_bulk_error(unique_err_id_list_offset[index])
        if not output:
            print("Test Five Flash Overflow Conditions. Could not write a error. On index " + str(index))
            return False, list_of_error_ids, error_amount
        output = mr_max.send_maxwell_message(MaxCmd.TRN_SRAM_FLASH, [])
        if output["status"] != "STATUS_SUCCESS":
            print("Test Five Flash Overflow Conditions Failed. Could not transfer flash. On index " + str(index))
            return False, list_of_error_ids, error_amount
        list_of_error_ids = list_of_error_ids + write_error_id_records(9, error)
        time.sleep(.050)
        error_amount = error_amount + 9
        print("Total errors: " + str(error_amount))
    return True, list_of_error_ids, error_amount

def write_and_send_bulk_errors_to_flash_one_by_one(unique_err_id_list, unique_err_id_list_offset, enum_start=0, list_of_error_ids = None, error_amount=None ):
    if list_of_error_ids == None:
        list_of_error_ids = []
    if error_amount == None:
        error_amount = 0
    for index, error in enumerate(unique_err_id_list, start=enum_start):

        for x in range(9):
            output = write_and_send_error_to_flash(unique_err_id_list_offset[index])
            if not output:
                print("Test Five Flash Overflow Conditions. Could not write a error. On index " + str(index))
                return False, list_of_error_ids, error_amount
            list_of_error_ids = list_of_error_ids + write_error_id_records(1, error)
            error_amount = error_amount + 1
        print("Total errors: " + str(error_amount))
    return True, list_of_error_ids, error_amount


def write_and_send_error_to_flash(error_to_write):
    output = send_error(error_to_write)
    if not output:
        return False
    output = mr_max.send_maxwell_message(MaxCmd.TRN_SRAM_FLASH, [])
    if output["status"] != "STATUS_SUCCESS":
        return False
    time.sleep(0.150)
    return True


def gather_and_parse_errors(error_address, total_number_of_errors, error_parser=None):
    if error_parser is None:
        error_parser = MaxwellErrorParser()
    sram_address_line = 0x1FFE0000
    if error_address > sram_address_line:
        from_flash = False
    else:
        from_flash = True
    raw_errors = gather_errors(error_address, total_number_of_errors)
    parsed_errors = error_parser.parse_bulk_sram_error_records(raw_errors,from_flash)
    return parsed_errors


def gather_errors(error_address, total_number_of_errors):
    number_of_full_mcu_requests = total_number_of_errors // 15  # integer division
    final_amount_of_errors_to_request = total_number_of_errors % 15
    final_payload = []
    memory_address = error_address
    for x in range(number_of_full_mcu_requests):
        payload = mr_max.dump_mcu(memory_address, 15 * sram_record_length)
        memory_address = memory_address + sram_record_length * 15
        final_payload.extend(payload["raw_payload"][2:])

    if final_amount_of_errors_to_request != 0:
        payload = mr_max.dump_mcu(memory_address, final_amount_of_errors_to_request * sram_record_length)
        final_payload.extend(payload["raw_payload"][2:])
    return final_payload


def does_telm_have_errors(telm_dict):
    error_clear = False
    arr_of_tlm_errors = ["First Error", "Second Error", "Third Error", "Most Recent Error 1", "Most Recent Error 2",
                         "Most Recent Error 3"]
    for x in arr_of_tlm_errors:
        if (telm_dict[x]["Error ID"] == 0) or (telm_dict[x]["Aux Data"] == 0):
            error_clear = True
    if telm_dict["Most Frequent Error ID"] == 0:
        error_clear = True
    if telm_dict["Number of Errors from Most Freq. Error ID"] == 0:
        error_clear = True
    if telm_dict["Total Error Count"] == 0:
        error_clear = True
    if telm_dict["Spacecraft Error 1"] == 0:
        error_clear = True
    if telm_dict["Spacecraft Error 2"] == 0:
        error_clear = True
    if telm_dict["Spacecraft Error 3"] == 0:
        error_clear = True

    if error_clear == True:
        print("TELM HAS SOME ERRORS CLEAR")
        return False
    else:
        return True


def confirm_last_error_in_flash(error_id):
    tlm_output = mr_max.get_tlm()
    if DEFAULT_HARDWARE =="Prop Controller":
        if tlm_output["parsed_payload"]["ec_powerup_err_code"] == error_id:
            return True
    else:
        if tlm_output["parsed_payload"]["tc_powerup_err_code"] == error_id:
            return True

    return False

def check_if_flash_is_blank():
    list_of_error_ids = []
    list_of_error_ids.extend(write_blank_error_id_record(512, "flash"))
    parsed_errors = get_and_parse_all_error_records("flash")
    if not confirm_errors_via_error_id(parsed_errors, list_of_error_ids):
        print("Flash is not Blank")
        return False
    return True

def is_telm_packet_blank(telm_dict):
    error_clear = True
    arr_of_tlm_errors = ["First Error", "Second Error", "Third Error", "Most Recent Error 1", "Most Recent Error 2",
                         "Most Recent Error 3"]
    for x in arr_of_tlm_errors:
        if (telm_dict[x]["Error ID"] != 0) or (telm_dict[x]["Aux Data"] != 0):
            error_clear = False
    if telm_dict["Most Frequent Error ID"] != 0:
        error_clear = False
    if telm_dict["Number of Errors from Most Freq. Error ID"] != 0:
        error_clear = False
    if telm_dict["Total Error Count"] != 0:
        error_clear = False
    if telm_dict["Spacecraft Error 1"] != 0:
        error_clear = False
    if telm_dict["Spacecraft Error 2"] != 0:
        error_clear = False
    if telm_dict["Spacecraft Error 3"] != 0:
        error_clear = False

    if error_clear == False:
        print("Some of the errors where not cleared")
        return False
    else:
        return True


def write_blank_error_id_record(number_of_blank_errors, mem_type):
    list_of_blank_errors = []
    if mem_type == "sram":
        list_of_blank_errors = write_error_id_records(number_of_blank_errors, 0x0000)
    elif mem_type == "flash":
        list_of_blank_errors = write_error_id_records(number_of_blank_errors, 0xFFFF)
    else:
        print("Type is invalid")
    return list_of_blank_errors

def write_error_id_records(number_of_blank_errors, error_id_to_write):
    list_of_blank_errors = []
    for x in range(number_of_blank_errors):
        list_of_blank_errors.append(error_id_to_write)
    return list_of_blank_errors

def fill_out_complete_flash_record_error_ids(list_of_error_ids):
    flash_record_list = list_of_error_ids.copy()
    total_flash_record_amount = 512
    if len(flash_record_list) > total_flash_record_amount:
        print("You have to many errors in your error id to fill anything")
    num_of_errors_to_fill_with_blank = total_flash_record_amount - len(flash_record_list)
    flash_record_list.extend(write_blank_error_id_record(num_of_errors_to_fill_with_blank, "flash"))
    return flash_record_list

# The goal of this function is to format a huge error_list that is used as record of what we sent to the mcu
# and returns the current error representation that should be in the error record of sram.
def error_list_to_sram_record_representation(error_list):
    sram_list = error_list.copy()
    number_of_errors_in_sram_record =256
    if len(sram_list) <= number_of_errors_in_sram_record:
        number_of_errors_to_add_zero = number_of_errors_in_sram_record - len(sram_list)
        sram_list.append(write_blank_error_id_record(number_of_errors_to_add_zero, "sram"))
        return sram_list
    number_of_errors_to_rotate = len(sram_list) - number_of_errors_in_sram_record

    for x in range(number_of_errors_to_rotate):
        sram_list[x] = sram_list[x+number_of_errors_in_sram_record]
    del sram_list[number_of_errors_in_sram_record:]
    return sram_list

# The goal of this function is to format a huge error_list that is used as record of what we sent to the mcu
# and returns the current error representation that should be in the error record of flash.
def error_list_to_flash_record_representation(flash_list):
    Flash_Sector_One_75_Percent = 192
    Flash_Sector_Two_75_Percent = 448
    Flash_Sector_One_Border = 256
    Flash_Sector_Two_Border = 512
    Flash_Sector_Size=256

    number_of_errors_in_flash_record = 512
    if len(flash_list) <= number_of_errors_in_flash_record:
        number_of_errors_to_add_zero = number_of_errors_in_flash_record - len(flash_list)
        flash_list.extend(write_blank_error_id_record(number_of_errors_to_add_zero, "flash"))
        next_error_address = find_the_next_error_address_flash(flash_list)
        #  Check if the 75 Percent marker has been hit.
        # Note this looks redundent because we are doing the same thing below but remember this case is only hit if we are under 512 bytes. We will not hit the case below.
        # So this only occurs if we are below max and we finally hit the first sector.
        if next_error_address == 0 or next_error_address >= Flash_Sector_Two_75_Percent:
            for x in range(Flash_Sector_Size):
                flash_list[x] = 0xFFFF
        return flash_list

    number_of_errors_to_rotate = len(flash_list) - number_of_errors_in_flash_record

    next_error_address = find_the_next_error_address_flash(flash_list)

    for x in range(number_of_errors_to_rotate):
        # We are erasing before we write.
        if next_error_address >= Flash_Sector_Two_Border or next_error_address == 0:
            # Setting the first sector back to 0xFFFF
            for i in range(Flash_Sector_Size):
                flash_list[i] = 0xFFFF
            next_error_address = 0

        elif next_error_address == Flash_Sector_One_Border:
             for i in range(Flash_Sector_Size):
                flash_list[i+Flash_Sector_One_Border] = 0xFFFF


        flash_list[next_error_address] = flash_list[x + number_of_errors_in_flash_record]
        next_error_address = next_error_address + 1

    if next_error_address == 0 or next_error_address >= Flash_Sector_Two_75_Percent:
        for x in range(Flash_Sector_Size):
            flash_list[x] = 0xFFFF

    elif next_error_address >= Flash_Sector_One_75_Percent and next_error_address <=Flash_Sector_One_Border:
        for x in range(Flash_Sector_Size):
            flash_list[x+Flash_Sector_One_Border] = 0xFFFF

    del flash_list[number_of_errors_in_flash_record:]
    return flash_list


def find_the_next_error_address_flash(error_list):
    flash_record = 512
    flash_sector_one_offset = 0
    flash_sector_two_offset = 256

    # This is used to determine where the "start of the check" should be. There is a case that the flash for one sector get erased prior to the other one filled up.
    # So straight down approach will not work. (IE, second sector back to the first one.)
    if len(error_list) >=flash_record:
        if error_list[flash_sector_one_offset] == 0xFFFF and error_list[flash_sector_two_offset] != 0xFFFF:
            # Sector one start is empty but sector 2 is not.
            # That means sector two has data and could potentially have a empty flash which would be first.
            next_error_address = flash_sector_two_offset
            for error in error_list[flash_sector_two_offset:]:
                if error == 0xFFFF:
                    break
                next_error_address = next_error_address + 1
            if next_error_address != len(error_list):
                return next_error_address

    #Looks like the condtion above does not follow so lets search top to bottom.

    next_error_address = 0
    for error in error_list:
        if error == 0xFFFF:
            break
        next_error_address = next_error_address + 1
    # if they are the same that means we did not find anything. Letsxs p set it to at the top.
    if next_error_address == len(error_list):
        next_error_address = 0
    return next_error_address



# This is the main section of the test. It will run through all of these test if you want to specify one the functions call will be below.
# primitive_test()
# basic_test_one()
# pick_latest_error_in_flash_test()
# basic_test_two()
# basic_test_three_underflow_error_condition()
# basic_test_four_overflow_error_condition()
# test_six_flash_75_percent_condtion()
def main():
    Power_Supply.reset_power_supply(1, 1)
    mr_max.set_auto_tlm_enable(500)
    print("Running Transfer Flash NVM Test Suite")
    if DEFAULT_HARDWARE == "Prop Controller":
        if not mr_max.is_prop_controller_connected():
            print("The Prop Controller is not connected. Please connect the Thruster Controller to run this test.")
            return
    else:
        if not mr_max.is_thruster_controller_connected():
            print("The Thruster Controller is not connected. Please connect the Thruster Controller to run this test.")
            return
    list_of_tests_to_run = [primitive_test, basic_test_one,pick_latest_error_in_flash_test,basic_test_two,basic_test_three_underflow_error_condition,basic_test_four_overflow_error_condition,test_six_flash_75_percent_condtion]
    for test in list_of_tests_to_run:
        status = test()
        if not status:
            print("Test Suite Failed. The test that failed was: "+ str(test.__name__))
            return False


    print("Test complete")
    return True

if __name__ == "__main__":
    main()
