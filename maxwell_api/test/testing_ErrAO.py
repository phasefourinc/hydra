import time
from maxapi.mr_maxwell import MrMaxwell, MaxConst, MaxCmd
from maxapi.max_cmd_id import MaxCmd
from maxapi.maxwell_information_processor import process_maxwell_record
from chozo.bk_precision_9201.maxwell_bk_precision_9201 import MaxwellBkPrecision9201

DEFAULT_BAUD_RATE = "1000000"
DEFAULT_COM_PORT = "COM4"
DEFAULT_DATASOURCE = "sandbox"
HOST = "nekone.phasefour.co"
DEFAULT_MEASUREMENT = "andrew_pcu_dev"
DEFAULT_HARDWARE = "Thruster Controller"  # Options are "Thruster Controller" and "Prop Controller"
# These commands send no data
SAFE = 0x000F
ABORT = 0x000F
LOW_PWR = 0x0001
IDLE = 0x0002
THRUST = 0x0004
MANUAL = 0x0007
SW_UPDATE = 0x0008
WILD_WEST = 0x000D
CLR_ERRORS = 0x0011
GET_TLM = 0x0020

# These commands send 1-byte boolean data
SET_WDI_EN = 0x0100
SET_RFT_RST_EN = 0x0101
SET_HEATER_EN = 0x0102
SET_HEATER_RST_EN = 0x0103
SET_HPISO_EN = 0x0104
SET_LPISO_EN = 0x0105
SET_PFCV_DAC_RST_EN = 0x0106
SET_DEBUG_LEDS = 0x0107  # data bits 3:0 set DEBUG_LED3:0 respectively

# These commands send 2-byte u16 data
SET_HEATER_PWM_RAW = 0x0108
SET_HPISO_PWM_RAW = 0x0109
SET_LPISO_PWM_RAW = 0x010A
SET_PFCV_DAC_RAW = 0x010B

mr_max = MrMaxwell(DEFAULT_COM_PORT, DEFAULT_BAUD_RATE, DEFAULT_DATASOURCE, DEFAULT_MEASUREMENT)


def main():
    mr_max.update_default_interfacting_hardware("Prop Controller")
    mr_max.set_auto_tlm_enable(1000)
    mr_max.goto_manual()
    mr_max.clear_errors()
    time.sleep(.500)
    for x in range(1, 31):
        send_bulk_error(x)
    mr_max.send_maxwell_message(MaxCmd.TRN_SRAM_FLASH, [])
    send_bulk_error(31)
    mr_max.send_maxwell_message(MaxCmd.TRN_SRAM_FLASH, [])
    input("Power Off Now")
    checking_last_error_startup()


def create_nack_errors():
    mr_max.goto_manual()
    mr_max.halt_on_nack = False
    input("Write a wrong id nack")
    create_wrong_msg_id_nack()
    input("Write a wrong state nack")
    create_wrong_state_nack()
    input("Write a wrong data size argument nack")
    create_wrong_data_argument_nack()
    input("No known function nack")
    create_no_function_nack()
    input("Write a application nack")
    create_application_nack()
    input("Finish?")
    print("Done")


def create_wrong_msg_id_nack():
    mr_max.send_maxwell_message(0x9999, [])


def create_wrong_state_nack():
    mr_max.sw_update_program(0x80000, 15, 0x3A56)


def create_wrong_data_argument_nack():
    mr_max.send_maxwell_message(MaxCmd.CLR_ERRORS, [52, 57, 48])


def create_no_function_nack():
    mr_max.send_maxwell_message(0x4030, [])


def create_application_nack():
    byte_arr = process_maxwell_record("adasdas", "asdasdsad ", "asda", "212sas", 0)
    mr_max.send_maxwell_message(MaxCmd.SET_MAX_HRD_INFO, list(byte_arr))


def checking_last_error_startup():
    power_supply = MaxwellBkPrecision9201()
    print("Testing last error at startup.")
    basic_last_error_test(power_supply)


def basic_last_error_test(power_supply):
    restart_maxwell(power_supply, "Prop Controller")
    mr_max.goto_manual()
    mr_max.clear_errors()
    time.sleep(.500)
    for x in range(1, 31):
        output = mr_max.send_maxwell_message(MaxCmd.WRT_ERR, [0x00, x, 0xFF])
    mr_max.send_maxwell_message(MaxCmd.TRN_SRAM_FLASH, [])
    restart_maxwell(power_supply, "Prop Controller")
    mr_max.goto_manual()
    time.sleep(.500)
    output = mr_max.get_tlm()
    result = _check_last_error_at_startup(output, 0x081C)
    if result:
        print("Basic Test Passed")
    else:
        print("Basic Test Failed")
    return result


def _check_last_error_at_startup(output, error_number):
    if output["parsed_payload"]["ERR_SINCE_POWERUP"] == error_number:
        return True
    return False

def flash_sector_two_startup_test(power_supply):
    restart_maxwell(power_supply, "Prop Controller")
    mr_max.goto_manual()
    mr_max.clear_errors()
    for x in range(1, 31):
        send_bulk_error(x)
    mr_max.send_maxwell_message(MaxCmd.TRN_SRAM_FLASH, [])
    input("Power Off Now..")
    restart_maxwell(power_supply, "Prop Controller")
    mr_max.goto_manual()
    time.sleep(.500)
    output = mr_max.get_tlm()
    result = _check_last_error_at_startup(output, 0x081C)
    if result:
        print("Basic Test Passed")
    else:
        print("Basic Test Failed")
    return result
def send_bulk_error(error_id):
    for x in range(9):
        mr_max.send_maxwell_message(MaxCmd.WRT_ERR, [0x00, error_id, 0xFF])


def restart_maxwell(power_supply, defualt_hardware):
    global mr_max
    power_supply.turn_off_power_supply()
    time.sleep(.5)
    power_supply.turn_on_power_supply()
    time.sleep(.5)
    mr_max.__del__()
    mr_max = MrMaxwell(DEFAULT_COM_PORT, DEFAULT_BAUD_RATE, DEFAULT_DATASOURCE, DEFAULT_MEASUREMENT)
    mr_max.update_default_interfacting_hardware(defualt_hardware)
    mr_max.set_auto_tlm_enable(1000)


def interactive_write_errors():
    mr_max.update_default_interfacting_hardware(DEFAULT_HARDWARE)
    mr_max.set_auto_tlm_enable(1000)
    print("Writing Errors to Thruster Controller")
    spacecraft_visible = 0x00  # If spacecraft_visable = 0xff that is true if it is 0x00 it is false

    input_num = input("Write first error?")
    mr_max.send_maxwell_message(MaxCmd.WRT_ERR, [0x00, int(input_num), spacecraft_visible])
    time.sleep(.25)
    input_num = input("Write second error?")
    mr_max.send_maxwell_message(MaxCmd.WRT_ERR, [0x00, int(input_num), spacecraft_visible])
    time.sleep(.25)
    input_num = input("Write third error?")
    mr_max.send_maxwell_message(MaxCmd.WRT_ERR, [0x00, int(input_num), spacecraft_visible])
    time.sleep(.25)
    input_num = input("Write fourth error?")
    mr_max.send_maxwell_message(MaxCmd.WRT_ERR, [0x00, int(input_num), spacecraft_visible])
    time.sleep(.25)
    input_num = input("Write fifth error?")
    mr_max.send_maxwell_message(MaxCmd.WRT_ERR, [0x00, int(input_num), spacecraft_visible])
    time.sleep(.25)
    input_num = input("Write sixith error?")
    mr_max.send_maxwell_message(MaxCmd.WRT_ERR, [0x00, int(input_num), spacecraft_visible])
    time.sleep(.25)
    spacecraft_visible = 0xFF
    input_num = input("Write seventh error?")
    mr_max.send_maxwell_message(MaxCmd.WRT_ERR, [0x00, int(input_num), spacecraft_visible])
    time.sleep(.25)
    input_num = input("Write eight error?")
    mr_max.send_maxwell_message(MaxCmd.WRT_ERR, [0x00, int(input_num), spacecraft_visible])
    input_num = input("Write Ninth error?")
    mr_max.send_maxwell_message(MaxCmd.WRT_ERR, [0x00, int(input_num), spacecraft_visible])
    time.sleep(.25)
    input_num = input("Write Tenth error?")
    mr_max.send_maxwell_message(MaxCmd.WRT_ERR, [0x00, int(input_num), spacecraft_visible])
    time.sleep(.25)
    spacecraft_visible = 0x00
    input_num = input("Write Final error?")
    mr_max.send_maxwell_message(MaxCmd.WRT_ERR, [0x00, int(input_num), spacecraft_visible])
    input("Finish?")
    print("Done")


if __name__ == "__main__":
    main()
