import datetime
import time
import statistics
import json
import arrow
import datetime
import sys
from maxapi.mr_maxwell_b2 import MrMaxwell
import maxwell_telm_checker
DEFAULT_BAUD_RATE =  "1000000"
DEFAULT_COM_PORT = "COM4"
DEFAULT_DATASOURCE= "sandbox"
HOST="nekone.phasefour.co"
DEFAULT_MEASUREMENT = "andrew_pcu_dev"
from reactor.influx.p4_influx_dataset import P4InfluxDataset


# dset = P4InfluxDataset("https://nekone.phasefour.co/d/Wb5ccE_7z/super-dev-dash-block-2?orgId=1&from=1657312275577&to=1657312351737&var-dev_measurement=andrew_pcu_dev")
# #dset.init_from_dict(params_dict = mr_max.max_api.telm_influx_database.main_client.get_dataset())
# dump_cnt = dset.query_points("count(ec_data_mode)", "ec_data_mode = 3")[0]["count"]
# recording_data_set = dset.query_to_new_dataset("ec_data_mode", "ec_data_mode = 3")
# recorded_data_points = recording_data_set.get_point_count()
# print(dump_cnt)
# print(recorded_data_points)
# print(recording_data_set.get_point_count(use_estimate=True))
# print(recording_data_set.get_point_count(group_by_ms=0))
#
# sys.exit()

dump_amount = 30
mr_max = MrMaxwell(DEFAULT_COM_PORT,DEFAULT_BAUD_RATE,DEFAULT_DATASOURCE,DEFAULT_MEASUREMENT)



def write_tlm_dump(time_to_record):
    mr_max.clear_errors()
    time.sleep(1.0)
    mr_max.goto_safe()
    time.sleep(1.0)
    mr_max.goto_manual()
    time.sleep(1.0)
    mr_max.set_recording_nand(True)
    mr_max.set_record_tlm_enable()
    print("Waiting for " + str(time_to_record) + " seconds")
    time.sleep(time_to_record)
    mr_max.set_record_tlm_disable()

def main():
    # print_flash_txt_output()
    #
    # return
    global dump_amount
    time_amount = gather_dump_time_input()
    if time_amount == -1:
        return
    dump_amount = time_amount


    print("\nStarting Dump Test")
    mr_max.set_auto_tlm_enable(250)
    #mr_max.set_auto_tlm_disable()
    test_matrix = [{"Dump Time":30, "Result": None},
                   {"Dump Time":60, "Result": None},
                   {"Dump Time":90, "Result": None},
                   {"Dump Time":120, "Result": None},
                   {"Dump Time":240, "Result": None},
                   {"Dump Time":480, "Result": None},
                   {"Dump Time":720, "Result": None},
                   {"Dump Time":960, "Result": None},
                   {"Dump Time":1920, "Result": None},
                   {"Dump Time":30, "Result": None}
                   ]
    for test in test_matrix:
        dump_amount = test["Dump Time"]
        time.sleep(2)
        print("Recording for " + str(test["Dump Time"]) + " seconds")

        print("Writing Dump Command")
        start_dump_time = arrow.utcnow()
        write_tlm_dump(dump_amount)

        print("Checking if any errors since recording")
        #tlm_dict = mr_max.get_tlm()
        # if not maxwell_telm_checker.confirm_command_tlm(tlm_dict):
        #     print("We received errors when recording high rate telm. Here is the tlm dump")
        #     print(tlm_dict)
        #     return

        #tlm_dict = mr_max.get_tlm()
        # if not maxwell_telm_checker.confirm_command_tlm(tlm_dict):
        #     print("We received errors when receiving FRAM data. Here is the tlm dump")
        #     print(tlm_dict)
        #     return
        time.sleep(3.0)
        print("Dumping FLASH")
        tlm_dict = get_tlm_until_status_success()
        tlm_count = tlm_dict["parsed_payload"]["ec_rec_nand_tlm_cnt"]
        print("Total NAND Flash Packet Are: " + str(tlm_count))
        time.sleep(2.0)
        tlm_dict = mr_max.dump_tlm_nand_full()
        wait_until_dumping_is_done(output_file="flash_dump")
        end_dump_time = arrow.utcnow()
        dset = P4InfluxDataset(db =DEFAULT_DATASOURCE, meas=DEFAULT_MEASUREMENT, time_from=start_dump_time, time_to=end_dump_time)
        #dset.init_from_dict(params_dict = mr_max.max_api.telm_influx_database.main_client.get_dataset())
        dump_cnt = dset.query_points("count(ec_data_mode)", "ec_data_mode = 3")[0]["count"]

        if dump_cnt != tlm_count:
            print("We did not upload all the points.")
            print("Points we uploaded were: " + str(dump_cnt))
            print("Points we were suppose to receive  were: " + str(tlm_count))
            test["Result"] = "Failed"
        else:
            print("All the points were uploaded!")
            test["Result"] = "Passed"
        test["upload_count"] = str(dump_cnt)
        test["ideal_upload_amt"] = str(tlm_count)
        print()
        print("------------------------")
        print()

    print("Final Results....")
    count = 1
    for test in test_matrix:
        print("========================")
        print(f" Test {count} - Dump Time: {test['Dump Time']} - Result: {test['Result']} - Packets Uploaded: {test['upload_count']} - Ideal Uploaded Packets: {test['ideal_upload_amt']}")
        print("========================")
        count +=1

        #tlm_dict = mr_max.get_tlm()
    # if not maxwell_telm_checker.confirm_command_tlm(tlm_dict):
    #     print("We received errors when receiving FRAM data. Here is the tlm dump")
    #     print(tlm_dict)
    #     return


def wait_until_dumping_is_done(output_file = None):
    count = 0
    start_time = arrow.utcnow().to('US/Pacific')
    while not mr_max.dump_tlm_ready():
        count = count +1
        time.sleep(1)
        if count %5 ==0:
            print("Dumping....")
            curr_time = arrow.utcnow().to('US/Pacific')
            print(f"Dumped for {(curr_time-start_time)} seconds")

    packet_number = 0
    tlm_dump = mr_max.get_mcu_dump_tlm()
    ts_analy = timestamp_analysis()
    if output_file is not None:
        with open(str(output_file) + ".txt", mode='w', newline="") as dump:
            for x in tlm_dump:
                dump.write(str(x["parsed_payload"]))
                dump.write("\r\n")
                packet_number +=1
                ts_analy.update_timestamp_diff(x)
    finish_time = arrow.utcnow().to('US/Pacific')
    diff_time = finish_time - start_time
    print("Dumping Complete. Total time to dump was: ", str(diff_time))
    print("Total Counted Packets: " + str(packet_number))
    print("Checking Dump Quality...")
    if not check_for_drop_packets(ts_analy):
        print("No Drop Packets!")
        print("Packet Dump Complete Successfully!")

    print("Final Packet Stats")
    ts_analy.calculate_packet_stats()
    display_packet_stats(ts_analy.ec_packet_stats, "Engine Controller")
    display_packet_stats(ts_analy.tc_packet_stats, "Thruster Controller")
    display_packet_stats(ts_analy.total_packet_stats, "Both Hardware")

    return finish_time







class timestamp_analysis():
    def __init__(self):
        self.ec_packet_count = 0
        self.ec_timestamp_current = None
        self.ec_timstamp_previous = None
        self.ec_timestamp_diff_list = []
        self.ec_packet_stats = {}
        self.tc_packet_count = 0
        self.tc_timestamp_current = None
        self.tc_timestamp_previous = None
        self.tc_timestamp_diff_list = []
        self.tc_packet_stats = {}

        self.total_packet_stats = {}

        self.junk_packets =[]
        self.junk_packets_count = 0


    def update_timestamp_diff(self, packet):
        if "tc_unix_time_us" in packet["parsed_payload"]:
            self.tc_packet_count +=1
            self.tc_timestamp_previous = self.tc_timestamp_current
            self.tc_timestamp_current = packet["parsed_payload"]["tc_unix_time_us"]
            if self.tc_timestamp_previous is not None:
                self.tc_timestamp_diff_list.append(self.tc_timestamp_current- self.tc_timestamp_previous)

        elif "ec_unix_time_us" in packet["parsed_payload"]:
            self.ec_packet_count +=1
            self.ec_timstamp_previous = self.ec_timestamp_current
            self.ec_timestamp_current = packet["parsed_payload"]["ec_unix_time_us"]
            if self.ec_timstamp_previous is not None:
                self.ec_timestamp_diff_list.append(self.ec_timestamp_current- self.ec_timstamp_previous)
        else:
            self.junk_packets_count +=1
            self.junk_packets.append(packet)

    def calculate_packet_stats(self):
        self.ec_packet_stats = self._calculate_packet_stats_inner(self.ec_timestamp_diff_list)
        self.tc_packet_stats = self._calculate_packet_stats_inner(self.tc_timestamp_diff_list)
        self.total_packet_stats = self._calculate_packet_stats_inner(self.ec_timestamp_diff_list + self.tc_timestamp_diff_list)


    def _calculate_packet_stats_inner(self, pkt_arr):
        stat_dict = {}
        stat_dict["cnt"] = len(pkt_arr)
        stat_dict["avg"] = statistics.mean(pkt_arr)
        stat_dict["max"] = max(pkt_arr)
        stat_dict["min"] = min(pkt_arr)
        stat_dict["median"] = statistics.median(pkt_arr)
        stat_dict["std"] = statistics.stdev(pkt_arr)
        stat_dict["cnt_under_10"] = len([item for item in pkt_arr if item<=10000])
        stat_dict["cnt_10_to_20"] = len([item for item in pkt_arr if item>10000 and item <=20000])
        stat_dict["cnt_20_to_50"] = len([item for item in pkt_arr if item>20000 and item <=50000])
        stat_dict["cnt_50_to_100"] = len([item for item in pkt_arr if item>50000 and item <=100000])
        stat_dict["cnt_above_100"] = len([item for item in pkt_arr if item>100000])
        stat_dict["invalid_cnt"] = len([item for item in pkt_arr if item>20000])
        stat_dict["valid_cnt"] = len([item for item in pkt_arr if item<=20000])
        return stat_dict




def check_for_drop_packets(ts_analy: timestamp_analysis):
    tlm_dict = get_tlm_until_status_success()
    ideal_tlm_cnt = tlm_dict["parsed_payload"]["ec_rec_nand_tlm_cnt"]
    print("Total NAND Flash Packet Are: " + str(ideal_tlm_cnt))
    total_count = ts_analy.ec_packet_count + ts_analy.tc_packet_count
    if ideal_tlm_cnt != total_count:
        print("Looks like we dropped some packets in our dump.")
        print("Here is the packet Breakdown:")
        create_packet_report_breakdown("Engine Controller",ts_analy.ec_packet_count,ideal_tlm_cnt)
        create_packet_report_breakdown("Thruster Controller",ts_analy.tc_packet_count,ideal_tlm_cnt)
        print("Junk Packet total: " + str(ts_analy.junk_packets_count))
        return True
    else:
        return False


def create_packet_report_breakdown(packet_type, read_packet_amount, ideal_packet_number):
        print("===============================================")
        print(f"{ packet_type} Packet Count:" + str(read_packet_amount))
        diff_packets =  read_packet_amount - ideal_packet_number
        if diff_packets < 0:
            str_diff = "lost"
        else:
            str_diff = "gain"
        str_diff_msg = f"We {str_diff} {diff_packets} {packet_type} Packets"
        print(str_diff_msg)
        print("===============================================")
        print()



def display_packet_stats(data_dict, hardware_string):
    print()
    print(f"Packet Time Analysis for {hardware_string}")
    print("========================================================")
    print(f"- Total Count Of Packets: {data_dict['cnt']}" )
    print(f"- Mean Time Difference: {data_dict['avg']}" )
    print(f"- Max Time Difference: {data_dict['max']}" )
    print(f"- Min Time Difference: {data_dict['min']}" )
    print(f"- Median Time Difference: {data_dict['median']}" )
    print(f"- Stand Dev Time Difference: {data_dict['std']}" )
    print(f"- Time Difference Under 10 ms: {data_dict['cnt_under_10']}" )
    print(f"- Time Difference Between 10 and 20 ms: {data_dict['cnt_10_to_20']}" )
    print(f"- Time Difference Between 20 and 50 ms: {data_dict['cnt_20_to_50']}" )
    print(f"- Time Difference Between 50 and 100 ms: {data_dict['cnt_50_to_100']}" )
    print(f"- Time Difference Above 100 ms: {data_dict['cnt_above_100']}" )
    print("----------------------------------------------------------------")
    print(f"- Total Valid Packets: {data_dict['valid_cnt']}" )
    print(f"- Total Invalid Packets: {data_dict['invalid_cnt']}")
    print("========================================================")
    print()

def print_flash_txt_output():
    with open(str("flash_dump") + ".txt", mode='r', newline="") as read_file:
        for line in read_file:
            line_str = line.rstrip()
            if "ec_unix_time_us" in line_str:
                line_str = line_str.replace("\'", "\"")
                ec_dict =json.loads(line_str)
                currr_ts = arrow.get(ec_dict["ec_unix_time_us"])
                currr_ts = currr_ts.to('US/Pacific')
                print(f"{currr_ts} - {ec_dict['sc_plowp']}")

            elif "tc_unix_time_us" in line_str:
                pass
            else:
                print("BAD")


def get_tlm_until_status_success():
    good_packet = False
    good_dict = None
    while not good_packet:
        tlm_dict = mr_max.get_tlm()
        if tlm_dict["status"] == "STATUS_SUCCESS":
            good_packet = True
            good_dict = tlm_dict

    return good_dict


def gather_dump_time_input():
    print("\nWelcome to the dump test center\n")
    input_valid = False
    time_amount = -1
    extra_string = "NOTE: You can change this manually in the script by updating dump_amount.\n"
    while not input_valid:
        if dump_amount == -1:
            time_amount = input("Enter the amount of seconds do you want to record high rate telemetry for. Or press Q to quit.\n" +extra_string +"Input:")
            if time_amount == "q" or time_amount == "Q":
                print("Got it. Exiting out.")
                return -1
            elif is_number(time_amount):
                time_amount = float(time_amount)
                if time_amount > 0.0 and time_amount < 1000.0:
                    input_valid = True
                else:
                    print("\nThe value selected is not valid. Please select a valid number between 0 - 1000 (in seconds).\n")
            else:
                print("\nThe value selected is not valid. Please select a valid number between 0 - 1000 (in seconds).\n")
        else:
            input_valid = True
            time_amount = dump_amount
        extra_string = ""
    return time_amount


def is_number(string_value):
    try:
        float(string_value)
        return True
    except ValueError:
        return False



if __name__== "__main__":
    main()
