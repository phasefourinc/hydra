from maxapi.mr_maxwell import MrMaxwell, MaxConst
from maxapi.maxwell_information_processor import _process_errors, parse_maxwell_header, process_maxwell_record, read_hardware_maxwell_record
import json
from maxapi.max_cmd_id import MaxCmd
DEFAULT_BAUD_RATE = "1000000"
DEFAULT_COM_PORT = "INSERT_COM_HERE"
DEFAULT_DATASOURCE = "sandbox"
HOST = "nekone.phasefour.co"
DEFAULT_MEASUREMENT = "default_measurement"

mr_max = MrMaxwell(DEFAULT_COM_PORT, DEFAULT_BAUD_RATE, DEFAULT_DATASOURCE, DEFAULT_MEASUREMENT)
GET_MAX_SW_INFO = MaxCmd.GET_SW_INFO
SET_MAX_HRD_INFO = MaxCmd.SET_MAX_HRD_INFO
GET_MAX_HRD_INFO = MaxCmd.GET_MAX_HRD_INFO
ERASE_MAX_HRD_INFO = MaxCmd.ERASE_MAX_HRD_INFO
NUMBER_OF_SECTORS = 32
STATUS_SUCCESS = 0
maxwell_list = []
software_list = []


def main():
    read_sw_information_test()
    to_write_and_read_maxwell_hardware_info()
    write_and_confirm_maxwell_hardware_info("Tank", "Tank ID goes here", "Rev1", "42", 17)
    #
    read_all_flash_applications()


'''
STANDARD TESTS TO RUN
'''

def read_sw_information_test():
    sw_information =  get_all_the_software_info()
    _write_json_file("json_test.json", sw_information)


def to_write_and_read_maxwell_hardware_info():
    mr_max.send_maxwell_message(ERASE_MAX_HRD_INFO, [])
    byte_arr = process_maxwell_record("Thruster Controller", "001 335-201", "Rev 1", "001-001", 0)
    mr_max.send_maxwell_message(SET_MAX_HRD_INFO, list(byte_arr))
    byte_arr = process_maxwell_record("Prop Controller", "001277-201", "Rev 2", "4", 29)
    mr_max.send_maxwell_message(SET_MAX_HRD_INFO, list(byte_arr))

    payload = mr_max.send_maxwell_message(GET_MAX_HRD_INFO, [0x00])
    output = read_hardware_maxwell_record(payload["raw_payload"], 0x00)
    maxwell_list.append(output)

    payload = mr_max.send_maxwell_message(GET_MAX_HRD_INFO, [29])
    output = read_hardware_maxwell_record(payload["raw_payload"], 29)
    maxwell_list.append(output)
    _write_json_file("maxwell_hardware.json", maxwell_list)


def write_and_confirm_maxwell_hardware_info(field_name, part_number, revision, serial_number, offset_location):
    byte_arr = process_maxwell_record(field_name,part_number,revision,serial_number,offset_location)
    if byte_arr != None:
        mr_max.send_maxwell_message(SET_MAX_HRD_INFO, list(byte_arr))
        payload = mr_max.send_maxwell_message(GET_MAX_HRD_INFO, [offset_location])
        output = read_hardware_maxwell_record(payload["raw_payload"], offset_location)
        _verify_maxwell_information(output,field_name, part_number, revision, serial_number)

def _verify_maxwell_information(output,field_name, part_number, revision, serial_number ):
    if output["field_name"] != field_name:
        _process_errors(4,[field_name,output["field_name"]])
    if output["part_number"] != part_number:
        _process_errors(4,[field_name,output["part_number"]])
    if output["revision"] != revision:
        _process_errors(4,[field_name,output["revision"]])
    if output["serial_number"] != serial_number:
        _process_errors(4,[field_name,output["serial_number"]])

'''
Helper Functions to run
'''

def get_all_the_software_info(hardware_select = "pcu"):
    if hardware_select == "pcu":
        hardware_select = MaxConst.PROP_CONTROLLER_ADDRESS
    elif hardware_select == "thr":
        hardware_select = MaxConst.THRUSTER_CONTROLLER_ADDRESS
    else:
        print("Error invalid hardware please select either pcu or thr")
    software_field_name = ["Current Application","Bootloader 1", "Bootloader 2", "Flash Application 1", "Flash Application 2", "Golden Image Application"]
    all_software_info = []
    get_sw_info = mr_max.get_sw_info("Current_App", transmit_mask=hardware_select)
    parsed_sw_info = parse_maxwell_header(get_sw_info, "Current Application")
    all_software_info.append(parsed_sw_info)

    get_sw_info = mr_max.get_sw_info("Boot_1", transmit_mask=hardware_select)
    parsed_sw_info = parse_maxwell_header(get_sw_info, "Bootloader 1")
    all_software_info.append(parsed_sw_info)

    get_sw_info = mr_max.get_sw_info("Boot_2", transmit_mask=hardware_select)
    parsed_sw_info = parse_maxwell_header(get_sw_info, "Bootloader 2")
    all_software_info.append(parsed_sw_info)

    get_sw_info = mr_max.get_sw_info("Flash_App_1", transmit_mask=hardware_select)
    parsed_sw_info = parse_maxwell_header(get_sw_info, "Flash Application 1")
    all_software_info.append(parsed_sw_info)

    get_sw_info = mr_max.get_sw_info("Flash_App_2", transmit_mask=hardware_select)
    parsed_sw_info = parse_maxwell_header(get_sw_info, "Flash Application 2")
    all_software_info.append(parsed_sw_info)

    get_sw_info = mr_max.get_sw_info("Gold_Img", transmit_mask=hardware_select)
    parsed_sw_info = parse_maxwell_header(get_sw_info, "Golden Image Application")
    all_software_info.append(parsed_sw_info)

    return all_software_info

def read_all_flash_applications():
    count = 0
    maxwell_list= []
    while (count<NUMBER_OF_SECTORS):
        payload = mr_max.send_maxwell_message(GET_MAX_HRD_INFO, [count])
        output = read_hardware_maxwell_record(payload["raw_payload"], count)
        if output["field_name"] != "null":
            maxwell_list.append(output)
        count = count + 1
    _write_json_file("read_all_hardware.json", maxwell_list)



def _write_json_file (file_name, data_struct):
    with open(file_name, mode='w', newline='\n') as json_file:
        json_string = json.dumps(data_struct, indent=4, sort_keys=True)
        json_file.write(str(json_string))

if __name__ == "__main__":
    main()