import time
from maxapi.mr_maxwell import MrMaxwell, MaxConst
import maxapi.maxwell_tlm_processor as max_check
from maxapi.max_errors import MaxErr
from chozo.power_supplies.maxwell_bk_precision_9201 import  MaxwellBkPrecision9201
DEFAULT_BAUD_RATE = "1000000"
DEFAULT_COM_PORT = "COM4"
DEFAULT_DATASOURCE = "sandbox"
HOST = "nekone.phasefour.co"
DEFAULT_MEASUREMENT = "andrew_pcu_dev"
DEFAULT_HARDWARE = "Prop Controller"


power_supply = MaxwellBkPrecision9201()

power_supply.turn_off_power_supply()
time.sleep(.500)
power_supply.set_voltage_supply(28)
time.sleep(.500)
power_supply.turn_on_power_supply()
time.sleep(.500)

mr_max = MrMaxwell(DEFAULT_COM_PORT, DEFAULT_BAUD_RATE, DEFAULT_DATASOURCE, DEFAULT_MEASUREMENT)
# NOTE technically this is redundant however it might be good programming to make it explict what hardware this script is talking to.
mr_max.update_default_interfacting_hardware(DEFAULT_HARDWARE)

LOWEST_BOOTLOADER_NUM = MaxErr.GENERAL_BOOTLOADER_ERROR
HIGHEST_BOOTLOADER_NUM = MaxErr.END_OF_BOOTLOADER_MAIN


def main():
    print("Running bootloader_selection_test\n")
    if not mr_max.is_prop_controller_connected():
        print("The Prop Controller is not connected. Please connect the Prop Controller to run the test for the prop controller.")
    else:
        mr_max.clear_errors(transmit_mask=MaxConst.PROP_CONTROLLER_ADDRESS)

    if not mr_max.is_thruster_controller_connected():
        print("The Thruster Controller is not connected. Please connect the Thruster Controller to run the test for the thrusters controller.")
    else:
        mr_max.clear_errors(transmit_mask=MaxConst.THRUSTER_CONTROLLER_ADDRESS)

    if not mr_max.is_prop_controller_connected() and not mr_max.is_thruster_controller_connected():
        print("Nothing is connected finishing test...")
        return
    time.sleep(1)

    result = run_bl_test_mutiple_times(25)  # verify_app_select(2)
    if result == True:
        print("Bootloader Select Test PASSED!")
    else:
        print("Bootloader Select Test Failed....")

    return

def run_bl_test_mutiple_times(num_of_runs):
    for run in range(num_of_runs):
        result = run_bl_selection_test()  # verify_app_select(2)
        if result == True:
            print("Bootloader Select Test run '" + str(run) + "' passed.")
        else:
            print("Bootloader Select Test Failed....")
            return False
        power_supply.reset_power_supply(1.0,1.0)
        if mr_max.is_prop_controller_connected():
            mr_max.set_utime()
            time.sleep(.100)

        else:
            mr_max.set_utime(transmit_mask=MaxConst.THRUSTER_CONTROLLER_ADDRESS)
            time.sleep(.100)


    return True


# This function runs through the bl selection test.
# It triggers both bootloader sections by tripping the wdt and validate that the bootloader has been selected correctly.
# It then checks at the end to ensure there is no errors.
# The actual checking of the bootloader is done in run_bl_check. This function orchestrates the whole test.
def run_bl_selection_test():

    test_passed_condition = False
    prop_bl_test_passed = True
    thrust_bl_test_passed = True
    bootloader_image = 1
    while bootloader_image < 3:

        # Since we are reseting wdt after the test we want to do the thruster controller first.
        if mr_max.is_thruster_controller_connected():
            # Selecting the right bootloader image based on if it is connected to the prop or not. Due to inrush issues.
            bootloader_image_to_test = select_thruster_controller_bootloader(bootloader_image)
            print("Expecting to select bootloader image for the Thruster Controller " + str(bootloader_image_to_test))

            if not run_bl_check(bootloader_image_to_test, transmit_mask=MaxConst.THRUSTER_CONTROLLER_ADDRESS):
                thrust_bl_test_passed = False  #If this not is true it indicates there was a error we shall report false.
            else:
                print("Thruster Controller Bootloader Test Passed. Bootloader was "+ str(bootloader_image_to_test) + "\n")

            mr_max.stop_wdt(transmit_mask=MaxConst.THRUSTER_CONTROLLER_ADDRESS)


        if mr_max.is_prop_controller_connected():
            print("Expecting to select bootloader image for the Prop Controller " + str(bootloader_image))

            if not run_bl_check(bootloader_image, transmit_mask=MaxConst.PROP_CONTROLLER_ADDRESS):
                prop_bl_test_passed = False  #If this not is true it indicates there was a error we shall report false.
            else:
                print("Prop Controller Bootloader Test Passed. Bootloader was "+ str(bootloader_image)+ "\n")

            mr_max.stop_wdt(transmit_mask=MaxConst.PROP_CONTROLLER_ADDRESS)

        # IF it is not the last image we are running run the reset sequence. If it is the last one do not run anything.
        if bootloader_image !=2:
            time.sleep(2.0)
            # If a prop controller is involved sending utime by itself will be fine.
            # Because the prop forwards the command to the thruster controller
            if mr_max.is_prop_controller_connected():
                mr_max.set_utime()
            else:
                mr_max.set_utime(transmit_mask=MaxConst.THRUSTER_CONTROLLER_ADDRESS) # no prop send to thruster controller.

            time.sleep(.500)
        bootloader_image = bootloader_image + 1
        print("\n-------------------------------------------------\n")


        # This checks if both conditions passed. This is done using boolean logic.
        # The logic flow is: (a or !b) and (c or !d) where b and d are checking if the hardware is connected.
        # IF the hardware is not connected we want that check to pass, because we can't test that part of the hardware if it is not connected.
    if (prop_bl_test_passed or not mr_max.is_prop_controller_connected()) and (thrust_bl_test_passed or not mr_max.is_thruster_controller_connected()):
        test_passed_condition = True
    return test_passed_condition


# This functions checks telemetry for a given hardware, make sures that the bootloader was selected correctly and there were no bootloader errors.
def run_bl_check(expected_bootloader: int, transmit_mask: int= MaxConst.PROP_CONTROLLER_ADDRESS):
    test_result = False
    tlm = get_tlm_loop(3,transmit_mask) # We are going to try to get tlm 3 times. Most times it will get the result the first time.

    # Checking the ack to make sure there is no embedded timeout.
    if max_check.check_ack(tlm):

        bootloader_number = get_bootloader_number(tlm, transmit_mask)
        if bootloader_number == expected_bootloader:
            if not check_bootloader_error(tlm,transmit_mask):
                test_result = True
            else:
                first_error = get_bootloader_error(tlm,transmit_mask)
                print("There was a error in the hardware. This error could be a bootloader error the error was  '" + str(first_error) + "'.")
        else:
            hardware_string = _get_hardware_string(transmit_mask)
            print ("The " + hardware_string + " bootloader image was not selected correctly. The test expected the bootloader to be on image '" + str(expected_bootloader) + "'. But it was on image '" + str(bootloader_number)+"'")

    return test_result



# This check is needed because when the prop controller is connected to the thruster controller the thruster controller bootlaoder starts at 2 because of inrush from the prop controller.
# Thus it will have the opposite result as the prop controller so the logic is here to correct that.
# If prop controller is connected the thruster controller will start at bootloader 2 then bootloader 1.
# If the prop controller is not connected the thruster controller will start at 1 then 2 like normal

def select_thruster_controller_bootloader(bootloader_image):
    if mr_max.is_prop_controller_connected():
        if bootloader_image == 1:
            bootloader_image_to_test = 2
        else:
            bootloader_image_to_test = 1

    else:
        bootloader_image_to_test = bootloader_image
    return bootloader_image_to_test

#Hardware Agnostic Parses bootloader number
def get_bootloader_number(payload, hardware):
    if hardware == MaxConst.PROP_CONTROLLER_ADDRESS:
        return payload["parsed_payload"]["BL_SELECT"]
    else:
        return payload["parsed_payload"]["RFT_BL_SELECT"]



# Checks if there is a bootloader error
# This is done by looking at the range of errors and see if there is a error in that range.
# If the error is between the lowest bootloader number and the highest, then we know we have a bootloader error.
def check_bootloader_error(payload, hardware: int):
    error_status = True
    if not max_check.search_for_error_range(payload,err_num_low=LOWEST_BOOTLOADER_NUM, err_num_high=HIGHEST_BOOTLOADER_NUM,tx_mask=hardware):
        error_status = False
    return error_status

# Finding a bootloader error by looking at all the errors we got from a dictonary.
# Going through each one and then determine which of the errors in in between the lowest and highest bootloader error.
def get_bootloader_error(payload, transmit_mask):
    bootloader_error = 0
    errors = max_check.parse_telemetry_errors(payload,tx_mask=transmit_mask)
    if errors["status"] == "ERROR":
        for key, value in errors.items():
            if key != "status":
                if value >= LOWEST_BOOTLOADER_NUM and value <= HIGHEST_BOOTLOADER_NUM:
                    bootloader_error = value
                    return bootloader_error

    return bootloader_error


# This function gets tlm a certian amount of times before it gives up. Helps reduce embedded timeouts.
def get_tlm_loop(tlm_attempts, transmit_mask: int= MaxConst.PROP_CONTROLLER_ADDRESS):
    if tlm_attempts > 0:
        for x in range(tlm_attempts):
            output = mr_max.get_tlm(transmit_mask=transmit_mask)
            if output["status"] == "STATUS_SUCCESS":
                return output
            time.sleep(.250)
    else:
        output = mr_max.get_tlm(transmit_mask=transmit_mask)
    # IF we get here we are returning junk data.
    return output


def _get_hardware_string(transmit_mask):
    if transmit_mask == MaxConst.PROP_CONTROLLER_ADDRESS:
        hardware_string = "Prop Controller's"
    else:
        hardware_string = "Thruster Controller's"
    return hardware_string


if __name__ == "__main__":
    main()