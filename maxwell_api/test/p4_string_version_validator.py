import maxapi.maxwell_information_processor as mip
'''
This is to test the is_valid_p4_version function by putting in strings to make sure it accurately determines 
That we can parse the system
'''

def main():
    test_cases = [
        {"vers_str":"some_tag_1.2.3","expected_result":True},
        {"vers_str":"some_tag_1.2.3-45","expected_result":True},
        {"vers_str":"some_tag_1.2.3-45-AbCdEf1234","expected_result":True},
        {"vers_str":"invalid_version_format","expected_result":False},
        {"vers_str": "edu_hydra_ec_sw_0.2.0-1-gd27f9dc", "expected_result": True},
        {"vers_str": "hydra_ec_sw_0.5.0", "expected_result": True},
        {"vers_str": "bl_full_edu_hydra_ec_sw_0.5.1", "expected_result": True},
        {"vers_str": "bl_full_edu_hydra_ec_sw_0.2.0", "expected_result": True},
        {"vers_str": "bl_full_hydra_tc_sw_0.4.0", "expected_result": True},
        {"vers_str": "bl_full_hydra_ec_sw_0.4.0-7-ga022562", "expected_result": True},
        {"vers_str": "bl_full_hydra_ec_sw_0.3.2", "expected_result": True},
        {"vers_str": "bl_full_hydra_ec_sw_0.3.2-5-g412b9dc", "expected_result": True},
        {"vers_str": "bl_full_hydra_ec_sw_0.3.2-6-g89f632c", "expected_result": True},
        {"vers_str": "A00275_0.9.0-64-g27e6990", "expected_result": True},
        {"vers_str": "bl_full_pcu_sw_0.6.0-17-gf1743f0c", "expected_result": True},
        {"vers_str": "bl_full_thr_sw_0.6.0-5-gd26078e3", "expected_result": True},
        {"vers_str": "pflash_thr_sw_0.6.0-5-gd26078e3", "expected_result": True},
        {"vers_str": "A00651_0.10.0-82-g807716e", "expected_result": True},
        {"vers_str": "A00137_Unit1_0.8.0-22-g31d6639", "expected_result": True},
        {"vers_str": "A00000_thr_0.10.0-18-gaa1bd73", "expected_result": True},
        {"vers_str": "12.1.5-12-asds451", "expected_result": False},
        {"vers_str": "q_0.0.0", "expected_result": True},
        {"vers_str": "q_12651.784651.4151", "expected_result": True},
        {"vers_str": "q_12651.784651.4151-1515151515155-SDF32wr3", "expected_result": True},
        {"vers_str": "234.3.4.5", "expected_result": False},
        {"vers_str": "hello-q_113.324.1", "expected_result": False},
        {"vers_str": "e_e_r_f_H_Fq_113.324.1", "expected_result": True},
        {"vers_str": "234.3.4.5_eree-32", "expected_result": False},
        {"vers_str":"some_tag_1.2.3-234","expected_result":True},
        {"vers_str":"some_tag_1.2.3-234_asf_","expected_result":False},
        {"vers_str":"some_tag_1.2.3_ewere-23-asdasd","expected_result":False},
        {"vers_str":"some_tag_1.2.3sd","expected_result":False},
        {"vers_str":"some_tag_1.2.3sdf-23-sdf3f","expected_result":False},
        {"vers_str":"some_tag_1.2.3-23-_sdf","expected_result":False},
        {"vers_str":"some_tag_1.2.3-_23-aasdw3","expected_result":False},
        {"vers_str":"some_tag_34.1.2.3","expected_result":False},
        {"vers_str":"some_tag_1.2.3","expected_result":True},
        {"vers_str": "3.4.5", "expected_result": False},
    ]
    any_test_failed = False
    for test in test_cases:
        val = check_if_valid(test)
        if not val:
            any_test_failed = True

    print("\n================================================\n")
    print(f"OVERALL RESULTS - {not any_test_failed}")
    print("\n================================================\n")

    any_test_failed = False
    for test in test_cases:
        parse_version_string(test)
        if not val:
            any_test_failed = True
    print("\n================================================\n")
    print(f"OVERALL RESULTS - {not any_test_failed}")
    print("\n================================================\n")

def check_if_valid(test_parm):
    version_string = test_parm["vers_str"]
    expected_result = test_parm["expected_result"]
    result = mip.is_valid_p4_version(version_string)
    if result != expected_result:
        print(f"Test Failed for {version_string}\n  -Result: Expected {expected_result}, got {result} from p4_version_validator")
        return False
    else:
        print(f"Test Passed for {version_string}\n  -Result: {result} from p4_version_validator")
        return True


def parse_version_string(test_parm):
    version_string = test_parm["vers_str"]
    expected_result = test_parm["expected_result"]
    result = mip.p4_version_parser(version_string)
    if result is None:
        parse_result = False
        print(f"Was not able to parse {version_string}")
    else:
        parse_result = True
        print(f"Was  able to parse {version_string}. Parse Version was {parse_result}")
    if parse_result != expected_result:
        print(f"Test Failed for {version_string}\n  -Result: Expected {expected_result}, got {result} from")
        return False
    else:
        print(f"Test Passed for {version_string}\n  -Result: {result} from p4_version_validator")
        return True
if __name__ == "__main__":
    main()
