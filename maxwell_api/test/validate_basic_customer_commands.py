import time
from maxapi.mr_maxwell import MrMaxwell, MaxConst, MaxErr, MaxCmd
from maxapi.maxwell_information_processor import parse_maxwell_header, p4_version_parser
import maxwell_telm_checker as max_check
import random
DEFAULT_BAUD_RATE =  "115200"
DEFAULT_COM_PORT = "COM4"
DEFAULT_DATASOURCE= "sandbox"
HOST="nekone.phasefour.co"
DEFAULT_MEASUREMENT = "andrew_pcu_dev"
DEFAULT_HARDWARE = "Prop Controller"


mr_max = MrMaxwell(DEFAULT_COM_PORT,DEFAULT_BAUD_RATE,DEFAULT_DATASOURCE,DEFAULT_MEASUREMENT,)

# NOTE technically this is redundant however it might be good programming to make it explict what hardware this script is talking to.
mr_max.update_default_interfacting_hardware(DEFAULT_HARDWARE)



def state_test():
    # LOW_PWR
    #  + to ABORT = true
    #  + to IDLE = true
    #  + to THRUST = false
    #  + to SW_UPDATE = true
    #  + to MANUAL = true
    #  + to WILD_WEST = false
    #
    # IDLE
    #  + to ABORT = true
    #  + to IDLE = true
    #  + to THRUST (immediate) = false (busy)
    #  to THRUST (delayed) = true
    #  + to SW_UPDATE = false
    #  + to MANUAL = true
    #  + to WILD_WEST = false
    #
    # SW_UPDATE
    #  + to ABORT = true
    #  + to IDLE = false
    #  + to THRUST = false
    #  + to SW_UPDATE = true
    #  + to MANUAL = false
    #  + to WILD_WEST = false
    #
    # MANUAL
    #  + to ABORT = true
    #  + to IDLE = true
    #  to THRUST = true
    #  + to SW_UPDATE = false
    #  + to MANUAL = true
    #  + to WILD_WEST = true
    #
    # WILD_WEST
    #  + to ABORT = true
    #  + to IDLE = true
    #  to THRUST = true
    #  + to SW_UPDATE = false
    #  + to MANUAL = true
    #  + to WILD_WEST = true
    #
    # THRUST
    #
    #
    #
    #


    tlm_dict = mr_max.goto_safe()     # Start in LOW_PWR
    time.sleep(0.250)
    mr_max.check_eq('STATE', LOW_PWR)
    tlm_dict = mr_max.goto_safe()     # LOW_PWR -> LOW_PWR
    time.sleep(0.250)
    mr_max.check_eq('STATE', LOW_PWR)
    tlm_dict = mr_max.goto_wild_west() # LOW_PWR -> WILD_WEST (fail)
    mr_max.check_eq('STATE', LOW_PWR)
    tlm_dict = mr_max.goto_thrust()    # LOW_PWR -> THRUST (fail)
    mr_max.check_eq('STATE', LOW_PWR)
    tlm_dict = mr_max.goto_sw_update() # LOW_PWR -> SW_UPDATE
    mr_max.check_eq('STATE', SW_UPDATE)
    tlm_dict = mr_max.goto_idle()      # SW_UPDATE -> IDLE (fail)
    mr_max.check_eq('STATE', SW_UPDATE)
    tlm_dict = mr_max.goto_sw_update() # SW_UPDATE -> SW_UPDATE
    mr_max.check_eq('STATE', SW_UPDATE)
    tlm_dict = mr_max.goto_manual()    # SW_UPDATE -> MANUAL (fail)
    mr_max.check_eq('STATE', SW_UPDATE)
    tlm_dict = mr_max.goto_wild_west() # SW_UPDATE -> WILD_WEST (fail)
    mr_max.check_eq('STATE', SW_UPDATE)
    tlm_dict = mr_max.goto_thrust()    # SW_UPDATE -> THRUST (fail)
    mr_max.check_eq('STATE', SW_UPDATE)
    tlm_dict = mr_max.goto_safe()     # SW_UPDATE -> LOW_PWR
    time.sleep(0.250)
    mr_max.check_eq('STATE', LOW_PWR)
    tlm_dict = mr_max.goto_idle()      # LOW_PWR -> IDLE
    mr_max.check_eq('STATE', IDLE)
    tlm_dict = mr_max.goto_thrust()    # IDLE -> THRUST immediate (fail)
    mr_max.check_eq('STATE', IDLE)
    tlm_dict = mr_max.goto_idle()      # IDLE -> IDLE
    mr_max.check_eq('STATE', IDLE)
    tlm_dict = mr_max.goto_sw_update() # IDLE -> SW_UPDATE (fail)
    mr_max.check_eq('STATE', IDLE)
    tlm_dict = mr_max.goto_wild_west() # IDLE -> WILD_WEST (fail)
    mr_max.check_eq('STATE', IDLE)
    tlm_dict = mr_max.goto_manual()    # IDLE -> MANUAL
    mr_max.check_eq('STATE', MANUAL)
    tlm_dict = mr_max.goto_sw_update() # MANUAL -> SW_UPDATE (fail)
    mr_max.check_eq('STATE', MANUAL)
    tlm_dict = mr_max.goto_manual()    # MANUAL -> MANUAL
    mr_max.check_eq('STATE', MANUAL)
    tlm_dict = mr_max.goto_wild_west() # MANUAL -> WILD_WEST
    mr_max.check_eq('STATE', WILD_WEST)
    tlm_dict = mr_max.goto_sw_update() # WILD_WEST -> SW_UPDATE (fail)
    mr_max.check_eq('STATE', WILD_WEST)
    tlm_dict = mr_max.goto_wild_west() # WILD_WEST -> WILD_WEST
    mr_max.check_eq('STATE', WILD_WEST)
    tlm_dict = mr_max.goto_safe()     # WILD_WEST -> ABORT
    time.sleep(0.250)
    mr_max.check_eq('STATE', LOW_PWR)
    tlm_dict = mr_max.goto_manual()    # LOW_PWR -> MANUAL
    mr_max.check_eq('STATE', MANUAL)
    tlm_dict = mr_max.goto_wild_west() # MANUAL -> WILD_WEST
    mr_max.check_eq('STATE', WILD_WEST)
    tlm_dict = mr_max.goto_manual()    # WILD_WEST -> MANUAL
    mr_max.check_eq('STATE', MANUAL)
    tlm_dict = mr_max.goto_idle()      # MANUAL -> IDLE
    mr_max.check_eq('STATE', IDLE)
    tlm_dict = mr_max.goto_safe()     # IDLE -> LOW_PWR
    time.sleep(0.250)
    mr_max.check_eq('STATE', LOW_PWR)
    tlm_dict = mr_max.goto_manual()    # LOW_PWR -> MANUAL
    mr_max.check_eq('STATE', MANUAL)
    tlm_dict = mr_max.goto_wild_west() # MANUAL -> WILD_WEST
    mr_max.check_eq('STATE', WILD_WEST)
    tlm_dict = mr_max.goto_idle()      # WILD_WEST -> IDLE
    mr_max.check_eq('STATE', IDLE)

    # TODO Continue with THRUST state tests here


def pcu_sanity_test():
    mr_max.sleep_w_tlm(1.0)
    tlm_dict = mr_max.goto_safe()          # Start in LOW_PWR
    mr_max.sleep_w_tlm(0.250)
    tlm_dict = mr_max.send_maxwell_message(CLR_ERRORS, [])     # Clear Acpt/Rjct counters
    mr_max.sleep_w_tlm(0.250)
    tlm_dict = mr_max.goto_manual()         # Get to MANUAL
    mr_max.sleep_w_tlm(1.0)
    tlm_dict = mr_max.send_maxwell_message(SET_PFCV_DAC_RST_EN, [0x00])
    mr_max.sleep_w_tlm(1.0)
    tlm_dict = mr_max.send_maxwell_message(SET_RFT_RST_EN, [0x01])
    mr_max.sleep_w_tlm(1.0)
    tlm_dict = mr_max.send_maxwell_message(SET_HEATER_EN, [0x01])
    mr_max.sleep_w_tlm(1.0)
    tlm_dict = mr_max.send_maxwell_message(SET_HEATER_RST_EN, [0x01])
    mr_max.sleep_w_tlm(1.0)
    tlm_dict = mr_max.send_maxwell_message(SET_HPISO_EN, [0x01])
    mr_max.sleep_w_tlm(1.0)
    tlm_dict = mr_max.send_maxwell_message(SET_LPISO_EN, [0x01])
    mr_max.sleep_w_tlm(1.0)
    tlm_dict = mr_max.send_maxwell_message(SET_PFCV_DAC_RST_EN, [0x01])
    mr_max.sleep_w_tlm(1.0)
    tlm_dict = mr_max.send_maxwell_message(SET_WDI_EN, [0x01])
    mr_max.sleep_w_tlm(1.0)
    tlm_dict = mr_max.send_maxwell_message(SET_RFT_RST_EN, [0x00])
    mr_max.sleep_w_tlm(1.0)
    tlm_dict = mr_max.send_maxwell_message(SET_HEATER_EN, [0x00])
    mr_max.sleep_w_tlm(1.0)
    tlm_dict = mr_max.send_maxwell_message(SET_HEATER_RST_EN, [0x00])
    mr_max.sleep_w_tlm(1.0)
    tlm_dict = mr_max.send_maxwell_message(SET_HPISO_EN, [0x00])
    mr_max.sleep_w_tlm(1.0)
    tlm_dict = mr_max.send_maxwell_message(SET_LPISO_EN, [0x00])
    mr_max.sleep_w_tlm(1.0)
    tlm_dict = mr_max.send_maxwell_message(SET_WDI_EN, [0x00])
    mr_max.sleep_w_tlm(1.0)
    tlm_dict = mr_max.send_maxwell_message(SET_PFCV_DAC_RST_EN, [0x00])
    mr_max.sleep_w_tlm(1.0)
    tlm_dict = mr_max.send_maxwell_message(SET_RFT_RST_EN, [0x01])
    mr_max.sleep_w_tlm(1.0)
    tlm_dict = mr_max.send_maxwell_message(SET_HEATER_EN, [0x01])
    mr_max.sleep_w_tlm(1.0)
    tlm_dict = mr_max.send_maxwell_message(SET_HEATER_RST_EN, [0x01])
    mr_max.sleep_w_tlm(1.0)
    tlm_dict = mr_max.send_maxwell_message(SET_HPISO_EN, [0x01])
    mr_max.sleep_w_tlm(1.0)
    tlm_dict = mr_max.send_maxwell_message(SET_LPISO_EN, [0x01])
    mr_max.sleep_w_tlm(1.0)
    tlm_dict = mr_max.send_maxwell_message(SET_WDI_EN, [0x01])
    mr_max.sleep_w_tlm(1.0)
    tlm_dict = mr_max.send_maxwell_message(SET_PFCV_DAC_RST_EN, [0x01])
    mr_max.sleep_w_tlm(1.0)
    tlm_dict = mr_max.send_maxwell_message(SET_HEATER_PWM_RAW, [0x80, 0x00])
    mr_max.sleep_w_tlm(0.5)
    tlm_dict = mr_max.send_maxwell_message(SET_HEATER_PWM_RAW, [0x40, 0x00])
    mr_max.sleep_w_tlm(0.5)
    tlm_dict = mr_max.send_maxwell_message(SET_HEATER_PWM_RAW, [0x20, 0x00])
    mr_max.sleep_w_tlm(0.5)
    tlm_dict = mr_max.send_maxwell_message(SET_HEATER_PWM_RAW, [0x10, 0x00])
    mr_max.sleep_w_tlm(0.5)
    tlm_dict = mr_max.send_maxwell_message(SET_HEATER_PWM_RAW, [0x08, 0x00])
    mr_max.sleep_w_tlm(0.5)
    tlm_dict = mr_max.send_maxwell_message(SET_HEATER_PWM_RAW, [0x04, 0x00])
    mr_max.sleep_w_tlm(0.5)
    tlm_dict = mr_max.send_maxwell_message(SET_HEATER_PWM_RAW, [0x00, 0x00])
    mr_max.sleep_w_tlm(0.5)

    tlm_dict = mr_max.send_maxwell_message(SET_HPISO_PWM_RAW, [0x80, 0x00])
    mr_max.sleep_w_tlm(0.5)
    tlm_dict = mr_max.send_maxwell_message(SET_HPISO_PWM_RAW, [0x40, 0x00])
    mr_max.sleep_w_tlm(0.5)
    tlm_dict = mr_max.send_maxwell_message(SET_HPISO_PWM_RAW, [0x20, 0x00])
    mr_max.sleep_w_tlm(0.5)
    tlm_dict = mr_max.send_maxwell_message(SET_HPISO_PWM_RAW, [0x10, 0x00])
    mr_max.sleep_w_tlm(0.5)
    tlm_dict = mr_max.send_maxwell_message(SET_HPISO_PWM_RAW, [0x08, 0x00])
    mr_max.sleep_w_tlm(0.5)
    tlm_dict = mr_max.send_maxwell_message(SET_HPISO_PWM_RAW, [0x04, 0x00])
    mr_max.sleep_w_tlm(0.5)
    tlm_dict = mr_max.send_maxwell_message(SET_HPISO_PWM_RAW, [0x00, 0x00])
    mr_max.sleep_w_tlm(0.5)

    tlm_dict = mr_max.send_maxwell_message(SET_LPISO_PWM_RAW, [0x80, 0x00])
    mr_max.sleep_w_tlm(0.5)
    tlm_dict = mr_max.send_maxwell_message(SET_LPISO_PWM_RAW, [0x40, 0x00])
    mr_max.sleep_w_tlm(0.5)
    tlm_dict = mr_max.send_maxwell_message(SET_LPISO_PWM_RAW, [0x20, 0x00])
    mr_max.sleep_w_tlm(0.5)
    tlm_dict = mr_max.send_maxwell_message(SET_LPISO_PWM_RAW, [0x10, 0x00])
    mr_max.sleep_w_tlm(0.5)
    tlm_dict = mr_max.send_maxwell_message(SET_LPISO_PWM_RAW, [0x08, 0x00])
    mr_max.sleep_w_tlm(0.5)
    tlm_dict = mr_max.send_maxwell_message(SET_LPISO_PWM_RAW, [0x04, 0x00])
    mr_max.sleep_w_tlm(0.5)
    tlm_dict = mr_max.send_maxwell_message(SET_LPISO_PWM_RAW, [0x00, 0x00])
    mr_max.sleep_w_tlm(0.5)

    tlm_dict = mr_max.send_maxwell_message(SET_PFCV_DAC_RAW, [0xFF, 0xFF])
    mr_max.sleep_w_tlm(0.5)
    tlm_dict = mr_max.send_maxwell_message(SET_PFCV_DAC_RAW, [0x7F, 0xFF])
    mr_max.sleep_w_tlm(0.5)
    tlm_dict = mr_max.send_maxwell_message(SET_PFCV_DAC_RAW, [0x3F, 0xFF])
    mr_max.sleep_w_tlm(0.5)
    tlm_dict = mr_max.send_maxwell_message(SET_PFCV_DAC_RAW, [0x1F, 0xFF])
    mr_max.sleep_w_tlm(0.5)
    tlm_dict = mr_max.send_maxwell_message(SET_PFCV_DAC_RAW, [0x0F, 0xFF])
    mr_max.sleep_w_tlm(0.5)
    tlm_dict = mr_max.send_maxwell_message(SET_PFCV_DAC_RAW, [0x07, 0xFF])
    mr_max.sleep_w_tlm(0.5)
    tlm_dict = mr_max.send_maxwell_message(SET_PFCV_DAC_RAW, [0x00, 0x00])
    mr_max.sleep_w_tlm(0.5)


'''
In this test we have to test every command a customer cappella can send. 
These commands are:

Go to Idle       = 0x0002
Go to Thrust     = 0x0014
Get Status       = 0x0028
Config Thrust    = 0x0032
Set UTime        = 0x003C
Clear Errors     = 0x0046
Set_Auto_Status  = 0x0050
Get SW Info      = 0x005A
Set Next App     = 0x0070  
Abort            = 0x00FF


We also have this command that can come back to the satellite.
RX_AUTO_STATUS_PCU

With these command and responses we are going to make a test for each of them via a function.
besides having a focus of a message per test we will try to use all the commands as much as we can like for redundancy purposes.
The goal is to limit our commands like are a customer of maxwell.
'''

def main():
    if not check_if_hardware_is_connected_and_maxapi_and_firmware_is_sync():
        return
    print("Running basic customer command test")
    # We are first we going to restart the systems since they already have unix timestamp and we want to test that specifcally.
    # We went through mr_maxwell to get the info of the system as well to confirm the api can be in sync with the unit.
    reset_wdt_and_confirm_startup()
    if not test_set_utime():
        return False
    print("Set U Time test passed.")
    # Test passed lets get auto status rolling.
    if not test_set_auto_status():
        print("Set auto status test failed")
        return False
    print("Set auto status test passed.")
    print("Testing IDLE")
    if not test_idle_command():
        print("Test Idle test failed")
        return False
    print("Idle Test Passed")
    print("Testing Config Thrust")
    if not test_config_thrust():
        print("Test config thrust failed")
        return False
    print("Config Thrust Test Passed")
    print("Testing Abort")
    if not test_abort():
        print("Abort Test failed")
    print("Abort Test Passed")

    print("Testing Clear Errors")
    if not test_clear_errors():
        print("Clear Error Test Failed")
    print("Clear Error Test Passed")

    if not get_software_info_test("pcu_sw_0.5.5-28-g91191a36"):
        print("Get Software Test Failed")
    print("Get Software Test Passed")

    while True:
        time.sleep(3)
        print("Getting Data...")

def test_idle_command():
    heat_setpoint = 35.0
    mr_max.goto_idle()
    status_out = mr_max.get_status()
    if not max_check.check_maxwell_state(status_out, "IDLE"):
        print("Maxwell is not in IDLE")
        return False

    idle_status = check_if_idle_is_complete()
    if not idle_status:
        return False
    status_pkt = mr_max.get_status()

    ttank = status_pkt['parsed_payload']['TTANK']
    if ttank <= heat_setpoint:
        print("Test Failed it did not hit to it's highest heat value.")
        print("The current tank value is " + str(ttank) + "C")
        return False
    mr_max.abort()
    time.sleep(.250)
    if not validate_no_errors_after_test():
        print("At the end of the test we received errors that were not expected.")
        return False
    return True

    

def test_thrust_command():
    return True
    # mr_max.goto_idle()

def test_get_status():
    output = mr_max.get_status()
    if not max_check.confirm_command_status(output):
        return False
    if not validate_no_errors_after_test():
        print("At the end of the test we received errors that were not expected.")
        return False
    return True


def get_software_info_test(software_version_number):
    sw_info_response = mr_max.get_sw_info("Current_App", MaxConst.PROP_CONTROLLER_ADDRESS)
    if sw_info_response["status"] == "STATUS_SUCCESS":
        parsed_sw_info = parse_maxwell_header(sw_info_response, "Current Application")
        parsed_firmware = p4_version_parser(parsed_sw_info["software_version"])
        print(parsed_firmware)
        if parsed_sw_info["software_version"] != software_version_number:
            print("The software version was not the same")



def test_config_thrust():
    # Lets check this in the Low Power State
    if not send_and_check_thrust_configuration():
        print("Test Config Thrust Failed in Low Power State")
        return False
    # Lets check this command again in the IDLE state while it is "busy".
    mr_max.goto_idle()
    if not send_and_check_thrust_configuration():
        print("Test Config Thrust Failed in Idle State while heating or busy.")
        mr_max.abort()
        return False

    # Lets check this command again in the IDLE state after it finished heating.
    if not check_if_idle_is_complete():
        print("Error when waiting to heat up in IDLE.")
        mr_max.abort()
        return False
    if not send_and_check_thrust_configuration():
        print("Test Config Thrust Failed in Idle State when finish heating.")
        mr_max.abort()
        return False
    mr_max.abort()
    time.sleep(.250)
    if not validate_no_errors_after_test():
        print("At the end of the test we received errors that were not expected.")
        return False

    return True




def test_set_utime():
    # We should only need to send one command for both prop controller and thruster controller to get the unix timestamp.
    output = mr_max.set_utime(MaxConst.PROP_CONTROLLER_ADDRESS)
    time.sleep(.500) #TODO this should not be this long but at .250 it does not work all the time.
    if not max_check.confirm_command_status(output):
        return False
    # The true customer way is just to get the prop controller but we are getting both to verify they both got the unix timestamp.
    # TODO might change this to only prop to be a satellite commands only.
    prop_status = mr_max.get_status(MaxConst.PROP_CONTROLLER_ADDRESS)
    thrust_status = mr_max.get_status(MaxConst.THRUSTER_CONTROLLER_ADDRESS)
    if not max_check.check_unix_timestamp_is_set(prop_status,thrust_status):
        print("Utime is not synced up. Test Failed")
        return False
    if not validate_no_errors_after_test():
        print("At the end of the test we received errors that were not expected.")
        return False

    return True

def test_clear_errors():
    # Sending 5 bulk errors with a total of 45 errors
    mr_max.goto_manual() # Going to manual to write some errors.
    for x in range(1, 6):
        send_bulk_error(x)
    mr_max.goto_safe()
    status_pkt = mr_max.get_status()
    if not max_check.is_spacecraft_error_tlm_full(status_pkt):
        print("Not enough errors when writing errors. Clear Error Test Failed")
    mr_max.clear_errors()
    time.sleep(.250)
    status_pkt = mr_max.get_status()
    if not max_check.confirm_command_status(status_pkt):
        print("Did not clear errors correctly. Clear Error Test Failed")
    if not validate_no_errors_after_test():
        print("At the end of the test we received errors that were not expected.")
        return False
    return True



def test_set_auto_status():
    list_of_auto_tlm_freq = [1000,500,200,2000]
    for freq in list_of_auto_tlm_freq:
        test_results = test_auto_status_packet_amount(freq)
        if not test_results:
            return False
    # Setting it back to 1000 ms so we can use for the other tests.
    mr_max.set_auto_status_enable(1000)
    if not validate_no_errors_after_test():
        print("At the end of the test we received errors that were not expected.")
        return False
    return True


def test_set_next_app():
    if not set_and_check_app(2):
        print("Set Next App Failed for app 2")
    if not set_and_check_app(3):
        print("Set Next App Failed for golden image")




def test_abort():
    mr_max.goto_idle()
    status_pkt = mr_max.get_status()
    # Checking Abort by first going to IDLE.
    if not max_check.check_maxwell_state(status_pkt, "IDLE"):
        print("Maxwell is not in IDLE when it should be.")
        return False
    mr_max.goto_safe()
    time.sleep(.250)
    status_pkt = mr_max.get_status()
    if not max_check.check_maxwell_state(status_pkt, "LOW_PWR"):
        print("Maxwell is not in LOW PWR, when it should be after safing.")
        return False

    # Checking Abort by staying in low power and then aborting to safe..
    status_pkt = mr_max.goto_safe()
    if not max_check.check_maxwell_state(status_pkt, "SAFE"):
        print("Maxwell is not in Safe right after safing.")
        return False
    time.sleep(.250)
    status_pkt = mr_max.get_status()
    if not max_check.check_maxwell_state(status_pkt, "LOW_PWR"):
        print("Maxwell is not in LOW PWR, when it should be after safing.")
        return False

    if not validate_no_errors_after_test():
        print("At the end of the test we received errors that were not expected.")
        return False
    return True

    # Checking Abort by going to thrust and then aborting to safe.
    # mr_max.goto_idle()
    # 
    # start_time = time.time()
    # while True:
    #     status_pkt = mr_max.get_status()
    #     busy_flag = status_pkt['parsed_payload']['BUSY_FLAG']
    #     if busy_flag == 0:
    #         print('System is no longer in busy state, Heater operations finished')
    #         ttank = status_pkt['parsed_payload']['TTANK']
    #         print('TTANK is reading', ttank, 'Celsius')
    #         break
    #     elif time.time() - start_time > 60:
    #         print('Still Heating...')
    #         start_time = time.time()
    #         ttank = status_pkt['parsed_payload']['TTANK']
    #         print('TTANK is reading', ttank, 'Celsius')
    # # We are good for thrust let er rip.
    # mr_max.goto_thrust()
    # # Get into the thrust state. and stay for a little bit
    # time.sleep(.100)
    # if not max_check.check_maxwell_state(status_out, "THRUST"):
    #     print("Maxwell is not in Thrust, when it should be")
    #     #We need to get out of thrust asap
    #     for x in range(3):
    #         mr_max.abort()
    #     return False
    # mr_max.goto_safe()
    # time.sleep(.250)
    # if not max_check.check_maxwell_state(status_out, "LOW_PWR"):
    #     print("Maxwell is not in LOW PWR, when it should be after safing.")
    #     return False
    

def send_bulk_error(error_id):
    for x in range(9):
        mr_max.send_maxwell_message(MaxCmd.WRT_ERR, [0x00, error_id, 0xFF])

def send_and_check_thrust_configuration():
    rf_voltage = 0.05
    sscm = round(random.uniform(1, 50), 3)
    duration = round(random.uniform(1, 30), 3)
    pt_status = mr_max.config_thrust(rf_voltage, sscm, duration)
    if not max_check.confirm_command_status(pt_status):
        print("Test Config Thrust Failed. Did not send data correctly.")
        return False
    pt_status = mr_max.get_status()
    if not max_check.confirm_thrust_settings(pt_status, rf_voltage, sscm, duration):
        print("Test Config Thrust Failed. Thrust configuration data that was not sent was not reported.")
        return False
    return True


def check_if_idle_is_complete():
    start_time = time.time()
    while True:
        status_pkt = mr_max.get_status()
        busy_flag = status_pkt['parsed_payload']['BUSY_FLAG']
        if busy_flag == 0:
            print('System is no longer in busy state, Heater operations finished')
            ttank = status_pkt['parsed_payload']['TTANK']
            print('TTANK is reading', ttank, 'Celsius')
            break
        elif time.time() - start_time > 60:
            print('Still Heating...')
            start_time = time.time()
            ttank = status_pkt['parsed_payload']['TTANK']
            print('TTANK is reading', ttank, 'Celsius')
        # If it takes more the 20 mins something is wrong and we should figure out why. (THIS IS FOR ATMOSPHERE) TODO UPDATE TO A BETTER NUMBER IF NEED BE.
        elif time.time() - start_time > (60 * 20):
            ttank = status_pkt['parsed_payload']['TTANK']
            print("Test Failed: Took to long to heat.")
            print("Current temp is: " + str(ttank))
            return False
    return True

#This function will validate the amount of packets we have recived after 10 seconds to see if the frequency is correct.
def test_auto_status_packet_amount(tlm_rate_ms):
    auto_tlm_noise_margin = 1  # This is margin where depending on when the data was captured and fast it sends there can be + - noise margin that we will consider a pass.
    TIME_TO_SLEEP_MS = 10000
    TIME_TO_SLEEP_SECONDS = TIME_TO_SLEEP_MS/1000 # 10 seconds
    number_of_expected_rx_counts = int (TIME_TO_SLEEP_MS/tlm_rate_ms)
    mr_max.set_auto_status_enable(tlm_rate_ms)
    time.sleep(TIME_TO_SLEEP_SECONDS)
    auto_tlm_count = mr_max.get_auto_tlm_rx_count()
    mr_max.set_auto_status_disable()
    if (auto_tlm_count > (number_of_expected_rx_counts + auto_tlm_noise_margin)) or (auto_tlm_count < (number_of_expected_rx_counts - auto_tlm_noise_margin)):
        print("Auto Tlm Test Failed. For auto tlm rate of " + str(tlm_rate_ms))
        print("Expected Tlm amount was " + str(number_of_expected_rx_counts))
        print("Tlm amount received was " + str(auto_tlm_count))
        return False
    return True


def set_and_check_app(select_app):
    DEFAULT_RESET_APP = 1
    mr_max.set_next_app(select_app)
    time.sleep(.250)
    reset_wdt_and_confirm_startup()
    if not check_app_select_tlm_value(select_app):
        print("app_select_test_failed.")
        return False
    if not validate_no_errors_after_test():
        print("There are error running app_select_test_failed.")
        return False
    reset_wdt_and_confirm_startup()
    if not check_app_select_tlm_value(DEFAULT_RESET_APP):
        print("app_select_test_failed.")
        return False
    if not validate_no_errors_after_test():
        print("There are error running app_select_test_failed.")
        return False
    return True

def validate_no_errors_after_test():
    prop_tlm = mr_max.get_tlm(MaxConst.PROP_CONTROLLER_ADDRESS)
    thrust_tlm = mr_max.get_tlm(MaxConst.THRUSTER_CONTROLLER_ADDRESS)
    if max_check.confirm_command_tlm(prop_tlm) and max_check.confirm_command_tlm(thrust_tlm,tx_mask=MaxConst.THRUSTER_CONTROLLER_ADDRESS):
        return True
    return False

# Even though we are running a test when sending commands.
# We want to confirm that there is no errors at all when sending these commands.
def reset_wdt_and_confirm_startup():
    mr_max.stop_wdt(MaxConst.PROP_CONTROLLER_ADDRESS)
    time.sleep(2.5)
    mr_max.stop_wdt(MaxConst.THRUSTER_CONTROLLER_ADDRESS)
    time.sleep(2.5)
    output = mr_max.get_tlm(
        MaxConst.PROP_CONTROLLER_ADDRESS)  # Have to do get tlm for this check to work. It really needs to be status/telm agnostic.
    if not max_check.search_for_error_only(output, MaxErr.WATCHDOG_TRIP):
        print("There is a basic error after resetting the WDT. In select_app test")
        return False
    mr_max.clear_errors()
    output = mr_max.get_status(MaxConst.PROP_CONTROLLER_ADDRESS)
    if not max_check.confirm_command_status(output):
        print("There is a basic error after clearing errors.")
        return False

def check_if_hardware_is_connected_and_maxapi_and_firmware_is_sync():
    if not mr_max.is_prop_controller_connected():
        print("The Prop Controller is not connected. The Complete Maxwell unit is needed to run this test. Please connect the Prop Controller to run this test.")
        return False
    elif not mr_max.is_thruster_controller_connected():
        print("The Thruster Controller is not connected. The Complete Maxwell unit is needed to run this test. Please connect the Thruster Controller to run this test.")
        return False
    elif not mr_max.is_maxapi_and_firmware_sync():
        print( "The firmware is not synced to this maxapi. Please update the maxapi or firmware so they are completely sync before running this test.")
        return False
    else:
        return True

def check_app_select_tlm_value(app_selected):
    status_prop = mr_max.get_status(MaxConst.PROP_CONTROLLER_ADDRESS)
    status_thrust = mr_max.get_status(MaxConst.THRUSTER_CONTROLLER_ADDRESS)
    if status_prop["parsed_payload"]["APP_SELECT"] == app_selected and status_thrust["parsed_payload"]["APP_SELECT"] == app_selected:
        return True
    return False


if __name__== "__main__":
    main()
