import setuptools


with open("maxapi/maxapi_version.txt", "r") as vr:
    version_text_raw = vr.read()
    version_text = version_text_raw.strip()
    version_text_snip = version_text.replace("max_api_", "")  # Getting rid of the max_api_
    dash_placement = version_text_snip.find("-")

    if dash_placement == -1:
        # From here we have 0.3.0 It looks like we are doing parsing.
        parsed_version = version_text_snip
    else:
        # We are  going to replace the checkouts prior to tag as the feature version number.
        # For versioning pip. That way we can gauretntee the pip versioning will always work when updating.
        # --------------------------------------------------------------------
        # From here we have 0.3.0-5-g3f2cf15f
        # Splitting the version into 3 sections first is the x.x.x 
        # next is the number of checkouts prior to the tag or the -5- section 
        # Last is the git hash itself or the -g3f2cf15f
        # We are now going to replace the checkouts prior as the feature version number.
        version_list = version_text_snip.split("-") 
        dot_section_version = version_list[0] # The main section of the version x.x.x
        checkout_commit_version = version_list[1] # The checkout portion of the version

        # Now we are going to split the dot section for each repsective dot. 
        # That way we can overrite the feature section wih out dealing with string edge case scenrios. 
        # IE what if the feature length was 2 chars instead of one.
        dot_version_list =dot_section_version.split(".")

        # Setting the feature version to the number of checkouts since the last tag. 
        # This way we know exactly that there will never bee any duplicates and the pip version will always work.
        dot_version_list[2] = checkout_commit_version

        # Combining the string versions together to create the final versions.
        parsed_version = ""
        for version in dot_version_list:
            parsed_version = parsed_version + version  + "."
        
        parsed_version = parsed_version[:-1] # Removing the last . to complete the version.


    print("Paresed Version: " + parsed_version)

	
	
with open("README.md", "r") as fh:
    long_description = fh.read()

with open("requirements.txt", "r") as rq:
    requirements = rq.read()
    requirements_list_raw = requirements.split('\n')
    requirements_list= []
    for x in requirements_list_raw:
        if x != '':
            requirements_list.append(x)
    print(requirements_list)



setuptools.setup(
    name="maxapi",
    version=parsed_version,
    author="Andrew Thornborough",
    author_email="andrew.thornborough@phasefour.io",
    description="A api to talk to the maxwell thruster this git hash checkout is: " + str(version_text_raw),
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="http://phasefour.io",
    packages=setuptools.find_packages(),
    install_requires=requirements_list,
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.7',
    include_package_data=True,
    entry_points={
        'console_scripts': ['maxapi=maxapi.command_line:main'],
    },
)