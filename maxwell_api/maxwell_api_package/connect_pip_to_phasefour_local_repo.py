import sys
import subprocess
def main():
    subprocess.call("python -m pip install --upgrade pip") #
    python_path = sys.executable
    python_path_list = python_path.split('\\')
    pip_path = ""
    count = 0
    total_len = len(python_path_list)
    for x in python_path_list:
        if count != total_len-1:
            #If it is the top we do not want to start the string with a \ this way we start with the proper directory in this case it is C:\..\..\..\.. etc.
            if count == 0:
                pip_path = pip_path + str(x)
            else:
                pip_path = pip_path +"\\"+ str(x)
        count = count +1
    pip_path = pip_path + "\\" #Adding a directory at the end to add a file.
    print(pip_path)
    with open (str(pip_path + "pip.ini"),mode='w') as file:
        pip_file_contents  = generate_pip_config_string()
        file.write(pip_file_contents)
    print("\n\n----- Done! -----\n\n")


def generate_pip_config_string():
    pip_contents = "[global]\n" \
                   "extra-index-url = http://10.0.0.250:8080\n" \
                   "trusted-host = 10.0.0.250"
    return pip_contents
if __name__== "__main__":
    main()