import requests
import json
import argparse
import sys
import random
list_of_pictures = ["https://media1.giphy.com/media/2FQx0XQHVXaJq/giphy-downsized.gif?cid=6104955e996ce946bc9e8f3c76aa4a8bb9d1111b9c6d6bd6&rid=giphy-downsized.gif",
    "https://media0.giphy.com/media/TLyhdPMHc7s7S/giphy.gif?cid=6104955e8259b1530fbda6a3b4f4944c84036c5160ff69b6&rid=giphy.gif",
    "https://media3.giphy.com/media/MF1kR4YmC2Z20/giphy.gif?cid=6104955efb843294fe0c93f3a9acad965b7e22297470e019&rid=giphy.gif",
    "https://media0.giphy.com/media/o0vwzuFwCGAFO/giphy-downsized.gif?cid=6104955eb1000edae3b61424473a95f43772ee7d5c0cc7fa&rid=giphy-downsized.gif",
    "https://media2.giphy.com/media/yK3PyRmUj0T3W/giphy.gif?cid=6104955e167d5b917f46b900b2db5e25d064c40570d64c5e&rid=giphy.gif",
    "https://media0.giphy.com/media/MGaacoiAlAti0/giphy.gif?cid=6104955ef6684c1eda4b494f587c13537f7cc49cdf4282b8&rid=giphy.gif",
    "https://media1.giphy.com/media/YQitE4YNQNahy/giphy-downsized.gif?cid=6104955e2eddcedb6d20b4e1da0bb9a2beefc72b249ee7b3&rid=giphy-downsized.gif",
    "https://media2.giphy.com/media/LcfBYS8BKhCvK/giphy-downsized.gif?cid=6104955eac005d5ee3f734ef85b7fbe6cfa1751997e700b1&rid=giphy-downsized.gif",
    "https://media0.giphy.com/media/ZVik7pBtu9dNS/giphy.gif?cid=6104955eb38faadb17de2d9c6d5d5cc2e5ee4e3628cea397&rid=giphy.gif",
    "https://media3.giphy.com/media/gG6OcTSRWaSis/giphy-downsized.gif?cid=6104955eb502c6c297a887eb1408970ccadb5338804be307&rid=giphy-downsized.gif",
    "https://media3.giphy.com/media/TNf5oSRelTeI8/giphy.gif?cid=6104955e129229df139ff6330b47c9a801c82d066789deae&rid=giphy.gif",
    "https://media3.giphy.com/media/kwEmwFUWO5Ety/giphy-downsized.gif?cid=6104955e1c08d4d99a894719740660f5c5edb7ae01783b08&rid=giphy-downsized.gif",
    "https://media2.giphy.com/media/zOvBKUUEERdNm/giphy-downsized.gif?cid=6104955ea202535fa81ed5123c98b529a9b05099e03d05d6&rid=giphy-downsized.gif",
    "https://media0.giphy.com/media/3oKIPwoeGErMmaI43S/giphy-downsized.gif?cid=6104955e916876e85421316889203305d59150337ef21c30&rid=giphy-downsized.gif",
    "https://media3.giphy.com/media/l4Kic7adcKMoWSjbq/giphy.gif?cid=6104955e8ac08dcfc50c771d876af6083f486b3c2924fd33&rid=giphy.gif",
    "https://media2.giphy.com/media/kBCy2FRBVw39m/giphy.gif?cid=6104955e893be3c5cfcf6a3d507c933df2ea008d6a52f934&rid=giphy.gif",
    "https://media0.giphy.com/media/LAcLF2C7pyqPu/giphy.gif?cid=6104955efed9e627d39c4a004f98ebb9b0f78b2dfbb1babe&rid=giphy.gif",
    "https://media0.giphy.com/media/LlTYKN146VMyI/giphy.gif?cid=6104955e11ae7f556950e8edd0553a7754b1a65b289f8852&rid=giphy.gif"]



def notify_maxwell_software_users(notification_type):


    webhookurl = "https://hooks.slack.com/services/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"
    if notification_type == "fw_update":
        notification_message = "Greeting Earthlings!! :earth_americas: \n It is I the magnificent bender!\nI am here to tell you that a new firmware update is available for you!\n\n" \
        "You can update the firmware using the following command in the command line: `maxapi update_fw`\n"\
        "When you update make sure you update the Prop Controller *AND* the Thruster Controller.\n"\
        "If you have any questions ask a phasefour dev monkey. :monkey:"
    elif notification_type == "max_api":
        notification_message = "Greeting Earthlings!! :earth_americas: \n It is I the magnificent bender!\nI am here to tell you that there is a new maxapi update!\n\n" \
        "You can update maxapi by using the following command in the command line: `pip install maxapi -U`\n"\
        "If you have any questions ask a phasefour dev monkey. :monkey:"
    elif notification_type == "both":
       notification_message= "Greeting Earthlings!! :earth_americas: \n It is I the magnificent bender!\nI am here to tell you that there is a *BIG* update available for you!\n\n" \
        "This update requires you to update both the maxwell api *AND* the firmware of Maxwell.\n"\
        "If you only update one of the two compatibility is *NOT* guaranteed.\n\n"\
        "You can update maxapi by using the following command in the command line: `pip install maxapi -U`\n\n"\
        "You can update the firmware using the following command in the command line: `maxapi update_fw`\n"\
        "When you update the firmware make sure you update the Prop Controller *AND* the Thruster Controller.\n\n"\
        "If you have any questions ask a phasefour dev monkey. :monkey:"
    else:
        print("You somehow got to a state that is not possible. Talk to me how you did this. Shutting down")
        sys.exit()
    # post string to channel
    rand_index = random.randint(0,(len(list_of_pictures)-1))
    image_url = list_of_pictures[rand_index]
    attachments = [{"title": "A Cool Photo", "image_url": image_url}]
    code = requests.post(webhookurl, data=json.dumps({"text": notification_message, "attachments":attachments}))
    # Return code 200 is a success!
    return code


def main(argv=None):
        parser = argparse.ArgumentParser()
        parser.add_argument("-update_type", help="The type of update that you want to notify.",choices=["fw_update", "max_api","both"])
        args = parser.parse_args()
        if getattr(args, "update_type") == None:
            valid_choice = False
            while valid_choice == False:
                input_val = input("Please select one of the 3 options to post to the maxwell_software_users.\n"\
                "1) fw_update\n"
                "2) max_api\n"
                "3) both \n"
                "Input:")
                if input_val == "fw_update"  or input_val == "max_api" or input_val == "both" or input_val == "1" or input_val == "2" or input_val == "3":
                    if input_val == "1":
                        final_input = "fw_update"
                    elif input_val == "2":
                        final_input = "max_api"
                    elif input_val == "3":
                        final_input = "both"
                    else:
                        final_input = input_val
                    valid_choice = True
                else:
                    print("You selected a invalid option.\n")
        else:
            notification_selection = args.update_type
        
        print("The notification is: "+ str(final_input))
        notify_maxwell_software_users(final_input)
if __name__ == '__main__':
    main()
 
