# Maxwell Api

This is a package to talk to phasefour maxwell thruster.


To build and release a package do the following steps.

 1. Make the changes to the maxapi you want to see.

 2. Git commit changes

 3. Update tag if necessary (git tag -a; git push origin --tags). If you need to see the current tag type in (git describe --match "max*)

 4. Run the following bash file: (build_maxwell_api_package.bash) This will update the version.txt file and build the maxapi.

 5. If you want to test the build on your local machine go to the /dist sub directory and type in  (pip install .)

 6. Once verfied put both files from the dist/ directory (*.tar.gz, .whl) into bender's directory (\\10.0.0.250\phase_four_repo\packages\maxwell_api) They are now available for everyone 

 7. If you want to notify p4 of the sw change run post_update_to_slack.py

