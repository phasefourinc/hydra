# Third Party Imports
import struct
import math
import logging
import crcmod.predefined
import serial
import sys
import queue
import threading
import pathlib
import os
CRC_LENGTH = 2
CLANK_VERSION_SYNC_BYTE = 0x55
HEADER_LENGTH = 8
TIME_DELAY_BETWEEN_PACKETS = 0.005
DELIMITER_LENGTH = 2
DEFAULT_MESSAGE = 0xFFFF
DEFAULT_PKT_SEQ = 0xFFFF
UART_BAUD_RATE = 1000000


BENDER_COMPUTER_SOURCE = 0x0E
BENDER_COMPUTER_ADDRESS = 0x01 << BENDER_COMPUTER_SOURCE
PROP_CONTROLLER_SOURCE = 0x00
PROP_CONTROLLER_ADDRESS = 0x01 << PROP_CONTROLLER_SOURCE
THRUSTER_CONTROLLER_SOURCE = 0x01
THRUSTER_CONTROLLER_ADDRESS = 0x01 << THRUSTER_CONTROLLER_SOURCE

MAXWELL_CONTROLLER_ADDRESS = PROP_CONTROLLER_ADDRESS | THRUSTER_CONTROLLER_ADDRESS


class EmbeddedCommunicatorClass(serial.Serial):
    def __init__(self):
        super().__init__()
        self._sync_byte_version = CLANK_VERSION_SYNC_BYTE
        self._source_address = None
        self._transmit_mask = MAXWELL_CONTROLLER_ADDRESS
        self.update_transmit_mask = True
        self._pkt_seq_count = None
        self._msg_id = DEFAULT_MESSAGE
        self._pkt_data_len = 0x00
        self._time_delay_between_packets = TIME_DELAY_BETWEEN_PACKETS
        self._configured = False
        self._output = {
            "status": "DEFAULT_STATUS",
            "msg_id": DEFAULT_MESSAGE,
            "packet_seq": DEFAULT_PKT_SEQ,
            "payload": []
        }
        self.rx_data_queue = queue.Queue()
        self.start_read_thread = False
        self.read_thread = threading.Thread(target=self.read_data_thread)

    # Calling destructor
    def __del__(self):
        self.close_port()

    def setup_configuration(self,
                            baud_rate,
                            port,
                            source_address,
                            seq_count=0):
        self._source_address = source_address
        self._pkt_seq_count = seq_count
        self.baudrate = baud_rate
        self.port = port
        self.uart_setup(baud_rate, port)
        self._configured = True

    def uart_setup(self, baud_rate, port):
        if self.is_open:
            self.close()
        if baud_rate is None:
            baud_rate = UART_BAUD_RATE
            clank_logger.warning("buad_rate was sent to none.")
        if port is None:
            port = "Invalid Port"
            clank_logger.warning("port was sent to none.")
        self.baudrate = baud_rate
        self.port = port
        clank_logger.info("The Serial Port config information:  Baud: %s  Port: %s \n \n ", str(self.baudrate), str(self.port))

    def open_port(self):
        if(self.isOpen() == False):
            try:
                self.open()
            except serial.SerialException as e:
                #print(e)
                error_message = "The port you are trying to open ("+str(self.port)+")"+" is currently open or it is not connected. Check your comport and try again. SHUTTNG DOWN."
                print(error_message)
                sys.exit()
            else:
                self.enable_read_thread()
        else:
            print("Port: " + str(self.port)+ " is already open")

    def enable_read_thread(self):
        self.start_read_thread = True
        self.read_thread.setDaemon(True)
        self.read_thread.start()

    def disable_read_thread(self):
        self.start_read_thread = False
    def close_port(self):
        try:
            self.disable_read_thread()
            self.close()
        except serial.SerialException as e:
            clank_logger.error(f"Port Closing Error: {e}")
            sys.exit()


    def get_serial_properties(self):
        Output = {}
        Output["baud_rate"] = self.baudrate
        Output["current_port"] = self.port
        Output["source_address"] = self._source_address
        Output["transmit_mask"] = self._transmit_mask
        return Output

    def write_data(self, msg_id, data_packet_input, transmit_mask):
        output = _format_output()

        if self._configured is False:
            clank_logger.error("ERROR: Writing data prior to configuring. Returning and doing nothing")
            output["status"] = "IMPROPER_CLANK_CONFIG"
            return output

        if self.update_transmit_mask:
            self._transmit_mask = transmit_mask

        self._msg_id = msg_id
        self._pkt_data_len = len(data_packet_input)
        complete_packet = self._create_packet(msg_id, data_packet_input,transmit_mask)
        output["raw_clank_packet"] = complete_packet
        # Formating payload length
        payload_length = (len(complete_packet) - HEADER_LENGTH - CRC_LENGTH - DELIMITER_LENGTH)
        output["payload"] = _convert_to_int_array(complete_packet[HEADER_LENGTH:HEADER_LENGTH + payload_length])
        output["crc"] = ((complete_packet[HEADER_LENGTH + payload_length] << 8) + complete_packet[HEADER_LENGTH + payload_length + 1])
        received_header = _header_parse(complete_packet)
        output = {**output, **self._reporting_header_to_output(received_header)}  # Merging the dictionaries together
        # Writing the data
        self.write(complete_packet)
        output["status"] = "STATUS_SUCCESS"
        return output


    def read_data_thread(self):
        while self.start_read_thread:

            # If the clank protocol is not configured error out.
            if self._configured is False:
                clank_logger.error("ERROR: Writing data prior to configuring. Returning and doing nothing")
                continue
            clank_header_bytes = self.read(HEADER_LENGTH)
            if len(clank_header_bytes) != 0:

                clank_header_bytes = self.find_start_sync_byte(clank_header_bytes)
                clank_header = _header_parse(clank_header_bytes)
                payload = self.read(clank_header["PKT_DATA_LEN"] + CRC_LENGTH)
                complete_raw_clank_packet = clank_header_bytes + payload

                output = self.parse_clank_protocol(complete_raw_clank_packet)

                self.rx_data_queue.put(output)
            # serial_log.info("PUT: "+ str(self.rx_data_queue.qsize()))


    def find_start_sync_byte(self,header_bytes):
        while header_bytes[0] != CLANK_VERSION_SYNC_BYTE:
            Sync_Version_Byte_Position = header_bytes.find(CLANK_VERSION_SYNC_BYTE)
            if Sync_Version_Byte_Position == -1:
                header_bytes = self.read(HEADER_LENGTH)
            else:
                additional_bytes_read = Sync_Version_Byte_Position
                extra_bytes = self.read(additional_bytes_read)
                header_bytes = header_bytes[Sync_Version_Byte_Position:] + extra_bytes

        return header_bytes

    def parse_clank_protocol(self, raw_clank_packet):
        output = _format_output()
        # Verify the packet was received with the correct amount of bytes
        output["status"] = _verify_received_packet(raw_clank_packet)
        if output["status"] != "STATUS_SUCCESS":

            if len(raw_clank_packet) >= 1:
                output["raw_clank_packet"] = _convert_to_int_array(raw_clank_packet)
            clank_logger.warning("Putting In Embedded QUEUE " + str(output))
            return output

        # Parse the CLANK packet
        output["raw_clank_packet"] = _convert_to_int_array(raw_clank_packet)
        payload_length = (len(raw_clank_packet) - HEADER_LENGTH - CRC_LENGTH)
        output["payload"] = output["raw_clank_packet"][HEADER_LENGTH:HEADER_LENGTH + payload_length]
        received_header = _header_parse(raw_clank_packet)
        output["status"], output["crc"] = _check_crc(raw_clank_packet, payload_length)

        # If the CRC passed we want to check if the rest of the header is bad. If not skip because we found our first error.
        if output["status"] == "STATUS_SUCCESS":
            output["status"] = self.check_header(received_header, payload_length)
        else:
            clank_logger.warning("CRC Error? The payload is " + str(received_header))

        return {**output, **self._reporting_header_to_output(received_header)}  # Merging the dictionaries together

    def read_data(self):
        try:
            return_val = self.rx_data_queue.get()
        except queue.Empty:
            return_val = {"status": "EMBEDDED_TIMEOUT"}
        # serial_log.info("GET: "+ str(self.rx_data_queue.qsize()))
        return return_val

    def is_rx_buff_empty(self):
        return self.rx_data_queue.empty()

    def transmit_check_enable (self, enable):
        if enable == False:
            self._transmit_mask = MAXWELL_CONTROLLER_ADDRESS
            self.update_transmit_mask = False
        else:
            self._transmit_mask = None
            self.update_transmit_mask = True

    def _reporting_header_to_output(self,clank_header):
        header_output = {"msg_id": clank_header["MSG_ID"],
                         "packet_seq": clank_header["PKT_SEQ_COUNT"],
                         "data_length": clank_header["PKT_DATA_LEN"],
                         "source_address": clank_header["SOURCE_ADDRESS"],
                         "transmit_mask": clank_header["TRANSMIT_MASK"],
                         "sync_byte_version": clank_header["SYNC_BYTE_VERSION"]
                         }
        return header_output

    def _create_header(self, msg_id, data_packet_len, transmit_mask = None):
        if transmit_mask == None:
            clank_logger.error("The transmit mask is set to NONE this is a flaw with the program. Shutting down")
            sys.exit()
        self._pkt_data_len = data_packet_len
        header = [self._sync_byte_version, self._source_address, transmit_mask >> 8, transmit_mask & 0xFF, self._pkt_seq_count, msg_id >> 8, msg_id & 0xFF, self._pkt_data_len]
        self._pkt_seq_count = _update_pkt_seq_count(self._pkt_seq_count)
        return header

    def _create_packet(self, msg_id, data_packet, transmit_mask):
        # Getting Header Data
        header = self._create_header(msg_id, len(data_packet), transmit_mask)

        # Combining Header and Data packets and converting it to 1 byte stream.
        header_and_data = header + data_packet
        header_and_data = _convert_to_byte_stream(header_and_data)

        # Creating 1 byte CRC Value
        crc_value = _convert_to_byte_stream(_calculate_crc(header_and_data))

        # Combining into 1 packet and returning that packet.
        complete_packet = header_and_data + crc_value + [0xD] + [0xA]
        return complete_packet

    def check_header(self, header_info, data_len):
        if header_info["SYNC_BYTE_VERSION"] != CLANK_VERSION_SYNC_BYTE:
            clank_logger.error("Error in Sync Byte the sync byte values was: %s",str(header_info["SYNC_BYTE_VERSION"]))
            status = "SYNC_BYTES_ERROR"

            # Checking if the RX Source Address matches our transmit mask. If it does not, the command was not expected and should report a error
        elif ((1 << (header_info["SOURCE_ADDRESS"]))& self._transmit_mask) == 0:
            clank_logger.error("Error in Source Address. The Source Address value was: %s",str(header_info["SOURCE_ADDRESS"]))
            status = "SOURCE_ADDRESS_ERROR"

            # If the transmit mask equals our source address or if it is meant for us.
        elif (header_info["TRANSMIT_MASK"] & (0x01 << self._source_address)) != (0x01 << self._source_address):
            clank_logger.error("Error in Transmit_Mask The Transmit Mask value was: %s",str(header_info["TRANSMIT_MASK"]))
            status = "TRANSMIT_MASK_ERROR"

        elif header_info["PKT_DATA_LEN"] != data_len:
            clank_logger.error("Error in Pkt_Data_Len The Data length was: %s",str(header_info["PKT_DATA_LEN"]))
            status = "PKT_DATA_LEN_ERROR"
        else:
            status = "STATUS_SUCCESS"

        return status


def _verify_received_packet(return_val):
    if not return_val:
        output = "EMBEDDED_TIMEOUT"

    elif len(return_val) < HEADER_LENGTH + CRC_LENGTH:
        output = "NOT_ENOUGH_BYTES"
        clank_logger.error("Not enough data came in for a complete packet writing a error to indicating not enough bytes. The Data coming in was: \n%s",str(return_val))

    else:
        output = "STATUS_SUCCESS"

    return output


def _update_pkt_seq_count(pkt_seq_count):
    pkt_seq_count = pkt_seq_count + 1
    if pkt_seq_count >= 256:
        pkt_seq_count = 0
    return pkt_seq_count


def _check_crc(return_val, payload_length):
    status = "STATUS_SUCCESS"
    header_and_data = return_val[:HEADER_LENGTH + payload_length]
    calculated_crc_packet = _calculate_crc(header_and_data)
    calculated_crc = (calculated_crc_packet[0] << 8) + calculated_crc_packet[1]
    received_crc = ((return_val[HEADER_LENGTH + payload_length] << 8) + return_val[HEADER_LENGTH + payload_length + 1])
    if received_crc != calculated_crc:
        clank_logger.error("CRC Mismatch The received CRC was %s. The calculated CRC was %s. The input value was: \n%s",str(received_crc),str(calculated_crc),str(return_val[:HEADER_LENGTH + payload_length]))
        status = "COM_RX_CRC_ERROR"

    return status ,received_crc


def _calculate_crc(data_input):
    crc16 = crcmod.predefined.Crc('crc-ccitt-false')
    byte_data = bytes(data_input)
    crc16.update(byte_data)
    crc_value = crc16.crcValue
    crc_packet = [crc_value >> 8, crc_value & 0xFF]
    return crc_packet


def _header_parse(header_data):
    header_info = {
        "SYNC_BYTE_VERSION": header_data[0],
        "SOURCE_ADDRESS": header_data[1],
        "TRANSMIT_MASK": ((header_data[2] << 8) + header_data[3]),
        "PKT_SEQ_COUNT": header_data[4],
        "MSG_ID": ((header_data[5] << 8) + header_data[6]),
        "PKT_DATA_LEN": header_data[7]
    }

    return header_info



def _convert_to_int_array(input_bytes):
    output = []
    for x in input_bytes:
        output.append(x)
    return output

def _format_output():
    output = {
        "status": "DEFAULT_STATUS",
        "payload": [],
        "raw_clank_packet": []
    }
    return output

def _create_data_packets(data_input):
    data_packet_chunks = []
    internal_buffer = []
    count = 0
    while count < len(data_input):
        if (len(data_input) - count) <= 255:
            internal_buffer.extend(data_input[count:count + len(data_input)])
            count = count + len(data_input)
        else:
            internal_buffer.extend(data_input[count:count + 255])
            count = count + 255
        if len(internal_buffer) == 255 or count == len(data_input):
            data_packet_chunks.append(internal_buffer)
            internal_buffer = []
    return data_packet_chunks


def _convert_to_byte_stream(input_data):
    output = []
    if isinstance(input_data, int):
        try:
            val_size = math.ceil(
                input_data.bit_length() / 8)  # Calculate how many "8 bits" sections we need write (1 = 8 bit, 2 = 16 bit, etc..)
            if val_size == 0:  # If the value itself is 0 the val_size is zero. However we still want to print out the 0
                val_size = 1
            if val_size == 3:  # If the value is a 24 bit cast to 32.
                val_size = 4
            if 4 < val_size < 7:  # If the value is a more then 32 bits  but less then 64 cast to 64.
                val_size = 8
            if val_size > 8:
                raise ValueError()
        except ValueError:
            clank_logger.error("Error: The value that is being converted for the MCU is over 64 bits. Please input a file less than 64 bits. Skipping conversion")
            return input_data
        for x in range(val_size, 0, -1):
            shift_val = input_data >> ((x - 1) * 8) & 0xFF
            output.append(shift_val)
        return output

    elif isinstance(input_data, float):
        clank_logger.debug("In Float")
        byte_val = bytearray(struct.pack("f", input_data))
        for y in range(len(byte_val) - 1, -1, -1):
            output.append(byte_val[y])
        return output

    elif isinstance(input_data, str):
        clank_logger.debug("In String")
        my_str_as_bytes = str.encode(input_data)
        for y in my_str_as_bytes:
            output.append(y)
        return output

    elif isinstance(input_data, list):
        if not input_data:
            clank_logger.warning("The input being converted was empty nothing was converted")
            return input_data
        clank_logger.debug("Input List %s", len(input_data))
        for val in input_data:
            if isinstance(val, int):
                try:
                    val_size = math.ceil(val.bit_length() / 8)
                    if val_size == 0:  # If the value itself is 0 the val_size is zero. However we still want to print out the 0
                        val_size = 1
                    if val_size == 3:  # If the value is a 24 bit cast to 32.
                        val_size = 4
                    if 4 < val_size < 7:  # If the value is a more then 32 bits  but less then 64 cast to 64.
                        val_size = 8
                    if val_size > 8:
                        raise ValueError()
                except ValueError:
                    clank_logger.error("Error: The value that is being converted for the MCU is over 64 bits. Please input a file less than 64 bits. Skipping conversion")
                    return input_data
                for x in range(val_size, 0, -1):
                    shift_val = val >> ((x - 1) * 8) & 0xFF
                    output.append(shift_val)
            elif isinstance(val, float):
                clank_logger.debug("In List Float")
                byte_val = bytearray(struct.pack("f", val))
                for y in range(len(byte_val) - 1, -1, -1):
                    output.append(byte_val[y])
            elif isinstance(val, str):
                clank_logger.debug("In List Str")
                my_str_as_bytes = str.encode(val)
                for y in my_str_as_bytes:
                    output.append(y)

        return output
    else:
        clank_logger.error("Data format not supported did not convert: %s", input_data)
        return input_data



def main():
    clank_logger.debug("Starting")
    carol = EmbeddedCommunicatorClass()
    clank_logger.debug("Starting")
    data_input = [2, 1]
    carol.write_data(0x1, data_input)
    clank_logger.debug("Done")


def setup_logger(name, log_file, level=logging.INFO):
    """Function setup as many loggers as you want"""
    formatter = logging.Formatter('%(asctime)s.%(msecs)03d %(levelname)s: %(message)s')
    handler = logging.FileHandler(log_file, mode='w')
    handler.setFormatter(formatter)

    logger = logging.getLogger(name)
    logger.setLevel(level)
    logger.addHandler(handler)

    return logger


current_directory = os.path.dirname(os.path.abspath(__file__))
pathlib.Path(os.path.join(current_directory,"logs","")).mkdir(parents=True, exist_ok=True)
serial_log = setup_logger('queue_log',os.path.join(current_directory, "logs","API_QUEUE.log"))
clank_logger = setup_logger('maxwell_api',os.path.join(current_directory, "logs","maxwell_api.log"))


if __name__ == "__main__":
    main()
