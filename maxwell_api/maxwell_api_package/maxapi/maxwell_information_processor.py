import struct
from collections import namedtuple
from dataclasses import dataclass
from typing import Optional
import re
NUMBER_OF_SECTORS = 32
STATUS_SUCCESS = 0

SOFTWARE_FIELD_NAME = ["Flash Application 1", "Flash Application 2", "Golden Image Application", "Bootloader 1",
                       "Bootloader 2"]
P4VersionInfo = namedtuple("P4VersionInfo", ["label", "major", "minor", "patch", "commit_dif", "git_hash"])
P4VERSIONREGEX =  r'^(\w+)_(\d+)\.(\d+)\.(\d+)(?:-(\d+))?(-[a-zA-Z0-9]+)?$'

# #A place to store The Flash header
# @dataclass
# class P4FlashHeader:
P4VersionInfo = namedtuple("P4VersionInfo", ["label", "major", "minor", "patch", "commit_dif", "git_hash"])
# Explanation of the regex pattern:
# ^                 Start of the string
# \w+               Match one or more word characters (tag)
# _                 Match underscore character
# \d+\.\d+\.\d+      Match major.minor.patch (digits separated by dots)
# (-\d+)?           Match optional -number (hyphen and digits)
# (-[a-zA-Z0-9]+)?  Match optional -githash (hyphen and alphanumeric characters)
# $                 End of the string

'''
Higher Level functions involving writing. Like read and verifying it is correct.

'''

'''
A Function that collects all the software information from a phase four hardware system.

'''
def get_all_the_software_info(mr_max, hardware_select):
    software_field_name = [("Current_App","Current Application"),
                           ("Boot_1","Bootloader 1"),
                           ("Boot_2","Bootloader 2"),
                           ("Flash_App_1","Flash Application 1"),
                           ("Flash_App_2","Flash Application 2"),
                           ("Gold_Img","Golden Image Application"),
                           ("Parm_Gold","Golden Image Parm"),
                           ("Parm_App_1","Parm App1"),
                           ("Parm_App_2","Parm App2"),
                           ("Current_Parm","Current Parm")]

    all_software_info = []
    for sw_input_arg, field_name  in software_field_name:
        get_sw_info = mr_max.get_sw_info(sw_input_arg, transmit_mask=hardware_select)

        if get_sw_info["status"] == "STATUS_SUCCESS":
            parsed_sw_info = parse_maxwell_header(get_sw_info, field_name)
            if field_name !="Current Application":
                parsed_sw_info.pop('boot_select', None)
                parsed_sw_info.pop('app_select', None)
            all_software_info.append(parsed_sw_info)
        else:
            break

    return all_software_info, get_sw_info["status"]

'''
A Function that collects the current firmware application information
'''
def get_current_app_info(mr_max, hardware_select)-> dict:
    return read_and_parse_software_record("Current_App","Current Application", mr_max, hardware_select)


'''
A Function that collects one of the software records from a p4 hardware system.
'''
def read_and_parse_software_record(sw_input_arg,field_name, mr_max, hardware_select):
    get_sw_info = mr_max.get_sw_info(sw_input_arg, transmit_mask=hardware_select)

    if get_sw_info["status"] == "STATUS_SUCCESS":
        parsed_sw_info = parse_maxwell_header(get_sw_info, field_name)
        return parsed_sw_info
    else:
        return None

'''
Functions to read software information in the maxwell unit.

'''
def parse_maxwell_header(sw_info_response, field_name):
    software_version_raw_string = sw_info_response["raw_payload"][2:]  # First two bytes are the ack status.
    parsed_info = _parse_software_record(software_version_raw_string)
    parsed_info["field_name"] = field_name
    return parsed_info



'''
Functions to read maxwell hardware record
'''

def read_hardware_maxwell_record(incoming_maxwell_info,flash_offset):
    # Initalizing the data struct
    maxwell_hardware_record = {
        "field_name": "null",
        "part_number": "null",
        "revision": "null",
        "serial_number": "null",
        "flash_offset": None
    }

    incoming_maxwell_info_bytes = bytearray(incoming_maxwell_info)
    #Checking if the data is valid
    for x in incoming_maxwell_info_bytes:
        if x > 0x7F:
            # _process_errors(3,[])
            return maxwell_hardware_record

    # It is assumed here that the data is valid. Starting to decode
    string_maxwell_info = str(incoming_maxwell_info_bytes.decode("utf-8"))
    pos_1 = string_maxwell_info.find(':') + 1
    pos_2 = string_maxwell_info.find(',')
    count = 0
    set_of_string = []
    while count < 4:
        set_of_string.append(str(string_maxwell_info[pos_1:pos_2]))
        pos_1 = string_maxwell_info.find(':', pos_2) + 1
        pos_2 = string_maxwell_info.find(',', pos_1)
        count = count + 1
    maxwell_hardware_record["field_name"] = set_of_string[0]
    maxwell_hardware_record["part_number"] = set_of_string[1]
    maxwell_hardware_record["revision"] = set_of_string[2]
    maxwell_hardware_record["serial_number"] = set_of_string[3]
    maxwell_hardware_record["flash_offset"] = flash_offset

    return maxwell_hardware_record


'''
Functions to write maxwell hardware record
'''


def process_maxwell_record(field_name, part_number, revision, serial_number, offset_location):
    status, byte_arr = _create_maxwell_record(field_name, part_number, revision, serial_number, offset_location)
    if status != STATUS_SUCCESS:
        _process_errors(status, byte_arr)
    else:
        return byte_arr[0]


def _create_maxwell_record(field_name, part_number, revision, serial_number, offset_location):
    FIELD_NAME_WIDTH = 48
    PART_NUMBER_WIDTH = 32
    REVISION_WIDTH = 16
    SERIAL_WIDTH = 32
    field_width_arr = [FIELD_NAME_WIDTH, PART_NUMBER_WIDTH, REVISION_WIDTH, SERIAL_WIDTH]
    field_string_arr = _format_strings(field_name, part_number, revision, serial_number)
    count = 0
    if (offset_location >= NUMBER_OF_SECTORS):
        return 1, [offset_location]
    byte_array_field = bytearray([offset_location])
    while count < len(field_width_arr):
        extra_bytes_to_append = field_width_arr[count] - len(field_string_arr[count])
        if (extra_bytes_to_append < 0):
            return 2, [field_string_arr[count], field_width_arr[count]]
        byte_array_field.extend(field_string_arr[count].encode("utf-8"))
        inter_count = 0
        while inter_count < extra_bytes_to_append:
            byte_array_field.append(0xFF)
            inter_count = inter_count + 1
        count = count + 1

    return STATUS_SUCCESS, [byte_array_field]


def _format_strings(field_name, part_number, revision, serial_number):
    field_strings_arr = []
    field_strings_arr.append("FN:" + field_name + ",")
    field_strings_arr.append("PN:" + part_number + ",")
    field_strings_arr.append("RV:" + revision + ",")
    field_strings_arr.append("SN:" + serial_number + ",")
    return field_strings_arr


'''
This function parses the data from maxwell application header every software application has this header.
'''
import struct
class ParsedData:
    def __init__(self, data:bytes, encoding_scheme = "utf-8"):
        # This string decoding is taking the first 4 bytes [0:4] reverse it [::-1] and then decoding it.
        self.software_image_type = data[0:4][::-1].decode(encoding_scheme) # Str - 4 Bytes
        self.software_length = hex(struct.unpack("<I", data[4:8])[0]) # Unsigned Int - 4 Bytes
        self.software_crc = hex(struct.unpack("<I", data[8:12])[0]) # Unsigned Int - 4 Bytes
        self.flash_reserved_1 =  hex(struct.unpack("<I", data[12:16])[0]) # Unsigned Int - 4 Bytes") # Str - 32 Bytes
        self.software_version =   self.convert_p4_bytes_to_str(data[16:48]) # Str - 32 Bytes We are not doing the decode since there is a bit of 2 little endian going on.
        self.hardware_type =    self.convert_p4_bytes_to_str(data[48:52]) # Str - 4 Bytes
        self.product_type =   self.convert_p4_bytes_to_str(data[52:56]) # Str - 4 Bytes
        # Since this is not 4 bytes or conventinal size lets just take all the bytes and convert to a int.
        # We could do this to the other ones but I like have the struct.unpack
        self.flash_reserved_2 =    hex(int.from_bytes(data[56:96], byteorder='little')) # Unsigned Int - 40 Bytes
        self.boot_select = hex(struct.unpack("<I", data[0x60:0x64])[0]) # Unsigned Int - 4 Bytes") # Str - 32 Bytes
        self.app_select =  hex(struct.unpack("<I", data[0x64:0x68])[0]) # Unsigned Int - 4 Bytes") # Str - 32 Bytes
        self.sram_reserved_1 =    hex(int.from_bytes(data[0x68:0x100], byteorder='little')) # Unsigned Int - 40 Bytes


    def convert_p4_bytes_to_int(self, bytes):
        input_val = 0
        for byte in bytes:
            input_val = input_val + ((byte) << (8 * (x)))  # Calculating the total value of the part.
        return hex(input_val)

    # To convert a string for our mcu we first do little endian on 4 byte chunks and then do little endiain on those packets.
    def convert_p4_bytes_to_str(self,bytes):
        str_result = ""
        byte_arr = bytearray()
        for x in bytes:
            byte_arr.append(x)
        int_32_list = list(struct.unpack("<{}I".format(str(int(len(bytes)/ 4))), bytes))

        for int_32 in int_32_list:
            byte_stream = _int_to_raw_arr(int_32)
            for byte in byte_stream:
                # Want to remove zeros values from the srecord this indicates end of the string
                if byte != 0x00 and byte != 0xff:
                    str_result = str_result + chr(byte)

        if str_result == "":
            str_result = None
        return str_result


def _parse_software_record(software_version_raw_string):
    main_count = 0
    sw_record_list = [
        {"unit_name": "software_image_type", "unit_size": 4, "unit_type": "str"},
        {"unit_name": "software_length", "unit_size": 4, "unit_type": "int"},
        {"unit_name": "software_crc", "unit_size": 4, "unit_type": "int"},
        {"unit_name": "reserved_1", "unit_size": 4, "unit_type": "int"},
        {"unit_name": "software_version", "unit_size": 32, "unit_type": "str"},
        {"unit_name": "hardware_type", "unit_size": 4, "unit_type": "str"},
        {"unit_name": "product_type", "unit_size": 4, "unit_type": "str"},
        {"unit_name": "reserved_2", "unit_size": 40, "unit_type": "int"},
    ]
    some_data = ParsedData(bytes(software_version_raw_string))
    sw_info = {}
    contents_arr = []
    for count in range(len(sw_record_list)):
        if sw_record_list[count]["unit_type"] == "int":
            input_val = 0
            for x in range(sw_record_list[count]["unit_size"]):
                input_val = input_val + (
                            (software_version_raw_string[main_count]) << (8 * (x)))  # Calculating the total value of the part.
                main_count = main_count + 1
            sw_info[sw_record_list[count]["unit_name"]] = hex(input_val)
            contents_arr.append(input_val)

        elif sw_record_list[count]["unit_type"] == "str":
            str_input = ""
            byte_arr = bytearray()

            for x in range(sw_record_list[count]["unit_size"]):
                byte_arr.append(software_version_raw_string[main_count])
                main_count = main_count + 1
            something = list(
                struct.unpack("<{}I".format(str(int(sw_record_list[count]["unit_size"] / 4))), byte_arr))
            packed_str = ""
            for int_32 in something:
                byte_stream = _int_to_raw_arr(int_32)
                packed_data = struct.pack(f'>I', int_32)
                packed_str= packed_str + packed_data.decode("utf-8")
                #reordered_bytes = list(struct.unpack("<4c",bytes(int_32)))
               # byte_stream = int_32[::-1] # Reverse the bytes
                for byte in byte_stream:
                    # Want to remove zeros values from the srecord this indicates end of the string
                    if byte != 0x00 and byte != 0xff:
                        str_input = str_input + chr(byte)

            if str_input == "":
                str_input = "N/A (Blank or Not implemented)"
            contents_arr.append(str_input)
            sw_info[sw_record_list[count]["unit_name"]] = str_input



    '''
    This section parses the current sram extra information.
    This information is 2 uint32_t bytes. One for bl_select while another is app_select.
    '''
    contents_arr = []
    for count in range(2):
        input_val = 0
        for x in range(4):
            input_val = input_val + (
                        (software_version_raw_string[main_count]) << (8 * (x)))  # Calculating the total value of the part.
            main_count = main_count + 1
        contents_arr.append(input_val)

    sw_info["boot_select"] = hex(contents_arr[0])
    sw_info["app_select"] = hex(contents_arr[1])
    return sw_info


'''
Function to process version strings.
'''

'''
This function will parse a version string from a p4 version
This can be used to analyze the versions they are using.

The layout of the string can be shown below:
pcu_sw_0.2.0-8-g350bcb7f
However this format can work as well 
pcu_sw_0.2.0
The type of firmware can be determined by looking at the first section:
pcu_sw

The version number can be determined by the second section:
0.2.0
    - The first number is a major revision. This will be used when software is qualified and certified for flight
    - The second number is a minor revision. This indicates a backward breaking change has happened.  The api and firmware should have the same minor version.
    - The third number is a patch revision. This is when a change is made that is not backward breaking.

The last part is the git hash and history:
-8-g350bcb7f
    - The -8 indicates how many commits it has been since the last version update (minor included)
    - The g350bcb7f indicates what is the git hash so it can be used to look up the exact commit. 


However this layout can not be assumed thus we will check if the firmware is "valid"
To do this we are going to use a regex search based on the template given above.
It will then return a named tuple of the version we parsed.
'''
def p4_version_parser(version_string)-> Optional[P4VersionInfo]:

    # Extract components using the Regex pattern for P4 Version
    match = re.match(P4VERSIONREGEX, version_string)
    if match:
        # Extract components from the match
        label = match.group(1)
        major = int(match.group(2))
        minor = int(match.group(3))
        patch = int(match.group(4))
        commit_count = int(match.group(5)) if match.group(5) else None  # Default to 0 if number is not present
        git_hash = str(match.group(6)).replace("-","",1) if match.group(6) else None # Make a string if found and get rid of the first -  else return None

        # Return the components as a Named Tuple
        return P4VersionInfo(label,major,minor,patch,commit_count,git_hash)
    else:
        return None # Parsing error


def is_valid_p4_version(version_string):
    # Define the updated pattern for the version format

    # Check if the version string matches the updated pattern
    if re.match(P4VERSIONREGEX, version_string):
        return True
    else:
        return False


'''
Internal Functions to run program
'''


def _process_errors(error_code, error_args):
    if error_code == 0:
        return
    elif error_code == 1:
        print("The location you specified '" + str(
            error_args[0]) + "' is not a valid location. Please specify a value between 0 and 31. NOT SENDING COMMAND.")
    elif error_code == 2:
        print("The string element '" + str(error_args[0]) + "' " + "is to long the max length is :" + str(
            error_args[1]) + " NOT SENDING COMMAND.")
    elif error_code == 3:
        print("The hardware data coming back is empty. Nothing can be recorded")
        return
    elif error_code == 4:
        print("The writing of the record failed. The value that was ask to be  wanted to write was '" + str(
            error_args[0]) + "' what was read back was  '" + str(error_args[1]) + "' Please try to write again.")
        return


def _int_to_raw_arr(int_val):
    u32_val = int(int_val) & 0xFFFFFFFF
    arr = []
    arr.append((u32_val >> 24) & 0xFF)
    arr.append((u32_val >> 16) & 0xFF)
    arr.append((u32_val >> 8) & 0xFF)
    arr.append((u32_val) & 0xFF)
    return arr
