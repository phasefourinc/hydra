import argparse
import pathlib
import sys
import pkg_resources
import maxapi.sentry_logger
from maxapi.command_line_scripts.firmware_update_tool_module import firmware_update_tool
from maxapi.command_line_scripts.get_software_information_tool import gather_software_information
from maxapi.command_line_scripts.check_hardware_connection_module import check_hardware_connection_tool

firmware_update_keyword_list =["fw_update","fw"]
parameter_table_update_keyword_list =["update_pt","pt"]
get_software_keyword_list =["get_software_info","gsi"]
check_hardware_connection_keyword_list =["check_hardware_connection","chc"]


def main(argv=None):
    args =parse_arguments(argv)
    if check_if_keyword_is_valid(args.command,firmware_update_keyword_list):
        print("Activating Firmware Update Tool...")
        board_type, port, baud, srec, firmware_setting = setup_firmware_tool(args)
        firmware_update_tool(board_type, port, baud, srec, firmware_setting)
    elif check_if_keyword_is_valid(args.command,get_software_keyword_list):
        print("Activating Read Software Tool...")
        run_get_software_tool(args)
    elif check_if_keyword_is_valid(args.command,check_hardware_connection_keyword_list):
        print("Activating Check Hardware Connection Tool...")
        run_check_hardware_connection_tool(args)
    elif check_if_keyword_is_valid(args.command,parameter_table_update_keyword_list):
        print("Currently we do not support Parameter Table Update. Sorry!")
    else:
        print("\nThis is the maxapi. This can be used as a general tool for things involving the maxwell product.\n"
              "The maxapi can be used as a tool to do the following:"
              "\n------------------------------------------------------------\n"
              "1) Update Firmware: 'fw_update'\n"
              "2) Read Maxwell Software: 'get_software_info'\n"
              "3) Check Hardware Connections: 'check_hardware_connection'"
              "\n-------------------------------------------------------------\n"
              "If more features are needed contact Andrew Thornborough at andrew.thornborough@phasefour.io")





'''
This is the function that will parse all the arguments coming from the command prompt.
We are using subparsers to parse out the features here.
Each subparser is a new tool use for the maxapi. For example subparser 1 is update-fw. subparser 2 is update-pt.
If a new tool is needed create another subparser below
Each subparser can have its own arguments. You can add these by taking the subparser and add a argument.
'''
def parse_arguments(argv):
    #Arg parse setup.
    argv = argv if argv is not None else sys.argv[1:]
    parser = argparse.ArgumentParser()
    subparsers = parser.add_subparsers(help='commands',dest="command")

    #Arguments that do not need another parser super high level
    max_api_version = pkg_resources.get_distribution("maxapi").version
    parser.add_argument('--version', action='version', version='%(prog)s ' + str(max_api_version))   # Version of the maxapi

    #Subparsers these are used as the main tools for maxwell api.
    fw_parser = subparsers.add_parser('fw_update', aliases=firmware_update_keyword_list, help='Updates the Maxwell Firmware. It requires the following:(1) The board type: ,(2) The COM port,(3) The baud rate, (4) Where to get the srecord.') #Sub parser to update maxwell's firmware.
    get_sw_parser = subparsers.add_parser('get_software_info', aliases=get_software_keyword_list, help='Gathers all the software information from the maxwell unit. It requires the following:(1) The board type: ,(2) The COM port,(3) The baud rate')  # Sub parser to  get_software_tool.
    pt_parser = subparsers.add_parser('update_pt', aliases=parameter_table_update_keyword_list, help='Updates the Maxwell Parameter Table. This is not implemented yet.') #Sub parser to update maxwell's param table.
    check_hardware_connection_paraser = subparsers.add_parser('check_hardware_connection', aliases=check_hardware_connection_keyword_list, help="Checks to see what hardware maxapi can talk to.")

    #Arguments for the fw_subparser
    fw_parser.add_argument('--board_type','-bt',  type=str, help='The board type you want to update the firmware with. The options are: Prop Controller (pcu) or Thruster Controller (thr)',choices=["pcu", "thr"])
    fw_parser.add_argument('--port','-p',  type=str, help='The port you are connected to maxwell (Ex:COM4)')
    fw_parser.add_argument('--baud_rate','-br',  type=int, help='The baud rate you want to send it at. The Spacecraft port is 115.2kbs, while the GSE port is 1Mbps')
    fw_parser.add_argument('--srec_file','-s',  type=str, help='The srecord you want to send. If you want to explicitly send one. This take precedence over srec_location')
    fw_parser.add_argument('--firmware_location', '-f',  help='The setting to let the tool know where do you want to look for the srec. NOTE if srec_file is called this is not used.', choices=["pip", "pip_sync","api","cur_folder"])


    #Arguments for the get_sw subparsers
    get_sw_parser.add_argument('--board_type','-bt',  type=str, help='The board type you want to update the firmware with. The options are: Prop Controller (pcu) or Thruster Controller (thr) or Both (max)',choices=["pcu", "thr", "max"])
    get_sw_parser.add_argument('--port','-p', type=str, help='The port you are connected to maxwell (Ex:COM4)')
    get_sw_parser.add_argument('--baud_rate','-br',  type=int, help='The baud rate you want to send it at. The Spacecraft port is 115.2kbs, while the GSE port is 1Mbps')
    get_sw_parser.add_argument('--output','-o',  action='store_true',help='If you want to store a json file of the output.') #Giving gsi a output flag so you can save the output if need be.


    #Arguements for the cc subparser
    check_hardware_connection_paraser.add_argument('--port', '-p', type=str, help='The port you are connected to maxwell (Ex:COM4)')
    check_hardware_connection_paraser.add_argument('--baud_rate', '-br', type=int, help = 'The baud rate you want to send it at. The Spacecraft port is 115.2kbs, while the GSE port is 1Mbps')
    check_hardware_connection_paraser.add_argument('--block', '-bl', type=str, help = 'The Maxwell Unit you want to talk to. Eg. Block1, Block2',default="Block2", choices=["Block1","Block2"])


    # gathering arguments
    parser.parse_args()
    args = parser.parse_args()
    return args




#Checks a list and determines if the command keyword is in the valid list.
def check_if_keyword_is_valid(command,command_list):
    for x in command_list:
        if x == command:
            return True
    return False


# Check to see if any command line arguments were set or not.
def argument_checker(args,argument_list):
    for arg in argument_list:
        if args.arg["arg_name"]:
            arg["set"] = True
    return argument_list

'''
Tool Functions
'''


def setup_firmware_tool(args):
    get_sw_arguments = {"board_type":False, "port":False, "baud_rate":False, "srec_file": False, "firmware_location":False}

    check_arguments(args, get_sw_arguments)

    if get_sw_arguments["board_type"] == False:
        board_type_parm = get_board_type(False)
    else:
        board_type_parm = args.board_type
    board_string = get_board_string(board_type_parm)
    print("Getting software for " + str(board_string))
    if get_sw_arguments["port"] == False:
        port = get_com_port()
    else:
        port = args.port
    if get_sw_arguments["baud_rate"] == False:
        baud_rate = get_baud_rate()
    else:
        baud_rate = args.baud_rate

    if get_sw_arguments["srec_file"] == False:
        if get_sw_arguments["firmware_location"] == False:
            firmware_setting, srec = get_srec_location()
        else:
            srec = ""
            firmware_setting = args.firmware_location
    else:
        srec = args.srec_file
    if board_type_parm == "pcu":
        board_type_parm= "pcu_sw"
    elif board_type_parm== "thr":
        board_type_parm = "thr_sw"

    return  board_type_parm, port, baud_rate, srec, firmware_setting

def check_if_user_wants_to_quit(current_input):
    if current_input == "q":
        print("Shutting Down Program...")
        sys.exit()

def check_arguments(args, arg_list):
    for key, value in arg_list.items():
        if getattr(args, key) != None and getattr(args, key) != False:
            arg_list[key] = True
    return arg_list

def check_if_file_exist(path_name):
    my_file = pathlib.Path(path_name)
    if my_file.is_file():
         return True
    return False



def run_get_software_tool(args):
    get_sw_arguments = {"board_type":False, "port":False, "baud_rate":False, "output":False}
    get_sw_arguments = check_arguments(args, get_sw_arguments)
    if get_sw_arguments["board_type"] == False:
        board_type = get_board_type(enable_maxwell_option=True, default_option_hardware="max")
    else:
        board_type = args.board_type
    board_string = get_board_string(board_type)
    print("Getting software for " + str(board_string))
    if get_sw_arguments["port"] == False:
        port = get_com_port()
    else:
        port = args.port
    if get_sw_arguments["baud_rate"] == False:
        baud_rate = get_baud_rate()
    else:
        baud_rate = args.baud_rate
    print("Getting Software Information.....\n")
    print(dash_creator(60)+"\n")
    status, file_name = gather_software_information(port, baud_rate, board_type,get_sw_arguments["output"])
    if status == "STATUS_SUCCESS":
        print("Software Information Gathering is Complete!")
        print(dash_creator(60) + "\n")
        if get_sw_arguments["output"] == True:
            print("The Output will be in the same folder where you called this tool.\nThe file name is "+ str(file_name))
    else:
        print(dash_creator(60) + "\n")
        print("Software Gathering failed....  The status was '" + status+"'\n")
        print("Please double check your configurations and power cycle the unit if possible.")
        print("If you verified that your configurations are correct and power cycled a couple of times.\nPlease contact your local phasefour developer monkey")


def run_check_hardware_connection_tool(args):
    get_sw_arguments = {"port":False, "baud_rate":False, "block":False}
    get_sw_arguments = check_arguments(args, get_sw_arguments)
    if get_sw_arguments["port"] == False:
        port = get_com_port()
    else:
        port = args.port
    if get_sw_arguments["baud_rate"] == False:
         baud_rate = get_baud_rate()
    else:
        baud_rate = args.baud_rate
    if get_sw_arguments["block"] == False:
        block = "block2"
    else:
        block = args.block
    print("Checking Hardware Connection.....\n")
    print(dash_creator(60)+"\n")
    status = check_hardware_connection_tool(port, baud_rate, block.lower())
    if status == True:
        print("Finished Checking Hardware!")
        print(dash_creator(60) + "\n")

    else:
        print(dash_creator(60) + "\n")
        print("Finished Checking Hardware....  There was a error in getting some of the data...")
        print("We lost contact with one of the boards when we were gathering some information.\n")
        print("Please double check your configurations and power cycle the unit if possible.")
        print("If you verified that your configurations are correct and power cycled a couple of times.\nPlease contact your local phasefour developer monkey")





def dash_creator(num_of_dash):
    dash_string = ""
    for x in range(num_of_dash):
        dash_string = dash_string + "-"
    return dash_string


def isfloat(x):
    try:
        float(x)
        return True
    except ValueError:
        return False


def isint(input_val):
    try:
        int(input_val, 0)
        return True
    except ValueError:
        return False

#
'''
This function will get the board type from the user.  
It will gather input from the user and determine what hardware he wants to use for a given tool.

It will return 1 of 3 outputs. (pcu,thr,max) This will help determine what the user wants to run with the tool.

If your tool can only run on one hardware unit it is recommend not to use this function. 
Instead just assume or verify the hardware is needed is connected instead of letting the user pick.

There is two inputs:
enable_maxwell_option = (True, False) This will determine if you want to include the maxwell unit as a input. 
        This is used in GSI where if they select maxwell it will get both prop and thruster controller information.
        If maxwell is False as a  argument no new values are put in the valid_maxwell_keywords list leaving it a blank array which will not for that list. 
        So when checking the user input on what hardware to select no inputs will work for the valid_maxwell_keywords beacasue it is still empty.
default_option_hardware = ("max","pcu","thr","none") This input will select a default value. 
         So if the user hits enter it will pick that hardware unit instead.
         

This function will dynamically create the prompt  for you.

'''

def get_board_type(enable_maxwell_option=False, default_option_hardware =None):
    valid_maxwell_keywords = []
    valid_prop_controller_keywords = ["prop", "prop controller"]
    valid_thruster_controller_keywords = ["thr", "tcu","thruster controller"]
    list_of_hardware_arguments = ["q", "Q"]
    list_of_hardware_arguments,string_options = process_board_type_options(enable_maxwell_option,
                                                                           default_option_hardware,
                                                                           valid_maxwell_keywords,
                                                                           valid_prop_controller_keywords,
                                                                           valid_thruster_controller_keywords,
                                                                           list_of_hardware_arguments)
    input_val = ""
    board_type = ""
    input_valid = False

    while not input_valid:
        input_val = input(
            "\nPlease insert the hardware you want to interface with. Or press 'q' to quit the program.\nThe current options are:"+ string_options +"\nInput:")
        input_val = input_val.strip()
        input_val = input_val.lower()
        if input_val in list_of_hardware_arguments:
            input_valid = True
            check_if_user_wants_to_quit(input_val)
        else:
            print("\nThe input you selected " + input_val + " is not valid.")
    if input_val in valid_maxwell_keywords:
        board_type = "max"
    elif input_val in valid_prop_controller_keywords:
        board_type = "pcu"
    elif input_val in valid_thruster_controller_keywords:
        board_type = "thr"

    return board_type


'''
This function deals with logistics of what should the software should be checking for valid inputs, what should the default value be, and format a string to output it to the user.
The arguments (valid_maxwell_keywords,valid_prop_controller_keywords, and valid_thruster_controller_keywords) are lists so when we append things to them they happen globally. 
That is why they are not returned in the function. Even though we make changes to those lists that other functions need the updated values, we don't need to return them because they happen due to python object arguments.
It is like passing the pointer values in c. Not a fan of this because you may not understand what is happening but that is what the comment is for.

This function determines what the list should be and append the number as a valid input for these keywords to check above.
It will also append "" if there is a value that is selected as default.
If maxwell is False as a  argument no new values are put in the valid_maxwell_keywords list leaving it a blank array which will not for that list. 
So when checking the user input on what hardware to select no inputs will work for the valid_maxwell_keywords beacasue it is still empty.
'''
def process_board_type_options(enable_maxwell_option, default_option_hardware,valid_maxwell_keywords,valid_prop_controller_keywords,valid_thruster_controller_keywords,list_of_hardware_arguments):
    hardware_count = 1
    string_options = ""
    if enable_maxwell_option:
        valid_maxwell_keywords.extend(["max", "maxwell", str(hardware_count)])
        string_options = string_options + "\n" + str(hardware_count) + ") Maxwell"
        if default_option_hardware == "max":
            valid_maxwell_keywords.append("")
            string_options = string_options + " (Default)"
        hardware_count = hardware_count + 1

    valid_prop_controller_keywords.append(str(hardware_count))
    string_options = string_options + "\n" + str(hardware_count) + ") Prop Controller"
    if default_option_hardware == "pcu":
        valid_prop_controller_keywords.append("")
        string_options = string_options + " (Default)"
    hardware_count = hardware_count + 1

    valid_thruster_controller_keywords.append(str(hardware_count))
    string_options = string_options + "\n" + str(hardware_count) + ") Thruster Controller"
    if default_option_hardware == "thr":
        valid_thruster_controller_keywords.append("")
        string_options = string_options + " (Default)"

    list_of_hardware_arguments = list_of_hardware_arguments + valid_maxwell_keywords + valid_prop_controller_keywords + valid_thruster_controller_keywords

    return list_of_hardware_arguments, string_options


def get_board_string(board_type):
    if board_type == "max":
        board_string = "Maxwell"
    elif board_type == "pcu":
        board_string = "Prop Controller"
    elif board_type == "thr":
        board_string = "Thruster Controller"
    else:
        board_string = "N/A"

    return board_string

def get_com_port():
    input_val = input("\nPlease insert the computer port you are talking to maxwell on. Or press 'q' to quit the program.Ex:'COM4'\nInput:")
    input_val = input_val.strip()
    if input_val:
        check_if_user_wants_to_quit(input_val)
    return input_val

def get_baud_rate():
    input_valid = False
    final_baud = 0
    while not input_valid:
        input_val = input("\nPlease select the hardware you are interfacing to. Or press 'q' to quit the program."
                          "\n1) Prop Controller (GSE Port)\n2) Prop Controller (Spacecraft Port)\n3) Thruster Controller\n4) Custom Baud Rate\nInput:")
        check_if_user_wants_to_quit(input_val)
        if input_val=='1':
            final_baud = 1000000
            input_valid = True
        if input_val=='2':
            final_baud = 115200
            input_valid = True
        if input_val=='3':
            final_baud = 1000000
            input_valid = True
        if input_val=='4':
            while not input_valid:
                input_val = input("Please insert the custom serial baud rate you are talking to the hardware with. Or press 'q' to quit the program.\nInput:")
                input_val = input_val.strip()
                if isint(input_val):
                    input_valid = True
                    final_baud = int(input_val)
                else:
                    check_if_user_wants_to_quit(input_val)
                    print("The input you selected " + input_val + " is not valid.\n")

    return final_baud

def get_srec_location():
    srec_setting = 0
    srec_file = ""
    input_val = input("\nPlease select where you want to get the firmware. Or press 'q' to quit the program.\n"
                      "1) Latest and greatest from the pip server\n"
                      "2) Latest from the pip server that is guarantee to work with api (NOT AVAILABLE)\n"
                      "3) The firmware in the maxapi\n"
                      "4) The firmware in your current directory. (NOTE please only have one firmware file there.)\n"
                      "5) The specific path to the firmware file.\n"
                      "Input:")
    input_val = input_val.strip()
    check_if_user_wants_to_quit(input_val)
    if input_val == '1':
        srec_setting = "pip"
    if input_val == '2':
        srec_setting = "pip_sync"
    if input_val == '3':
        srec_setting = "api"
    if input_val == '4':
        srec_setting = "cur_folder"
    if input_val == '5':
        srec_file_found = False
        while not srec_file_found:
            input_val = input("Please insert the path of the srec file. Or press q to quit.\nInput:")
            print(input_val[-5:])
            check_if_user_wants_to_quit(input_val)
            if check_if_file_exist(input_val) and input_val[-5:] == ".srec":
                srec_file = input_val
                srec_setting = "custom_srec"
                srec_file_found= True
            print("The file path you selected is not a valid srec file.\n")
    return srec_setting, srec_file

if __name__ == '__main__':
    main()