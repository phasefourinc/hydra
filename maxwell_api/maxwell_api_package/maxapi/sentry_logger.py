import sentry_sdk
import os
maxwell_api_path = os.path.dirname(os.path.abspath(__file__))
version_path = os.path.join(maxwell_api_path, 'maxapi_version.txt')
with open(version_path) as vr:
    version_text_raw = vr.read()
    version_text = version_text_raw.strip()

RELEASE_STRING = version_text
# sentry_sdk.init("https://6cfdc9ae1b7b496c8bcbd3d1cbe57f36@sentry.io/5066744",
#                 send_default_pii = True,
#                 attach_stacktrace= True,
#                 release=RELEASE_STRING)