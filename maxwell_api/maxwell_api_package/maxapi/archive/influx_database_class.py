# Third Party Imports
import collections
import logging
import sys

import requests
from influxdb import InfluxDBClient

from maxapi.max_errors import MaxErr

logger = logging.getLogger(__name__)

COMPANY_FOUNDATION_TIME_UNIX = 1420099200000000

INVALID_TIME_MEASUREMENT = "neverland"


class InfluxDBClass:
    def __init__(self, database, measurement, host='localhost', port=8086, timeout=1):
        self.host = host
        self.database = database
        self.port = port
        if measurement == "default_measurement":
            logger.error(
                "This is the default measurement please update the measurement to something you want. Shutting down program.")
            sys.exit()
        else:
            self.measurement = measurement
        self.user = ''
        self.password = ''
        self.main_client = None
        self.post_append_tags = {}
        self.annotate_msg_id_enable = False  # This enable is the option to annotate_msg_id or not.
        self.msg_id_annotation_black_list = []  # This is the black list for the annotation of message ids
        self.packet_batch = 1000
        # self.batch_time = None
        # self.batch_freq = datetime.timedelta(seconds=.100)
        # self.last_upload_time = datetime.datetime.now()
        # self.batch_enable = False
        # self.json_queue = collections.deque()
        # self.timer_thread = threading.Thread(target=self.logging_thread)
        # self.log_to_grafana = False
        self.start_influx(timeout)
        self.embed_timestamp_field = "UNIX_TIME_US"


    def start_influx(self, timeout_connection):
        self.main_client = InfluxDBClient(self.host, self.port, self.user, self.password, self.database,
                                          timeout=timeout_connection, retries=1)
        database_list = self.main_client.get_list_database()
        found_database = False

        for x in database_list:
            if x == self.database:
                found_database = True

        if not found_database:
            self.main_client.create_database(self.database)

    def update_influx(self, database, measurement, host):
        self.main_client.close()
        self.database = database
        self.host = host
        self.measurement = measurement
        self.start_influx()

    def update_post_append_tags(self, tag_dict):
        if self.post_append_tags:
            self.post_append_tags = {**self.post_append_tags, **tag_dict}
        else:
            self.post_append_tags = tag_dict

    def clean_up_tags(self, tag_dict):
        clean_dict = {}
        for key, value in tag_dict.items():
            key_string = str(key)
            value_string = str(value)
            key_string = self._clean_up_string(key_string)
            value_string = self._clean_up_string(value_string)
            clean_dict[key_string] = value_string
        return clean_dict

    def _clean_up_string(self, string_val: str):
        string_clean = string_val.strip()  # Removing Trailing and leading whitespaces
        string_clean = string_clean.replace(" ", "_")  # Replacing all spaces in between to be underscores
        string_clean = string_clean.lower()  # Lowercase the whole string
        return string_clean

    def update_post_annotation_black_list(self, black_list, extend=False):
        if not extend:
            self.msg_id_annotation_black_list = black_list
        else:
            self.msg_id_annotation_black_list.extend(black_list)

    def msg_id_annotation_enable(self, enable=False):
        self.annotate_msg_id_enable = enable

    def write_clank_to_influx(self, Test_Results):
        # If the data coming in to influx is valid
        json_body = []
        if isinstance(Test_Results,
                      collections.Mapping):  # collections.Mapping is a general solution that includes dictionary
            json_body.append(self.__create_influx_clank_json(Test_Results))
        elif isinstance(Test_Results, list):
            for packet in Test_Results:
                json_body.append(self.__create_influx_clank_json(packet))
        else:
            logger.error(
                "The clank packet you gave (results) is not a list or a dictionary. Please send a list or a dictionary. NOT LOGGING TO INFLUX")
            return

        self.__write_to_influx_db(json_body)  # Brackets are to convert the json value into a list.

    def write_tlm_to_influx(self, results, field_names):
        json_body = []
        if isinstance(results,
                      collections.Mapping):  # collections.Mapping is a general solution that includes dictionary
            json_body.append(self.__create_influx_tlm_json(results, field_names))
        elif isinstance(results, list):
            for packet in results:
                json_body.append(self.__create_influx_tlm_json(packet, field_names))
        else:
            logger.error(
                "The tlm packet you gave (results) is not a list or a dictionary. Please send a list or a dictionary. NOT LOGGING TO INFLUX")
            return
        self.__write_to_influx_db(json_body)  # Brackets are to convert the json value into a list.

    def __create_influx_clank_json(self, Test_Results):
        # If the data coming in to influx is valid
        if Test_Results["status"] != "EMBEDDED_TIMEOUT" and Test_Results["status"] != "NOT_ENOUGH_BYTES":
            field_set = {
                "Status": Test_Results["status"],  # Tagged
                "Sync Byte Version": Test_Results["sync_byte_version"],
                "Msg ID": Test_Results["msg_id"],  # Tagged
                "Source Address": Test_Results["source_address"],  # Tagged
                "Transmit Mask": Test_Results["transmit_mask"],  # Tagged
                "Packet Sequence Count": Test_Results["packet_seq"],
                "Data Length": Test_Results["data_length"],
                "CRC": Test_Results["crc"],
                "Total Number of Communication Errors": Test_Results["total_com_errors"],
                "Total Number of Communication RX": Test_Results["total_clank_messages_rx"],
                "Total Number of Communication TX": Test_Results["total_clank_messages_tx"]
            }

            tag_set = {
                "Status": Test_Results["status"],  # Tagged
                "Msg ID": Test_Results["msg_id"],  # Tagged
                "Source Address": Test_Results["source_address"],  # Tagged
                "Transmit Mask": Test_Results["transmit_mask"],  # Tagged
                "Data Direction": Test_Results["data_direction"],  # Tagged
                "COM Port": Test_Results["current_port"],
                "Baud Rate": Test_Results["baud_rate"]
            }

        else:
            field_set = {
                "Status": Test_Results["status"],
                "Total Number of Communication Errors": Test_Results["total_com_errors"],
                "Total Number of Communication RX": Test_Results["total_clank_messages_rx"],
                "Total Number of Communication TX": Test_Results["total_clank_messages_tx"]
            }
            tag_set = {
                "Status": Test_Results["status"],
                "Data Direction": Test_Results["data_direction"],
                "COM Port": Test_Results["current_port"],
                "Baud Rate": Test_Results["baud_rate"]
            }

        if Test_Results["status"] != "STATUS_SUCCESS":
            field_set["Raw Clank Packet"] = Test_Results["clank_packet_string_hex"]
            # We are checking here because we don't want to process annotations when we are uploading a data dump. It is to much data/
        field_set, tag_set = self._dev_clank_formating_grafana(Test_Results, field_set, tag_set)
        json_body = {
            "measurement": self.measurement,
            "tags": tag_set,
            "fields": field_set

        }
        return json_body

        # This function does a couple things for the clank json file.
        # It first checks to see if msg id will be annotated or not.
        # This is done by going through the black list to see if the message id is on it.
        # If it is, it will not be annotated. If not, it will.
        # The way annotation works is triggering off of the 'msg_id_annotate_enable'
        # If it is a one grafana will be instructed to annotate that message id string.
        # If it is a zero it will not. But the Msg ID String always gets logged.
        # The blacklist is predetermined by default. But people can load in there own if desired.
        #

    def _dev_clank_formating_grafana(self, Test_Results, field_set, tag_set):
        if self.annotate_msg_id_enable:
            if Test_Results["data_direction"] == "TX":
                field_set["Msg ID String"] = Test_Results["msg_id_string"]
                tag_set["transmit_mask_string"] = Test_Results["transmit_mask_string"]
                if not self._msg_in_black_list(Test_Results["msg_id"]):
                    field_set["msg_id_annotate_enable"] = 1  # One indicates to annotate.
                else:
                    field_set["msg_id_annotate_enable"] = 0  # Zero indicates to not annotate.
        final_tag_set = {**tag_set, **self.post_append_tags}
        return field_set, final_tag_set

    # Goes through the blacklist to see if any of the messages ids come back.
    def _msg_in_black_list(self, msg_id):
        for msg_id_black in self.msg_id_annotation_black_list:
            if msg_id_black == msg_id:
                return True
        return False

    def __create_influx_tlm_json(self, results, field_names):
        Telemtry_Results = self.__create_influx_data_telm(field_names, results)
        json_body = {
            "measurement": self.measurement,
            "tags": self.post_append_tags,
            "fields": Telemtry_Results

        }
        if results["message_type"] == "Telemetry Packet" or results["message_type"] == "Status Packet":
            if results[self.embed_timestamp_field] > COMPANY_FOUNDATION_TIME_UNIX:
                json_body["time"] = results[self.embed_timestamp_field]
            else:
                json_body["measurement"] = INVALID_TIME_MEASUREMENT
        return json_body

    def __create_influx_data_telm(self, field_names, results):
        Telemtry_Data = {}
        for field in field_names:
            Telemtry_Data[field] = results[field]
        return Telemtry_Data

    def __write_to_influx_db(self, total_points):
        point_array = []
        while len(total_points) > self.packet_batch:
            point_array.append(list(total_points[:self.packet_batch]))
            del total_points[:self.packet_batch]
        point_array.append(total_points[:len(total_points)])

        for set_of_influx_points in point_array:
            sent_influx_timeout_condition = True
            timeout_count = 0
            while sent_influx_timeout_condition:
                try:
                    self.main_client.write_points(set_of_influx_points, time_precision='u')
                except requests.exceptions.Timeout:
                    timeout_count = timeout_count + 1
                else:
                    sent_influx_timeout_condition = False
                if timeout_count >= 3:
                    logger.error("TIMEOUT OCCURRED ON INFLUX")
                    print("TIMEOUT OCCURRED ON INFLUX")
                    print("The amount of points sent was: '" + str(len(set_of_influx_points)) + "' The raw data was: \n" + str(set_of_influx_points))
                    sent_influx_timeout_condition = False

        return



def append_error_strings(Telmetry_Data):
    if Telmetry_Data["SC_ERR_CODE1"] !=0:
            Telmetry_Data["SC_ERR_CODE1_STRING"] = next(name for name, value in vars(MaxErr).items() if value == Telmetry_Data["SC_ERR_CODE1"])
    if Telmetry_Data["SC_ERR_CODE2"] !=0:
            Telmetry_Data["SC_ERR_CODE2_STRING"] = next(name for name, value in vars(MaxErr).items() if value == Telmetry_Data["SC_ERR_CODE2"])
    if Telmetry_Data["SC_ERR_CODE3"] !=0:
            Telmetry_Data["SC_ERR_CODE3_STRING"] = next(name for name, value in vars(MaxErr).items() if value == Telmetry_Data["SC_ERR_CODE3"])
    if Telmetry_Data["ERR1_CODE"] !=0:
            Telmetry_Data["ERR1_CODE_STRING"] = next(name for name, value in vars(MaxErr).items() if value == Telmetry_Data["ERR1_CODE"])
    if Telmetry_Data["ERR2_CODE"] !=0:
            Telmetry_Data["ERR2_CODE_STRING"] = next(name for name, value in vars(MaxErr).items() if value == Telmetry_Data["ERR2_CODE"])
    if Telmetry_Data["ERR3_CODE"] !=0:
            Telmetry_Data["ERR3_CODE_STRING"] = next(name for name, value in vars(MaxErr).items() if value == Telmetry_Data["ERR3_CODE"])

    if Telmetry_Data["ERRHX1_CODE"] !=0:
            Telmetry_Data["ERRHX1_CODE_STRING"] = next(name for name, value in vars(MaxErr).items() if value == Telmetry_Data["ERRHX1_CODE"])
    if Telmetry_Data["ERRHX2_CODE"] !=0:
            Telmetry_Data["ERRHX2_CODE_STRING"] = next(name for name, value in vars(MaxErr).items() if value == Telmetry_Data["ERRHX2_CODE"])
    if Telmetry_Data["ERRHX3_CODE"] !=0:
            Telmetry_Data["ERRHX3_CODE_STRING"] = next(name for name, value in vars(MaxErr).items() if value == Telmetry_Data["ERRHX3_CODE"])

    if Telmetry_Data["MOST_FREQ_ERR_CODE"] != 0:
        Telmetry_Data["MOST_FREQ_ERR_CODE_STRING"] = next(name for name, value in vars(MaxErr).items() if value == Telmetry_Data["MOST_FREQ_ERR_CODE"])






def __convert_int_array_into_hex_string(int_array):
    raw_clank_packet_hex_format = ""
    for byte in int_array:
        raw_clank_packet_hex_format = raw_clank_packet_hex_format + hex(byte) + ", "

    raw_clank_packet_hex_format = raw_clank_packet_hex_format[:len(raw_clank_packet_hex_format) - 2]
    return raw_clank_packet_hex_format


''' 


This functionality is meant if we want to do batching based on time copy and replace functions as sees fit.


    def disable_logging (self):
        self.log_to_grafana = False

    def enable_logging(self):
        self.timer_thread.setDaemon(True)
        self.log_to_grafana = True
        self.timer_thread.start()


    def create_clank_packet(self,Test_Results):
        # If the data coming in to influx is valid
        json_body = []
        if isinstance(Test_Results, collections.Mapping):  # collections.Mapping is a general solution that includes dictionary
            json_body.append(self.create_influx_clank_json(Test_Results))
        elif isinstance(Test_Results, list):
            for packet in Test_Results:
                json_body.append(self.create_influx_clank_json(packet))
        else:
            logger.error("The clank packet you gave (results) is not a list or a dictionary. Please send a list or a dictionary. NOT LOGGING TO INFLUX")
            return
        if self.batch_enable:
            self.json_queue = self.json_queue + json_body
            if datetime.datetime.now() - self.last_upload_time > self.batch_freq:
                self.write_to_influx_db(list(self.json_queue))  # Brackets are to convert the json value into a list.
                self.json_queue.clear()
                self.last_upload_time = datetime.datetime.now()
        else:
            self.write_to_influx_db(json_body) # Brackets are to convert the json value into a list.
            
            
            
    def create_tlm_packet(self,results,field_names):
        logger.debug("In Write to Influx")
        json_body = []
        if isinstance(results,collections.Mapping): # collections.Mapping is a general solution that includes dictionary
            json_body.append(self.create_influx_tlm_json(results,field_names))
        elif isinstance(results, list):
            for packet in results:
                json_body.append(self.create_influx_tlm_json(packet, field_names))
        else:
            logger.error("The tlm packet you gave (results) is not a list or a dictionary. Please send a list or a dictionary. NOT LOGGING TO INFLUX")
            return
        if self.batch_enable:
            self.json_queue = self.json_queue + json_body
            if datetime.datetime.now() - self.last_upload_time >  self.batch_freq:
                self.write_to_influx_db(list(self.json_queue))  # Brackets are to convert the json value into a list.
                self.json_queue.clear()
                self.last_upload_time = datetime.datetime.now()
        else:
            self.write_to_influx_db(json_body) # Brackets are to convert the json value into a list.
            
            
    def influx_still_logging(self):
        return self.timer_thread.is_alive()
            
    def logging_thread(self):
        running_timer = True
        while running_timer:
            time.sleep(self.batch_time)
            #logger.error("Length of list" +str(len(self.json_list)))
            if self.json_queue:
                result = self.write_to_influx_db(list(self.json_queue))
                if result == True:
                    self.json_queue.clear()

            if not self.log_to_grafana:
                running_timer = False
            
'''
