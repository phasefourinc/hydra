# First Party Imports
import csv
import datetime
# Third Party Imports
import logging
import os
import pathlib
import queue
import struct
import sys
import threading
import time
import requests
import json
import maxapi.sentry_logger
from maxapi import clank_protocol
from maxapi.max_cmd_id import MaxCmd
import maxapi.maxapi_output_driver as influx_client


PROP_CONTROLLER_KEYWORD = "Prop Controller"
THRUSTER_CONTROLLER_KEYWORD = "Thruster Controller"
BENDER_COMPUTER_SOURCE = 0x0E
BENDER_COMPUTER_ADDRESS = 0x01 << BENDER_COMPUTER_SOURCE
PROP_CONTROLLER_SOURCE = 0x00
PROP_CONTROLLER_ADDRESS = 0x01 << PROP_CONTROLLER_SOURCE
THRUSTER_CONTROLLER_SOURCE = 0x01
THRUSTER_CONTROLLER_ADDRESS = 0x01 << THRUSTER_CONTROLLER_SOURCE

MAXWELL_CONTROLLER_ADDRESS = PROP_CONTROLLER_ADDRESS | THRUSTER_CONTROLLER_ADDRESS
AUTO_TLM_PCU_MSG_ID_RESPONSE = 0x8000 | MaxCmd.RX_AUTO_TLM_PCU
AUTO_STATUS_PCU_MSG_ID_RESPONSE = 0x8000 | MaxCmd.RX_AUTO_STATUS_PCU
TELEMETRY_MSG_ID_RESPONSE = 0x8000 | MaxCmd.GET_TLM
MCU_DUMP_MSG_ID_RESPONSE = 0x8000 | MaxCmd.DUMP_MCU
GET_SW_INFO_MSG_ID_RESPONSE = 0x8000 | MaxCmd.GET_SW_INFO
GET_MAX_HRD_INFO_MSG_ID_RESPONSE = 0x8000 | MaxCmd.GET_MAX_HRD_INFO
AUTO_TLM_RFT_MSG_ID_RESPONSE  = 0x8000 | MaxCmd.RX_AUTO_TLM_RFT
DUMP_TLM_PCU_MSG_ID_RESPONSE   = 0x8000 | MaxCmd.RX_DUMP_TLM_PCU
DUMP_TLM_RFT_MSG_ID_RESPONSE  = 0x8000 | MaxCmd.RX_DUMP_TLM_RFT
GET_TLM_RFT_MSG_ID_RESPONSE   = 0x8000 | MaxCmd.GET_TLM_RFT_CACHED
COMPANY_FOUNDATION_TIME_UNIX = 1420099200000000  # January 1/2015 12:00 AM

CSV_TLM_HARDWARE_BREAK = "THRUSTER_CONTROLLER_HARDWARE_BREAK" # A row in the csv to indicate that the rest of the row below is for the thruster controller hardware
CSV_TLM_DERIVE_BREAK = "DERIVED_BREAK" # A row in the csv that indicates that the telemtery below will be derived telmetry from above.

TLM_DUMP = 0x8080
NO_DUMP_INIT = 0 # This indicates there is no data in the dump list and no dump is currently being read
DUMP_RUNNING = 1 # This indicates there is data being added in the dump list and a dump is currently being processed
DUMP_COMPLETED = 2 # This indicates that there is data in the dump list and there is no current data being proccesed.


ACK_OFFSET = 0x8000
# This is used as fieldnames to write clank data in a csv format.
CLANK_FIELD_NAMES_CSV = ["Timestamp",
                         "data_direction",
                         "baud_rate",
                         "current_port",
                         "status",
                         "msg_id",
                         "sync_byte_version",
                         "source_address",
                         "transmit_mask",
                         "packet_seq",
                         "data_length",
                         "crc",
                         "total_com_errors",
                         "total_clank_messages_rx",
                         "total_clank_messages_tx",
                         "clank_packet_string_hex"]


MAXAPI_CORE_PATH = os.path.dirname(os.path.abspath(__file__))

# This is the main class that will be interfaced when using the Maxwell API.
# It currently can:
# 1) Send Clank commands and read clank commands from a embedded system
# 2) Receive serial and clank/hardware information about the maxwell api
# 3) Parse and record Clank Commands into a CSV or a influxDB. This can give you a play by play of the communication through this API.
# 4) Parse and format raw telemetry data via a csv to get the formatted output.
# 5) Recorded the formatted telemetry into a CSV or a influxDB.
# 6) Formats data output in to include status, msg_id, packet_seq, raw_payload, parsed_payload.
#
# The main public functions are:
# 1) record_clank_protocol_to_influxDB: This will record clank protocol messages that go through this API into influx.
# 2) record_maxwell_telemetry_to_influxDB: This will record parsed telemetry that was read through this API into influx.
# 3) record_clank_protocol_to_csv: This will record clank protocol messages that go through this API into a CSV.
# 4) record_maxwell_telemetry_to_csv: This will record parsed telemetry that was read through this API into a CSV.
# 5) write_clank_command: This will write a clank command and read the results being returned from that command.
# 6) send_raw_clank_command: This will write a clank command with out reading anything back.
# 7) read_clank_response: This will just read a clank command and parse the data if it is a telemetry or status message.
# 8) get_serial_properties: This will output the current serial properties in the maxwell api.
# 9) enable_auto_telm: This command will send a auto telm command via a write_clank_command.
# 10) send_unix_timestamp:  This command will send a unix timestamp to the maxwell via a write_clank_command.
#


class MaxwellApi():
    def __init__(self, baud_rate, port, source_address=BENDER_COMPUTER_SOURCE, seq_count=0, open_port=True, hardware="block_1"):
        self.clank_influx_database = None
        self.telm_influx_database = None
        self.hardware_config = self.load_hardware_configuration(hardware)
        self.ClankProtocol = clank_protocol.EmbeddedCommunicatorClass()
        self.ClankProtocol.setup_configuration(baud_rate, port, source_address, seq_count)
        if open_port:
            self.ClankProtocol.open_port()
        self.csv_config = {
            "telemetry": {
                "file_name_prop": None,
                "file_name_thrust": None,
                "enable": False
            },
            "clank_protocol": {
                "file_name": None,
                "enable": False
            }
        }
        self.influx_enable = {
            "telemetry": False,
            "clank_protocol": False
        }
        self.total_comm_errors = 0
        self.total_clank_rx_messages = 0
        self.total_clank_tx_messages = 0
        self.prop_controller_parser, self.thruster_controller_parser = _prepare_parser(self.hardware_config["tlm_config"])
        self.output = {
            "status": "DEFAULT STATUS",
            "raw_payload": [],
            "parsed_payload": []
        }

        self.Read_Queue = queue.Queue(maxsize=10)
        self.read_clank_thread = threading.Thread(target=self._read_thread)
        self.start_read_thread = True
        self.start_time = str(datetime.datetime.now().strftime('%Y_%m_%d_%H_%M_%S'))
        self._enable_read_thread()
        self.dump_tlm_list = []
        self.tlm_dump_status = NO_DUMP_INIT
        self.auto_rx_count = 0
        self.timestamp_keyword = None





    # Calling destructor
    def __del__(self):
        self._disable_read_thread()
        self.ClankProtocol.close_port()

    def open_serial_port(self):
        self.ClankProtocol.open_port()

    def load_hardware_configuration(self, block_type):
        hardware_config_path = os.path.join(MAXAPI_CORE_PATH, 'configurations','hardware_configurations.json')
        # See if the hardware configuration exists.
        if os.path.isfile(hardware_config_path) == False:
            print("There is no hardware_configuration file. Thus we can run maxapi properly. SHUTTING DOWN")
            sys.exit()

        with open(hardware_config_path, "r") as json_file:
            hardware_config = json.load(json_file)
            hardware_config = hardware_config["hardware_models"][block_type]
        return hardware_config



    def record_maxwell_output(self, file_name, database="sandbox", host="nekone.phasefour.co", python_script_name = "None",enable_influx =True, enable_csv = True, custom_tags ={}):
            self.script_calling_api = python_script_name
            if isinstance(file_name, str) == False:
                maxwell_log.error("the file_name you chose is not a string. Please insert a string. NOT RECORDING")
            else:
                clank_record_name = file_name + "_clank"
                telm_record_name = file_name
                if enable_influx ==True:
                    self.record_clank_protocol_to_influxDB(database, clank_record_name, host)
                    self.record_maxwell_telemetry_to_influxDB(database, telm_record_name, host)
                    if custom_tags and self.influx_enable["telemetry"]:
                        clean_custom_tags = influx_client.clean_up_tags(custom_tags)
                        self.telm_influx_database.update_post_append_tags(clean_custom_tags)

                if enable_csv:
                    self.record_clank_protocol_to_csv(clank_record_name, True)
                    self.record_maxwell_telemetry_to_csv(telm_record_name, True)


    def record_clank_protocol_to_influxDB(self, database, measurement, host="localhost", timeout=1):
        connected = True
        try:
            self.clank_influx_database = influx_client.MaxapiInflux(database, measurement, host, timeout=timeout)
        except (requests.exceptions.ConnectTimeout, requests.exceptions.ConnectionError) as e:
            connected = False
            print("Could not connect to '" + host + "' with database '" + database + "'. Please check your internet connection and confirm you are on the appropriate network. Clank will not be recorded to influx.")
            str_err = "Could not connect to '" + host + "' with database '" + database + "' for the clank protocol."
            maxwell_log.error(str_err)
        if connected:
            self.influx_enable["clank_protocol"] = True

    def record_maxwell_telemetry_to_influxDB(self, database, measurement, host="localhost", timeout=1):
        connected = True
        try:
            self.telm_influx_database = influx_client.MaxapiInflux(database, measurement, host, timeout=timeout)

        except (requests.exceptions.ConnectTimeout, requests.exceptions.ConnectionError) as e:
            connected = False
            print( "Could not connect to '" + host + "' with database '" + database + "'. Please check your internet connection and confirm you are on the appropriate network. Telemetry will not be recorded to influx.")
            str_err = "Could not connect to '" + host + "' with database '" + database + "' for Telemetry."
            maxwell_log.error(str_err)

        if connected:
            self.influx_enable["telemetry"] = True

    def record_clank_protocol_to_csv(self, csv_name, append_timestamp = False):
        csv_title = self._write_csv_name(csv_name, append_timestamp)
        with open(csv_title, mode='w', newline='') as csvfile:
            csv_writer = csv.DictWriter(csvfile, fieldnames=CLANK_FIELD_NAMES_CSV)
            csv_writer.writeheader()
        self.csv_config["clank_protocol"]["file_name"] = csv_title
        self.csv_config["clank_protocol"]["enable"] = True


    def record_maxwell_telemetry_to_csv(self, csv_name,append_timestamp = False):

        telm_fieldnames_prop = ["Timestamp", "message_type"]
        telm_fieldnames_thrust = ["Timestamp", "message_type"]
        telm_fieldnames_prop = _create_parser_field_array(telm_fieldnames_prop, self.prop_controller_parser)
        telm_fieldnames_thrust = _create_parser_field_array(telm_fieldnames_thrust, self.thruster_controller_parser)


        csv_title_prop = self._write_csv_name(csv_name+"_prop", append_timestamp)
        csv_title_thrust = self._write_csv_name(csv_name+"_thrust", append_timestamp)


        with open(csv_title_prop, mode='w', newline='') as csvfile:
            csv_writer = csv.DictWriter(csvfile, fieldnames=telm_fieldnames_prop)
            csv_writer.writeheader()
        with open(csv_title_thrust, mode='w', newline='') as csvfile:
            csv_writer = csv.DictWriter(csvfile, fieldnames=telm_fieldnames_thrust)
            csv_writer.writeheader()
        self.csv_config["telemetry"]["file_name_prop"] = csv_title_prop
        self.csv_config["telemetry"]["file_name_thrust"] = csv_title_thrust
        self.csv_config["telemetry"]["enable"] = True



    def write_clank_command(self, msg, data, timeout=1.0, transmit_mask = PROP_CONTROLLER_ADDRESS, attempt = 0):
        output = self.send_raw_clank_command(msg, data, transmit_mask)
        if output["status"] == "STATUS_SUCCESS":
            output = self.read_clank_response(timeout)
            valid_msg_id = self._check_msg_id_matches(msg, output)
            if valid_msg_id == False and attempt == 0:
                output = self.write_clank_command(msg,data,transmit_mask=transmit_mask, attempt=1)
        return output

    def send_raw_clank_command(self, msg, data, transmit_mask):
        self._initialize_output()
        if self.ClankProtocol.isOpen():
            if isinstance(data, list) and isinstance(msg, int):
                data_packets = _create_data_packets(data)
                for data_packet in data_packets:
                    status = self.ClankProtocol.write_data(msg, data_packet, transmit_mask)
                    self.total_clank_tx_messages = self.total_clank_tx_messages + 1
                    self.output["status"] = status["status"]
                    if status["status"] != "STATUS_SUCCESS":
                        self.output["msg_id"] = msg  # Reporting the MSG ID incase that is relevant to the user calling this
                        self.total_comm_errors = self.total_comm_errors + 1
                        maxwell_log.error("Writing to Maxwell Failed")
                        self._record_clank_data(status, "TX")  # Note there might be a concern of dropping packets here.
                        return self.output
                    else:
                        self.output["msg_id"] = status["msg_id"]
                        self.output["packet_seq_id"] = status["packet_seq"]
                        self._record_clank_data(status, "TX")  # Note there might be a concern of dropping packets here.
            else:
                maxwell_log.error("The data being sent is not in  the correct format. Please have the msg_id as a int and the data_packet_input as a list format and try again.")
                self.output["status"] = "IMPROPER_CLANK_MESSAGE_FORMAT"
                return self.output
        else:
            print("Maxapi serial is not initalized please initalize the serial port to send commands to maxwell")
            self.output["status"] = "SERIAL_NOT_INITIALIZE"

        return self.output

    def read_clank_response(self, read_timeout=1.0):
        #self._initialize_output()
        output = self._initialize_output_sep()
        read_results = self._read_received_packets(read_timeout)
        if read_results["status"] != "EMBEDDED_TIMEOUT":
            self.total_clank_rx_messages = self.total_clank_rx_messages + 1
        # Write Clank data to CSV or influx
        self._record_clank_data(read_results, "RX")
        # Check status and write telemetry to CSV or influx if needed.
        if read_results["status"] != "STATUS_SUCCESS":
            self.total_comm_errors = self.total_comm_errors + 1
            output["parsed_payload"] = {"message_type": read_results["status"]}
            self._record_maxwell_tlm("Prop_Parser", output["parsed_payload"], ["message_type"])
        else:

            #Another Bandaid need to unify msg_id info
            if (read_results["msg_id"] != MCU_DUMP_MSG_ID_RESPONSE) and (read_results["msg_id"] != GET_SW_INFO_MSG_ID_RESPONSE) and (read_results["msg_id"] != GET_MAX_HRD_INFO_MSG_ID_RESPONSE ):
                output["parsed_payload"], payload_field_names, parser_selected = self._process_tlm(read_results)
                self._record_maxwell_tlm(parser_selected, output["parsed_payload"], payload_field_names)
                output["application_status"] = 0
                #self.output["application_status"] = self.output["parsed_payload"]["tc_ack_status"] TODO need to figure this out.
            # BELOW IS A  IS A BANDAID I NEED TO MAKE A BETTER SYSTEM HERE
            else:
                output["application_status"] = 0

        # Check if clank has enough bytes via status
        if read_results["status"] != "EMBEDDED_TIMEOUT" and read_results["status"] != "NOT_ENOUGH_BYTES":
            output["msg_id"] = read_results["msg_id"]
            output["packet_seq_id"] = read_results["packet_seq"]

        output["raw_payload"] = read_results["payload"]
        output["status"] = read_results["status"]
        return output

    def _read_received_packets(self,read_timeout=1.0):
        try:
            read_results = self.Read_Queue.get(timeout=read_timeout)
        except queue.Empty:
            read_results = {
                "status": "EMBEDDED_TIMEOUT",
                "payload": [],
                "raw_clank_packet": []
            }
        else:
            queue_log.info("GET: " + str(self.Read_Queue.qsize()))
        return read_results

    def dump_mcu_tlm(self,dump_cmd,data):
        self._initialize_output()
        if self.tlm_dump_status == DUMP_RUNNING:
            self.output["status"] = "CURRENTLY_DUMPING"
            return self.output
        self.dump_tlm_list.clear() # Clearing the list because we are initializing another dump.
        tlm_dict = self.write_clank_command(dump_cmd, data)
        if tlm_dict["status"] == "STATUS_SUCCESS":
            timer_thread = threading.Thread(target=self._time_thread,)
            timer_thread.setDaemon(True)
            timer_thread.start()
            self.tlm_dump_status = DUMP_RUNNING
        return tlm_dict

    def dump_tlm_ready(self):
        if self.tlm_dump_status == DUMP_COMPLETED:
            return True
        if self.tlm_dump_status == NO_DUMP_INIT or self.tlm_dump_status == DUMP_RUNNING:
            return False

    def get_mcu_tlm(self):
        filtred_dump_results = self._create_filtered_dump(self.dump_tlm_list)
        tlm_dump = filtred_dump_results.copy() # Need to cast as list to make a copy
        self.dump_tlm_list.clear()
        self.tlm_dump_status = NO_DUMP_INIT
        return tlm_dump

    def get_serial_properties(self, detailed=False) -> dict:
        """
        This method will return the serial properties for the maxwell API. This includes\n
        1. The Baud Rate\n
        2. The Current Port\n
        3. The current Source Address (Detailed Enabled)\n
        4. The current Transmit Mask (Detailed Enabled)\n

        :param detailed: A boolean type that determines if you want a more detailed result of the current serial properties. True = The detailed view. False = The normal view.
        :type detailed: bool
        :return: A dictionary of the current serial properties. It will always return baud rate and the current port.
        """
        output = {}
        output_ser = self.ClankProtocol.get_serial_properties()
        output["baud_rate"] = output_ser["baud_rate"]
        output["current_port"] = output_ser["current_port"]
        if detailed:
            output["source_address"] = output_ser["source_address"]
            output["transmit_mask"] = output_ser["transmit_mask"]
        return output

    def clank_transmit_check_enable(self,enable_value):
        self.ClankProtocol.transmit_check_enable(enable_value)

    def shut_down_max_api(self):
        self._disable_read_thread()
        self.ClankProtocol.close_port()

    def _create_filtered_dump(self, dump_results):
        filtered_results = []
        for result in dump_results:
            if result["status"] == "STATUS_SUCCESS":
                filtered_results.append(result)
        return filtered_results

    def _write_csv_name(self,csv_name, append_timestamp):
        pathlib.Path('Outputs').mkdir(parents=True, exist_ok=True)
        if ".csv" in csv_name:
            csv_name = csv_name.replace('.csv', '')
        if append_timestamp:
            pathlib.Path(os.path.join("Outputs", str(self.script_calling_api), str(self.start_time))).mkdir(parents=True, exist_ok=True)
            directory_path = os.path.join("Outputs", str(self.script_calling_api), str(self.start_time))
        else:
            directory_path = "Outputs"
            pass
        csv_path = os.path.join(directory_path, str(csv_name) + ".csv")
        return csv_path

    def _prepare_additional_clank_info(self, status, dir):
        status["total_com_errors"] = self.total_comm_errors
        status["total_clank_messages_rx"] = self.total_clank_rx_messages
        status["total_clank_messages_tx"] = self.total_clank_tx_messages
        status.update( self.get_serial_properties())  # Take the original dictionary and adds the values from serial_properties
        status["data_direction"] = dir
        if status["status"] != "STATUS_SUCCESS":
            # Converting the raw clank array into a string to save
            status["clank_packet_string_hex"] = _convert_int_array_into_hex_string(status["raw_clank_packet"])
            if len(status["clank_packet_string_hex"]) == 0:
                status["clank_packet_string_hex"] = "NO DATA"
        if dir == "TX":
            msg_id_value = status["msg_id"]

        elif dir =="RX":
            if status["status"]!= "EMBEDDED_TIMEOUT":
                msg_id_value = status["msg_id"] - 0x8000
        try:
            if status["status"] != "EMBEDDED_TIMEOUT":
                msg_name = next(name for name, value in vars(MaxCmd).items() if value == msg_id_value)
                status["msg_id_string"] = msg_name
            else:
                status["msg_id_string"] = "EMBEDDED_TIMEOUT"
        except StopIteration:
            status["msg_id_string"] = "N/A"

        if status["status"] != "EMBEDDED_TIMEOUT":
            if status["transmit_mask"] == PROP_CONTROLLER_ADDRESS:
                status["transmit_mask_string"] = "prop_contoller"
            elif status["transmit_mask"] == THRUSTER_CONTROLLER_ADDRESS:
                status["transmit_mask_string"] = "thruster_controller"
            else:
                status["transmit_mask_string"] = "N/A"
        else:
            status["transmit_mask_string"] = "N/A"


        return status

    def _record_clank_data(self, status, dir):
        # Preparing data for manipulation
        self._prepare_additional_clank_info(status, dir)
        # Write To Influx
        if self.influx_enable["clank_protocol"]:
            self.clank_influx_database.write_clank_to_influx(status)

        # Write to CSV
        if self.csv_config["clank_protocol"]["enable"]:
            status_string = dict(status)
            if status["status"] != "EMBEDDED_TIMEOUT" and status["status"] != "NOT_ENOUGH_BYTES":
                # Making a copy of the dictionary to be used as the output for the CSV.
                # The reason for this is I want change the output form of the data to be human readable. Example switching to a hex form.
                # I do not want to  change the form of the original because data processing could be done. Thus I am copying and changing the edits.

                #  Formatting values to be string based for higher readability.

                status_string["sync_byte_version"] = str(hex(status_string["sync_byte_version"]))
                status_string["source_address"] = str(hex(status_string["source_address"]))
                status_string["transmit_mask"] = str(bin(status_string["transmit_mask"]))
                status_string["msg_id"] = str(hex(status_string["msg_id"]))
                status_string["crc"] = str(hex(status_string["crc"]))
            _write_csv(self.csv_config["clank_protocol"]["file_name"], status_string, CLANK_FIELD_NAMES_CSV)


    #TODO need to calculate and put time difference between finishing the tlm dump and actually processing the bulk clank data.
    def _record_clank_data_dump(self, serial_data, dir):
        # Preparing data for manipulation
        for packet_status in serial_data:
            self._prepare_additional_clank_info(packet_status, dir)
        # Write To Influx
        if self.influx_enable["clank_protocol"]:
            self.clank_influx_database.write_clank_to_influx(serial_data)
        # Write to CSV
        if self.csv_config["clank_protocol"]["enable"]:
            for packet_status in serial_data:
                if packet_status["status"] != "EMBEDDED_TIMEOUT" and packet_status["status"] != "NOT_ENOUGH_BYTES":
                    packet_status["transmit_mask"] = str(bin(packet_status["transmit_mask"]))  # Formating value to be string based for higher readability.
            _write_csv(self.csv_config["clank_protocol"]["file_name"], serial_data, CLANK_FIELD_NAMES_CSV)


    def _record_maxwell_tlm(self, parser_selected ,parsed_payload, payload_fieldnames):
        log_info = True
        csv_title = ""
        if parser_selected == "Prop_Parser":
            csv_title  = self.csv_config["telemetry"]["file_name_prop"]
            time_field = self.hardware_config["prop_controller"]["timestamp"]
        elif parser_selected == "Thruster_Parser":
            csv_title = self.csv_config["telemetry"]["file_name_thrust"]
            time_field = self.hardware_config["thruster_controller"]["timestamp"]
        elif parser_selected == None:
            log_info = False
        else:
            logging.warning("The data coming back is from a  unknown hardware. NOT LOGGING INFORMATION.")
            log_info = False

        if log_info == True:
            # Write To Influx
            if self.influx_enable["telemetry"]:
                self.telm_influx_database.write_tlm_to_influx(parsed_payload, time_field)


            # Write to CSV
            if self.csv_config["telemetry"]["enable"]:
                output_field = ["Timestamp"]
                output_field.extend(payload_fieldnames)  # Adding time and any other non telemetry items we want to the csv.

                if isinstance(parsed_payload, list):
                    for count in range (len(parsed_payload)):
                        parsed_payload[count] = self._check_timestamp(parsed_payload[count], time_field)

                    _write_csv(csv_title, parsed_payload, output_field)

                    for count in range (len(parsed_payload)):
                        parsed_payload[count].pop('message_type', None)

                elif isinstance(parsed_payload, dict):
                    parsed_payload = self._check_timestamp(parsed_payload,time_field)
                    _write_csv(csv_title, parsed_payload, output_field)
                    parsed_payload.pop('message_type', None)

                else:
                    maxwell_log.error("The data you gave (parsed_payload) is not a list or a dict please send a list or a dict NOT LOGGING")

    def _check_timestamp(self,packet, time_field):
        if packet["message_type"] == "Telemetry Packet" or packet["message_type"] == "Status Packet":
            if packet[time_field] > COMPANY_FOUNDATION_TIME_UNIX:
                packet["Timestamp"] = datetime.datetime.fromtimestamp(packet[time_field] / 1000000)
        return packet


    def _check_msg_id_matches(self,msg_id,output_results):
        if output_results["status"] == "STATUS_SUCCESS":
            if ACK_OFFSET | msg_id != output_results["msg_id"]:
                error_message = "RECEIVED A NACK: original message ID sent: " + str(hex(msg_id)) + " ACK Response: " + str(hex(output_results["msg_id"]))
                maxwell_log.error(error_message)
                self.Read_Queue.queue.clear()
                return False
        return True

    def _process_tlm(self, clank_output):

        if _is_telm_packet(clank_output["msg_id"]) == True:
            telm_type = "Telemetry Packet"
        else:
            telm_type = "Status Packet"

        if clank_output["source_address"] == PROP_CONTROLLER_SOURCE:
            if clank_output["msg_id"] == AUTO_TLM_RFT_MSG_ID_RESPONSE or clank_output["msg_id"] == DUMP_TLM_RFT_MSG_ID_RESPONSE or clank_output["msg_id"] == GET_TLM_RFT_MSG_ID_RESPONSE:
                parsed_telm, field_telm = _parse_data(self.thruster_controller_parser, clank_output["payload"], telm_type)
                parser_selected = "Thruster_Parser"
            else:
                parsed_telm, field_telm = _parse_data(self.prop_controller_parser, clank_output["payload"], telm_type)
                parser_selected = "Prop_Parser"

        elif clank_output["source_address"] == THRUSTER_CONTROLLER_SOURCE:
            parsed_telm, field_telm = _parse_data(self.thruster_controller_parser, clank_output["payload"], telm_type)
            parser_selected = "Thruster_Parser"

        else:
            maxwell_log.warning("This source address is not supported, and I wont be able to parse the tlm correctly. Sending null instead.")
            parsed_telm = None
            field_telm = None
            parser_selected = None
        return parsed_telm, field_telm, parser_selected

    def _initialize_output(self):
        self.output = {"status": "DEFAULT STATUS",
                       "raw_payload": [],
                       "parsed_payload": [],
                       "raw_clank_packet": []
                       }

    def _initialize_output_sep(self):
        output = {"status": "DEFAULT STATUS",
                       "raw_payload": [],
                       "parsed_payload": [],
                       "raw_clank_packet": []
                       }
        return output

    def _enable_read_thread(self):
        self.start_read_thread = True
        self.read_clank_thread.setDaemon(True)
        self.read_clank_thread.start()

    def _disable_read_thread(self):
        self.start_read_thread = False

    def _read_thread(self):
        while self.start_read_thread:
            read_results = self.ClankProtocol.read_data()
            if read_results ["status"] == "EMBEDDED_TIMEOUT" or read_results["status"] == "NOT_ENOUGH_BYTES" or not read_results:
                maxwell_log.warning("I got a embedded timeout or a not_enough_bytes. This should not happen I am not sure what is going on.")
            elif (read_results["msg_id"] == AUTO_TLM_PCU_MSG_ID_RESPONSE or read_results["msg_id"] == AUTO_TLM_RFT_MSG_ID_RESPONSE or read_results["msg_id"] == AUTO_STATUS_PCU_MSG_ID_RESPONSE) :
                # Write Clank data to CSV or influx
                self._record_clank_data(read_results, "RX")
                if read_results["status"] == "STATUS_SUCCESS":
                    parsed_payload, payload_field_names, parser_selected = self._process_tlm(read_results)
                    self._record_maxwell_tlm(parser_selected, parsed_payload, payload_field_names)
                    self.auto_rx_count = self.auto_rx_count + 1

            elif(read_results["msg_id"] == DUMP_TLM_PCU_MSG_ID_RESPONSE or read_results["msg_id"] == DUMP_TLM_RFT_MSG_ID_RESPONSE):
                self.dump_tlm_list.append(read_results)
            else:
                if self.Read_Queue.full():
                    maxwell_log.error("BUFFER IS FULL YOU ARE RECEIVING TO MANY PACKETS. DROPPING PACKET. DID NOT RECEIVE")
                else:
                    self.Read_Queue.put(dict(read_results))
                    queue_log.info("PUT: "+ str(self.Read_Queue.qsize()))

    def _time_thread(self):
        maxwell_log.info("Start time thread for dumping")
        # We are waiting until we get all the data back from the hardware
        self._wait_and_check_for_all_dump_packets_received()
        prop_parsed_payload = []
        thrust_parsed_payload = []
        maxwell_log.info("Data Collection Complete, Received All Telemetry Starting to dump to influx/csv")
        if len(self.dump_tlm_list):
            for packet in self.dump_tlm_list:
                if packet["status"] == "STATUS_SUCCESS":
                    if packet["msg_id"] == DUMP_TLM_PCU_MSG_ID_RESPONSE:
                        packet["parsed_payload"], prop_payload_field_names, parser_selected = self._process_tlm(packet)
                        prop_parsed_payload.append(packet["parsed_payload"])
                    if packet["msg_id"] == DUMP_TLM_RFT_MSG_ID_RESPONSE:
                        packet["parsed_payload"], thrust_payload_field_names, parser_selected = self._process_tlm(packet)
                        thrust_parsed_payload.append(packet["parsed_payload"])
            maxwell_log.info("Finish Parsing Data")
            maxwell_log.debug(f"All Clank Packets Count: {len(self.dump_tlm_list)}")
            self._record_clank_data_dump(self.dump_tlm_list, "RX")
            maxwell_log.info("Finish Sending Clank Data")
            maxwell_log.info("Logging Prop TLM Data")
            maxwell_log.debug(f"Prop Packets Count: {len(prop_parsed_payload)}")
            self._record_maxwell_tlm("Prop_Parser", prop_parsed_payload, prop_payload_field_names)
            maxwell_log.info("Logging Thrust TLM Data")
            maxwell_log.debug(f"Thruster Packets Count: {len(thrust_parsed_payload)}")
            self._record_maxwell_tlm("Thruster_Parser", thrust_parsed_payload, thrust_payload_field_names)
            maxwell_log.info("Finish Sending TLM Data")

        else:
            maxwell_log.warning("No data came in after a dump request. thus no data will be logged from the tlm dump")
        self.tlm_dump_status = DUMP_COMPLETED
        print("DUMP DONE")


    # This waits and checks until all the dump packets are recived from the hardware.
    # It does this in 2 ways.
    # 1. Waits until it gets the DUMP_TLM_DONE msg id. This is the fastest way because we can just exit out of it immediately.
    # 2. It looks at the length of the buffer that is being use to keep the dump data. If it stops growing larger we know we are done getting data.
    #   There is persistence of 3 counts (roughly 3 seconds) before it triggers so we don't get a accidental trigger
    def _wait_and_check_for_all_dump_packets_received(self):
        dump_done_flag = False
        wait_timeout_persistence = 0  # We are going to wait if we see it 3 times until we trigger.
        while dump_done_flag == False:
            # Get the current dump count. We will compare to see if we are still getting data.
            past_dump_count = len(self.dump_tlm_list)
            status = self.read_clank_response(1)
            # If we get a embedded timeout then all packets coming back are still dump /auto tlm packets.
            if status["status"] != "EMBEDDED_TIMEOUT":
                if status["msg_id"] == 0x8000 | MaxCmd.DUMP_TLM_DONE:
                    dump_done_flag = True
                else:
                    maxwell_log.error("We are leaking packets the packet message id was " + str(status["msg_id"]))
            else:
                # If it is a embedded timeout lets sleep some more.
                time.sleep(1)


            # This function checks if we are still receiving packets or not.
            # If the length of dump list is the same before then we have not got any new dump packets.
            if len(self.dump_tlm_list) <= past_dump_count:
                # We have a persistence of 3 to make sure we are not triggering to fast.
                if wait_timeout_persistence >= 3:
                    dump_done_flag = True
                    maxwell_log.warning("Had a timeout reach when dumping. This means we did not see our tlm flag.")
                else:
                    wait_timeout_persistence = wait_timeout_persistence + 1
            else:
                wait_timeout_persistence = 0

def _is_telm_packet (msg_id):
    if msg_id == AUTO_TLM_PCU_MSG_ID_RESPONSE or \
            msg_id == DUMP_TLM_PCU_MSG_ID_RESPONSE or \
            msg_id == TELEMETRY_MSG_ID_RESPONSE or \
            msg_id == AUTO_TLM_RFT_MSG_ID_RESPONSE or \
            msg_id == DUMP_TLM_RFT_MSG_ID_RESPONSE or \
            msg_id == GET_TLM_RFT_MSG_ID_RESPONSE:
        return True
    else:
        return False

def _write_csv(csv_title, payload_data, field_names):
    with open(csv_title, mode='a', newline='') as csvfile:
        csv_writer = csv.DictWriter(csvfile, fieldnames=field_names, extrasaction='ignore')
        if isinstance(payload_data, list):
            for dict_data in payload_data:
                _write_csv_data(dict_data,csv_writer)
        elif isinstance(payload_data, dict):
            _write_csv_data(payload_data,csv_writer)
        else:
            maxwell_log.error("The payload data coming in is not a csv or a list. NOT LOGGING TO CSV")


def _write_csv_data(dict_data,csv_writer):
    if dict_data.get("Timestamp") is None:
        dict_data["Timestamp"] = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S.%f')
    csv_writer.writerow(dict_data)
    # Getting rid of Timestamp it is not needed here.
    dict_data.pop('Timestamp', None)


def _create_data_packets(data_input):
    data_packet_chunks = []
    internal_buffer = []
    # If  the data input  buffer is 0 then we still append a buffer of nothing. This is a common edge case to cover.
    if len(data_input) == 0:
        data_packet_chunks.append(internal_buffer)
    # If there is a payload this will calculate and size it up into 255 chunks
    else:
        count = 0
        while count < len(data_input):
            if (len(data_input) - count) <= 255:
                internal_buffer.extend(data_input[count:count + len(data_input)])
                count = count + len(data_input)
            else:
                internal_buffer.extend(data_input[count:count + 255])
                count = count + 255
            if len(internal_buffer) == 255 or count == len(data_input):
                data_packet_chunks.append(internal_buffer)
                internal_buffer = []
    return data_packet_chunks

def _create_parser_field_array(telm_fieldnames, parser):
    for tlm_config in parser:
        if (tlm_config["Type"] != "pad") and (tlm_config["Type"] != "BREAK"):
            telm_fieldnames.append(tlm_config["Field"])
    return telm_fieldnames

def _prepare_parser(config_file):
    prop_controller_parser = []
    thrust_controller_parser = []
    telm_hardware_break = 0  # Used to determine where the hardware break is in the csv.
    csv_parse_file_path = os.path.join(MAXAPI_CORE_PATH, 'configurations',config_file)
    # Find how long the prop controller telm list
    if os.path.isfile(csv_parse_file_path) == False:
        print("There is no tlm_parse_config.csv file. Thus we can not parse the telmetry please add a tlm_parse_config.csv")
        sys.exit()

    with open(csv_parse_file_path, newline='') as csvfile:
        telm_csv_reader = csv.DictReader(csvfile, delimiter=',', quotechar='"')
        telm_csv = list(telm_csv_reader)
        row_count = 0
        for row in telm_csv:
            if row["Field"] == CSV_TLM_HARDWARE_BREAK:
                telm_hardware_break = row_count
            row_count = row_count + 1

        # Populating prop controller thruster
        for row_count in range(0, telm_hardware_break):
            prop_controller_parser.append(telm_csv[row_count])

        # Populating Thrust controller thruster
        if telm_hardware_break != 0:
            for row_count in range(telm_hardware_break + 1, len(telm_csv)):
                thrust_controller_parser.append(telm_csv[row_count])
        else:
            maxwell_log.critical("ERROR Could not find the telemetry for the thruster. ILL FORMATTED TELM CSV. Generate a new TELM CSV file. TELM WILL FAIL ON parsing the thruster. SHUTTING DOWN.")
            sys.exit()

    return prop_controller_parser, thrust_controller_parser


def _parse_data(parser, data_input, data_type):
    output_dict = {"message_type": data_type}
    fieldname_list = ["message_type"]
    for tlm_config in parser:
        telm_type = tlm_config["Type"]
        tlm_config["Offset"] = int(tlm_config["Offset"])
        if telm_type == "u8" or telm_type == "u16" or telm_type == "u32" or telm_type == "u64":
            tlm_config["PYType"] = "int"
        elif telm_type == "f32":
            tlm_config["PYType"] = "float"

        if telm_type != "pad" and telm_type != "BREAK" and telm_type != CSV_TLM_DERIVE_BREAK:
            unit_size = int(tlm_config["Size"])
            input_val = 0
            for x in range(unit_size):
                input_val = input_val + ((data_input[tlm_config["Offset"] + x]) << (
                            8 * ((unit_size - 1) - x)))  # Calculating the total value of the part.
            if tlm_config["PYType"] == "float":
                input_val = _float32(input_val)

            output_dict[tlm_config["Field"]] = input_val
            fieldname_list.append(tlm_config["Field"])
        if telm_type == "BREAK":
            if data_type == "Status Packet":
                return output_dict, fieldname_list

        # TODO We will fix this later This will be used to get derived values from the telemetry.
        if telm_type == CSV_TLM_DERIVE_BREAK:
            return output_dict, fieldname_list

    return output_dict, fieldname_list


def _float32(i):
    return struct.unpack(">f", struct.pack(">I", i))[0]


# Takes a int array and converts into a string of hex
def _convert_int_array_into_hex_string(int_array):
    raw_clank_packet_hex_format = ""
    for byte in int_array:
        raw_clank_packet_hex_format = raw_clank_packet_hex_format + hex(byte) + ", "

    raw_clank_packet_hex_format = raw_clank_packet_hex_format[:len(raw_clank_packet_hex_format) - 2]
    return raw_clank_packet_hex_format



def _setup_logger(name, log_file, level=logging.INFO, print_to_screen=False):
    """Function setup as many loggers as you want"""
    formatter = logging.Formatter('%(asctime)s.%(msecs)03d %(levelname)s: %(message)s')
    handler = logging.FileHandler(log_file, mode='w')
    handler.setFormatter(formatter)

    logger = logging.getLogger(name)
    logger.setLevel(level)
    logger.addHandler(handler)
    if print_to_screen == True:
        handler.stream
        logger.addHandler(logging.StreamHandler(sys.stdout))
    else:
        pass

    return logger



current_directory = os.path.dirname(os.path.abspath(__file__))
pathlib.Path(os.path.join(current_directory,"logs","")).mkdir(parents=True, exist_ok=True)
queue_log = _setup_logger('queue_log',os.path.join(current_directory, "logs","API_QUEUE.log"))
maxwell_log = _setup_logger('maxwell_api',os.path.join(current_directory, "logs","maxwell_api.log"), level=logging.DEBUG)