# Third Party Imports
import collections
import logging
import sys
import threading

import requests
from influxdb import exceptions as influx_exceptions
from reactor.influx.p4_influx_client import P4InfluxClient as p4_influx
from reactor.influx.p4_db_utils import P4InfluxDBError
from maxapi.max_errors import MaxErr

logger = logging.getLogger(__name__)


INVALID_TIME_MEASUREMENT = "neverland"



class MaxapiInflux:
    def __init__(self, database, measurement, host='localhost', port=8086, timeout=1):
        self.host = host
        self.database = database
        self.port = port
        if measurement == "default_measurement":
            logger.error(
                "This is the default measurement please update the measurement to something you want. Shutting down program.")
            sys.exit()
        else:
            self.measurement = measurement
        self.user = ''
        self.password = ''
        self.main_client = p4_influx(self.database, self.measurement, host=host, timeout=timeout) # Just to initalize the self var gets updated start_influx
        self.post_append_tags = {}
        self.annotate_msg_id_enable = False  # This enables is the option to annotate_msg_id or not.
        # This is the black list for the annotation of message ids
        self.msg_id_annotation_black_list = set()  # Sets are much faster than lists for searching (20x)
        self.packet_batch = 1000
        self.timeout = timeout
        # self.batch_time = None
        # self.batch_freq = datetime.timedelta(seconds=.100)
        # self.last_upload_time = datetime.datetime.now()
        # self.batch_enable = False
        # self.json_queue = collections.deque()
        # self.timer_thread = threading.Thread(target=self.logging_thread)
        # self.log_to_grafana = False
        self.start_influx(self.timeout)
        self.embed_timestamp_field = "UNIX_TIME_US"
        self.influx_send_retries = 3
        self.lock = threading.Lock()


    def start_influx(self, timeout_connection):
        self.main_client = p4_influx(self.database, self.measurement, timeout=timeout_connection, host=self.host)

    def update_influx(self, database, measurement, host):
        self.database = database
        self.measurement = measurement
        self.host =host
        self.main_client.update_influx(host=self.host, database=self.database, measurement=self.measurement)

    def update_post_append_tags(self, tag_dict):
        if self.post_append_tags:
            self.post_append_tags = {**self.post_append_tags, **tag_dict}
        else:
            self.post_append_tags = tag_dict
        self.main_client.set_tags(self.post_append_tags)

    def update_post_annotation_black_list(self, black_list: list, extend=False):
        """
        This function is used to update the black list of message ids that will be annotated.
        :param black_list : A list of message ids that will be annotated. It will automatically convert the lists into
        sets for faster searching.
        :param extend : If true, the black list will be extended with the new list.
        If false, the black list will be the new list.
        """
        if not extend:
            self.msg_id_annotation_black_list = set(black_list)
        else:
            self.msg_id_annotation_black_list = self.msg_id_annotation_black_list.union(set(black_list))

    def write_clank_to_influx(self, Test_Results):
        # collections.Mapping is a general solution that includes dictionary
        if isinstance(Test_Results,collections.Mapping):
            self.__create_influx_clank_json(Test_Results)
        elif isinstance(Test_Results, list):
            for packet in Test_Results:
                self.__create_influx_clank_json(packet)
        else:
            logger.error(
                "The clank packet you gave (results) is not a list or a dictionary. "
                "Please send a list or a dictionary. NOT LOGGING TO INFLUX")
            return
        return self.__write_to_influx_db()

    def write_tlm_to_influx(self, maxwell_tlm, time_key=None):
        with self.lock:
            time_field = None # time_field will be populated based on the time_key else time_field will be none.
            # collections.Mapping is a general solution that includes dictionary
            if isinstance(maxwell_tlm, collections.Mapping):
                self.data_dump = False
                if time_key in maxwell_tlm:
                    time_field = maxwell_tlm[time_key]
                self.add_tlm_point(maxwell_tlm,time_field)
            elif isinstance(maxwell_tlm, list):
                self.data_dump = True
                # print(f"In write_tlm_to_influx packet amount is {len(maxwell_tlm)}")
                for packet in maxwell_tlm:
                    if time_key in packet:
                        time_field = packet[time_key]
                    self.add_tlm_point(packet, time_field)
            else:
                logger.error( "The tlm packet you gave (results) is not a list or a dictionary. "
                              "Please send a list or a dictionary. NOT LOGGING TO INFLUX")
                return
            return self.__write_to_influx_db()

    def __create_influx_clank_json(self, Test_Results):
        # If the data coming in to influx is valid
        if Test_Results["status"] != "EMBEDDED_TIMEOUT" and Test_Results["status"] != "NOT_ENOUGH_BYTES":
            field_set = {
                "status": Test_Results["status"],  # Tagged
                "sync_byte_version": Test_Results["sync_byte_version"],
                "msg_id": Test_Results["msg_id"],  # Tagged
                "source_address": Test_Results["source_address"],  # Tagged
                "transmit_mask": Test_Results["transmit_mask"],  # Tagged
                "packet_seq": Test_Results["packet_seq"],
                "data_length": Test_Results["data_length"],
                "crc": Test_Results["crc"],
                "total_com_errors": Test_Results["total_com_errors"],
                "total_clank_messages_rx": Test_Results["total_clank_messages_rx"],
                "total_clank_messages_tx": Test_Results["total_clank_messages_tx"]
            }

            tag_set = {
                "status": str(Test_Results["status"]).lower(),  # Tagged
                "msg_id": str(Test_Results["msg_id"]).lower(),  # Tagged
                "source_address": str(Test_Results["source_address"]).lower(),  # Tagged
                "transmit_mask": str(Test_Results["transmit_mask"]).lower(),  # Tagged
                "data_direction": str(Test_Results["data_direction"]).lower(),  # Tagged
                "current_port": str(Test_Results["current_port"]).lower(),
                "baud_rate": str(Test_Results["baud_rate"])
            }

        else:
            field_set = {
                "status": Test_Results["status"],
                "total_com_errors": Test_Results["total_com_errors"],
                "total_clank_messages_rx": Test_Results["total_clank_messages_rx"],
                "total_clank_messages_tx": Test_Results["total_clank_messages_tx"]
            }
            tag_set = {
                "status": str(Test_Results["status"]).lower(),
                "data_direction": str(Test_Results["data_direction"]).lower(),
                "current_port": str(Test_Results["current_port"]).lower(),
                "baud_rate": str(Test_Results["baud_rate"])
            }

        if Test_Results["status"] != "STATUS_SUCCESS":
            field_set["raw_clank_packet"] = Test_Results["clank_packet_string_hex"]
            # We are checking here because we don't want to process annotations when we are uploading a data dump. It is to much data/
        field_set, tag_set = self._dev_clank_formatting_grafana(Test_Results, field_set, tag_set)
        self.main_client.set_tags(tag_set)
        self.main_client.add_point(field_set)


        # This function does a couple of things for the clank json file.
        # It first checks to see if msg id will be annotated or not.
        # This is done by going through the black list to see if the message id is on it.
        # If it is, it will not be annotated. If not, it will.
        # The way annotation works is triggering off of the 'msg_id_annotate_enable'
        # If it is a one grafana will be instructed to annotate that message id string.
        # If it is a zero it will not. But the Msg ID String always gets logged.
        # The blacklist is predetermined by default. But people can load in there own if desired.
        #
    def _dev_clank_formatting_grafana(self, Test_Results, field_set, tag_set):
        if self.annotate_msg_id_enable:
            if Test_Results["data_direction"] == "TX":
                field_set["msg_id_string"] = Test_Results["msg_id_string"]
                tag_set["transmit_mask_string"] = Test_Results["transmit_mask_string"]
                if Test_Results["msg_id"] in self.msg_id_annotation_black_list:
                    field_set["msg_id_annotate_enable"] = 1  # One indicates to annotate.
                else:
                    field_set["msg_id_annotate_enable"] = 0  # Zero indicates to not annotate.
        final_tag_set = {**tag_set, **self.post_append_tags}
        return field_set, final_tag_set

    def add_tlm_point(self, maxwell_tlm, time_value=None):
        try:
            self.main_client.add_point(maxwell_tlm, utc_time=time_value)
        except P4InfluxDBError as p4_err:
            print("Error: {}".format(p4_err))
            print("We are sending this packet to neverland.")
            self.main_client.set_measurement(INVALID_TIME_MEASUREMENT)
            self.main_client.add_point(maxwell_tlm)
            self.main_client.set_measurement(self.measurement)

    def __write_to_influx_db(self):
        write_successful = True
        timeout_count = 0
        while True:
            try:
                # if len(self.main_client.p4_point_list) > 1:
                #     print(f"Uploading {len(self.main_client.p4_point_list)} points")
                pts_written = self.main_client.write_points(batch_size=self.packet_batch)
                # if pts_written >1:
                #     print(f"Total points written were: {pts_written}")
            except influx_exceptions.InfluxDBClientError as error:
                logger.error(error)
                print("We had a error when sending data to influx. The error is: " + str(error))
                write_successful = False
            except requests.exceptions.Timeout:
                # print("Got a timeout")
                timeout_count += 1
                if timeout_count >= self.influx_send_retries:
                    logger.error("TIMEOUT OCCURRED ON INFLUX - The amount of points sent was: '" + str(len(self.main_client.p4_point_list)) + "' The raw data was: \n" + str(self.main_client.p4_point_list))
                    write_successful = False
                else:
                    continue  # Will stop executing the rest of the loop and go back to the top.
            break
        return write_successful


def append_error_strings(Telmetry_Data):
    if Telmetry_Data["SC_ERR_CODE1"] !=0:
            Telmetry_Data["SC_ERR_CODE1_STRING"] = next(name for name, value in vars(MaxErr).items() if value == Telmetry_Data["SC_ERR_CODE1"])
    if Telmetry_Data["SC_ERR_CODE2"] !=0:
            Telmetry_Data["SC_ERR_CODE2_STRING"] = next(name for name, value in vars(MaxErr).items() if value == Telmetry_Data["SC_ERR_CODE2"])
    if Telmetry_Data["SC_ERR_CODE3"] !=0:
            Telmetry_Data["SC_ERR_CODE3_STRING"] = next(name for name, value in vars(MaxErr).items() if value == Telmetry_Data["SC_ERR_CODE3"])
    if Telmetry_Data["ERR1_CODE"] !=0:
            Telmetry_Data["ERR1_CODE_STRING"] = next(name for name, value in vars(MaxErr).items() if value == Telmetry_Data["ERR1_CODE"])
    if Telmetry_Data["ERR2_CODE"] !=0:
            Telmetry_Data["ERR2_CODE_STRING"] = next(name for name, value in vars(MaxErr).items() if value == Telmetry_Data["ERR2_CODE"])
    if Telmetry_Data["ERR3_CODE"] !=0:
            Telmetry_Data["ERR3_CODE_STRING"] = next(name for name, value in vars(MaxErr).items() if value == Telmetry_Data["ERR3_CODE"])

    if Telmetry_Data["ERRHX1_CODE"] !=0:
            Telmetry_Data["ERRHX1_CODE_STRING"] = next(name for name, value in vars(MaxErr).items() if value == Telmetry_Data["ERRHX1_CODE"])
    if Telmetry_Data["ERRHX2_CODE"] !=0:
            Telmetry_Data["ERRHX2_CODE_STRING"] = next(name for name, value in vars(MaxErr).items() if value == Telmetry_Data["ERRHX2_CODE"])
    if Telmetry_Data["ERRHX3_CODE"] !=0:
            Telmetry_Data["ERRHX3_CODE_STRING"] = next(name for name, value in vars(MaxErr).items() if value == Telmetry_Data["ERRHX3_CODE"])

    if Telmetry_Data["MOST_FREQ_ERR_CODE"] != 0:
        Telmetry_Data["MOST_FREQ_ERR_CODE_STRING"] = next(name for name, value in vars(MaxErr).items() if value == Telmetry_Data["MOST_FREQ_ERR_CODE"])


def clean_up_tags(tag_dict):
    clean_dict = {}
    for key, value in tag_dict.items():
        key_string = str(key)
        value_string = str(value)
        key_string = clean_string_for_influx(key_string)
        value_string = clean_string_for_influx(value_string)
        clean_dict[key_string] = value_string
    return clean_dict


def clean_string_for_influx(string_val: str):
    string_clean = string_val.strip()  # Removing Trailing and leading whitespaces
    string_clean = string_clean.replace(" ", "_")  # Replacing all spaces in between to be underscores
    string_clean = string_clean.lower()  # Lowercase the whole string
    return string_clean


def __convert_int_array_into_hex_string(int_array):
    raw_clank_packet_hex_format = ""
    for byte in int_array:
        raw_clank_packet_hex_format = raw_clank_packet_hex_format + hex(byte) + ", "

    raw_clank_packet_hex_format = raw_clank_packet_hex_format[:len(raw_clank_packet_hex_format) - 2]
    return raw_clank_packet_hex_format


''' 


This functionality is meant if we want to do batching based on time copy and replace functions as sees fit.


    def disable_logging (self):
        self.log_to_grafana = False

    def enable_logging(self):
        self.timer_thread.setDaemon(True)
        self.log_to_grafana = True
        self.timer_thread.start()


    def create_clank_packet(self,Test_Results):
        # If the data coming in to influx is valid
        json_body = []
        if isinstance(Test_Results, collections.Mapping):  # collections.Mapping is a general solution that includes dictionary
            json_body.append(self.create_influx_clank_json(Test_Results))
        elif isinstance(Test_Results, list):
            for packet in Test_Results:
                json_body.append(self.create_influx_clank_json(packet))
        else:
            logger.error("The clank packet you gave (results) is not a list or a dictionary. Please send a list or a dictionary. NOT LOGGING TO INFLUX")
            return
        if self.batch_enable:
            self.json_queue = self.json_queue + json_body
            if datetime.datetime.now() - self.last_upload_time > self.batch_freq:
                self.write_to_influx_db(list(self.json_queue))  # Brackets are to convert the json value into a list.
                self.json_queue.clear()
                self.last_upload_time = datetime.datetime.now()
        else:
            self.write_to_influx_db(json_body) # Brackets are to convert the json value into a list.
            
            
            
    def create_tlm_packet(self,results,field_names):
        logger.debug("In Write to Influx")
        json_body = []
        if isinstance(results,collections.Mapping): # collections.Mapping is a general solution that includes dictionary
            json_body.append(self.create_influx_tlm_json(results,field_names))
        elif isinstance(results, list):
            for packet in results:
                json_body.append(self.create_influx_tlm_json(packet, field_names))
        else:
            logger.error("The tlm packet you gave (results) is not a list or a dictionary. Please send a list or a dictionary. NOT LOGGING TO INFLUX")
            return
        if self.batch_enable:
            self.json_queue = self.json_queue + json_body
            if datetime.datetime.now() - self.last_upload_time >  self.batch_freq:
                self.write_to_influx_db(list(self.json_queue))  # Brackets are to convert the json value into a list.
                self.json_queue.clear()
                self.last_upload_time = datetime.datetime.now()
        else:
            self.write_to_influx_db(json_body) # Brackets are to convert the json value into a list.
            
            
    def influx_still_logging(self):
        return self.timer_thread.is_alive()
            
    def logging_thread(self):
        running_timer = True
        while running_timer:
            time.sleep(self.batch_time)
            #logger.error("Length of list" +str(len(self.json_list)))
            if self.json_queue:
                result = self.write_to_influx_db(list(self.json_queue))
                if result == True:
                    self.json_queue.clear()

            if not self.log_to_grafana:
                running_timer = False
            
'''
