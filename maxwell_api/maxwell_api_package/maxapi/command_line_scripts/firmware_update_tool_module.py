import os
import pathlib
import queue
import shutil
import socket
import sys
import threading
import time

from natsort import natsorted

import maxapi.maxapi_sw_loader as soft_load
from maxapi.maxwell_information_processor import p4_version_parser


def firmware_update_tool(board_type, port, baud_rate, srec_file, firmware_setting):
    if srec_file != "":
        #"That means there is a srec they want to load. Lets try to load it."
        print("Loading Software", flush = True)
        run_load_software(com_port=port, baud_rate=baud_rate, hardware_type=board_type, srec=srec_file)

        # Check if the user want to load the local firmware from the api.
    elif firmware_setting =="api":
        current_directory = os.path.dirname(os.path.abspath(__file__))
        last_folder_index = current_directory.rfind(os.sep)
        maxapi_directory = current_directory[:last_folder_index]
        maxapi_package_latest_firmware_directory = os.path.join(maxapi_directory, 'firmware','standard',str(board_type))
        array_of_firmware_folders = os.listdir(maxapi_package_latest_firmware_directory)
        found_file = False
        for file in array_of_firmware_folders:
            if ".srec" in file:
                print(file, flush = True)
                firmware_file = file
                found_file = True
        if found_file == False:
            print("Could not find file. in my own directory. It is not you it's me."
                  "\nUnless you went rouge and deleted the firmware file from the pip package."
                  "If that is the case shame on you. If not talk to p4 developers.\nShutting down...", flush = True)
            sys.exit()

        total_srec_path = os.path.join(maxapi_package_latest_firmware_directory, str(firmware_file))
        run_load_software(com_port=port, baud_rate=baud_rate, hardware_type=board_type, srec=total_srec_path)


    elif firmware_setting =="cur_folder":
        run_load_software(com_port=port, baud_rate=baud_rate, hardware_type=board_type, srec="")



    elif firmware_setting == "pip" or firmware_setting== "pip_sync":
        # If no file is specifed lets try to download one from the server.
        pip_status = connect_to_pip_server()
        if pip_status == True:
            print("Downloading Firmware!", flush = True)
            if firmware_setting == "pip":
                firmware_api_sync_flag = False
            else:
                firmware_api_sync_flag = True
            latest_fimrware_dict, latest_firmware_path = find_latest_firmware(board_type,firmware_api_sync_flag)
            #TODO check if we need to get the firmware first. We might already have it.
            srecord_location = get_new_firmware(latest_fimrware_dict, latest_firmware_path)
            print("Download Complete!", flush = True)
            print("Starting Software Load...", flush = True)
            run_load_software(com_port=port, baud_rate=baud_rate, hardware_type=board_type, srec=srecord_location)
        else:
            print("Could not connect to network...", flush = True)
            valid_input = False
            while not valid_input:
                input_value = input("Since we can not connect to the network we have 2 options for you."
                                    "\n1) Flash the hardware with firmware this api came with (Will always be in sync.)"
                                    "\n2) Flash the hardware with firmware that is in your local directory."
                                    "\n3) Quit the application.\n"
                                    "Input:")
                input_value = input_value.strip()
                if input_value == '1' or input_value == '2'or input_value == '3':
                    valid_input = True
            if input_value == '1':
                current_directory = os.path.dirname(os.path.abspath(__file__))
                last_folder_index = current_directory.rfind(os.sep)
                maxapi_directory = current_directory[:last_folder_index]
                maxapi_package_latest_firmware_directory = os.path.join(maxapi_directory, 'firmware', 'standard',str(board_type))
                array_of_firmware_folders = os.listdir(maxapi_package_latest_firmware_directory)
                found_file = False
                for file in array_of_firmware_folders:
                    if ".srec" in file:
                        print(file, flush = True)
                        firmware_file = file
                        found_file = True
                if found_file == False:
                    print("Could not find file. in my own directory. It is not you it's me."
                          "\nUnless you went rouge and deleted the firmware file from the pip package."
                          "If that is the case shame on you. If not talk to p4 developers.\nShutting down...", flush = True)
                    sys.exit()

                total_srec_path = os.path.join(maxapi_package_latest_firmware_directory, str(firmware_file))
                run_load_software(com_port=port, baud_rate=baud_rate, hardware_type=board_type, srec=total_srec_path)


            elif input_value == '2':
                run_load_software(com_port=port, baud_rate=baud_rate, hardware_type=board_type, srec="")

            else:
                print("Sounds good. Api will not load software. Shutting down...", flush = True)
                sys.exit()
            print("Can't download Firmware..", flush = True)




def run_load_software(com_port, baud_rate, hardware_type, srec):
    status_queue = queue.Queue()
    display_thread = threading.Thread(target=loading_thread, args=(status_queue,))
    display_thread.setDaemon(True)
    display_thread.start()

    load_passed = soft_load.load_software(com_port=com_port, baud_rate=baud_rate, hardware_type=hardware_type, srec=srec, status_queue=status_queue)
    status_queue.put('q')
    print("")
    print("")
    if load_passed == False:
        print("Failed to update software..", flush=True)
    else:
        print("Software Loading Complete!\n\nPlease remember to power cycle the Maxwell Unit to use the new firmware!", flush=True)

    return


def find_latest_firmware(hardware_type, firmware_api_sync_flag):
    bender_path = os.path.join('\\\\10.0.0.250', 'bender', 'phase_four_repo', 'firmware',str(hardware_type))
    array_of_firmware_folders = os.listdir(bender_path)
    if firmware_api_sync_flag:
        print("This feature is not currently supported. Shutting Down Program")
        sys.exit()
        # valid_synced_folders = [s for s in array_of_firmware_folders if "abc" in s]
        # array_of_firmware_folders = valid_synced_folders

    sorted_firmware_folders = natsorted(array_of_firmware_folders, reverse=True)
    latest_firmware_folder= sorted_firmware_folders[0]
    firmware_dict = {}
    fw_version_info = p4_version_parser(latest_firmware_folder)


    if not fw_version_info:
        print("The firmware name is not formatted correctly from the server. Talk to one of the developers why this is.", flush = True)
        sys.exit()
    # This is not ideal we need a new way to get hardware information.
    firmware_dict["version_info"]= fw_version_info
    if "pcu_sw" in fw_version_info.label or "ec_sw" in fw_version_info.label:
        firmware_dict["hardware"] = "pcu_sw"
    elif "thr_sw" in fw_version_info.label or "tc_sw" in fw_version_info.label:
        firmware_dict["hardware"] = "thr_sw"
    elif "max_api" in fw_version_info.label:
        firmware_dict["hardware"] = "max_api"
    else:
        firmware_dict["hardware"] = fw_version_info.label

    firmware_dict["file_name_raw"] = latest_firmware_folder
    firmware_dict["file_name"] = "bl_full_" +latest_firmware_folder + ".srec"
    latest_firmware_path = os.path.join(str(bender_path), str(latest_firmware_folder), str(firmware_dict["file_name"]))  # This is the latest firmware we need
    return firmware_dict, latest_firmware_path




def get_new_firmware(firmware_dict, firmware_path):
    current_directory = os.path.dirname(os.path.abspath(__file__))
    last_folder_index = current_directory.rfind(os.sep)
    maxapi_directory = current_directory[:last_folder_index]
    maxapi_package_latest_firmware_directory = os.path.join(maxapi_directory, 'firmware', 'latest', str(firmware_dict["hardware"]))
    pathlib.Path(maxapi_package_latest_firmware_directory).mkdir(parents=True, exist_ok=True)
    delete_contents_in_directory(maxapi_package_latest_firmware_directory)
    maxapi_lastest_firmware_destination = os.path.join(maxapi_package_latest_firmware_directory, str(firmware_dict["file_name"]))
    source = firmware_path
    destination = maxapi_lastest_firmware_destination
    shutil.copyfile(source, destination)

    firmware_file = pathlib.Path(maxapi_lastest_firmware_destination)
    if not firmware_file.is_file():
        copy_success = False
        count = 0
        while count <3 and copy_success == False:
            shutil.copyfile(source, destination)
            firmware_file = pathlib.Path(maxapi_lastest_firmware_destination)
            if firmware_file.is_file():
                copy_success = True
            count = count + 1
        if copy_success == False:
            print("Failed to copy file over from server. Please try again or perform a upadate the firmware that came with the api.", flush = True)
            sys.exit()

    return maxapi_lastest_firmware_destination



def connect_to_pip_server():
    print("Connecting to server....", flush = True)
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.settimeout(1)
    result = sock.connect_ex(('10.0.0.250', 8080))
    if result == 0:
        print("Connected to Server!", flush = True)
        sock.close()
        return True
    else:
        print("Can't connect to server.", flush = True)
        sock.close()
        return False


def delete_contents_in_directory(folder):
    for filename in os.listdir(folder):
        file_path = os.path.join(folder, filename)
        try:
            if os.path.isfile(file_path) or os.path.islink(file_path):
                os.unlink(file_path)
            elif os.path.isdir(file_path):
                shutil.rmtree(file_path)
        except Exception as e:
            print('Failed to delete %s. Reason: %s' % (file_path, e), flush = True)



def loading_thread(status_queue: queue):
    time_between_visual = 0.250
    in_queue = False
    percent_increase = 0
    continue_visual_thread = True
    while continue_visual_thread:
        try:
            status = status_queue.get()
            in_queue = True
        except queue.Empty:
            in_queue = False
        if in_queue:
            if status != 'q':
                percent_increase_old = percent_increase
                percent_increase = int(status) * 10
                count = percent_increase_old
                if percent_increase > 65:
                    time_between_visual = .300
                while count <= percent_increase:
                    progress(count)
                    time.sleep(time_between_visual)
                    count = count + 1
            if percent_increase == 100 or status == 'q':
                continue_visual_thread = False


def progress(percent=0, width=30):
    left = width * percent // 100
    right = width - left
    print('\r[', '#' * left, ' ' * right, ']',
          f' {percent:.0f}%',
          sep='', end='', flush=True)
