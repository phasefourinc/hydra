import json
import arrow
from rich import print

from maxapi.maxwell_information_processor import get_all_the_software_info, get_current_app_info, read_hardware_maxwell_record
from maxapi.mr_maxwell import MrMaxwell, MaxConst, MaxCmd

HARDWARE_KEY = "hardware"
SOFTWARE_KEY = "software_information"
SN_KEY = "serial_number"
DEVICES_TOP_KEY = "device_information"
def gather_software_information(com_port, baud, board_select,write_json):

    if board_select == "pcu":
        board_type = MaxConst.PROP_CONTROLLER_ADDRESS
        board_string = "Engine Controller"
        file_name = "engine_controller_sw_info.json"

    elif  board_select == "thr":
        board_type = MaxConst.THRUSTER_CONTROLLER_ADDRESS
        board_string = "Thruster Controller"
        file_name = "thruster_controller_sw_info.json"

    elif board_select == "max":
        board_string = "Maxwell System"
        file_name = "maxwell_controller_sw_info.json"

    else:
        print("The board option that was selected is incorrect. Please select {pcu,thr,max}")
        status = "INVALID HARDWARE OPTION"
        return status, "N/A"

    mr_max = MrMaxwell(com_port, baud, "sandbox", "default_measurement", enable_startup_scripts=False, enable_csv=False, enable_influx=False)
    final_sw_info_json = {"record_name":"P4 Software Record","schema_version": 2, "time":arrow.utcnow().to('US/Pacific').for_json()}
    status = "Script Error" # We should never get this. If we do something is wrong with this script.

    if board_select != "max":
        sw_information, status = get_all_the_software_info(mr_max, hardware_select=board_type)

        if status == "STATUS_SUCCESS":
            sw_string_info = format_dictonary_to_output(sw_information, board_string)
            print(sw_string_info)
            final_sw_info_json[DEVICES_TOP_KEY] = {HARDWARE_KEY: board_string, SN_KEY: "null",SOFTWARE_KEY: sw_information}

    #This section is if they selected maxwell. This check will get both prop controller and thruster controller software info.
    else:
        prop_controller_string = "The Prop Controller Is Not Connected."
        thruster_controller_string = "The Thruster Controller Is Not Connected."
        top_level_information_json = {HARDWARE_KEY: "Top Level", "serial_number":"null"}
        pcu_sw_information_json = {HARDWARE_KEY: "Engine Controller", SN_KEY: "null", SOFTWARE_KEY: "N/A"}
        thr_sw_information_json = {HARDWARE_KEY: "Thruster Controller", SN_KEY: "null", SOFTWARE_KEY: "N/A"}

        if mr_max.is_prop_controller_connected():
            pcu_sw_information, status = get_all_the_software_info(mr_max, hardware_select=MaxConst.PROP_CONTROLLER_ADDRESS)
            if status == "STATUS_SUCCESS":

                prop_controller_string = format_dictonary_to_output(pcu_sw_information, "Engine Controller")
                pcu_sw_information_json[SOFTWARE_KEY] = pcu_sw_information
            if write_json:
                sn = get_pcba_serial_number_old(mr_max, MaxConst.PROP_CONTROLLER_ADDRESS)
                pcu_sw_information_json[SN_KEY] = str(sn)

        if mr_max.is_thruster_controller_connected():
            thr_sw_information, status = get_all_the_software_info(mr_max, hardware_select=MaxConst.THRUSTER_CONTROLLER_ADDRESS)
            if status == "STATUS_SUCCESS":
                thruster_controller_string = format_dictonary_to_output(thr_sw_information, "Thruster Controller")
                thr_sw_information_json[SOFTWARE_KEY] = thr_sw_information
            if write_json:
                sn = get_pcba_serial_number_old(mr_max, MaxConst.PROP_CONTROLLER_ADDRESS)
                thr_sw_information_json[SN_KEY] = str(sn)

        maxwell_string = prop_controller_string + "\n\n" + thruster_controller_string

        final_sw_info_json[DEVICES_TOP_KEY] =[top_level_information_json,  pcu_sw_information_json, thr_sw_information_json]
        print(maxwell_string)

    if write_json:
        # We are creating a custom dictonary for the json file. This will make it easier to determine what hardware it is meant for, when doing analysis on it.
        # We are creating a structure of json of  {hardware: prop, software info: [soft1,soft2, etc..]}

        print("The software information for the " + board_string + " will be located in " + file_name + "\n")

        _write_json_file(file_name, final_sw_info_json)


    return status, file_name



def format_dictonary_to_output(software_directory,hardware_string):

    flash_dictonary_keys = ["field_name","software_image_type","software_length","software_crc","software_version","hardware_type", "product_type"]
    sram_dictonary_keys = ["boot_select","app_select"]
    data_string = ""
    block_firmware_str =  "NOT_SET" # A simple variable that will check if all the firmware versions are for a given block
    for directory in software_directory:
        # This set of code will check if we can detemrine the hardware version of the software.
        # Let get the hardware specfic code first which will contain block information.
        if directory["software_image_type"] =="EXE!" or directory["software_image_type"] =="PARM":
            # If this is first time we are seeing a record with hardware info lets capture it.
            if block_firmware_str == "NOT_SET":
                block_firmware_str = directory["product_type"]
            # If we already have one let's make sure it is the same.
            else:
                # If it is different set it to none there is a difference so we do not know the hardware.
                if directory["product_type"] != block_firmware_str:
                    block_firmware_str = None


        for fields in flash_dictonary_keys:
            data_string = data_string + str(fields) + ": " + str(directory[fields]) + "\n"

        if directory["field_name"] =="Current Application":
            for fields in sram_dictonary_keys:
                data_string = data_string + str(fields) + ": " + str(directory[fields]) + "\n"

        data_string = data_string + dash_creator(30) + "\n"


    if block_firmware_str is None:
        block_string = ""
    else:
        if "B2" in block_firmware_str:
            block_string =  "Firmware Type: Block 2 Firmware"
        else:
            block_string = "Firmware Type: Block 1 Firmware"
    string_to_print = dash_creator(30)+"\n"+hardware_string+"\n" + block_string+"\n"+dash_creator(30) + "\n"
    return string_to_print +data_string



def dash_creator(num_of_dash):
    dash_string = ""
    for x in range(num_of_dash):
        dash_string = dash_string + "-"
    return dash_string

def _write_json_file (file_name, data_struct, mode ='w'):
    with open(file_name, mode=mode, newline='\n') as json_file:
        json_string = json.dumps(data_struct, indent=4, sort_keys=False)
        json_file.write(str(json_string))

# This gets the serial number the old way via the flash table or hardware records.
def get_pcba_serial_number_old(mr_max:MrMaxwell, transmit_mask):
    serial_num = "null"
    attempt = 3
    # We are going to try 3 times for best effort.
    for x in range(attempt):
        payload = mr_max.send_maxwell_message(MaxCmd.GET_MAX_HRD_INFO, [0x00], transmit_mask=transmit_mask)
        if payload["status"] == "STATUS_SUCCESS":
            hardware_rec = read_hardware_maxwell_record(payload["raw_payload"], 0x00)
            # If the field_name in the hardware_record not null that means the record is has some data. Lets extract it.
            if hardware_rec["field_name"] != 'null':
                serial_num = hardware_rec["serial_number"]
            break
        else:
            # let us try again.
            pass
    return serial_num

