import time
from rich import print
from dataclasses import dataclass
#import maxapi.maxwell_tlm_processor as tlm_processor
from maxapi.maxwell_information_processor import parse_maxwell_header,read_hardware_maxwell_record
from maxapi.mr_maxwell import MrMaxwell as mb1, MaxConst, MaxState
from maxapi.mr_maxwell_b2 import MrMaxwell as mb2, MaxTlm as mb2_tlm
from maxapi.max_errors import MaxErr
from enum import Enum


# A small class to help populate the right string for a given embedded system.
@ dataclass
class maxwell_tlm:
    ec_ack_status : str
    ec_app_select: str
    ec_pt_select: str
    ec_bl_select: str
    ec_state: str
    ec_total_err_cnt: str
    ec_first_sc_err: str
    tl_sn: str
    pprop: str
    need_reset: str

    tc_ack_status: str
    tc_app_select: str
    tc_pt_select: str
    tc_bl_select:str
    tc_state: str
    tc_total_err_cnt: str
    tc_first_err: str
    tc_adc0:str


class MaxImageSelect(Enum):
    Primary = 1
    Secondary = 2
    Golden = 3

def check_hardware_connection_tool(com_port, baud, max_block= "block2"):
    status = True
    if max_block == "block1":
        mr_max = mb1(com_port, baud, "sandbox", "default_measurement", enable_startup_scripts=False, enable_csv=False, enable_influx=False)
    elif max_block == "block2":
        mr_max = mb2(com_port, baud, "sandbox", "default_measurement", enable_startup_scripts=False, enable_csv=False, enable_influx=False)
        # If we do not know what it is lets just say it is block 2 for now.
    else:
        mr_max = mb2(com_port, baud, "sandbox", "default_measurement", enable_startup_scripts=False, enable_csv=False, enable_influx=False)
    tlm_config = populate_tlm_per_block(max_block)
    if mr_max.is_prop_controller_connected():

        if not check_hardware_connection(mr_max,MaxConst.PROP_CONTROLLER_ADDRESS, tlm_config.ec_ack_status):
            prop_controller_string = dash_creator(40) + "\nThe Prop Controller Is Not Connected.\n" + dash_creator(40) + "\n"
            prop_controller_string =  prop_controller_string + "Can not maintain a stable connection with the Prop Controller"
            status = False
        else:
            prop_controller_string = dash_creator(40) + "\nThe Prop Controller Is Connected.\n" +dash_creator(40)+ "\n"

            com_status = [False, False, False]

            current_app_raw = mr_max.get_sw_info("Current_App", transmit_mask=MaxConst.PROP_CONTROLLER_ADDRESS)
            if current_app_raw["status"] == "STATUS_SUCCESS":
                current_app_parsed = parse_maxwell_header(current_app_raw, "Current_App")

                prop_controller_string = prop_controller_string + "Current App Version: '" + str(current_app_parsed["software_version"]) + "'\n"
                com_status[0] = True
            else:
                prop_controller_string = prop_controller_string + "Current App Version: '" +  "COULD NOT READ SOFTWARE VERSION'\n"

            payload = mr_max.get_max_hrd_info(0, transmit_mask=MaxConst.PROP_CONTROLLER_ADDRESS)
            if payload["status"] == "STATUS_SUCCESS":
                prop_controller_pcba_hardware = read_hardware_maxwell_record(payload["raw_payload"], 0)
                prop_controller_string = prop_controller_string + "Prop Controller Serial Number: '" + str(prop_controller_pcba_hardware["serial_number"]) + "'\n"
                com_status[1] = True
            else:
                prop_controller_string = prop_controller_string + "Prop Controller Serial Number: '" + "COULD NOT READ SERIAL NUMBER'\n"

            prop_telm = mr_max.get_tlm(MaxConst.PROP_CONTROLLER_ADDRESS)

            if prop_telm["status"] == "STATUS_SUCCESS":
                ptank = prop_telm["parsed_payload"][tlm_config.pprop]
                string_state = MaxState(prop_telm["parsed_payload"][tlm_config.ec_state]).name
                app_select = MaxImageSelect(prop_telm["parsed_payload"][tlm_config.ec_app_select]).name
                pt_select = MaxImageSelect(prop_telm["parsed_payload"][tlm_config.ec_pt_select]).name
                bl_select = prop_telm["parsed_payload"][tlm_config.ec_bl_select]
                total_error_count = prop_telm["parsed_payload"][tlm_config.ec_total_err_cnt]
                if total_error_count == 0:
                    error_string = "No Errors"
                elif total_error_count == 1:
                    first_error_name = find_error_name(prop_telm["parsed_payload"][tlm_config.ec_first_sc_err])
                    error_string = str(total_error_count) + " Error\n  -- First Error is: " + str(first_error_name)
                else:
                    first_error_name = find_error_name(prop_telm["parsed_payload"][tlm_config.ec_first_sc_err])
                    error_string = str(total_error_count) + " Errors\n  -- First Error is: " + str(first_error_name)

                prop_controller_string = prop_controller_string + "Image Select: FSW:" + str(app_select) + ", PT:" + str(pt_select) + ", BL:"+str(bl_select)+"\n"
                prop_controller_string = prop_controller_string + "Prop Controller State: '" + str(string_state) + "'\n"
                prop_controller_string = prop_controller_string + "Prop Controller Errors: " + str(error_string) + "\n"

                if prop_telm["parsed_payload"][tlm_config.need_reset] == 1:
                    prop_controller_string = prop_controller_string + "NOTE: THE ENGINE CONTROLLER NEEDS A RESET\n"
                prop_controller_string = prop_controller_string + "PTank: " +  str(round(ptank,3)) + " psi\n"
                com_status[2] = True
            else:
                prop_controller_string = prop_controller_string + "Prop Controller Telemetry: ERROR COULD NOT GET TELEMETRY\n"



            for cmd in com_status:
                if cmd == False:
                    status = False
    else:
        prop_controller_string = dash_creator(40) + "\nThe Prop Controller Is Not Connected.\n" + dash_creator(40) + "\n"
    print(prop_controller_string)

    if mr_max.is_thruster_controller_connected():
        if not check_hardware_connection(mr_max, MaxConst.THRUSTER_CONTROLLER_ADDRESS, tlm_config.tc_ack_status):
            thrust_controller_string = dash_creator(40) + "\nThe Thruster Controller Is Not Connected.\n" + dash_creator(40) + "\n"
            thrust_controller_string = thrust_controller_string + "Can not maintain a stable connection with the Thruster Controller"
            status = False
        else:
            thrust_controller_string = dash_creator(40) + "\nThe Thruster Controller Is Connected.\n" + dash_creator(40) + "\n"

            com_status = [False, False, False]
            current_app_raw = mr_max.get_sw_info("Current_App", transmit_mask=MaxConst.THRUSTER_CONTROLLER_ADDRESS)
            if current_app_raw["status"] == "STATUS_SUCCESS":
                current_app_parsed = parse_maxwell_header(current_app_raw, "Current_App")

                thrust_controller_string = thrust_controller_string + "Current App Version: '" + str(current_app_parsed["software_version"]) + "'\n"
                com_status[0] = True
            else:
                thrust_controller_string = thrust_controller_string + "Current App Version: '" + "COULD NOT READ SOFTWARE VERSION'\n"

            payload = mr_max.get_max_hrd_info(0, transmit_mask=MaxConst.THRUSTER_CONTROLLER_ADDRESS)
            if payload["status"] == "STATUS_SUCCESS":
                com_status[1] = True
                thrust_controller_pcba_hardware = read_hardware_maxwell_record(payload["raw_payload"], 0)
                thrust_controller_string = thrust_controller_string + "Thruster Controller Serial Number: '" + str(thrust_controller_pcba_hardware["serial_number"]) + "'\n"
            else:
                thrust_controller_string = thrust_controller_string + "Thruster Controller Serial Number: '" + "COULD NOT READ SERIAL NUMBER'\n"

            thrust_telm = mr_max.get_tlm(MaxConst.THRUSTER_CONTROLLER_ADDRESS)

            if thrust_telm["status"] == "STATUS_SUCCESS":
                tc_adc0 = thrust_telm["parsed_payload"][tlm_config.tc_adc0]
                string_state = MaxState(thrust_telm["parsed_payload"][tlm_config.tc_state]).name
                app_select = MaxImageSelect(thrust_telm["parsed_payload"][tlm_config.tc_app_select]).name
                pt_select = MaxImageSelect(thrust_telm["parsed_payload"][tlm_config.tc_pt_select]).name
                bl_select = thrust_telm["parsed_payload"][tlm_config.tc_bl_select]
                total_error_count = thrust_telm["parsed_payload"][tlm_config.tc_total_err_cnt]
                if total_error_count ==0:
                    error_string = "No Errors"
                elif total_error_count == 1:
                    first_error_name = find_error_name(thrust_telm["parsed_payload"][tlm_config.tc_first_err])
                    error_string = str(total_error_count) + " Error\n  -- First Error is: " + str(first_error_name)
                else:
                    first_error_name = find_error_name(thrust_telm["parsed_payload"][tlm_config.tc_first_err])
                    error_string = str(total_error_count) + " Errors\n  -- First Error is: " + str(first_error_name)

                thrust_controller_string = thrust_controller_string + "Image Select: FSW:" + str(app_select) + ", PT:" + str(pt_select) + ", BL:"+str(bl_select)+"\n"
                thrust_controller_string = thrust_controller_string + "Thruster Controller State: '" + str(string_state) + "'\n"
                thrust_controller_string = thrust_controller_string + "Thruster Controller Errors: " + str(error_string) + "\n"

                thrust_controller_string = thrust_controller_string + "Thruster Controller Internal Temp: " + str(round(tc_adc0,3)) + " C\n"
                com_status[2] = True
            else:
                thrust_controller_string = thrust_controller_string + "Thruster Controller Telemetry: ERROR COULD NOT GET TELEMETRY\n"

            for cmd in com_status:
                if cmd == False:
                    status = False
    else:
        thrust_controller_string = dash_creator(40) + "\nThe Thruster Controller Is Not Connected.\n" + dash_creator(40) + "\n"

    print(thrust_controller_string)

    return status



def populate_tlm_per_block(block_num:str) -> maxwell_tlm:
    if block_num == "block1":
        tlm_config = maxwell_tlm(
            ec_ack_status = "ACK_STATUS",
            ec_app_select = "APP_SELECT",
            ec_pt_select = "PT_SELECT",
            ec_bl_select = "BL_SELECT",
            ec_state="STATE",
            ec_total_err_cnt= "TOTAL_ERR_CNT",
            ec_first_sc_err= "SC_ERR_CODE1",
            tl_sn = None,
            pprop= "PTANK",
            need_reset= "NEEDS_RESET",
            tc_ack_status = "ACK_STATUS",
            tc_app_select= "RFT_APP_SELECT",
            tc_pt_select= "RFT_PT_SELECT",
            tc_bl_select = "RFT_BL_SELECT",
            tc_state= "RFT_STATE",
            tc_total_err_cnt= "RFT_TOTAL_ERR_CNT",
            tc_first_err= "RFT_ERR1_CODE",
            tc_adc0= "RFT_TADC0"
        )
    else:
        tlm_config = maxwell_tlm(
            ec_ack_status = "ec_ack_status",
            ec_app_select = "ec_app_select",
            ec_bl_select = "ec_bl_select",
            ec_pt_select = "ec_pt_select",
            ec_state = "ec_state",
            ec_total_err_cnt= "ec_err_cnt",
            ec_first_sc_err= "sc_ec_err1_code",
            tl_sn = "sc_sn",
            pprop= "sc_pprop",
            need_reset= "ec_needs_reset",
            tc_ack_status = "tc_ack_status",
            tc_app_select= "tc_app_select",
            tc_pt_select= "tc_pt_select",
            tc_bl_select= "tc_bl_select",
            tc_state = "tc_state",
            tc_total_err_cnt= "tc_err_cnt",
            tc_first_err= "tc_err1_code",
            tc_adc0= "tc_tadc0"
        )
    return tlm_config

def check_ack(output, ack_status_str:str):
    if output["status"] == "STATUS_SUCCESS" and output["parsed_payload"][ack_status_str] == 0:
        return True
    else:
        return False



def check_hardware_connection(mr_max, hardware, ack_status_str:str):
    num_of_transmits = 5
    for x in range(num_of_transmits):
        output_status = mr_max.get_status(transmit_mask=hardware)
        if not check_ack(output_status, ack_status_str):
            return False
    return True


def dash_creator(num_of_dash):
    dash_string = ""
    for x in range(num_of_dash):
        dash_string = dash_string + "-"
    return dash_string


def find_error_name(error_id):
    try:
        msg_name = next(name for name, value in vars(MaxErr).items() if value == error_id)
    except:
        # We can't find the error just put the name in.
        msg_name = error_id
    return str(msg_name)