# First Party Imports
import inspect
import struct

# Third Party Imports
import sys
import time
import os
import csv
from enum import Enum, auto
import maxapi.sentry_logger
from maxapi import maxwell_api_core
from maxapi.max_cmd_id import MaxCmd
from maxapi.max_errors import MaxErr
from maxapi.maxwell_information_processor import parse_maxwell_header, read_hardware_maxwell_record, p4_version_parser, P4VersionInfo

ACK_OFFSET = 0x8000

'''
This decorator is used to build some singleton functionality to the mr maxwell.

This is mainly meant for Bender and test runner functionality. Where we only want the test runner maxapi to be used.
Since most scripts use the maxapi as a global variable for ease of use it makes sense to keep the test alone for flexibility.
However we still need a mechanism to use the test runner's mr_maxwell instead of the one that is created by the test. So adding a singleton pattern enables us to not interfere with the test script but still have control of mr_maxwell.

Note this is a modified singleton pattern. Instead of only allowing one mr_maxwell object. We only return a already created mr_maxwell object based on certian critera.
This decorator will check if any of the mr_maxwell instances want to be a singleton.
If one or or more already been initialized it will check if the "singleton_status" is a singleton, (more on that in a minuite)
NOTE: This singleton pattern has been updated to remove the ability to automatically return a instance based on the same comport. 
        - We had issue where people want to recreate mr_maxwell for a differen unit (we powered off and now a new unit is connected and we say connect) where it would carry over.
        - As well as users trying to delete the instance by shuting down mrmaxwell (which disable the ports) only to create a object with the same instance and that port being disabled.
        - If it has the same com port as a previous initialized instance it will return that instance back.

In the decorator each mr_maxwell object that wants to be a singleton is checked for this singleton_status value.
If it the singleton type condition is true with the new object being created, the singleton mr_maxwell object will be returned instead of creating a new mr_maxwell object.
Note that a mr_maxwell object is not the list of singletons that is checkd when a new object is made, by default it only gets added if they change there singleton status via the 'update_singleton_status()'
singleton_status is a status that can be changed by using the "update_singleton_status()" method. This will change the property of maxapi to be a singleton or not.
There is 3 options.
1. NO_SINGLETON: This is the default option and it says to the decorator that this object is not a singleton. It will not be included in the list of singletons and will never return this instance to a new object.
2. MAIN_SINGLETON: This mode indicates that this mr_maxwell is a singleton and should be returned if another mr_maxwell is created.
3. SINGLETON_WITH_MATCHING_BAUD_RATE: This mode indicates that this mr_maxwell is a singleton if the baud rate of the new mr_maxwell object's baud rate is the same as this obejcts baud rate.  This can help with secenrios where you want a two singleton with different baud_rates.
4. SINGLETON_WITH_MATCHING_COMPORT: This mode indicates that mr_maxwell is a singleton if the com port of the new Mr_maxwell object's comport is the same as this objects comport.


So in summary at the start of each mr_maxwell initialization it will run this decorator to see:
 1. If any of the previous mr_maxwell objects have there singleton_status changed. If it does, return the object instead of creating a new one. (For Bender)
 2. If the com port from previous mr_maxwell objects is the same as the argument of the new mr_maxwell object being created. If it is return the object that has that com port. (To avoid re intlaizing the same com port)
 3. If both of those conditions do not exist simply create a new mr_maxwell object.
'''
def maxapi_singleton_proccess(cls, *args, **kw):
    cls.mr_max_singleton_instance = []

    def _maxapi_singleton_proccess(*args, **kw):
        if len(args) >2:
            COMPORT = args[0] # When AUTO COM happens you can add this here.
            BAUD_RATE = args[1]
        else:
            COMPORT= kw["com_port"]
            BAUD_RATE = kw["com_baud"]

        for instance in cls.mr_max_singleton_instance:
            if instance.singleton_status == MaxConst.MAIN_SINGLETON:
                return instance
            elif instance.singleton_status == MaxConst.SINGLETON_WITH_MATCHING_BAUD_RATE:
                if instance.max_api.ClankProtocol.baudrate == int(BAUD_RATE):
                    return instance
            # If they have the same COMPORT then return that object because we have a object that is using that comport.
            elif instance.singleton_status == MaxConst.SINGLETON_WITH_MATCHING_COMPORT and instance.max_api.ClankProtocol.port == COMPORT:
                return instance
        # If the com port is not used add it to the list and return the new instance.
        current_instance = cls(*args, **kw)
        return current_instance

    return _maxapi_singleton_proccess
class MaxConst:
    BENDER_COMPUTER_SOURCE = 0x0E
    BENDER_COMPUTER_ADDRESS = 0x01 << BENDER_COMPUTER_SOURCE
    PROP_CONTROLLER_SOURCE = 0x00
    PROP_CONTROLLER_ADDRESS = 0x01 << PROP_CONTROLLER_SOURCE
    THRUSTER_CONTROLLER_SOURCE = 0x01
    THRUSTER_CONTROLLER_ADDRESS = 0x01 << THRUSTER_CONTROLLER_SOURCE
    MAXWELL_CONTROLLER_ADDRESS = PROP_CONTROLLER_ADDRESS | THRUSTER_CONTROLLER_ADDRESS
    NUMBER_OF_MAXWELL_HARDWARE_SECTORS = 32
    ANNOTATION_BLACK_LIST = [MaxCmd.GET_MAX_HRD_INFO, MaxCmd.GET_TLM, MaxCmd.GET_STATUS, MaxCmd.SET_DEBUG_LEDS, MaxCmd.WRT_ERR, MaxCmd.RFT_SET_DEBUG_LED0]
    NO_SINGLETON = 0x00
    MAIN_SINGLETON = 0x01
    SINGLETON_WITH_MATCHING_BAUD_RATE = 0x02
    SINGLETON_WITH_MATCHING_COMPORT = 0x03

class MaxState(Enum):
    LOW_POWER = 1
    IDLE = 2
    THRUST = 4
    MANUAL = 7
    SW_LOADING = 8
    WILD_WEST = 13
    SAFE = 15



class MaxTlm(Enum):
    ACK_STATUS = auto()
    SC_ERR_CODE1 = auto()
    SC_ERR_CODE2 = auto()
    SC_ERR_CODE3 = auto()
    STATE = auto()
    BUSY_FLAG = auto()
    NEEDS_RESET = auto()
    PROP_VALID = auto ()
    PROP_MASS = auto()
    TTANK = auto()
    PTANK = auto()
    PLOWP = auto()
    MFS_MDOT = auto()
    EHVD = auto()
    IHVD = auto()
    TLINER = auto()
    TINVERTER = auto()
    THRUST_RF_VSET_FB = auto()
    THRUST_MDOT_FB = auto()
    THRUST_DUR_SEC_FB = auto()
    APP_SELECT = auto()
    RESERVED1 = auto()
    RESERVED2 = auto()
    ACPT_CNT = auto()
    RJCT_CNT = auto()
    UNIX_TIME_US = auto()

    E3V3 = auto()
    IPFCV = auto()
    IBOARD = auto()
    EBUS = auto()
    IHPISO = auto()
    IRFT = auto()
    E12V = auto()
    TADC0 = auto()
    ITANK = auto()
    ILPISO = auto()
    E5V = auto()
    E2V5_REF = auto()
    TRTD2 = auto()
    TADC1 = auto()
    RFT_FAULT = auto()
    HEATER_FAULT = auto()
    HPISO_FAULT = auto()
    LPISO_FAULT = auto()
    PFCV_FAULT = auto()
    BL_SELECT = auto()
    RESERVED3 = auto()
    PT_SELECT = auto()
    BL_CRC = auto()
    APP_CRC = auto()
    PT_CRC = auto()
    ERR_SINCE_POWERUP = auto()
    ERR1_CODE = auto()
    ERR1_DATA = auto()
    ERR2_CODE = auto()
    ERR2_DATA = auto()
    ERR3_CODE = auto()
    ERR3_DATA = auto()
    ERRHX1_CODE = auto()
    ERRHX1_DATA = auto()
    ERRHX2_CODE = auto()
    ERRHX2_DATA = auto()
    ERRHX3_CODE = auto()
    ERRHX3_DATA = auto()
    MOST_FREQ_ERR_CODE = auto()
    MOST_FREQ_ERR_COUNT = auto()
    TOTAL_ERR_CNT = auto()
    HEAT_CTRL_EN_FB = auto()
    HEATER_EN_FB = auto()
    HPISO_EN_FB = auto()
    LPISO_EN_FB = auto()
    FLOW_CTRL_EN_FB = auto()
    REC_FRAM_EN_FB = auto()
    REC_NAND_EN_FB = auto()
    REC_BANK_SEL_FB = auto()
    HEAT_CTRL_TEMP_FB = auto()
    HEATER_SETPT_MA_FB = auto()
    HPISO_SETPT_MA_FB = auto()
    LPISO_SETPT_MA_FB = auto()
    FLOW_CTRL_MDOT_FB = auto()
    PFCV_MDOT_FB = auto()
    RCM_VALUE = auto()
    WDT_RST_TRIG = auto()
    WDT_TOGGLE_SEL = auto()
    FSW_CPU_UTIL = auto()
    DATA_MODE = auto()
    REC_NAND_TLM_CNT_B0 = auto()
    REC_NAND_TLM_CNT_B1 = auto()
    PFCV_MA_FB = auto()



    RFT_STATE = auto()
    Pad0 = auto()
    RFT_SC_ERR_CODE1 = auto()
    RFT_SC_ERR_CODE2 = auto()
    RFT_SC_ERR_CODE3 = auto()


    ADC0_IN5_RAW = auto()
    ADC1_IN0_RAW = auto()
    ADC1_IN3_RAW = auto()
    ADC1_IN6_RAW = auto()
    ADC1_IN7_RAW = auto()
    REC_NAND_CYCLE_CNT = auto()
    UART0_DMA_RX_CNT = auto()

    RFT_DATA_MODE = auto()


PROP_CONTROLLER_KEYWORD = "Prop Controller"
THRUSTER_CONTROLLER_KEYWORD = "Thruster Controller"

class MrMaxException(Exception):
    def __init__(self, msg_str, aux_data=None):
        if (aux_data == None)               or \
                isinstance(aux_data, list)  or \
                isinstance(aux_data, dict)  or \
                isinstance(aux_data, str)   or \
                isinstance(aux_data, float) or \
                isinstance(aux_data, int):
            self.aux_data = aux_data
        else:
            self.aux_data = "Bad data '%s'. Accepts list,dict,str,float,int" % (type(aux_data).__name__)

        super().__init__(msg_str, self.aux_data)


@maxapi_singleton_proccess
class MrMaxwell:
    def __init__(self, com_port, com_baud, db_datasource, db_measurement,influx_host="nekone.phasefour.co", enable_influx = True, enable_startup_scripts = True, enable_csv = True, custom_influx_tags = {}, annotate_msg_id = True, initialize_serial = True, hardware="block_1"):
        self.initialized = False # We will set to true once self.maxapi has been established. Setting to False in case self.max_api fails to initialize
        self.max_api = maxwell_api_core.MaxwellApi(com_baud, com_port,open_port=initialize_serial, hardware=hardware)
        self.initialized = initialize_serial
        self.startup_scripts_enabled = enable_startup_scripts
        self.annotate_msg_id = annotate_msg_id
        self.default_hardware = MaxConst.PROP_CONTROLLER_ADDRESS
        self.pt_format_dictarr = []  # Holds the PT Formatting from CSV if in Sell Your Soul mode
        self.check_ack = True
        self.singleton_status = MaxConst.NO_SINGLETON
        self.prop_connected = False
        self.thrust_connected = False

        self.get_max_api_version()

        '''
              Setting up recording for mr_maxwell and running startup scripts
              
        We are checking various conditions here to determine what parts of the script we have to run.
        Currently there is two options. 
        1. initialize serial
        2. enable startup_scripts
        
        Most startup scripts need initialize serial besides one currently (__record_mr_maxwell_output). 
        Thus we have enable_startup_scripts twice while the rest of the startup scripts is underneath the initialize serial check. 
        There are some scripts as well that are not part of the initialize scripts that do require serial so we will have
        
        1. functions that just need enable startup_scripts
        2. Functions that just need initialize serial (In _run_serial_startup_scripts)
        3. Functions that need both enable startup_scripts and initialize serial (In _run_serial_startup_scripts)
        '''
        if enable_startup_scripts:
            python_file_name = self._get_python_file_name()
            self.__record_mr_maxwell_output(db_measurement, db_datasource, influx_host, python_file_name, enable_influx= enable_influx, enable_csv=enable_csv, custom_influx_tags=custom_influx_tags)
        self._run_serial_startup_scripts()





    def __del__(self):
        if self.initialized:
            self.max_api.shut_down_max_api()

    def initialize_mr_maxwell(self):
        if self.initialized == True:
            print("Mr Maxwell is already initialized")
        else:
            self.max_api.open_serial_port()
            self.initialized = True
            self._run_serial_startup_scripts()

    def check_version_compatability(self, version_1:P4VersionInfo, version_2:P4VersionInfo):
        if version_1  and version_2:
            if version_1.major== version_2.major and version_1.major == version_2.major:
                return True
            else:
                return False
        else:
            return False

    def is_prop_controller_connected(self):
        return self.prop_connected

    def is_thruster_controller_connected(self):
        return self.thrust_connected

    def update_annotate_black_list(self, list):
        if self.annotate_msg_id:
            if self.max_api.clank_influx_database is not None:
                self.max_api.clank_influx_database.annotate_msg_id_enable = True
                self.max_api.clank_influx_database.update_post_annotation_black_list(list)
            else:
                print("Can not update black list because maxapi can not connect to the influx server.")


    def terminate_on_nack(self, value):
        if isinstance(value, bool):
            self.halt_on_nack = value
        else:
            print("the input for terminate_on_nack is not a boolean")

    def set_check_ack(self, value: bool):
        self.check_ack = value

    def get_check_ack(self):
       return self.check_ack

    def _get_python_file_name(self):
        '''
        Getting information the file name of the script that called us.
        We are doing this by looking at the stack and go back one. This is assumed to be the script that called us.
        '''
        # Get the full stack
        stack = inspect.stack()
        previous_stack_frame = stack[1]  # Get one level up from current frame.
        python_file_name_list = previous_stack_frame.filename.split(
            '/')  # Getting the file name of the frame and splitting it on the directory.
        # sometimes parsing this way does not work. To confirm that is the case check if the size of 1. If it is it did not parse correctly trying a different dilimiter.
        if len(python_file_name_list) == 1:
            python_file_name_list = previous_stack_frame.filename.split('\\')
        python_file_name = python_file_name_list[len(
            python_file_name_list) - 1]  # Getting the last file from this path. This is the python file that is executing.
        python_file_name = python_file_name[
                           :len(python_file_name) - 3]  # Getting rid of .py extension of the python file name
        return python_file_name

    def _run_serial_startup_scripts(self):
        if self.initialized:
            self.clear_auto_telm()
            self.prop_connected = self.check_hardware_connection("Prop Controller", silent_mode=True, loop_amount=3)
            self.thrust_connected = self.check_hardware_connection("Thruster Controller", silent_mode=True, loop_amount=3)

            if self.startup_scripts_enabled:
                self.halt_on_nack = True
                if self.prop_connected:
                    self._check_software_compatibility(transmit_mask=MaxConst.PROP_CONTROLLER_ADDRESS)
                else:
                    print("The Prop Controller is not connected.")

                if self.thrust_connected:
                    self._check_software_compatibility(transmit_mask=MaxConst.THRUSTER_CONTROLLER_ADDRESS)
                else:
                    print("The Thruster Controller is not connected.")

                time.sleep(1)
                if self.prop_connected:
                    # Giving a unix time stamp to the prop controller.
                    # This is done by reading and parsing all the flash information. From there it will
                    # If it was successful and if the record is there,
                    # it take the serial id of the prop controller and put it appended to telm_influx  post_tags
                    # This can be used as a way to filter data from influx
                    tlm_dict = self.set_utime(transmit_mask=MaxConst.PROP_CONTROLLER_ADDRESS)
                    if tlm_dict["status"] != "STATUS_SUCCESS":
                        print("PROP CONTROLLER UNIX TIMESTAMP FAILED TO SEND")
                    # Getting all the flash hardware ids and putting them into tags.
                    hardware_output = self.read_all_maxwell_hardware_records()
                    if hardware_output:
                        prop_board_maxwell_info = hardware_output[0]
                        if prop_board_maxwell_info["field_name"] == "prop_controller":
                            prop_board_tag = {
                                prop_board_maxwell_info["field_name"]: prop_board_maxwell_info["serial_number"].lower()}
                            if self.max_api.telm_influx_database is not None:
                                self.max_api.telm_influx_database.update_post_append_tags(prop_board_tag)
                        else:
                            "The Maxwell Hardware field is Illformed."

                if self.thrust_connected:
                    tlm_dict = self.set_utime(transmit_mask=MaxConst.THRUSTER_CONTROLLER_ADDRESS)
                    if tlm_dict["status"] != "STATUS_SUCCESS":
                        print("THRUSTER CONTROLLER UNIX TIMESTAMP FAILED TO SEND")
                    hardware_output = self.read_all_maxwell_hardware_records(
                        transmit_mask=MaxConst.THRUSTER_CONTROLLER_ADDRESS)
                    if hardware_output:
                        thrust_board_maxwell_info = hardware_output[0]
                        if thrust_board_maxwell_info["field_name"] == "Thruster Controller":
                            thrust_board_tag = {
                                thrust_board_maxwell_info["field_name"]: thrust_board_maxwell_info["serial_number"]}
                            if self.max_api.telm_influx_database is not None:
                                self.max_api.telm_influx_database.update_post_append_tags(thrust_board_tag)
                        else:
                            "The Maxwell Hardware field is Illformed."

    def _check_software_compatibility(self, transmit_mask):
        sw_info_response = self.get_sw_info("Current_App", transmit_mask=transmit_mask)
        if sw_info_response["status"] == "STATUS_SUCCESS":
            parsed_sw_info = parse_maxwell_header(sw_info_response, "Current Application")
            fw_parsed_vers = p4_version_parser(parsed_sw_info["software_version"])
            if fw_parsed_vers is None:
                print("CAN NOT READ SW INFORMATION!")
            else:
                api_parsed_version = p4_version_parser(self.maxapi_version)
                if transmit_mask == MaxConst.PROP_CONTROLLER_ADDRESS:
                    hardware_string = "PROP CONTROLLER"
                else:
                    hardware_string = "THRUSTER CONTROLLER"
                # Not sure where the 7 came from but I imagine that we are 0.7 for maxapi we are in a good spot. for now and there was some major combataiblity is 0.6 and below.
                if not self.check_version_compatability(fw_parsed_vers, api_parsed_version)  and (int(api_parsed_version.minor) < 7):

                    print("WARNING THIS API AND THE " + hardware_string + " FIRMWARE VERSION IS NOT GUARANTEED TO BE COMPATIBLE!")
                    print("Firmware version is: " + parsed_sw_info["software_version"])
                    print("Maxapi version is: " + self.maxapi_version)
        else:
            print("Can not get Software Information!")

    def update_default_interfacting_hardware(self, hardware_type):
        if hardware_type == "Prop Controller":
            self.default_hardware = MaxConst.PROP_CONTROLLER_ADDRESS
        elif hardware_type == "Thruster Controller":
            self.default_hardware = MaxConst.THRUSTER_CONTROLLER_ADDRESS
        else:
            print("The hardware selection you picked was invalid.")
            self.default_hardware = MaxConst.PROP_CONTROLLER_ADDRESS

    def update_singleton_status(self, singleton_option):
        if singleton_option == MaxConst.NO_SINGLETON:
            # If the singleton status was something else lets remove it from the list of singleton_instances.
            if self.singleton_status != MaxConst.NO_SINGLETON:
                self.remove_object_from_singleton_instance_list(self)
            self.singleton_status = MaxConst.NO_SINGLETON

        elif singleton_option == MaxConst.MAIN_SINGLETON:
            if self.singleton_status == MaxConst.NO_SINGLETON:
                self.mr_max_singleton_instance.append(self)
            self.singleton_status = MaxConst.MAIN_SINGLETON

        elif singleton_option == MaxConst.SINGLETON_WITH_MATCHING_BAUD_RATE:
            if self.singleton_status == MaxConst.NO_SINGLETON:
                self.mr_max_singleton_instance.append(self)
            self.singleton_status = MaxConst.SINGLETON_WITH_MATCHING_BAUD_RATE
            self.mr_max_singleton_instance.append(self)
        elif singleton_option == MaxConst.SINGLETON_WITH_MATCHING_COMPORT:
            if self.singleton_status == MaxConst.NO_SINGLETON:
                self.mr_max_singleton_instance.append(self)
            self.singleton_status = MaxConst.SINGLETON_WITH_MATCHING_COMPORT
        else:
            print("The singleton status that was specified is invalid.")
            # If the singleton status was something else lets remove it from the list of singleton_instances.
            if self.singleton_status != MaxConst.NO_SINGLETON:
                self.remove_object_from_singleton_instance_list(self)
            self.singleton_status = MaxConst.NO_SINGLETON

    def clear_auto_telm(self):
        self.max_api.ClankProtocol.flushInput()
        self.set_auto_tlm_disable(transmit_mask=MaxConst.PROP_CONTROLLER_ADDRESS,silent_mode = True, timeout = .500)
        self.set_auto_tlm_disable(transmit_mask=MaxConst.THRUSTER_CONTROLLER_ADDRESS, silent_mode = True, timeout = .500)
        self.max_api.ClankProtocol.flushInput()

    def get_max_api_version(self):
        maxwell_api_path = os.path.dirname(os.path.abspath(__file__))
        version_path = os.path.join(maxwell_api_path, 'maxapi_version.txt')
        with open(version_path) as version_file:
            self.maxapi_version = version_file.read()

    def led_test(self):
        self._send_maxwell_message(MaxCmd.ABORT, [])
        time.sleep(0.250)
        tlm_dict = self._send_maxwell_message(MaxCmd.MANUAL, [])
        time.sleep(0.250)
        tlm_dict = self._send_maxwell_message(MaxCmd.SET_DEBUG_LEDS, [0x00])
        time.sleep(0.250)
        tlm_dict = self._send_maxwell_message(MaxCmd.SET_DEBUG_LEDS, [0x01])
        time.sleep(0.250)
        tlm_dict = self._send_maxwell_message(MaxCmd.SET_DEBUG_LEDS, [0x02])
        time.sleep(0.250)
        tlm_dict = self._send_maxwell_message(MaxCmd.SET_DEBUG_LEDS, [0x04])
        time.sleep(0.250)
        tlm_dict = self._send_maxwell_message(MaxCmd.SET_DEBUG_LEDS, [0x08])
        time.sleep(0.250)
        tlm_dict = self._send_maxwell_message(MaxCmd.SET_DEBUG_LEDS, [0x0F])
        time.sleep(0.250)

    def get_status(self, transmit_mask=None):
        if transmit_mask is None:
            transmit_mask = self.default_hardware
        status_dict = self._send_maxwell_message(MaxCmd.GET_STATUS, [], transmit_mask=transmit_mask)
        return status_dict


    def get_status_item(self, max_tlm_item, transmit_mask=None):
        if transmit_mask is None:
            transmit_mask = self.default_hardware
        status_dict = self._send_maxwell_message(MaxCmd.GET_STATUS, [], transmit_mask=transmit_mask)
        return status_dict['parsed_payload'][max_tlm_item.name]

    def sleep_w_status(self, time_sec, transmit_mask=None, freq=.250):
        if transmit_mask is None:
            transmit_mask = self.default_hardware
        while (time_sec >= freq):
            status_dict = self._send_maxwell_message(MaxCmd.GET_STATUS, [], transmit_mask=transmit_mask)
            time.sleep(freq)
            time_sec = time_sec - freq

    def get_tlm(self, transmit_mask=None):
        if transmit_mask is None:
            transmit_mask = self.default_hardware
        tlm_dict = self._send_maxwell_message(MaxCmd.GET_TLM, [], transmit_mask=transmit_mask)
        return tlm_dict

    def get_tlm_item(self, tlm_key, tlm_pkt=None, transmit_mask=None):
        if transmit_mask is None:
            transmit_mask = self.default_hardware

        # Support either MaxTlm.BUSY_FLAG enums or "BUSY_FLAG" raw strings
        if isinstance(tlm_key, MaxTlm):
            tlm_key_str = tlm_key.name
        else:
            tlm_key_str = tlm_key

        if tlm_pkt == None:
            tlm_dict = self._send_maxwell_message(MaxCmd.GET_TLM, [], transmit_mask=transmit_mask)
        else:
            tlm_dict = tlm_pkt

        return tlm_dict['parsed_payload'][tlm_key_str]


    def sleep_w_tlm(self, time_sec, transmit_mask=None, freq=.250):
        if transmit_mask is None:
            transmit_mask = self.default_hardware
        while (time_sec >= freq):
            tlm_dict = self._send_maxwell_message(MaxCmd.GET_TLM, [], transmit_mask=transmit_mask)
            time.sleep(freq)
            time_sec = time_sec - freq

    def check_tlm(self, tlm_key, comp_str, exp_value, tlm_pkt=None, transmit_mask=None):
        if transmit_mask is None:
            transmit_mask = self.default_hardware

        # Support either MaxTlm.BUSY_FLAG enums or "BUSY_FLAG" raw strings
        if isinstance(tlm_key, MaxTlm):
            tlm_key_str = tlm_key.name
        else:
            tlm_key_str = tlm_key

        if tlm_pkt is None:
            tlm_dict = self._send_maxwell_message(MaxCmd.GET_TLM, [], transmit_mask=transmit_mask)
        else:
            tlm_dict = tlm_pkt

        if tlm_dict["status"] != "STATUS_SUCCESS":
            raise MrMaxException("FAIL: Invalid TLM status '%s'" % (tlm_dict["status"]), tlm_dict)

        act_value = tlm_dict['parsed_payload'][tlm_key_str]

        if comp_str == "==":
            result = (act_value == exp_value)
        elif comp_str == "!=":
            result = (act_value != exp_value)
        elif comp_str == ">":
            result = (act_value > exp_value)
        elif comp_str == ">=":
            result = (act_value >= exp_value)
        elif comp_str == "<":
            result = (act_value < exp_value)
        elif comp_str == "<=":
            result = (act_value <= exp_value)
        else:
            raise MrMaxException("FAIL: Invalid check compare operator '%s'" % (comp_str))

        return result, act_value


    def check(self, tlm_key, comp_str, exp_value, tlm_pkt=None, transmit_mask=None):
        check_passed, act_val = self.check_tlm(tlm_key, comp_str, exp_value, tlm_pkt=tlm_pkt, transmit_mask=transmit_mask)
        if not check_passed:
            raise MrMaxException("FAIL: Check %s %s %s -- Actual: %s" % (tlm_key, comp_str, exp_value, act_val))


    def wait_check(self, tlm_key, comp_str, exp_value, dur_sec=0.0, transmit_mask=None):
        elapsed_time = 0.0
        check_passed = False
        time_start = time.time()

        # Desired behavior is to do at least one iteration of the while loop
        # by default when dur_sec is not specified
        while (not check_passed) and (elapsed_time <= dur_sec):
            check_passed, act_val = self.check_tlm(tlm_key, comp_str, exp_value, tlm_pkt=None, transmit_mask=transmit_mask)
            elapsed_time = time.time() - time_start

        if not check_passed:
            raise MrMaxException("FAIL: Wait Check %s %s %s -- Actual: %s" % (tlm_key, comp_str, exp_value, act_val))



    def set_utime(self, transmit_mask=None):
        if transmit_mask is None:
            transmit_mask = self.default_hardware
        time_arr = []
        curr_unix_time_usec = int(time.time()*1000000.0)
        time_arr.append((curr_unix_time_usec >> 56) & 0xFF)
        time_arr.append((curr_unix_time_usec >> 48) & 0xFF)
        time_arr.append((curr_unix_time_usec >> 40) & 0xFF)
        time_arr.append((curr_unix_time_usec >> 32) & 0xFF)
        time_arr.append((curr_unix_time_usec >> 24) & 0xFF)
        time_arr.append((curr_unix_time_usec >> 16) & 0xFF)
        time_arr.append((curr_unix_time_usec >> 8)  & 0xFF)
        time_arr.append((curr_unix_time_usec)       & 0xFF)
        return self._send_maxwell_message(MaxCmd.SET_UTIME, time_arr, transmit_mask=transmit_mask)

    # Silent mode will not verify the ack for you. This is useful when you want to run internall tools that do not print the screen.
    # Currently the __verify ack is useful for people looking at the screen.
    def check_hardware_connection(self, hardware_type, loop_amount=1, silent_mode=False):
        if hardware_type == "Prop Controller":
            hardware_select = MaxConst.PROP_CONTROLLER_ADDRESS
        elif hardware_type == "Thruster Controller":
            hardware_select = MaxConst.THRUSTER_CONTROLLER_ADDRESS
        else:
            print("Your argument you put in is wrong please select Prop Controller or Thruster Controller")
            return False
        count = 0
        while count < loop_amount:
            if silent_mode:
                output= self.max_api.write_clank_command(MaxCmd.GET_STATUS,[], transmit_mask=hardware_select, timeout =.250)
            else:
                output=self._send_maxwell_message(MaxCmd.GET_STATUS,[],hardware_select, timeout =.250)
            if output["status"] == "STATUS_SUCCESS":
                return True
            count = count + 1
        return False

    def set_next_app(self, next_app_num, transmit_mask=None):
        if transmit_mask is None:
            transmit_mask = self.default_hardware
        return self._send_maxwell_message(MaxCmd.SET_NEXT_APP, [int(next_app_num) & 0xFF], transmit_mask=transmit_mask)

    def get_rft_tlm_cache(self):
        tlm_dict = self._send_maxwell_message(MaxCmd.GET_TLM_RFT_CACHED, [], transmit_mask=MaxConst.PROP_CONTROLLER_ADDRESS)
        return tlm_dict

    def set_heater_control_temp(self, degC):
        return self._send_maxwell_message(MaxCmd.SET_HEAT_SETPT, self.__f32_to_raw_arr(degC))

    def set_heater_control_enable(self):
        return self._send_maxwell_message(MaxCmd.SET_HEAT_CTL_ALG_EN, [1])

    def set_heater_control_disable(self):
        return self._send_maxwell_message(MaxCmd.SET_HEAT_CTL_ALG_EN, [0])

    def turn_on_prop_control_heater(self, val_mA):
        results = self.set_heater_enable()
        if results["status"] == "STATUS_SUCCESS":
            results = self.set_heater_i(val_mA)
        return results

    def turn_off_prop_control_heater(self):
        results = self.set_heater_i(0)
        if results["status"] == "STATUS_SUCCESS":
            results = self.set_heater_disable()
        else:
            aborted = self.__emergency_abort()
            if aborted == False:
                sys.exit()
        return results

    def actuate_lp_iso_valve(self, val_mA):
        self.set_lpiso_enable()
        self.set_lpiso_i(val_mA)
        time.sleep(.050)
        self.set_lpiso_i(val_mA/2)

    def deactivate_lp_iso_valve(self):
        self.set_lpiso_i(0x00)
        self.set_lpiso_disable()

    def set_auto_tlm_enable(self, period_ms, transmit_mask=None):
        if transmit_mask is None:
            transmit_mask = self.default_hardware
        self.max_api.ClankProtocol.transmit_check_enable(False)
        return self._send_maxwell_message(MaxCmd.SET_AUTO_TLM, [1] + self.__int_to_raw_arr(period_ms),transmit_mask=transmit_mask)

    def set_auto_tlm_disable(self,transmit_mask=None,silent_mode = False, timeout = 1.0):
        if transmit_mask is None:
            transmit_mask = self.default_hardware
        if silent_mode:
            output = self.write_clank_command(MaxCmd.SET_AUTO_TLM, [0, 0, 0, 0, 0], transmit_mask=transmit_mask, timeout  = timeout)
            self.max_api.ClankProtocol.transmit_check_enable(True)
        else:
            output = self._send_maxwell_message(MaxCmd.SET_AUTO_TLM, [0, 0, 0, 0, 0], transmit_mask=transmit_mask, timeout = timeout)
            self.max_api.ClankProtocol.transmit_check_enable(True)
            if output["status"] != "STATUS_SUCCESS":
                print("Could not Disable TLM")
        return output

    def set_auto_status_enable(self, period_ms, transmit_mask=None):
        if transmit_mask is None:
            transmit_mask = self.default_hardware
        self.max_api.ClankProtocol.transmit_check_enable(False)
        return self._send_maxwell_message(MaxCmd.SET_AUTO_STATUS, [1] + self.__int_to_raw_arr(period_ms), transmit_mask=transmit_mask)

    def set_auto_status_disable(self, transmit_mask=None):
        if transmit_mask is None:
            transmit_mask = self.default_hardware
        output = self._send_maxwell_message(MaxCmd.SET_AUTO_STATUS, [0, 0, 0, 0, 0], transmit_mask=transmit_mask)
        self.max_api.ClankProtocol.transmit_check_enable(True)
        if output["status"] != "STATUS_SUCCESS":
            print("Could not Disable TLM")
        return output

    def abort(self, transmit_mask=None):
        if transmit_mask is None:
            transmit_mask = self.default_hardware
        output =  self.max_api.write_clank_command(MaxCmd.ABORT, [], transmit_mask=transmit_mask)
        time.sleep(.250)
        return output

    def clear_errors(self, transmit_mask=None):
        if transmit_mask is None:
            transmit_mask = self.default_hardware
        output = self._send_maxwell_message(MaxCmd.CLR_ERRORS, [], transmit_mask=transmit_mask)
        time.sleep(.250)
        return output

    def clear_errors_advanced(self, error_type_str, transmit_mask=None):
        if transmit_mask is None:
            transmit_mask = self.default_hardware
        if error_type_str == "Error Log":
            error_type = 0x1
        elif error_type_str == "Telemetry Error Record":
            error_type = 0x2
        elif error_type_str == "Sram Error Record":
            error_type = 0x4
        elif error_type_str == "Error Flash":
            error_type = 0x8
        elif error_type_str == "All":
            error_type = 0xF
        else:
            print("Invalid argument not for the type of error to delete. Not deleting any errors.")
            return
        output = self._send_maxwell_message(MaxCmd.CLEAR_ERRORS_ADVANCED, [error_type], transmit_mask=transmit_mask)
        time.sleep(.250)
        return output

    def goto_safe(self, transmit_mask=None):
        if transmit_mask is None:
            transmit_mask = self.default_hardware
        output = self.max_api.write_clank_command(MaxCmd.ABORT, [], transmit_mask=transmit_mask)
        time.sleep(.250)
        return output

    def goto_idle(self):
        return self._send_maxwell_message(MaxCmd.IDLE, [])

    def goto_manual(self, transmit_mask=None):
        if transmit_mask is None:
            transmit_mask = self.default_hardware
        return self._send_maxwell_message(MaxCmd.MANUAL, [], transmit_mask=transmit_mask)

    def goto_wild_west(self, transmit_mask=None):
        if transmit_mask is None:
            transmit_mask = self.default_hardware
        return self._send_maxwell_message(MaxCmd.WILD_WEST, [], transmit_mask=transmit_mask)

    def goto_sw_update(self, transmit_mask=None):
        if transmit_mask is None:
            transmit_mask = self.default_hardware
        return self._send_maxwell_message(MaxCmd.SW_UPDATE, [], transmit_mask=transmit_mask)

    def goto_thrust(self):
        return self._send_maxwell_message(MaxCmd.THRUST, [])

    def config_thrust(self, rf_voltage, flow_rate_sccm, duration_sec):
        data_arr = self.__f32_to_raw_arr(rf_voltage) + self.__f32_to_raw_arr(flow_rate_sccm) + self.__f32_to_raw_arr(duration_sec)
        return self._send_maxwell_message(MaxCmd.CONFIG_THRUST, data_arr)

    def config_thrust_adv(self, rf_voltage, flow_rate_sccm, duration_sec, puff_sccm, wf_gen_freq, puff_dur_sec,
                          ignite_dur_sec, ignite_vset, power_ramp_dur_sec):
        data_arr = self.__f32_to_raw_arr(rf_voltage) + \
                   self.__f32_to_raw_arr(flow_rate_sccm) + \
                   self.__f32_to_raw_arr(duration_sec) + \
                   self.__f32_to_raw_arr(puff_sccm) + \
                   self.__f32_to_raw_arr(wf_gen_freq) + \
                   self.__f32_to_raw_arr(puff_dur_sec) + \
                   self.__f32_to_raw_arr(ignite_dur_sec) + \
                   self.__f32_to_raw_arr(ignite_vset) + \
                   self.__f32_to_raw_arr(power_ramp_dur_sec)
        return self._send_maxwell_message(MaxCmd.CONFIG_THRUST_ADV, data_arr)

    def get_sw_info(self, app_type, transmit_mask=None):
        if transmit_mask is None:
            transmit_mask = self.default_hardware
        if app_type == "Current_App":
            app_select = 0
        elif app_type == "Boot_1":
            app_select = 1
        elif app_type == "Boot_2":
            app_select = 2
        elif app_type == "Gold_Img":
            app_select = 3
        elif app_type == "Flash_App_1":
            app_select = 4
        elif app_type == "Flash_App_2":
            app_select = 5
        elif app_type == "Parm_Gold":
            app_select = 6
        elif app_type == "Parm_App_1":
            app_select = 7
        elif app_type == "Parm_App_2":
            app_select = 8
        elif app_type == "Current_Parm":
            app_select = 9
        else:
            return
        sw_info_response = self._send_maxwell_message(MaxCmd.GET_SW_INFO, [app_select], transmit_mask=transmit_mask)
        return sw_info_response

    def get_max_hrd_info(self, record_offset, transmit_mask=None):
        if transmit_mask is None:
            transmit_mask = self.default_hardware
        if record_offset > 31 and record_offset < 0:
            print("The sector offset you picked is not valid. Not sending any data.")
            return
        sw_info_response = self._send_maxwell_message(MaxCmd.GET_MAX_HRD_INFO, [record_offset], transmit_mask = transmit_mask, timeout=.250)
        return sw_info_response

    def set_max_hrd_info(self, data_bytes, transmit_mask=None):
        if transmit_mask is None:
            transmit_mask = self.default_hardware
        sw_info_response = self._send_maxwell_message(MaxCmd.SET_MAX_HRD_INFO, list(data_bytes),transmit_mask=transmit_mask)
        return sw_info_response

    def mass_erase_max_hrd_info(self, transmit_mask=None):
        if transmit_mask is None:
            transmit_mask = self.default_hardware
        sw_info_response = self._send_maxwell_message(MaxCmd.ERASE_MAX_HRD_INFO, [], transmit_mask=transmit_mask)
        return sw_info_response

    def read_all_maxwell_hardware_records(self, transmit_mask=None):
        if transmit_mask is None:
            transmit_mask = self.default_hardware
        count = 0
        maxwell_list = []
        while (count < MaxConst.NUMBER_OF_MAXWELL_HARDWARE_SECTORS):
            payload = self.get_max_hrd_info(count, transmit_mask=transmit_mask)
            output = read_hardware_maxwell_record(payload["raw_payload"], count)
            if output["field_name"] != "null":
                maxwell_list.append(output)
            count = count + 1
        return maxwell_list

    def set_heater_enable(self):
        return self._send_maxwell_message(MaxCmd.SET_HEATER_EN, [0x01])

    def set_heater_disable(self):
        return self._send_maxwell_message(MaxCmd.SET_HEATER_EN, [0x00])

    def set_hpiso_enable(self):
        return self._send_maxwell_message(MaxCmd.SET_HPISO_EN, [0x01])

    def set_hpiso_disable(self):
        return self._send_maxwell_message(MaxCmd.SET_HPISO_EN, [0x00])

    def set_lpiso_enable(self):
        return self._send_maxwell_message(MaxCmd.SET_LPISO_EN, [0x01])

    def set_lpiso_disable(self):
        return self._send_maxwell_message(MaxCmd.SET_LPISO_EN, [0x00])

    def set_heater_i(self, val_mA):
        """Commands the Heater CC Driver to the specified value"""
        return self._send_maxwell_message(MaxCmd.SET_HEAT_I, self.__f32_to_raw_arr(val_mA))

    def set_hpiso_i(self, val_mA):
        """Commands the High Pressure Isolation Valve CC Driver to the specified value"""
        return self._send_maxwell_message(MaxCmd.SET_HPISO_I, self.__f32_to_raw_arr(val_mA))

    def set_lpiso_i(self, val_mA):
        """Commands the Low Pressure Isolation Valve CC Driver to the specified value"""
        return self._send_maxwell_message(MaxCmd.SET_LPISO_I, self.__f32_to_raw_arr(val_mA))

    def set_pfcv_i(self, val_mA):
        """Commands the PFCV CC Driver to the specified value"""
        return self._send_maxwell_message(MaxCmd.SET_PFCV_I, self.__f32_to_raw_arr(val_mA))

    def set_flow_control_enable(self, flow_rate_sccm):
        return self._send_maxwell_message(MaxCmd.SET_FLOW_CTL_ALG_EN, [1] + self.__f32_to_raw_arr(flow_rate_sccm))

    def set_flow_control_disable(self):
        return self._send_maxwell_message(MaxCmd.SET_FLOW_CTL_ALG_EN, [0] + self.__f32_to_raw_arr(0.0))

    def set_flow_rate_direct(self, flow_rate_sccm):
        return self._send_maxwell_message(MaxCmd.SET_FLOW_RATE, self.__f32_to_raw_arr(flow_rate_sccm))

    def set_heater_pwm(self,pwm_count):
        """Commands the Heater CC Driver to the specified RAW PWM value"""
        if pwm_count >= 0 and pwm_count <= 0x8000:
            return self._send_maxwell_message(MaxCmd.SET_HEATER_PWM_RAW, self.__int16_to_raw_arr(pwm_count))
        else:
            print("Invalid argument put in a value between 0x0000 and 0x8000")
            return {"status": "STATUS_FAILURE"}


    def set_hpiso_pwm(self,pwm_count):
        """Commands the High Pressure Isolation Valve CC Driver to the specified RAW PWM value"""
        if pwm_count >= 0 and pwm_count <= 0x8000:
            return self._send_maxwell_message(MaxCmd.SET_HPISO_PWM_RAW, self.__int16_to_raw_arr(pwm_count))
        else:
            print("Invalid argument put in a value between 0x0000 and 0x8000")
            return {"status": "STATUS_FAILURE"}

    def set_lpiso_pwm(self,pwm_count):
        """Commands the Low Pressure Isolation Valve CC Driver to the specified RAW PWM value"""
        if pwm_count >= 0 and pwm_count <= 0x8000:
            return self._send_maxwell_message(MaxCmd.SET_LPISO_PWM_RAW, self.__int16_to_raw_arr(pwm_count))
        else:
            print("Invalid argument put in a value between 0x0000 and 0x8000")
            return {"status": "STATUS_FAILURE"}

    def set_pfcv_dac_raw(self,dac_value):
        """Commands the PFCV CC Driver to the specified dac value"""
        return self._send_maxwell_message(MaxCmd.SET_PFCV_DAC_RAW, self.__int16_to_raw_arr(dac_value))

    def set_heater_pwm_percent(self, percent):
        """Commands the Heater CC Driver to the specified RAW PWM value  via percentege"""
        if percent < 0 or percent > 100:
            print("Invalid argument put in a percent value between 0 and 100")
            return {"status": "STATUS_FAILURE"}
        pwm_count = int(percent/100 * 0x8000)
        if pwm_count >= 0 and pwm_count <= 0x8000:
            return self._send_maxwell_message(MaxCmd.SET_HEATER_PWM_RAW, self.__int16_to_raw_arr(pwm_count))
        else:
            print("Invalid argument put in a percent value between 0 and 100")
            return {"status": "STATUS_FAILURE"}

    def set_hpiso_pwm_percent(self, percent):
        """Commands the High Pressure Isolation Valve CC Driver to the specified RAW PWM value  via percentege"""
        if percent < 0 or percent > 100:
            print("Invalid argument put in a percent value between 0 and 100")
            return {"status": "STATUS_FAILURE"}
        pwm_count = int(percent/100 * 0x8000)
        if pwm_count >= 0 and pwm_count <= 0x8000:
            return self._send_maxwell_message(MaxCmd.SET_HPISO_PWM_RAW, self.__int16_to_raw_arr(pwm_count))
        else:
            print("Invalid argument put in a percent value between 0 and 100")
            return {"status": "STATUS_FAILURE"}

    def set_lpiso_pwm_percent(self, percent):
        """Commands the Low Pressure Isolation Valve CC Driver to the specified RAW PWM value via percentege"""
        if percent < 0 or percent > 100:
            print("Invalid argument put in a percent value between 0 and 100")
            return {"status": "STATUS_FAILURE"}
        pwm_count = int(percent/100 * 0x8000)
        if pwm_count >= 0 and pwm_count <= 0x8000:
            return self._send_maxwell_message(MaxCmd.SET_LPISO_PWM_RAW, self.__int16_to_raw_arr(pwm_count))
        else:
            print("Invalid argument put in a percent value between 0 and 100")
            return {"status": "STATUS_FAILURE"}

    def set_pfcv_dac_percent(self, percent):
        """Commands the PFCV CC Driver to the specified dac value via percentege"""
        if percent < 0 or percent > 100:
            print("Invalid argument put in a percent value between 0 and 100")
            return {"status": "STATUS_FAILURE"}

        dac_value = int ((percent/100)*2**16)
        if dac_value == 2**16:
            dac_value = int((2**16)-1)
        if dac_value >2**16:
            print("Invalid argument put in a percent value between 0 and 100")
            return {"status": "STATUS_FAILURE"}
        else:
            return self._send_maxwell_message(MaxCmd.SET_PFCV_DAC_RAW, self.__int16_to_raw_arr(dac_value))

    def clear_recorded_tlm(self):
        """Clears recorded TLM and associated parameters"""
        return self._send_maxwell_message(MaxCmd.CLR_REC_TLM, [])

    def clear_nand_full(self):
        """Does a hard clear of the NAND Flash (takes a few seconds)"""
        return self._send_maxwell_message(MaxCmd.CLR_NAND_FULL, [])

    def set_record_tlm_enable(self):
        return self._send_maxwell_message(MaxCmd.ENBL_REC_TLM, [])

    def set_record_tlm_disable(self):
        return self._send_maxwell_message(MaxCmd.ABORT_DATA_OP, [])

    def set_recording_bank(self, bank_num):
        return self._send_maxwell_message(MaxCmd.SET_REC_BANK, [(int(bank_num) & 0x01)])

    def set_recording_fram(self, enable):
        return self._send_maxwell_message(MaxCmd.SET_REC_FRAM_EN, [int(enable) & 0x01])

    def set_recording_nand(self, enable):
        return self._send_maxwell_message(MaxCmd.SET_REC_NAND_EN, [int(enable) & 0x01])

    def abort_data_operation(self):
        return self._send_maxwell_message(MaxCmd.ABORT_DATA_OP, [])

    def dump_mcu(self, mcu_addr, length):
        return self._send_maxwell_message(MaxCmd.DUMP_MCU, self.__int_to_raw_arr(mcu_addr) + [(int(length) & 0xFF)])

    def dump_tlm_fram_bank(self, bank_num):
        """Initiates a TLM dump from short-term storage FRAM specifying either bank 0 or 1"""
        return self.dump_tlm(MaxCmd.DUMP_TLM, [0xA, int(bank_num) & 0x1])

    def dump_tlm_fram_full(self):
        """Initiates a TLM dump from both banks of short-term storage FRAM """
        return self.dump_tlm(MaxCmd.DUMP_TLM, [0xA, 0x2])

    def dump_tlm_nand_bank(self, bank_num):
        """Initiates a TLM dump from long-term storage NAND Flash specifying either bank 0 or 1"""
        return self.dump_tlm(MaxCmd.DUMP_TLM, [0xB, int(bank_num) & 0x1])

    def dump_tlm_nand_full(self):
        """Initiates a TLM dump from both banks of long-term storage NAND Flash """
        return self.dump_tlm(MaxCmd.DUMP_TLM, [0xB, 0x2])
    def stop_wdt(self, transmit_mask=None):
        if transmit_mask is None:
            transmit_mask = self.default_hardware
        return self._send_maxwell_message(MaxCmd.STOP_WDT, [], transmit_mask=transmit_mask)
    def _send_maxwell_message(self, msg, data, transmit_mask=None, timeout=1.0):
        if transmit_mask is None:
            transmit_mask = self.default_hardware
        maxwell_response = self.max_api.write_clank_command(msg, data, timeout, transmit_mask)
        return self.__verify_ack(msg, maxwell_response)

    def dump_tlm(self, cmd, data):
        if data[0] != 0xA:
            tlm_dict = self.get_tlm()
            if tlm_dict["status"] == "STATUS_SUCCESS":
                bank0_rec = tlm_dict["parsed_payload"]["REC_NAND_TLM_CNT_B0"]
                bank1_rec = tlm_dict["parsed_payload"]["REC_NAND_TLM_CNT_B1"]
                print("Dumping From Flash")
            else:
                return tlm_dict
        else:
            # We are dumping  fram. The rough time out will be based on the maximum data fram can collect which is 514.
            bank0_rec = 514
            bank1_rec = 514
            print("Dumping From FRAM")
        print("Bank 0 has this many packets: " + str(bank0_rec))
        print("Bank 1 has this many packets: " + str(bank1_rec))
        return self.max_api.dump_mcu_tlm(cmd, data)

    def dump_tlm_ready(self):
        return self.max_api.dump_tlm_ready()

    def get_mcu_dump_tlm(self):
        return self.max_api.get_mcu_tlm()

    def sw_update_preload(self, pflash_addr, length, fram_offset, transmit_mask=None):
        if transmit_mask is None:
            transmit_mask = self.default_hardware
        data_arr = self.__int_to_raw_arr(pflash_addr) + self.__int_to_raw_arr(length) + self.__int_to_raw_arr(fram_offset)
        return self.max_api.write_clank_command(MaxCmd.SW_UP_PRELOAD, data_arr, transmit_mask=transmit_mask)

    def sw_update_stage(self, fram_offset, img_buf_bytes_arr, transmit_mask=None):
        if transmit_mask is None:
            transmit_mask = self.default_hardware

        # Transfer size does not include the 4B fram_offset that is
        # included with each staging transfer.
        MAX_STAGE_XFER_SIZE = 250

        if len(img_buf_bytes_arr) % 4 != 0:
            print("Error: image bytes must be multiple of 4. Staging aborted.")
            return

        # Integer math to get the number of full 250B transfers
        num_of_xfers = int(len(img_buf_bytes_arr) / MAX_STAGE_XFER_SIZE)

        # Get the remaining bytes for the last transfer
        bytes_left = int(len(img_buf_bytes_arr) - (num_of_xfers * MAX_STAGE_XFER_SIZE))

        for i in range(0, num_of_xfers):
            start_idx = int(i * MAX_STAGE_XFER_SIZE)
            stop_idx = int(start_idx + MAX_STAGE_XFER_SIZE)
            curr_fram_offset = int(start_idx + fram_offset)
            curr_bytes_xfer = self.__int_to_raw_arr(curr_fram_offset) + img_buf_bytes_arr[start_idx:stop_idx]
            count = 0
            output_status = False
            while count < 3 and output_status == False:
                output = self.max_api.write_clank_command(MaxCmd.SW_UP_STAGE, curr_bytes_xfer, transmit_mask=transmit_mask)
                if output["status"] == "STATUS_SUCCESS" and output['parsed_payload']['ACK_STATUS'] == 0:
                    output_status = True
                count = count + 1

            if output_status == False:
                print("Failed to stage transfer #%d. Staging aborted." % (i))
                return output


        # Perform last transfer
        if bytes_left > 0:
            start_idx = int(num_of_xfers * MAX_STAGE_XFER_SIZE)
            stop_idx = int(start_idx + bytes_left)
            curr_fram_offset = int(start_idx + fram_offset)
            curr_bytes_xfer = self.__int_to_raw_arr(curr_fram_offset) + img_buf_bytes_arr[start_idx:stop_idx]

            output = self.max_api.write_clank_command(MaxCmd.SW_UP_STAGE, curr_bytes_xfer, transmit_mask=transmit_mask)
            if output["status"] != "STATUS_SUCCESS" or output['parsed_payload']['ACK_STATUS'] != 0:
                print("Failed to stage the last transfer! So close, but so far. You suck.")
                return output

        return output

    def sw_update_program(self, pflash_addr, length, crc, transmit_mask=None):
        if transmit_mask is None:
            transmit_mask = self.default_hardware

        data_arr = self.__int_to_raw_arr(pflash_addr) + self.__int_to_raw_arr(length) + self.__int_to_raw_arr(crc)
        return self.max_api.write_clank_command(MaxCmd.SW_UP_PROGRAM, data_arr, transmit_mask=transmit_mask)

    def test_vent_safe(self, delay_sec, transmit_mask=MaxConst.PROP_CONTROLLER_ADDRESS):
        self.max_api.ClankProtocol.transmit_check_enable(False)
        return self._send_maxwell_message(MaxCmd.TEST_VENT_SAFE, self.__int_to_raw_arr(delay_sec),transmit_mask=transmit_mask)

    def test_thrust_anomaly(self, delay_sec, transmit_mask=MaxConst.PROP_CONTROLLER_ADDRESS):
        self.max_api.ClankProtocol.transmit_check_enable(False)
        return self._send_maxwell_message(MaxCmd.TEST_THRUST_ANOMALY, self.__int_to_raw_arr(delay_sec),transmit_mask=transmit_mask)

    def test_needs_reset(self, delay_sec, transmit_mask=MaxConst.PROP_CONTROLLER_ADDRESS):
        self.max_api.ClankProtocol.transmit_check_enable(False)
        return self._send_maxwell_message(MaxCmd.TEST_NEEDS_RESET, self.__int_to_raw_arr(delay_sec),transmit_mask=transmit_mask)

    def rft_abort(self):
        return self._send_maxwell_message(MaxCmd.ABORT, [], transmit_mask=MaxConst.THRUSTER_CONTROLLER_ADDRESS)

    def rft_manual(self):
        return self._send_maxwell_message(MaxCmd.MANUAL, [], transmit_mask=MaxConst.THRUSTER_CONTROLLER_ADDRESS)

    def rft_wild_west(self):
        return self._send_maxwell_message(MaxCmd.WILD_WEST, [], transmit_mask=MaxConst.THRUSTER_CONTROLLER_ADDRESS)

    def rft_sw_update(self):
        return self._send_maxwell_message(MaxCmd.SW_UPDATE, [], transmit_mask=MaxConst.THRUSTER_CONTROLLER_ADDRESS)

    def rft_clear_errors(self):
        return self._send_maxwell_message(MaxCmd.CLR_ERRORS, [], transmit_mask=MaxConst.THRUSTER_CONTROLLER_ADDRESS)

    def rft_stop_wdt(self):
        return self._send_maxwell_message(MaxCmd.STOP_WDT, [], transmit_mask=MaxConst.THRUSTER_CONTROLLER_ADDRESS)

    def rft_set_wdi(self, enable):
        return self._send_maxwell_message(MaxCmd.SET_WDI_EN, [int(enable) & 0x01], transmit_mask=MaxConst.THRUSTER_CONTROLLER_ADDRESS)

    def rft_set_wdt_rst_nclr(self, enable):
        return self._send_maxwell_message(MaxCmd.RFT_SET_WDT_RST_NCLR_EN, [int(enable) & 0x01], transmit_mask=MaxConst.THRUSTER_CONTROLLER_ADDRESS)

    def rft_set_debug_led0(self, enable):
        return self._send_maxwell_message(MaxCmd.RFT_SET_DEBUG_LED0, [int(enable) & 0x01], transmit_mask=MaxConst.THRUSTER_CONTROLLER_ADDRESS)

    def rft_set_hum_temp_sel(self, enable):
        return self._send_maxwell_message(MaxCmd.RFT_SET_HUM_TEMP_SEL_EN, [int(enable) & 0x01], transmit_mask=MaxConst.THRUSTER_CONTROLLER_ADDRESS)

    def rft_set_wf(self, enable):
        return self._send_maxwell_message(MaxCmd.RFT_SET_WF_EN, [int(enable) & 0x01], transmit_mask=MaxConst.THRUSTER_CONTROLLER_ADDRESS)

    def rft_set_pp(self, enable):
        return self._send_maxwell_message(MaxCmd.RFT_SET_PP_EN, [int(enable) & 0x01], transmit_mask=MaxConst.THRUSTER_CONTROLLER_ADDRESS)

    def rft_set_pp_vset(self, push_pull_voltage):
        data_arr = self.__f32_to_raw_arr(push_pull_voltage)
        return self._send_maxwell_message(MaxCmd.RFT_SET_PP_VSET, data_arr, transmit_mask=MaxConst.THRUSTER_CONTROLLER_ADDRESS)

    def rft_set_pp_vset_ramp(self, push_pull_voltage,ramp_time_sec):
        data_arr = self.__f32_to_raw_arr(push_pull_voltage)
        data_arr.extend(self.__f32_to_raw_arr(ramp_time_sec*1000))
        return self._send_maxwell_message(MaxCmd.RFT_RAMP_PUSH_PULL, data_arr, transmit_mask=MaxConst.THRUSTER_CONTROLLER_ADDRESS)

    def rft_set_pp_vset_pwm_raw(self, counts):
        # Counts is between 0 to 0x8000 inclusive
        return self._send_maxwell_message(MaxCmd.RFT_SET_PP_VSET_PWM_RAW, [(int(counts) >> 8) & 0xFF, int(counts) & 0xFF], transmit_mask=MaxConst.THRUSTER_CONTROLLER_ADDRESS)

    def rft_set_wav_gen_freq(self, freq):
        return self._send_maxwell_message(MaxCmd.RFT_SET_WAV_GEN_FREQ, self.__f32_to_raw_arr(freq), transmit_mask=MaxConst.THRUSTER_CONTROLLER_ADDRESS)

    def rft_get_tlm(self):
        tlm_dict = self._send_maxwell_message(MaxCmd.GET_TLM, [], transmit_mask=MaxConst.THRUSTER_CONTROLLER_ADDRESS)
        return tlm_dict

    def get_rft_tlm_on_prop (self):
        tlm_dict = self._send_maxwell_message(MaxCmd.RFT_GET_TLM, [])
        return tlm_dict

    def dft_pt_sell_your_soul(self, csv_file, transmit_mask=None):
        if transmit_mask is None:
            transmit_mask = self.default_hardware
        self.pt_format_dictarr = []
        with open(csv_file, 'r') as csvfile:
            csv_reader = csv.DictReader(csvfile)
            for param_format_item in csv_reader:
                self.pt_format_dictarr.append(param_format_item)
        return self.write_clank_command(MaxCmd.DFT_PT_SELL_YOUR_SOUL, [], transmit_mask=transmit_mask)

    def dft_pt_set(self, param_name, new_value, arr_idx=0, transmit_mask=None):
        if len(self.pt_format_dictarr) == 0:
            print("Mephistopheles: The Devil requires your soul before granting this wish")
            return None
        param_offset = -1
        for curr_fmt_param in self.pt_format_dictarr:
            if curr_fmt_param['Parameter'].lower() == param_name.lower().strip():
                param_offset = int(curr_fmt_param['Offset'])
                param_type = curr_fmt_param['Type']
                break

        if param_offset < 0:
            print("Mephistopheles: The Devil knows of no such parameter '%s'" % (param_name))
            return None

        arr_size = None
        if "[" in param_type and "]" in param_type:
            type_str = param_type.split('[')[0]
            arr_size = int(param_type.split('[')[1].split(']')[0])
        else:
            type_str = param_type

        if arr_size == None and arr_idx != 0:
            print("Mephistopheles: No tricksies! '%s' is not an array" % (param_name))
            return None
        elif arr_size != None and arr_idx >= arr_size:
            print("Mephistopheles: No tricksies! Param array only has %d elements, no idx %d" % (arr_idx, arr_size))
            return None
        else:
            param_offset_arr = self.__int_to_raw_arr(param_offset + (4*arr_idx))

        if isinstance(new_value, (float, int)):
            if type_str == 'u32':
                param_value_arr = self.__int_to_raw_arr(new_value)
            elif type_str == 'i32':
                param_value_arr = self.__int_to_raw_arr(new_value)
            elif type_str == 'f32':
                param_value_arr = self.__f32_to_raw_arr(new_value)
            else:
                print("Mephistopheles: Your param type '%s' displeases the Devil" % (type_str))
                return None
        else:
            print("Mephistopheles: Value '%s' displeases the Devil, he only deals with int/float" % (str(new_value)))
            return None

        if transmit_mask is None:
            transmit_mask = self.default_hardware
        return self.write_clank_command(MaxCmd.DFT_PT_SET, param_offset_arr + param_value_arr,
                                        transmit_mask=transmit_mask)

    def easter_egg_led(self):
        return self._send_maxwell_message(MaxCmd.LED_AUX_DRIVER_COMMAND, [])


    def write_clank_command(self, cmd, data, timeout=1, transmit_mask=None):
        if transmit_mask is None:
            transmit_mask = self.default_hardware
        return self.max_api.write_clank_command(cmd, data, timeout, transmit_mask=transmit_mask)

    def send_maxwell_message(self, cmd, data, transmit_mask=None, timeout=1):
        if transmit_mask is None:
            transmit_mask = self.default_hardware
        return self._send_maxwell_message(cmd, data, transmit_mask=transmit_mask, timeout=timeout)

    def __record_mr_maxwell_output(self, db_measurement, db_datasource, influx_host, python_file_name,enable_influx=True, enable_csv=True, custom_influx_tags={}):
        # Recording to max api based on the settings here
        self.max_api.record_maxwell_output(db_measurement, db_datasource, influx_host, python_file_name, enable_influx=enable_influx, enable_csv=enable_csv,custom_tags=custom_influx_tags)
        # Enabling or disabling annotation
        if self.annotate_msg_id:
            if self.max_api.clank_influx_database is not None:
                self.max_api.clank_influx_database.annotate_msg_id_enable = True
                self.max_api.clank_influx_database.update_post_annotation_black_list(MaxConst.ANNOTATION_BLACK_LIST)


    def __verify_ack(self, sent_msg, command_response):
        if self.check_ack:
            if command_response["status"] == "STATUS_SUCCESS":
                if ACK_OFFSET | sent_msg != command_response["msg_id"]:
                    command_response["status"] = "NACK_RECEIVED"
                    error_message = "RECEIVED A NACK: original message ID sent: " + str(hex(sent_msg)) + " ACK Response: " + str(hex(command_response["msg_id"]))
                    print(error_message, flush=True)
                    if self.halt_on_nack:
                        self.__emergency_abort()
                        sys.exit()
                elif command_response["application_status"] != 0:
                    print("RECEIVED A APPLICATION STATUS THAT IS NOT SUCCESSFUL IT WAS: " + str(hex(command_response["application_status"])))
            elif command_response["status"] == "EMBEDDED_TIMEOUT":
                print("THERE IS A EMBEDDED_TIMEOUT PLEASE CHECK IF SYSTEM IS POWERED AND CONNECTED TO COMMUNICATE ")
            elif command_response["status"] == "NOT_ENOUGH_BYTES":
                print("THERE IS NOT ENOUGH BYTES TO COMMUNICATE PROPERLY PLEASE CHECK COMMUNICATION HARNESS")
            else:
                print("There is an error in communication the error was :" + str(command_response["status"]))
        return command_response

    def __emergency_abort(self):
        count = 0
        print("SENDING EMERGENCY ABORT COMMANDS TO MAXWELL")
        results = self.abort()
        while results["status"] != "STATUS_SUCCESS" and count < 5:
            results = self.abort()
            count = count + 1
        if count >= 5:
            print("ERROR MAXWELL IS NOT COMMUNICATING CAN NOT SEND ABORT COMMAND PLEASE POWER OFF HARDWARE", flush=True)
            return False
        else:
            print("MAXWELL WENT SUCCESSFULLY INTO SAFE")
            return True


    def __int_to_raw_arr(self, int_val):
        u32_val = int(int_val) & 0xFFFFFFFF
        arr = []
        arr.append((u32_val >> 24) & 0xFF)
        arr.append((u32_val >> 16) & 0xFF)
        arr.append((u32_val >> 8) & 0xFF)
        arr.append((u32_val) & 0xFF)
        return arr

    def __int16_to_raw_arr(self, int_val):
        u16_val = int(int_val) & 0xFFFF
        arr = []
        arr.append((u16_val >> 8) & 0xFF)
        arr.append((u16_val) & 0xFF)
        return arr

    def __f32_to_raw_arr(self, float_val):
        float_val_hex = struct.unpack('<I', struct.pack('<f', float(float_val)))[0]
        return self.__int_to_raw_arr(float_val_hex)

    @classmethod
    def print_mr_max_singleton_instances(cls):
        print(f"mr_max singleton instances are {cls.mr_max_singleton_instance} with a length of {len(cls.mr_max_singleton_instance)}")

    @classmethod
    def remove_object_from_singleton_instance_list(cls, class_object):
        for instance in cls.mr_max_singleton_instance:
            if instance == class_object:
                cls.mr_max_singleton_instance.remove(instance)

    @classmethod
    def clear_singleton_instances(cls):
        cls.mr_max_singleton_instance = []
