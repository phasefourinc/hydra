import logging
import os
import pathlib
import sys
import time

from maxapi.maxwell_information_processor import get_all_the_software_info,p4_version_parser, _parse_software_record
from maxapi.mr_maxwell_b2 import MrMaxwell, MaxTlm, MaxConst


class SrecordParser:
    """
    This class is used to parse and extract a software image, either
    App or Parameter Table, and load it into a buffer of raw bytes.
    This class also facilitates reading the length and CRC from the
    generic image header.
    """

    # SREC lines expect to have the following format:
    #    |Srec type: S3=Data (1B)
    #   | |Line length of all data following including checksum (1B)
    #  | | |Address (8B)
    # | | |       |Data (variable)                |Checksum (1B)
    # S31500000100C9040000C9040000C9040000C9040000B5

    IMG_HEADER_SIZE = 0x100

    # The file_path must be valid
    def __init__(self, file_path, img_addr_start):
        self.file_path = file_path
        self.max_img_size = int(self.IMG_HEADER_SIZE + (128 * 1024))

        # Coerce start address to be 4 byte aligned
        self.img_addr_start = int(img_addr_start & 0xFFFFFFFC)
        self.img_addr_end = int(img_addr_start + self.max_img_size)

        # Initialize the image to all F's, mimicking a cleared flash
        self.img_buf_bytes = [0xFF] * self.max_img_size

        # Load the image at the specified start address into a buffer
        self.get_img_from_file()

        self.flash_header = _parse_software_record(self.img_buf_bytes)

        # The img_buf_bytes points to the very first element of the generic header.
        # The length is at an offset of 4, however must also convert LE to BE. Don't
        # forget to include the header size.
        self.img_size = int(self.flash_header["software_length"], 16) + self.IMG_HEADER_SIZE


        # The img_buf_bytes points to the very first element of the generic header.
        # The length is at an offset of 8, however must also convert LE to BE
        self.img_crc = int(self.flash_header["software_crc"], 16)

        # The img_buf_bytes points to the very first element of the generic header.
        # The image type is at an offset of 0, however must also convert LE to BE
        self.img_type = self.flash_header["software_image_type"]


        # The img_buf_bytes points to the very first element of the generic header.
        # The image version is at an offset of 0, however must also convert LE to BE
        self.img_version = self.flash_header["software_version"]


        # Trim the extra fluff beyond the image data
        self.img_buf_bytes = self.img_buf_bytes[:self.img_size]

    def get_img_bytes_arr(self):
        return self.img_buf_bytes

    def get_img_size(self):
        return self.img_size

    def get_img_crc(self):
        return self.img_crc

    def get_img_type(self):
        return self.img_type

    def get_img_version(self):
        return self.img_version
    def get_img_from_file(self):
        """
        This function works by taking a buffer full of 0xFFs that fits
        the size of the maximum image size, and then parsing through
        each line of the srecord and overwriting data bytes in the
        buffer that fall within the appropriate address range.
        :return:
        """
        self.file = open(self.file_path, "r")

        i = 0
        for line in self.file:
            i = i + 1

            # Empty array to temporarily hold the data line bytes
            line_data_bytes = []

            # Remove any newline chars at the end of the line
            curr_line_str = line.rstrip()

            # Grab srecord line type, skipping lines that are anything but data for 32-bit addresses
            srec_type = curr_line_str[0:2]
            curr_line_str = curr_line_str[2:]
            if srec_type != "S3":
                continue

            # Grab the srecord line length, skipping lines that mismatch
            srec_length = int(curr_line_str[0:2], 16)
            curr_line_str = curr_line_str[2:]
            if int(len(curr_line_str) / 2) != srec_length:
                print("Info: Invalid srecord length %d. Skipping line %d" % (len(line), i), flush=True)
                print("Line raw: %s" % line, flush=True)
                continue

            # Grab the srecord 32-bit line address, skipping lines that are outside of the range
            srec_addr = int(curr_line_str[0:8], 16)
            curr_line_str = curr_line_str[8:]
            if srec_addr < self.img_addr_start or srec_addr >= self.img_addr_end:
                continue

            # Turn the rest of the line chars into a byte array for data
            for i in range(0, int(len(curr_line_str) / 2)):
                line_data_bytes.append(int(curr_line_str[:2], 16))
                curr_line_str = curr_line_str[2:]

            # Remove the checksum byte at the end of the array in place
            del line_data_bytes[-1]

            # At this point line_data_bytes has all the data, so we just need to place that data
            # into the appropriate image buffer index
            offset_addr = srec_addr - self.img_addr_start

            for curr_byte in line_data_bytes:
                self.img_buf_bytes[offset_addr] = curr_byte
                offset_addr = offset_addr + 1

        self.file.close()


# Finds all of the .srec files in the specified dir path
def find_srecords_in_dir(dir_path):
    srec_file_match = ""
    for file in os.listdir(dir_path):
        if ".srec" in file.lower():
            if srec_file_match == "":
                srec_file_match = file.lower()
            else:
                print("ERROR: Multiple srecords detected!", flush=True)
                print("Found files: %s, %s" % (srec_file_match, file), flush=True)
                return ""

    if (srec_file_match == ""):
        print("ERROR: No srecord found!", flush=True)
        print("Please add a .srec file to this script's directory or use --srec to pass one in", flush=True)
        return ""
    else:
        return srec_file_match


class LoadMaxwellSoftware:
    def __init__(self, mr_max, srec_image, srec_crc, hardware_type):
        self.mr_max = mr_max
        self.srec_image = srec_image
        self.srec_crc = srec_crc
        if hardware_type == "Prop Controller":
            self.tx_mask = MaxConst.PROP_CONTROLLER_ADDRESS
            self.data_mode_tlm = str(MaxTlm.EC_DATA_MODE.name).lower()
            self.hardware_string = "Prop Controller"
            self.GET_STATE_STRING = "ec_state"
            self.configured = True
            self.rest_state = 1
            self.going_through_prop = False
        elif hardware_type == "Thruster Controller":
            self.tx_mask = MaxConst.THRUSTER_CONTROLLER_ADDRESS
            self.data_mode_tlm = str(MaxTlm.TC_DATA_MODE).lower()
            self.hardware_string = "Thruster Controller"
            if self.mr_max.prop_connected == True:
                self.going_through_prop = True
            else:
                self.going_through_prop = False
            self.GET_STATE_STRING = "tc_state"
            self.configured = True
            self.rest_state = 7
        else:
            self.configured = False

    def get_srec_image(self):
        return self.srec_image

    def get_srec_crc(self):
        return self.srec_crc

    def get_mr_maxwell(self):
        return self.mr_max

    def update_srec(self, srec_image, srec_crc):
        self.srec_image = srec_image
        self.srec_crc = srec_crc

    def update_mr_max(self, mr_max):
        self.mr_max = mr_max

    def get_target_hardware(self):
        return self.hardware_string

    def set_hardware(self, hardware_type):
        if hardware_type == "Prop Controller":
            self.tx_mask = MaxConst.PROP_CONTROLLER_ADDRESS
            self.data_mode_tlm = str(MaxTlm.EC_DATA_MODE.name).lower()
            self.hardware_string = "Prop Controller"
            self.configured = True
            self.rest_state = 1
            self.GET_STATE_STRING = "ec_state"
            self.going_through_prop = False

        elif hardware_type == "Thruster Controller":
            self.tx_mask = MaxConst.THRUSTER_CONTROLLER_ADDRESS
            self.data_mode_tlm = str(MaxTlm.TC_DATA_MODE.name).lower()
            self.hardware_string = "Thruster Controller"
            self.GET_STATE_STRING = "tc_state"
            self.rest_state = 7
            self.configured = True
            if self.mr_max.prop_connected == True:
                self.going_through_prop = True
            else:
                self.going_through_prop = False
        else:
            print("Invalid Hardware Selection", flush=True)
            self.configured = False

    def prepare_maxwell_system_for_update(self):
        error_message = "Maxwell preparation failed"
        # Safing the prop controller first.
        # This is only needed if we are loading software with the thruster and connected to the prop controller.
        if self.going_through_prop == True:
            self.mr_max.goto_safe(transmit_mask=MaxConst.PROP_CONTROLLER_ADDRESS)
            time.sleep(1)
            self.mr_max.sleep_w_tlm(1.0, transmit_mask=MaxConst.PROP_CONTROLLER_ADDRESS)
            output = self.mr_max.get_tlm(transmit_mask=MaxConst.PROP_CONTROLLER_ADDRESS)
            if output["status"] != "STATUS_SUCCESS":
                software_load_log.error(str(output))
                return self._internal_fail_procedure(error_message)

            error_status = self._check_errors(output["parsed_payload"], check_prop_flag=True)
            if output["parsed_payload"]["STATE"] != 1 or error_status != "STATUS_SUCCESS":
                software_load_log.error(str(output))
                return self._internal_fail_procedure(error_message)

        self.mr_max.goto_safe(transmit_mask=self.tx_mask)
        time.sleep(1)
        self.mr_max.sleep_w_tlm(1.0, transmit_mask=self.tx_mask)
        output = self.mr_max.get_tlm(transmit_mask=self.tx_mask)

        if output["status"] != "STATUS_SUCCESS":
            software_load_log.error(str(output))
            return self._internal_fail_procedure(error_message)

        error_status = self._check_errors(output["parsed_payload"])
        if output["parsed_payload"][self.GET_STATE_STRING] != self.rest_state or error_status != "STATUS_SUCCESS":
            software_load_log.error(str(output))
            return self._internal_fail_procedure(error_message)

        output = self.mr_max.clear_errors_advanced("Telemetry Error Record", transmit_mask=self.tx_mask)
        output = self.mr_max.get_tlm(transmit_mask=self.tx_mask)
        if output["status"] != "STATUS_SUCCESS":
            software_load_log.error(str(output))
            return self._internal_fail_procedure(error_message)

        error_status = self._check_errors(output["parsed_payload"])
        if error_status != "STATUS_SUCCESS":
            software_load_log.error(str(output))
            return self._internal_fail_procedure(error_message)

        self.mr_max.sleep_w_tlm(1.0, transmit_mask=self.tx_mask)

        # Putting  the prop controller in.
        # This is only needed if we are loading software with the thruster and connected to the prop controller.

        if self.going_through_prop == True:
            self.mr_max.goto_sw_update(transmit_mask=MaxConst.PROP_CONTROLLER_ADDRESS)
            self.mr_max.sleep_w_tlm(1.0, transmit_mask=MaxConst.PROP_CONTROLLER_ADDRESS)
            output = self.mr_max.get_tlm(transmit_mask=MaxConst.PROP_CONTROLLER_ADDRESS)

            if output["status"] != "STATUS_SUCCESS":
                software_load_log.error(str(output))
                return self._internal_fail_procedure(error_message)

            error_status = self._check_errors(output["parsed_payload"], check_prop_flag=True)
            if output["parsed_payload"]["STATE"] != 8 or error_status != "STATUS_SUCCESS":
                software_load_log.error(str(output))
                return self._internal_fail_procedure(error_message)

        self.mr_max.goto_sw_update(transmit_mask=self.tx_mask)
        self.mr_max.sleep_w_tlm(1.0, transmit_mask=self.tx_mask)
        output = self.mr_max.get_tlm(transmit_mask=self.tx_mask)

        if output["status"] != "STATUS_SUCCESS":
            software_load_log.error(str(output))
            return self._internal_fail_procedure(error_message)

        error_status = self._check_errors(output["parsed_payload"])
        if output["parsed_payload"][self.GET_STATE_STRING] != 8 or error_status != "STATUS_SUCCESS":
            software_load_log.error(str(output))
            return self._internal_fail_procedure(error_message)

        self.mr_max.sleep_w_tlm(1.0, transmit_mask=self.tx_mask)

        # print(self.hardware_string + " ready for update...", flush=True)
        return True

    def stage_software(self, srec_parser):
        output = self.mr_max.sw_update_stage(0x0, srec_parser.get_img_bytes_arr(), transmit_mask=self.tx_mask)
        if output == None or output["status"] != "STATUS_SUCCESS":
            self.program_failed_clean_up()
            software_load_log.error(str(output))
            return False
        software_load_log.info("Staging succeeded!")
        # print("Staging succeeded!")
        self.mr_max.sleep_w_tlm(5.0, transmit_mask=self.tx_mask)
        return True

    def program_primary_app_one(self):
        if self.configured:
            status = self._programming_software_from_fram("Primary Application One")
            if status == False:
                self._internal_fail_procedure("Cannot Program Primary Application One")
            return status
        else:
            self.program_failed_clean_up()
            return False

    def program_primary_app_two(self):
        if self.configured:
            status = self._programming_software_from_fram("Primary Application Two")
            if status == False:
                self._internal_fail_procedure("Cannot Program Primary Application Two")
            return status
        else:
            self.program_failed_clean_up()
            return False

    def program_failed_clean_up(self):
        print("OH LOOK WHAT YOU DID CAROL... Programming failed. Cleaning up what was done.. ", flush=True)
        error_message = "Can not clean up completely. Can not put system into safe. Please reset power to the maxwell system in order to try again."
        count = 0
        clean_up_status = False
        output_prop = None
        while count < 3 and clean_up_status == False:
            if self.going_through_prop:
                self.mr_max.goto_safe(transmit_mask=MaxConst.PROP_CONTROLLER_ADDRESS)
                time.sleep(1)
                self.mr_max.sleep_w_tlm(1.0, transmit_mask=MaxConst.PROP_CONTROLLER_ADDRESS)
                output_prop = self.mr_max.get_tlm(transmit_mask=MaxConst.PROP_CONTROLLER_ADDRESS)

            self.mr_max.goto_safe(transmit_mask=self.tx_mask)
            time.sleep(1)
            self.mr_max.sleep_w_tlm(1.0, transmit_mask=self.tx_mask)
            output = self.mr_max.get_tlm(transmit_mask=self.tx_mask)
            if self.going_through_prop:
                if output["status"] == "STATUS_SUCCESS" and output_prop["status"] == "STATUS_SUCCESS":
                    error_status = self._check_errors(output["parsed_payload"])
                    if output["parsed_payload"][
                        self.GET_STATE_STRING] == self.rest_state and error_status == "STATUS_SUCCESS":
                        clean_up_status = True
                count = count + 1
            else:
                if output["status"] == "STATUS_SUCCESS":
                    error_status = self._check_errors(output["parsed_payload"])
                    if output["parsed_payload"][
                        self.GET_STATE_STRING] == self.rest_state and error_status == "STATUS_SUCCESS":
                        clean_up_status = True
                count = count + 1

        if clean_up_status == False:
            print(error_message, flush=True)

    def _internal_fail_procedure(self, error_message):
        print(error_message, flush=True)
        self.program_failed_clean_up()
        return False

    def _programming_software_from_fram(self, application_number):
        if application_number == "Primary Application One":
            flash_address_location = 0x80000
        elif application_number == "Primary Application Two":
            flash_address_location = 0xC0000
        else:
            print("There is a error in selecting the allowable ", flush=True)
        software_load_log.info("Programming primary application at " + str(hex(flash_address_location)))
        # print("Programming primary application at " + str(hex(flash_address_location)), flush=True)

        # This paragdime you see below is to test run function a couple times if it fails.
        # Since this a tool we want to try a couple times to see if works. Seems a bit excesive may want to try another way to solve this.
        count = 0
        status = False
        # sending mutiple times
        while count < 3 and status == False:
            com_status = self.mr_max.sw_update_program(flash_address_location, self.srec_image, self.srec_crc,
                                                       transmit_mask=self.tx_mask)
            if com_status["status"] == "STATUS_SUCCESS":
                status = True
            count = count + 1

        if status == False:
            software_load_log.error(str(com_status))
            print("Failed to send update program!!", flush=True)
            return False

        self.mr_max.sleep_w_tlm(8.0, transmit_mask=self.tx_mask, freq=1)

        count = 0
        status = False
        while count < 3 and status == False:
            output = self.mr_max.get_tlm(transmit_mask=self.tx_mask)
            if output["status"] == "STATUS_SUCCESS":
                parsed_tlm = output["parsed_payload"]
                status = True
            count = count + 1

        if not status:
            software_load_log.error(str(output))
            return False

        data_mode = parsed_tlm[self.data_mode_tlm]
        err_status = self._check_errors(parsed_tlm)

        if data_mode == 15 and err_status == "STATUS_SUCCESS":
            software_load_log.info("Programming complete!")
            # print("Programming complete!", flush=True)
            return True
        else:
            if data_mode == 15:
                print("Programming FAILED! Error was " + str(err_status))
                software_load_log.error(str(output))
            else:
                print("Programming FAILED! Data mode = %d" % (data_mode))
                software_load_log.error(str(output))
            return False

    def _check_errors(self, parsed_data, check_prop_flag=False):
        #Needs to fix
        return "STATUS_SUCCESS"
        if check_prop_flag == True:
            error_1 = parsed_data["SC_ERR_CODE1"]
            error_2 = parsed_data["SC_ERR_CODE2"]
            error_3 = parsed_data["SC_ERR_CODE3"]
        else:
            if self.tx_mask == MaxConst.PROP_CONTROLLER_ADDRESS:
                error_1 = parsed_data["SC_ERR_CODE1"]
                error_2 = parsed_data["SC_ERR_CODE2"]
                error_3 = parsed_data["SC_ERR_CODE3"]

            if self.tx_mask == MaxConst.THRUSTER_CONTROLLER_ADDRESS:
                error_1 = parsed_data["RFT_ERR1_CODE"]
                error_2 = parsed_data["RFT_ERR2_CODE"]
                error_3 = parsed_data["RFT_ERR3_CODE"]

        if error_1 + error_2 + error_3 == 0:
            return "STATUS_SUCCESS"
        else:
            return error_1


def load_software(com_port, baud_rate, hardware_type, srec="", status_queue=None):
    mr_maxwell = MrMaxwell(com_port, baud_rate, "sandbox", "software_load", enable_influx=True, enable_startup_scripts=False, enable_csv=False)
    software_load_successful = False
    if "pcu_sw" in hardware_type:
        hardware_types = ["pcu_sw", "hydra_ec_sw", "edu_hydra_ec_sw"]
        hardware_type_string = "Prop Controller"
        tx_mask = MaxConst.PROP_CONTROLLER_ADDRESS
        GET_STATE_STRING = "ec_state"
        REST_STATE = 1
        if mr_maxwell.prop_connected == False:
            print("Cannot upload software to the Prop Controller because the tool can not talk to the Prop Controller", flush=True)
            return software_load_successful
    else:
        hardware_types = ["thr_sw", "hydra_tc_sw", "edu_hydra_tc_sw"]
        hardware_type_string = "Thruster Controller"
        tx_mask = MaxConst.THRUSTER_CONTROLLER_ADDRESS
        GET_STATE_STRING = "tc_state"
        REST_STATE = 7
        if mr_maxwell.thrust_connected == False:
            print(
                "Cannot upload software to the Thruster Controller because the tool can not talk to the Thruster Controller", flush=True)
            return software_load_successful

    # Use the specified srecord if passed in, otherwise search the current script
    # directory for one srecord to use
    srec_file = ""
    if (".srec" in srec):
        # If srecord was specified, then us it
        srec_file = srec.lower()

    else:
        # TODO check bender to see if I can get a srecord file.
        dir_path = os.getcwd()
        srec_filename = find_srecords_in_dir(dir_path.lower())

        if srec_filename == "":
            print("No SREC file %s found at path %s" % (srec_filename.lower(), dir_path.lower()), flush=True)
            sys.exit()


        # Combine with filename to get full path, replace stupid windows slashes with unix slashes
        srec_file = os.path.join(dir_path.lower(), srec_filename.lower())
        srec_file = srec_file.replace("\\", "/")


    # We have the srec file path now. Lets parse the srecord.
    print("Using Firmware file: " + srec_file, flush=True)

    # Checking to see the firmware is meant for the hardware we are loading for.
    # This is done by checking the version in the srec file.
    srec_parser = SrecordParser(srec_file, int("0x40000", 0))
    firmware_name = srec_parser.get_img_version()
    fw_version_info = p4_version_parser(firmware_name)

    if fw_version_info:
        if "pcu_sw" in fw_version_info.label or "ec_sw" in fw_version_info.label:
            hardware_str = "pcu_sw"
        elif "thr_sw" in fw_version_info.label or "tc_sw" in fw_version_info.label:
            hardware_str = "thr_sw"
        elif "max_api" in fw_version_info.label:
            hardware_str = "max_api"
        else:
            hardware_str = fw_version_info.label
        if hardware_str not in hardware_types:
            print("The SREC you inputed is not for the chosen hardware you specified. Make sure you srec file is a meant for your hardware. Shutting Down...",flush=True)
            sys.exit()
    else:
        print("The SREC naming is not accurate please check the srec naming and try again. Shutting Down...",flush=True)
        sys.exit()

    new_img_size = srec_parser.get_img_size()
    new_img_crc = srec_parser.get_img_crc()

    print("Srecord parsing complete!", flush=True)
    print("Image size: 0x%08x (%d)" % (new_img_size, new_img_size), flush=True)
    print("Image CRC: 0x%08x" % (new_img_crc), flush=True)
    print("", flush=True)
    print("Starting Programming...")


    update_software_status_queue(status_queue, 1)
    max_soft_class = LoadMaxwellSoftware(mr_maxwell, new_img_size, new_img_crc, hardware_type_string)
    software_load_log.info("Preparing Maxwell for software update....")
   #print("Preparing Maxwell for software update....", flush=True)
    update_software_status_queue(status_queue, 2)
    software_load_successful = max_soft_class.prepare_maxwell_system_for_update()
    update_software_status_queue(status_queue, 3)
    if not software_load_successful:
        return software_load_successful

    software_load_log.info("Preparation Complete")
    software_load_log.info("Staging Software to FRAM....")
    # print("Preparation Complete", flush=True)
    # print("Staging Software to FRAM....", flush=True)
    update_software_status_queue(status_queue, 7)
    software_load_successful = max_soft_class.stage_software(srec_parser)
    if not software_load_successful:
        return software_load_successful
    update_software_status_queue(status_queue, 8)
    software_load_log.info("Staging Complete")
    software_load_log.info("Programing software in primary app 1....")
    # print("Staging Complete", flush=True)
    # print("Programing software in primary app 1....", flush=True)
    software_load_successful = max_soft_class.program_primary_app_one()

    if not software_load_successful:
        return software_load_successful
    update_software_status_queue(status_queue, 9)

    software_load_log.info("Programing software in primary app 2....")
    # print("Programing software in primary app 2....", flush=True)
    software_load_successful = max_soft_class.program_primary_app_two()
    if not software_load_successful:
        return software_load_successful
    update_software_status_queue(status_queue, 10)

    software_load_log.info("Programing Complete.")
    # print("Programing Complete.", flush=True)
    software_load_log.info("Cleaning up the mess...")
    # print("Cleaning up the mess...", flush=True)
    if max_soft_class.going_through_prop == True:
        mr_maxwell.goto_safe(transmit_mask=MaxConst.PROP_CONTROLLER_ADDRESS)
        time.sleep(1)
        mr_maxwell.sleep_w_tlm(1.0, transmit_mask=MaxConst.PROP_CONTROLLER_ADDRESS)
        output = mr_maxwell.get_tlm(transmit_mask=MaxConst.PROP_CONTROLLER_ADDRESS)
        software_load_log.info(str(output))
        if output["status"] != "STATUS_SUCCESS":
            software_load_log.error(str(output))
            return max_soft_class._internal_fail_procedure("There was errors in cleaning up from the software load. Please try again.")

        software_load_log.info(str(output))
        error_status = max_soft_class._check_errors(output["parsed_payload"], check_prop_flag=True)
        if output["parsed_payload"]["STATE"] != 1 or error_status != "STATUS_SUCCESS":
            software_load_log.error(str(output))
            return max_soft_class._internal_fail_procedure(
                "There was errors in cleaning up from the software load. Please try again.")
    time.sleep(1)

    output = mr_maxwell.get_tlm(transmit_mask=tx_mask)
    if output["status"] != "STATUS_SUCCESS":
        software_load_log.error(str(output))
        return max_soft_class._internal_fail_procedure("There was errors in updating software. Please try again.")

    time.sleep(1)

    output = mr_maxwell.goto_safe(transmit_mask=tx_mask)
    if output["status"] != "STATUS_SUCCESS":
        software_load_log.error(str(output))
        return max_soft_class._internal_fail_procedure("There was errors in updating software. Please try again.")

    time.sleep(1)

    output = mr_maxwell.get_tlm(transmit_mask=tx_mask)
    if output["status"] != "STATUS_SUCCESS":
        software_load_log.error(str(output))
        return max_soft_class._internal_fail_procedure("There was errors in updating software. Please try again.")

    time.sleep(1)

    output = mr_maxwell.get_tlm(transmit_mask=tx_mask)
    if output["status"] != "STATUS_SUCCESS":
        software_load_log.error(str(output))
        return max_soft_class._internal_fail_procedure("There was errors in updating software. Please try again.")


    software_load_log.info(str(output))

    error_status = max_soft_class._check_errors(output["parsed_payload"])
    if output["parsed_payload"][GET_STATE_STRING] != REST_STATE or error_status != "STATUS_SUCCESS":
        software_load_log.error(str(output))
        return max_soft_class._internal_fail_procedure("There was errors in updating software. Please try again.")

    # update_software_status_queue(status_queue, 9)
    software_load_log.info("Clean as a whistle")
    software_load_log.info("Verifying Software")
    # print("Clean as a whistle", flush=True)
    # print("Verifying Software", flush=True)
    parsed_software_list, status = get_all_the_software_info(mr_maxwell, tx_mask)
    software_load_log.info(str(status))
    if status == "STATUS_SUCCESS":
        primary_app_one_info = parsed_software_list[3]  # Flash Application 1
        primary_app_two_info = parsed_software_list[4]  # Flash Application 2
        if (primary_app_one_info["software_version"] == primary_app_one_info["software_version"]) and \
                primary_app_one_info["software_version"] == firmware_name:
            software_load_log.info("\nSoftware Verified!!\n")
            # print("\nSoftware Verified!!\n", flush=True)
            software_load_successful = True
        else:
            print(
                "The software verification failed one of the apps did not update correctly. Here is the information below.\n"
                "1) Firmware Name to program on hardware: " + str(firmware_name) + "\n"
                "2) Firmware Name of primary program 1: " + str(primary_app_one_info["software_version"]) + "\n"
                "3) Firmware Name to primary program 2: " + str(primary_app_two_info["software_version"]) + "\n", flush=True)
            software_load_successful = False
    else:
        print("Can not verify if software is correct. Please run a get software and verify yourself.", flush=True)
        software_load_successful = False
    # update_software_status_queue(status_queue, 10)
    return software_load_successful


def _setup_logger(name, log_file, level=logging.INFO, print_to_screen=False):
    """Function setup as many loggers as you want"""
    formatter = logging.Formatter('%(asctime)s.%(msecs)03d p%(process)s {%(pathname)s:%(lineno)d} %(levelname)s: %(message)s')
    handler = logging.FileHandler(log_file, mode='w')
    handler.setFormatter(formatter)

    logger = logging.getLogger(name)
    logger.setLevel(level)
    logger.addHandler(handler)
    if print_to_screen == True:
        handler.stream
        logger.addHandler(logging.StreamHandler(sys.stdout))
    else:
        pass

    return logger



def update_software_status_queue(status_queue, number):
    if status_queue == None:
        return
    else:
        status_queue.put(number)


current_directory = os.path.dirname(os.path.abspath(__file__))
pathlib.Path(os.path.join(current_directory, "logs", "")).mkdir(parents=True, exist_ok=True)
software_load_log = _setup_logger('software_load_log', os.path.join(current_directory, "logs", "software_load_log.log"))
