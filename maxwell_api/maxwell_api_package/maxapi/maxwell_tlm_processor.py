from maxapi.mr_maxwell import MaxConst

def confirm_command_status(output,tx_mask = MaxConst.PROP_CONTROLLER_ADDRESS):
    if check_ack(output):
        if check_for_any_error_status(output,tx_mask) == False:
            return True

    return False

def check_unix_timestamp_is_set(prop_telm,thrust_tlm):
    phasefour_founding_epoch =1420099200
    if prop_telm["parsed_payload"]["UNIX_TIME_US"] >= phasefour_founding_epoch and thrust_tlm["parsed_payload"]["UNIX_TIME_US"] >= phasefour_founding_epoch:
        return True
    return False


def confirm_command_tlm(output, tx_mask = MaxConst.PROP_CONTROLLER_ADDRESS):
    if check_ack(output):
        if check_for_any_error_tlm(output,tx_mask) == False:
            return True

    return False


def check_for_any_error_status(payload, tx_mask = MaxConst.PROP_CONTROLLER_ADDRESS):
    output = {
        "status": "NO_ERROR",
        "error1": 0,
        "error2": 0,
        "error3": 0
    }
    found_error = False
    if tx_mask == MaxConst.PROP_CONTROLLER_ADDRESS:
        if payload["parsed_payload"]["SC_ERR_CODE1"] != 0:
            output["status"] = "ERROR"
            output["error1"] = payload["parsed_payload"]["SC_ERR_CODE1"]
            found_error = True
        elif payload["parsed_payload"]["SC_ERR_CODE2"] != 0:
            output["status"] = "ERROR"
            output["error2"] = payload["parsed_payload"]["SC_ERR_CODE2"]
            found_error = True
        elif payload["parsed_payload"]["SC_ERR_CODE3"] != 0:
            output["status"] = "ERROR"
            output["error3"] = payload["parsed_payload"]["SC_ERR_CODE3"]
            found_error = True
    else:
        if payload["parsed_payload"]["RFT_SC_ERR_CODE1"] != 0:
            output["status"] = "ERROR"
            output["error1"] = payload["parsed_payload"]["RFT_SC_ERR_CODE1"]
            found_error = True
        elif payload["parsed_payload"]["RFT_SC_ERR_CODE2"] != 0:
            output["status"] = "ERROR"
            output["error2"] = payload["parsed_payload"]["RFT_SC_ERR_CODE2"]
            found_error = True
        elif payload["parsed_payload"]["RFT_SC_ERR_CODE3"] != 0:
            output["status"] = "ERROR"
            output["error3"] = payload["parsed_payload"]["RFT_SC_ERR_CODE3"]
            found_error = True

    return found_error


def parse_telemetry_errors(payload, tx_mask = MaxConst.PROP_CONTROLLER_ADDRESS):
    tlm_error_dict = {
        "status": "NO_ERROR",
        "sc_error1": 0,
        "sc_error2": 0,
        "sc_error3": 0,
        "tlm_error1": 0,
        "tlm_error2": 0,
        "tlm_error3": 0,
        "tlm_hx_error1": 0,
        "tlm_hx_error2": 0,
        "tlm_hx_error3": 0,
    }
    if tx_mask == MaxConst.PROP_CONTROLLER_ADDRESS:
        if (payload["parsed_payload"]["SC_ERR_CODE1"] != 0):
            tlm_error_dict["status"] = "ERROR"
            tlm_error_dict["sc_error1"] = payload["parsed_payload"]["SC_ERR_CODE1"]
            
        elif payload["parsed_payload"]["SC_ERR_CODE2"] != 0:
            tlm_error_dict["status"] = "ERROR"
            tlm_error_dict["sc_error2"] = payload["parsed_payload"]["SC_ERR_CODE2"]

        elif payload["parsed_payload"]["SC_ERR_CODE3"] != 0:
            tlm_error_dict["status"] = "ERROR"
            tlm_error_dict["sc_error3"] = payload["parsed_payload"]["SC_ERR_CODE3"]

        elif (payload["parsed_payload"]["ERR1_CODE"] != 0):
            tlm_error_dict["status"] = "ERROR"
            tlm_error_dict["tlm_error1"] = payload["parsed_payload"]["ERR1_CODE"]
            
        elif payload["parsed_payload"]["ERR2_CODE"] != 0:
            tlm_error_dict["status"] = "ERROR"
            tlm_error_dict["tlm_error2"] = payload["parsed_payload"]["ERR2_CODE"]
            
        elif payload["parsed_payload"]["ERR3_CODE"] != 0:
            tlm_error_dict["status"] = "ERROR"
            tlm_error_dict["tlm_error3"] = payload["parsed_payload"]["ERR3_CODE"]

        elif payload["parsed_payload"]["ERRHX1_CODE"] != 0:
            tlm_error_dict["status"] = "ERROR"
            tlm_error_dict["tlm_hx_error1"] = payload["parsed_payload"]["ERRHX1_CODE"]

        elif payload["parsed_payload"]["ERRHX2_CODE"] != 0:
            tlm_error_dict["status"] = "ERROR"
            tlm_error_dict["tlm_hx_error2"] = payload["parsed_payload"]["ERRHX2_CODE"]

        elif payload["parsed_payload"]["ERRHX3_CODE"] != 0:
            tlm_error_dict["tlm_hx_error3"] = payload["parsed_payload"]["ERRHX3_CODE"]
            tlm_error_dict["status"] = "ERROR"

    else:
        if payload["parsed_payload"]["RFT_SC_ERR_CODE1"] != 0:
            tlm_error_dict["status"] = "ERROR"
            tlm_error_dict["sc_error1"] = payload["parsed_payload"]["RFT_SC_ERR_CODE1"]

        elif payload["parsed_payload"]["RFT_SC_ERR_CODE2"] != 0:
            tlm_error_dict["status"] = "ERROR"
            tlm_error_dict["sc_error2"] = payload["parsed_payload"]["RFT_SC_ERR_CODE2"]

        elif payload["parsed_payload"]["RFT_SC_ERR_CODE3"] != 0:
            tlm_error_dict["status"] = "ERROR"
            tlm_error_dict["sc_error3"] = payload["parsed_payload"]["RFT_SC_ERR_CODE3"]


        elif payload["parsed_payload"]["RFT_ERR1_CODE"] != 0:
            tlm_error_dict["status"] = "ERROR"
            tlm_error_dict["tlm_error1"] = payload["parsed_payload"]["RFT_ERR1_CODE"]

        elif payload["parsed_payload"]["RFT_ERR2_CODE"] != 0:
            tlm_error_dict["status"] = "ERROR"
            tlm_error_dict["tlm_error2"] = payload["parsed_payload"]["RFT_ERR2_CODE"]

        elif payload["parsed_payload"]["RFT_ERR3_CODE"] != 0:
            tlm_error_dict["status"] = "ERROR"
            tlm_error_dict["tlm_error3"] = payload["parsed_payload"]["RFT_ERR3_CODE"]

        elif payload["parsed_payload"]["RFT_ERRHX1_CODE"] != 0:
            tlm_error_dict["status"] = "ERROR"
            tlm_error_dict["tlm_hx_error1"] = payload["parsed_payload"]["RFT_ERRHX1_CODE"]

        elif payload["parsed_payload"]["RFT_ERRHX2_CODE"] != 0:
            tlm_error_dict["status"] = "ERROR"
            tlm_error_dict["tlm_hx_error2"] = payload["parsed_payload"]["RFT_ERRHX2_CODE"]

        elif payload["parsed_payload"]["RFT_ERRHX3_CODE"] != 0:
            tlm_error_dict["tlm_hx_error3"] = payload["parsed_payload"]["RFT_ERRHX3_CODE"]
            tlm_error_dict["status"] = "ERROR"


    return tlm_error_dict
def check_for_any_error_tlm(payload, tx_mask = MaxConst.PROP_CONTROLLER_ADDRESS):
    output = {
        "status": "NO_ERROR",
        "sc_error1": 0,
        "sc_error2": 0,
        "sc_error3": 0,
        "tlm_error1": 0,
        "tlm_error2": 0,
        "tlm_error3": 0,
        "tlm_hx_error1": 0,
        "tlm_hx_error2": 0,
        "tlm_hx_error3": 0,
    }
    found_error = False
    if tx_mask == MaxConst.PROP_CONTROLLER_ADDRESS:
        if (payload["parsed_payload"]["SC_ERR_CODE1"] != 0):
            output["status"] = "ERROR"
            output["sc_error1"] = payload["parsed_payload"]["SC_ERR_CODE1"]
            found_error = True
        elif payload["parsed_payload"]["SC_ERR_CODE2"] != 0:
            output["status"] = "ERROR"
            output["sc_error2"] = payload["parsed_payload"]["SC_ERR_CODE2"]
            found_error = True
        elif payload["parsed_payload"]["SC_ERR_CODE3"] != 0:
            output["status"] = "ERROR"
            output["sc_error3"] = payload["parsed_payload"]["SC_ERR_CODE3"]
            found_error = True

        elif (payload["parsed_payload"]["ERR1_CODE"] != 0):
            output["status"] = "ERROR"
            output["tlm_error1"] = payload["parsed_payload"]["ERR1_CODE"]
            found_error = True
        elif payload["parsed_payload"]["ERR2_CODE"] != 0:
            output["status"] = "ERROR"
            output["tlm_error2"] = payload["parsed_payload"]["ERR2_CODE"]
            found_error = True
        elif payload["parsed_payload"]["ERR3_CODE"] != 0:
            output["status"] = "ERROR"
            output["tlm_error3"] = payload["parsed_payload"]["ERR3_CODE"]
            found_error = True
        elif payload["parsed_payload"]["ERRHX1_CODE"] != 0:
            output["status"] = "ERROR"
            output["tlm_hx_error1"] = payload["parsed_payload"]["ERRHX1_CODE"]
            found_error = True
        elif payload["parsed_payload"]["ERRHX2_CODE"] != 0:
            output["status"] = "ERROR"
            output["tlm_hx_error2"] = payload["parsed_payload"]["ERRHX2_CODE"]
            found_error = True
        elif payload["parsed_payload"]["ERRHX3_CODE"] != 0:
            output["tlm_hx_error3"] = payload["parsed_payload"]["ERRHX3_CODE"]
            output["status"] = "ERROR"
            found_error = True
    else:
        if payload["parsed_payload"]["RFT_SC_ERR_CODE1"] != 0:
            output["status"] = "ERROR"
            output["sc_error1"] = payload["parsed_payload"]["RFT_SC_ERR_CODE1"]
            found_error = True
        elif payload["parsed_payload"]["RFT_SC_ERR_CODE2"] != 0:
            output["status"] = "ERROR"
            output["sc_error2"] = payload["parsed_payload"]["RFT_SC_ERR_CODE2"]
            found_error = True
        elif payload["parsed_payload"]["RFT_SC_ERR_CODE3"] != 0:
            output["status"] = "ERROR"
            output["sc_error3"] = payload["parsed_payload"]["RFT_SC_ERR_CODE3"]
            found_error = True

        elif payload["parsed_payload"]["RFT_ERR1_CODE"] != 0:
            output["status"] = "ERROR"
            output["tlm_error1"] = payload["parsed_payload"]["RFT_ERR1_CODE"]
            found_error = True
        elif payload["parsed_payload"]["RFT_ERR2_CODE"] != 0:
            output["status"] = "ERROR"
            output["tlm_error2"] = payload["parsed_payload"]["RFT_ERR2_CODE"]
            found_error = True
        elif payload["parsed_payload"]["RFT_ERR3_CODE"] != 0:
            output["status"] = "ERROR"
            output["tlm_error3"] = payload["parsed_payload"]["RFT_ERR3_CODE"]
            found_error = True
        elif payload["parsed_payload"]["RFT_ERRHX1_CODE"] != 0:
            output["status"] = "ERROR"
            output["tlm_hx_error1"] = payload["parsed_payload"]["RFT_ERRHX1_CODE"]
            found_error = True
        elif payload["parsed_payload"]["RFT_ERRHX2_CODE"] != 0:
            output["status"] = "ERROR"
            output["tlm_hx_error2"] = payload["parsed_payload"]["RFT_ERRHX2_CODE"]
            found_error = True
        elif payload["parsed_payload"]["RFT_ERRHX3_CODE"] != 0:
            output["tlm_hx_error3"] = payload["parsed_payload"]["RFT_ERRHX3_CODE"]
            output["status"] = "ERROR"
            found_error = True

    return found_error

def search_for_error(payload, error_number, tx_mask = MaxConst.PROP_CONTROLLER_ADDRESS, position = 0):
    found_error = False
    if tx_mask == MaxConst.PROP_CONTROLLER_ADDRESS:
        if payload["parsed_payload"]["SC_ERR_CODE1"] == error_number and (_check_if_equal(position, 1)):
            found_error = True
        elif payload["parsed_payload"]["SC_ERR_CODE2"] == error_number and (_check_if_equal(position, 2)):
            found_error = True
        elif payload["parsed_payload"]["SC_ERR_CODE3"] == error_number and (_check_if_equal(position, 3)):
            found_error = True
        elif payload["parsed_payload"]["ERR1_CODE"] == error_number and (_check_if_equal(position, 1)):
            found_error = True
        elif payload["parsed_payload"]["ERR2_CODE"] == error_number and (_check_if_equal(position, 2)):
            found_error = True
        elif payload["parsed_payload"]["ERR3_CODE"] == error_number and (_check_if_equal(position, 3)):
            found_error = True
        elif payload["parsed_payload"]["ERRHX1_CODE"] == error_number and (_check_if_equal(position, 4)):
            found_error = True
        elif payload["parsed_payload"]["ERRHX2_CODE"] == error_number and (_check_if_equal(position, 5)):
            found_error = True
        elif payload["parsed_payload"]["ERRHX3_CODE"] == error_number and (_check_if_equal(position, 6)):
            found_error = True
    else:
        if payload["parsed_payload"]["RFT_SC_ERR_CODE1"] == error_number and (_check_if_equal(position, 1)):
            found_error = True
        elif payload["parsed_payload"]["RFT_SC_ERR_CODE2"] == error_number and (_check_if_equal(position, 2)):
            found_error = True
        elif payload["parsed_payload"]["RFT_SC_ERR_CODE3"] == error_number and (_check_if_equal(position, 3)):
            found_error = True

        elif payload["parsed_payload"]["RFT_ERR1_CODE"] == error_number and (_check_if_equal(position, 1)):
            found_error = True
        elif payload["parsed_payload"]["RFT_ERR2_CODE"] == error_number and (_check_if_equal(position, 2)):
            found_error = True
        elif payload["parsed_payload"]["RFT_ERR3_CODE"] == error_number and (_check_if_equal(position, 3)):
            found_error = True
        elif payload["parsed_payload"]["RFT_ERRHX1_CODE"] == error_number and (_check_if_equal(position, 4)):
            found_error = True
        elif payload["parsed_payload"]["RFT_ERRHX2_CODE"] == error_number and (_check_if_equal(position, 5)):
            found_error = True
        elif payload["parsed_payload"]["RFT_ERRHX3_CODE"] == error_number and (_check_if_equal(position, 6)):
            found_error = True

    return found_error

def search_for_spacecraft_error(payload, error_number, tx_mask = MaxConst.PROP_CONTROLLER_ADDRESS, position = 0):
    found_error = False
    if tx_mask == MaxConst.PROP_CONTROLLER_ADDRESS:
        if (payload["parsed_payload"]["SC_ERR_CODE1"] == error_number) and (_check_if_equal(position, 1)):
            found_error = True
        elif (payload["parsed_payload"]["SC_ERR_CODE2"] == error_number) and (_check_if_equal(position, 2)):
            found_error = True
        elif (payload["parsed_payload"]["SC_ERR_CODE3"] == error_number) and (_check_if_equal(position, 3)):
            found_error = True

    else:
        if payload["parsed_payload"]["RFT_SC_ERR_CODE1"] == error_number and (_check_if_equal(position, 1)):
            found_error = True
        elif payload["parsed_payload"]["RFT_SC_ERR_CODE2"] == error_number and (_check_if_equal(position, 2)):
            found_error = True
        elif payload["parsed_payload"]["RFT_SC_ERR_CODE3"] == error_number and (_check_if_equal(position, 3)):
            found_error = True
    return found_error



def search_for_error_range(payload, err_num_low, err_num_high, tx_mask = MaxConst.PROP_CONTROLLER_ADDRESS, position = 0):
    tlm = payload["parsed_payload"]
    found_error = False
    if tx_mask == MaxConst.PROP_CONTROLLER_ADDRESS:
        if _check_error_range(tlm["SC_ERR_CODE1"], err_num_low, err_num_high) and (_check_if_equal(position, 1)):
            found_error = True
        elif _check_error_range(tlm["SC_ERR_CODE2"], err_num_low, err_num_high) and (_check_if_equal(position, 2)):
            found_error = True
        elif _check_error_range(tlm["SC_ERR_CODE3"], err_num_low, err_num_high) and (_check_if_equal(position, 3)):
            found_error = True
        elif _check_error_range(tlm["ERR1_CODE"], err_num_low, err_num_high) and (_check_if_equal(position, 1)):
            found_error = True
        elif _check_error_range(tlm["ERR2_CODE"], err_num_low, err_num_high) and (_check_if_equal(position, 2)):
            found_error = True
        elif _check_error_range(tlm["ERR3_CODE"], err_num_low, err_num_high) and (_check_if_equal(position, 3)):
            found_error = True
        elif _check_error_range(tlm["ERRHX1_CODE"], err_num_low, err_num_high) and (_check_if_equal(position, 4)):
            found_error = True
        elif _check_error_range(tlm["ERRHX2_CODE"], err_num_low, err_num_high) and (_check_if_equal(position, 5)):
            found_error = True
        elif _check_error_range(tlm["ERRHX3_CODE"], err_num_low, err_num_high) and (_check_if_equal(position, 6)):
            found_error = True
    else:
        if _check_error_range(tlm["RFT_SC_ERR_CODE1"], err_num_low, err_num_high) and (_check_if_equal(position, 1)):
            found_error = True
        elif _check_error_range(tlm["RFT_SC_ERR_CODE2"], err_num_low, err_num_high) and (_check_if_equal(position, 2)):
            found_error = True
        elif  _check_error_range(tlm["RFT_SC_ERR_CODE3"], err_num_low, err_num_high) and (_check_if_equal(position, 3)):
            found_error = True

        elif _check_error_range(tlm["RFT_ERR1_CODE"], err_num_low, err_num_high) and (_check_if_equal(position, 1)):
            found_error = True
        elif  _check_error_range(tlm["RFT_ERR2_CODE"], err_num_low, err_num_high) and (_check_if_equal(position, 2)):
            found_error = True
        elif  _check_error_range(tlm["RFT_ERR3_CODE"], err_num_low, err_num_high) and (_check_if_equal(position, 3)):
            found_error = True
        elif  _check_error_range(tlm["RFT_ERRHX1_CODE"], err_num_low, err_num_high) and (_check_if_equal(position, 4)):
            found_error = True
        elif _check_error_range(tlm["RFT_ERRHX2_CODE"], err_num_low, err_num_high) and (_check_if_equal(position, 5)):
            found_error = True
        elif _check_error_range(tlm["RFT_ERRHX3_CODE"], err_num_low, err_num_high) and (_check_if_equal(position, 6)):
            found_error = True
    return found_error



def search_for_spacecraft_error_range(payload, err_num_low, err_num_high, tx_mask = MaxConst.PROP_CONTROLLER_ADDRESS, position = 0):
    tlm = payload["parsed_payload"]
    found_error = False
    if tx_mask == MaxConst.PROP_CONTROLLER_ADDRESS:
        if _check_error_range(tlm["SC_ERR_CODE1"], err_num_low, err_num_high) and (_check_if_equal(position, 1)):
            found_error = True
        elif _check_error_range(tlm["SC_ERR_CODE2"], err_num_low, err_num_high) and (_check_if_equal(position, 2)):
            found_error = True
        elif _check_error_range(tlm["SC_ERR_CODE3"], err_num_low, err_num_high) and (_check_if_equal(position, 3)):
            found_error = True
    else:
        if _check_error_range(tlm["RFT_SC_ERR_CODE1"], err_num_low, err_num_high) and (_check_if_equal(position, 1)):
            found_error = True
        elif _check_error_range(tlm["RFT_SC_ERR_CODE2"], err_num_low, err_num_high) and (_check_if_equal(position, 2)):
            found_error = True
        elif  _check_error_range(tlm["RFT_SC_ERR_CODE3"], err_num_low, err_num_high) and (_check_if_equal(position, 3)):
            found_error = True
    return found_error



# This will search for only the first error and then verify there are no other errors and acks.
def search_for_error_only(payload, error_number, tx_mask = MaxConst.PROP_CONTROLLER_ADDRESS):
    if not check_ack(payload):
        return False
    found_error = False
    if tx_mask == MaxConst.PROP_CONTROLLER_ADDRESS:
        if payload["parsed_payload"]["SC_ERR_CODE1"] == error_number:
            found_error = True
            if payload["parsed_payload"]["SC_ERR_CODE2"] != 0:
                found_error = False
            elif payload["parsed_payload"]["SC_ERR_CODE3"] != 0:
                found_error = False
            elif payload["parsed_payload"]["ERR1_CODE"] != error_number:
                found_error = False
            elif payload["parsed_payload"]["ERR2_CODE"] != 0:
                found_error = False
            elif payload["parsed_payload"]["ERR3_CODE"] != 0:
                found_error = False
            elif payload["parsed_payload"]["ERRHX1_CODE"] != 0:
                found_error = False
            elif payload["parsed_payload"]["ERRHX2_CODE"] != 0:
                found_error = False
            elif payload["parsed_payload"]["ERRHX3_CODE"] != 0:
                found_error = False
    else:
        if payload["parsed_payload"]["RFT_SC_ERR_CODE1"] == error_number:
            found_error = True
            if payload["parsed_payload"]["RFT_SC_ERR_CODE2"] != 0:
                found_error = False
            elif payload["parsed_payload"]["RFT_SC_ERR_CODE3"] != 0:
                found_error = False
            elif payload["parsed_payload"]["RFT_ERR1_CODE"] != error_number:
                found_error = False
            elif payload["parsed_payload"]["RFT_ERR2_CODE"] != 0:
                found_error = False
            elif payload["parsed_payload"]["RFT_ERR3_CODE"] != 0:
                found_error = False
            elif payload["parsed_payload"]["RFT_ERRHX1_CODE"] != 0:
                found_error = False
            elif payload["parsed_payload"]["RFT_ERRHX2_CODE"] != 0:
                found_error = False
            elif payload["parsed_payload"]["RFT_ERRHX3_CODE"] != 0:
                found_error = False

    return found_error

def is_spacecraft_error_tlm_full(payload, tx_mask = MaxConst.PROP_CONTROLLER_ADDRESS):
    if tx_mask == MaxConst.PROP_CONTROLLER_ADDRESS:
        if (payload["parsed_payload"]["SC_ERR_CODE1"] == 0):
            return False
        elif (payload["parsed_payload"]["SC_ERR_CODE2"] == 0):
            return False
        elif (payload["parsed_payload"]["SC_ERR_CODE3"] == 0):
            return False

    else:
        if payload["parsed_payload"]["RFT_SC_ERR_CODE1"] == 0:
            return False
        elif payload["parsed_payload"]["RFT_SC_ERR_CODE2"] == 0:
            return False
        elif payload["parsed_payload"]["RFT_SC_ERR_CODE3"] == 0:
            return False
    return True

def get_maxwell_state_string(payload, hardware):
    state_string = ""
    if hardware == MaxConst.PROP_CONTROLLER_ADDRESS:
        state_number = payload["parsed_payload"]["STATE"]
    else:
        state_number = payload["parsed_payload"]["RFT_STATE"]
    if state_number == 1:
        state_string = "LOW_PWR"
    elif state_number == 2:
        state_string = "IDLE"
    elif state_number == 4:
        state_string = "THRUST"
    elif state_number == 7:
        state_string = "MANUAL"
    elif state_number == 8:
        state_string = "SW_LOADING"
    elif state_number == 11:
        state_string = "VENT_SAFE"
    elif state_number == 13:
        state_string = "WILD_WEST"
    elif state_number == 15:
        state_string = "SAFE"
    else:
        print("Invalid State selection. Input the correct state argument to get a valid result.")
        return state_string
    return state_string

def check_maxwell_state(payload,state):
    if state == "LOW_PWR":
        current_state = 1
    elif state == "IDLE":
        current_state = 2
    elif state == "THRUST":
        current_state = 4
    elif state == "MANUAL":
        current_state = 7
    elif state == "SW_LOADING":
        current_state = 8
    elif state == "VENT_SAFE":
        current_state = 11
    elif state == "WILD_WEST":
        current_state = 13
    elif state == "SAFE":
        current_state = 15
    else:
        print("Invalid State selection. Input the correct state argument to get a valid result.")
        return False
    if payload["status"] == "STATUS_SUCCESS":
        if payload["parsed_payload"]["STATE"] == current_state:
            return True
        else:
            return False
    else:
        return False

def check_ack(output):
    if output["status"] == "STATUS_SUCCESS" and output["parsed_payload"]["ACK_STATUS"] == 0:
        return True
    else:
        return False

def confirm_thrust_settings(output,rf_volt,sscm,time):
    if confirm_command_status(output):
        thrust_confirmed_flag = True
        if output["parsed_payload"]["THRUST_RF_VSET_FB"] != float(rf_volt):
            thrust_confirmed_flag = False
        if output["parsed_payload"]["THRUST_MDOT_FB"] != float(sscm):
            thrust_confirmed_flag = False
        if output["parsed_payload"]["THRUST_DUR_SEC_FB"] != float(time):
            thrust_confirmed_flag = False
        return thrust_confirmed_flag
    else:
        return False

def check_maxwell_is_safe_and_sound(output):
    if confirm_command_tlm(output) and check_maxwell_state(output,"LOW_PWR"):
        return True
    return False

# Check to see if the two values are equal. These values are positions for a error logging
def _check_if_equal (arg_pos,actual_pos):
    if arg_pos != 0:
        if arg_pos == actual_pos:
            found_error = True
        else:
            found_error = False
    else:
        found_error = True
    return found_error

# Check to see if a error (a int) is in range of to other error numbers (both ints)
def _check_error_range(current_error: int, error_low: int, error_high: int):
    error_status = True
    if current_error < error_low or current_error > error_high:
        error_status = False
    return error_status
