#!/usr/bin/bash

cd M:/maxwell_api/maxwell_api_package # Going to the proper directory


# Removing all files from previous build
rm -r dist
rm -r build
rm -r maxwell_api.egg-info
M:/tools/git/bin/git.exe describe --match "max_api*" > "maxapi\maxapi_version.txt"
# run build step
python setup.py sdist bdist_wheel


