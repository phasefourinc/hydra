# First Party Imports
import random

import maxapi.mr_maxwell_b2 as mb2
from maxapi.max_cmd_id import MaxCmd


# Third Party Imports
from rich import print
import time
import sys


# Constants
mr_max = mb2.MrMaxwell(com_port="COM3",com_baud=1000000, db_datasource="sandbox" , db_measurement="andrew_pcu_dev")
mr_max.set_auto_tlm_enable(1000)
#mr_max.goto_manual()

# This class is to get options that are used to send commands
class CmdOption():
    option_num = 0
    def __init__(self, option_string, func_to_run):
        self.option_string = option_string
        self.func_to_run= func_to_run
        self.num_option = CmdOption.option_num
        CmdOption.option_num +=1


def main():
    print("Mr Maxwell Is in Manual Mode")
    # Generate the options we have here.
    option_list = [
        CmdOption("Clear Maxwell Errors",  mr_max.clear_errors),
        CmdOption("Send A Error to Maxwell", send_maxwell_errors),
        CmdOption("Command Maxwell To The Manual State", mr_max.goto_manual),
        CmdOption("Safe Maxwell", mr_max.goto_safe),
        CmdOption("Quit", exit_program)
    ]

    while True:

        try:
            get_input(option_list)
            print("Command Processed.... ")
        except KeyboardInterrupt:
            print("Quiting")

def get_input(option_list):

    print("Pick A Option from the following below")
    print("--------------------------------------------")
    for option in option_list:
        print(f"{option.num_option}) - {option.option_string}")
    input_val = input("Input: ")
    option_list[int(input_val)].func_to_run()





# Custom Functions to use for functionality.

def send_maxwell_errors():
    print("Writing Error")
    rand_err = random.randint(0,30)
    mr_max.send_maxwell_message(MaxCmd.WRT_ERR, [0x00, rand_err, 0xFF])

def exit_program():
    print("Quiting")
    sys.exit()





if __name__ == "__main__":
    main()