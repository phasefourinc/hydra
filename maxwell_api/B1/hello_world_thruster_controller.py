import time
from maxapi.mr_maxwell import MrMaxwell

DEFAULT_BAUD_RATE =  "1000000"
DEFAULT_COM_PORT = "COM4"
DEFAULT_DATASOURCE= "sandbox"
HOST="nekone.phasefour.co"
DEFAULT_MEASUREMENT = "andrew_pcu_dev"
DEFAULT_HARDWARE = "Thruster Controller"
# These commands send no data
SAFE       = 0x000F
ABORT      = 0x000F
LOW_PWR    = 0x0001
IDLE       = 0x0002
THRUST     = 0x0004
MANUAL     = 0x0007
SW_UPDATE  = 0x0008
WILD_WEST  = 0x000D
CLR_ERRORS = 0x0011
GET_TLM    = 0x0020

# These commands send 1-byte boolean data
SET_WDI_EN          = 0x0100
SET_RFT_RST_EN      = 0x0101
SET_HEATER_EN       = 0x0102
SET_HEATER_RST_EN   = 0x0103
SET_HPISO_EN        = 0x0104
SET_LPISO_EN        = 0x0105
SET_PFCV_DAC_RST_EN = 0x0106
SET_DEBUG_LEDS      = 0x0107    # data bits 3:0 set DEBUG_LED3:0 respectively

# These commands send 2-byte u16 data
SET_HEATER_PWM_RAW  = 0x0108
SET_HPISO_PWM_RAW   = 0x0109
SET_LPISO_PWM_RAW   = 0x010A
SET_PFCV_DAC_RAW    = 0x010B


mr_max = MrMaxwell(DEFAULT_COM_PORT,DEFAULT_BAUD_RATE,DEFAULT_DATASOURCE,DEFAULT_MEASUREMENT, hardware="Block2")
mr_max.update_default_interfacting_hardware(DEFAULT_HARDWARE)


def main():
    if not mr_max.is_thruster_controller_connected():
        print("The Thruster Controller is not connected. Please connect the Thruster Controller to run this test.")
        return
    mr_max.set_auto_tlm_enable(1000)
    led_light = 0
    while 1:

        mr_max.rft_set_debug_led0(led_light)
        print("Running...")
        time.sleep(3)
        led_light= led_light + 1
        if led_light > 2:
            led_light = 0
            print("LED OFF")
        else:
            print("LED ON")



if __name__== "__main__":
    main()
