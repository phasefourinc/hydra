import time
from maxapi.mr_maxwell import MrMaxwell

DEFAULT_BAUD_RATE =  "1000000"
DEFAULT_COM_PORT = "INSERT_COM_HERE"
DEFAULT_DATASOURCE= "sandbox"
HOST="nekone.phasefour.co"
DEFAULT_MEASUREMENT = "andrew_pcu_dev"
DEFAULT_HARDWARE = "Prop Controller"


mr_max = MrMaxwell(DEFAULT_COM_PORT,DEFAULT_BAUD_RATE,DEFAULT_DATASOURCE,DEFAULT_MEASUREMENT)

# NOTE technically this is redundant however it might be good programming to make it explict what hardware this script is talking to.
mr_max.update_default_interfacting_hardware(DEFAULT_HARDWARE)


def main():
    if not mr_max.is_prop_controller_connected():
        print("The Prop Controller is not connected. Please connect the Prop Controller to run this test.")
        return
    mr_max.set_auto_tlm_enable(1000)
    while 1:
        output = mr_max.get_tlm()
        mr_max.led_test()
        print("Running...")
        time.sleep(3)



if __name__== "__main__":
    main()
