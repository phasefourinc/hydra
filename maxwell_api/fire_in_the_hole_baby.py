import time
import datetime
from maxapi.mr_maxwell import MrMaxwell, MaxConst

DEFAULT_BAUD_RATE =  "115200"
DEFAULT_COM_PORT = "COM4"
DEFAULT_DATASOURCE= "sandbox"
HOST="nekone.phasefour.co"
DEFAULT_MEASUREMENT = "andrew_pcu_dev"
DEFAULT_HARDWARE = "Prop Controller"
# These commands send no data
SAFE       = 0x000F
ABORT      = 0x000F
LOW_PWR    = 0x0001
IDLE       = 0x0002
THRUST     = 0x0004
MANUAL     = 0x0007
SW_UPDATE  = 0x0008
WILD_WEST  = 0x000D
CLR_ERRORS = 0x0011
GET_TLM    = 0x0020

# These commands send 1-byte boolean data
SET_WDI_EN          = 0x0100
SET_RFT_RST_EN      = 0x0101
SET_HEATER_EN       = 0x0102
SET_HEATER_RST_EN   = 0x0103
SET_HPISO_EN        = 0x0104
SET_LPISO_EN        = 0x0105
SET_PFCV_DAC_RST_EN = 0x0106
SET_DEBUG_LEDS      = 0x0107    # data bits 3:0 set DEBUG_LED3:0 respectively

# These commands send 2-byte u16 data
SET_HEATER_PWM_RAW  = 0x0108
SET_HPISO_PWM_RAW   = 0x0109
SET_LPISO_PWM_RAW   = 0x010A
SET_PFCV_DAC_RAW    = 0x010B


mr_max = MrMaxwell(DEFAULT_COM_PORT,DEFAULT_BAUD_RATE,DEFAULT_DATASOURCE,DEFAULT_MEASUREMENT)

# NOTE technically this is redundant however it might be good programming to make it explict what hardware this script is talking to.
mr_max.update_default_interfacting_hardware(DEFAULT_HARDWARE)
import maxwell_telm_checker as max_check

def check_if_idle_is_complete():
    start_time = time.time()
    while True:
        status_pkt = mr_max.get_status()
        busy_flag = status_pkt['parsed_payload']['BUSY_FLAG']
        if busy_flag == 0:
            print('System is no longer in busy state, Heater operations finished')
            ttank = status_pkt['parsed_payload']['TTANK']
            print('TTANK is reading', ttank, 'Celsius')
            break
        elif time.time() - start_time > 60:
            print('Still Heating...')
            start_time = time.time()
            ttank = status_pkt['parsed_payload']['TTANK']
            print('TTANK is reading', ttank, 'Celsius')
        # If it takes more the 20 mins something is wrong and we should figure out why. (THIS IS FOR ATMOSPHERE) TODO UPDATE TO A BETTER NUMBER IF NEED BE.
        elif time.time() - start_time > (60 * 20):
            ttank = status_pkt['parsed_payload']['TTANK']
            print("Test Failed: Took to long to heat.")
            print("Current temp is: " + str(ttank))
            return False
    return True

def wait_until_dumping_is_done(tlm_dict,output_file = None):
    count = 0

    if max_check.confirm_command_status(tlm_dict):
        while mr_max.dump_tlm_ready() == False:
            count = count +1
            time.sleep(1)
            print(count)
        tlm_dump = mr_max.get_mcu_dump_tlm()
        if output_file is not None:
            with open(str(output_file) + ".txt", mode='w', newline="") as dump:
                for x in tlm_dump:
                    dump.write(str(x["parsed_payload"]))
                    dump.write("\r\n")
        current_time = datetime.datetime.now()
        print("DONE! Time to take to dump is: ", str(current_time))
    else:
        print("FAIL. Could not dump dump command failed to respond.")


def main():

    print("FIRE IN THE HOLE TEST")
    rf_voltage = 30
    sscm = 20
    time_to_fire = 1
    if not mr_max.is_prop_controller_connected():
        print("The Prop Controller is not connected. Please connect the Prop Controller to run this test.")
        return
    print("Enabling Auto Tlm")

    mr_max.set_auto_tlm_enable(500)
    print("Dumping FLASH")
    tlm_dict = mr_max.dump_tlm_nand_full()
    wait_until_dumping_is_done(tlm_dict, output_file="flash_dump")
    print("Done!")
    print("Gather Telem")
    mr_max.goto_safe()
    mr_max.clear_errors()
    mr_max.goto_idle()
    print("Started Idle")
    if not check_if_idle_is_complete():
        print("Could not go to idle safely.")
        return False
    prop_tlm = mr_max.get_tlm(MaxConst.PROP_CONTROLLER_ADDRESS)
    thrust_tlm = mr_max.get_tlm(MaxConst.THRUSTER_CONTROLLER_ADDRESS)
    if not max_check.confirm_command_tlm(prop_tlm) or not max_check.confirm_command_tlm(thrust_tlm, tx_mask=MaxConst.THRUSTER_CONTROLLER_ADDRESS):
        print("There are errors with with the maxwell unit.")
        print("The Prop Telemetry is " + str(prop_tlm["parsed_payload"]))
        print("The Thrust Telemetry is " + str(thrust_tlm["parsed_payload"]))
        return False


    status = mr_max.config_thrust(rf_voltage,sscm,time_to_fire)
    if not max_check.confirm_command_status(status):
        return False
    prop_tlm = mr_max.get_tlm(MaxConst.PROP_CONTROLLER_ADDRESS)
    if not max_check.confirm_thrust_settings(prop_tlm,rf_voltage,sscm,time_to_fire):
        return False

    prop_tlm = mr_max.get_tlm(MaxConst.PROP_CONTROLLER_ADDRESS)
    thrust_tlm = mr_max.get_tlm(MaxConst.THRUSTER_CONTROLLER_ADDRESS)
    if not max_check.confirm_command_tlm(prop_tlm) or not max_check.confirm_command_tlm(thrust_tlm, tx_mask=MaxConst.THRUSTER_CONTROLLER_ADDRESS):
        print("There are errors with with the maxwell unit.")
        print("The Prop Telemetry is " + str(prop_tlm["parsed_payload"]))
        print("The Thrust Telemetry is " + str(thrust_tlm["parsed_payload"]))
        return False
    print("The thrust Parameters you input are RF_Voltage: " +str(rf_voltage) + "  sscm_flow: " + str(sscm)+ " time to thrust " + str(time_to_fire))
    input_val = input("Are you sure? This is your last chance to exit out? (Press q to quit)")
    if input_val == "q" or input_val == "Q":
        print("Well you quit. How do you feel?")
        return False

    print("Ok you asked for it. Bye Bye")
    mr_max.goto_thrust()
    time.sleep(6)
    mr_max.goto_safe()

    print("yeah hopefully things did not break.")
    time.sleep(5.0)
    input ("Finish Code?")








if __name__== "__main__":
    main()
