/******************************************************************************
*
*   Copyright 2013-2018 NXP
*
*   This software is owned or controlled by NXP and may only be used strictly in accordance with the
*   applicable license terms.  By expressly accepting such terms or by downloading, installing,
*   activating and/or otherwise using the software, you are agreeing that you have read, and that
*   you agree to comply with and are bound by, such license terms.  If you do not agree to be bound
*   by the applicable license terms, then you may not retain, install, activate or otherwise use the
*   software.
*
******************************************************************************/
/**
*
* @file     gmclib.h
*
* @version  1.0.8.0
* 
* @date     Jan-31-2017
* 
* @brief    Master header file.
*
******************************************************************************/
#ifndef GMCLIB_H
#define GMCLIB_H

/******************************************************************************
* Includes
******************************************************************************/
#include "gflib.h"
#include "GMCLIB_Clark.h"
#include "GMCLIB_ClarkInv.h"
#include "GMCLIB_Park.h"
#include "GMCLIB_ParkInv.h"
#include "GMCLIB_SvmStd.h"
#include "GMCLIB_ElimDcBusRip.h"
#include "GMCLIB_DecouplingPMSM.h"

#endif /* GMCLIB_H */
