/******************************************************************************
*
*   Copyright 2016-2018 NXP
*
*   This software is owned or controlled by NXP and may only be used strictly in accordance with the
*   applicable license terms.  By expressly accepting such terms or by downloading, installing,
*   activating and/or otherwise using the software, you are agreeing that you have read, and that
*   you agree to comply with and are bound by, such license terms.  If you do not agree to be bound
*   by the applicable license terms, then you may not retain, install, activate or otherwise use the
*   software.
*
***************************************************************************//**
*
* @file     AMCLIB_CurrentLoop_w.c
*
* @version  1.0.6.0
*
* @date     Oct-26-2016
*
* @brief    Source file of wrapper function for AMCLIB_CurrentLoop function.
*
*******************************************************************************
*
* Function implemented as ANSIC ISO/IEC 9899:1990, C90. 
*
******************************************************************************/
/**
@if AMCLIB_GROUP
    @addtogroup AMCLIB_GROUP
@else
    @defgroup AMCLIB_GROUP AMCLIB
@endif
*/ 

#ifdef __cplusplus
extern "C" {
#endif

/******************************************************************************
* Includes
******************************************************************************/
#include "SWLIBS_Typedefs.h"
#include "AMCLIB_CurrentLoop.h"
#include "AMCLIB_CurrentLoop_w.h"

/******************************************************************************
* Function implementations      (scope: module-local)
******************************************************************************/

/******************************************************************************
* Function implementations      (scope: module-exported)
******************************************************************************/

/******************************************************************************
* Implementation variant: 32-bit fractional
******************************************************************************/
void AMCLIB_CurrentLoop_w_F32(tFrac32 f32UDcBus, \
                              tFrac32 f32IDReq, \
                              tFrac32 f32IQReq, \
                              tFrac32 f32IDFbck, \
                              tFrac32 f32IQFbck, \
                              tFrac32 f32AccD, \
                              tFrac32 f32InErrK1D, \
                              tFrac32 f32CC1scD, \
                              tFrac32 f32CC2scD, \
                              tU16    u16NShiftD, \
                              tFrac32 f32AccQ, \
                              tFrac32 f32InErrK1Q, \
                              tFrac32 f32CC1scQ, \
                              tFrac32 f32CC2scQ, \
                              tU16    u16NShiftQ, \
                              tFrac32 *pOutAccD, \
                              tFrac32 *pOutInErrK1D, \
                              tFrac32 *pOutAccQ, \
                              tFrac32 *pOutInErrK1Q, \
                              tFrac32 *pOutUDReq, \
                              tFrac32 *pOutUQReq)
{
    AMCLIB_CURRENT_LOOP_T_F32 Ctrl;
    SWLIBS_2Syst_F32 UDQReq;
    SWLIBS_2Syst_F32 IDQReqLocal;
    SWLIBS_2Syst_F32 IDQFbckLocal;

    IDQReqLocal.f32Arg1 = f32IDReq;
    IDQReqLocal.f32Arg2 = f32IQReq;
    IDQFbckLocal.f32Arg1 = f32IDFbck;
    IDQFbckLocal.f32Arg2 = f32IQFbck;
    
    Ctrl.pIDQReq = &IDQReqLocal;
    Ctrl.pIDQFbck = &IDQFbckLocal;
    
    Ctrl.pPIrAWD.f32CC1sc = f32CC1scD;
    Ctrl.pPIrAWD.f32CC2sc = f32CC2scD;
    Ctrl.pPIrAWD.f32Acc = f32AccD;
    Ctrl.pPIrAWD.f32InErrK1 = f32InErrK1D;
    Ctrl.pPIrAWD.u16NShift = u16NShiftD;
    
    Ctrl.pPIrAWQ.f32CC1sc = f32CC1scQ;
    Ctrl.pPIrAWQ.f32CC2sc = f32CC2scQ;
    Ctrl.pPIrAWQ.f32Acc = f32AccQ;
    Ctrl.pPIrAWQ.f32InErrK1 = f32InErrK1Q;
    Ctrl.pPIrAWQ.u16NShift = u16NShiftQ;
    
    AMCLIB_CurrentLoop_F32(f32UDcBus, &UDQReq, &Ctrl);
    
    *pOutAccD = Ctrl.pPIrAWD.f32Acc;
    *pOutInErrK1D = Ctrl.pPIrAWD.f32InErrK1;
    *pOutAccQ = Ctrl.pPIrAWQ.f32Acc;
    *pOutInErrK1Q = Ctrl.pPIrAWQ.f32InErrK1;
    *pOutUDReq = UDQReq.f32Arg1;
    *pOutUQReq = UDQReq.f32Arg2;

    return;
}




/******************************************************************************
* Implementation variant: 16-bit fractional
******************************************************************************/
void AMCLIB_CurrentLoop_w_F16(tFrac16 f16UDcBus, \
                              tFrac16 f16IDReq, \
                              tFrac16 f16IQReq, \
                              tFrac16 f16IDFbck, \
                              tFrac16 f16IQFbck, \
                              tFrac32 f32AccD, \
                              tFrac16 f16InErrK1D, \
                              tFrac16 f16CC1scD, \
                              tFrac16 f16CC2scD, \
                              tU16    u16NShiftD, \
                              tFrac32 f32AccQ, \
                              tFrac16 f16InErrK1Q, \
                              tFrac16 f16CC1scQ, \
                              tFrac16 f16CC2scQ, \
                              tU16    u16NShiftQ, \
                              tFrac32 *pOutAccD, \
                              tFrac16 *pOutInErrK1D, \
                              tFrac32 *pOutAccQ, \
                              tFrac16 *pOutInErrK1Q, \
                              tFrac16 *pOutUDReq, \
                              tFrac16 *pOutUQReq)
{
    AMCLIB_CURRENT_LOOP_T_F16 Ctrl;
    SWLIBS_2Syst_F16 UDQReq;
    SWLIBS_2Syst_F16 IDQReqLocal;
    SWLIBS_2Syst_F16 IDQFbckLocal;

    IDQReqLocal.f16Arg1 = f16IDReq;
    IDQReqLocal.f16Arg2 = f16IQReq;
    IDQFbckLocal.f16Arg1 = f16IDFbck;
    IDQFbckLocal.f16Arg2 = f16IQFbck;
    
    Ctrl.pIDQReq = &IDQReqLocal;
    Ctrl.pIDQFbck = &IDQFbckLocal;
    
    Ctrl.pPIrAWD.f16CC1sc = f16CC1scD;
    Ctrl.pPIrAWD.f16CC2sc = f16CC2scD;
    Ctrl.pPIrAWD.f32Acc = f32AccD;
    Ctrl.pPIrAWD.f16InErrK1 = f16InErrK1D;
    Ctrl.pPIrAWD.u16NShift = u16NShiftD;
    
    Ctrl.pPIrAWQ.f16CC1sc = f16CC1scQ;
    Ctrl.pPIrAWQ.f16CC2sc = f16CC2scQ;
    Ctrl.pPIrAWQ.f32Acc = f32AccQ;
    Ctrl.pPIrAWQ.f16InErrK1 = f16InErrK1Q;
    Ctrl.pPIrAWQ.u16NShift = u16NShiftQ;
    
    AMCLIB_CurrentLoop_F16(f16UDcBus, &UDQReq, &Ctrl);
    
    *pOutAccD = Ctrl.pPIrAWD.f32Acc;
    *pOutInErrK1D = Ctrl.pPIrAWD.f16InErrK1;
    *pOutAccQ = Ctrl.pPIrAWQ.f32Acc;
    *pOutInErrK1Q = Ctrl.pPIrAWQ.f16InErrK1;
    *pOutUDReq = UDQReq.f16Arg1;
    *pOutUQReq = UDQReq.f16Arg2;

    return;
}




/******************************************************************************
* Implementation variant: Single precision floating point
******************************************************************************/
void AMCLIB_CurrentLoop_w_FLT(tFloat fltUDcBus, \
                              tFloat fltIDReq, \
                              tFloat fltIQReq, \
                              tFloat fltIDFbck, \
                              tFloat fltIQFbck, \
                              tFloat fltAccD, \
                              tFloat fltInErrK1D, \
                              tFloat fltCC1scD, \
                              tFloat fltCC2scD, \
                              tFloat fltAccQ, \
                              tFloat fltInErrK1Q, \
                              tFloat fltCC1scQ, \
                              tFloat fltCC2scQ, \
                              tFloat *pOutAccD, \
                              tFloat *pOutInErrK1D, \
                              tFloat *pOutAccQ, \
                              tFloat *pOutInErrK1Q, \
                              tFloat *pOutUDReq, \
                              tFloat *pOutUQReq)
{
    AMCLIB_CURRENT_LOOP_T_FLT Ctrl;
    SWLIBS_2Syst_FLT UDQReq;
    SWLIBS_2Syst_FLT IDQReqLocal;
    SWLIBS_2Syst_FLT IDQFbckLocal;

    IDQReqLocal.fltArg1 = fltIDReq;
    IDQReqLocal.fltArg2 = fltIQReq;
    IDQFbckLocal.fltArg1 = fltIDFbck;
    IDQFbckLocal.fltArg2 = fltIQFbck;
    
    Ctrl.pIDQReq = &IDQReqLocal;
    Ctrl.pIDQFbck = &IDQFbckLocal;
    
    Ctrl.pPIrAWD.fltCC1sc = fltCC1scD;
    Ctrl.pPIrAWD.fltCC2sc = fltCC2scD;
    Ctrl.pPIrAWD.fltAcc = fltAccD;
    Ctrl.pPIrAWD.fltInErrK1 = fltInErrK1D;
    
    Ctrl.pPIrAWQ.fltCC1sc = fltCC1scQ;
    Ctrl.pPIrAWQ.fltCC2sc = fltCC2scQ;
    Ctrl.pPIrAWQ.fltAcc = fltAccQ;
    Ctrl.pPIrAWQ.fltInErrK1 = fltInErrK1Q;
    
    AMCLIB_CurrentLoop_FLT(fltUDcBus, &UDQReq, &Ctrl);
    
    *pOutAccD = Ctrl.pPIrAWD.fltAcc;
    *pOutInErrK1D = Ctrl.pPIrAWD.fltInErrK1;
    *pOutAccQ = Ctrl.pPIrAWQ.fltAcc;
    *pOutInErrK1Q = Ctrl.pPIrAWQ.fltInErrK1;
    *pOutUDReq = UDQReq.fltArg1;
    *pOutUQReq = UDQReq.fltArg2;

    return;
}

#ifdef __cplusplus
}
#endif
