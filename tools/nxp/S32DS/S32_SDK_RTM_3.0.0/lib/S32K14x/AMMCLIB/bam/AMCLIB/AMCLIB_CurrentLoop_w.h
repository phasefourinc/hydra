/******************************************************************************
*
*   Copyright 2016-2018 NXP
*
*   This software is owned or controlled by NXP and may only be used strictly in accordance with the
*   applicable license terms.  By expressly accepting such terms or by downloading, installing,
*   activating and/or otherwise using the software, you are agreeing that you have read, and that
*   you agree to comply with and are bound by, such license terms.  If you do not agree to be bound
*   by the applicable license terms, then you may not retain, install, activate or otherwise use the
*   software.
*
***************************************************************************//**
*
* @file     AMCLIB_CurrentLoop_w.h
*
* @version  1.0.4.0
*
* @date     Oct-26-2016
*
* @brief    Header file for #AMCLIB_CurrentLoop_w function.
*
******************************************************************************/
#ifndef _AMCLIB_CURRENTLOOP_W_H
#define _AMCLIB_CURRENTLOOP_W_H

#ifdef __cplusplus
extern "C" {
#endif

/******************************************************************************
* Defines and macros            (scope: module-local)
******************************************************************************/

/******************************************************************************
* Typedefs and structures       (scope: module-local)
******************************************************************************/

/******************************************************************************
* Implementation variant: 32-bit fractional
******************************************************************************/
/******************************************************************************
* Exported function prototypes
******************************************************************************/
extern void AMCLIB_CurrentLoop_w_F32(tFrac32 f32UDcBus, \
                              tFrac32 f32IDReq, \
                              tFrac32 f32IQReq, \
                              tFrac32 f32IDFbck, \
                              tFrac32 f32IQFbck, \
                              tFrac32 f32AccD, \
                              tFrac32 f32InErrK1D, \
                              tFrac32 f32CC1scD, \
                              tFrac32 f32CC2scD, \
                              tU16    u16NShiftD, \
                              tFrac32 f32AccQ, \
                              tFrac32 f32InErrK1Q, \
                              tFrac32 f32CC1scQ, \
                              tFrac32 f32CC2scQ, \
                              tU16    u16NShiftQ, \
                              tFrac32 *pOutAccD, \
                              tFrac32 *pOutInErrK1D, \
                              tFrac32 *pOutAccQ, \
                              tFrac32 *pOutInErrK1Q, \
                              tFrac32 *pOutUDReq, \
                              tFrac32 *pOutUQReq);




/******************************************************************************
* Implementation variant: 16-bit fractional
******************************************************************************/
/******************************************************************************
* Exported function prototypes
******************************************************************************/
extern void AMCLIB_CurrentLoop_w_F16(tFrac16 f16UDcBus, \
                              tFrac16 f16IDReq, \
                              tFrac16 f16IQReq, \
                              tFrac16 f16IDFbck, \
                              tFrac16 f16IQFbck, \
                              tFrac32 f32AccD, \
                              tFrac16 f16InErrK1D, \
                              tFrac16 f16CC1scD, \
                              tFrac16 f16CC2scD, \
                              tU16    u16NShiftD, \
                              tFrac32 f32AccQ, \
                              tFrac16 f16InErrK1Q, \
                              tFrac16 f16CC1scQ, \
                              tFrac16 f16CC2scQ, \
                              tU16    u16NShiftQ, \
                              tFrac32 *pOutAccD, \
                              tFrac16 *pOutInErrK1D, \
                              tFrac32 *pOutAccQ, \
                              tFrac16 *pOutInErrK1Q, \
                              tFrac16 *pOutUDReq, \
                              tFrac16 *pOutUQReq);




/******************************************************************************
* Implementation variant: Single precision floating point
******************************************************************************/
/******************************************************************************
* Exported function prototypes
******************************************************************************/
extern void AMCLIB_CurrentLoop_w_FLT(tFloat fltUDcBus, \
                              tFloat fltIDReq, \
                              tFloat fltIQReq, \
                              tFloat fltIDFbck, \
                              tFloat fltIQFbck, \
                              tFloat fltAccD, \
                              tFloat fltInErrK1D, \
                              tFloat fltCC1scD, \
                              tFloat fltCC2scD, \
                              tFloat fltAccQ, \
                              tFloat fltInErrK1Q, \
                              tFloat fltCC1scQ, \
                              tFloat fltCC2scQ, \
                              tFloat *pOutAccD, \
                              tFloat *pOutInErrK1D, \
                              tFloat *pOutAccQ, \
                              tFloat *pOutInErrK1Q, \
                              tFloat *pOutUDReq, \
                              tFloat *pOutUQReq);

/******************************************************************************
* Inline functions
******************************************************************************/

#ifdef __cplusplus
}
#endif

#endif /* _AMCLIB_CURRENTLOOP_W_H */
