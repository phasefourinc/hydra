/******************************************************************************
*
*   Copyright 2016-2018 NXP
*
*   This software is owned or controlled by NXP and may only be used strictly in accordance with the
*   applicable license terms.  By expressly accepting such terms or by downloading, installing,
*   activating and/or otherwise using the software, you are agreeing that you have read, and that
*   you agree to comply with and are bound by, such license terms.  If you do not agree to be bound
*   by the applicable license terms, then you may not retain, install, activate or otherwise use the
*   software.
*
***************************************************************************//**
*
* @file     AMCLIB_SpeedLoop_w.c
*
* @version  1.0.6.0
*
* @date     Oct-26-2016
*
* @brief    Source file of wrapper function for AMCLIB_SpeedLoop function.
*
*******************************************************************************
*
* Function implemented as ANSIC ISO/IEC 9899:1990, C90. 
*
******************************************************************************/
/**
@if AMCLIB_GROUP
    @addtogroup AMCLIB_GROUP
@else
    @defgroup AMCLIB_GROUP AMCLIB
@endif
*/ 

#ifdef __cplusplus
extern "C" {
#endif

/******************************************************************************
* Includes
******************************************************************************/
#include "SWLIBS_Typedefs.h"
#include "AMCLIB_SpeedLoop.h"
#include "AMCLIB_SpeedLoop_w.h"

/******************************************************************************
* Function implementations      (scope: module-local)
******************************************************************************/

/******************************************************************************
* Function implementations      (scope: module-exported)
******************************************************************************/

/******************************************************************************
* Implementation variant: 32-bit fractional
******************************************************************************/
void AMCLIB_SpeedLoop_w_F32(tFrac32 f32VelocityReq, \
                            tFrac32 f32VelocityFbck, \
                            tFrac32 f32AccW, \
                            tU16 u16NSamplesW, \
                            tFrac32 f32UpperLimitQ, \
                            tFrac32 f32LowerLimitQ, \
                            tFrac32 f32InK_1Q, \
                            tFrac32 f32IntegPartK_1Q, \
                            tFrac32 f32PropGainQ, \
                            tS16 s16PropGainShiftQ, \
                            tFrac32 f32IntegGainQ, \
                            tS16 s16IntegGainShiftQ, \
                            tFrac32 f32RampState, \
                            tFrac32 f32RampUp, \
                            tFrac32 f32RampDown, \
                            tFrac32 *pOutRampState, \
                            tFrac32 *pOutIntegPartK_1Q, \
                            tFrac32 *pOutInK_1Q, \
                            tU16 *pOutLimitFlagQ, \
                            tFrac32 *pOutAccW, \
                            tFrac32 *pOutIQReq)
{
    AMCLIB_SPEED_LOOP_T_F32 Ctrl;
    SWLIBS_2Syst_F32 IDQReq;

    Ctrl.pFilterW.f32Acc = f32AccW;
    Ctrl.pFilterW.u16NSamples = u16NSamplesW;

    Ctrl.pPIpAWQ.f32PropGain = f32PropGainQ;
    Ctrl.pPIpAWQ.f32IntegGain = f32IntegGainQ;
    Ctrl.pPIpAWQ.s16PropGainShift = s16PropGainShiftQ;
    Ctrl.pPIpAWQ.s16IntegGainShift = s16IntegGainShiftQ;
    Ctrl.pPIpAWQ.f32LowerLimit = f32LowerLimitQ;
    Ctrl.pPIpAWQ.f32UpperLimit = f32UpperLimitQ;
    Ctrl.pPIpAWQ.f32IntegPartK_1 = f32IntegPartK_1Q;
    Ctrl.pPIpAWQ.f32InK_1 = f32InK_1Q;

    Ctrl.pRamp.f32State = f32RampState;
    Ctrl.pRamp.f32RampUp = f32RampUp;
    Ctrl.pRamp.f32RampDown = f32RampDown;
    
    AMCLIB_SpeedLoop_F32(f32VelocityReq, f32VelocityFbck, &IDQReq, &Ctrl);
    
    *pOutRampState = Ctrl.pRamp.f32State;
    *pOutIntegPartK_1Q = Ctrl.pPIpAWQ.f32IntegPartK_1;
    *pOutInK_1Q = Ctrl.pPIpAWQ.f32InK_1;
    *pOutLimitFlagQ = Ctrl.pPIpAWQ.u16LimitFlag;
    *pOutAccW = Ctrl.pFilterW.f32Acc;
    *pOutIQReq = IDQReq.f32Arg2;

    return;
}




/******************************************************************************
* Implementation variant: 16-bit fractional
******************************************************************************/
void AMCLIB_SpeedLoop_w_F16(tFrac16 f16VelocityReq, \
                            tFrac16 f16VelocityFbck, \
                            tFrac32 f32AccW, \
                            tU16 u16NSamplesW, \
                            tFrac16 f16UpperLimitQ, \
                            tFrac16 f16LowerLimitQ, \
                            tFrac16 f16InK_1Q, \
                            tFrac32 f32IntegPartK_1Q, \
                            tFrac16 f16PropGainQ, \
                            tS16 s16PropGainShiftQ, \
                            tFrac16 f16IntegGainQ, \
                            tS16 s16IntegGainShiftQ, \
                            tFrac32 f32RampState, \
                            tFrac32 f32RampUp, \
                            tFrac32 f32RampDown, \
                            tFrac32 *pOutRampState, \
                            tFrac32 *pOutIntegPartK_1Q, \
                            tFrac16 *pOutInK_1Q, \
                            tU16 *pOutLimitFlagQ, \
                            tFrac32 *pOutAccW, \
                            tFrac16 *pOutIQReq)
{
    AMCLIB_SPEED_LOOP_T_F16 Ctrl;
    SWLIBS_2Syst_F16 IDQReq;

    Ctrl.pFilterW.f32Acc = f32AccW;
    Ctrl.pFilterW.u16NSamples = u16NSamplesW;

    Ctrl.pPIpAWQ.f16PropGain = f16PropGainQ;
    Ctrl.pPIpAWQ.f16IntegGain = f16IntegGainQ;
    Ctrl.pPIpAWQ.s16PropGainShift = s16PropGainShiftQ;
    Ctrl.pPIpAWQ.s16IntegGainShift = s16IntegGainShiftQ;
    Ctrl.pPIpAWQ.f16LowerLimit = f16LowerLimitQ;
    Ctrl.pPIpAWQ.f16UpperLimit = f16UpperLimitQ;
    Ctrl.pPIpAWQ.f32IntegPartK_1 = f32IntegPartK_1Q;
    Ctrl.pPIpAWQ.f16InK_1 = f16InK_1Q;

    Ctrl.pRamp.f32State = f32RampState;
    Ctrl.pRamp.f32RampUp = f32RampUp;
    Ctrl.pRamp.f32RampDown = f32RampDown;
    
    AMCLIB_SpeedLoop_F16(f16VelocityReq, f16VelocityFbck, &IDQReq, &Ctrl);
    
    *pOutRampState = Ctrl.pRamp.f32State;
    *pOutIntegPartK_1Q = Ctrl.pPIpAWQ.f32IntegPartK_1;
    *pOutInK_1Q = Ctrl.pPIpAWQ.f16InK_1;
    *pOutLimitFlagQ = Ctrl.pPIpAWQ.u16LimitFlag;
    *pOutAccW = Ctrl.pFilterW.f32Acc;
    *pOutIQReq = IDQReq.f16Arg2;

    return;
}




/******************************************************************************
* Implementation variant: Single precision floating point
******************************************************************************/
void AMCLIB_SpeedLoop_w_FLT(tFloat fltVelocityReq, \
                            tFloat fltVelocityFbck, \
                            tFloat fltAccW, \
                            tFloat fltLambdaW, \
                            tFloat fltUpperLimitQ, \
                            tFloat fltLowerLimitQ, \
                            tFloat fltInK_1Q, \
                            tFloat fltIntegPartK_1Q, \
                            tFloat fltPropGainQ, \
                            tFloat fltIntegGainQ, \
                            tFloat fltRampState, \
                            tFloat fltRampUp, \
                            tFloat fltRampDown, \
                            tFloat *pOutRampState, \
                            tFloat *pOutIntegPartK_1Q, \
                            tFloat *pOutInK_1Q, \
                            tU16 *pOutLimitFlagQ, \
                            tFloat *pOutAccW, \
                            tFloat *pOutIQReq)
{
    AMCLIB_SPEED_LOOP_T_FLT Ctrl;
    SWLIBS_2Syst_FLT IDQReq;

    Ctrl.pFilterW.fltAcc = fltAccW;
    Ctrl.pFilterW.fltLambda = fltLambdaW;

    Ctrl.pPIpAWQ.fltPropGain = fltPropGainQ;
    Ctrl.pPIpAWQ.fltIntegGain = fltIntegGainQ;
    Ctrl.pPIpAWQ.fltLowerLimit = fltLowerLimitQ;
    Ctrl.pPIpAWQ.fltUpperLimit = fltUpperLimitQ;
    Ctrl.pPIpAWQ.fltIntegPartK_1 = fltIntegPartK_1Q;
    Ctrl.pPIpAWQ.fltInK_1 = fltInK_1Q;

    Ctrl.pRamp.fltState = fltRampState;
    Ctrl.pRamp.fltRampUp = fltRampUp;
    Ctrl.pRamp.fltRampDown = fltRampDown;
    
    AMCLIB_SpeedLoop_FLT(fltVelocityReq, fltVelocityFbck, &IDQReq, &Ctrl);
    
    *pOutRampState = Ctrl.pRamp.fltState;
    *pOutIntegPartK_1Q = Ctrl.pPIpAWQ.fltIntegPartK_1;
    *pOutInK_1Q = Ctrl.pPIpAWQ.fltInK_1;
    *pOutLimitFlagQ = Ctrl.pPIpAWQ.u16LimitFlag;
    *pOutAccW = Ctrl.pFilterW.fltAcc;
    *pOutIQReq = IDQReq.fltArg2;

    return;
}

#ifdef __cplusplus
}
#endif
