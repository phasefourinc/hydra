/******************************************************************************
*
*   Copyright 2016-2018 NXP
*
*   This software is owned or controlled by NXP and may only be used strictly in accordance with the
*   applicable license terms.  By expressly accepting such terms or by downloading, installing,
*   activating and/or otherwise using the software, you are agreeing that you have read, and that
*   you agree to comply with and are bound by, such license terms.  If you do not agree to be bound
*   by the applicable license terms, then you may not retain, install, activate or otherwise use the
*   software.
*
***************************************************************************//**
*
* @file     AMCLIB_FW_w.c
*
* @version  1.0.6.0
*
* @date     Oct-26-2016
*
* @brief    Source file of wrapper function for AMCLIB_FW function.
*
*******************************************************************************
*
* Function implemented as ANSIC ISO/IEC 9899:1990, C90. 
*
******************************************************************************/
/**
@if AMCLIB_GROUP
    @addtogroup AMCLIB_GROUP
@else
    @defgroup AMCLIB_GROUP AMCLIB
@endif
*/ 

#ifdef __cplusplus
extern "C" {
#endif

/******************************************************************************
* Includes
******************************************************************************/
#include "SWLIBS_Typedefs.h"
#include "AMCLIB_FW.h"
#include "AMCLIB_FW_w.h"

/******************************************************************************
* Function implementations      (scope: module-local)
******************************************************************************/

/******************************************************************************
* Function implementations      (scope: module-exported)
******************************************************************************/

/******************************************************************************
* Implementation variant: 32-bit fractional
******************************************************************************/
void AMCLIB_FW_w_F32(tFrac32 f32ReqAmp, \
                     tFrac32 f32VelocityFbck, \
                     tFrac32 f32IQReqK_1, \
                     tFrac32 f32IQFbck, \
                     tFrac32 f32UQReq, \
                     tFrac32 f32UQLim, \
                     tFrac32 f32AccFW, \
                     tU16 u16NSamplesFW, \
                     tFrac32 f32UpperLimitFW, \
                     tFrac32 f32LowerLimitFW, \
                     tFrac32 f32InK_1FW, \
                     tFrac32 f32IntegPartK_1FW, \
                     tFrac32 f32PropGainFW, \
                     tS16 s16PropGainShiftFW, \
                     tFrac32 f32IntegGainFW, \
                     tS16 s16IntegGainShiftFW, \
                     tFrac32 *pOutIntegPartK_1FW, \
                     tFrac32 *pOutInK_1FW, \
                     tU16 *pOutLimitFlagFW, \
                     tFrac32 *pOutAccFW, \
                     tFrac32 *pOutIDReq, \
                     tFrac32 *pOutIQReq)
{
    AMCLIB_FW_T_F32 Ctrl;
    SWLIBS_2Syst_F32 IDQReq;
    tFrac32 f32IQFbckLocal;
    tFrac32 f32UQReqLocal;
    tFrac32 f32UQLimLocal;
    
    f32IQFbckLocal = f32IQFbck;
    f32UQReqLocal = f32UQReq;
    f32UQLimLocal = f32UQLim;

    Ctrl.pIQFbck = &f32IQFbckLocal;
    Ctrl.pUQReq = &f32UQReqLocal;
    Ctrl.pUQLim = &f32UQLimLocal;

    Ctrl.pFilterFW.f32Acc = f32AccFW;
    Ctrl.pFilterFW.u16NSamples = u16NSamplesFW;

    Ctrl.pPIpAWFW.f32PropGain = f32PropGainFW;
    Ctrl.pPIpAWFW.f32IntegGain = f32IntegGainFW;
    Ctrl.pPIpAWFW.s16PropGainShift = s16PropGainShiftFW;
    Ctrl.pPIpAWFW.s16IntegGainShift = s16IntegGainShiftFW;
    Ctrl.pPIpAWFW.f32LowerLimit = f32LowerLimitFW;
    Ctrl.pPIpAWFW.f32UpperLimit = f32UpperLimitFW;
    Ctrl.pPIpAWFW.f32IntegPartK_1 = f32IntegPartK_1FW;
    Ctrl.pPIpAWFW.f32InK_1 = f32InK_1FW;

    IDQReq.f32Arg2 = f32IQReqK_1;
    
    AMCLIB_FW_F32(f32ReqAmp, f32VelocityFbck, &IDQReq, &Ctrl);
    
    *pOutIntegPartK_1FW = Ctrl.pPIpAWFW.f32IntegPartK_1;
    *pOutInK_1FW = Ctrl.pPIpAWFW.f32InK_1;
    *pOutLimitFlagFW = Ctrl.pPIpAWFW.u16LimitFlag;
    *pOutAccFW = Ctrl.pFilterFW.f32Acc;
    *pOutIDReq = IDQReq.f32Arg1;
    *pOutIQReq = IDQReq.f32Arg2;

    return;
}




/******************************************************************************
* Implementation variant: 16-bit fractional
******************************************************************************/
void AMCLIB_FW_w_F16(tFrac16 f16ReqAmp, \
                     tFrac16 f16VelocityFbck, \
                     tFrac16 f16IQReqK_1, \
                     tFrac16 f16IQFbck, \
                     tFrac16 f16UQReq, \
                     tFrac16 f16UQLim, \
                     tFrac32 f32AccFW, \
                     tU16 u16NSamplesFW, \
                     tFrac16 f16UpperLimitFW, \
                     tFrac16 f16LowerLimitFW, \
                     tFrac16 f16InK_1FW, \
                     tFrac32 f32IntegPartK_1FW, \
                     tFrac16 f16PropGainFW, \
                     tS16 s16PropGainShiftFW, \
                     tFrac16 f16IntegGainFW, \
                     tS16 s16IntegGainShiftFW, \
                     tFrac32 *pOutIntegPartK_1FW, \
                     tFrac16 *pOutInK_1FW, \
                     tU16 *pOutLimitFlagFW, \
                     tFrac32 *pOutAccFW, \
                     tFrac16 *pOutIDReq, \
                     tFrac16 *pOutIQReq)
{
    AMCLIB_FW_T_F16 Ctrl;
    SWLIBS_2Syst_F16 IDQReq;
    tFrac16 f16IQFbckLocal;
    tFrac16 f16UQReqLocal;
    tFrac16 f16UQLimLocal;
    
    f16IQFbckLocal = f16IQFbck;
    f16UQReqLocal = f16UQReq;
    f16UQLimLocal = f16UQLim;

    Ctrl.pIQFbck = &f16IQFbckLocal;
    Ctrl.pUQReq = &f16UQReqLocal;
    Ctrl.pUQLim = &f16UQLimLocal;

    Ctrl.pFilterFW.f32Acc = f32AccFW;
    Ctrl.pFilterFW.u16NSamples = u16NSamplesFW;

    Ctrl.pPIpAWFW.f16PropGain = f16PropGainFW;
    Ctrl.pPIpAWFW.f16IntegGain = f16IntegGainFW;
    Ctrl.pPIpAWFW.s16PropGainShift = s16PropGainShiftFW;
    Ctrl.pPIpAWFW.s16IntegGainShift = s16IntegGainShiftFW;
    Ctrl.pPIpAWFW.f16LowerLimit = f16LowerLimitFW;
    Ctrl.pPIpAWFW.f16UpperLimit = f16UpperLimitFW;
    Ctrl.pPIpAWFW.f32IntegPartK_1 = f32IntegPartK_1FW;
    Ctrl.pPIpAWFW.f16InK_1 = f16InK_1FW;

    IDQReq.f16Arg2 = f16IQReqK_1;
    
    AMCLIB_FW_F16(f16ReqAmp, f16VelocityFbck, &IDQReq, &Ctrl);
    
    *pOutIntegPartK_1FW = Ctrl.pPIpAWFW.f32IntegPartK_1;
    *pOutInK_1FW = Ctrl.pPIpAWFW.f16InK_1;
    *pOutLimitFlagFW = Ctrl.pPIpAWFW.u16LimitFlag;
    *pOutAccFW = Ctrl.pFilterFW.f32Acc;
    *pOutIDReq = IDQReq.f16Arg1;
    *pOutIQReq = IDQReq.f16Arg2;

    return;
}




/******************************************************************************
* Implementation variant: Single precision floating point
******************************************************************************/
void AMCLIB_FW_w_FLT(tFloat fltReqAmp, \
                     tFloat fltVelocityFbck, \
                     tFloat fltIQReqK_1, \
                     tFloat fltIQFbck, \
                     tFloat fltUQReq, \
                     tFloat fltUQLim, \
                     tFloat fltAccFW, \
                     tFloat fltLambdaFW, \
                     tFloat fltUpperLimitFW, \
                     tFloat fltLowerLimitFW, \
                     tFloat fltInK_1FW, \
                     tFloat fltIntegPartK_1FW, \
                     tFloat fltPropGainFW, \
                     tFloat fltIntegGainFW, \
                     tFloat fltUmaxDivImax, \
                     tFloat *pOutIntegPartK_1FW, \
                     tFloat *pOutInK_1FW, \
                     tU16 *pOutLimitFlagFW, \
                     tFloat *pOutAccFW, \
                     tFloat *pOutIDReq, \
                     tFloat *pOutIQReq)
{
    AMCLIB_FW_T_FLT Ctrl;
    SWLIBS_2Syst_FLT IDQReq;
    tFloat fltIQFbckLocal;
    tFloat fltUQReqLocal;
    tFloat fltUQLimLocal;
    
    fltIQFbckLocal = fltIQFbck;
    fltUQReqLocal = fltUQReq;
    fltUQLimLocal = fltUQLim;

    Ctrl.pIQFbck = &fltIQFbckLocal;
    Ctrl.pUQReq = &fltUQReqLocal;
    Ctrl.pUQLim = &fltUQLimLocal;

    Ctrl.pFilterFW.fltAcc = fltAccFW;
    Ctrl.pFilterFW.fltLambda = fltLambdaFW;

    Ctrl.pPIpAWFW.fltPropGain = fltPropGainFW;
    Ctrl.pPIpAWFW.fltIntegGain = fltIntegGainFW;
    Ctrl.pPIpAWFW.fltLowerLimit = fltLowerLimitFW;
    Ctrl.pPIpAWFW.fltUpperLimit = fltUpperLimitFW;
    Ctrl.pPIpAWFW.fltIntegPartK_1 = fltIntegPartK_1FW;
    Ctrl.pPIpAWFW.fltInK_1 = fltInK_1FW;
    
    Ctrl.fltUmaxDivImax = fltUmaxDivImax;
    
    IDQReq.fltArg2 = fltIQReqK_1;
    
    AMCLIB_FW_FLT(fltReqAmp, fltVelocityFbck, &IDQReq, &Ctrl);
    
    *pOutIntegPartK_1FW = Ctrl.pPIpAWFW.fltIntegPartK_1;
    *pOutInK_1FW = Ctrl.pPIpAWFW.fltInK_1;
    *pOutLimitFlagFW = Ctrl.pPIpAWFW.u16LimitFlag;
    *pOutAccFW = Ctrl.pFilterFW.fltAcc;
    *pOutIDReq = IDQReq.fltArg1;
    *pOutIQReq = IDQReq.fltArg2;

    return;
}

#ifdef __cplusplus
}
#endif
