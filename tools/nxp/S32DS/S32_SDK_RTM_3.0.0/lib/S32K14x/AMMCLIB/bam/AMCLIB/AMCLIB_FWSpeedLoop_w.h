/******************************************************************************
*
*   Copyright 2016-2018 NXP
*
*   This software is owned or controlled by NXP and may only be used strictly in accordance with the
*   applicable license terms.  By expressly accepting such terms or by downloading, installing,
*   activating and/or otherwise using the software, you are agreeing that you have read, and that
*   you agree to comply with and are bound by, such license terms.  If you do not agree to be bound
*   by the applicable license terms, then you may not retain, install, activate or otherwise use the
*   software.
*
***************************************************************************//**
*
* @file     AMCLIB_FWSpeedLoop_w.h
*
* @version  1.0.4.0
*
* @date     Oct-26-2016
*
* @brief    Header file for #AMCLIB_FWSpeedLoop_w function.
*
******************************************************************************/
#ifndef _AMCLIB_FWSPEEDLOOP_W_H
#define _AMCLIB_FWSPEEDLOOP_W_H

#ifdef __cplusplus
extern "C" {
#endif

/******************************************************************************
* Defines and macros            (scope: module-local)
******************************************************************************/

/******************************************************************************
* Typedefs and structures       (scope: module-local)
******************************************************************************/

/******************************************************************************
* Implementation variant: 32-bit fractional
******************************************************************************/
/******************************************************************************
* Exported function prototypes
******************************************************************************/
extern void AMCLIB_FWSpeedLoop_w_F32(tFrac32 f32VelocityReq, \
                              tFrac32 f32VelocityFbck, \
                              tFrac32 f32IQReqK_1, \
                              tFrac32 f32IQFbck, \
                              tFrac32 f32UQReq, \
                              tFrac32 f32UQLim, \
                              tFrac32 f32AccW, \
                              tU16 u16NSamplesW, \
                              tFrac32 f32AccFW, \
                              tU16 u16NSamplesFW, \
                              tFrac32 f32UpperLimitQ, \
                              tFrac32 f32LowerLimitQ, \
                              tFrac32 f32InK_1Q, \
                              tFrac32 f32IntegPartK_1Q, \
                              tFrac32 f32PropGainQ, \
                              tS16 s16PropGainShiftQ, \
                              tFrac32 f32IntegGainQ, \
                              tS16 s16IntegGainShiftQ, \
                              tFrac32 f32UpperLimitFW, \
                              tFrac32 f32LowerLimitFW, \
                              tFrac32 f32InK_1FW, \
                              tFrac32 f32IntegPartK_1FW, \
                              tFrac32 f32PropGainFW, \
                              tS16 s16PropGainShiftFW, \
                              tFrac32 f32IntegGainFW, \
                              tS16 s16IntegGainShiftFW, \
                              tFrac32 f32RampState, \
                              tFrac32 f32RampUp, \
                              tFrac32 f32RampDown, \
                              tFrac32 *pOutRampState, \
                              tFrac32 *pOutIntegPartK_1Q, \
                              tFrac32 *pOutInK_1Q, \
                              tU16 *pOutLimitFlagQ, \
                              tFrac32 *pOutIntegPartK_1FW, \
                              tFrac32 *pOutInK_1FW, \
                              tU16 *pOutLimitFlagFW, \
                              tFrac32 *pOutAccW, \
                              tFrac32 *pOutAccFW, \
                              tFrac32 *pOutIDReq, \
                              tFrac32 *pOutIQReq);




/******************************************************************************
* Implementation variant: 16-bit fractional
******************************************************************************/
/******************************************************************************
* Exported function prototypes
******************************************************************************/
extern void AMCLIB_FWSpeedLoop_w_F16(tFrac16 f16VelocityReq, \
                              tFrac16 f16VelocityFbck, \
                              tFrac16 f16IQReqK_1, \
                              tFrac16 f16IQFbck, \
                              tFrac16 f16UQReq, \
                              tFrac16 f16UQLim, \
                              tFrac32 f32AccW, \
                              tU16 u16NSamplesW, \
                              tFrac32 f32AccFW, \
                              tU16 u16NSamplesFW, \
                              tFrac16 f16UpperLimitQ, \
                              tFrac16 f16LowerLimitQ, \
                              tFrac16 f16InK_1Q, \
                              tFrac32 f32IntegPartK_1Q, \
                              tFrac16 f16PropGainQ, \
                              tS16 s16PropGainShiftQ, \
                              tFrac16 f16IntegGainQ, \
                              tS16 s16IntegGainShiftQ, \
                              tFrac16 f16UpperLimitFW, \
                              tFrac16 f16LowerLimitFW, \
                              tFrac16 f16InK_1FW, \
                              tFrac32 f32IntegPartK_1FW, \
                              tFrac16 f16PropGainFW, \
                              tS16 s16PropGainShiftFW, \
                              tFrac16 f16IntegGainFW, \
                              tS16 s16IntegGainShiftFW, \
                              tFrac32 f32RampState, \
                              tFrac32 f32RampUp, \
                              tFrac32 f32RampDown, \
                              tFrac32 *pOutRampState, \
                              tFrac32 *pOutIntegPartK_1Q, \
                              tFrac16 *pOutInK_1Q, \
                              tU16 *pOutLimitFlagQ, \
                              tFrac32 *pOutIntegPartK_1FW, \
                              tFrac16 *pOutInK_1FW, \
                              tU16 *pOutLimitFlagFW, \
                              tFrac32 *pOutAccW, \
                              tFrac32 *pOutAccFW, \
                              tFrac16 *pOutIDReq, \
                              tFrac16 *pOutIQReq);




/******************************************************************************
* Implementation variant: Single precision floating point
******************************************************************************/
/******************************************************************************
* Exported function prototypes
******************************************************************************/
extern void AMCLIB_FWSpeedLoop_w_FLT(tFloat fltVelocityReq, \
                              tFloat fltVelocityFbck, \
                              tFloat fltIQReqK_1, \
                              tFloat fltIQFbck, \
                              tFloat fltUQReq, \
                              tFloat fltUQLim, \
                              tFloat fltAccW, \
                              tFloat fltLambdaW, \
                              tFloat fltAccFW, \
                              tFloat fltLambdaFW, \
                              tFloat fltUpperLimitQ, \
                              tFloat fltLowerLimitQ, \
                              tFloat fltInK_1Q, \
                              tFloat fltIntegPartK_1Q, \
                              tFloat fltPropGainQ, \
                              tFloat fltIntegGainQ, \
                              tFloat fltUpperLimitFW, \
                              tFloat fltLowerLimitFW, \
                              tFloat fltInK_1FW, \
                              tFloat fltIntegPartK_1FW, \
                              tFloat fltPropGainFW, \
                              tFloat fltIntegGainFW, \
                              tFloat fltRampState, \
                              tFloat fltRampUp, \
                              tFloat fltRampDown, \
                              tFloat fltUmaxDivImax, \
                              tFloat *pOutRampState, \
                              tFloat *pOutIntegPartK_1Q, \
                              tFloat *pOutInK_1Q, \
                              tU16 *pOutLimitFlagQ, \
                              tFloat *pOutIntegPartK_1FW, \
                              tFloat *pOutInK_1FW, \
                              tU16 *pOutLimitFlagFW, \
                              tFloat *pOutAccW, \
                              tFloat *pOutAccFW, \
                              tFloat *pOutIDReq, \
                              tFloat *pOutIQReq);

/******************************************************************************
* Inline functions
******************************************************************************/

#ifdef __cplusplus
}
#endif

#endif /* _AMCLIB_FWSPEEDLOOP_W_H */
