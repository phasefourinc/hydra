/******************************************************************************
*
*   Copyright 2016-2018 NXP
*
*   This software is owned or controlled by NXP and may only be used strictly in accordance with the
*   applicable license terms.  By expressly accepting such terms or by downloading, installing,
*   activating and/or otherwise using the software, you are agreeing that you have read, and that
*   you agree to comply with and are bound by, such license terms.  If you do not agree to be bound
*   by the applicable license terms, then you may not retain, install, activate or otherwise use the
*   software.
*
***************************************************************************//**
*
* @file     AMCLIB_FWSpeedLoop_w.c
*
* @version  1.0.6.0
*
* @date     Oct-26-2016
*
* @brief    Source file of wrapper function for AMCLIB_FWSpeedLoop function.
*
*******************************************************************************
*
* Function implemented as ANSIC ISO/IEC 9899:1990, C90. 
*
******************************************************************************/
/**
@if AMCLIB_GROUP
    @addtogroup AMCLIB_GROUP
@else
    @defgroup AMCLIB_GROUP AMCLIB
@endif
*/ 

#ifdef __cplusplus
extern "C" {
#endif

/******************************************************************************
* Includes
******************************************************************************/
#include "SWLIBS_Typedefs.h"
#include "AMCLIB_FWSpeedLoop.h"
#include "AMCLIB_FWSpeedLoop_w.h"

/******************************************************************************
* Function implementations      (scope: module-local)
******************************************************************************/

/******************************************************************************
* Function implementations      (scope: module-exported)
******************************************************************************/

/******************************************************************************
* Implementation variant: 32-bit fractional
******************************************************************************/
void AMCLIB_FWSpeedLoop_w_F32(tFrac32 f32VelocityReq, \
                              tFrac32 f32VelocityFbck, \
                              tFrac32 f32IQReqK_1, \
                              tFrac32 f32IQFbck, \
                              tFrac32 f32UQReq, \
                              tFrac32 f32UQLim, \
                              tFrac32 f32AccW, \
                              tU16 u16NSamplesW, \
                              tFrac32 f32AccFW, \
                              tU16 u16NSamplesFW, \
                              tFrac32 f32UpperLimitQ, \
                              tFrac32 f32LowerLimitQ, \
                              tFrac32 f32InK_1Q, \
                              tFrac32 f32IntegPartK_1Q, \
                              tFrac32 f32PropGainQ, \
                              tS16 s16PropGainShiftQ, \
                              tFrac32 f32IntegGainQ, \
                              tS16 s16IntegGainShiftQ, \
                              tFrac32 f32UpperLimitFW, \
                              tFrac32 f32LowerLimitFW, \
                              tFrac32 f32InK_1FW, \
                              tFrac32 f32IntegPartK_1FW, \
                              tFrac32 f32PropGainFW, \
                              tS16 s16PropGainShiftFW, \
                              tFrac32 f32IntegGainFW, \
                              tS16 s16IntegGainShiftFW, \
                              tFrac32 f32RampState, \
                              tFrac32 f32RampUp, \
                              tFrac32 f32RampDown, \
                              tFrac32 *pOutRampState, \
                              tFrac32 *pOutIntegPartK_1Q, \
                              tFrac32 *pOutInK_1Q, \
                              tU16 *pOutLimitFlagQ, \
                              tFrac32 *pOutIntegPartK_1FW, \
                              tFrac32 *pOutInK_1FW, \
                              tU16 *pOutLimitFlagFW, \
                              tFrac32 *pOutAccW, \
                              tFrac32 *pOutAccFW, \
                              tFrac32 *pOutIDReq, \
                              tFrac32 *pOutIQReq)
{
    AMCLIB_FW_SPEED_LOOP_T_F32 Ctrl;
    SWLIBS_2Syst_F32 IDQReq;
    tFrac32 f32IQFbckLocal;
    tFrac32 f32UQReqLocal;
    tFrac32 f32UQLimLocal;
    
    f32IQFbckLocal = f32IQFbck;
    f32UQReqLocal = f32UQReq;
    f32UQLimLocal = f32UQLim;

    Ctrl.pIQFbck = &f32IQFbckLocal;
    Ctrl.pUQReq = &f32UQReqLocal;
    Ctrl.pUQLim = &f32UQLimLocal;

    Ctrl.pFilterW.f32Acc = f32AccW;
    Ctrl.pFilterW.u16NSamples = u16NSamplesW;

    Ctrl.pFilterFW.f32Acc = f32AccFW;
    Ctrl.pFilterFW.u16NSamples = u16NSamplesFW;

    Ctrl.pPIpAWQ.f32PropGain = f32PropGainQ;
    Ctrl.pPIpAWQ.f32IntegGain = f32IntegGainQ;
    Ctrl.pPIpAWQ.s16PropGainShift = s16PropGainShiftQ;
    Ctrl.pPIpAWQ.s16IntegGainShift = s16IntegGainShiftQ;
    Ctrl.pPIpAWQ.f32LowerLimit = f32LowerLimitQ;
    Ctrl.pPIpAWQ.f32UpperLimit = f32UpperLimitQ;
    Ctrl.pPIpAWQ.f32IntegPartK_1 = f32IntegPartK_1Q;
    Ctrl.pPIpAWQ.f32InK_1 = f32InK_1Q;

    Ctrl.pPIpAWFW.f32PropGain = f32PropGainFW;
    Ctrl.pPIpAWFW.f32IntegGain = f32IntegGainFW;
    Ctrl.pPIpAWFW.s16PropGainShift = s16PropGainShiftFW;
    Ctrl.pPIpAWFW.s16IntegGainShift = s16IntegGainShiftFW;
    Ctrl.pPIpAWFW.f32LowerLimit = f32LowerLimitFW;
    Ctrl.pPIpAWFW.f32UpperLimit = f32UpperLimitFW;
    Ctrl.pPIpAWFW.f32IntegPartK_1 = f32IntegPartK_1FW;
    Ctrl.pPIpAWFW.f32InK_1 = f32InK_1FW;

    Ctrl.pRamp.f32State = f32RampState;
    Ctrl.pRamp.f32RampUp = f32RampUp;
    Ctrl.pRamp.f32RampDown = f32RampDown;
    
    IDQReq.f32Arg2 = f32IQReqK_1;
    
    AMCLIB_FWSpeedLoop_F32(f32VelocityReq, f32VelocityFbck, &IDQReq, &Ctrl);
    
    *pOutRampState = Ctrl.pRamp.f32State;
    *pOutIntegPartK_1Q = Ctrl.pPIpAWQ.f32IntegPartK_1;
    *pOutInK_1Q = Ctrl.pPIpAWQ.f32InK_1;
    *pOutLimitFlagQ = Ctrl.pPIpAWQ.u16LimitFlag;
    *pOutIntegPartK_1FW = Ctrl.pPIpAWFW.f32IntegPartK_1;
    *pOutInK_1FW = Ctrl.pPIpAWFW.f32InK_1;
    *pOutLimitFlagFW = Ctrl.pPIpAWFW.u16LimitFlag;
    *pOutAccW = Ctrl.pFilterW.f32Acc;
    *pOutAccFW = Ctrl.pFilterFW.f32Acc;
    *pOutIDReq = IDQReq.f32Arg1;
    *pOutIQReq = IDQReq.f32Arg2;

    return;
}




/******************************************************************************
* Implementation variant: 16-bit fractional
******************************************************************************/
void AMCLIB_FWSpeedLoop_w_F16(tFrac16 f16VelocityReq, \
                              tFrac16 f16VelocityFbck, \
                              tFrac16 f16IQReqK_1, \
                              tFrac16 f16IQFbck, \
                              tFrac16 f16UQReq, \
                              tFrac16 f16UQLim, \
                              tFrac32 f32AccW, \
                              tU16 u16NSamplesW, \
                              tFrac32 f32AccFW, \
                              tU16 u16NSamplesFW, \
                              tFrac16 f16UpperLimitQ, \
                              tFrac16 f16LowerLimitQ, \
                              tFrac16 f16InK_1Q, \
                              tFrac32 f32IntegPartK_1Q, \
                              tFrac16 f16PropGainQ, \
                              tS16 s16PropGainShiftQ, \
                              tFrac16 f16IntegGainQ, \
                              tS16 s16IntegGainShiftQ, \
                              tFrac16 f16UpperLimitFW, \
                              tFrac16 f16LowerLimitFW, \
                              tFrac16 f16InK_1FW, \
                              tFrac32 f32IntegPartK_1FW, \
                              tFrac16 f16PropGainFW, \
                              tS16 s16PropGainShiftFW, \
                              tFrac16 f16IntegGainFW, \
                              tS16 s16IntegGainShiftFW, \
                              tFrac32 f32RampState, \
                              tFrac32 f32RampUp, \
                              tFrac32 f32RampDown, \
                              tFrac32 *pOutRampState, \
                              tFrac32 *pOutIntegPartK_1Q, \
                              tFrac16 *pOutInK_1Q, \
                              tU16 *pOutLimitFlagQ, \
                              tFrac32 *pOutIntegPartK_1FW, \
                              tFrac16 *pOutInK_1FW, \
                              tU16 *pOutLimitFlagFW, \
                              tFrac32 *pOutAccW, \
                              tFrac32 *pOutAccFW, \
                              tFrac16 *pOutIDReq, \
                              tFrac16 *pOutIQReq)
{
    AMCLIB_FW_SPEED_LOOP_T_F16 Ctrl;
    SWLIBS_2Syst_F16 IDQReq;
    tFrac16 f16IQFbckLocal;
    tFrac16 f16UQReqLocal;
    tFrac16 f16UQLimLocal;
    
    f16IQFbckLocal = f16IQFbck;
    f16UQReqLocal = f16UQReq;
    f16UQLimLocal = f16UQLim;

    Ctrl.pIQFbck = &f16IQFbckLocal;
    Ctrl.pUQReq = &f16UQReqLocal;
    Ctrl.pUQLim = &f16UQLimLocal;

    Ctrl.pFilterW.f32Acc = f32AccW;
    Ctrl.pFilterW.u16NSamples = u16NSamplesW;

    Ctrl.pFilterFW.f32Acc = f32AccFW;
    Ctrl.pFilterFW.u16NSamples = u16NSamplesFW;

    Ctrl.pPIpAWQ.f16PropGain = f16PropGainQ;
    Ctrl.pPIpAWQ.f16IntegGain = f16IntegGainQ;
    Ctrl.pPIpAWQ.s16PropGainShift = s16PropGainShiftQ;
    Ctrl.pPIpAWQ.s16IntegGainShift = s16IntegGainShiftQ;
    Ctrl.pPIpAWQ.f16LowerLimit = f16LowerLimitQ;
    Ctrl.pPIpAWQ.f16UpperLimit = f16UpperLimitQ;
    Ctrl.pPIpAWQ.f32IntegPartK_1 = f32IntegPartK_1Q;
    Ctrl.pPIpAWQ.f16InK_1 = f16InK_1Q;

    Ctrl.pPIpAWFW.f16PropGain = f16PropGainFW;
    Ctrl.pPIpAWFW.f16IntegGain = f16IntegGainFW;
    Ctrl.pPIpAWFW.s16PropGainShift = s16PropGainShiftFW;
    Ctrl.pPIpAWFW.s16IntegGainShift = s16IntegGainShiftFW;
    Ctrl.pPIpAWFW.f16LowerLimit = f16LowerLimitFW;
    Ctrl.pPIpAWFW.f16UpperLimit = f16UpperLimitFW;
    Ctrl.pPIpAWFW.f32IntegPartK_1 = f32IntegPartK_1FW;
    Ctrl.pPIpAWFW.f16InK_1 = f16InK_1FW;

    Ctrl.pRamp.f32State = f32RampState;
    Ctrl.pRamp.f32RampUp = f32RampUp;
    Ctrl.pRamp.f32RampDown = f32RampDown;
    
    IDQReq.f16Arg2 = f16IQReqK_1;
    
    AMCLIB_FWSpeedLoop_F16(f16VelocityReq, f16VelocityFbck, &IDQReq, &Ctrl);
    
    *pOutRampState = Ctrl.pRamp.f32State;
    *pOutIntegPartK_1Q = Ctrl.pPIpAWQ.f32IntegPartK_1;
    *pOutInK_1Q = Ctrl.pPIpAWQ.f16InK_1;
    *pOutLimitFlagQ = Ctrl.pPIpAWQ.u16LimitFlag;
    *pOutIntegPartK_1FW = Ctrl.pPIpAWFW.f32IntegPartK_1;
    *pOutInK_1FW = Ctrl.pPIpAWFW.f16InK_1;
    *pOutLimitFlagFW = Ctrl.pPIpAWFW.u16LimitFlag;
    *pOutAccW = Ctrl.pFilterW.f32Acc;
    *pOutAccFW = Ctrl.pFilterFW.f32Acc;
    *pOutIDReq = IDQReq.f16Arg1;
    *pOutIQReq = IDQReq.f16Arg2;

    return;
}




/******************************************************************************
* Implementation variant: Single precision floating point
******************************************************************************/
void AMCLIB_FWSpeedLoop_w_FLT(tFloat fltVelocityReq, \
                              tFloat fltVelocityFbck, \
                              tFloat fltIQReqK_1, \
                              tFloat fltIQFbck, \
                              tFloat fltUQReq, \
                              tFloat fltUQLim, \
                              tFloat fltAccW, \
                              tFloat fltLambdaW, \
                              tFloat fltAccFW, \
                              tFloat fltLambdaFW, \
                              tFloat fltUpperLimitQ, \
                              tFloat fltLowerLimitQ, \
                              tFloat fltInK_1Q, \
                              tFloat fltIntegPartK_1Q, \
                              tFloat fltPropGainQ, \
                              tFloat fltIntegGainQ, \
                              tFloat fltUpperLimitFW, \
                              tFloat fltLowerLimitFW, \
                              tFloat fltInK_1FW, \
                              tFloat fltIntegPartK_1FW, \
                              tFloat fltPropGainFW, \
                              tFloat fltIntegGainFW, \
                              tFloat fltRampState, \
                              tFloat fltRampUp, \
                              tFloat fltRampDown, \
                              tFloat fltUmaxDivImax, \
                              tFloat *pOutRampState, \
                              tFloat *pOutIntegPartK_1Q, \
                              tFloat *pOutInK_1Q, \
                              tU16 *pOutLimitFlagQ, \
                              tFloat *pOutIntegPartK_1FW, \
                              tFloat *pOutInK_1FW, \
                              tU16 *pOutLimitFlagFW, \
                              tFloat *pOutAccW, \
                              tFloat *pOutAccFW, \
                              tFloat *pOutIDReq, \
                              tFloat *pOutIQReq)
{
    AMCLIB_FW_SPEED_LOOP_T_FLT Ctrl;
    SWLIBS_2Syst_FLT IDQReq;
    tFloat fltIQFbckLocal;
    tFloat fltUQReqLocal;
    tFloat fltUQLimLocal;
    
    fltIQFbckLocal = fltIQFbck;
    fltUQReqLocal = fltUQReq;
    fltUQLimLocal = fltUQLim;

    Ctrl.pIQFbck = &fltIQFbckLocal;
    Ctrl.pUQReq = &fltUQReqLocal;
    Ctrl.pUQLim = &fltUQLimLocal;

    Ctrl.pFilterW.fltAcc = fltAccW;
    Ctrl.pFilterW.fltLambda = fltLambdaW;

    Ctrl.pFilterFW.fltAcc = fltAccFW;
    Ctrl.pFilterFW.fltLambda = fltLambdaFW;

    Ctrl.pPIpAWQ.fltPropGain = fltPropGainQ;
    Ctrl.pPIpAWQ.fltIntegGain = fltIntegGainQ;
    Ctrl.pPIpAWQ.fltLowerLimit = fltLowerLimitQ;
    Ctrl.pPIpAWQ.fltUpperLimit = fltUpperLimitQ;
    Ctrl.pPIpAWQ.fltIntegPartK_1 = fltIntegPartK_1Q;
    Ctrl.pPIpAWQ.fltInK_1 = fltInK_1Q;

    Ctrl.pPIpAWFW.fltPropGain = fltPropGainFW;
    Ctrl.pPIpAWFW.fltIntegGain = fltIntegGainFW;
    Ctrl.pPIpAWFW.fltLowerLimit = fltLowerLimitFW;
    Ctrl.pPIpAWFW.fltUpperLimit = fltUpperLimitFW;
    Ctrl.pPIpAWFW.fltIntegPartK_1 = fltIntegPartK_1FW;
    Ctrl.pPIpAWFW.fltInK_1 = fltInK_1FW;

    Ctrl.pRamp.fltState = fltRampState;
    Ctrl.pRamp.fltRampUp = fltRampUp;
    Ctrl.pRamp.fltRampDown = fltRampDown;
    
    Ctrl.fltUmaxDivImax = fltUmaxDivImax;
    
    IDQReq.fltArg2 = fltIQReqK_1;
    
    AMCLIB_FWSpeedLoop_FLT(fltVelocityReq, fltVelocityFbck, &IDQReq, &Ctrl);
    
    *pOutRampState = Ctrl.pRamp.fltState;
    *pOutIntegPartK_1Q = Ctrl.pPIpAWQ.fltIntegPartK_1;
    *pOutInK_1Q = Ctrl.pPIpAWQ.fltInK_1;
    *pOutLimitFlagQ = Ctrl.pPIpAWQ.u16LimitFlag;
    *pOutIntegPartK_1FW = Ctrl.pPIpAWFW.fltIntegPartK_1;
    *pOutInK_1FW = Ctrl.pPIpAWFW.fltInK_1;
    *pOutLimitFlagFW = Ctrl.pPIpAWFW.u16LimitFlag;
    *pOutAccW = Ctrl.pFilterW.fltAcc;
    *pOutAccFW = Ctrl.pFilterFW.fltAcc;
    *pOutIDReq = IDQReq.fltArg1;
    *pOutIQReq = IDQReq.fltArg2;

    return;
}

#ifdef __cplusplus
}
#endif
