%-
%- Implementation of RTOS ADAPTOR for DefaultRTOS
%-
%- (C) 2009 Freescale, all rights reserved
%-
%------------------------------------------------------------------------------
%- Including common RTOS adapter library.
%------------------------------------------------------------------------------
%- It is included indirectly through symbol as a workaround of limitation
%- of the tool used for creating installation.
%-
%define RTOSAdap_priv_CommonAdapterInterface Sw\RTOSAdaptor\Common_RTOSAdaptor.prg
%include %'RTOSAdap_priv_CommonAdapterInterface'
%undef RTOSAdap_priv_CommonAdapterInterface
%-
%-
%------------------------------------------------------------------------------
%SUBROUTINE RTOSAdap_genRTOSTypeDefinitions
%------------------------------------------------------------------------------
%--  %- PUBLIC TYPES
%--%{ {%'OperatingSystemId' RTOS Adapter} Pointer to the device data structure used and managed by RTOS %}

%--%{ {%'OperatingSystemId' RTOS Adapter} RTOS specific definition of type of Ioctl() command constants %}

  %- PRIVATE TYPES
%{ {%'OperatingSystemId' RTOS Adapter} Type of the parameter passed into ISR from RTOS interrupt dispatcher %}
typedef void *LDD_RTOS_TISRParameter;

%{ {%'OperatingSystemId' RTOS Adapter} Structure for saving/restoring interrupt vector %}
typedef struct {
  void (*isrFunction)(LDD_RTOS_TISRParameter);                   %>>%{ ISR function handler %}
  LDD_RTOS_TISRParameter isrData;                                %>>%{ ISR parameter %}
} LDD_RTOS_TISRVectorSettings;

%SUBROUTINE_END
%-
%-
%------------------------------------------------------------------------------
%SUBROUTINE RTOSAdap_priv_getInstallationFunctionForDriverType(arg_componentType,out_installFunctionName)
%------------------------------------------------------------------------------
%- Private method. Returns the name of RTOS installation function for the specified
%- component type.
%-
%- Input parameters:
%-      arg_componentType       - Type of the component. One from enumeration list 'RTOSAdap_enum_componentTypes'.
%-
%- Outputs:
%-      out_installFunctionName - Name of RTOS function installing the driver of component
%-                                of specified type.
%-
  %- Check if arg_componentType is a valid enumerated value
  %:loc_compTypeIndex = %get_index1(%'arg_componentType',RTOSAdap_enum_componentTypes)
  %if loc_compTypeIndex = '-1'
    %error! Invalid HAL component type "%'arg_componentType'"
  %endif
  %-
  %- In RTOSAdap_priv_componentTypeAttribs there is item of format "<componentType>|<installFunctionName>"
  %- for each component type on the same index as within the list RTOSAdap_enum_componentTypes.
  %define loc_compTypeAttribs %[%'loc_compTypeIndex',RTOSAdap_priv_componentTypeAttribs]
  %inclSUB RTOSAdap_lib_getStructMember(%'loc_compTypeAttribs',componentType,loc_checkCompType,required)
  %inclSUB RTOSAdap_lib_getStructMember(%'loc_compTypeAttribs',installFunctionName,%'out_installFunctionName',required)
  %if loc_checkCompType != arg_componentType
    %error! %'OperatingSystemId' RTOS Adapter internal error, lists RTOSAdap_priv_componentTypeAttribs and RTOSAdap_componentTypes are not in index-consistency (at index %'loc_compTypeIndex' there is "%'loc_checkCompType'" instead of "%'arg_componentType'")
  %endif
  %-
  %undef loc_compTypeIndex
  %undef loc_compTypeAttribs
  %undef loc_checkCompType
%SUBROUTINE_END
%-
%-
%------------------------------------------------------------------------------
%SUBROUTINE RTOSAdap_genDriverInstallationsFunctionDefinition
%------------------------------------------------------------------------------
%- DefaultRTOS implementation: For each instance of component calls RTOS installation function
%- (specific for each type of component). The name of the component instance is passed as first parameter.
%- Then pointers to all registered method implementations are passed as next parameters
%- (in the same order they have been registered).
%-
%define! Description_PE_Install_Hardware_Drivers The function responsible for installing drivers for all HAL components within the project.
%include Common\GeneralInternalGlobal.Inc (PE_Install_Hardware_Drivers)
%{ {%'OperatingSystemId' RTOS Adapter} Definition of main function for installation of drivers for all components %}
void PE_Install_Hardware_Drivers (void)
{
  %- List PE_G_RTOSAdap_installFuns contains all names of lists of lines of installation function calls
  %for loc_instanceName from PE_G_RTOSAdap_regCompInstanceNames
    %- Obtain component type
    %define! loc_componentType %[%'for_index_1',PE_G_RTOSAdap_regCompInstanceTypes]
    %ifdef RTOSAdap_priv_API_%'loc_componentType'
      %if %list_size(RTOSAdap_priv_API_%'loc_componentType') > '0'
        %- If list is empty, no installation function will be called
        %-
        %- Get installation function for arg_componentType
        %inclSUB RTOSAdap_priv_getInstallationFunctionForDriverType(%'loc_componentType',loc_installFunctionName)
        %-
  %{ {%'OperatingSystemId' RTOS Adapter} Call to the driver installation function for component "%'loc_instanceName'" %}
  (void)%'loc_installFunctionName'(
        %- First argument: driver name = component instance name + ':'
    "%'loc_instanceName':", %{ Driver name equals to the name of the component instance %}
        %-
        %- Next arguments are pointers to methods
        %for loc_methodName from RTOSAdap_priv_API_%'loc_componentType'
          %ifdef PE_G_RTOSAdap_regCompInstance_%'loc_instanceName'_methodPtr_%'loc_methodName'
            %if %for_last
    %{ %'loc_methodName' %} %"PE_G_RTOSAdap_regCompInstance_%'loc_instanceName'_methodPtr_%'loc_methodName'"
            %else
    %{ %'loc_methodName' %} %"PE_G_RTOSAdap_regCompInstance_%'loc_instanceName'_methodPtr_%'loc_methodName'",
            %endif
          %else
            %error! Method "%'loc_methodName'" is not registered for HAL component "%'loc_instanceName'" of type "%'loc_componentType'" (check call to RTOSAdap_registerComponentMethodImplementation() from HAL component DRV file)
          %endif
        %endfor  %- loc_methodName
        %- Add closing ")" to the installation function call
  );
      %endif %- list_size(RTOSAdap_priv_API_%'loc_componentType')>'0'
    %else
      %error! API for HAL component of type "%'loc_componentType'" is not defined. Check definition of list RTOSAdap_priv_API_%'loc_componentType' within %'OperatingSystemId' RTOS Adapter.
    %endif
  %endfor %- loc_instanceName
  %-
  %undef! loc_componentType
  %undef! loc_installFunctionName
}
%SUBROUTINE_END
%-
%-
%------------------------------------------------------------------------------
%SUBROUTINE RTOSAdap_genDriverInstallationsFunctionDeclaration
%------------------------------------------------------------------------------
%include Common\GeneralInternalGlobal.Inc (PE_Install_Hardware_Drivers)
%{ {%'OperatingSystemId' RTOS Adapter} Prototype of main function for installation of drivers of all components %}
void PE_Install_Hardware_Drivers (void);
%SUBROUTINE_END
%-
%-
%------------------------------------------------------------------------------
%SUBROUTINE RTOSAdap_genDriverMemoryAlloc(arg_destPtrBuffer,arg_objType,opt_arg_errCode,arg_globDefsThread)
%------------------------------------------------------------------------------
  %- Check if RTOSAdap_driverMemoryAlloc() was called only once for this object
  %if defined(RTOSAdap_alloc_object_%'arg_destPtrBuffer')
    %error! There was called allocation for object "%'arg_destPtrBuffer'" for object type "%'arg_objType'" twice (which does not work for bareboard RTOS what only simulates the dynamic allocation)
  %else
    %- o.k., it is first call, mark that such object was allocated
    %define RTOSAdap_alloc_object_%'arg_destPtrBuffer'
  %endif
  %-
  %- Dynamic allocation is supported (generate it)
  %inclSUB RTOSAdap_getRTOSFunction(Malloc,loc_MallocFunction)
%{ {%'OperatingSystemId' RTOS Adapter} Driver memory allocation: RTOS function call is defined by %'OperatingSystemId' RTOS Adapter property %}
%-MEM_ALLOC_FUNCTION_START
%-MEM_ALLOC_FUNCTION_END
%if opt_arg_errCode <> ''
#if DefaultRTOS_CHECK_MEMORY_ALLOCATION_ERRORS
if (%'arg_destPtrBuffer' == NULL) {
  %'opt_arg_errCode'
}
#endif
%endif
  %-
  %- Set memory block type (not supported now)
%-_mem_set_type(%'arg_destPtrBuffer', MEM_TYPE_...);
  %-
  %undef loc_MallocFunction
%SUBROUTINE_END
%-
%-
%------------------------------------------------------------------------------
%SUBROUTINE RTOSAdap_genDriverMemoryDealloc(arg_ptrBuffer,arg_objType)
%------------------------------------------------------------------------------
  %- Dynamic allocation is supported, generate it
  %inclSUB RTOSAdap_getRTOSFunction(Dealloc,loc_DeallocFunction)
%{ {%'OperatingSystemId' RTOS Adapter} Driver memory deallocation: RTOS function call is defined by %'OperatingSystemId' RTOS Adapter property %}
%-MEM_DEALLOC_FUNCTION_START
%-MEM_DEALLOC_FUNCTION_END
  %-
  %undef loc_DeallocFunction
%SUBROUTINE_END
%-
%-
%------------------------------------------------------------------------------
%SUBROUTINE RTOSAdap_getInterruptVectorSymbol(arg_vectorName,out_vectorIndexConstantName)
%------------------------------------------------------------------------------
  %define! %out_vectorIndexConstantName LDD_ivIndex_%'arg_vectorName'
%SUBROUTINE_END
%-
%-
%------------------------------------------------------------------------------
%SUBROUTINE RTOSAdap_genSetInterruptVector(arg_intVectorProperty,arg_isrFunctionName,arg_isrParameterType,arg_isrParameterValue,opt_arg_oldISRSettings,arg_globDefsThread)
%------------------------------------------------------------------------------
%- DefaultRTOS 'arg_isrParameterType' limitation: Parameter type should be convertible to 'void *' type.
%-
  %- Check if RTOSAdap_genSetInterruptVector() was called only once for this interrupt vector
  %if defined(RTOSAdap_alloc_interrupt_%"%'arg_intVectorProperty'_Name")
    %error! There was called allocation for interrupt vector %"%'arg_intVectorProperty'_Name" twice (which does not work for bareboard RTOS what only simulates the run-time vector allocation)
  %else
    %- o.k., it is first call, mark that interrupt vector was allocated
    %define RTOSAdap_alloc_interrupt_%"%'arg_intVectorProperty'_Name"
  %endif
  %-
  %- DefaultRTOS has support of run-time installable ISR vectors
  %-
  %define! loc_vectorIndexConstantName
  %inclSUB RTOSAdap_getInterruptVectorSymbol(%"%'arg_intVectorProperty'_Name",loc_vectorIndexConstantName)
  %-
  %if opt_arg_oldISRSettings != ''
%{ {%'OperatingSystemId' RTOS Adapter} Save old and set new interrupt vector (function handler and ISR parameter) %}
%{ Note: Exception handler for interrupt is not saved, because it is not modified %}
%-SAVE_ISR_FUNCTION_START
%-SAVE_ISR_FUNCTION_END
  %endif
  %-else
%{ {%'OperatingSystemId' RTOS Adapter} Set new interrupt vector (function handler and ISR parameter) %}
%-INSTALL_ISR_FUNCTION_START
%-INSTALL_ISR_FUNCTION_END
  %-endif
  %-
  %undef loc_vectorIndexConstantName
%SUBROUTINE_END
%-
%-
%------------------------------------------------------------------------------
%SUBROUTINE RTOSAdap_genRestoreInterruptVector(arg_intVectorProperty,arg_oldISRSettings)
%------------------------------------------------------------------------------
  %define! loc_vectorIndexConstantName
  %inclSUB RTOSAdap_getInterruptVectorSymbol(%"%'arg_intVectorProperty'_Name",loc_vectorIndexConstantName)
%{ {%'OperatingSystemId' RTOS Adapter} Restore interrupt vector (function handler and ISR parameter) %}
%{ Note: Exception handler for interrupt is not restored, because it was not modified %}
%-RESTORE_ISR_FUNCTION_START
%-RESTORE_FUNCTION_END
%SUBROUTINE_END
%-
%-
%------------------------------------------------------------------------------
%SUBROUTINE RTOSAdap_genISRSettingsVarDeclaration(arg_varName,opt_arg_comment)
%------------------------------------------------------------------------------
  %if opt_arg_comment != ''
LDD_RTOS_TISRVectorSettings %'arg_varName';                      %>>%{ {%'OperatingSystemId' RTOS Adapter} %'opt_arg_comment' %}
  %else
LDD_RTOS_TISRVectorSettings %'arg_varName';                      %>>%{ {%'OperatingSystemId' RTOS Adapter} Buffer where old interrupt settings are saved %}
  %endif
%SUBROUTINE_END
%-
%-
%------------------------------------------------------------------------------
%SUBROUTINE RTOSAdap_genISRFunctionDefinitionOpen(arg_intVectorProperty,arg_isrFunctionName,arg_isrParameterType,arg_isrParameterName)
%------------------------------------------------------------------------------
%- DefaultRTOS 'arg_isrParameterType' limitation: Parameter type should be convertible to 'void *' type.
%-
%-ISR_OPEN_CODE_START
%-ISR_OPEN_CODE_END
%SUBROUTINE_END
%-
%-
%------------------------------------------------------------------------------
%SUBROUTINE RTOSAdap_genISRFunctionDefinitionClose(arg_intVectorProperty,arg_isrFunctionName,arg_isrParameterType,arg_isrParameterName)
%------------------------------------------------------------------------------
%-ISR_CLOSE_CODE_START
%-ISR_CLOSE_CODE_END
%SUBROUTINE_END
%-
%-
%------------------------------------------------------------------------------
%SUBROUTINE RTOSAdap_genISRFunctionDeclaration(arg_intVectorProperty,arg_isrFunctionName,arg_isrParameterType,arg_isrParameterName)
%------------------------------------------------------------------------------
%- DefaultRTOS 'arg_isrParameterType' limitation: Parameter type should be convertible to 'void *' type.
%-
%{ {%'OperatingSystemId' RTOS Adapter} ISR function prototype %}
%-ISR_DECLARATION_FUNCTION_START
%-ISR_DECLARATION_FUNCTION_END
%-
%SUBROUTINE_END
%-
%-
%------------------------------------------------------------------------------
%SUBROUTINE RTOSAdap_genReentrantMethodBegin(opt_arg_genReentrantMethods)
%------------------------------------------------------------------------------
  %- Handle optional parameter
  %inclSUB RTOSAdap_priv_getOptionalParameterEffectiveValue(genReentrantMethods,%'opt_arg_genReentrantMethods',arg_genReentrantMethods)
  %-
  %if arg_genReentrantMethods == 'yes'
    %- Reentrant methods are required, generate enter critical section
    %inclSUB RTOSAdap_getRTOSFunction(EnterCritical,loc_enterCriticalFunctionName)
%{ {%'OperatingSystemId' RTOS Adapter} Method is required to be reentrant, critical section begins (RTOS function call is defined by %'OperatingSystemId' RTOS Adapter property) %}
%-ENTER_REENTRANT_FUNCTION_START
%-ENTER_REENTRANT_FUNCTION_END
    %undef loc_enterCriticalFunctionName
  %elif arg_genReentrantMethods == 'no'
    %- Reentrant methods are not required, no code is generated
  %else
    %error! Invalid value of parameter 'opt_arg_genReentrantMethods' ("%'arg_genReentrantMethods'")
  %endif
  %-
  %undef arg_genReentrantMethods
%SUBROUTINE_END
%-
%-
%------------------------------------------------------------------------------
%SUBROUTINE RTOSAdap_genReentrantMethodEnd(opt_arg_genReentrantMethods)
%------------------------------------------------------------------------------
  %- Handle optional parameter
  %inclSUB RTOSAdap_priv_getOptionalParameterEffectiveValue(genReentrantMethods,%'opt_arg_genReentrantMethods',arg_genReentrantMethods)
  %-
  %if arg_genReentrantMethods == 'yes'
    %- Reentrant methods are required, generate exit critical section
    %inclSUB RTOSAdap_getRTOSFunction(ExitCritical,loc_exitCriticalFunctionName)
%{ {%'OperatingSystemId' RTOS Adapter} Critical section end (RTOS function call is defined by %'OperatingSystemId' RTOS Adapter property) %}
%-EXIT_REENTRANT_FUNCTION_START
%-EXIT_REENTRANT_FUNCTION_END
    %undef loc_exitCriticalFunctionName
  %elif arg_genReentrantMethods == 'no'
    %- Reentrant methods are not required, no code is generated
  %else
    %error! Invalid value of parameter 'opt_arg_genReentrantMethods' ("%'arg_genReentrantMethods'")
  %endif
  %-
  %undef arg_genReentrantMethods
%SUBROUTINE_END
%-
%-
%------------------------------------------------------------------------------
%SUBROUTINE RTOSAdap_genCriticalSectionBegin(opt_arg_genReentrantMethods)
%------------------------------------------------------------------------------
  %- Handle optional parameter
  %inclSUB RTOSAdap_priv_getOptionalParameterEffectiveValue(genReentrantMethods,%'opt_arg_genReentrantMethods',arg_genReentrantMethods)
  %-
  %if arg_genReentrantMethods == 'yes'
    %- The method itself is generated as reentrant, additional critical sections are not needed
  %elif arg_genReentrantMethods == 'no'
    %- Reentrant methods are not required, but critical sections must remain
    %inclSUB RTOSAdap_getRTOSFunction(EnterCritical,loc_enterCriticalFunctionName)
%{ {%'OperatingSystemId' RTOS Adapter} Critical section begin (RTOS function call is defined by %'OperatingSystemId' RTOS Adapter property) %}
%-ENTER_CRITICAL_FUNCTION_START
%-ENTER_CRITICAL_FUNCTION_END
    %undef loc_enterCriticalFunctionName
  %else
    %error! Invalid value of parameter 'opt_arg_genReentrantMethods' ("%'arg_genReentrantMethods'")
  %endif
  %-
  %undef arg_genReentrantMethods
%SUBROUTINE_END
%-
%-
%------------------------------------------------------------------------------
%SUBROUTINE RTOSAdap_genCriticalSectionEnd(opt_arg_genReentrantMethods)
%------------------------------------------------------------------------------
  %- Handle optional parameter
  %inclSUB RTOSAdap_priv_getOptionalParameterEffectiveValue(genReentrantMethods,%'opt_arg_genReentrantMethods',arg_genReentrantMethods)
  %-
    %if arg_genReentrantMethods == 'yes'
    %- The method itself is generated as reentrant, additional critical sections are not needed
  %elif arg_genReentrantMethods == 'no'
    %- Reentrant methods are not required, but critical sections must remain
    %inclSUB RTOSAdap_getRTOSFunction(ExitCritical,loc_exitCriticalFunctionName)
%{ {%'OperatingSystemId' RTOS Adapter} Critical section ends (RTOS function call is defined by %'OperatingSystemId' RTOS Adapter property) %}
%-EXIT_CRITICAL_FUNCTION_START
%-EXIT_CRITICAL_FUNCTION_END
    %undef loc_exitCriticalFunctionName
  %else
    %error! Invalid value of parameter 'opt_arg_genReentrantMethods' ("%'arg_genReentrantMethods'")
  %endif
  %-
  %undef arg_genReentrantMethods
%SUBROUTINE_END
%-
%-
%------------------------------------------------------------------------------
%SUBROUTINE RTOSAdap_genIoctlCommandConstantDefinition(arg_commandName,arg_commandLogicalIndex,opt_arg_simpleComponentType)
%------------------------------------------------------------------------------
  %- Get constant name
  %inclSUB RTOSAdap_getIoctlCommandConstantName(%'arg_commandName',%'opt_arg_simpleComponentType',loc_constantName)
%-
%-
%------------------------------------------------------------------------------
%SUBROUTINE RTOSAdap_getIoctlCommandConstantName(arg_commandName,opt_arg_simpleComponentType,out_constantName)
%------------------------------------------------------------------------------
  %- Handle optional parameters
  %inclSUB RTOSAdap_priv_getOptionalParameterEffectiveValue(simpleComponentType,%'opt_arg_simpleComponentType',arg_simpleComponentType)
  %-
  %- Check if arg_componentType is a valid enumerated value
  %:loc_simpleCompTypeIndex = %get_index1(%'arg_simpleComponentType',RTOSAdap_enum_simpleComponentTypes)
  %if loc_simpleCompTypeIndex = '-1'
    %error! Invalid HAL simple component type "%'arg_simpleComponentType'"
  %endif
  %-
  %define! %'out_constantName' %'arg_simpleComponentType'_IOCTL_%'arg_commandName'
  %-
  %undef arg_simpleComponentType
  %undef loc_simpleCompTypeIndex
%SUBROUTINE_END
%-
%-
%------------------------------------------------------------------------------
%SUBROUTINE RTOSAdap_genRTOSIncludes()
%------------------------------------------------------------------------------
%SUBROUTINE_END
%-
%-
%------------------------------------------------------------------------------
%SUBROUTINE RTOSAdap_genRTOSDriverIncludes()
%------------------------------------------------------------------------------
%ifdef PE_G_RTOSAdap_regCompInstanceTypes

%endif
%SUBROUTINE_END
%-
%-
%------------------------------------------------------------------------------
%SUBROUTINE RTOSAdap_genPublishedRTOSSettings
%------------------------------------------------------------------------------
  %-
%SUBROUTINE_END
%-
%-
%------------------------------------------------------------------------------
%SUBROUTINE RTOSAdap_genMemoryBarrierBegin(opt_arg_genReentrantMethods)
%------------------------------------------------------------------------------
%SUBROUTINE_END
%-
%-
%------------------------------------------------------------------------------
%SUBROUTINE RTOSAdap_genMemoryBarrierEnd(opt_arg_genReentrantMethods)
%------------------------------------------------------------------------------
%SUBROUTINE_END
%-
%-
%------------------------------------------------------------------------------
%SUBROUTINE RTOSAdap_genReentrantMethodDeclare(opt_arg_genReentrantMethods, spinlock_name)
%------------------------------------------------------------------------------
 %- Do nothing for bareboard
%SUBROUTINE_END
%-
%-
%------------------------------------------------------------------------------
%SUBROUTINE RTOSAdap_genRTOSReturnIRQ_Attended()
%------------------------------------------------------------------------------
%SUBROUTINE_END
%-
%-
%------------------------------------------------------------------------------
%SUBROUTINE RTOSAdap_genRTOSReturnIRQ_NOTAttended()
%------------------------------------------------------------------------------
%SUBROUTINE_END
%-
%-
