%-
%- Implementation of RTOS ADAPTOR for Linux
%-
%- (C) 2009 Freescale, all rights reserved
%- Author: Jaroslav Cernoch
%- Updates for Linux: Oscar Gueta
%-
%------------------------------------------------------------------------------
%- Including common RTOS adapter library.
%----genReentrantMethods--------------------------------------------------------------------------
%- It is included indirectly through symbol as a workaround of limitation
%- of the tool used for creating installation.
%-
%define RTOSAdap_priv_CommonAdapterInterface sw\RTOSAdaptor\Common_RTOSAdaptor.prg
%include %'RTOSAdap_priv_CommonAdapterInterface'
%undef RTOSAdap_priv_CommonAdapterInterface
%-
%-
%------------------------------------------------------------------------------
%- list RTOSAdap_priv_[simple]componentTypeAttribs
%------------------------------------------------------------------------------
%- Contains for each enumerated component type string of format:
%- "|componentType=<componentType>|installFunctionName=<installFunctionName>|"
%- Indices in this list must match the indices within list RTOSAdap_enum_componentTypes.
%-
%define_list RTOSAdap_priv_componentTypeAttribs {
|componentType=HAL_UART_Polling|installFunctionName=_io_serial_polled_install|
|componentType=HAL_UART_Int|installFunctionName=_io_serial_int_install|
|componentType=HAL_I2C_Polling|installFunctionName=_io_i2c_polled_install|
|componentType=HAL_I2C_Int|installFunctionName=_io_i2c_int_install|
|componentType=HAL_GPIO|installFunctionName=_io_gpio_install|
|componentType=HAL_ADC|installFunctionName=_io_adc_install|
%define_list}
%-
%define_list RTOSAdap_priv_simpleComponentTypeAttribs {
|simpleComponentType=HAL_UART|mqxIOType=IO_TYPE_SERIAL|
|simpleComponentType=HAL_I2C|mqxIOType=IO_TYPE_I2C|
|simpleComponentType=HAL_GPIO|mqxIOType=IO_TYPE_GPIO|
|simpleComponentType=HAL_ADC|mqxIOType=IO_TYPE_ADC|
|simpleComponentType=HAL_RTC|mqxIOType=|
|simpleComponentType=HAL_Ethernet|mqxIOType=|
%define_list}
%-
%- UART Polling and interrupt-driven
%define_list RTOSAdap_priv_API_HAL_UART_Polling {
Init
Deinit
RecvChar
SendChar
Ioctl
%define_list}
%-
%define_list RTOSAdap_priv_API_HAL_UART_Int {
Init
Deinit
RecvChar
SendChar
Ioctl
%define_list}
%-
%- I2C Polling and Interrupt-driven
%define_list RTOSAdap_priv_API_HAL_I2C_Polling {
Init
Deinit
RecvBlock
SendBlock
Ioctl
%define_list}
%-
%define_list RTOSAdap_priv_API_HAL_I2C_Int {
Init
Deinit
RecvBlock
SendBlock
Ioctl
%define_list}
%-
%- GPIO
%define_list RTOSAdap_priv_API_HAL_GPIO {
Init
Deinit
Ioctl
%define_list}
%-
%- HAL_ADC has no installation function
%define_list RTOSAdap_priv_API_HAL_ADC {
Init
Deinit
GetMeasuredValues
Ioctl
%define_list}
%-
%- RTC has no installation function
%define_list RTOSAdap_priv_API_HAL_RTC {
%define_list}
%-
%- Ethernet has no installation function
%define_list RTOSAdap_priv_API_HAL_Ethernet {
%define_list}
%-
%- CAN has no installation function
%define_list RTOSAdap_priv_API_HAL_CAN {
%define_list}
%-
%------------------------------------------------------------------------------
%SUBROUTINE RTOSAdap_genRTOSTypeDefinitions
%------------------------------------------------------------------------------
%--  %- PUBLIC TYPES
%--%{ {%'OperatingSystemId' RTOS Adapter} Pointer to the device data structure used and managed by RTOS %}
%--typedef void *LDD_RTOS_TDeviceDataPtr;
%--%{ {%'OperatingSystemId' RTOS Adapter} RTOS specific definition of type of Ioctl() command constants %}
%--typedef unsigned long LDD_RTOS_TIoctlCommand;
%--
  %- PRIVATE TYPES
%{ {%'OperatingSystemId' RTOS Adapter} Type of the parameter passed into ISR from RTOS interrupt dispatcher %}
typedef void *LDD_RTOS_TISRParameter;

%-{ {%'OperatingSystemId' RTOS Adapter} Structure for saving/restoring interrupt vector %}
%-typedef struct {
%-  void (*isrFunction)(LDD_RTOS_TISRParameter);                 %>>%{ ISR function handler %}
%-  LDD_RTOS_TISRParameter isrData;                              %>>%{ ISR parameter %}
%-} LDD_RTOS_TISRVectorSettings;
%SUBROUTINE_END
%-
%-
%------------------------------------------------------------------------------
%SUBROUTINE RTOSAdap_priv_getInstallationFunctionForDriverType(arg_componentType,out_installFunctionName)
%------------------------------------------------------------------------------
%- Private method. Returns the name of RTOS installation function for the specified
%- component type.
%-
%- Input parameters:
%-      arg_componentType       - Type of the component. One from enumeration list 'RTOSAdap_enum_componentTypes'.
%-
%- Outputs:
%-      out_installFunctionName - Name of RTOS function installing the driver of component
%-                                of specified type.
%-
  %- Check if arg_componentType is a valid enumerated value
  %:loc_compTypeIndex = %get_index1(%'arg_componentType',RTOSAdap_enum_componentTypes)
  %if loc_compTypeIndex = '-1'
    %error! Invalid HAL component type "%'arg_componentType'"
  %endif
  %-
  %- In RTOSAdap_priv_componentTypeAttribs there is item of format "<componentType>|<installFunctionName>"
  %- for each component type on the same index as within the list RTOSAdap_enum_componentTypes.
  %define loc_compTypeAttribs %[%'loc_compTypeIndex',RTOSAdap_priv_componentTypeAttribs]
  %inclSUB RTOSAdap_lib_getStructMember(%'loc_compTypeAttribs',componentType,loc_checkCompType,required)
  %inclSUB RTOSAdap_lib_getStructMember(%'loc_compTypeAttribs',installFunctionName,%'out_installFunctionName',required)
  %if loc_checkCompType != arg_componentType
    %error! %'OperatingSystemId' RTOS Adapter internal error, lists RTOSAdap_priv_componentTypeAttribs and RTOSAdap_componentTypes are not in index-consistency (at index %'loc_compTypeIndex' there is "%'loc_checkCompType'" instead of "%'arg_componentType'")
  %endif
  %-
  %undef loc_compTypeIndex
  %undef loc_compTypeAttribs
  %undef loc_checkCompType
%SUBROUTINE_END
%-
%-
%------------------------------------------------------------------------------
%SUBROUTINE RTOSAdap_genDriverInstallationsFunctionDefinition
%------------------------------------------------------------------------------
%- Linux implementation: For each instance of component calls RTOS installation function
%- (specific for each type of component). The name of the component instance is passed as first parameter.
%- Then pointers to all registered method implementations are passed as next parameters
%- (in the same order they have been registered).
%-
%define! Description_PE_Install_Hardware_Drivers The function responsible for installing drivers for all HAL components within the project.
%include Common\GeneralInternalGlobal.inc (PE_Install_Hardware_Drivers)
%{ {%'OperatingSystemId' RTOS Adapter} Definition of main function for installation of drivers for all components %}
void PE_Install_Hardware_Drivers (void)
{
  %- List PE_G_RTOSAdap_installFuns contains all names of lists of lines of installation function calls
  %for loc_instanceName from PE_G_RTOSAdap_regCompInstanceNames
    %- Obtain component type
    %define! loc_componentType %[%'for_index_1',PE_G_RTOSAdap_regCompInstanceTypes]
    %ifdef RTOSAdap_priv_API_%'loc_componentType'
      %if %list_size(RTOSAdap_priv_API_%'loc_componentType') > '0'
        %- If list is empty, no installation function will be called
        %-
        %- Get installation function for arg_componentType
        %inclSUB RTOSAdap_priv_getInstallationFunctionForDriverType(%'loc_componentType',loc_installFunctionName)
        %-
  %{ {%'OperatingSystemId' RTOS Adapter} Call to the driver installation function for component "%'loc_instanceName'" %}
  (void)%'loc_installFunctionName'(
        %- First argument: driver name = component instance name + ':'
    "%'loc_instanceName':", %{ Driver name equals to the name of the component instance %}
        %-
        %- Next arguments are pointers to methods
        %for loc_methodName from RTOSAdap_priv_API_%'loc_componentType'
          %ifdef PE_G_RTOSAdap_regCompInstance_%'loc_instanceName'_methodPtr_%'loc_methodName'
            %if %for_last
    %{ %'loc_methodName' %} %"PE_G_RTOSAdap_regCompInstance_%'loc_instanceName'_methodPtr_%'loc_methodName'"
            %else
    %{ %'loc_methodName' %} %"PE_G_RTOSAdap_regCompInstance_%'loc_instanceName'_methodPtr_%'loc_methodName'",
            %endif
          %else
            %error! Method "%'loc_methodName'" is not registered for HAL component "%'loc_instanceName'" of type "%'loc_componentType'" (check call to RTOSAdap_registerComponentMethodImplementation() from HAL component DRV file)
          %endif
        %endfor  %- loc_methodName
        %- Add closing ")" to the installation function call
  );
      %endif %- list_size(RTOSAdap_priv_API_%'loc_componentType')>'0'
    %else
      %error! API for HAL component of type "%'loc_componentType'" is not defined. Check definition of list RTOSAdap_priv_API_%'loc_componentType' within %'OperatingSystemId' RTOS Adapter.
    %endif
  %endfor %- loc_instanceName
  %-
  %undef! loc_componentType
  %undef! loc_installFunctionName
}
%SUBROUTINE_END
%-
%-
%------------------------------------------------------------------------------
%SUBROUTINE RTOSAdap_genDriverInstallationsFunctionDeclaration
%------------------------------------------------------------------------------
%include Common\GeneralInternalGlobal.inc (PE_Install_Hardware_Drivers)
%{ {%'OperatingSystemId' RTOS Adapter} Prototype of main function for installation of drivers of all components %}
void PE_Install_Hardware_Drivers (void);
%SUBROUTINE_END
%-
%-
%-
%-
%- =============================================================================
%-
%-        MEMORY ALLOCATION
%-
%- =============================================================================
%-
%-
%-
%------------------------------------------------------------------------------
%SUBROUTINE RTOSAdap_genDriverMemoryAlloc(arg_destPtrBuffer,arg_objType,opt_arg_errCode,arg_globDefsThread)
%------------------------------------------------------------------------------
  %- Check if RTOSAdap_driverMemoryAlloc() was called only once for this object
  %if defined(RTOSAdap_alloc_object_%'arg_destPtrBuffer')
    %error! There was called allocation for object "%'arg_destPtrBuffer'" for object type "%'arg_objType'" twice (which does not work for bareboard RTOS what only simulates the dynamic allocation)
  %else
    %- o.k., it is first call, mark that such object was allocated
    %define RTOSAdap_alloc_object_%'arg_destPtrBuffer'
  %endif
  %-
  %- Dynamic allocation is supported (generate it)
  %inclSUB RTOSAdap_getRTOSFunction(Malloc,loc_MallocFunction)
%{ {%'OperatingSystemId' RTOS Adapter} Driver memory allocation: RTOS function call is defined by %'OperatingSystemId' RTOS Adapter property %}
%'arg_destPtrBuffer' = (%'arg_objType' *)%'loc_MallocFunction'(sizeof(%'arg_objType'), GFP_KERNEL);
  %if opt_arg_errCode<>''
#if Linux_CHECK_MEMORY_ALLOCATION_ERRORS
  if (%'arg_destPtrBuffer' == NULL) {
    %'opt_arg_errCode'
  }
#endif
  %endif
  %-
  %- Set memory block type (not supported now)
%-_mem_set_type(%'arg_destPtrBuffer', MEM_TYPE_...);
  %-
  %undef loc_MallocFunction
%SUBROUTINE_END
%-
%-
%------------------------------------------------------------------------------
%SUBROUTINE RTOSAdap_genDriverMemoryDealloc(arg_ptrBuffer,arg_objType)
%------------------------------------------------------------------------------
  %- Dynamic allocation is supported, generate it
  %inclSUB RTOSAdap_getRTOSFunction(Dealloc,loc_DeallocFunction)
%{ {%'OperatingSystemId' RTOS Adapter} Driver memory deallocation: RTOS function call is defined by %'OperatingSystemId' RTOS Adapter property %}
%'loc_DeallocFunction'(%'arg_ptrBuffer');
  %-
  %undef loc_DeallocFunction
%SUBROUTINE_END
%-
%-
%-
%- =============================================================================
%-
%-        INTERRUPT SERVICE ROUTINES
%-
%- =============================================================================
%-
%-
%-
%------------------------------------------------------------------------------
%SUBROUTINE RTOSAdap_getInterruptVectorSymbol(arg_vectorName,out_vectorIndexConstantName)
%------------------------------------------------------------------------------
  %define! %'out_vectorIndexConstantName' LDD_ivIndex_%'arg_vectorName'
%SUBROUTINE_END
%-
%-
%------------------------------------------------------------------------------
%SUBROUTINE RTOSAdap_genSetInterruptVector(arg_intVectorProperty,arg_isrFunctionName,arg_isrParameterType,arg_isrParameterValue,opt_arg_oldISRSettings,arg_globDefsThread)
%------------------------------------------------------------------------------
%- Linux 'arg_isrParameterType' limitation: Parameter type should be convertible to 'void *' type.
%-
  %- Check if RTOSAdap_genSetInterruptVector() was called only once for this interrupt vector
  %if defined(RTOSAdap_alloc_interrupt_%"%'arg_intVectorProperty'_Name")
    %error! There was called allocation for interrupt vector %"%'arg_intVectorProperty'_Name" twice (which does not work for bareboard RTOS what only simulates the run-time vector allocation)
  %else
    %- o.k., it is first call, mark that interrupt vector was allocated
    %define RTOSAdap_alloc_interrupt_%"%'arg_intVectorProperty'_Name"
  %endif
  %-
  %- Linux has support of run-time installable ISR vectors
  %-
  %define! loc_vectorIndexConstantName
  %inclSUB RTOSAdap_getInterruptVectorSymbol(%"%'arg_intVectorProperty'_Name",loc_vectorIndexConstantName)
  %-
  %-if opt_arg_oldISRSettings != ''
%-{ {%'OperatingSystemId' RTOS Adapter} Save old and set new interrupt vector (function handler and ISR parameter) %}
%-{ Note: Exception handler for interrupt is not saved, because it is not modified %}
%-opt_arg_oldISRSettings.isrData = _int_get_isr_data(%loc_vectorIndexConstantName);
%-opt_arg_oldISRSettings.isrFunction = _int_install_isr(%loc_vectorIndexConstantName, %arg_isrFunctionName, %arg_isrParameterValue);
%-  %else
%{ {%'OperatingSystemId' RTOS Adapter} Set new interrupt vector (function handler and ISR parameter) %}
if (request_irq(%loc_vectorIndexConstantName, %arg_isrFunctionName, IRQF_SHARED, "%arg_isrFunctionName", %arg_isrParameterValue))
        panic("request_irq() failed");
%-  %endif
  %-
  %undef loc_vectorIndexConstantName
%SUBROUTINE_END
%-
%-
%------------------------------------------------------------------------------
%SUBROUTINE RTOSAdap_genRestoreInterruptVector(arg_intVectorProperty,arg_oldISRSettings)
%------------------------------------------------------------------------------
  %define! loc_vectorIndexConstantName
  %-inclSUB RTOSAdap_getInterruptVectorSymbol(%arg_intVectorProperty,loc_vectorIndexConstantName)
  %inclSUB RTOSAdap_getInterruptVectorSymbol(%"%'arg_intVectorProperty'_Name",loc_vectorIndexConstantName)
  %-
%{ {%'OperatingSystemId' RTOS Adapter} Restore interrupt vector (function handler and ISR parameter) %}
%{ Note: Exception handler for interrupt is not restored, because it was not modified %}
%-(void)_int_install_isr(%loc_vectorIndexConstantName, %arg_oldISRSettings.isrFunction, %arg_oldISRSettings.isrData);
free_irq(%loc_vectorIndexConstantName, NULL);
  %-
  %undef loc_vectorIndexConstantName
%SUBROUTINE_END
%-
%-
%------------------------------------------------------------------------------
%SUBROUTINE RTOSAdap_genISRSettingsVarDeclaration(arg_varName,opt_arg_comment)
%------------------------------------------------------------------------------
  %if opt_arg_comment != ''
%-LDD_RTOS_TISRVectorSettings %'arg_varName';                    %>>%{ {%'OperatingSystemId' RTOS Adapter} %'opt_arg_comment' %}
  %else
%-LDD_RTOS_TISRVectorSettings %'arg_varName';                    %>>%{ {%'OperatingSystemId' RTOS Adapter} Buffer where old interrupt settings are saved %}
  %endif
%SUBROUTINE_END
%-
%-
%------------------------------------------------------------------------------
%SUBROUTINE RTOSAdap_genISRFunctionDefinitionOpen(arg_intVectorProperty,arg_isrFunctionName,arg_isrParameterType,arg_isrParameterName)
%------------------------------------------------------------------------------
%- Linux 'arg_isrParameterType' limitation: Parameter type should be convertible to 'void *' type.
%-
static irqreturn_t %'arg_isrFunctionName'(int irq, LDD_RTOS_TISRParameter _isrParameter)
%-static irqreturn_t %'arg_isrFunctionName'(int irq, void * _isrParameter)
{
  %{ {%'OperatingSystemId' RTOS Adapter} ISR parameter is passed as parameter from RTOS interrupt dispatcher %}
  %'arg_isrParameterType' %'arg_isrParameterName' = (%'arg_isrParameterType')_isrParameter;
%SUBROUTINE_END
%-
%-
%------------------------------------------------------------------------------
%SUBROUTINE RTOSAdap_genISRFunctionDefinitionClose(arg_intVectorProperty,arg_isrFunctionName,arg_isrParameterType,arg_isrParameterName)
%------------------------------------------------------------------------------
}
%SUBROUTINE_END
%-
%-
%------------------------------------------------------------------------------
%SUBROUTINE RTOSAdap_genISRFunctionDeclaration(arg_intVectorProperty,arg_isrFunctionName,arg_isrParameterType,arg_isrParameterName)
%------------------------------------------------------------------------------
%- Linux 'arg_isrParameterType' limitation: Parameter type should be convertible to 'void *' type.
%-
%{ {%'OperatingSystemId' RTOS Adapter} ISR function prototype %}
static irqreturn_t %'arg_isrFunctionName'(int irq, LDD_RTOS_TISRParameter _isrParameter);
%-static irqreturn_t %'arg_isrFunctionName'(int irq, void * _isrParameter);
%SUBROUTINE_END
%-
%-
%-
%- =============================================================================
%-
%-        CRITICAL SECTIONS:
%-               spinlocks
%-
%- =============================================================================
%-
%-
%-
%------------------------------------------------------------------------------
%SUBROUTINE RTOSAdap_genReentrantMethodDeclare(opt_arg_genReentrantMethods, spinlock_name)
%------------------------------------------------------------------------------
  %- Handle optional parameter
  %inclSUB RTOSAdap_priv_getOptionalParameterEffectiveValue(genReentrantMethods,%'opt_arg_genReentrantMethods',arg_genReentrantMethods)
  %-
  %if arg_genReentrantMethods == 'yes'
    %- Reentrant methods are required, create the spinlock
%-    %inclSUB RTOSAdap_getRTOSFunction(SoftInitCritical,loc_initSoftCriticalFunctionName)
    %- globaly declare the spin lock name, will be accessed later on spinlocks conde genration routines
    %inclSUB RTOSAdap_setDefaultParameter(SPIN_LOCK,%'spinlock_name')
%{ {%'OperatingSystemId' RTOS Adapter} Method is required to be reentrant, spin_lock is declared %}
static spinlock_t %'spinlock_name' = SPIN_LOCK_UNLOCKED;
static unsigned long %'spinlock_name'_flags;
%-'loc_initSoftCriticalFunctionName'();
    %-undef loc_initSoftCriticalFunctionName
  %elif arg_genReentrantMethods == 'no'
    %- Reentrant methods are not required, no code is generated
  %else
    %error! Invalid value of parameter 'opt_arg_genReentrantMethods' ("%'arg_genReentrantMethods'")
  %endif
  %-
  %undef arg_genReentrantMethods
%SUBROUTINE_END
%-
%-
%------------------------------------------------------------------------------
%SUBROUTINE RTOSAdap_genReentrantMethodBegin(opt_arg_genReentrantMethods)
%------------------------------------------------------------------------------
  %- Handle optional parameter
  %inclSUB RTOSAdap_priv_getOptionalParameterEffectiveValue(genReentrantMethods,%'opt_arg_genReentrantMethods',arg_genReentrantMethods)
  %-
  %if arg_genReentrantMethods == 'yes'
    %- Reentrant methods are required, generate enter critical section
    %inclSUB RTOSAdap_getRTOSFunction(SoftEnterCritical,loc_softEnterCriticalFunctionName)
    %inclSUB RTOSAdap_priv_getOptionalParameterEffectiveValue(SPIN_LOCK,%'opt_arg_genReentrantMethods',arg_genSpinLock)
%{ {%'OperatingSystemId' RTOS Adapter} Method is required to be reentrant, critical section begins (RTOS function call is defined by %'OperatingSystemId' RTOS Adapter property) %}
%'loc_softEnterCriticalFunctionName'(&%'arg_genSpinlock',%'arg_genSpinlock'_flags);
    %undef loc_softEnterCriticalFunctionName
  %elif arg_genReentrantMethods == 'no'
    %- Reentrant methods are not required, no code is generated
  %else
    %error! Invalid value of parameter 'opt_arg_genReentrantMethods' ("%'arg_genReentrantMethods'")
  %endif
  %-
  %undef arg_genReentrantMethods
%SUBROUTINE_END
%-
%-
%------------------------------------------------------------------------------
%SUBROUTINE RTOSAdap_genReentrantMethodEnd(opt_arg_genReentrantMethods)
%------------------------------------------------------------------------------
  %- Handle optional parameter
  %inclSUB RTOSAdap_priv_getOptionalParameterEffectiveValue(genReentrantMethods,%'opt_arg_genReentrantMethods',arg_genReentrantMethods)
  %-
  %if arg_genReentrantMethods == 'yes'
    %- Reentrant methods are required, generate exit critical section
    %inclSUB RTOSAdap_getRTOSFunction(SoftExitCritical,loc_softExitCriticalFunctionName)
    %inclSUB RTOSAdap_priv_getOptionalParameterEffectiveValue(SPIN_LOCK,%'opt_arg_genReentrantMethods',arg_genSpinLock)
%{ {%'OperatingSystemId' RTOS Adapter} Critical section end (RTOS function call is defined by %'OperatingSystemId' RTOS Adapter property) %}
%'loc_softExitCriticalFunctionName'(&%'arg_genSpinlock',%'arg_genSpinlock'_flags);
    %undef loc_softExitCriticalFunctionName
  %elif arg_genReentrantMethods == 'no'
    %- Reentrant methods are not required, no code is generated
  %else
    %error! Invalid value of parameter 'opt_arg_genReentrantMethods' ("%'arg_genReentrantMethods'")
  %endif
  %-
  %undef arg_genReentrantMethods
%SUBROUTINE_END
%-
%-
%-
%- =============================================================================
%-
%-        CRITICAL SECTIONS:
%-               sti/cli (enable/disable interrupts) and/or spinlocks
%-
%- =============================================================================
%-
%-
%-
%------------------------------------------------------------------------------
%SUBROUTINE RTOSAdap_genCriticalSectionBegin(opt_arg_genReentrantMethods)
%------------------------------------------------------------------------------
  %- Handle optional parameter
  %inclSUB RTOSAdap_priv_getOptionalParameterEffectiveValue(genCriticalSectionMethods,%'opt_arg_genReentrantMethods',arg_genReentrantMethods)
  %-
  %- disable interrupts
  %if arg_genReentrantMethods == 'yes'
    %- The method itself is generated as reentrant, additional critical sections are not needed
  %elif arg_genReentrantMethods == 'no'
    %- Reentrant methods are not required, but critical sections must remain
    %inclSUB RTOSAdap_getRTOSFunction(EnterCritical,loc_enterCriticalFunctionName)
%{ {%'OperatingSystemId' RTOS Adapter} Critical section begin (RTOS function call is defined by %'OperatingSystemId' RTOS Adapter property) %}
%'loc_enterCriticalFunctionName'();
    %undef loc_enterCriticalFunctionName
  %else
    %error! Invalid value of parameter 'opt_arg_genReentrantMethods' ("%'arg_genReentrantMethods'")
  %endif
  %-
  %undef arg_genReentrantMethods
%SUBROUTINE_END
%-
%-
%------------------------------------------------------------------------------
%SUBROUTINE RTOSAdap_genCriticalSectionEnd(opt_arg_genReentrantMethods)
%------------------------------------------------------------------------------
  %- Handle optional parameter
  %inclSUB RTOSAdap_priv_getOptionalParameterEffectiveValue(genCriticalSectionMethods,%'opt_arg_genReentrantMethods',arg_genReentrantMethods)
  %-
  %-spinlocks
  %if arg_genReentrantMethods == 'no'
    %- Reentrant methods are not required, generate exit critical section
    %inclSUB RTOSAdap_getRTOSFunction(ExitCritical,loc_softExitCriticalFunctionName)
%{ {%'OperatingSystemId' RTOS Adapter} Critical section end (RTOS function call is defined by %'OperatingSystemId' RTOS Adapter property) %}
%'loc_softExitCriticalFunctionName'();
    %undef loc_softExitCriticalFunctionName
  %elif arg_genReentrantMethods == 'yes'
    %- Reentrant methods are not required, no code is generated
  %else
    %error! Invalid value of parameter 'opt_arg_genReentrantMethods' ("%'arg_genReentrantMethods'")
  %endif
  %-
  %undef arg_genReentrantMethods
%SUBROUTINE_END
%-
%-
%-
%- =============================================================================
%-
%-        MEMORY BARRIERS
%-
%- =============================================================================
%-
%-
%-
%------------------------------------------------------------------------------
%SUBROUTINE RTOSAdap_genMemoryBarrierBegin(opt_arg_genReentrantMethods)
%------------------------------------------------------------------------------
  %- Handle optional parameter
  %inclSUB RTOSAdap_priv_getOptionalParameterEffectiveValue(genMemoryBarrierMethods,%'opt_arg_genReentrantMethods',arg_genReentrantMethods)
  %-
  %if arg_genReentrantMethods == 'yes'
    %- Reentrant methods are not required, but critical sections must remain
    %inclSUB RTOSAdap_getRTOSFunction(MemoryBarrierStart,loc_MemoryBarrierFunctionName)
%{ {%'OperatingSystemId' RTOS Adapter} Critical section begin (RTOS function call is defined by %'OperatingSystemId' RTOS Adapter property) %}
%'loc_memoryBarrierFunctionName'();
    %undef loc_memoryBarrierFunctionName
  %else
    %error! Invalid value of parameter 'opt_arg_genReentrantMethods' ("%'arg_genReentrantMethods'")
  %endif
  %-
  %undef arg_genReentrantMethods
%SUBROUTINE_END
%-
%-
%------------------------------------------------------------------------------
%SUBROUTINE RTOSAdap_genMemoryBarrierEnd(opt_arg_genReentrantMethods)
%------------------------------------------------------------------------------
  %- Handle optional parameter
  %inclSUB RTOSAdap_priv_getOptionalParameterEffectiveValue(genMemoryBarrierMethods,%'opt_arg_genReentrantMethods',arg_genReentrantMethods)
  %-
  %if arg_genReentrantMethods == 'yes'
    %- Reentrant methods are not required, but critical sections must remain
    %inclSUB RTOSAdap_getRTOSFunction(MemoryBarrierEnd,loc_exitCriticalFunctionName)
%{ {%'OperatingSystemId' RTOS Adapter} Critical section ends (RTOS function call is defined by %'OperatingSystemId' RTOS Adapter property) %}
%'loc_exitCriticalFunctionName'();
    %undef loc_exitCriticalFunctionName
  %else
    %error! Invalid value of parameter 'opt_arg_genReentrantMethods' ("%'arg_genReentrantMethods'")
  %endif
  %-
  %undef arg_genReentrantMethods
%SUBROUTINE_END
%-
%-
%-
%- =============================================================================
%-
%-        INCLUDES
%-
%- =============================================================================
%-
%-
%-
%------------------------------------------------------------------------------
%SUBROUTINE RTOSAdap_genRTOSIncludes()
%------------------------------------------------------------------------------
//#include <linux.h>
#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/spinlock.h>
#include <linux/interrupt.h>
#include "%'CPUVariant'.h"
%SUBROUTINE_END
%-
%-
%------------------------------------------------------------------------------
%SUBROUTINE RTOSAdap_genRTOSReturnIRQ_Attended()
%------------------------------------------------------------------------------
return IRQ_HANDLED;
%SUBROUTINE_END
%-
%-
%------------------------------------------------------------------------------
%SUBROUTINE RTOSAdap_genRTOSReturnIRQ_NOTAttended()
%------------------------------------------------------------------------------
return IRQ_NONE;
%SUBROUTINE_END
%-
%-
%------------------------------------------------------------------------------
%SUBROUTINE RTOSAdap_genRTOSDriverIncludes()
%------------------------------------------------------------------------------
%ifdef PE_G_RTOSAdap_regCompInstanceTypes
  %if ('HAL_UART_Polling' in PE_G_RTOSAdap_regCompInstanceTypes) || ('HAL_UART_Int' in PE_G_RTOSAdap_regCompInstanceTypes)
#include "serial.h"
  %endif
%endif
%SUBROUTINE_END
%-
%-
%------------------------------------------------------------------------------
%SUBROUTINE RTOSAdap_genPublishedRTOSSettings
%------------------------------------------------------------------------------
  %inclSUB RTOSAdap_getRTOSProperty(DefaultSerialChannel,loc_DefaultSerialChannel)
  %if loc_DefaultSerialChannel != 'undef'
%{ {%'OperatingSystemId' RTOS Adapter} Default serial communication channel (it is defined by RTOS adapter property) %}
#define RTOS_DEFAULT_IO  "%'loc_DefaultSerialChannel':"
  %endif
  %-
  %undef loc_DefaultSerialChannel
%SUBROUTINE_END
%-
%-