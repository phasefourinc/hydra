%-
%- Implementation of RTOS ADAPTOR for MQX Lite
%-
%- (C) 2009 Freescale, all rights reserved
%-
%------------------------------------------------------------------------------
%- Including common RTOS adapter library.
%------------------------------------------------------------------------------
%- It is included indirectly through symbol as a workaround of limitation
%- of the tool used for creating installation.
%-
%define RTOSAdap_priv_CommonAdapterInterface sw\RTOSAdaptor\Common_RTOSAdaptor.prg
%include %'RTOSAdap_priv_CommonAdapterInterface'
%undef RTOSAdap_priv_CommonAdapterInterface
%-
%include Common\EBGNSupport\StringLib.prg
%-
%------------------------------------------------------------------------------
%SUBROUTINE RTOSAdap_genRTOSTypeDefinitions
%------------------------------------------------------------------------------
  %- PUBLIC TYPES
  %-
  %-
  %- PRIVATE TYPES
%{! {%'OperatingSystemId' RTOS Adapter} Type of the parameter passed into ISR from RTOS interrupt dispatcher %}
typedef void *LDD_RTOS_TISRParameter;

%{! {%'OperatingSystemId' RTOS Adapter} Structure for saving/restoring interrupt vector %}
typedef struct {
  void (*isrFunction)(LDD_RTOS_TISRParameter);                   %>>%{!< ISR function handler %}
  LDD_RTOS_TISRParameter isrData;                                %>>%{!< ISR parameter %}
} LDD_RTOS_TISRVectorSettings;
%SUBROUTINE_END
%-
%-
%------------------------------------------------------------------------------
%SUBROUTINE RTOSAdap_priv_getInstallationFunctionForDriverType(arg_componentType,out_installFunctionName)
%------------------------------------------------------------------------------
%- For MQX Lite RTOS no driver installations are generated.
%SUBROUTINE_END
%-
%-
%------------------------------------------------------------------------------
%SUBROUTINE RTOSAdap_genDriverInstallationsFunctionDeclaration
%------------------------------------------------------------------------------
%- For bare-board RTOS no driver installations are generated.
%-
%{ {%'OperatingSystemId' RTOS Adapter} Function PE_LDD_driver_installations() declaration: It is not generated %}
%SUBROUTINE_END
%-
%-
%------------------------------------------------------------------------------
%SUBROUTINE RTOSAdap_genDriverMemoryAlloc(arg_destPtrBuffer,arg_objType,opt_arg_errCode,arg_globDefsThread,opt_arg_MemoryAllocParams)
%------------------------------------------------------------------------------
  %-
  %inclSUB RTOSAdap_priv_getOptionalParameterEffectiveValue(componentInstanceName,,loc_componentInstanceName)
  %-
  %- Check if RTOSAdap_driverMemoryAlloc() was called only once for this object
  %if defined(RTOSAdap_alloc_object_%'arg_destPtrBuffer')
    %error! There was called allocation for object "%'arg_destPtrBuffer'" for object type "%'arg_objType'" twice (which does not work when the dynamic allocation is simulated only)
  %else
    %- o.k., it is first call, mark that such object was allocated
    %define RTOSAdap_alloc_object_%'arg_destPtrBuffer'
  %endif
  %-
  %THREAD %'arg_globDefsThread' SELECT
%>1%{ {%'OperatingSystemId' RTOS Adapter} Static object used for simulation of dynamic driver memory allocation %}
  %THREAD %'arg_globDefsThread' UNSELECT
  %-
  %define! loc_ObjDefPrefix
  %define! loc_ObjDefSuffix
  %-
  %------------------------------------------------------------------------------
  %- Processing of optional parameters
  %------------------------------------------------------------------------------
  %inclSUB RTOSAdap_lib_getStructMember(%'opt_arg_MemoryAllocParams',ALIGN,loc_Param_Align,optional)
  %inclSUB RTOSAdap_lib_getStructMember(%'opt_arg_MemoryAllocParams',ZERO,loc_Param_Zero,optional)
  %------------------------------------------------------------------------------
  %- Processing of ALIGN optional parameter
  %------------------------------------------------------------------------------
  %if loc_Param_Align != 'undef'
    %if loc_Param_Align != ''
      %- Add new data section to linker file
      %if (Compiler = 'CodeWarriorARM') | (Compiler = 'CodeWarriorMCF')
        %add PE_G_LCF_DATA_SECTION .%'loc_componentInstanceName'_memory_section
        %add PE_G_LCF_DATA_ALIGN %loc_Param_Align
        %define! loc_ObjDefPrefix __declspec(%'loc_componentInstanceName'_memory_section)%_space_
      %elif (Compiler = 'IARARM')
        %add PE_G_LCF_DATA_SECTION %'loc_componentInstanceName'_memory_section
        %add PE_G_LCF_DATA_ALIGN %loc_Param_Align
      %elif (Compiler = 'IARMCF')
        %- No linker file modifications needed.
      %elif (Compiler = 'GNUC')
        %- No linker file modifications needed.
      %elif (Compiler = 'ARM_CC')
        %- No linker file modifications needed.
      %else
        %error! RTOS adapter: Unsupported compiler to allign allocated memory!
      %endif
      %THREAD %'arg_globDefsThread' SELECT
      %if (Compiler = 'CodeWarriorARM') | (Compiler = 'CodeWarriorMCF')
%>1%{ This pragma aligns an object to %loc_Param_Align bytes boundary. %}
%>1#pragma define_section %'loc_componentInstanceName'_memory_section ".%'loc_componentInstanceName'_memory_section" far_abs RW
      %elif (Compiler = 'IARARM')
%>1%{ This pragma aligns an object to %loc_Param_Align bytes boundary. %}
%>1#pragma location=".%'loc_componentInstanceName'_memory_section"
      %elif (Compiler = 'IARMCF')
%>1%{ This pragma aligns an object to %loc_Param_Align bytes boundary. %}
%>1#pragma data_alignment=%loc_Param_Align
      %elif (Compiler = 'GNUC')
        %define! loc_ObjDefSuffix %'_space_'__attribute__ ((aligned (%'loc_Param_Align')))
      %elif (Compiler = 'ARM_CC')
        %define! loc_ObjDefSuffix %'_space_'__attribute__ ((aligned (%'loc_Param_Align')))
      %endif
      %THREAD %'arg_globDefsThread' UNSELECT
    %else
      %error! Parameter ALIGN has no value
    %endif
    %undef loc_Param_Align
  %endif
  %-
  %- Generate defintion of static object
  %define loc_staticObjName %'arg_destPtrBuffer'__DEFAULT_RTOS_ALLOC
  %THREAD %'arg_globDefsThread' SELECT
%>1%'loc_ObjDefPrefix'static %'arg_objType' %'loc_staticObjName'%'loc_ObjDefSuffix';
  %THREAD %'arg_globDefsThread' UNSELECT
  %-
%{ {%'OperatingSystemId' RTOS Adapter} Driver memory allocation: Dynamic allocation is simulated by a pointer to the static object %}
%'arg_destPtrBuffer' = &%'loc_staticObjName';
  %------------------------------------------------------------------------------
  %- Processing of ZERO optional parameter
  %------------------------------------------------------------------------------
  %if loc_Param_Zero != 'undef'
%{ {%'OperatingSystemId' RTOS Adapter} Driver memory allocation: Fill the allocated memory by zero value %}
PE_FillMemory(%'arg_destPtrBuffer', 0U, sizeof(%'arg_objType'));
    %undef loc_Param_Zero
  %endif
  %-
  %undef loc_staticObjName
  %undef loc_ObjDefPrefix
%SUBROUTINE_END
%-
%-
%------------------------------------------------------------------------------
%SUBROUTINE RTOSAdap_genDriverMemoryDealloc(arg_ptrBuffer,arg_objType)
%------------------------------------------------------------------------------
  %- Bare-board RTOS is simulating dynamic allocation by static allocation.
  %- Allocation is only simulated, so deallocation has no effect.
%{ {%'OperatingSystemId' RTOS Adapter} Driver memory deallocation: Dynamic allocation is simulated, no deallocation code is generated %}
%SUBROUTINE_END
%-
%-
%------------------------------------------------------------------------------
%SUBROUTINE RTOSAdap_getInterruptVectorSymbol(arg_vectorName,out_vectorIndexConstantName)
%------------------------------------------------------------------------------
  %define! %'out_vectorIndexConstantName' LDD_ivIndex_%'arg_vectorName'
%SUBROUTINE_END
%-
%-
%------------------------------------------------------------------------------
%SUBROUTINE RTOSAdap_genSetInterruptVector(arg_intVectorProperty,arg_isrFunctionName,arg_isrParameterType,arg_isrParameterValue,opt_arg_oldISRSettings,arg_globDefsThread)
%------------------------------------------------------------------------------
%- MQX 'arg_isrParameterType' limitation: Parameter type should be convertible to 'void *' type.
%-
  %:tmpFirstUserIntIdx = 15
  %-
  %- Check if RTOSAdap_genSetInterruptVector() was called only once for this interrupt vector
  %if defined(RTOSAdap_alloc_interrupt_%"%'arg_intVectorProperty'_Name")
    %error! There was called allocation for interrupt vector %"%'arg_intVectorProperty'_Name" twice (which does not work for bareboard RTOS what only simulates the run-time vector allocation)
  %else
    %- o.k., it is first call, mark that interrupt vector was allocated
    %define RTOSAdap_alloc_interrupt_%"%'arg_intVectorProperty'_Name"
  %endif
  %-
  %- Get component instance name
  %inclSUB RTOSAdap_priv_getOptionalParameterEffectiveValue(componentInstanceName,,arg_componentInstanceName)
  %-
  %- MQX has support of run-time installable ISR vectors
  %-
  %define! loc_vectorIndexConstantName
  %inclSUB RTOSAdap_getInterruptVectorSymbol(%"%'arg_intVectorProperty'_Name",loc_vectorIndexConstantName)
  %-
  %if (%"%'arg_intVectorProperty'Shared" = 'no')
    %define! loc_isrFunctionName %arg_isrFunctionName
    %define! loc_isrParameterValue %arg_isrParameterValue
    %if (%"%'arg_intVectorProperty'SpecInterruptVectorNumberAddr" <. %tmpFirstUserIntIdx)
      %- System interrupts are installed directly to the vector table. Not neccessary to install ISR through MQX.
      %define_prj iv%"%'arg_intVectorProperty'_Name" %'arg_isrFunctionName'
    %endif
  %else
    %define! loc_isrFunctionName %'ProcessorModule'_iv%"%'arg_intVectorProperty'_Name"
    %define! loc_isrParameterValue NULL
    %append iv%"%'arg_intVectorProperty'_Name"procParams PE_LDD_GetDeviceStructure(PE_LDD_COMPONENT_%'arg_componentInstanceName'_ID)
  %endif
  %-
  %if opt_arg_oldISRSettings != ''
%{ {%'OperatingSystemId' RTOS Adapter} Save old and set new interrupt vector (function handler and ISR parameter) %}
%{ Note: Exception handler for interrupt is not saved, because it is not modified %}
    %if (%"%'arg_intVectorProperty'SpecInterruptVectorNumberAddr" >=. %tmpFirstUserIntIdx)
      %- User interrupts are install through the MQX standard ISR installation mechanism
%opt_arg_oldISRSettings.isrData = _int_get_isr_data(%loc_vectorIndexConstantName);
%opt_arg_oldISRSettings.isrFunction = _int_install_isr(%loc_vectorIndexConstantName, %loc_isrFunctionName, %loc_isrParameterValue);
    %else
      %- System interrupts are installed directly to the vector table. Not neccessary to install ISR through MQX.
%--%opt_arg_oldISRSettings.isrFunction = _int_install_kernel_isr(%loc_vectorIndexConstantName, %loc_isrFunctionName);
    %endif
  %else
%{ {%'OperatingSystemId' RTOS Adapter} Set new interrupt vector (function handler and ISR parameter) %}
    %if (%"%'arg_intVectorProperty'SpecInterruptVectorNumberAddr" >=. %tmpFirstUserIntIdx)
      %- User interrupts are install through the MQX standard ISR installation mechanism
(void)_int_install_isr(%loc_vectorIndexConstantName, %loc_isrFunctionName, %loc_isrParameterValue);
    %else
      %- System interrupts are installed directly to the vector table. Not neccessary to install ISR through MQX.
%--(void)_int_install_kernel_isr(%loc_vectorIndexConstantName, %loc_isrFunctionName);
    %endif
  %endif
  %-
  %undef loc_vectorIndexConstantName
  %undef loc_isrFunctionName
  %undef loc_isrParameterValue
  %undef arg_componentInstanceName
%SUBROUTINE_END
%-
%-
%------------------------------------------------------------------------------
%SUBROUTINE RTOSAdap_genRestoreInterruptVector(arg_intVectorProperty,arg_oldISRSettings)
%------------------------------------------------------------------------------
  %:tmpFirstUserIntIdx = 15
  %-
  %if %str_pos('@',arg_intVectorProperty) != '0'
    %inclSUB ExplodeStr(%'arg_intVectorProperty',loc_OutList,@)
    %define loc_IntVectName %get(%'arg_intVectorProperty',SymbolValue[%[2,loc_OutList]])
    %undef loc_OutList
  %else
    %define loc_IntVectName %"%'arg_intVectorProperty'_Name"
  %endif
  %inclSUB RTOSAdap_getInterruptVectorSymbol(%loc_IntVectName,loc_vectorIndexConstantName)
  %-
%{ {%'OperatingSystemId' RTOS Adapter} Restore interrupt vector (function handler and ISR parameter) %}
%{ Note: Exception handler for interrupt is not restored, because it was not modified %}
    %if (%"%'arg_intVectorProperty'SpecInterruptVectorNumberAddr" >=. %tmpFirstUserIntIdx)
      %- User interrupts install through the MQX standard ISR installation mechanism
(void)_int_install_isr(%loc_vectorIndexConstantName, %arg_oldISRSettings.isrFunction, %arg_oldISRSettings.isrData);
    %else
      %- System interrupts are installed directly to the vector table. Not neccessary to install ISR through MQX.
%--(void)_int_install_kernel_isr(%loc_vectorIndexConstantName, %arg_oldISRSettings.isrFunction, %arg_oldISRSettings.isrData);
    %endif
  %-
  %undef loc_vectorIndexConstantName
  %undef loc_IntVectName
%SUBROUTINE_END
%-
%-
%------------------------------------------------------------------------------
%SUBROUTINE RTOSAdap_genISRSettingsVarDeclaration(arg_varName,opt_arg_comment)
%------------------------------------------------------------------------------
  %if opt_arg_comment != ''
LDD_RTOS_TISRVectorSettings %'arg_varName';                      %>>%{ {%'OperatingSystemId' RTOS Adapter} %'opt_arg_comment' %}
  %else
LDD_RTOS_TISRVectorSettings %'arg_varName';                      %>>%{ {%'OperatingSystemId' RTOS Adapter} Buffer where old interrupt settings are saved %}
  %endif
%SUBROUTINE_END
%-
%-
%------------------------------------------------------------------------------
%SUBROUTINE RTOSAdap_genISRFunctionDefinitionOpen(arg_intVectorProperty,arg_isrFunctionName,arg_isrParameterType,arg_isrParameterName)
%------------------------------------------------------------------------------
%- MQX 'arg_isrParameterType' limitation: Parameter type should be convertible to 'void *' type.
%-
  %:tmpFirstUserIntIdx = 15
  %-
    %if %'arg_intVectorProperty' = '$SHARED'
    %- Shared interrupts will always install through the MQX standard ISR installation mechanism
void %'arg_isrFunctionName'(LDD_RTOS_TISRParameter _isrParameter)
      %define! StandardISRMechanism
    %elif (%"%'arg_intVectorProperty'SpecInterruptVectorNumberAddr" >=. %tmpFirstUserIntIdx)
      %- User interrupts install through the MQX standard ISR installation mechanism
void %'arg_isrFunctionName'(LDD_RTOS_TISRParameter _isrParameter)
      %define! StandardISRMechanism
    %else
      %- System interrupts are installed through the MQX kernel ISR installation mechanism
void %'arg_isrFunctionName'(void)
    %endif
{
  %if (arg_isrParameterName != '' & defined(StandardISRMechanism))
  %{ {%'OperatingSystemId' RTOS Adapter} ISR parameter is passed as parameter from RTOS interrupt dispatcher %}
  %'arg_isrParameterType' %'arg_isrParameterName' = (%'arg_isrParameterType')_isrParameter;
  %endif
%SUBROUTINE_END
%-
%-
%------------------------------------------------------------------------------
%SUBROUTINE RTOSAdap_genISRFunctionDefinitionClose(arg_intVectorProperty,arg_isrFunctionName,arg_isrParameterType,arg_isrParameterName)
%------------------------------------------------------------------------------
}
%SUBROUTINE_END
%-
%-
%------------------------------------------------------------------------------
%SUBROUTINE RTOSAdap_genISRFunctionDeclaration(arg_intVectorProperty,arg_isrFunctionName,arg_isrParameterType,arg_isrParameterName)
%------------------------------------------------------------------------------
%- MQX 'arg_isrParameterType' limitation: Parameter type should be convertible to 'void *' type.
%-
  %:tmpFirstUserIntIdx = 15
  %-
%{ {%'OperatingSystemId' RTOS Adapter} ISR function prototype %}
  %if %'arg_intVectorProperty' = '$SHARED'
    %- Shared interrupts will always install through the MQX standard ISR installation mechanism
void %'arg_isrFunctionName'(LDD_RTOS_TISRParameter _isrParameter);
  %elif (%"%'arg_intVectorProperty'SpecInterruptVectorNumberAddr" >=. %tmpFirstUserIntIdx)
    %- User interrupts install through the MQX standard ISR installation mechanism
void %'arg_isrFunctionName'(LDD_RTOS_TISRParameter _isrParameter);
  %else
    %- System interrupts are installed through the MQX kernel ISR installation mechanism
void %'arg_isrFunctionName'(void);
  %endif
%SUBROUTINE_END
%-
%-
%------------------------------------------------------------------------------
%SUBROUTINE RTOSAdap_genReentrantMethodBegin(opt_arg_genReentrantMethods)
%------------------------------------------------------------------------------
  %- Handle optional parameter
  %inclSUB RTOSAdap_priv_getOptionalParameterEffectiveValue(genReentrantMethods,%'opt_arg_genReentrantMethods',arg_genReentrantMethods)
  %-
  %if arg_genReentrantMethods == 'yes'
    %- Reentrant methods are required, generate enter critical section
    %inclSUB RTOSAdap_getRTOSFunction(EnterCritical,loc_enterCriticalFunctionName)
%{ {%'OperatingSystemId' RTOS Adapter} Method is required to be reentrant, critical section begins (RTOS function call is defined by %'OperatingSystemId' RTOS Adapter property) %}
%'loc_enterCriticalFunctionName'();
    %undef loc_enterCriticalFunctionName
  %elif arg_genReentrantMethods == 'no'
    %- Reentrant methods are not required, no code is generated
  %else
    %error! Invalid value of parameter 'opt_arg_genReentrantMethods' ("%'arg_genReentrantMethods'")
  %endif
  %-
  %undef arg_genReentrantMethods
%SUBROUTINE_END
%-
%-
%------------------------------------------------------------------------------
%SUBROUTINE RTOSAdap_genReentrantMethodEnd(opt_arg_genReentrantMethods)
%------------------------------------------------------------------------------
  %- Handle optional parameter
  %inclSUB RTOSAdap_priv_getOptionalParameterEffectiveValue(genReentrantMethods,%'opt_arg_genReentrantMethods',arg_genReentrantMethods)
  %-
  %if arg_genReentrantMethods == 'yes'
    %- Reentrant methods are required, generate exit critical section
    %inclSUB RTOSAdap_getRTOSFunction(ExitCritical,loc_exitCriticalFunctionName)
%{ {%'OperatingSystemId' RTOS Adapter} Critical section end (RTOS function call is defined by %'OperatingSystemId' RTOS Adapter property) %}
%'loc_exitCriticalFunctionName'();
    %undef loc_exitCriticalFunctionName
  %elif arg_genReentrantMethods == 'no'
    %- Reentrant methods are not required, no code is generated
  %else
    %error! Invalid value of parameter 'opt_arg_genReentrantMethods' ("%'arg_genReentrantMethods'")
  %endif
  %-
  %undef arg_genReentrantMethods
%SUBROUTINE_END
%-
%-
%------------------------------------------------------------------------------
%SUBROUTINE RTOSAdap_genCriticalSectionBegin(opt_arg_genReentrantMethods)
%------------------------------------------------------------------------------
  %- Handle optional parameter
  %inclSUB RTOSAdap_priv_getOptionalParameterEffectiveValue(genReentrantMethods,%'opt_arg_genReentrantMethods',arg_genReentrantMethods)
  %-
  %if arg_genReentrantMethods == 'yes'
    %- The method itself is generated as reentrant, additional critical sections are not needed
  %elif arg_genReentrantMethods == 'no'
    %- Reentrant methods are not required, but critical sections must remain
    %inclSUB RTOSAdap_getRTOSFunction(EnterCritical,loc_enterCriticalFunctionName)
%{ {%'OperatingSystemId' RTOS Adapter} Critical section begin (RTOS function call is defined by %'OperatingSystemId' RTOS Adapter property) %}
%'loc_enterCriticalFunctionName'();
    %undef loc_enterCriticalFunctionName
  %else
    %error! Invalid value of parameter 'opt_arg_genReentrantMethods' ("%'arg_genReentrantMethods'")
  %endif
  %-
  %undef arg_genReentrantMethods
%SUBROUTINE_END
%-
%-
%------------------------------------------------------------------------------
%SUBROUTINE RTOSAdap_genCriticalSectionEnd(opt_arg_genReentrantMethods)
%------------------------------------------------------------------------------
  %- Handle optional parameter
  %inclSUB RTOSAdap_priv_getOptionalParameterEffectiveValue(genReentrantMethods,%'opt_arg_genReentrantMethods',arg_genReentrantMethods)
  %-
  %if arg_genReentrantMethods == 'yes'
    %- The method itself is generated as reentrant, additional critical sections are not needed
  %elif arg_genReentrantMethods == 'no'
    %- Reentrant methods are not required, but critical sections must remain
    %inclSUB RTOSAdap_getRTOSFunction(ExitCritical,loc_exitCriticalFunctionName)
%{ {%'OperatingSystemId' RTOS Adapter} Critical section ends (RTOS function call is defined by %'OperatingSystemId' RTOS Adapter property) %}
%'loc_exitCriticalFunctionName'();
    %undef loc_exitCriticalFunctionName
  %else
    %error! Invalid value of parameter 'opt_arg_genReentrantMethods' ("%'arg_genReentrantMethods'")
  %endif
  %-
  %undef arg_genReentrantMethods
%SUBROUTINE_END
%-
%-
%------------------------------------------------------------------------------
%SUBROUTINE RTOSAdap_genIoctlCommandConstantDefinition(arg_commandName,arg_commandLogicalIndex,opt_arg_simpleComponentType)
%------------------------------------------------------------------------------
  %- Get constant name
  %inclSUB RTOSAdap_getIoctlCommandConstantName(%'arg_commandName',%'opt_arg_simpleComponentType',loc_constantName)
  %- Handle optional parameters
  %inclSUB RTOSAdap_priv_getOptionalParameterEffectiveValue(simpleComponentType,%'opt_arg_simpleComponentType',arg_simpleComponentType)
  %- Get MQX IO type for specified simple component type
  %:loc_simpleCompTypeIndex = %get_index1(%'arg_simpleComponentType',RTOSAdap_enum_simpleComponentTypes)
  %if loc_simpleCompTypeIndex = '-1'
    %error! Invalid HAL simple component type "%'arg_simpleComponentType'"
  %endif
  %define loc_simpleCompTypeAttribs %[%'loc_simpleCompTypeIndex',RTOSAdap_priv_simpleComponentTypeAttribs]
  %inclSUB RTOSAdap_lib_getStructMember(%'loc_simpleCompTypeAttribs',simpleComponentType,loc_checkSimpleCompType,required)
  %inclSUB RTOSAdap_lib_getStructMember(%'loc_simpleCompTypeAttribs',mqxIOType,loc_mqxIOType,required)
  %if loc_checkSimpleCompType != arg_simpleComponentType
    %error! %'OperatingSystemId' RTOS Adapter internal error, lists RTOSAdap_priv_simpleComponentTypeAttribs and RTOSAdap_simpleComponentTypes are not in index-consistency (at index %'loc_simpleCompTypeIndex' there is "%'loc_checkSimpleCompType'" instead of "%'arg_simpleComponentType'")
  %endif
  %if loc_mqxIOType == ''
    %error! %'OperatingSystemId' RTOS Adapter does not define MQX IO Type for component type "%'arg_simpleComponentType'"
  %endif
  %-
#define %'loc_constantName'                                      %>57_IO(%'loc_mqxIOType',%'arg_commandLogicalIndex')
  %-
  %undef arg_simpleComponentType
  %undef loc_constantName
  %undef loc_simpleCompTypeIndex
  %undef loc_simpleCompTypeAttribs
  %undef loc_checkSimpleCompType
  %undef loc_mqxIOType
%SUBROUTINE_END
%-
%-
%------------------------------------------------------------------------------
%SUBROUTINE RTOSAdap_getIoctlCommandConstantName(arg_commandName,opt_arg_simpleComponentType,out_constantName)
%------------------------------------------------------------------------------
  %- Handle optional parameters
  %inclSUB RTOSAdap_priv_getOptionalParameterEffectiveValue(simpleComponentType,%'opt_arg_simpleComponentType',arg_simpleComponentType)
  %-
  %- Check if arg_componentType is a valid enumerated value
  %:loc_simpleCompTypeIndex = %get_index1(%'arg_simpleComponentType',RTOSAdap_enum_simpleComponentTypes)
  %if loc_simpleCompTypeIndex = '-1'
    %error! Invalid HAL simple component type "%'arg_simpleComponentType'"
  %endif
  %-
  %define! %'out_constantName' %'arg_simpleComponentType'_IOCTL_%'arg_commandName'
  %-
  %undef arg_simpleComponentType
  %undef loc_simpleCompTypeIndex
%SUBROUTINE_END
%-
%-
%------------------------------------------------------------------------------
%SUBROUTINE RTOSAdap_genRTOSIncludes()
%------------------------------------------------------------------------------
/* MQX Lite include files */
#include "mqx.h"
#include "mqx_prv.h"
%SUBROUTINE_END
%-
%-
%------------------------------------------------------------------------------
%SUBROUTINE RTOSAdap_genRTOSDriverIncludes()
%------------------------------------------------------------------------------
%SUBROUTINE_END
%-
%-
%------------------------------------------------------------------------------
%SUBROUTINE RTOSAdap_genPublishedRTOSSettings
%------------------------------------------------------------------------------
%SUBROUTINE_END
%-
%-
%------------------------------------------------------------------------------
%SUBROUTINE RTOSAdap_genMemoryBarrierBegin(opt_arg_genReentrantMethods)
%------------------------------------------------------------------------------
%SUBROUTINE_END
%-
%-
%------------------------------------------------------------------------------
%SUBROUTINE RTOSAdap_genMemoryBarrierEnd(opt_arg_genReentrantMethods)
%------------------------------------------------------------------------------
%SUBROUTINE_END
%-
%-
%------------------------------------------------------------------------------
%SUBROUTINE RTOSAdap_genReentrantMethodDeclare(opt_arg_genReentrantMethods, spinlock_name)
%------------------------------------------------------------------------------
 %- Do nothing for bareboard
%SUBROUTINE_END
%-
%-
%------------------------------------------------------------------------------
%SUBROUTINE RTOSAdap_genRTOSReturnIRQ_Attended()
%------------------------------------------------------------------------------
return;
%SUBROUTINE_END
%-
%-
%------------------------------------------------------------------------------
%SUBROUTINE RTOSAdap_genRTOSReturnIRQ_NOTAttended()
%------------------------------------------------------------------------------
return;
%SUBROUTINE_END
%-