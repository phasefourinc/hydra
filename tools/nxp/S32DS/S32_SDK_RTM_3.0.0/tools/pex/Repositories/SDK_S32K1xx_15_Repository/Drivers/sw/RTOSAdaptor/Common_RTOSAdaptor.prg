%-
%- Routines common for all RTOS adapters
%-
%- (C) 2009 Freescale, all rights reserved
%-
%------------------------------------------------------------------------------
%- enumeration RTOSAdap_enum_componentTypes
%------------------------------------------------------------------------------
%- Enumeration of all possible (supported) component types.
%- Component simple type compounds multiple component types.
%- See also: RTOSAdap_registerComponentMethodImplementation().
%-
%if ndefined(RTOSAdap_enum_componentTypes)
%define_list RTOSAdap_enum_componentTypes {
HAL_UART_Polling
HAL_UART_Int
HAL_I2C_Polling
HAL_I2C_Int
HAL_GPIO
HAL_ADC
HAL_RTC
HAL_Ethernet
HAL_TimerUnit
AsyncSerial_LDD
USB_LDD
%define_list}
%endif
%-
%if ndefined(RTOSAdap_enum_simpleComponentTypes)
%define_list RTOSAdap_enum_simpleComponentTypes {
HAL_UART
HAL_I2C
HAL_GPIO
HAL_ADC
HAL_RTC
HAL_Ethernet
HAL_TimerUnit
AsyncSerial_LDD
USB_LDD
%define_list}
%endif
%-
%-
%------------------------------------------------------------------------------
%- enumeration RTOSAdap_enum_defaultParameterNames
%------------------------------------------------------------------------------
%- Enumeration of all possible (supported) default parameters which value
%- can be globally set and then omitted in the subroutine calls.
%- See also: RTOSAdap_setDefaultParameter().
%-
%if ndefined(RTOSAdap_enum_defaultParameterNames)
%define_list RTOSAdap_enum_defaultParameterNames {
componentType
simpleComponentType
componentInstanceName
genReentrantMethods
genCriticalSectionMethods
constantDeclarationsThread
allocatedDevice
allocatedDeviceBaseAddr
SPIN_LOCK
%define_list}
%endif
%-
%-
%------------------------------------------------------------------------------
%SUBROUTINE RTOSAdap_priv_getRTOSFunctionSymbolName(arg_rtosFunction,out_symbolName)
%------------------------------------------------------------------------------
%- Returns the name of PE macro language symbol containing the name of the C function
%- which is implementing the specified RTOS function.
%-
%- Input parameters:
%-      arg_rtosFunction        - Name of the abstract RTOS function.
%-
%- Outputs:
%-      out_symbolName          - Name of the PE symbol containing the name of C function
%-                                implementing the specified RTOS function.
%-                                (Parameter contains the name of PE macro language symbol into which the result is placed)
%-
  %define! %'out_symbolName' PE_G_RTOSAdap_P_RTOSFun_%'arg_rtosFunction'
%SUBROUTINE_END
%-
%-
%------------------------------------------------------------------------------
%SUBROUTINE RTOSAdap_priv_getRTOSPropertySymbolName(arg_rtosProperty,out_symbolName)
%------------------------------------------------------------------------------
%- Returns the name of PE macro language symbol containing the value of the specified RTOS property.
%-
%- Input parameters:
%-      arg_rtosProperty        - Name of the RTOS property.
%-
%- Outputs:
%-      out_symbolName          - Name of the PE symbol containing the value of the specified RTOS property.
%-                                (Parameter contains the name of PE macro language symbol into which the result is placed)
%-
  %define! %'out_symbolName' PE_G_RTOSAdap_P_%'arg_rtosProperty'
%SUBROUTINE_END
%-
%-
%------------------------------------------------------------------------------
%SUBROUTINE RTOSAdap_priv_parseDeclaration(arg_declaration,out_returnType,out_parameterList)
%------------------------------------------------------------------------------
%- Parses the declaration 'arg_declaration' in form "<out_returnType> <symbol-ignored> <out_parameterList>;"
%-
  %:loc_strPos = %str_length(arg_declaration)
  %-
  %- Check trailing ';'
  %:loc_c = %'arg_declaration'
  %substring loc_c,%'loc_strPos',1
  %if loc_c != ';'
    %error! Declaration %'arg_declaration' cannot be automatically parsed
  %endif
  %:loc_strPos = (%'loc_strPos' - 1)
  %-
  %- Check finishing ')'
  %:loc_c = %'arg_declaration'
  %substring loc_c,%'loc_strPos',1
  %if loc_c != ')'
    %error! Declaration %'arg_declaration' cannot be automatically parsed
  %endif
  %:loc_paramListEndPos = %'loc_strPos'
  %-
  %- Skip until pair '(' not found - inside there is parameter list
  %:loc_parLevel = 1
  %while ( (%'loc_parLevel' != '0') && (%'loc_strPos' != '2') )
    %:loc_strPos = (%'loc_strPos' - 1)
    %:loc_c = %'arg_declaration'
    %substring loc_c,%'loc_strPos',1
    %if loc_c == ')'
      %:loc_parLevel = (%'loc_parLevel' + 1)
    %elif loc_c == '('
      %:loc_parLevel = (%'loc_parLevel' - 1)
    %endif
  %endwhile
  %if loc_parLevel != '0'
    %error! Declaration %'arg_declaration' cannot be automatically parsed
  %endif
  %undef loc_parLevel
  %:loc_paramListStartPos = %'loc_strPos'
  %-
  %- Get parameter list
  %:loc_paramListLen = (%'loc_paramListEndPos' - %'loc_paramListStartPos' + 1)
  %define! loc_parameterList %'arg_declaration'
  %substring loc_parameterList, %'loc_paramListStartPos', %'loc_paramListLen'
  %undef loc_paramListStartPos
  %undef loc_paramListEndPos
  %undef loc_paramListLen
  %-
  %- Skip symbol - identifier
  %:loc_strPos = (%'loc_strPos' - 1)    %-- This is allowed because loc_strPos is at least 2
  %:loc_c = %'arg_declaration'
  %substring loc_c,%'loc_strPos',1
  %:loc_idLetters = abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890_
  %while ( (%'loc_strPos' != '0') && (%str_pos(loc_c,loc_idLetters)!='0') )
    %:loc_strPos = (%'loc_strPos' - 1)
    %:loc_c = %'arg_declaration'
    %substring loc_c,%'loc_strPos',1
  %endwhile
  %if loc_strPos == '0'
    %error! Declaration %'arg_declaration' cannot be automatically parsed
  %endif
  %-
  %- Not follows return type
  %:loc_retTypeEndPos = %'loc_strPos'
  %:loc_returnType = %'arg_declaration'
  %substring loc_returnType,1,%'loc_retTypeEndPos'
  %undef loc_retTypeEndPos
  %inclSUB RTOSAdap_lib_trimString(%'loc_returnType',loc_returnType)
  %-
  %- Pass return values
  %define! %'out_returnType' %'loc_returnType'
  %define! %'out_parameterList' %'loc_parameterList'
  %-
  %undef loc_returnType
  %undef loc_parameterList
  %undef loc_c
  %undef loc_strPos
  %undef loc_idLetters
%SUBROUTINE_END
%-
%-
%------------------------------------------------------------------------------
%SUBROUTINE RTOSAdap_setDefaultParameter(arg_parameterName,arg_defaultValue)
%------------------------------------------------------------------------------
%- Sets the default value for the specified RTOS adapter parameter.
%- If any RTOS adapter sub-routine has optional parameter, it can be specified that
%- the default is used when the parameter is omitted.
%- E.g. Call "RTOSAdap_setDefaultParameter(componentType,LDD_UART_Polling)"
%-      sets the default value ("LDD_UART_Polling") for parameters "opt_arg_componentType".
%- Note: The default value set by this method is used for the parameter value only
%-       if it is documented in this way.
%-
%- Input parameters:
%-      arg_parameterName       - Name of the parameter which default value is set.
%-                                One from the enumeration 'RTOSAdap_enum_defaultParameterNames'.
%-      arg_defaultValue        - Default value for the parameter.
%-
  %- Check default parameter name validity
  %:loc_paramIndex = %get_index1(%'arg_parameterName',RTOSAdap_enum_defaultParameterNames)
  %if loc_paramIndex = '-1'
    %error! Invalid RTOS adapter default parameter name "%'arg_parameterName'" (name must be one from enumeration RTOSAdap_enum_defaultParameterNames)
  %endif
  %undef loc_paramIndex
  %- Store the default parameter value
  %define! RTOSAdap_defParam_%'arg_parameterName' %'arg_defaultValue'
%SUBROUTINE_END
%-
%-
%------------------------------------------------------------------------------
%SUBROUTINE RTOSAdap_priv_getOptionalParameterEffectiveValue(arg_parameterName,arg_argumentValue,out_effectiveParameterValue)
%------------------------------------------------------------------------------
%- Private method. Handles the value of optional subroutine parameter.
%- Applies a rule:
%-   - If the optional parameter is specified, its effective value equals the value that has been passed.
%-   - If the optional parameter is not specified (it is omitted), its effective value equals
%-     to the default value (if specified). If default value is not specified, it is an error.
%-
%- See also: RTOSAdap_setDefaultParameter().
%-
%- Input parameters:
%-      arg_parameterName       - Name of the parameter which effective value is determined.
%-                                One from the enumeration 'RTOSAdap_enum_defaultParameterNames'.
%-      arg_argumentValue       - Value which has been passed into the sub-routine.
%-                                It is an empty string if the parameter was omitted
%-                                or non-empty value if the parameter was specified.
%-
%- Outputs:
%-      out_effectiveParameterValue
%-                              - Effective value of the parameter (results in 'arg_argumentValue'
%-                                if the parameter has been specified or to default value
%-                                if the parameter was omitted).
%-                                (Parameter contains the name of PE macro language symbol into which the result is placed)
%-
  %- Check default parameter name validity
  %:loc_paramIndex = %get_index1(%'arg_parameterName',RTOSAdap_enum_defaultParameterNames)
  %if loc_paramIndex = '-1'
    %error! Invalid RTOS adapter default parameter name "%'arg_parameterName'"
  %endif
  %undef loc_paramIndex
  %-
  %if arg_argumentValue != ''
    %- Parameter was specified, effective value equals the argument value
    %define! %'out_effectiveParameterValue' %'arg_argumentValue'
  %elif defined(RTOSAdap_defParam_%'arg_parameterName')
    %- Parameter was not specified and default value was specified, effective value equals the default value
    %define! %'out_effectiveParameterValue' %"RTOSAdap_defParam_%'arg_parameterName'"
  %else
    %error! Optional parameter "%'arg_parameterName'" was omitted but there is not specified default value for them (missing call to RTOSAdap_setDefaultParameter()?)
  %endif
%SUBROUTINE_END
%-
%-
%------------------------------------------------------------------------------
%SUBROUTINE RTOSAdap_getRTOSFunction(arg_rtosFunction,out_funName)
%------------------------------------------------------------------------------
%- Returns the name of the C function which is implementing the specified abstract RTOS function.
%- RTOS adapter provides the layer of abstract RTOS functions. Calls of the RTOS functions
%- are not generated directly: Each RTOS call has its symbolic name and RTOS adapter assigns
%- for each abstract RTOS function an implementing C function. The mapping between abstract RTOS
%- functions and C functions can be redefined by the user.
%-
%- Input parameters:
%-      arg_rtosFunction        - Name of the abstract RTOS function.
%-
%- Outputs:
%-      out_funName             - Name of the C function which is implementing the specified RTOS function.
%-                                (Parameter contains the name of PE macro language symbol into which the result is placed)
%-
  %inclSUB RTOSAdap_priv_getRTOSFunctionSymbolName(%'arg_rtosFunction',loc_rtosFunctionSymbolName)
  %ifdef %'loc_rtosFunctionSymbolName'
    %define! %'out_funName' %"%'loc_rtosFunctionSymbolName'"
  %else
    %error! Undefined abstract RTOS function %'arg_rtosFunction'
  %endif
  %undef loc_rtosFunctionSymbolName
%SUBROUTINE_END
%-
%-
%------------------------------------------------------------------------------
%SUBROUTINE RTOSAdap_defineRTOSFunction(arg_rtosFunction,arg_hasUserFunBoolGroupPropertySymbol,arg_userFunNamePropertySymbol,arg_funRetType,arg_funParams,arg_defaultFunName)
%------------------------------------------------------------------------------
%- Defines by which C function the specified abstract RTOS function is implemented.
%- The RTOS function implementation is defined by specified properties (see parameters description below).
%- Handles also extra-texts (hints) and errors of defining properties.
%-
%- Note: Should by called from .TS2 of RTOS component to ensure that all RTOS functions
%- are defined before they can be used from drivers of LDD components and where direct access to RTOS component
%- properties is possible.
%-
%- See also: RTOSAdap_getRTOSFunction subroutine.
%-
%- Input parameters:
%-      arg_rtosFunction        - Name of the abstract RTOS function to define.
%-      arg_hasUserFunBoolGroupPropertySymbol
%-                              - Symbol of boolean group property which enables or disables the
%-                                user override of the default implementation function.
%-                                Property should have value 'yes' if function is defined by user
%-                                or value 'no' if default implementation function is used.
%-      arg_userFunNamePropertySymbol
%-                              - Symbol of text property which defines the name of the user defined
%-                                implementation function
%-                                (used only if property arg_hasUserFunBoolGroupPropertySymbol has value 'yes').
%-      arg_funRetType          - Return type of the implementation function (or "void" if no return value)
%-      arg_funParams           - Parameter list of implementation function. The list should be enclosed in "()"
%-                                Contains "(void)" if there are no parameters.
%-                                E.g.: "(unsigned size, unsigned char fillChar)"
%-      arg_defaultFunName      - Name of the default implementation function
%-                                (this function is used if property arg_hasUserFunBoolGroupPropertySymbol
%-                                has value 'no').
%-
  %inclSUB RTOSAdap_priv_getRTOSFunctionSymbolName(%'arg_rtosFunction',loc_rtosFunctionSymbolName)
  %-
  %if %get(%'arg_hasUserFunBoolGroupPropertySymbol',BoolValue) == 'no'
    %set %'arg_hasUserFunBoolGroupPropertySymbol' ExtraText Using default %'arg_defaultFunName'()
    %define_prj %'loc_rtosFunctionSymbolName' %'arg_defaultFunName'
  %elif %get(%'arg_hasUserFunBoolGroupPropertySymbol',BoolValue) == 'yes'
    %set %'arg_userFunNamePropertySymbol' ExtraText Enter the name of the function of type %'arg_funRetType' %'arg_funParams'
    %if %get(%'arg_userFunNamePropertySymbol',Value) != ''
      %define_prj %'loc_rtosFunctionSymbolName' %get(%'arg_userFunNamePropertySymbol',Value)
      %set %'arg_hasUserFunBoolGroupPropertySymbol' ExtraText
    %else
      %set %'arg_hasUserFunBoolGroupPropertySymbol' Error Missing specification of user function. It is expected a name of the function of type %'arg_funRetType' %'arg_funParams'
      %define_prj %'loc_rtosFunctionSymbolName' %'arg_defaultFunName'
    %endif
  %else
    %error! Bad value of boolean group %'arg_hasUserFunBoolGroupPropertySymbol'
  %endif
  %-
  %undef loc_rtosFunctionSymbolName
%SUBROUTINE_END
%-
%-
%------------------------------------------------------------------------------
%SUBROUTINE RTOSAdap_getRTOSProperty(arg_rtosProperty,out_propValue)
%------------------------------------------------------------------------------
%- Returns the value of the specified property of RTOS component.
%- Only properties published by 'RTOSAdap_publishRTOSProperty' can be readed.
%-
%- Input parameters:
%-      arg_rtosProperty        - Name of the published RTOS property.
%-
%- Outputs:
%-      out_propValue           - Value of the specified RTOS property.
%-                                (Parameter contains the name of PE macro language symbol into which the result is placed)
%-
  %inclSUB RTOSAdap_priv_getRTOSPropertySymbolName(%'arg_rtosProperty',loc_rtosPropSymbolName)
  %ifdef %'loc_rtosPropSymbolName'
    %define! %'out_propValue' %"%'loc_rtosPropSymbolName'"
  %else
    %error! RTOS property %'arg_rtosProperty' was not published or does not exist.
  %endif
  %undef loc_rtosPropSymbolName
%SUBROUTINE_END
%-
%-
%------------------------------------------------------------------------------
%SUBROUTINE RTOSAdap_publishRTOSProperty(arg_rtosProperty,arg_propValue)
%------------------------------------------------------------------------------
%- Publishes and defines the value of the specified RTOS property.
%-
%- Note: Should by called from .TS2 of RTOS component to ensure that all RTOS published properties
%- are defined before they can be used from drivers of LDD components and where direct access to RTOS component
%- properties is possible.
%-
%- See also: RTOSAdap_getRTOSProperty subroutine.
%-
%- Input parameters:
%-      arg_rtosProperty        - Name of the RTOS property to publish and define its value.
%-      arg_propValue           - Value of property.
%-
  %inclSUB RTOSAdap_priv_getRTOSPropertySymbolName(%'arg_rtosProperty',loc_rtosPropSymbolName)
  %define_prj %'loc_rtosPropSymbolName' %'arg_propValue'
  %undef loc_rtosPropSymbolName
%SUBROUTINE_END
%-
%-
%------------------------------------------------------------------------------
%SUBROUTINE RTOSAdap_registerComponentInstance(opt_arg_componentType,opt_arg_componentInstanceName)
%------------------------------------------------------------------------------
%- Registers the instance of LDD component. The type of the component is also specified.
%- Each component instance should be registered from its DRV file to inform
%- RTOS Adapter that a component of specified type is instantiated.
%-
%- Note: Generates no code. The registered instance is only stored into internal variables.
%-
%- See also: RTOSAdap_registerComponentMethodImplementation(), RTOSAdap_genDriverInstallationsFunction().
%-
%- Input parameters:
%-      opt_arg_componentType   - Type of the registered component.
%-                                It is one value from enumeration 'RTOSAdap_enum_componentTypes'
%-                                Optional parameter. If not specified, default value specified by RTOSAdap_setDefaultParameter() is used.
%-      opt_arg_componentInstanceName
%-                              - Name of the instance of the component to register.
%-                                It is an identifier (does not contain quotation marks and possible suffixing ":").
%-                                E.g. "ttya".
%-                                Optional parameter. If not specified, default value specified by RTOSAdap_setDefaultParameter() is used.
%-
  %- Handle optional parameters
  %inclSUB RTOSAdap_priv_getOptionalParameterEffectiveValue(componentType,%'opt_arg_componentType',arg_componentType)
  %inclSUB RTOSAdap_priv_getOptionalParameterEffectiveValue(componentInstanceName,%'opt_arg_componentInstanceName',arg_componentInstanceName)
  %inclSUB RTOSAdap_priv_getOptionalParameterEffectiveValue(allocatedDevice,,arg_allocatedDevice)
  %inclSUB RTOSAdap_priv_getOptionalParameterEffectiveValue(allocatedDeviceBaseAddr,,arg_allocatedDeviceBaseAddr)
  %-
  %- Add the component instance into the list of registered component instances
  %if defined(PE_G_RTOSAdap_regCompInstanceNames) & %'arg_componentInstanceName' in PE_G_RTOSAdap_regCompInstanceNames
    %error! The instance of LDD component "%'arg_componentInstanceName'" of type "%'arg_componentType'" is already registered.
  %endif
  %-
  %- Store component instance name and type
  %append PE_G_RTOSAdap_regCompInstanceNames %'arg_componentInstanceName'
  %append PE_G_RTOSAdap_regCompInstanceTypes %'arg_componentType'
  %append PE_G_RTOSAdap_regCompInstanceAllocatedDevices %'arg_allocatedDevice'
  %append PE_G_RTOSAdap_regCompInstanceDeviceBaseAddrs %'arg_allocatedDeviceBaseAddr'
  %-
  %- Shared component name. Name of the component name is assigned in case of not shared component.
  %if defined(SharingComponentName)
    %append PE_G_RTOSAdap_regCompInstanceSharedComponentName %'SharingComponentName'
  %else
    %append PE_G_RTOSAdap_regCompInstanceSharedComponentName %'arg_componentInstanceName'
  %endif
  %-
  %- Component index to identify component in the project.
  %if ndefined(PE_G_RTOSAdap_regCompIndex)
    %define_prj! PE_G_RTOSAdap_regCompIndex 0
  %else
    %define_prj! PE_G_RTOSAdap_regCompIndex %EXPR(%PE_G_RTOSAdap_regCompIndex + 1)
  %endif
  %append PE_G_RTOSAdap_regCompInstanceIds %'PE_G_RTOSAdap_regCompIndex'
  %-
  %-
  %undef arg_componentType
  %undef arg_componentInstanceName
  %undef arg_allocatedDevice
  %undef arg_allocatedDeviceBaseAddr
%SUBROUTINE_END
%-
%-
%------------------------------------------------------------------------------
%SUBROUTINE RTOSAdap_registerComponentMethodImplementation(opt_arg_componentInstanceName,arg_methodName,arg_methodReturnType,arg_methodParameterList,arg_implemFunctionName)
%------------------------------------------------------------------------------
%- Registers the implementation of public method of the component API.
%- There are specified return type and method parameters.
%- Each component method implementation should be registered from its DRV file to inform
%- RTOS Adapter that the method is available.
%-
%- Specifies that component instance 'opt_arg_componentInstanceName' has public method with name 'arg_methodName'.
%- The method is returning type 'arg_methodReturnType' ("void" if no return value) and has parameters 'arg_methodParameterList'.
%- For instance of the component with name 'opt_arg_componentInstanceName' the method is
%- implemented by function with name 'arg_implemFunctionName'.
%-
%- Note: Generates no code. The method implementation is only registered into internal variables.
%-
%- See also: RTOSAdap_registerComponentInstance(), RTOSAdap_genDriverInstallationsFunction(),
%-           RTOSAdap_autoRegisterComponentMethodImplementation().
%-
%- Input parameters:
%-      opt_arg_componentInstanceName
%-                              - Name of instance of the component for what the method implementation is registered.
%-                                It is an identifier (does not contain quotation marks and possible suffixing ":").
%-                                E.g. "ttya".
%-                                Optional parameter. If not specified, default value specified by RTOSAdap_setDefaultParameter() is used.
%-      arg_methodName          - Name of a public method of component which implementation is registered.
%-                                The name should NOT contain <compId> prefix.
%-                                E.g. "SetBufferSize".
%-      arg_methodReturnType    - Return type of the method or "void" if no return value. E.g. "LDD_TSize".
%-      arg_methodParameterList - List of parameters of method. Parameter list contains also enclosing ().
%-                                And parameters are separated by ", ".
%-                                E.g. "(LDD_TDeviceData DeviceData, LDD_TSize newSize)".
%-      arg_implemFunctionName  - Name of the function which is implementing the specified method of the component
%-                                for the specified component instance.
%-                                Such function must return value of type 'arg_methodReturnType' and
%-                                must have parameters 'arg_methodParameterList'.
%-                                Contains an empty string if such method is not implemented for specified component instance.
%-                                E.g. "ttya_SetBufferSize".
%-
  %- Handle optional parameters
  %inclSUB RTOSAdap_priv_getOptionalParameterEffectiveValue(componentInstanceName,%'opt_arg_componentInstanceName',arg_componentInstanceName)
  %-
  %- Check if the component instance is already registered
  %if defined(PE_G_RTOSAdap_regCompInstanceNames) & %'arg_componentInstanceName' in PE_G_RTOSAdap_regCompInstanceNames
    %- Register the component instance method into the list
    %append PE_G_RTOSAdap_regCompInstance_%'arg_componentInstanceName'_methods %'arg_methodName'
    %- Register the component instance method pointer
    %if arg_implemFunctionName != ''
      %- Component method is implemented
      %define_prj! PE_G_RTOSAdap_regCompInstance_%'arg_componentInstanceName'_methodPtr_%'arg_methodName' (%'arg_methodReturnType' (*)%'arg_methodParameterList')&%'arg_implemFunctionName'
      %define_prj! PE_G_RTOSAdap_regCompInstance_%'arg_componentInstanceName'_methodEnabled_%'arg_methodName'
    %else
      %- Component method is not implemented, use 0 as pointer to the method function
      %define_prj! PE_G_RTOSAdap_regCompInstance_%'arg_componentInstanceName'_methodPtr_%'arg_methodName' (%'arg_methodReturnType' (*)%'arg_methodParameterList')0
    %endif
  %else
    %error! Component instance "%'arg_componentInstanceName'" is not registered yet. Use RTOSAdap_registerComponentInstance().
  %endif
  %-
  %undef arg_componentInstanceName
%SUBROUTINE_END
%-
%-
%------------------------------------------------------------------------------
%SUBROUTINE RTOSAdap_autoRegisterComponentMethodImplementation(arg_methodSymbol)
%------------------------------------------------------------------------------
%- It is a short-cut call to RTOSAdap_registerComponentMethodImplementation().
%- Parameters to that subroutine are derived in the following way:
%-      opt_arg_componentType   - Default value for argument is used.
%-      opt_arg_componentInstanceName
%-                              - Default value for argument is used.
%-      arg_methodName          - Equals to the 'arg_methodSymbol'.
%-      arg_methodReturnType    - Acquired by %get(%'arg_methodSymbol',methodDeclaration)
%-      arg_methodParameterList - Acquired by %get(%'arg_methodSymbol',methodDeclaration)
%-      arg_implemFunctionName  - If method symbol 'arg_methodSymbol' is defined (method is enabled),
%-                                equals to %'arg_componentInstanceName'%.%'arg_methodSymbol'
%-                              - If method symbol 'arg_methodSymbol' is not defined (method is disabled),
%-                                equals to an empty string.
%-
  %inclSUB RTOSAdap_priv_getOptionalParameterEffectiveValue(componentInstanceName,,loc_componentInstanceName)
  %-
  %- Parse method declaration in format "<returnType><methodName><parameterList>;"
  %define loc_methodDeclaration %get(%'arg_methodSymbol',MethodDeclaration)
  %inclSUB RTOSAdap_priv_parseDeclaration(%'loc_methodDeclaration',loc_methodReturnType,loc_methodParameterList)
  %-
  %- Call RTOSAdap_registerComponentMethodImplementation
  %ifdef %'arg_methodSymbol'
    %- The symbol is defined, the method is enabled
    %inclSUB RTOSAdap_registerComponentMethodImplementation(%'loc_componentInstanceName',%'arg_methodSymbol',%'loc_methodReturnType',%'loc_methodParameterList',%'loc_componentInstanceName'%.%"%'arg_methodSymbol'")
  %else
    %- The symbol is not defined, the method is not enabled
    %inclSUB RTOSAdap_registerComponentMethodImplementation(%'loc_componentInstanceName',%'arg_methodSymbol',%'loc_methodReturnType',%'loc_methodParameterList',)
  %endif
  %-
  %undef loc_componentInstanceName
  %undef loc_methodReturnType
  %undef loc_methodParameterList
  %undef loc_methodDeclaration
%SUBROUTINE_END
%-
%-
%------------------------------------------------------------------------------
%SUBROUTINE RTOSAdap_registerComponentEventImplementation(opt_arg_componentInstanceName,arg_eventName,arg_eventReturnType,arg_eventParameterList,arg_implemFunctionName)
%------------------------------------------------------------------------------
%- Registers the public event implementation of the component API.
%- There are specified return type and event parameters.
%- Each component event implementation should be registered from its DRV file to inform
%- RTOS Adapter that the event is available.
%-
%- Specifies that component instance 'opt_arg_componentInstanceName' has public event with name 'arg_eventName'.
%- The event is returning type 'arg_eventReturnType' ("void" if no return value) and has parameters 'arg_eventParameterList'.
%- For instance of the component with name 'opt_arg_componentInstanceName' the event is
%- implemented by function with name 'arg_implemFunctionName'.
%-
%- Note: Generates no code. The event implementation is only registered into internal variables.
%-
%- See also: RTOSAdap_registerComponentInstance(), RTOSAdap_genDriverInstallationsFunction(),
%-           RTOSAdap_autoRegisterComponentEvent().
%-
%- Input parameters:
%-      opt_arg_componentInstanceName
%-                              - Name of instance of the component for what the event implementation is registered.
%-                                It is an identifier (does not contain quotation marks and possible suffixing ":").
%-                                E.g. "ttya".
%-                                Optional parameter. If not specified, default value specified by RTOSAdap_setDefaultParameter() is used.
%-      arg_eventName           - Name of a public event of component which implementation is registered.
%-                                The name should NOT contain <compId> prefix.
%-                                E.g. "OnError".
%-      arg_eventReturnType     - Return type of the event or "void" if no return value.
%-      arg_eventParameterList  - List of parameters of event. Parameter list contains also enclosing ().
%-                                And parameters are separated by ", ".
%-                                E.g. "(LDD_TRTOSDeviceData RTOSDeviceData, LDD_TSize newSize)".
%-      arg_implemFunctionName  - Name of the function which is implementing the specified event of the component
%-                                for the specified component instance.
%-                                Such function must return value of type 'arg_eventReturnType' and
%-                                must have parameters 'arg_eventParameterList'.
%-                                Contains an empty string if such event is not implemented for specified component instance.
%-                                E.g. "ttya_OnError".
%-
  %- Handle optional parameters
  %inclSUB RTOSAdap_priv_getOptionalParameterEffectiveValue(componentInstanceName,%'opt_arg_componentInstanceName',arg_componentInstanceName)
  %-
  %- Check if the component instance is already registered
  %if defined(PE_G_RTOSAdap_regCompInstanceNames) & %'arg_componentInstanceName' in PE_G_RTOSAdap_regCompInstanceNames
    %- Register the component instance event into the list
    %append PE_G_RTOSAdap_regCompInstance_%'arg_componentInstanceName'_events %'arg_eventName'
    %- Register the component instance event pointer
    %if arg_implemFunctionName != ''
      %- Component event is implemented
      %define_prj! PE_G_RTOSAdap_regCompInstance_%'arg_componentInstanceName'_eventPtr_%'arg_eventName' (%'arg_eventReturnType' (*)%'arg_eventParameterList')&%'arg_implemFunctionName'
      %define_prj! PE_G_RTOSAdap_regCompInstance_%'arg_componentInstanceName'_eventEnabled_%'arg_eventName'
    %else
      %- Component event is not implemented, use 0 as pointer to the event function
      %define_prj! PE_G_RTOSAdap_regCompInstance_%'arg_componentInstanceName'_eventPtr_%'arg_eventName' (%'arg_eventReturnType' (*)%'arg_eventParameterList')0
    %endif
  %else
    %error! Component instance "%'arg_componentInstanceName'" is not registered yet. Use RTOSAdap_registerComponentInstance().
  %endif
  %-
  %undef arg_componentInstanceName
%SUBROUTINE_END
%-
%-
%------------------------------------------------------------------------------
%SUBROUTINE RTOSAdap_autoRegisterComponentEventImplementation(arg_eventSymbol)
%------------------------------------------------------------------------------
%- It is a short-cut call to RTOSAdap_registerComponentEventImplementation().
%- Parameters to that subroutine are derived in the following way:
%-      opt_arg_componentType   - Default value for argument is used.
%-      opt_arg_componentInstanceName
%-                              - Default value for argument is used.
%-      arg_eventName           - Equals to the 'arg_eventSymbol'.
%-      arg_eventReturnType     - Acquired by %get(%'arg_eventSymbol',eventDeclaration)
%-      arg_eventParameterList  - Acquired by %get(%'arg_eventSymbol',eventDeclaration)
%-      arg_implemFunctionName  - If event symbol 'arg_eventSymbol' is defined (event is enabled),
%-                                equals to %'arg_componentInstanceName'%.%'arg_eventSymbol'
%-                              - If event symbol 'arg_eventSymbol' is not defined (event is disabled),
%-                                equals to an empty string.
%-
  %inclSUB RTOSAdap_priv_getOptionalParameterEffectiveValue(componentInstanceName,,loc_componentInstanceName)
  %-
  %- Parse event declaration in format "<returnType><eventName><parameterList>;"
  %define loc_eventDeclaration %get(%'arg_eventSymbol',EventDeclaration)
  %inclSUB RTOSAdap_priv_parseDeclaration(%'loc_eventDeclaration',loc_eventReturnType,loc_eventParameterList)
  %-
  %- Call RTOSAdap_registerComponentEventImplementation
  %ifdef %'arg_eventSymbol'
    %- The symbol is defined, the event is enabled
    %inclSUB RTOSAdap_registerComponentEventImplementation(%'loc_componentInstanceName',%'arg_eventSymbol',%'loc_eventReturnType',%'loc_eventParameterList',%'loc_componentInstanceName'%.%"%'arg_eventSymbol'")
  %else
    %- The symbol is not defined, the event is not enabled
    %inclSUB RTOSAdap_registerComponentEventImplementation(%'loc_componentInstanceName',%'arg_eventSymbol',%'loc_eventReturnType',%'loc_eventParameterList',)
  %endif
  %-
  %undef loc_componentInstanceName
  %undef loc_eventReturnType
  %undef loc_eventParameterList
  %undef loc_eventDeclaration
%SUBROUTINE_END
%-
%-
%------------------------------------------------------------------------------
%-        RTOS ADAPTOR LIBRARY
%------------------------------------------------------------------------------
%- CONTAINS GENERAL SUBROUTINES FOR OTHER RTOS ADAPTOR METHODS (SUBROUTINES)
%-
%------------------------------------------------------------------------------
%SUBROUTINE RTOSAdap_lib_getStructMember(arg_structString,arg_structMemberName,out_structMemberValue,arg_necessity)
%------------------------------------------------------------------------------
%- Parses the string 'arg_structString' in format:
%-   "|<structMember1Name>=<structMember1Value>|...|<structMember<n>Name>=<structMember<n>Value>|"
%- and returns <structMemberValue> for specified struct member name.
%-
  %- Find the starting position of the struct member value
  %define loc_memberStructTag |%'arg_structMemberName'=
  %:loc_memberPos = %str_pos(loc_memberStructTag,arg_structString)
  %if loc_memberPos = '0'
    %if arg_necessity = 'required'
      %error! Undefined structure member "%'arg_structMemberName'" in structure string "%'arg_structString'" (for the first struct member check leading "|" character)
    %elif arg_necessity = 'optional'
      %- Returns 'undef' when the structure member doesn't exist.
      %define loc_memberValue undef
    %else
      %error! Unknown necessity parameter %'arg_necessity'
    %endif
  %else
    %:loc_valuePos = (%loc_memberPos + 1 + %str_length(arg_structMemberName) + 1)
    %:loc_tailLen = (%str_length(arg_structString) - %loc_valuePos + 1)
    %define loc_memberValue %'arg_structString'
    %substring loc_memberValue, %'loc_valuePos', %'loc_tailLen'
    %-
    %- Truncate member value from the following struct members
    %:loc_valueLen = (%str_pos('|',loc_memberValue) - 1)
    %if loc_valueLen = '-1'
      %error! Bad structure member "%'arg_structMemberName'" format in structure string "%'arg_structString'" (probably missing terminating "|" character)
    %endif
    %substring loc_memberValue, 1, %'loc_valueLen'
    %-
    %undef loc_memberPos
    %undef loc_valuePos
    %undef loc_tailLen
    %undef loc_valueLen
  %endif
  %-
  %define! %'out_structMemberValue' %'loc_memberValue'
  %-
  %undef loc_memberStructTag
  %undef loc_memberValue
%SUBROUTINE_END
%-
%-
%------------------------------------------------------------------------------
%SUBROUTINE RTOSAdap_lib_trimString(arg_string,out_trimmedString)
%------------------------------------------------------------------------------
%- Removes leading and trailing white spaces from the string.
%-
%- Input parameters:
%-      arg_string              - String in which the spaces should be removed
%-
%- Outputs:
%-      out_trimmedString       - Output string with removed leading and trailing spaces
%-                                Note: Output buffer may be represented by the same symbol
%-                                      which value is used as input (because the arguments
%-                                      are passed by value).
%-                                (Parameter contains the name of PE macro language symbol into which the result is placed)
%-
  %:loc_startPos = 1
  %:loc_len = %str_length(arg_string)
  %-
  %define! loc_onlySpacesString no
  %-
  %if (%'arg_string'!='')
    %define! loc_char %'arg_string'
    %substring loc_char, %'loc_startPos', 1
    %while (%'loc_char'==' ')
      %if (%'loc_startPos' < %'loc_len')
        %:loc_startPos = (%'loc_startPos' + 1)
        %define! loc_char %'arg_string'
        %substring loc_char, %'loc_startPos', 1
      %else
        %- The string contains only spaces, terminate the loop
        %define! loc_onlySpacesString yes
        %define! loc_char x
      %endif
    %endwhile
    %-
    %if loc_onlySpacesString == 'yes'
      %:loc_len = 0
    %else
      %define! loc_char %'arg_string'
      %substring loc_char, %'loc_len', 1
      %while (%'loc_char'==' ')
        %:loc_len = (%'loc_len' - 1)
        %define! loc_char %'arg_string'
        %substring loc_char, %'loc_len', 1
      %endwhile
      %:loc_len = (%'loc_len' - %'loc_startPos' + 1)
      %-
    %endif
    %undef loc_char
    %-
  %endif
  %define! %'out_trimmedString' %'arg_string'
  %substring %'out_trimmedString', %'loc_startPos', %'loc_len'
  %-
  %undef loc_startPos
  %undef loc_len
%SUBROUTINE_END
%-
%-
%------------------------------------------------------------------------------
%SUBROUTINE RTOSAdap_createConstant(arg_constantName,arg_constantValue,arg_constantComment,opt_arg_constantDeclarationsThread,out_opt_constantName)
%------------------------------------------------------------------------------
%- Creates a declarations of constant with name 'arg_constantName', constant value
%- 'arg_constantValue' (may contain an expression, need not to be evaluable at design time)
%- and short comment 'arg_constantComment' into thread 'opt_arg_constantDeclaraionThread'.
%-     If the specified constant has been already generated (with the same name), it is
%- checked that the value is exactly the same (difference in comment does not matter)
%- and no constant declaration is generated.
%-     The name of the constant is stored into 'out_opt_constantName' (parameter
%- 'arg_constantName' is stored into 'out_opt_constantName' to avoid retyping the name of
%- the constant and support easy renaming of the constant).
%-
%- Input parameters:
%-      arg_constantName        - Name of the constant to define.
%-      arg_constantValue       - Compile-time constant expression which evaluates
%-                                to the value of the constant.
%-                                The expression should be enclosed in parentheses to
%-                                allow placing in any context.
%-      arg_constantComment     - Comment generated by the constant declaration.
%-      opt_arg_constantDeclarationsThread
%-                              - Thread where the declaration of constant should be generated into.
%-                                Optional parameter. If not specified, default value specified by RTOSAdap_setDefaultParameter() is used.
%- Outputs:
%-      out_opt_constantName    - The name of generated constant (=='arg_constantName')
%-                                will be placed into this symbol.
%-                                Optional parameter. If not specified, the output value will not be provided.
%-                                (Parameter contains the name of PE macro language symbol into which the result is placed.)
%-
  %- Handle optional parameters
  %inclSUB RTOSAdap_priv_getOptionalParameterEffectiveValue(constantDeclarationsThread,%'opt_arg_constantDeclarationsThread',arg_constantDeclarationsThread)
  %-
  %if defined(RTOSAdap_genConstants_Names)
    %:loc_previousDeclIndex = %get_index1(%'arg_constantName',RTOSAdap_genConstants_Names)
  %else
    %:loc_previousDeclIndex = -1
  %endif
  %if loc_previousDeclIndex == '-1'
    %- Such constant has not been generated yet
    %- Generate the declaration
    %THREAD %'arg_constantDeclarationsThread' SELECT
    %if arg_constantComment == ''
#define %'arg_constantName' %'arg_constantValue'
    %else
#define %'arg_constantName' %'arg_constantValue'                 %>>%{!< %'arg_constantComment' %}
    %endif
    %THREAD %'arg_constantDeclarationsThread' UNSELECT
    %- Mark it as already generated
    %apploc RTOSAdap_genConstants_Names %'arg_constantName'
    %apploc RTOSAdap_genConstants_Values %'arg_constantValue'
  %else
    %- Constant has been already generated
    %- Generate nothing, just check if the constant value is the same
    %if arg_constantValue != %[%'loc_previousDeclIndex',RTOSAdap_genConstants_Values]
      %error! Auto-generated constant %'arg_constantName' is redefined from "%[%'loc_previousDeclIndex',RTOSAdap_genConstants_Values]" to "%'arg_constantValue'"
    %endif
  %endif
  %-
  %- Pass the constant name to the output
  %if out_opt_constantName != ''
    %define! %'out_opt_constantName' %'arg_constantName'
  %endif
  %-
  %undef loc_previousDeclIndex
  %undef arg_constantDeclarationsThread
%SUBROUTINE_END
%-
%-
%------------------------------------------------------------------------------
%SUBROUTINE RTOSAdap_registerComponentDeviceStructure(arg_deviceStructure,arg_operation, opt_arg_componentInstanceName)
%------------------------------------------------------------------------------
%- Generates driver constants to 'the the component's header (methods configuration, shared mode).
%-
%- Input parameters:
%-      arg_deviceStructure           - An l-value expression to which the address of allocated object is assigned to.
%-      arg_operation                 - Specifies the requested operation [REGISTER|UNREGISTER]
%-      opt_arg_componentInstanceName - Optional argument specifying the component instance name. If it isn't specified,
%-                                      default 'componentInstanceName' parameter is used. This argument is typically used when adressing
%-                                      other components (shared mode).
%-
  %if opt_arg_componentInstanceName != ''
    %define! arg_componentInstanceName %opt_arg_componentInstanceName
  %else
    %inclSUB RTOSAdap_priv_getOptionalParameterEffectiveValue(componentInstanceName,,arg_componentInstanceName)
  %endif
  %inclSUB RTOSAdap_priv_getOptionalParameterEffectiveValue(allocatedDevice,,arg_allocatedDevice)
  %if arg_operation = 'REGISTER'
/* Registration of the device structure */
    %if arg_allocatedDevice != '$shared$'
PE_LDD_RegisterDeviceStructure(PE_LDD_COMPONENT_%'arg_componentInstanceName'_ID,%'arg_deviceStructure');
    %else
PE_LDD_RegisterDeviceStructure(PE_LDD_COMPONENT_%'arg_componentInstanceName'_ID,NULL);
    %endif
  %elif arg_operation = 'UNREGISTER'
/* Unregistration of the device structure */
PE_LDD_UnregisterDeviceStructure(PE_LDD_COMPONENT_%'arg_componentInstanceName'_ID);
  %else
    %error! Unexpected value of parameter arg_operation (%'arg_operation').
  %endif
  %undef arg_componentInstanceName
  %undef arg_allocatedDevice
  %-
%SUBROUTINE_END
%-
%------------------------------------------------------------------------------
%SUBROUTINE RTOSAdap_genDriverPublicConstants(arg_sharedModuleName)
%------------------------------------------------------------------------------
%- Generates driver constants to the the component's header (methods configuration, shared mode).
%-
%- Input parameters:
%-      arg_sharedModuleName    - Name of the shared module. Empty string means no sharing.
%-
  %inclSUB RTOSAdap_priv_getOptionalParameterEffectiveValue(componentInstanceName,,loc_componentInstanceName)
  %inclSUB RTOSAdap_priv_getOptionalParameterEffectiveValue(allocatedDeviceBaseAddr,,loc_allocatedDeviceBaseAddr)
  %-
  %if (loc_allocatedDeviceBaseAddr != '')
%{! Peripheral base address of a device allocated by the component. This constant can be used directly in PDD macros. %}
#define %'loc_componentInstanceName'_PRPH_BASE_ADDRESS  %#h%'loc_allocatedDeviceBaseAddr'U
  
  %endif
  %if (%get(AutoInitialization,Exists?) = 'yes') & (%get(AutoInitialization,Bool) = 'yes')
%{! Device data structure pointer used when auto initialization property is enabled. This constant can be passed as a first parameter to all component's methods. %}
#define %'loc_componentInstanceName'_DeviceData  ((LDD_TDeviceData *)PE_LDD_GetDeviceStructure(PE_LDD_COMPONENT_%'loc_componentInstanceName'_ID))

  %endif
  %-
  %if ( (arg_sharedModuleName != '') & (arg_sharedModuleName != %'ModuleName') )
%{! Shared configuration constant - availabe in the shared mode only. A value specify a name of the shared component %}
#define %'loc_componentInstanceName'_SHARED_BY %'arg_sharedModuleName' 

  %endif
  %if defined(PE_G_RTOSAdap_regCompInstance_%'loc_componentInstanceName'_methods)
%{ Methods configuration constants - generated for all enabled component's methods %}
    %for method from PE_G_RTOSAdap_regCompInstance_%'loc_componentInstanceName'_methods
      %if defined(PE_G_RTOSAdap_regCompInstance_%'loc_componentInstanceName'_methodEnabled_%'method')
        %- Generated constant for enabled method only
#define %'loc_componentInstanceName'_%'method'_METHOD_ENABLED    %>>/*!< %'method' method of the component %'loc_componentInstanceName' is enabled (generated) */
      %endif
    %endfor

  %endif
  %if defined(PE_G_RTOSAdap_regCompInstance_%'loc_componentInstanceName'_events)
%{ Events configuration constants - generated for all enabled component's events %}
    %for event from PE_G_RTOSAdap_regCompInstance_%'loc_componentInstanceName'_events
      %if defined(PE_G_RTOSAdap_regCompInstance_%'loc_componentInstanceName'_eventEnabled_%'event')
        %- Generated constant for enabled event only
#define %'loc_componentInstanceName'_%'event'_EVENT_ENABLED      %>>/*!< %'event' event of the component %'loc_componentInstanceName' is enabled (generated) */
      %endif
    %endfor

  %endif
  %-
  %undef loc_componentInstanceName
  %undef loc_allocatedDeviceBaseAddr
%SUBROUTINE_END
%-
%-
%------------------------------------------------------------------------------
%-        RTOS SPECIFIC IMPLEMENTATIONS
%------------------------------------------------------------------------------
%- THE IMPLEMENTATIONS OF THE FOLLOWING SUBROUTINES ARE RTOS SPECIFIC.
%- THEY ARE FOLLOWING ONLY SUBROUTINE HEADERS AND DOCUMENTATION.
%- THE IMPLEMENTATIONS ARE IN SW/RTOSADAPTOR/<RTOSNAME>_RTOSADAPTOR.PRG FILES.
%-
%-
%------------------------------------------------------------------------------
%-SUBROUTINE RTOSAdap_genRTOSTypeDefinitions()
%------------------------------------------------------------------------------
%- Generates the definitions of RTOS specific types (types with name LDD_RTOS_*).
%- This types should be declared in (or included into) every module where any
%- code generated by the RTOS adapter is used.
%-
%-
%------------------------------------------------------------------------------
%-SUBROUTINE RTOSAdap_genDriverInstallationsFunctionDefinition
%------------------------------------------------------------------------------
%- Generates the C definition of 'void PE_LDD_driver_installations(void)' function.
%- This function is responsible for installing all instances of LDD components
%- (what have been previously registered by RTOSAdap_registerComponentMethodImplementation() subroutine)
%- into RTOS. The style of installation of component is RTOS specific, but typically
%- it calls some RTOS installation function for each instance of component
%- and as arguments it passes the pointers to functions implementing the methods of the component
%- (see specific RTOS adapter for more detailed documentation).
%-
%- Note: It is necessary to process includes generated by RTOSAdap_genRTOSDriverIncludes()
%-       before the code generated by this subroutine appears.
%-
%-
%------------------------------------------------------------------------------
%-SUBROUTINE RTOSAdap_genDriverInstallationsFunctionDeclaration
%------------------------------------------------------------------------------
%- Generates the C declaration of 'void PE_LDD_driver_installations(void)' function.
%- See also: RTOSAdap_genDriverInstallationsFunctionDefinition() sub-routine.
%-
%-
%------------------------------------------------------------------------------
%-SUBROUTINE RTOSAdap_genDriverMemoryAlloc(arg_destPtrBuffer,arg_objType,opt_arg_errCode,arg_globDefsThread,opt_arg_MemoryAllocParams)
%------------------------------------------------------------------------------
%- Generates a code for dynamic memory allocation of object of type 'arg_objType' and
%- storing the address of allocated object into 'arg_destPtrBuffer'.
%- The allocated memory is suitable for direct usage in LDD driver code (paging, protection, ...).
%- If an allocation is not successful, statement 'arg_errCode' will be executed.
%- If dynamic allocation is not supported, the help static object is generated
%- (into thread 'arg_globDefsThread') and the address of such static object is stored into 'arg_destBufferPtr'.
%-
%- Constraint: The generated code cannot be executed within the run-time cycle.
%-   The only allowed run-time scenario is allocation - usage - deallocation
%-   (and possibly repeatedly). It is because of possible simulation of dynamic allocation.
%-
%- Input parameters:
%-      arg_destPtrBuffer          - An l-value expression to which the address of allocated object is assigned to.
%-      arg_objType                - Type of the allocated object (Note: NOT pointer to that type).
%-      opt_arg_errCode            - Optional parameter. Code which will be executed if the allocation fails.
%-                                   If run-time detection of memory allocation failure is not enabled or
%-                                   supported by RTOS, the parameter is omitted.
%-                                   If not specified (or empty), error handling is not generated.
%-      arg_globDefsThread         - Name of the generation thread created by %THREAD CREATE.
%-                                   Specifies where the definition of help static object is generated
%-                                   if dynamic allocation is not supported.
%-      opt_arg_MemoryAllocParams  - Optional memory allocation parameters.
%-                                   Specifies additional optional parameters in format |<param1>=<value>|<param2>=<value>|.....|
%-                                   Optional parameters:
%-                                     ZERO              - Allocates memory block and fill the memory by zero.
%-                                     UNCACHED          - Allocates memory block in uncached area.
%-                                     ALIGN=<boundary>  - Allocates memory block whose address is a multiple of <boundary>.
%-
%-
%------------------------------------------------------------------------------
%-SUBROUTINE RTOSAdap_genDriverMemoryDealloc(arg_ptrBuffer,arg_objType)
%------------------------------------------------------------------------------
%- Generates a code fragment for dynamic memory deallocation (if supported) of memory area at address 'arg_ptrBuffer'.
%- The object should be of 'arg_objType'.
%- If dynamic allocation is simulated by static allocation, does nothing.
%-
%- Input parameters:
%-      arg_ptrBuffer           - Expression which evaluates to the address to object to deallocate.
%-      arg_objType             - Type of the object to deallocate (NOT pointer to that type).
%-
%- See also: RTOSAdap_genDriverMemoryAlloc().
%-
%-
%------------------------------------------------------------------------------
%-SUBROUTINE RTOSAdap_getInterruptVectorSymbol(arg_vectorName,out_vectorIndexConstantName)
%------------------------------------------------------------------------------
%- Returns the name of C constant which evaluates (during compile-time) to the index of
%- the interrupt vector with specified name.
%- The user of RTOS adapter is responsible for defining such constants. The user should also
%- ensure that the constants are also available at all places where RTOS adapter generates
%- the code for handling interrupts.
%-
%- Input parameters:
%-      arg_vectorName          - Name of the interrupt vector (without "iv" prefix used in PE).
%-
%- Outputs:
%-      out_vectorIndexConstantName
%-                              - Name of the C constant containing the index of the vector
%-                                (Parameter contains the name of PE macro language symbol into which the result is placed)
%-
%-
%------------------------------------------------------------------------------
%-SUBROUTINE RTOSAdap_genSetInterruptVector(arg_intVectorProperty,arg_isrFunctionName,arg_isrParameterType,arg_isrParameterValue,opt_arg_oldISRSettings,arg_globDefsThread)
%------------------------------------------------------------------------------
%- Generates the code fragment which routes interrupt vector with name defined by 'arg_intVectorProperty' component property
%- to ISR function with name 'arg_isrFunctionName'.
%- The ISR function obtains an argument of type 'arg_isrParameterType'.
%- The value of such parameter is defined by 'arg_isrParameterValue'.
%- The previous settings of the interrupt vector (handling ISR function and
%- ISR function argument value) are stored into 'arg_oldISRSettings'.
%-
%- If run-time allocation of ISR vectors is not available, ISR vector is allocated statically
%- in interrupt vector table generated by PE and parameter passed into the ISR function is transferred
%- by help global variable (its definition is generated into thread 'arg_globDefsThread').
%- The code generated by this subroutine and by RTOSAdap_genISRFunctionDefinition() must have
%- access to this possibly generated global symbol.
%-
%- Note: There can be some RTOS specific limitations to 'arg_isrParameterType' type.
%-       See documentation of concrete RTOS adapter for details.
%-
%- Note: The generated code requires access to the interrupt vector index constants (see RTOSAdap_getInterruptVectorSymbol).
%-
%- See also: RTOSAdap_genRestoreInterruptVector(), RTOSAdap_genISRFunctionDefinition().
%-
%- Input parameters:
%-      arg_intVectorProperty   - Name of the interrupt vector component property.
%-      arg_isrFunctionName     - Name of the ISR function which will handle the interrupt vector.
%-                                ISR function declaration/definition must be generated by proper RTOS adapter
%-                                subroutines (RTOSAdap_genISRFunctionDefinition*, RTOSAdap_genISRFunctionDeclaration*).
%-      arg_isrParameterType    - Type of the ISR function parameter passed into the ISR function.
%-      arg_isrParameterValue   - Expression which evaluates to the value of type arg_isrParameterType.
%-                                The value will be passed into ISR function as an argument.
%-      opt_arg_oldISRSettings  - An l-value expression to which the previous settings of interrupt vector are saved.
%-                                The definition of such object should be generated by RTOSAdap_genISRSettingsVarDeclaration().
%-                                Optional parameter. If not specified, old vector settings are not saved.
%-      arg_globDefsThread      - Name of the generation thread created by %THREAD CREATE.
%-                                Specifies where global variable definition should be generated
%-                                if run-time installation of ISR vectors is not available.
%-
%-
%------------------------------------------------------------------------------
%-SUBROUTINE RTOSAdap_genRestoreInterruptVector(arg_intVectorProperty,arg_oldISRSettings)
%------------------------------------------------------------------------------
%- Generates the code which restores the settings of the interrupt vector (handling
%- ISR function and ISR function argument value) with name defined by 'arg_intVectorProperty' component property to
%- the state specified by 'arg_oldISRSettings'.
%-
%- Note: The generated code requires access to the interrupt vector index constants (see RTOSAdap_getInterruptVectorSymbol).
%-
%- See also: RTOSAdap_genSetInterruptVector().
%-
%- Input parameters:
%-      arg_intVectorProperty   - Name of the interrupt vector component property.
%-      arg_oldISRSettings      - Expression evaluating to the settings to which the interrupt vector
%-                                The definition of such object should be generated by RTOSAdap_genISRSettingsVarDeclaration().
%-
%-
%------------------------------------------------------------------------------
%-SUBROUTINE RTOSAdap_genISRSettingsVarDeclaration(arg_varName,opt_arg_comment)
%------------------------------------------------------------------------------
%- Generates the C declaration of object storing the settings of ISR vector
%- (handling ISR function and ISR function argument value).
%- If generated inside C function body, it results into definition of automatic variable with name 'arg_varName'.
%- If generated inside C structure definition, it results into declaration of structure member with name 'arg_varName'.
%- If generated outside any function definition, it results into definition of global variable with name 'arg_varName'.
%-
%- See also: RTOSAdap_genSetInterruptVector(), RTOSAdap_genRestoreInterruptVector().
%-
%- Input parameters:
%-      arg_varName             - Name of the variable/struct member to declare.
%-      opt_arg_comment         - Optional comment for the declaration
%-
%-
%------------------------------------------------------------------------------
%-SUBROUTINE RTOSAdap_genISRFunctionDefinitionOpen(arg_intVectorProperty,arg_isrFunctionName,arg_isrParameterType,arg_isrParameterName)
%------------------------------------------------------------------------------
%- Generates the definition of C function named 'arg_isrFunctionName' what can be used
%- as an ISR function for interrupt vector with name defined by 'arg_intVectorProperty' component property.
%- Generates also opening { of the function body and also (if needed) RTOS specific ISR prolog
%- (saving registers, handling stack pointer, etc.).
%- Inside the function body there can be used symbol 'arg_isrParameterName' of type
%- 'arg_isrParameterType' which contains the value defined during installation of the ISR function.
%-
%- Note: The code generated by this subroutine must have access to the possibly generated global symbol
%-       by RTOSAdap_genSetInterruptVector().
%-
%- Note: There can be some RTOS specific limitations to 'arg_isrParameterType' type.
%-       See documentation of concrete RTOS adapter for details.
%-
%- Note: To generate ISR epilog and closing } use RTOSAdap_genISRFunctionDefinitionClose().
%-
%- Input parameters:
%-      arg_intVectorProperty   - Name of the interrupt vector defined 'arg_isrFunctionName' component property which can be handled by
%-                                the ISR function.
%-      arg_isrFunctionName     - Name of the C function to define.
%-      arg_isrParameterType    - Type of the parameter passed into the ISR function.
%-      arg_isrParameterName    - Name of the parameter passed into the ISR function
%-                                (this name can be used within the function body
%-                                to access the value defined during ISR function installation).
%-                                The parameter evaluates to arg_isrParameterType.
%-
%- See also: RTOSAdap_genSetInterruptVector(), RTOSAdap_genISRFunctionDeclaration().
%-
%-
%------------------------------------------------------------------------------
%-SUBROUTINE RTOSAdap_genISRFunctionDefinitionClose(arg_intVectorProperty,arg_isrFunctionName,arg_isrParameterType,arg_isrParameterName)
%------------------------------------------------------------------------------
%- Pair subroutine to RTOSAdap_genISRFunctionDefinitionClose().
%-
%-
%------------------------------------------------------------------------------
%-SUBROUTINE RTOSAdap_genISRFunctionDeclaration(arg_intVectorProperty,arg_isrFunctionName,arg_isrParameterType,arg_isrParameterName)
%------------------------------------------------------------------------------
%- Generates the declaration of C function named 'arg_isrFunctionName' what can be used
%- as an ISR function for interrupt vector with name defined by 'arg_intVectorProperty' component property.
%-
%- Note: There can be some RTOS specific limitations to 'arg_isrParameterType' type.
%-       See documentation of concrete RTOS adapter for details.
%-
%- Note: To generate ISR epilog and closing '}' use RTOSAdap_genISRFunctionDefinitionClose().
%-
%- Input parameters:
%-      arg_intVectorProperty   - Name of the interrupt vector defined 'arg_isrFunctionName' component property which can be handled by
%-                                the ISR function.
%-      arg_isrFunctionName     - Name of the C function to declare.
%-      arg_isrParameterType    - Type of the parameter passed into the ISR function.
%-      arg_isrParameterName    - Name of the parameter passed into the ISR function
%-                                (only declaration is generated, so the parameter name is only for information).
%-
%- See also: RTOSAdap_genSetInterruptVector(), RTOSAdap_genISRFunctionDefinition*().
%-
%-
%------------------------------------------------------------------------------
%-SUBROUTINE RTOSAdap_genReentrantMethodBegin(opt_arg_genReentrantMethods)
%------------------------------------------------------------------------------
%- Generates the code for entering the reentrant method of LDD component.
%-
%- ** If driver methods are required to be reentrant (opt_arg_genReentrantMethods=='yes')
%- generates the code that guarantees that there cannot be simultaneously executed two methods of
%- LDD component. If there is next thread trying enter any method of LDD component and there is
%- some method of the LDD component already active (running), the next thread is suspended and
%- must wait until the first thread leaves.
%- ** If driver methods are not required to be reentrant (opt_arg_genReentrantMethods=='no'),
%- typically generates no code. But call of RTOSAdap_genReentrantMethodBegin() should be
%- called at any circumstance, should not be conditioned by method reentrancy requirement.
%-
%- The generated code must be executed in pair with the code generated by RTOSAdap_genReentrantMethodEnd()
%- or RTOSAdap_genReentrantMethodEndAndReturn()
%-
%- Note: It is supported that reentrant methods can be dynamically nested (e.g. there can be
%-       called a reentrant method from another reentrant method).
%-
%- Note: Reentrancy of methods serializes only the user calls of the LDD methods.
%-       Even if the methods are required to be reentrant they can be interrupted by
%-       interrupts. For protecting against interrupting by interrupts, see RTOSAdap_genCriticalSectionBegin().
%-
%- Input parameters:
%-      opt_arg_genReentrantMethods
%-                              - Contains 'yes' or 'no' if reentrant methods are or aren't
%-                                required for the current LDD component.
%-                                Optional parameter. If not specified, default value specified by RTOSAdap_setDefaultParameter() is used.
%-
%-
%------------------------------------------------------------------------------
%-SUBROUTINE RTOSAdap_genReentrantMethodEnd(opt_arg_genReentrantMethods)
%------------------------------------------------------------------------------
%- Pair subroutine to RTOSAdap_genReentrantMethodBegin().
%- Generates the code for ending the reentrant method synchronized section.
%- After the execution of the generated code, the execution continues without
%- synchronization.
%-
%-
%------------------------------------------------------------------------------
%SUBROUTINE RTOSAdap_genReentrantMethodEndAndReturn(arg_returnType,arg_returnValueExprThread,arg_tempVarDeclarationThread,opt_arg_genReentrantMethods)
%------------------------------------------------------------------------------
%- NOTE: Currently it seems that the implementation is general and not RTOS specific.
%-       That is why the implementation is placed here and not within specific RTOS adapter code.
%-
%- Pair subroutine to RTOSAdap_genReentrantMethodBegin().
%- Generates the construction for ending the synchronized section and returning
%- the specified value of specified type.
%- The generated code may contain definition of temporary C local variable 'returnValue'.
%- The definition is placed into arg_tempVarDeclarationThread.
%-
%- Input parameters:
%-      arg_returnType          - Return type of the function.
%-      arg_returnValueExprThread
%-                              - ThreadId of thread what already contains the expression
%-                                of return value.
%-                                The expression should evaluate to type 'arg_returnType' and
%-                                should be enclosed in parentheses to allow placing in any context.
%-                                The thread is destroyed by the subroutine.
%-      arg_tempVarDeclarationThread
%-                              - ThreadId of thread where temporary variable declaration is
%-                                generated (if needed).
%-      opt_arg_genReentrantMethods
%-                              - Contains 'yes' or 'no' if reentrant methods are or aren't
%-                                required for the current LDD component.
%-                                Optional parameter. If not specified, default value specified by RTOSAdap_setDefaultParameter() is used.
%-
%- Output parameters:
%-      None.
%-
  %- Handle optional parameter
  %inclSUB RTOSAdap_priv_getOptionalParameterEffectiveValue(genReentrantMethods,%'opt_arg_genReentrantMethods',arg_genReentrantMethods)
  %-
  %if arg_genReentrantMethods == 'yes'
    %- Reentrant methods are required, temporary variable need to be generated
    %-
    %- Temporary variable definition
    %THREAD %'arg_tempVarDeclarationThread' SELECT
%'arg_returnType' returnValue;
    %THREAD %'arg_tempVarDeclarationThread' UNSELECT
    %- Temporary variable assign
returnValue =
    %THREAD %'arg_returnValueExprThread' INSERT DESTROY
%++;
    %- End of reentrant section
%inclSUB RTOSAdap_genReentrantMethodEnd()
    %- Return temporary variable
return returnValue;
    %-
  %elif arg_genReentrantMethods == 'no'
    %- Reentrant methods are not required, generated "return" directly
return
    %THREAD %'arg_returnValueExprThread' INSERT DESTROY
    %++;
    %-
  %else
    %error! Invalid value of parameter 'opt_arg_genReentrantMethods' ("%'arg_genReentrantMethods'")
  %endif
%SUBROUTINE_END
%-
%-
%------------------------------------------------------------------------------
%-SUBROUTINE RTOSAdap_genCriticalSectionBegin(opt_arg_genReentrantMethods)
%------------------------------------------------------------------------------
%- Generates the code for entering the critical section.
%- All code inside the critical section is protected against interrupting by any
%- interrupt. It is typically implemented as disabling of interrupts.
%-
%- Note: If reentrancy of methods (see RTOSAdap_genReentrantMethodBegin()) is handled
%-       by disabling interrupts and methods are required to be reentrant,
%-       may not generate any code.
%-
%- The generated code must be in pair with the code generated by RTOSAdap_genCriticalSectionEnd().
%- The critical section must be always dynamically nested within the code generated by RTOSAdap_genReentrantMethodBegin()/End()
%- (even if reentrant methods are not required).
%-
%- Note: It is supported that critical sections can be dynamically nested (e.g. there can be
%-       called a method containing critical section from different critical section).
%-
%- Input parameters:
%-      opt_arg_genReentrantMethods
%-                              - Contains 'yes' or 'no' if reentrant methods are or aren't
%-                                required for the current LDD component.
%-                                It is used only if generation of critical sections can be
%-                                optimized regarding this parameter.
%-                                Optional parameter. If not specified, default value specified by RTOSAdap_setDefaultParameter() is used.
%-
%-
%------------------------------------------------------------------------------
%-SUBROUTINE RTOSAdap_genCriticalSectionEnd(opt_arg_genReentrantMethods)
%------------------------------------------------------------------------------
%- Pair subroutine to RTOSAdap_genCriticalSectionBegin().
%-
%-
%------------------------------------------------------------------------------
%-SUBROUTINE RTOSAdap_genIoctlCommandConstantDefinition(arg_commandName,arg_commandLogicalIndex,opt_arg_simpleComponentType)
%------------------------------------------------------------------------------
%- Generates proper definition of constant of command for Ioctl() method for specified
%- component type. Every command has its unique logical index (which need not match the value
%- of generated constant) and name. The name of command is a base for construction
%- of the name of the constant. Command parameter coming into Ioctl() method should
%- be compared with generated constant value, not with logical index.
%-
%- See also RTOSAdap_getIoctlCommandConstantName().
%-
%- Input parameters:
%-      arg_commandName         - Name of the command in uppercase/underscore format.
%-                                Should not contain any prefixes nor suffixes (e.g. component type prefix).
%-                                E.g.: START_SINGLE_MEASUREMENT.
%-      arg_commandLogicalIndex - Index of the Ioctl command which should be unique within
%-                                one component type. Command indices are not required to form
%-                                continuous sequence. Logical index is counted from zero.
%-      opt_arg_simpleComponentType
%-                              - Simple type of the component to generate IOCTL command constante definition.
%-                                Optional parameter. If not specified, default value specified by RTOSAdap_setDefaultParameter() is used.
%-
%-
%------------------------------------------------------------------------------
%-SUBROUTINE RTOSAdap_getIoctlCommandConstantName(arg_commandName,opt_arg_simpleComponentType,out_constantName)
%------------------------------------------------------------------------------
%- Returns the name of constant representing the specified command of Ioctl() method for
%- specified component type.
%-
%- See also RTOSAdap_genIoctlCommandConstantDefinition().
%-
%- Input parameters:
%-      arg_commandName         - Name of the command in uppercase/underscore format.
%-                                Should not contain any prefixes nor suffixes (e.g. component type prefix).
%-                                E.g.: START_SINGLE_MEASUREMENT.
%-      opt_arg_simpleComponentType
%-                              - Simple type of the component handling the specified Ioctl command.
%-                                Optional parameter. If not specified, default value specified by RTOSAdap_setDefaultParameter() is used.
%-
%- Outputs:
%-      out_constantName        - The name of constant generated by RTOSAdap_genIoctlCommandConstantDefinition()
%-                                including all prefixes and suffixes.
%-                                (Parameter contains the name of PE macro language symbol into which the result is placed)
%-
%-
%------------------------------------------------------------------------------
%-SUBROUTINE RTOSAdap_genRTOSIncludes()
%------------------------------------------------------------------------------
%- Generates proper #includes to access RTOS API. It is necessary to process
%- these includes before any code generated from RTOS adapter occurs.
%-
%-
%------------------------------------------------------------------------------
%-SUBROUTINE RTOSAdap_genRTOSDriverIncludes()
%------------------------------------------------------------------------------
%- Generates proper #includes to access RTOS driver's API. It is necessary to process
%- these includes before code generated by RTOSAdap_genDriverInstallationsFunctionDefinition()
%- occurs.
%-
%-
%------------------------------------------------------------------------------
%-SUBROUTINE RTOSAdap_genPublishedRTOSSettings()
%------------------------------------------------------------------------------
%- Generates code for publishing design-time RTOS specific settings
%- (if there are any RTOS component properties what need to be published
%- in the generated code). E.g.: Some RTOS configuration properties need to be
%- published by C constants in PE_LDD.h file.
%-
%-