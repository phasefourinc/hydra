/*!
    @page uart_pal_echo_s32k148_group UART PAL ECHO
    @brief Basic application that presents the project scenarios for S32 SDK

    ## Application description ##
    _____
    The purpose of this demo is to show the user how UART PAL works over FLEXIO_UART or LPUART peripherals.
    The user can choose whether to use FLEXIO_UART or LPUART (see USE_FLEXIO_UART define from example code).
    The board sends a welcome message to the console with further instructions.
    If 'Hello!' string is sent from the console, the board will reply with 'Hello World!'; when any other string is sent from the console, it will be echoed back.
    Green led(devkit) or led 1(Motherboard) shall be turned on if the communication is done over FLEXIO_UART; similarly the led shall be turned off if the communication is done over LPUART.

    ## Prerequisites ##
    _____
    To run the example you will need to have the following items:
    - 1 S32K148 board
    - 1 Power Adapter 12V (if the board cannot be powered from the USB port)
    - 2 Dupont male to male cable
    - 1 Personal Computer
    - 1 Jlink Lite Debugger (optional, users can use Open SDA)
    - UART to USB converter if it is not included on the target board. (Please consult your boards documentation to check if UART-USB converter is present).

    ## Boards supported ##
    _____
    The following boards are supported by this application:
    - S32K148EVB-Q144
    - S32K148-MB

    ## Hardware Wiring ##
    _____
    The following connections must be done to for this example application to work:

    PIN FUNCTION                  | S32K148EVB-Q144                 | S32K148-MB                      |
    ------------------------------|---------------------------------|---------------------------------|
    GREEN_LED (\b PTE22)          | RGB_GREEN - wired on the board  |                                 |
    LED1 (\b PTC1)                |                                 | JP50 - jump 50 on motherboard   |
    LPUART1 TX (\b PTC7)          | UART_TX - wired on the board    | J11.26 - J20.2                  |
    LPUART1 RX (\b PTC6)          | UART_RX - wired on the board    | J11.25 - J20.5                  |
    FLEXIO_UART RX  (\b PTA0)     | J6.11  - J4.15                  | J9.31 - J20.2                   |
    FLEXIO_UART TX  (\b PTA11)    | J6.8 - J5.6                     | J9.22 - J20.5                   |

    ## How to run ##
    _____
    #### 1. Importing the project into the workspace ####
    After opening S32 Design Studio, go to \b File -> \b New \b S32DS \b Project \b From... and select \b uart_pal_echo_s32k148. Then click on \b Finish. \n
    The project should now be copied into you current workspace.
    #### 2. Generating the Processor Expert configuration ####
    First go to \b Project \b Explorer View in S32 DS and select the current project(\b uart_pal_echo_s32k148). Then go to \b Project and click on \b Generate \b Processor \b Expert \b Code \n
    Wait for the code generation to be completed before continuing to the next step.
    #### 3. Building the project ####
    Select the configuration to be built \b FLASH (Debug_FLASH) or \b RAM (Debug_RAM) by left clicking on the downward arrow corresponding to the \b build button(@image hammer.png).
    Wait for the build action to be completed before continuing to the next step.
    #### 4. Running the project ####
    Go to \b Run and select \b Debug \b Configurations. There will be four debug configurations for this project:
     Configuration Name                                 | Description
     ---------------------------------------------------|------------------------------------------------------------
     \b uart_pal_echo_s32k148_debug_ram_jlink     | Debug the RAM configuration using Segger Jlink debuggers
     \b uart_pal_echo_s32k148_debug_flash_jlink   | Debug the FLASH configuration using Segger Jlink debuggers
     \b uart_pal_echo_s32k148_debug_ram_pemicro   | Debug the RAM configuration using PEMicro debuggers
     \b uart_pal_echo_s32k148_debug_flash_pemicro | Debug the FLASH configuration using PEMicro debuggers
    \n Select the desired debug configuration and click on \b Launch. Now the perspective will change to the \b Debug \b Perspective. \n
    Use the controls to control the program flow.

    @note For more detailed information related to S32 Design Studio usage please consult the available documentation.

    ## Notes ##
    ______
    For this example it is necessary to open a terminal emulator and configure it with:
    -   115200 baud
    -   One stop bit
    -   No parity
    -   No flow control
    -   '\\n' line ending
*/

