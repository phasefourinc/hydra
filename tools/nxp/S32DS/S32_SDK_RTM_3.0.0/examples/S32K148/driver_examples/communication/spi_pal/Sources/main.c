/*
 * Copyright (c) 2015 - 2016 , Freescale Semiconductor, Inc.
 * Copyright 2016-2017 NXP
 * All rights reserved.
 *
 * THIS SOFTWARE IS PROVIDED BY NXP "AS IS" AND ANY EXPRESSED OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL NXP OR ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */
/* ###################################################################
**     Filename    : main.c
**     Project     : spi_pal_s32k148
**     Processor   : S32K148_144
**     Version     : Driver 01.00
**     Compiler    : GNU C Compiler
**     Date/Time   : 2017-08-02, 16:28, # CodeGen: 0
**     Abstract    :
**         Main module.
**         This module contains user's application code.
**     Settings    :
**     Contents    :
**         No public methods
**
** ###################################################################*/
/*!
** @file main.c
** @version 01.00
** @brief
**         Main module.
**         This module contains user's application code.
*/
/*!
**  @addtogroup main_module main module documentation
**  @{
*/
/* MODULE main */
/* Including needed modules to compile this module/procedure */
#include "Cpu.h"
#include "pin_mux.h"
#include "spi1.h"
#include "dmaController1.h"
#include "spi2.h"
#include "clockMan1.h"

volatile int exit_code = 0;
/* User includes (#include below this line is not maintained by Processor Expert) */

#include <stdint.h>
#include <stdbool.h>

#define NUMBER_OF_FRAMES    10U
#define TIMEOUT             1000UL   /* 1 second interval */
#define BUFFER_SIZE         10
#define RED_LED             (1<<21U)    /* RED_LED  PTE[22] */
#define BLUE_LED            (1<<23U)    /* BLUE_LED  PTE[23] */
#define GPIO_LED            PTE

/*!
  \brief The main function for the project.
  \details The startup initialization sequence is the following:
 * - startup asm routine
 * - main()
*/
int main(void)
{
  /* Write your local variable definition here */
    uint8_t master_send[BUFFER_SIZE] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
    uint8_t master_receive[BUFFER_SIZE];
    uint8_t slave_send[BUFFER_SIZE] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
    uint8_t slave_receive[BUFFER_SIZE];
    uint8_t i = 0;
    uint8_t frame_sent = 1;

  /* Write configuration for master using flexio */
    extension_flexio_for_spi_t extension;
    extension.misoPin = 0;
    extension.mosiPin = 1;
    extension.sckPin = 2;
    extension.ssPin = 3;
    MasterConfig0.extension = &extension;

  /*** Processor Expert internal initialization. DON'T REMOVE THIS CODE!!! ***/
  #ifdef PEX_RTOS_INIT
    PEX_RTOS_INIT();                   /* Initialization of the selected RTOS. Macro is defined by the RTOS component. */
  #endif
  /*** End of Processor Expert internal initialization.                    ***/

    /* Initialize and configure clocks
     *  -   see clock manager component for details
     */
    CLOCK_SYS_Init(g_clockManConfigsArr, CLOCK_MANAGER_CONFIG_CNT, g_clockManCallbacksArr, CLOCK_MANAGER_CALLBACK_CNT);
    CLOCK_SYS_UpdateConfiguration(0U, CLOCK_MANAGER_POLICY_AGREEMENT);

    /* Initialize pins
     *  -   See PinSettings component for more info
     */
    PINS_DRV_Init(NUM_OF_CONFIGURED_PINS, g_pin_mux_InitConfigArr);
    PINS_DRV_ClearPins(GPIO_LED, RED_LED);

    /* Initialize SPI0 */
    SPI_MasterInit(&spi1Instance, &MasterConfig0);
     /* Initialize SPI1 */
    SPI_SlaveInit(&spi2Instance, &SlaveConfig0);

    /* Infinite loop
     * Do nothing, when the timeout is reached it generates an
     * interrupt which toggles a led.
     */
    while(1)
    {
        SPI_SlaveTransfer(&spi2Instance, slave_send, slave_receive, NUMBER_OF_FRAMES);
        SPI_MasterTransferBlocking(&spi1Instance, master_send, master_receive, (uint8_t) NUMBER_OF_FRAMES, (uint8_t) TIMEOUT);
        /* verify that the data received is correct */
        frame_sent = 1;
        for(i = 0U; i < BUFFER_SIZE; i++)
        {
            if((slave_send[i] != master_receive[i]) || (master_send[i] != slave_receive[i]))
            {
                frame_sent = 0;
                break;
            }
        }
        if(frame_sent == 0)
        {
            PINS_DRV_ClearPins(GPIO_LED, RED_LED);
        }
        else
        {
            PINS_DRV_SetPins(GPIO_LED, RED_LED);
        }
    }
    /*** Don't write any code pass this line, or it will be deleted during code generation. ***/
  /*** RTOS startup code. Macro PEX_RTOS_START is defined by the RTOS component. DON'T MODIFY THIS CODE!!! ***/
  #ifdef PEX_RTOS_START
    PEX_RTOS_START();                  /* Startup of the selected RTOS. Macro is defined by the RTOS component. */
  #endif
  /*** End of RTOS startup code.  ***/
  /*** Processor Expert end of main routine. DON'T MODIFY THIS CODE!!! ***/
  for(;;) {
    if(exit_code != 0) {
      break;
    }
  }
  return exit_code;
  /*** Processor Expert end of main routine. DON'T WRITE CODE BELOW!!! ***/
} /*** End of main routine. DO NOT MODIFY THIS TEXT!!! ***/

/* END main */
/*!
** @}
*/
/*
** ###################################################################
**
**     This file was created by Processor Expert 10.1 [05.21]
**     for the Freescale S32K series of microcontrollers.
**
** ###################################################################
*/
