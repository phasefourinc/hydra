/*
 * Copyright 2018 NXP
 * All rights reserved.
 *
 * THIS SOFTWARE IS PROVIDED BY NXP "AS IS" AND ANY EXPRESSED OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL NXP OR ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */
/*
 * Required equipment:
 * S32K148 EVB
 * Variable voltage power supply (0 - 18V)
 * Jumper wires
 * USB 2.0 A-male to micro B cable
 *
 * Example description:
 * This example demonstrates, how to handle low-power modes on MCU in conjunction
 * with the SBC. The user is informed via Terminal about the status etc. and whether
 * a user response is required.
 * At the beginning the application asks to flash the MTPVN. Afterwards the SBC
 * is configured and put into normal mode periodically triggering the SBC watchdog.
 * After SW3 button is pushed, the SBC mode is changed to stanby, the watchdog
 * is set into autonomous mode and the MCU is switched to STOP1 mode.
 * To wake-up the device, either SW3 or SW6 button can be used to wake-up the
 * MCU. After wake-up the MCU reconfigures SBCs watchdog back to window mode
 * and switches the SBC into normal operation.
 * A red LED is toggled on every watchdog reset. This demonstrates that the MCU
 * has been switched to STOP1 mode, as toggling will stop.
 *
 * EVB revisions:
 * Board Rev-A has the MOSI & MISO lines switched. This is fixed in Rev-C.
 * In code, right after includes section a macro defining the Rev-A board is
 * defined - EVB_REV_A. If defined, the MOSI/MISO lines are switched by in SW -
 * LPSPI feature.
 *
 * Programming MCU while SBC non-volatile memory not programmed:
 * While the SBC is in FNMC mode, the reset line is not influenced by the SBC and
 * the voltage regulator provides voltage for powering the MCU. From this perspective
 * the jumpers on the EVB can stay as is:
 * J18: 1-2
 * J7: 2-3
 * J8: 1-2
 *
 * Programming MCU while SBC non-volatile memory programmed:
 * While the SBC is in non-FNMC mode, the device starts to pull the reset line.
 * Starting a debug session etc. requiring a reset by a debug probe will cause
 * the SBC to trigger reset again after the line is released by the probe. This
 * interference disallows connecting to the device and will lead to an error.
 * There are three options how to re-flash the device:
 *
 * 1) Cut the reset line between SBC & MCU and solder a jumper/switch on the board
 * with interrupted reset line + power the MCU from openSDA - J8: 2-3 - don't
 * use this setup for production testing. The MCU has to be powered from SBC!
 * 2) Cut the reset line between SBC & MCU and solder a jumper/switch on the board
 * with interrupted reset line + power the SBC with a lower voltage - 6.5V
 * recommended.
 * 3) Erase the SBCs MTPNV memory - make sure the main application won't program
 * the memory right after start!!!
 *
 * Interrupting the reset line:
 * It's recommended to interrupt the reset line between SBC and MCU. This can be
 * achieved by desoldering the R547 0 Ohm resistor and solder a header pin for
 * a jumper.
 *
 * Erasing the MTPNV memory:
 * Connect J11-1 (CANH) to J13-2 (LIN1 VBAT)
 * Connect J11-2 (CANL) to J11-4 (GND)
 * Push and hold down the reset button SW5
 * Turn off the 12V power supply
 * Turn on the 12V power supply
 * Release the reset button and remove the jumper wires
 * The MTPNV is erased, ready to be programmed
 *
 * Preconditions after the MCU has been flashed:
 * J8 jumper on 1-2 (P5V0 powered from P5V0_V1SBC)
 * EVB connected to PC via micro-usb debug interface
 * EVB connected to 12V power supply using the on board jack connector - SBC power supply
 * Installed terminal - Putty, Termite etc. with following configuration:
 * Serial, Speed (baud) 115200, 8 data bits, 1 stop bit, no parity, no Flow control,
 * COM port corresponding to Virtual COM Port of your EVB, terminal echo enabled,
 * so the user can write on the console.
 * Clear the MTPNV memory, before example usage.
 *
 * Hints:
 * 1) While debugging, it's useful to cut the reset line between SBC and MCU.
 * However, this leads to desynchronized startup on MCU and SBC - SBC drives the
 * reset line. In case the SBC is in non-FNMC & non-SDMC mode, the watchdog is
 * enabled by default. If the SBC is started earlier than the MCU, it may happen
 * the watchdog timeout will elapse and will lead to SBC reset incrementing the
 * error counter. After three resets the SBC will go into fail-safe mode disabling
 * all outputs and enabling the limp home pin - yellow LED lights up.
 * In the latter case, SBC is started after MCU, it may happen some initialization
 * of SBC is skipped, which can lead to unexpected behavior and end up in
 * SBC disabling all outputs and setting the limp home output.
 *
 * 2) Make sure the reset line is not interrupted as recommended, while erasing
 * the MTPNV content as the SW5 button is not connected to the SBC in such case.
 */

/* Including needed modules to compile this module/procedure */
#include "Cpu.h"
#include "clockMan1.h"
#include "pin_mux.h"
#include "dmaController1.h"
#include "sbc_uja113x_conf.h"
#include "lpspiCom1.h"
#include "lpuart1.h"
/* #include "lpspiCom2.h" */
#if CPU_INIT_CONFIG
  #include "Init_Config.h"
#endif

volatile int exit_code = 0;
/* User includes (#include below this line is not maintained by Processor Expert) */
#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include <stdint.h>
#include <stdbool.h>

/* Comment the EVB_REV_A macro in case of higher revision */
//#define EVB_REV_A

/* SBC library helper macros */
#define SBC_UJA113X     0U
#define READ            true
#define WRITE           false

#define UJA113X_TOTAL_RESET_SOURCES   22U
/* Board helper macros */
#define INTN1_PIN       19U
#define INTN2_PIN       20U
#define LED_PIN_RED     21U
#define LED_PIN_BLUE    23U

#define MODE_RUN        0U
#define MODE_STOP1      1U

/* Reset source macros */
#define UJA113X_MSBCS_RSS_POWER_ON            0x00U
#define UJA113X_MSBCS_RSS_CAN_WUP             0x01U
#define UJA113X_MSBCS_RSS_LIN1_WUP            0x02U
#define UJA113X_MSBCS_RSS_LIN2_WUP            0x03U
#define UJA113X_MSBCS_RSS_HVIO1_WUP           0x04U
#define UJA113X_MSBCS_RSS_HVIO2_WUP           0x05U
#define UJA113X_MSBCS_RSS_HVIO3_WUP           0x06U
#define UJA113X_MSBCS_RSS_HVIO4_WUP           0x07U
#define UJA113X_MSBCS_RSS_HVIO5_WUP           0x08U
#define UJA113X_MSBCS_RSS_HVIO6_WUP           0x09U
#define UJA113X_MSBCS_RSS_HVIO7_WUP           0x0AU
#define UJA113X_MSBCS_RSS_HVIO8_WUP           0x0BU
#define UJA113X_MSBCS_RSS_WDG_OVERFLOW_SLP    0x0CU
#define UJA113X_MSBCS_RSS_DIAG_WUP            0x0DU
#define UJA113X_MSBCS_RSS_WDG_TRG_EARLY       0x0EU
#define UJA113X_MSBCS_RSS_WDG_OVERFLOW        0x0FU
#define UJA113X_MSBCS_RSS_WDG_ILLEGAL         0x10U
#define UJA113X_MSBCS_RSS_RSTN_EXTERNAL       0x11U
#define UJA113X_MSBCS_RSS_OVERLOAD_LEAVE      0x12U
#define UJA113X_MSBCS_RSS_V1_UNDERVOLTAGE     0x13U
#define UJA113X_MSBCS_RSS_SLP_CMD_ILLEGAL     0x14U
#define UJA113X_MSBCS_RSS_FSP_WUP             0x15U

/*
 * Stop request flag determines whether transition to stop mode is requested.
 * On SW3 push button event the flag is set to true.
 */
volatile bool stop_mode_request = false;
volatile bool mcu_mode = MODE_RUN;

uja113x_wtdc_cfg_t  wtdcConfig;

/* Text buffer used for terminal I/O */
char     message[128U];
uint8_t  reset_source;

/* Additional functions used in the example */
void LedBlinkRed(void);
void LedBlinkBlue(void);
void PORTE_Isr(void);
void PORTC_Isr(void);
void HandleGlobalStatus(void);
void Print_ResetSource(uint8_t rst_src);
void WakeupRoutine(void);

/*
 * @brief Application starting point
 */
int main(void)
{
/*** Processor Expert internal initialization. DON'T REMOVE THIS CODE!!! ***/
  #ifdef PEX_RTOS_INIT
    PEX_RTOS_INIT();                 /* Initialization of the selected RTOS. Macro is defined by the RTOS component. */
  #endif
  /*** End of Processor Expert internal initialization.                    ***/
  status_t  retval;
  uint8_t   txBuffer[4U]    = {0U};
  uint8_t   rxBuffer[4U]    = {0U};
  uint8_t   user_input[16U] = {0U};

  uja113x_drv_cfg_t   uja1132F5V;
  uja113x_hvio_cfg_t  hvioCfg;

  /* Configure clock based on PEx setup */
  retval = CLOCK_SYS_Init(g_clockManConfigsArr, CLOCK_MANAGER_CONFIG_CNT,
            g_clockManCallbacksArr, CLOCK_MANAGER_CALLBACK_CNT);
  retval = CLOCK_SYS_UpdateConfiguration(0U, CLOCK_MANAGER_POLICY_AGREEMENT);

  /* Configure pins based on PEx setup */
  retval = PINS_DRV_Init(NUM_OF_CONFIGURED_PINS, g_pin_mux_InitConfigArr);

  /* Configure LPSPI */
  retval = LPSPI_DRV_MasterInit(LPSPICOM1,&lpspiCom1State,&lpspiCom1_MasterConfig0);
  LPSPI_DRV_MasterSetDelay(LPSPICOM1, 7,7,14);

#if defined(EVB_REV_A)
  /*
   * EVB Revision A has wrong MOSI/MISO interconnection, in such case use the
   * LPSPI feature and switch the SIN and SOUT by SW
   */
  LPSPI1->CFGR1 |= LPSPI_CFGR1_PINCFG(3U);
#endif

  /* Configure UART so we can communicate with a termimal */
  LPUART_DRV_Init(INST_LPUART1, &lpuart1_State, &lpuart1_InitConfig0);

  /* Add ISR for GPIOE port - INTN1 (PTE19) & INTN2 (PTE20) of SBC */
  INT_SYS_InstallHandler(PORTE_IRQn, PORTE_Isr, NULL);
  /* Add ISR for GPIOC port - User button SW3 */
  INT_SYS_InstallHandler(PORTC_IRQn, PORTC_Isr, NULL);

  /*
   * Make sure the priority of SysTick is the highest among all interrupts, as
   * delays based on SysTick are used across the library. If SysTick won't have
   * highest priority and a delay function is called in an interrupt, the time
   * counter would never be incremented and we would end up in an infinite loop
   * in the delay.
   * Enable PORTE interrupts, so we can react on errors caused by INTN1 & INTN2
   * outputs.
   */
  INT_SYS_SetPriority(SysTick_IRQn, 0U);
  INT_SYS_SetPriority(PORTE_IRQn, 1U);
  INT_SYS_SetPriority(PORTC_IRQn, 1U);
  INT_SYS_EnableIRQ(PORTE_IRQn);
  INT_SYS_EnableIRQ(PORTC_IRQn);

  uja1132F5V.chipType       = UJA1132HWFD5V0;
  uja1132F5V.lpspiInstance  = LPSPICOM1;
  uja1132F5V.timeout        = 500U;
  UJA113X_RegisterDevice(SBC_UJA113X, &uja1132F5V);

  /*
   * Read the MTPNV status register so we know, whether the device has been
   * programmed already or not
   */
  txBuffer[0U] = MTPNVS_ADDRESS;
  retval = UJA113X_SbcTransfer(SBC_UJA113X, READ, txBuffer, rxBuffer, 2U);
  if (STATUS_SUCCESS != retval)
  {
    /*
     * We don't perform any more checks in the example, as it's assumed once the
     * SPI communication works, there is no reason it won't work further.
     */
    sprintf(message, "Error in SPI communication\r\n");
    LPUART_DRV_SendDataBlocking(INST_LPUART1, (uint8_t *) message, strlen(message), 2000U);
    LedBlinkRed();
  }

  sprintf(message, "MTPNV status register (0x70) content\r\n");
  LPUART_DRV_SendDataBlocking(INST_LPUART1, (uint8_t *) message, strlen(message), 2000U);
  sprintf(message, "WRCNTS: %d\r\n", (rxBuffer[1U] & UJA113X_MTPNVS_WRCNTS_MASK) >> UJA113X_MTPNVS_WRCNTS_SHIFT);
  LPUART_DRV_SendDataBlocking(INST_LPUART1, (uint8_t *) message, strlen(message), 2000U);
  sprintf(message, "ECCS  : %d\r\n", (rxBuffer[1U] & UJA113X_MTPNVS_ECCS_MASK) >> UJA113X_MTPNVS_ECCS_SHIFT);
  LPUART_DRV_SendDataBlocking(INST_LPUART1, (uint8_t *) message, strlen(message), 2000U);
  sprintf(message, "NVMPS : %d\r\n", rxBuffer[1U] & UJA113X_MTPNVS_NVMPS_MASK);
  LPUART_DRV_SendDataBlocking(INST_LPUART1, (uint8_t *) message, strlen(message), 2000U);

  if (0U != (rxBuffer[1U] & UJA113X_MTPNVS_NVMPS_MASK))
  {
    sprintf(message, "Non-volatile memory not programmed...\r\n");
    LPUART_DRV_SendDataBlocking(INST_LPUART1, (uint8_t *) message, strlen(message), 2000U);
    sprintf(message, "Do you want to programm the MTPNV? Y/N:\r\n");
    LPUART_DRV_SendDataBlocking(INST_LPUART1, (uint8_t *) message, strlen(message), 2000U);
    do
    {
      LPUART_DRV_ReceiveDataPolling(INST_LPUART1, user_input, 1U);
    } while (('Y' != toupper(user_input[0U])) && ('N' != toupper(user_input[0U])));

    if ('Y' == toupper(user_input[0U]))
    {
      /* The MTPNV has not been programmed yet, write the MTPVN */
      sprintf(message, "Request for MTPNV programming has been raised, preparing MTPVN data...\r\n");
      LPUART_DRV_SendDataBlocking(INST_LPUART1, (uint8_t *) message, strlen(message), 2000U);
      uja113x_mtpnv_cfg_t mtpnvCfg;
      mtpnvCfg.highSideCtrl   = HHSDC_IO8HOC_ENABLED | HHSDC_IO7HOC_ENABLED |
                                HHSDC_IO6HOC_ENABLED | HHSDC_IO5HOC_ENABLED |
                                HHSDC_IO4HOC_ENABLED | HHSDC_IO3HOC_ENABLED |
                                HHSDC_IO2HOC_ENABLED | HHSDC_IO1HOC_ENABLED;
      mtpnvCfg.lowSideCtrl    = HLSDC_IO8LOC_ENABLED | HLSDC_IO7LOC_ENABLED |
                                HLSDC_IO6LOC_ENABLED | HLSDC_IO5LOC_ENABLED |
                                HLSDC_IO4LOC_ENABLED | HLSDC_IO3LOC_ENABLED |
                                HLSDC_IO2LOC_ENABLED | HLSDC_IO1LOC_ENABLED;

      mtpnvCfg.startupCtrl    = RLC_PULSE_WIDTH_20_25_MS | V2SUC_ALWAYS_OFF |
                                IO4SFC_STD_IO            | IO3SFC_STD_IO    |
                                IO2SFC_STD_IO;

      mtpnvCfg.sbcConfigCtrl  = V1RTSUC_90_NOM       | FNMC_DISABLE         |
                                VEXTAC_NO_PROTECTION | SLPC_SLEEP_SUPPORTED;

      sprintf(message, "Do you want to enable the SDMC mode? Y/N:\r\n");
      LPUART_DRV_SendDataBlocking(INST_LPUART1, (uint8_t *) message, strlen(message), 2000U);
      do
      {
        LPUART_DRV_ReceiveDataPolling(INST_LPUART1, user_input, 1U);
      } while (('Y' != toupper(user_input[0U])) && ('N' != toupper(user_input[0U])));

      if ('Y' == toupper(user_input[0U]))
      {
        sprintf(message, "Setting SDMC mode...\r\n");
        LPUART_DRV_SendDataBlocking(INST_LPUART1, (uint8_t *) message, strlen(message), 2000U);
        mtpnvCfg.sbcConfigCtrl |= SDMC_ENABLE;
      }
      sprintf(message, "Writing MTPNV configuration into SBC:\r\n");
      LPUART_DRV_SendDataBlocking(INST_LPUART1, (uint8_t *) message, strlen(message), 2000U);
      sprintf(message, "0x71: %x\r\n", mtpnvCfg.highSideCtrl);
      LPUART_DRV_SendDataBlocking(INST_LPUART1, (uint8_t *) message, strlen(message), 2000U);
      sprintf(message, "0x72: %x\r\n", mtpnvCfg.lowSideCtrl);
      LPUART_DRV_SendDataBlocking(INST_LPUART1, (uint8_t *) message, strlen(message), 2000U);
      sprintf(message, "0x73: %x\r\n", mtpnvCfg.startupCtrl);
      LPUART_DRV_SendDataBlocking(INST_LPUART1, (uint8_t *) message, strlen(message), 2000U);
      sprintf(message, "0x74: %x\r\n", mtpnvCfg.sbcConfigCtrl);
      LPUART_DRV_SendDataBlocking(INST_LPUART1, (uint8_t *) message, strlen(message), 2000U);
      retval = UJA113X_SetMtpnv(SBC_UJA113X, &mtpnvCfg);
    }
  }
  else
  {
    sprintf(message, "Non-volatile memory already programmed...\r\n");
    LPUART_DRV_SendDataBlocking(INST_LPUART1, (uint8_t *) message, strlen(message), 2000U);
  }

  sprintf(message, "Reading watchdog status register address (0x05)...\r\n");
  LPUART_DRV_SendDataBlocking(INST_LPUART1, (uint8_t *) message, strlen(message), 2000U);
  txBuffer[0U] = WTDS_ADDRESS;
  UJA113X_SbcTransfer(SBC_UJA113X, READ, txBuffer, rxBuffer, 2U);
  if (0U != (rxBuffer[1U] & UJA113X_WDS_FNMS_MASK))
  {
    /* We are in FNMC mode, inidicate it by toggling Blue LED */
    sprintf(message, "SBC is in FNMC mode, end of program indicated by blue LED blinking.\r\n");
    LPUART_DRV_SendDataBlocking(INST_LPUART1, (uint8_t *) message, strlen(message), 2000U);
    LedBlinkBlue();
  }
  else
  {
    sprintf(message, "SBC is in non-FNMC mode, continue...\r\n");
    LPUART_DRV_SendDataBlocking(INST_LPUART1, (uint8_t *) message, strlen(message), 2000U);
  }

  /* Get the reset source and perform appropriate actions... */
  sprintf(message, "\r\nReading the reset source (0x03)...\r\n");
  LPUART_DRV_SendDataBlocking(INST_LPUART1, (uint8_t *) message, strlen(message), 2000U);
  txBuffer[0U] = MSBCS_ADDRESS;
  UJA113X_SbcTransfer(SBC_UJA113X, READ, txBuffer, rxBuffer, 2U);
  reset_source = rxBuffer[1U] & 0x1FU;
  Print_ResetSource(reset_source);

  HandleGlobalStatus();

  /*
   * After reset the INTNx is low due to some event. In this example we assume
   * it's related to wake-up or power-on. Let's clear dedicated flags so the
   * INTN1 & INTN2 are restored to HIGH.
   * In general, we should read the Global Interrupt Status register. Based on
   * it's content perform a subsequent read to figure out the status bit and
   * clear it.
   * Clear the pending interrupts we are aware of. In this case it's related to
   * reset caused by power-on or watchdog trigger in sleep mode. In general,
   * we should handle all the status flags, not just these two.
   */
  if (UJA113X_MSBCS_RSS_WDG_OVERFLOW_SLP == reset_source)
  {
    /* The WDI flag in System Interrupt status register (0x61) is set, clear it */
    sprintf(message, "Clearing the WDI flag in 0x61 register...\r\n");
    LPUART_DRV_SendDataBlocking(INST_LPUART1, (uint8_t *) message, strlen(message), 2000U);
    txBuffer[0U] = SYSIS_ADDRESS;
    txBuffer[1U] = UJA113X_SYSIS_WDI_MASK;
    UJA113X_SbcTransfer(SBC_UJA113X, WRITE, txBuffer, rxBuffer, 2U);
  }
  else if (UJA113X_MSBCS_RSS_POWER_ON == reset_source)
  {
    /* The POSI flag in System Interrupt status register (0x61) is set, clear it */
    sprintf(message, "Clearing the POSI flag in 0x61 register...\r\n");
    LPUART_DRV_SendDataBlocking(INST_LPUART1, (uint8_t *) message, strlen(message), 2000U);
    txBuffer[0U] = SYSIS_ADDRESS;
    txBuffer[1U] = UJA113X_SYSIS_POS_MASK;
    UJA113X_SbcTransfer(SBC_UJA113X, WRITE, txBuffer, rxBuffer, 2U);
  }
  else if (UJA113X_MSBCS_RSS_HVIO2_WUP == reset_source)
  {
    /* The IO2FEI flag in Bank 0 Wake-up Interrupt status register (0x64) is set, clear it */
    sprintf(message, "Clearing the IO2FEI flag in 0x64 register...\r\n");
    LPUART_DRV_SendDataBlocking(INST_LPUART1, (uint8_t *) message, strlen(message), 2000U);
    txBuffer[0U] = B0WUPIS_ADDRESS;
    txBuffer[1U] = UJA113X_BNWIS_IO26FEI_MASK;
    UJA113X_SbcTransfer(SBC_UJA113X, WRITE, txBuffer, rxBuffer, 2U);
  }
  else if (UJA113X_MSBCS_RSS_FSP_WUP == reset_source)
  {
    /*
     * We woke-up from Forced Sleep mode - in such case all regular interrupts
     * were enabled. In our case the wake-up came most probably from HVIO2, so
     * we need to clear both rising/falling interrupt pending status...
     */
    sprintf(message, "Clearing the IO2FEI | IO2REI flag in 0x64 register...\r\n");
    LPUART_DRV_SendDataBlocking(INST_LPUART1, (uint8_t *) message, strlen(message), 2000U);
    txBuffer[0U] = B0WUPIS_ADDRESS;
    txBuffer[1U] = UJA113X_BNWIS_IO26FEI_MASK | UJA113X_BNWIS_IO26REI_MASK;
    UJA113X_SbcTransfer(SBC_UJA113X, WRITE, txBuffer, rxBuffer, 2U);
  }
  else
  {
    /* Unhandled reset source!!! */
    sprintf(message, "Unhandled reset source! Don't know, what to do!!!\r\n");
    LPUART_DRV_SendDataBlocking(INST_LPUART1, (uint8_t *) message, strlen(message), 2000U);
  }

  /* Read Global Interrupt Status register and subsequent registers to get
   * info about pending interrups. This function doesn't clear any flags, its
   * just for debugging purpose. */
  HandleGlobalStatus();

  /* Read the RCC counter */
  txBuffer[0U] = FSC_ADDRESS;
  UJA113X_SbcTransfer(SBC_UJA113X, READ, txBuffer, rxBuffer, 2U);
  sprintf(message, "RCC: %d\r\n", 0x03U & rxBuffer[1U]);
  LPUART_DRV_SendDataBlocking(INST_LPUART1, (uint8_t *) message, strlen(message), 2000U);


  /* Set Watchdog - Window mode; 4096 ms period */
  sprintf(message, "\r\nUpdating the watchdog configuration (0x00):\r\n");
  LPUART_DRV_SendDataBlocking(INST_LPUART1, (uint8_t *) message, strlen(message), 2000U);
  sprintf(message, "Mode:   Window\r\n");
  LPUART_DRV_SendDataBlocking(INST_LPUART1, (uint8_t *) message, strlen(message), 2000U);
  sprintf(message, "Period: 4096 ms\r\n");
  LPUART_DRV_SendDataBlocking(INST_LPUART1, (uint8_t *) message, strlen(message), 2000U);
  wtdcConfig.wtdModeCtrl  = WMC_WINDOW;
  wtdcConfig.nomWtdPeriod = NWP_PERIOD_4096;
  retval = UJA113X_SetWatchdog(SBC_UJA113X, &wtdcConfig);

  /* Configure HVIO 2 as wake-up input */
  sprintf(message, "\r\nConfiguring HVIO 2 (0x31): \r\n");
  LPUART_DRV_SendDataBlocking(INST_LPUART1, (uint8_t *) message, strlen(message), 2000U);
  sprintf(message, "Shutdown control  : none\r\n");
  LPUART_DRV_SendDataBlocking(INST_LPUART1, (uint8_t *) message, strlen(message), 2000U);
  sprintf(message, "Activation control: HVIO2 is enabled\r\n");
  LPUART_DRV_SendDataBlocking(INST_LPUART1, (uint8_t *) message, strlen(message), 2000U);
  sprintf(message, "HVIO configuration: wake-up input\r\n");
  LPUART_DRV_SendDataBlocking(INST_LPUART1, (uint8_t *) message, strlen(message), 2000U);
  hvioCfg.ionShutdownCtrl = IONSC_NONE;
  hvioCfg.ionActivateCtrl = IONAC_ENABLED;
  hvioCfg.ionConfCtrl     = IONCC_WUP;
  retval = UJA113X_SetHvio(SBC_UJA113X, &hvioCfg, 1U);

  /*
   * Even though our wake-up source is watchdog, We need to enable a regular
   * interrupt. Otherwise the SBC will reset as the conditions for transition
   * into sleep mode won't be met.
   */
  retval = UJA113X_SetBankWupInterrupts(SBC_UJA113X, 0x08U, 0U);

  sprintf(message, "\r\nSwitching to normal mode...\r\n");
  LPUART_DRV_SendDataBlocking(INST_LPUART1, (uint8_t *) message, strlen(message), 2000U);
  retval = UJA113X_SetMode(SBC_UJA113X, MC_NORMAL);
  sprintf(message, "\r\nReading watchdog status periodically...\r\n");
  LPUART_DRV_SendDataBlocking(INST_LPUART1, (uint8_t *) message, strlen(message), 2000U);
  sprintf(message, "\r\nPress the SW3 button to put SBC into standby\r\n");
  LPUART_DRV_SendDataBlocking(INST_LPUART1, (uint8_t *) message, strlen(message), 2000U);
  sprintf(message, "To wake-up the MCU, press SW3 or SW6...\r\n");
  LPUART_DRV_SendDataBlocking(INST_LPUART1, (uint8_t *) message, strlen(message), 2000U);

  while (1U)
  {
    txBuffer[0U] = WTDS_ADDRESS;
    retval = UJA113X_SbcTransfer(SBC_UJA113X, READ, txBuffer, rxBuffer, 2U);

    if (0x02U == (UJA113X_WDS_WDS_MASK & rxBuffer[1U]))
    {
      /*
       * We are in second half of window, trigger (reset) watchdog
       * Triggering a watchdog means writing any valid value into the watchdog
       * register - write the same values, as we don't want to change watchdog
       * settings. Changing values in normal mode would lead to an error.
       */
      sprintf(message, "We are in second half of window, performing watchdog trigger...\r\n");
      LPUART_DRV_SendDataBlocking(INST_LPUART1, (uint8_t *) message, strlen(message), 2000U);
      retval = UJA113X_SetWatchdog(SBC_UJA113X, &wtdcConfig);

      PINS_DRV_TogglePins(PTE, (uint32_t)1U << LED_PIN_RED);

      if (true == stop_mode_request)
      {
        stop_mode_request = false;
        mcu_mode          = MODE_STOP1;

        sprintf(message, "\r\nSwitching SBC to standby mode...\r\n");
        LPUART_DRV_SendDataBlocking(INST_LPUART1, (uint8_t *) message, strlen(message), 2000U);
        UJA113X_SetMode(SBC_UJA113X, MC_STANDBY);
        sprintf(message, "Changing SBC watchdog mode to autonomous...\r\n");
        LPUART_DRV_SendDataBlocking(INST_LPUART1, (uint8_t *) message, strlen(message), 2000U);
        wtdcConfig.wtdModeCtrl = WMC_AUTONOMOUS;
        UJA113X_SetWatchdog(SBC_UJA113X, &wtdcConfig);
      }
    }
  }

  /*** Don't write any code pass this line, or it will be deleted during code generation. ***/
  /*** RTOS startup code. Macro PEX_RTOS_START is defined by the RTOS component. DON'T MODIFY THIS CODE!!! ***/
  #ifdef PEX_RTOS_START
    PEX_RTOS_START();                  /* Startup of the selected RTOS. Macro is defined by the RTOS component. */
  #endif
  /*** End of RTOS startup code.  ***/
  /*** Processor Expert end of main routine. DON'T MODIFY THIS CODE!!! ***/
  for(;;) {
    if(exit_code != 0) {
      break;
    }
  }
  return exit_code;
  /*** Processor Expert end of main routine. DON'T WRITE CODE BELOW!!! ***/
} /*** End of main routine. DO NOT MODIFY THIS TEXT!!! ***/

void LedBlinkRed(void)
{
  while (1U)
  {
    OSIF_TimeDelay(100U);
    PINS_DRV_TogglePins(PTE, (uint32_t)1U << LED_PIN_RED);
  }
}

void LedBlinkBlue(void)
{
  while (1U)
  {
    OSIF_TimeDelay(100U);
    PINS_DRV_TogglePins(PTE, (uint32_t)1U << LED_PIN_BLUE);
  }
}

/**
 * @brief   Handles user events from SW3 & SW4 push buttons.
 * @details Sets sleep request to true on SW3 user push button event and swichtes
 * the wake-up source on SW4 user push button event between watchdog & HVIO2.
 * The board allows the user to put the device into sleep on SW3 user
 * button push. A flag is set and once a watchdog trigger occures, a transition
 * into sleep mode is performed.\n
 */
void PORTC_Isr(void)
{
  if (0U != (((uint32_t)((uint32_t)1U << 12U)) & PORTC->ISFR))
  {
    PINS_DRV_ClearPinIntFlagCmd(PORTC, 12U);
    if (MODE_RUN == mcu_mode)
    {
      stop_mode_request = true;
    }
    else
    { /*
       * Wake-up from STOP1 mode. Switch Watchdog to window mode and change SBC
       * mode to Normal.
       */
      WakeupRoutine();
      mcu_mode = MODE_RUN;
    }
  }
}

/**
 * @brief   General function for reading all status registers
 * @details Reads the Global Interrupt Status register (GIS:0x60) and any status
 * registers which pending flag was set in GIS. Reports the result on terminal.
 */
void HandleGlobalStatus(void)
{
  uint8_t txBuffer[2U] = {GIS_ADDRESS, 0x00U};
  uint8_t rxBuffer[2U];
  uint8_t gis_status;

  UJA113X_SbcTransfer(SBC_UJA113X, READ, txBuffer, rxBuffer, 2U);
  gis_status = rxBuffer[1U];
  sprintf(message, "\r\nGIS: 0x%X\r\n", rxBuffer[1U]);
  LPUART_DRV_SendDataBlocking(INST_LPUART1, (uint8_t *) message, strlen(message), 2000U);

  if (0U != (gis_status & UJA113X_GIS_B1FIS_MASK))
  {
    gis_status &= ~UJA113X_GIS_B1FIS_MASK;
    txBuffer[0U] = B1FIS_ADDRESS;
    UJA113X_SbcTransfer(SBC_UJA113X, READ, txBuffer, rxBuffer, 2U);
    sprintf(message, "B1FIS: 0x%X\r\n", rxBuffer[1U]);
    LPUART_DRV_SendDataBlocking(INST_LPUART1, (uint8_t *) message, strlen(message), 2000U);
  }

  if (0U != (gis_status & UJA113X_GIS_B1WIS_MASK))
  {
    gis_status &= ~UJA113X_GIS_B1WIS_MASK;
    txBuffer[0U] = B1WUPIS_ADDRESS;
    UJA113X_SbcTransfer(SBC_UJA113X, READ, txBuffer, rxBuffer, 2U);
    sprintf(message, "B1WIS: 0x%X\r\n", rxBuffer[1U]);
    LPUART_DRV_SendDataBlocking(INST_LPUART1, (uint8_t *) message, strlen(message), 2000U);
  }

  if (0U != (gis_status & UJA113X_GIS_B0FIS_MASK))
  {
    gis_status &= ~UJA113X_GIS_B0FIS_MASK;
    txBuffer[0U] = B0FIS_ADDRESS;
    UJA113X_SbcTransfer(SBC_UJA113X, READ, txBuffer, rxBuffer, 2U);
    sprintf(message, "B0FIS: 0x%X\r\n", rxBuffer[1U]);
    LPUART_DRV_SendDataBlocking(INST_LPUART1, (uint8_t *) message, strlen(message), 2000U);
  }

  if (0U != (gis_status & UJA113X_GIS_B0WIS_MASK))
  {
    gis_status &= ~UJA113X_GIS_B0WIS_MASK;
    txBuffer[0U] = B0WUPIS_ADDRESS;
    UJA113X_SbcTransfer(SBC_UJA113X, READ, txBuffer, rxBuffer, 2U);
    sprintf(message, "B0WIS: 0x%X\r\n", rxBuffer[1U]);
    LPUART_DRV_SendDataBlocking(INST_LPUART1, (uint8_t *) message, strlen(message), 2000U);
    sprintf(message, "Clearing B0WIS register...\r\n");
    txBuffer[1U] = rxBuffer[1U];
    LPUART_DRV_SendDataBlocking(INST_LPUART1, (uint8_t *) message, strlen(message), 2000U);
    UJA113X_SbcTransfer(SBC_UJA113X, WRITE, txBuffer, rxBuffer, 2U);

    if (MODE_STOP1 == mcu_mode)
    {
      WakeupRoutine();
      mcu_mode = MODE_RUN;
    }
  }

  if (0U != (gis_status & UJA113X_GIS_TRXIS_MASK))
  {
    gis_status &= ~UJA113X_GIS_TRXIS_MASK;
    txBuffer[0U] = TIS_ADDRESS;
    UJA113X_SbcTransfer(SBC_UJA113X, READ, txBuffer, rxBuffer, 2U);
    sprintf(message, "TRXIS: 0x%X\r\n", rxBuffer[1U]);
    LPUART_DRV_SendDataBlocking(INST_LPUART1, (uint8_t *) message, strlen(message), 2000U);
  }

  if (0U != (gis_status & UJA113X_GIS_SUPIS_MASK))
  {
    gis_status &= ~UJA113X_GIS_SUPIS_MASK;
    txBuffer[0U] = SUPIS_ADDRESS;
    UJA113X_SbcTransfer(SBC_UJA113X, READ, txBuffer, rxBuffer, 2U);
    sprintf(message, "SUPIS: 0x%X\r\n", rxBuffer[1U]);
    LPUART_DRV_SendDataBlocking(INST_LPUART1, (uint8_t *) message, strlen(message), 2000U);
  }

  if (0U != (gis_status & UJA113X_GIS_SYSIS_MASK))
  {
    gis_status &= ~UJA113X_GIS_SYSIS_MASK;
    txBuffer[0U] = SYSIS_ADDRESS;
    UJA113X_SbcTransfer(SBC_UJA113X, READ, txBuffer, rxBuffer, 2U);
    sprintf(message, "SYSIS: 0x%X\r\n", rxBuffer[1U]);
    LPUART_DRV_SendDataBlocking(INST_LPUART1, (uint8_t *) message, strlen(message), 2000U);
  }

  if (0U != gis_status)
  {
    sprintf(message, "\r\nUnknown interrupt state!\r\n");
    LPUART_DRV_SendDataBlocking(INST_LPUART1, (uint8_t *) message, strlen(message), 2000U);
  }
}

/**
 * @brief   INTN1 & INTN2 interrupt service routine
 * @details In case of an event/error, the INTN1 or/and INTN2 lines are driven
 * low. The MCU reacts on the event and reads the Global Interrupt Status (GIS:0x60)
 * register and subsequent registers to get the root cause of an event/error.
 */
void PORTE_Isr(void)
{
  sprintf(message, "\r\nPORTE IRQ:");
  LPUART_DRV_SendDataBlocking(INST_LPUART1, (uint8_t *) message, strlen(message), 2000U);
  HandleGlobalStatus();

  PINS_DRV_ClearPinIntFlagCmd(PORTE, INTN1_PIN);
  PINS_DRV_ClearPinIntFlagCmd(PORTE, INTN2_PIN);
}

char * message_array_rst[UJA113X_TOTAL_RESET_SOURCES] =
{
    "Power on reset.\r\n",
    "CAN wake-up detected in Sleep mode.\r\n",
    "LIN1 wake-up detected in Sleep mode.\r\n",
    "LIN2 wake-up detected in Sleep mode.\r\n",
    "HVIO1 wake-up detected in Sleep mode.\r\n",
    "HVIO2 wake-up detected in Sleep mode.\r\n",
    "HVIO3 wake-up detected in Sleep mode.\r\n",
    "HVIO4 wake-up detected in Sleep mode.\r\n",
    "HVIO5 wake-up detected in Sleep mode.\r\n",
    "HVIO6 wake-up detected in Sleep mode.\r\n",
    "HVIO7 wake-up detected in Sleep mode.\r\n",
    "HVIO8 wake-up detected in Sleep mode.\r\n",
    "Watchdog overflow in sleep mode.\r\n",
    "Diagnostic wake-up in sleep mode.\r\n",
    "Watchdog triggered to early.\r\n",
    "Watchdog overflow.\r\n",
    "Illegal watchdog mode control access.\r\n",
    "RSTN pulled down externally.\r\n",
    "Leaving overload mode.\r\n",
    "V1 undervoltage event.\r\n",
    "Illegal sleep mode command received.\r\n",
    "Wake-up after leaving FSP mode.\r\n"
};

/**
 * @brief Prints the reset source on terminal.
 * @param[in] rst_src The reset source code (0 - 21)
 */
void Print_ResetSource(uint8_t rst_src)
{
  if (UJA113X_TOTAL_RESET_SOURCES > rst_src)
  {
    LPUART_DRV_SendDataBlocking(INST_LPUART1,
                                (uint8_t *) message_array_rst[rst_src],
                                strlen(message_array_rst[rst_src]),
                                2000U);
  }
  else
  {
    sprintf(message, "Unknown/Invalid reset source...\r\n");
    LPUART_DRV_SendDataBlocking(INST_LPUART1, (uint8_t *) message, strlen(message), 2000U);
  }
}

/**
 * @brief Switches watchdog to window mode and switches the SBC mode to normal...
 */
void WakeupRoutine(void)
{
  sprintf(message, "\r\nWake-up from STOP1 mode...\r\n");
  LPUART_DRV_SendDataBlocking(INST_LPUART1, (uint8_t *) message, strlen(message), 2000U);
  sprintf(message, "Changing SBC watchdog mode to window...\r\n");
  LPUART_DRV_SendDataBlocking(INST_LPUART1, (uint8_t *) message, strlen(message), 2000U);
  wtdcConfig.wtdModeCtrl = WMC_WINDOW;
  UJA113X_SetWatchdog(SBC_UJA113X, &wtdcConfig);
  sprintf(message, "Switching SBC to normal mode...\r\n");
  LPUART_DRV_SendDataBlocking(INST_LPUART1, (uint8_t *) message, strlen(message), 2000U);
  UJA113X_SetMode(SBC_UJA113X, MC_NORMAL);
}
/* END main */
/*!
** @}
*/
/*
** ###################################################################
**
**     This file was created by Processor Expert 10.1 [05.21]
**     for the Freescale S32K series of microcontrollers.
**
** ###################################################################
*/
