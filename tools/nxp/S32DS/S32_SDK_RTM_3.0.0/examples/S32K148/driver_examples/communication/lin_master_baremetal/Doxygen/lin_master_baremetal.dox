/*!
    @page lin_master_baremetal_s32k148_group LIN MASTER
    @brief Example that shows the usage of the LIN driver in master mode

    ## Application description ##
    __________
   This example demonstrates the LIN communication between S32K148 EVB Master and Slave using LIN driver without LIN Stack

    - A frame contains header and data. The Master node can send header and data, but Slave node only can send data.
    Base on header, Master node or Slave node will take corresponding action.
    On Master node:
    - Press BUTTON 0:
        - For the first time, Master node sends FRAME_MASTER_RECEIVE_DATA header and require slave node responds by sending data (txBuff2).
        - For the second time, Master sends FRAME_SLAVE_RECEIVE_DATA header, then continue sending data (txBuff1) and slave node will receive the data.
        - If node successful receives data, this node will turn on GREEN_LED, otherwise turn on RED_LED.
    - Press BUTTON 1:
        - Master node will check current node state. If the state is LIN_NODE_STATE_SLEEP_MODE, Master node will send wakeup signal and BLUE_LED will be turned on both nodes, otherwise Master node will send header to set Master node and Slave node to sleep mode and all LED will be turned off both nodes.

    ## Prerequisites ##
    __________
    To run the example you will need to have the following items:
    - 1 S32K148 board
    - 1 Power Adapter 12V
    - 4 Dupont female to female cable
    - 1 Personal Computer
    - 1 Jlink Lite Debugger (optional, users can use Open SDA)

    ## Boards supported ##
    __________
    The following boards are supported by this application:
    - S32K148EVB-Q144
    - S32K-MB

    ## Hardware Wiring ##
    __________
    The following connections must be done to for this example application to work:

    PIN FUNCTION         |   S32K148EVB-Q144              |   S32K-MB
    ---------------------|--------------------------------|-----------------------------
    BUTTON 0 (\b PTC12)  | SW3 - wired on the board       | BTN2 - wired on the board
    BUTTON 1 (\b PTC13)  | SW4 - wired on the board       | BTN3 - wired on the board
    RED_LED (\b PTE21)   | RGB_RED - wired on the board   | J12.17 - J11.31
    GREEN_LED (\b PTE22) | RGB_GREEN - wired on the board | J12.16 - J11.30
    BLUE_LED (\b PTE23)  | RGB_BLUE - wired on the board  | J12.31 - J11.29
    GND (\b GND)         | J17-4 - Slave GND              | J6 - Slave GND
    LIN (\b *)           | J17-1 - Slave LIN              | J48.4 - Slave LIN

    <b>(*) Those lines must be modulated using a transceiver, if it is not specified the boards already include the LIN transceiver. This example use LIN 2 interface on S32K148EVB-Q144.</b>

    ## How to run ##
    __________
    #### 1. Importing the project into the workspace ####
    After opening S32 Design Studio, go to \b File -> \b New \b S32DS \b Project \b From... and select \b lin_master_baremetal_s32k148. Then click on \b Finish. \n
    The project should now be copied into you current workspace.
    #### 2. Generating the Processor Expert configuration ####
    First go to \b Project \b Explorer View in S32 DS and select the current project(\b lin_master_baremetal_s32k148). Then go to \b Project and click on \b Generate \b Processor \b Expert \b Code \n
    Wait for the code generation to be completed before continuing to the next step.
    #### 3. Building the project ####
    Select the configuration to be built \b FLASH (Debug_FLASH) or \b RAM (Debug_RAM) by left clicking on the downward arrow corresponding to the \b build button(@image hammer.png).
    Wait for the build action to be completed before continuing to the next step.
    #### 4. Running the project ####
    Go to \b Run and select \b Debug \b Configurations. There will be four debug configurations for this project:
     Configuration Name | Description
     -------------------|------------
     \b lin_master_baremetal_s32k148_debug_ram_jlink | Debug the RAM configuration using Segger Jlink debuggers
     \b lin_master_baremetal_s32k148_debug_flash_jlink | Debug the FLASH configuration using Segger Jlink debuggers
     \b lin_master_baremetal_s32k148_debug_ram_pemicro | Debug the RAM configuration using PEMicro debuggers
     \b lin_master_baremetal_s32k148_debug_flash_pemicro | Debug the FLASH configuration using PEMicro debuggers
    \n Select the desired debug configuration and click on \b Launch. Now the perspective will change to the \b Debug \b Perspective. \n
    Use the controls to control the program flow.

    @note For more detailed information related to S32 Design Studio usage please consult the available documentation. The SBC's default mode is FORCE NORMAL mode. This mode enabled LIN transceiver. If SBC is another mode, user need move SBC to NORMAL mode by set SBC_FORCE_NORMAL_MODE to 0.

*/
