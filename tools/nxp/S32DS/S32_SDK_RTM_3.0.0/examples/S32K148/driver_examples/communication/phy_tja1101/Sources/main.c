/*
 * Copyright (c) 2015 - 2016 , Freescale Semiconductor, Inc.
 * Copyright 2016-2018 NXP
 * All rights reserved.
 *
 * THIS SOFTWARE IS PROVIDED BY NXP "AS IS" AND ANY EXPRESSED OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL NXP OR ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */
/* MODULE main */

/* Including needed modules to compile this module/procedure */
#include "Cpu.h"
#include "pin_mux.h"
#include "ethernet1.h"
#include "phy_cfg.h"
#include "dmaController1.h"
#include "clockMan1.h"

volatile int exit_code = 0;
/* User includes (#include below this line is not maintained by Processor Expert) */
#include <string.h>
#include <stdint.h>
#include <stdbool.h>

/* This example is setup to work by default with EVB. To use it with other boards
   please comment the following line
*/

#define EVB

#ifdef EVB
	#define GPIO_PORT   PTE
	#define PCC_CLOCK   PCC_PORTE_CLOCK
	#define LED1_RED    (1 << 21U)
	#define LED2_GREEN  (1 << 22U)
	#define LED3_BLUE   (1 << 23U)
#else
	#define GPIO_PORT	PTC
	#define PCC_CLOCK	PCC_PORTC_CLOCK
	#define LED1_RED    (1 << 0U)
	#define LED2_GREEN  (1 << 1U)
	#define LED3_BLUE   (1 << 2U)
#endif

#define PTB_PHY_INT (1 << 20U)
#define PTC_BTN0    (1 << 12U)
#define PTC_BTN1    (1 << 13U)


#define PHY_CONFIG1                  18U
#define PHY_CONFIG1_FWDREM           0x0004U
#define PHY_EXTENDED_CTRL_ADDR       17U
#define PHY_EXTENDED_CTRL_CONFIG_EN  0x0004U
#define PHY_COM_CONFIG               27U
#define PHY_COM_CONFIG_WAKE          0x0040U

/* Port C IRQ handler */
void portc_Handler(void)
{
	uint32_t flags;
	static bool loopback;
	static phy_role_t phyRole = PHY_ROLE_MASTER;
	uint32_t delay = 1000000U;

	do
	{
		/* wait some time to allow capturing pushing multiple buttons at once */
		delay--;
	}
	while(delay != 0);

	flags = PINS_DRV_GetPortIntFlag(PORTC);
	if ((flags & (PTC_BTN1 | PTC_BTN0)) == (PTC_BTN1 | PTC_BTN0))
	{
		/* both buttons pressed - change master/slave settings */
		PHY_SetRole(0, phyRole);
		phyRole = (phyRole == PHY_ROLE_MASTER) ? (PHY_ROLE_SLAVE) : (PHY_ROLE_MASTER);
	}
	else
	{
		if ((flags & PTC_BTN1) != 0U)
		{
			PHY_Sleep(0);
		}
		if ((flags & PTC_BTN0) != 0U)
		{
			if (loopback)
			{
				PHY_SetLoopback(0, PHY_LOOPBACK_NONE);
			}
			else
			{
				PHY_SetLoopback(0, PHY_LOOPBACK_INTERNAL);
			}
			loopback = !loopback;
		}
	}
    /* Clear interrupt flag */
	PINS_DRV_ClearPortIntFlagCmd(PORTC);
}

/* Link up callback */
void link_up(uint8_t phy)
{
	if (phy == 0U)
	{
		PINS_DRV_ClearPins(GPIO_PORT, LED1_RED);
		PINS_DRV_SetPins(GPIO_PORT, LED2_GREEN);
		PINS_DRV_ClearPins(GPIO_PORT, LED3_BLUE);
	}
}

/* Link down callback  */
void link_down(uint8_t phy)
{
	if (phy == 0U)
	{
		PINS_DRV_SetPins(GPIO_PORT, LED1_RED);
		PINS_DRV_ClearPins(GPIO_PORT, LED2_GREEN);
		PINS_DRV_ClearPins(GPIO_PORT, LED3_BLUE);
	}
}

/*!
 \brief The main function for the project.
 \details The startup initialization sequence is the following:
 * - startup asm routine
 * - main()
 */
int main(void)
{
  /* Write your local variable definition here */

  /*** Processor Expert internal initialization. DON'T REMOVE THIS CODE!!! ***/
#ifdef PEX_RTOS_INIT
  PEX_RTOS_INIT(); /* Initialization of the selected RTOS. Macro is defined by the RTOS component. */
#endif
  /*** End of Processor Expert internal initialization.                    ***/

  /* Initialize and configure clocks
   * 	-	see clock manager component for details
   */
  CLOCK_SYS_Init(g_clockManConfigsArr, CLOCK_MANAGER_CONFIG_CNT,
                 g_clockManCallbacksArr, CLOCK_MANAGER_CALLBACK_CNT);
  CLOCK_SYS_UpdateConfiguration(0U, CLOCK_MANAGER_POLICY_AGREEMENT);

  /* Output Buffer Enable for ENET MII clock in internal loopback mode */
  SIM->MISCTRL0 |= SIM_MISCTRL0_RMII_CLK_OBE_MASK;

  /* Initialize pins
   *	-	See PinSettings component for more info
   */
  PINS_DRV_Init(NUM_OF_CONFIGURED_PINS, g_pin_mux_InitConfigArr);

  /* Turn off LEDs */
  PINS_DRV_ClearPins(GPIO_PORT, LED1_RED);
  PINS_DRV_ClearPins(GPIO_PORT, LED2_GREEN);
  PINS_DRV_ClearPins(GPIO_PORT, LED3_BLUE);

  /* Disable MPU */
  MPU->CESR = 0x00815200;

  /* Initialize ENET instance */
  ENET_DRV_Init(INST_ETHERNET1, &ethernet1_State, &ethernet1_InitConfig0, ethernet1_buffConfigArr0, ethernet1_MacAddr);
  ENET_DRV_EnableMDIO(INST_ETHERNET1, false);

  PINS_DRV_SetPins(GPIO_PORT, LED1_RED);

  PHY_FrameworkInit(phyConfig, phyDrivers);
  PHY_Init(0);

  /* make custom settings */
  PHY_RMR(0, PHY_EXTENDED_CTRL_ADDR, PHY_EXTENDED_CTRL_CONFIG_EN, PHY_EXTENDED_CTRL_CONFIG_EN);
  PHY_RMR(0, PHY_CONFIG1, PHY_CONFIG1_FWDREM, PHY_CONFIG1_FWDREM);
  PHY_RMR(0, PHY_COM_CONFIG, PHY_COM_CONFIG_WAKE, PHY_COM_CONFIG_WAKE);  /* ratio metric threshold on wake pin */
  PHY_RMR(0, PHY_EXTENDED_CTRL_ADDR, 0, PHY_EXTENDED_CTRL_CONFIG_EN);

  INT_SYS_InstallHandler(PORTC_IRQn, portc_Handler, (isr_t *)0);
  INT_SYS_EnableIRQ(PORTC_IRQn);

  while (1)
  {
      PHY_MainFunction(0);

      static uint16_t extCtrl = 0;
      PHY_Read(0, PHY_EXTENDED_CTRL_ADDR, &extCtrl);
      if (extCtrl == 0xD000U)
      {
          /* sleep mode */
          PINS_DRV_ClearPins(GPIO_PORT, LED1_RED);
          PINS_DRV_ClearPins(GPIO_PORT, LED2_GREEN);
          PINS_DRV_SetPins(GPIO_PORT, LED3_BLUE);
      }

      OSIF_TimeDelay(10);
  }
  /*** Don't write any code pass this line, or it will be deleted during code generation. ***/
  /*** RTOS startup code. Macro PEX_RTOS_START is defined by the RTOS component. DON'T MODIFY THIS CODE!!! ***/
  #ifdef PEX_RTOS_START
    PEX_RTOS_START();                  /* Startup of the selected RTOS. Macro is defined by the RTOS component. */
  #endif
  /*** End of RTOS startup code.  ***/
  /*** Processor Expert end of main routine. DON'T MODIFY THIS CODE!!! ***/
  for(;;) {
    if(exit_code != 0) {
      break;
    }
  }
  return exit_code;
  /*** Processor Expert end of main routine. DON'T WRITE CODE BELOW!!! ***/
} /*** End of main routine. DO NOT MODIFY THIS TEXT!!! ***/

/* END main */
/*!
 ** @}
 */
/*
 ** ###################################################################
 **
 **     This file was created by Processor Expert 10.1 [05.21]
 **     for the Freescale S32K series of microcontrollers.
 **
 ** ###################################################################
 */

