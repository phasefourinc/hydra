/*!
    @page spi_pal_s32k148_group SPI PAL
    @brief Driver example using SPI

    ## Application description ##
    ______
    The purpose of this application is to show you how to use the LPSPI and FLEXIO Interfaces on the S32K148 using the S32 SDK API.

    The application uses one board instance of LPSPI in slave configuration and one board instance of FLEXIO in master configuration to communicate data via the SPI bus using interrupts. It also verifies that the data sent is the same as the received data. If transfer is successful, RED led will be on, otherwise it will be off.

    ## Prerequisites ##
    ______
    To run the example you will need to have the following items:
    - 1 S32K148 board
    - 1 Power Adapter 12V (if the board can't be powered from the USB)
    - 1 Personal Computer
    - 4 Dupont male to male or female to female cable
    - 1 Jlink Lite Debugger (optional, users can use Open SDA)

    ## Boards supported ##
    ______
    The following boards are supported by this application:
    - S32K148EVB-Q144
    - S32K148-MB

    ## Hardware Wiring ##
    ______
    The following connections must be done to for this example application to work:

    PIN FUNCTION    | S32K148EVB-Q144 | S32K148-MB
    ----------------|-----------------|--------------
    FLEXIO_MASTER CS    (\b PTD19)  |J6.22 - J2.7   |   PTD19 - PTB0
    FLEXIO_MASTER SCK   (\b PTD18)  |J6.19 - J2.16  |   PTD18 - PTB2
    FLEXIO_MASTER MOSI  (\b PTC31)  |J4.16 - J2.13  |   PTC31 - PTB3
    FLEXIO_MASTER MISO  (\b PTD0)   |J6.29 - J2.10  |   PTD0 - PTB1
    LPSPI_SLAVE SS      (\b PTB0)   |J2.7 - J6.22   |   PTB0 - PTD19
    LPSPI_SLAVE SCK     (\b PTB2)   |J2.16 - J6.19  |   PTB2 - PTD18
    LPSPI_SLAVE MOSI    (\b PTB3)   |J2.13 - J4.16  |   PTB3 - PTC31
    LPSPI_SLAVE MISO    (\b PTB1)   |J2.10 - J6.29  |   PTB1 - PTD0

    ## How to run ##

    #### 1. Importing the project into the workspace ####
    After opening S32 Design Studio, go to \b File -> \b New \b S32DS \b Project \b From... and select \b spi_pal. Then click on \b Finish. \n
    The project should now be copied into you current workspace.
    #### 2. Generating the Processor Expert configuration ####
    First go to \b Project \b Explorer View in S32 DS and select the current project(\b spi_pal). Then go to \b Project and click on \b Generate \b Processor \b Expert \b Code \n
    Wait for the code generation to be completed before continuing to the next step.
    #### 3. Building the project ####
    Select the configuration to be built \b FLASH (Debug_FLASH) or \b RAM (Debug_RAM) by left clicking on the downward arrow corresponding to the \b build button(@image hammer.png).
    Wait for the build action to be completed before continuing to the next step.
    #### 4. Running the project ####
    Go to \b Run and select \b Debug \b Configurations. There will be four debug configurations for this project:
     Configuration Name | Description
     -------------------|------------
     \b spi_pal_s32k148_debug_ram_jlink     | Debug the RAM configuration using Segger Jlink debuggers
     \b spi_pal_s32k148_debug_flash_jlink   | Debug the FLASH configuration using Segger Jlink debuggers
     \b spi_pal_s32k148_debug_ram_pemicro   | Debug the RAM configuration using PEMicro debuggers
     \b spi_pal_s32k148_debug_flash_pemicro | Debug the FLASH configuration using PEMicro debuggers
    \n Select the desired debug configuration and click on \b Launch. Now the perspective will change to the \b Debug \b Perspective. \n
    Use the controls to control the program flow.

    @note For more detailed information related to S32 Design Studio usage please consult the available documentation.


*/

