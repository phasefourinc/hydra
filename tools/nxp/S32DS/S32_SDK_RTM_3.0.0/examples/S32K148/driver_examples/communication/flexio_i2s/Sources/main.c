/*
 * Copyright (c) 2015 - 2016 , Freescale Semiconductor, Inc.
 * Copyright 2016-2018 NXP
 * All rights reserved.
 *
 * THIS SOFTWARE IS PROVIDED BY NXP "AS IS" AND ANY EXPRESSED OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL NXP OR ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */
/* ###################################################################
**     Filename    : main.c
**     Project     : flexio_i2s_s32k148
**     Processor   : S32K148_144
**     Version     : Driver 01.00
**     Compiler    : GNU C Compiler
**     Date/Time   : 2016-12-09, 14:05, # CodeGen: 0
**     Abstract    :
**         Main module.
**         This module contains user's application code.
**     Settings    :
**     Contents    :
**         No public methods
**
** ###################################################################*/
/*!
** @file main.c
** @version 01.00
** @brief
**         Main module.
**         This module contains user's application code.
*/
/*!
**  @addtogroup main_module main module documentation
**  @{
*/
/* MODULE main */


/* Including needed modules to compile this module/procedure */
#include "Cpu.h"
#include "clockMan1.h"
#include "pin_mux.h"
#include "dmaController1.h"
#include "flexio_i2s1.h"
#include "flexio_i2s2.h"
#if CPU_INIT_CONFIG
  #include "Init_Config.h"
#endif

volatile int exit_code = 0;
/* User includes (#include below this line is not maintained by Processor Expert) */

#include <stdint.h>

#define LED_PORT  (PTE)
#define LED_RED   (21u)
#define LED_GREEN (22u)

/* 2KB Transfer size, the value should be modulo 4, since the transfers are 32bit wide */
#define TRANSFER_SIZE (2048u)

/* Master TX and RX buffers definition */
uint8_t masterTxBuffer[TRANSFER_SIZE];
uint8_t masterRxBuffer[TRANSFER_SIZE];
/* Slave TX and RX buffers definition */
uint8_t slaveTxBuffer[TRANSFER_SIZE];
uint8_t slaveRxBuffer[TRANSFER_SIZE];

/*!
  \brief The main function for the project.
  \details The startup initialization sequence is the following:
 * - __start (startup asm routine)
 * - main()
*/
int main(void)
{
    /* FlexIO device state */
    flexio_device_state_t       flexIODeviceState;
    /* FlexIO I2S state structure for master and slave emulation */
    flexio_i2s_master_state_t   I2SMasterState;
    flexio_i2s_slave_state_t    I2SSlaveState;
    /* Counter used for initializing the buffers */
    uint32_t cnt;
    uint8_t isTransferOk;
/*** Processor Expert internal initialization. DON'T REMOVE THIS CODE!!! ***/
#ifdef PEX_RTOS_INIT
    PEX_RTOS_INIT();                 /* Initialization of the selected RTOS. Macro is defined by the RTOS component. */
#endif
/*** End of Processor Expert internal initialization.                    ***/

    /* Initialize and configure clocks
     *  -   Setup system clocks, dividers
     *  -   Configure FlexIO clock
     *  -   see clock manager component for more details
     */
    CLOCK_SYS_Init(g_clockManConfigsArr, CLOCK_MANAGER_CONFIG_CNT,
                   g_clockManCallbacksArr, CLOCK_MANAGER_CALLBACK_CNT);
    CLOCK_SYS_UpdateConfiguration(0U, CLOCK_MANAGER_POLICY_AGREEMENT);

    /* Initialize pins
     *  -   Init FlexIO I2S pins
     *  -   See PinSettings component for more info
     */
    PINS_DRV_Init(NUM_OF_CONFIGURED_PINS, g_pin_mux_InitConfigArr);

    /* Initialize eDMA module & channels */
    EDMA_DRV_Init(&dmaController1_State,
                  &dmaController1_InitConfig0,
                  edmaChnStateArray,
                  edmaChnConfigArray,
                  EDMA_CONFIGURED_CHANNELS_COUNT);

    /* Initialize the FLEXIO device */
    FLEXIO_DRV_InitDevice(INST_FLEXIO_I2S1, &flexIODeviceState);

    /*
     * Initialize FlexIO I2S Master driver
     *   - 1.4112 MHz clock speed
     *   - DMA transfer Type
     */
    FLEXIO_I2S_DRV_MasterInit(INST_FLEXIO_I2S1, &flexio_i2s1_MasterConfig0, &I2SMasterState);
    /*
     * Initialize FlexIO I2S Slave driver
     *   - Interrupt transfer Type
     */
    FLEXIO_I2S_DRV_SlaveInit(INST_FLEXIO_I2S2,  &flexio_i2s2_SlaveConfig0, &I2SSlaveState);

    /* Red off */
    PINS_DRV_WritePin(LED_PORT, LED_RED, 0);
    /* Green off */
    PINS_DRV_WritePin(LED_PORT, LED_GREEN, 0);

    /* Initialize master and slave buffers for master send sequence */
    for(cnt = 0; cnt < TRANSFER_SIZE; cnt++)
    {
        masterTxBuffer[cnt] = (uint8_t)cnt;
        masterRxBuffer[cnt] = 0U;
        slaveTxBuffer[cnt]  = (uint8_t)(TRANSFER_SIZE - cnt - 1u);
        slaveRxBuffer[cnt]  = 0U;
    }

    /* Setup I2S slave to start listening and assign RX buffer */
    FLEXIO_I2S_DRV_SlaveReceiveData(&I2SSlaveState, slaveRxBuffer, TRANSFER_SIZE);
    /* Transfer Data to slave with DMA, using blocking method */
    FLEXIO_I2S_DRV_MasterSendDataBlocking(&I2SMasterState, masterTxBuffer, TRANSFER_SIZE, 100UL);

    /* Send data to the master */
    FLEXIO_I2S_DRV_SlaveSendData(&I2SSlaveState, slaveTxBuffer, TRANSFER_SIZE);
    /* Transfer Data from slave with DMA, using blocking method */
    FLEXIO_I2S_DRV_MasterReceiveDataBlocking(&I2SMasterState, masterRxBuffer, TRANSFER_SIZE, 100UL);

    isTransferOk = 1u;

    /* Perform a comparison between the master and slave buffers, to check the
     * validity of the values transferred.
     * The Red LED will be turned if data does not match. For the case in which
     * the data was correctly transfered the Green LED will be lit.
     */
    for (cnt = 0; cnt < TRANSFER_SIZE; cnt++)
    {
        if((masterRxBuffer[cnt] != slaveTxBuffer[cnt]) ||
           (slaveRxBuffer[cnt] != masterTxBuffer[cnt]))
        {
            isTransferOk = 0u;
            break;
        }
    }

    /* Turn on Red or Green LED depending on the check result */
    PINS_DRV_WritePin(LED_PORT, LED_RED, (1u - isTransferOk));
    PINS_DRV_WritePin(LED_PORT, LED_GREEN, isTransferOk);

  /*** Don't write any code pass this line, or it will be deleted during code generation. ***/
  /*** RTOS startup code. Macro PEX_RTOS_START is defined by the RTOS component. DON'T MODIFY THIS CODE!!! ***/
  #ifdef PEX_RTOS_START
    PEX_RTOS_START();                  /* Startup of the selected RTOS. Macro is defined by the RTOS component. */
  #endif
  /*** End of RTOS startup code.  ***/
  /*** Processor Expert end of main routine. DON'T MODIFY THIS CODE!!! ***/
  for(;;) {
    if(exit_code != 0) {
      break;
    }
  }
  return exit_code;
  /*** Processor Expert end of main routine. DON'T WRITE CODE BELOW!!! ***/
} /*** End of main routine. DO NOT MODIFY THIS TEXT!!! ***/

/* END main */
/*!
** @}
*/
/*
** ###################################################################
**
**     This file was created by Processor Expert 10.1 [05.21]
**     for the Freescale S32K series of microcontrollers.
**
** ###################################################################
*/

