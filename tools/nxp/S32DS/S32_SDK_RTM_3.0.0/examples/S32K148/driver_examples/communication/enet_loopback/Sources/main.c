/* 
 * Copyright (c) 2015 - 2016 , Freescale Semiconductor, Inc.                             
 * Copyright 2016-2017 NXP                                                                    
 * All rights reserved.                                                                  
 *                                                                                       
 * THIS SOFTWARE IS PROVIDED BY NXP "AS IS" AND ANY EXPRESSED OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL NXP OR ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.                          
 */
/* MODULE main */

/* Including needed modules to compile this module/procedure */
#include "Cpu.h"
#include "pin_mux.h"
#include "ethernet1.h"
#include "dmaController1.h"
#include "clockMan1.h"

volatile int exit_code = 0;
/* User includes (#include below this line is not maintained by Processor Expert) */
#include <string.h>
#include <stdint.h>
#include <stdbool.h>

/* This example is setup to work by default with EVB. To use it with other boards
   please comment the following line
*/

#define EVB

#ifdef EVB
	#define GPIO_PORT	PTE
	#define PCC_CLOCK	PCC_PORTE_CLOCK
	#define LED1		21U
	#define LED2		22U
#else
	#define GPIO_PORT	PTC
	#define PCC_CLOCK	PCC_PORTC_CLOCK
	#define LED1		0U
	#define LED2		1U
#endif

typedef struct {
	uint8_t destAddr[6];
	uint8_t srcAddr[6];
	uint16_t length;
	uint8_t payload[1500];
} mac_frame_t;

void delay(volatile int cycles)
{
    /* Delay function - do nothing for a number of cycles */
    while(cycles--);
}

void copyBuff(uint8_t *dest, uint8_t *src, uint32_t len)
{
	uint32_t i;

	for (i = 0; i < len; i++)
	{
		dest[i] = src[i];
	}
}

void rx_callback(uint8_t instance, enet_event_t event, uint8_t ring)
{
	(void)instance;

	if (event == ENET_RX_EVENT)
	{
		enet_buffer_t buff;
		status_t status;

		status = ENET_DRV_ReadFrame(INST_ETHERNET1, ring, &buff, NULL);
		if (status == STATUS_SUCCESS)
		{
			mac_frame_t *frame;

			frame = (mac_frame_t *) buff.data;

			/* You can process the payload here */
			(void)frame->payload;

			/* We successfully received a frame -> turn on LED 2 */
			PINS_DRV_ClearPins(GPIO_PORT, (1 << LED1));
			PINS_DRV_SetPins(GPIO_PORT, (1 << LED2));

			ENET_DRV_ProvideRxBuff(INST_ETHERNET1, ring, &buff);
		}
	}
}

/*!
 \brief The main function for the project.
 \details The startup initialization sequence is the following:
 * - startup asm routine
 * - main()
 */
int main(void)
{
  /* Write your local variable definition here */

  /*** Processor Expert internal initialization. DON'T REMOVE THIS CODE!!! ***/
#ifdef PEX_RTOS_INIT
  PEX_RTOS_INIT(); /* Initialization of the selected RTOS. Macro is defined by the RTOS component. */
#endif
  /*** End of Processor Expert internal initialization.                    ***/

  /* Initialize and configure clocks
   * 	-	see clock manager component for details
   */
  CLOCK_SYS_Init(g_clockManConfigsArr, CLOCK_MANAGER_CONFIG_CNT,
                 g_clockManCallbacksArr, CLOCK_MANAGER_CALLBACK_CNT);
  CLOCK_SYS_UpdateConfiguration(0U, CLOCK_MANAGER_POLICY_AGREEMENT);

  /* Output Buffer Enable for ENET MII clock in internal loopback mode */
  SIM->MISCTRL0 |= SIM_MISCTRL0_RMII_CLK_OBE_MASK;

  /* Initialize pins
   *	-	See PinSettings component for more info
   */
  PINS_DRV_Init(NUM_OF_CONFIGURED_PINS, g_pin_mux_InitConfigArr);

  /* Turn off LED 1 and LED 2 */
  PINS_DRV_ClearPins(GPIO_PORT, (1 << LED1));
  PINS_DRV_ClearPins(GPIO_PORT, (1 << LED2));

  /* Disable MPU */
  MPU->CESR = 0x00815200;

  /* Initialize ENET instance */
  ENET_DRV_Init(INST_ETHERNET1, &ethernet1_State, &ethernet1_InitConfig0, ethernet1_buffConfigArr0, ethernet1_MacAddr);

  enet_buffer_t buff;
  mac_frame_t frame;
  uint8_t i;

  for (i = 0; i < 50U; i++)
  {
	  frame.payload[i] = i;
  }

  copyBuff(frame.destAddr, ethernet1_MacAddr, 6U);
  copyBuff(frame.srcAddr, ethernet1_MacAddr, 6U);
  frame.length = 50U;

  buff.data = (uint8_t *)&frame;
  /* Length == 12 bytes MAC addresses + 2 bytes length + 50 bytes payload */
  buff.length = 64U;

  /* Infinite loop:
   * 	- Send frames
   */
  while (1)
  {
	  /* We are about to send a frame -> turn on LED 1 */
	  PINS_DRV_SetPins(GPIO_PORT, (1 << LED1));
	  PINS_DRV_ClearPins(GPIO_PORT, (1 << LED2));

      ENET_DRV_SendFrame(INST_ETHERNET1, 0U, &buff, NULL);

      delay(70000);
  }
  /*** Don't write any code pass this line, or it will be deleted during code generation. ***/
  /*** RTOS startup code. Macro PEX_RTOS_START is defined by the RTOS component. DON'T MODIFY THIS CODE!!! ***/
  #ifdef PEX_RTOS_START
    PEX_RTOS_START();                  /* Startup of the selected RTOS. Macro is defined by the RTOS component. */
  #endif
  /*** End of RTOS startup code.  ***/
  /*** Processor Expert end of main routine. DON'T MODIFY THIS CODE!!! ***/
  for(;;) {
    if(exit_code != 0) {
      break;
    }
  }
  return exit_code;
  /*** Processor Expert end of main routine. DON'T WRITE CODE BELOW!!! ***/
} /*** End of main routine. DO NOT MODIFY THIS TEXT!!! ***/

/* END main */
/*!
 ** @}
 */
/*
 ** ###################################################################
 **
 **     This file was created by Processor Expert 10.1 [05.21]
 **     for the Freescale S32K series of microcontrollers.
 **
 ** ###################################################################
 */

