/*!
    @page sbc_uja113x_s32k148_group sbc_uja113x
    @brief Basic application that presents how to handle power modes on UJA113x devices.

    ## Application description ##
    _____
    The purpose of this demo is to provide the user with an out-of-the box
    example application for S32K148 platform, using S32 SDK.
    The demo uses SBC UJA113x driver to show to to handle power modes of the external device and how to configure this decive to work out of FNMC mode.
	
	Befor running this application is mandatory to have the SBC in FNMC. By default the SBC is in this mode and if can be put in this mode using this steps:
	- Connect J11-1 (CANH) to J13-2 (LIN1 VBAT)
    - Connect J11-2 (CANL) to J11-4 (GND)
    - Push and hold down the reset button SW5 to pull RST pin of the SBC to LOW
    - Turn off the 12V power supply
    - Turn on the 12V power supply
    - Release the reset button and remove the jumper wires
	
	After starting the application you will see in your serial terminal this message: 
	
	MTPNV status register (0x70) content
	WRCNTS: 36
	ECCS  : 0
	NVMPS : 1
	Non-volatile memory not programmed...
	Do you want to programm the MTPNV? Y/N:
	
	Please type 'Y' and press enter to program the non volatile memory.
	
	On the next step you will see this message on your serial terminal:
	Request for MTPNV programming has been raised, preparing MTPVN data...
	Do you want to enable the SDMC mode? Y/N:
	
	SDMC is used to configure software develop mode which disables watchdog. For this application please type "Y" because it will feed the watchdog.
	After this set-up the SBC is in NORMAL mode. To with to standby mode press SW3. To wake-up SBC and to move it in NORMAL mode press SW6.
	
	During the execution of the application some details about watchdog feed or registers values will be shown in serial terminal.
	
	After powering down the board is mandatory to perform the steps to move the SBC in FNMC mode and only after this the application can be re-run. 

    ## Prerequisites ##
    _____
    To run the example you will need to have the following items:
    - 1 S32K148 board
    - 1 Power Adapter 12V 
    - 1 Personal Computer
    - 1 Jlink Lite Debugger (optional, users can use Open SDA)
	- Start an serial monitor 

    ## Boards supported ##
    _____
    The following boards are supported by this application:
    - S32K148EVB-Q100

    ## Hardware Wiring ##
    _____
    The following connections must be done to for this example application to work:

    Jumper   |	S32K148EVB-Q100	        
    ---------|---------------
    J18      | 1-2
    J7       | 2-3
	J8       | 1-2

    ## How to run ##
    _____
    #### 1. Importing the project into the workspace ####
    After opening S32 Design Studio, go to \b File -> \b New \b S32DS \b Project \b From... and select \b sbc_uja113x_s32k148. Then click on \b Finish. \n
    The project should now be copied into you current workspace.
    #### 2. Generating the Processor Expert configuration ####
    First go to \b Project \b Explorer View in S32 DS and select the current project(\b sbc_uja113x_s32k148). Then go to \b Project and click on \b Generate \b Processor \b Expert \b Code \n
    Wait for the code generation to be completed before continuing to the next step.
    #### 3. Building the project ####
    Select the configuration to be built \b FLASH (Debug_FLASH) or \b RAM (Debug_RAM) by left clicking on the downward arrow corresponding to the \b build button(@image hammer.png).
    Wait for the build action to be completed before continuing to the next step.
    #### 4. Running the project ####
    Go to \b Run and select \b Debug \b Configurations. There will be two debug configurations for this project:
     Configuration Name | Description
     -------------------|------------
     \b sbc_uja113x_s32k148 \b Debug_FLASH \b Jlink | Debug the FLASH configuration using Segger Jlink debuggers
     \b sbc_uja113x_s32k148 \b Debug_FLASH \b PEMicro | Debug the FLASH configuration using PEMicro debuggers
    \n Select the desired debug configuration and click on \b Launch. Now the perspective will change to the \b Debug \b Perspective. \n
    Use the controls to control the program flow.

    @note For more detailed information related to S32 Design Studio usage please consult the available documentation.

	 ## Notes ##
    ______

    For this example it is necessary to open a terminal emulator and configure it with:
        -   115200 baud
        -   One stop bit
        -   No parity
        -   No flow control
		
	If board version is REV_A please add this define in main.c EVB_REV_A.	

*/

