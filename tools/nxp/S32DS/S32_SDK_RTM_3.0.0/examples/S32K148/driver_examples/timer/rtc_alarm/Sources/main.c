/* 
 * Copyright (c) 2015 - 2016 , Freescale Semiconductor, Inc.                             
 * Copyright 2016-2017 NXP                                                                    
 * All rights reserved.                                                                  
 *                                                                                       
 * THIS SOFTWARE IS PROVIDED BY NXP "AS IS" AND ANY EXPRESSED OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL NXP OR ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.                          
 */

/* ###################################################################
**     Filename    : main.c
**     Project     : rtc_alarm_s32k148
**     Processor   : S32K148_144
**     Version     : Driver 01.00
**     Compiler    : GNU C Compiler
**     Date/Time   : 2015-07-03, 14:05, # CodeGen: 0
**     Abstract    :
**         Main module.
**         This module contains user's application code.
**     Settings    :
**     Contents    :
**         No public methods
**
** ###################################################################*/
/*!
** @file main.c
** @version 01.00
** @brief
**         Main module.
**         This module contains user's application code.
*/
/*!
**  @addtogroup main_module main module documentation
**  @{
*/
/* MODULE main */


/* Including needed modules to compile this module/procedure */
#include "Cpu.h"
#include "clockMan1.h"
#include "pin_mux.h"
#include "rtcTimer1.h"

#if CPU_INIT_CONFIG
  #include "Init_Config.h"
#endif

volatile int exit_code = 0;
/* User includes (#include below this line is not maintained by Processor Expert) */

#include <stdint.h>
#include <stdbool.h>

/* This example is setup to work by default with EVB. To use it with other boards
   please comment the following line
*/

#define PBRIDGE_RTC_SLOT	29
#define EVB

#ifdef EVB
    #define ALARM           21U
    #define SECOND          22U
    #define LED_GPIO        PTE
    #define BTN_GPIO        PTC
    #define BTN_PIN         13U
    #define BTN_PORT        PORTC
    #define BTN_PORT_IRQn   PORTC_IRQn
#else
    #define ALARM           0U
    #define SECOND          1U
    #define LED_GPIO        PTC
    #define BTN_GPIO        PTC
    #define BTN_PIN         13U
    #define BTN_PORT        PORTC
    #define BTN_PORT_IRQn   PORTC_IRQn
#endif

/* Switch to supervisor mode */
void swSupervisorMode(void)
{
    __asm(" MOVS R0, #0x0 ");
    __asm(" MSR CONTROL, R0 ");
    __asm("DSB");
    __asm("ISB");
}

/* Switch to user mode */
void swUserMode(void)
{
    __asm(" MOVS R0, #0x1 ");
    __asm(" MSR CONTROL, R0 ");
    __asm("DSB");
    __asm("ISB");
}

/* SVC Exception interrupt */
void SVC_Handler(void)
{
    /* Switch to supervisor mode need to be done through an exception handler*/
    swSupervisorMode();
}

/* Time Seconds interrupt handler */
void secondsISR(void)
{
    /* Toggle the seconds led */
    PINS_DRV_TogglePins(LED_GPIO, (1 << SECOND));
}

/* Alarm interrupt handler */
void alarmISR(void)
{
    /* Toggle the alarm led */
    PINS_DRV_TogglePins(LED_GPIO, (1 << ALARM));
}

/* Button interrupt handler */
void buttonISR(void)
{
    /* Check if the button was pressed */
    if((PINS_DRV_GetPortIntFlag(BTN_PORT) & (1 << BTN_PIN)) != 0)
    {
        /* Clear interrupt flag */
        PINS_DRV_ClearPinIntFlagCmd(BTN_PORT, BTN_PIN);

        rtc_timedate_t tempTime;
        /* Get current time */
        RTC_DRV_GetCurrentTimeDate(RTCTIMER1, &tempTime);
        /* Add to current time 5 seconds */
        tempTime.seconds += 5;
        /* Set new alarm time and date */
        alarmConfig0.alarmTime = tempTime;
        /* Configure the alarm */
        RTC_DRV_ConfigureAlarm(RTCTIMER1, &alarmConfig0);
    }
}


/*! 
  \brief The main function for the project.
  \details The startup initialization sequence is the following:
 * - __start (startup asm routine)
 * - __init_hardware()
 * - main()
 *   - PE_low_level_init()
 *     - Common_Init()
 *     - Peripherals_Init()
*/
int main(void)
{
  /*** Processor Expert internal initialization. DON'T REMOVE THIS CODE!!! ***/
  #ifdef PEX_RTOS_INIT
    PEX_RTOS_INIT();                 /* Initialization of the selected RTOS. Macro is defined by the RTOS component. */
  #endif
    uint8_t regIdx = PBRIDGE_RTC_SLOT/8;

  /*** End of Processor Expert internal initialization.                    ***/

    /* Initialize and configure clocks
     *  -   see clock manager component for details
     */
    CLOCK_SYS_Init(g_clockManConfigsArr, CLOCK_MANAGER_CONFIG_CNT,
                        g_clockManCallbacksArr, CLOCK_MANAGER_CALLBACK_CNT);
    CLOCK_SYS_UpdateConfiguration(0U, CLOCK_MANAGER_POLICY_AGREEMENT);

    /* Initialize pins
     *  -   See PinSettings component for more info
     */
    PINS_DRV_Init(NUM_OF_CONFIGURED_PINS, g_pin_mux_InitConfigArr);

    /* Setup alarm pin and seconds pin as output */
    PINS_DRV_SetPinsDirection(LED_GPIO, (1 << ALARM) | (1 << SECOND));
    PINS_DRV_ClearPins(LED_GPIO, (1 << ALARM) | (1 << SECOND));

    /* Setup alarm button pin */
    PINS_DRV_SetPinsDirection(BTN_GPIO, ~(1 << BTN_PIN));

    /* Setup alarm pin interrupt */
#ifdef EVB
    /* Falling edge for EVB */
    PINS_DRV_SetPinIntSel(BTN_PORT, BTN_PIN, PORT_INT_FALLING_EDGE);
#else
    /* Rising edge for Validation board */
    PINS_DRV_SetPinIntSel(BTN_PORT, BTN_PIN, PORT_INT_RISING_EDGE);
#endif

    /* Install button ISR */
    INT_SYS_InstallHandler(BTN_PORT_IRQn, &buttonISR, (isr_t*) 0);
    /* Enable button interrupt */
    INT_SYS_EnableIRQ(BTN_PORT_IRQn);

    /* Initialize RTC instance
     *  - See RTC configuration component for options
     */
    RTC_DRV_Init(RTCTIMER1, &rtcTimer1_Config0);
    /* Configure RTC Time Seconds Interrupt */
    RTC_DRV_ConfigureSecondsInt(RTCTIMER1, &rtcTimer1_SecIntConfig0);

    /* Set APIS to allow usermode access to RTC Memory Space */
    /* Clear bit position to grant usermode access level */
    AIPS->OPACR[regIdx] &= ~AIPS_OPACR_SP5_MASK;
    /* Switch to user mode */
    swUserMode();

    /* Set the time and date */
    RTC_DRV_SetTimeDate(RTCTIMER1, &rtcTimer1_StartTime0);

    /* Start the RTC counter */
    RTC_DRV_StartCounter(RTCTIMER1);

    /* Switch to supervisor mode by calling exception SVC_Handler */
    __asm("svc #0x32");

  /*** Don't write any code pass this line, or it will be deleted during code generation. ***/
  /*** RTOS startup code. Macro PEX_RTOS_START is defined by the RTOS component. DON'T MODIFY THIS CODE!!! ***/
  #ifdef PEX_RTOS_START
    PEX_RTOS_START();                  /* Startup of the selected RTOS. Macro is defined by the RTOS component. */
  #endif
  /*** End of RTOS startup code.  ***/
  /*** Processor Expert end of main routine. DON'T MODIFY THIS CODE!!! ***/
  for(;;) {
    if(exit_code != 0) {
      break;
    }
  }
  return exit_code;
  /*** Processor Expert end of main routine. DON'T WRITE CODE BELOW!!! ***/
} /*** End of main routine. DO NOT MODIFY THIS TEXT!!! ***/

/* END main */
/*!
** @}
*/
/*
** ###################################################################
**
**     This file was created by Processor Expert 10.1 [05.21]
**     for the Freescale S32K series of microcontrollers.
**
** ###################################################################
*/
