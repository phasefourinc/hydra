/*
 * Copyright (c) 2015 - 2016 , Freescale Semiconductor, Inc.
 * Copyright 2016-2018 NXP
 * All rights reserved.
 *
 * THIS SOFTWARE IS PROVIDED BY NXP "AS IS" AND ANY EXPRESSED OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL NXP OR ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

/* Including needed modules to compile this module/procedure */
#include "Cpu.h"
#include "pin_mux.h"
#include "lpuart1.h"
#include "clockMan1.h"
#include "dmaController1.h"
#include "flexTimer_ic1.h"
#include "flexTimer_pwm1.h"

volatile int exit_code = 0U;
/* User includes (#include below this line is not maintained by Processor Expert) */

#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <stdbool.h>

/***********************************
 * @brief: Wait for a number of cycles
 * @param nbOfCycles is number of cycles to be waited for
 ***********************************/
void delayCycles(volatile uint32_t cycles)
{
    while (cycles--)
    {
    }
}

/* brief:           Method used to print a string with LPUART
 * param sourceStr: pointer to the string beginning
 */
void print(const char * sourceStr)
{
    uint32_t bytesRemaining;
    LPUART_DRV_SendData(INST_LPUART1, (uint8_t *)sourceStr, strlen(sourceStr));
    while(LPUART_DRV_GetTransmitStatus(INST_LPUART1, &bytesRemaining) != STATUS_SUCCESS);
}

/*!
 \brief The main function for the project.
 \details The startup initialization sequence is the following:
 * - startup asm routine
 * - main()
 */
int main(void)
{
    ftm_state_t ftm1StateStruct;
    ftm_state_t ftm2StateStruct;
    /* Variables used to store PWM frequency,
     * input capture measurement value
     */
    uint16_t inputCaptureMeas = 0U;
    uint32_t frequency;

    bool conversionComplete = false;
    /* Buffer for string processing */
    char txBuff[255];

    /*** Processor Expert internal initialization. DON'T REMOVE THIS CODE!!! ***/
#ifdef PEX_RTOS_INIT
    PEX_RTOS_INIT(); /* Initialization of the selected RTOS. Macro is defined by the RTOS component. */
#endif
    /*** End of Processor Expert internal initialization.                    ***/


    /* Initialize and configure clocks
     *  -   see clock manager component for details
     */
    CLOCK_SYS_Init(g_clockManConfigsArr, CLOCK_MANAGER_CONFIG_CNT,
                   g_clockManCallbacksArr, CLOCK_MANAGER_CALLBACK_CNT);
    CLOCK_SYS_UpdateConfiguration(0U, CLOCK_MANAGER_POLICY_AGREEMENT);

    /* Initialize pins
     *  -   See PinSettings component for more info
     */
    PINS_DRV_Init(NUM_OF_CONFIGURED_PINS, g_pin_mux_InitConfigArr);

    /* Initialize FTM instances, PWM and Input capture
     *  -   See ftm component for more details
     */
    FTM_DRV_Init(INST_FLEXTIMER_IC1, &flexTimer_ic1_InitConfig, &ftm1StateStruct);
    FTM_DRV_Init(INST_FLEXTIMER_PWM1, &flexTimer_pwm1_InitConfig, &ftm2StateStruct);

    /* Initialize LPUART instance
     *  -   See LPUART component for configuration details
     */
    LPUART_DRV_Init(INST_LPUART1, &lpuart1_State, &lpuart1_InitConfig0);

    /* Setup input capture for FTM 0 channel 0  - PTC0 */
    FTM_DRV_InitInputCapture(INST_FLEXTIMER_IC1, &flexTimer_ic1_InputCaptureConfig);

    /* Start PWM channel on FTM 4 channel 1 - PTE21 */
    FTM_DRV_InitPwm(INST_FLEXTIMER_PWM1, &flexTimer_pwm1_PwmConfig);


    /* Get the FTM1 frequency to calculate
     * the frequency of the measured signal.
     */
    frequency = FTM_DRV_GetFrequency(INST_FLEXTIMER_IC1);

    print("This example will show you how to use FTM's signal measurement feature.\
            \rTo achieve that we will generate a modifiable frequency PWM and read \r\nit with Input Capture\r\n");

    /* Infinite loop
     *  -   Wait for user input
     *  -   Measure and calculate the signal frequency
     *  -   Send original and measured freq via LPUART
     *  -   Modify the PWM frequency
     */
    while (1)
    {
        print("Press any key to initiate a new conversion...\r\n");
        /* Wait for user input */
        LPUART_DRV_ReceiveDataBlocking(INST_LPUART1, (uint8_t *)&txBuff[255], 1U, 1 << 31U);
        conversionComplete = false;

        while(conversionComplete == false)
        {
            /* Get values */
            inputCaptureMeas = FTM_DRV_GetInputCaptureMeasurement(INST_FLEXTIMER_IC1, 0U);
            /* Calculate the signal frequency using recorded data*/
            inputCaptureMeas = frequency / (inputCaptureMeas);

            /* Convert the integer to char array and send it*/
            sprintf(txBuff, "PWM frequency:\t%lu\tMeasured frequency:\t%u\t[Hz]\r\n", \
                    flexTimer_pwm1_PwmConfig.uFrequencyHZ, inputCaptureMeas);
            print(txBuff);

            /* Stop PWM */
            FTM_DRV_DeinitPwm(INST_FLEXTIMER_PWM1);

            /* Increase frequency */
            if(flexTimer_pwm1_PwmConfig.uFrequencyHZ < 3000U)
                flexTimer_pwm1_PwmConfig.uFrequencyHZ += 100U;
            else
            {
                flexTimer_pwm1_PwmConfig.uFrequencyHZ = 300U;
                conversionComplete = true;
            }
            /* Restart PWM with new settings */
            FTM_DRV_InitPwm(INST_FLEXTIMER_PWM1, &flexTimer_pwm1_PwmConfig);

        /* Wait a number of cycles for the PWM to reach stability */
        delayCycles(0x7FFFFU);
        }

    }
    /*** Don't write any code pass this line, or it will be deleted during code generation. ***/
  /*** RTOS startup code. Macro PEX_RTOS_START is defined by the RTOS component. DON'T MODIFY THIS CODE!!! ***/
  #ifdef PEX_RTOS_START
    PEX_RTOS_START();                  /* Startup of the selected RTOS. Macro is defined by the RTOS component. */
  #endif
  /*** End of RTOS startup code.  ***/
  /*** Processor Expert end of main routine. DON'T MODIFY THIS CODE!!! ***/
  for(;;) {
    if(exit_code != 0) {
      break;
    }
  }
  return exit_code;
  /*** Processor Expert end of main routine. DON'T WRITE CODE BELOW!!! ***/
} /*** End of main routine. DO NOT MODIFY THIS TEXT!!! ***/

/* END main */
/*!
 ** @}
 */
/*
 ** ###################################################################
 **
 **     This file was created by Processor Expert 10.1 [05.21]
 **     for the Freescale S32K series of microcontrollers.
 **
 ** ###################################################################
 */
