/*!
    @page qspi_external_flash_s32k148_group External Flash over QuadSPI
    @brief Driver example for external serial flash.
    
    ## Application description ##
    _____
    The purpose of this demo is to present the usage of the flash_mx25l6433f (external serial flash)
    and QSPI drivers. The flash_mx25l6433f driver allows the application to use an external 
    Macronix MX25L6433F serial flash device, using the QuadSPI interface for communication.

    ## Prerequisites ##
    _____
    To run the example you will need to have the following items:
    - 1 S32K148 board
    - 1 Power Adapter 12V (if the board cannot be powered from the USB port)
    - 1 Personal Computer
    - 1 Jlink Lite Debugger (optional, users can use Open SDA)
    
    ## Boards supported ##
    _____
    The following boards are supported by this application:
    - S32K148EVB-Q144
    
    ## Hardware Wiring ##
    _____
    No external connections are required for this example.\n
    \b Note: On the S32K148EVB-Q144 board, the external flash is by default not connected to the MCU. In order to run this example some hardware changes are required to the board:

    Remove    |	Connect	           |
    --------------------------------|--------------|
    R212                   |R213         |
    R210                   |R211         |
    R208                   |R209         |
    R250                   |R244         |
    R260                   |R254         |
    R272                   |R271         |

    ## How to run ##
    _____
    #### 1. Importing the project into the workspace ####
    After opening S32 Design Studio, go to \b File -> \b New \b S32DS \b Project \b From... and select \b qspi_external_flash_s32k148. Then click on \b Finish. \n
    The project should now be copied into you current workspace.
    #### 2. Generating the Processor Expert configuration ####
    First go to \b Project \b Explorer View in S32 DS and select the current project(\b qspi_external_flash_s32k148). Then go to \b Project and click on \b Generate \b Processor \b Expert \b Code \n
    Wait for the code generation to be completed before continuing to the next step.
    #### 3. Building the project ####
    Select the configuration to be built \b FLASH (Debug_FLASH) or \b RAM (Debug_RAM) by left clicking on the downward arrow corresponding to the \b build button(@image hammer.png). 
    Wait for the build action to be completed before continuing to the next step.
    #### 4. Running the project ####
    Go to \b Run and select \b Debug \b Configurations. There will be four debug configurations for this project:
     Configuration Name | Description
     -------------------|------------
     \b qspi_external_flash_s32k148_debug_ram_jlink | Debug the RAM configuration using Segger Jlink debuggers
     \b qspi_external_flash_s32k148_debug_flash_jlink | Debug the FLASH configuration using Segger Jlink debuggers
     \b qspi_external_flash_s32k148_debug_ram_pemicro | Debug the RAM configuration using PEMicro debuggers 
     \b qspi_external_flash_s32k148_debug_flash_pemicro | Debug the FLASH configuration using PEMicro debuggers 
    \n Select the desired debug configuration and click on \b Launch. Now the perspective will change to the \b Debug \b Perspective. \n
    Use the controls to control the program flow.
    
    @note For more detailed information related to S32 Design Studio usage please consult the available documentation.
    

*/

