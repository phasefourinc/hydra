/*!
    @page erm_report_s32k148_group ERM REPORT
    @brief Driver example that shows the user how to use the Error reporting module

    ## Application description ##
    _____
    The EIM module enables the user to inject 1 bit error or 2 bit errors into bus data, when read from a   designated RAM area.
    The ECC module must correct all 1 bit errors.
    The ERM module reports any detected memory error.
    The example runs only on FLASH

    ## Run the code ##
    _____
    1.  After reset LED_RED on, LED_GREEN off and initialize value for address used to test .
    2.  Press button SW to initialize ERM and EIM module .
    3.  Read address which initialized  if value in address test the same before value initialized, then LED_RED OFF and LED_GREEN ON .

    ## Prerequisites ##
    _____
    To run the example you will need to have the following items:
    - 1 S32K148 board
    - 1 Power Adapter 12V
    - 2 Dupont male to male cable
    - 1 Personal Computer
    - 1 Jlink Lite Debugger (optional, users can use Open SDA)

    ## Boards supported ##
    _____
    The following boards are supported by this application:
    - S32K148EVB-Q144
    - S32K148-MB

    ## Hardware Wiring ##
    _____

    The following connections must be done to for this example application to work:


    PIN FUNCTION         |        S32K148EVB-Q144          |          S32K148-MB
    ---------------------|---------------------------------|------------------------------
    RED_LED (\b PTE21)   |  RGB_RED - wired on the board   |  J12.17 - J11.31
    GREEN_LED (\b PTE22) |  RGB_GREEN - wired on the board |  J12.16 - J11.30
    SW (\b PTC12)        |  SW3_BTN0 - wired on the board  |  BUTTON2 - wired on the board


    ## How to run ##
    _____
    #### 1. Importing the project into the workspace ####
    After opening S32 Design Studio, go to \b File -> \b New \b S32DS \b Project \b From... and select \b erm_report_s32k148. Then click on \b Finish. \n
    The project should now be copied into you current workspace.
    #### 2. Generating the Processor Expert configuration ####
    First go to \b Project \b Explorer View in S32 DS and select the current project(\b erm_report_s32k148). Then go to \b Project and click on \b Generate \b Processor \b Expert \b Code \n
    Wait for the code generation to be completed before continuing to the next step.
    #### 3. Building the project ####
    Select the configuration to be built \b FLASH (Debug_FLASH) by left clicking on the downward arrow corresponding to the \b build button(@image hammer.png).
    Wait for the build action to be completed before continuing to the next step.
    #### 4. Running the project ####
    Go to \b Run and select \b Debug \b Configurations. There will be two debug configurations for this project:
     Configuration Name | Description
     -------------------|------------
     \b erm_report_s32k148_debug_flash_jlink | Debug the FLASH configuration using Segger Jlink debuggers
     \b erm_report_s32k148_debug_flash_pemicro | Debug the FLASH configuration using PEMicro debuggers
    \n Select the desired debug configuration and click on \b Launch. Now the perspective will change to the \b Debug \b Perspective. \n
    Use the controls to control the program flow.

    @note For more detailed information related to S32 Design Studio usage please consult the available documentation.
    @note This Example only run on Flash
*/

