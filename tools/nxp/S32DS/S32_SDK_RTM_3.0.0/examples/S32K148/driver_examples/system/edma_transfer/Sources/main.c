/* 
 * Copyright (c) 2015 - 2016 , Freescale Semiconductor, Inc.                             
 * Copyright 2016-2018 NXP
 * All rights reserved.                                                                  
 *                                                                                       
 * THIS SOFTWARE IS PROVIDED BY NXP "AS IS" AND ANY EXPRESSED OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL NXP OR ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.                          
 */
/* ###################################################################
**     Filename    : main.c
**     Project     : edma_transfer_s32k148
**     Processor   : S32K148_176
**     Version     : Driver 01.00
**     Compiler    : GNU C Compiler
**     Date/Time   : 2018-03-29, 14:40, # CodeGen: 2
**     Abstract    :
**         Main module.
**         This module contains user's application code.
**     Settings    :
**     Contents    :
**         No public methods
**
** ###################################################################*/
/*!
** @file main.c
** @version 01.00
** @brief
**         Main module.
**         This module contains user's application code.
*/         
/*!
**  @addtogroup main_module main module documentation
**  @{
*/ 
/* MODULE main */

/* Including needed modules to compile this module/procedure */
#include "Cpu.h"
#include "pin_mux.h"
#include "clockMan1.h"
#include "dmaController1.h"
#include "lpuart1.h"
#include "edmaTransfer.h"

volatile int exit_code = 0;
/* User includes (#include below this line is not maintained by Processor Expert) */
#include <string.h>
#include <stdint.h>
#include <stdbool.h>

/* Macro defined to display text to console; it sends the
 * buffer via UART and waits the transmission to complete
 */
uint32_t bytesRemaining;
#define PRINTF(text) LPUART_DRV_SendData(INST_LPUART1, (uint8_t *)text, strlen(text));\
                     while(LPUART_DRV_GetTransmitStatus(INST_LPUART1, &bytesRemaining) != STATUS_SUCCESS);

/* Text to be displayed on the console */
#define DMA_MENU_TEXT "\r\nThis is a demo illustrating eDMA driver usage.\r\n\
Messages are sent and received to/from the console using DMA based serial communication.\r\n\
Select one of the following options:\r\n\
  - press '1' to trigger a single block memory-to-memory transfer\r\n\
  - press '2' to trigger a loop memory-to-memory transfer\r\n\
  - press '3' to trigger a memory-to-memory transfer using scatter/gather feature\r\n\
  - press '0' to exit the demo\r\n"

/* Test buffer transferred by DMA */
#define DMA_SAMPLE_BUFFER "'This buffer is transferred via eDMA requests'\r\n"

/* Receive buffer size */
#define BUFFER_SIZE 256U

/* Declare a buffer used to store the received data */
static uint8_t buffer[BUFFER_SIZE];
static uint8_t bufferIdx;

/* Flag updated in DMA isr to signal transfer completion */
volatile bool transferComplete;

/* Callback for DMA channels */
void edmaCallback(void *parameter, edma_chn_status_t status)
{
    (void)status;
    (void)parameter;
    /* signal transfer completion */
    transferComplete = true;
}

/* UART rx callback for continuous reception, byte by byte */
void uartRxCallback(void *driverState, uart_event_t event, void *userData)
{
    /* Unused parameters */
    (void)driverState;
    (void)userData;

    /* Check the event type */
    if (event == UART_EVENT_RX_FULL)
    {
        /* The reception stops when newline is received or the buffer is full */
        if ((buffer[bufferIdx] != '\n') && (bufferIdx != (BUFFER_SIZE - 1U)))
        {
            /* Update the buffer index and the rx buffer */
            bufferIdx++;
            LPUART_DRV_SetRxBuffer(INST_LPUART1, &buffer[bufferIdx], 1U);
        }
        else if(bufferIdx >= (BUFFER_SIZE - 1U))
        {
        	buffer[bufferIdx] = '\n';
        }
    }
}

/* Static function used to clear all elements in a buffer */
static void clearBuff(uint8_t *buff, uint32_t size)
{
    uint32_t i;
    for(i = 0U; i < size; i++)
    {
        buff[i] = 0U;
    }
}

/* Function that reads the option chosen by the user */
void readOptionFromConsole(uint8_t * option)
{
    bool strReceived = false;

    while(strReceived == false)
    {
        /* Because the terminal appends new line to user data,
         * receive and store data into a buffer until it is received
         */
        LPUART_DRV_ReceiveData(INST_LPUART1, &buffer[bufferIdx], 1UL);
        /* Wait for transfer to be completed */
        while(LPUART_DRV_GetReceiveStatus(INST_LPUART1, &bytesRemaining) != STATUS_SUCCESS);
        /* Check if current byte is new line */
        if(buffer[bufferIdx] == '\n')
        {
            strReceived = true;
        }
    }

    /* Check for invalid options */
    if((bufferIdx > 2) || (buffer[0] < '0') || (buffer[0] > '3'))
    {
        PRINTF("Invalid option!\r\n");
        *option = 0;
    }
    else
    {
        *option = buffer[0];
    }

    /* Reset buffer index for new reception */
    bufferIdx = 0;
}

/*!
 \brief The main function for the project.
 \details The startup initialization sequence is the following:
 * - startup asm routine
 * - main()
 */
int main(void)
{
  /* Write your local variable definition here */

  /* Declare a variable to store the option entered by user */
  uint8_t option = 0;
  /* Declare a boolean flag to control the demo loop */
  bool exitDemo = false;

  /*** Processor Expert internal initialization. DON'T REMOVE THIS CODE!!! ***/
#ifdef PEX_RTOS_INIT
  PEX_RTOS_INIT(); /* Initialization of the selected RTOS. Macro is defined by the RTOS component. */
#endif
  /*** End of Processor Expert internal initialization.                    ***/

  /* Write your code here */
  /* For example: for(;;) { } */

  /* Initialize and configure clocks
   *     -    see clock manager component for details
   */
  CLOCK_SYS_Init(g_clockManConfigsArr, CLOCK_MANAGER_CONFIG_CNT,
                 g_clockManCallbacksArr, CLOCK_MANAGER_CALLBACK_CNT);
  CLOCK_SYS_UpdateConfiguration(0U, CLOCK_MANAGER_POLICY_AGREEMENT);

  /* Initialize pins
   *    -    See PinSettings component for more info
   */
  PINS_DRV_Init(NUM_OF_CONFIGURED_PINS, g_pin_mux_InitConfigArr);

  /* Initialize the LPUART instance */
  LPUART_DRV_Init(INST_LPUART1, &lpuart1_State, &lpuart1_InitConfig0);

  /* Install the callback for rx events */
  LPUART_DRV_InstallRxCallback(INST_LPUART1, uartRxCallback, NULL);

  /* Initialize eDMA module & channels */
  EDMA_DRV_Init(&dmaController1_State, &dmaController1_InitConfig0,
                edmaChnStateArray, edmaChnConfigArray, EDMA_CONFIGURED_CHANNELS_COUNT);

   /* Demo loop:
   *     - Receive option from user
   *     - Trigger the appropriate action
   */
  while (!exitDemo)
  {
      /* Display the menu message */
      PRINTF(DMA_MENU_TEXT);

      /* Read the user option */
      readOptionFromConsole(&option);

      /* If the option is not valid, display the menu again */
      if(option == 0)
          continue;

      /* Trigger the action chosen by user */
      switch(option)
      {
          case '1':
              /* Single block memory-to-memory transfer */
              triggerSingleBlock(EDMA_CHN0_NUMBER, (uint8_t *)DMA_SAMPLE_BUFFER,
                                 buffer, strlen(DMA_SAMPLE_BUFFER));

              /* Display the source buffer */
              PRINTF("Source buffer:\r\n");
              PRINTF(DMA_SAMPLE_BUFFER);

              /* Display the destination buffer */
              PRINTF("Destination buffer (transferred in single block mode):\r\n");
              PRINTF((char *)buffer);

              /* Clear the destination buffer */
              clearBuff(buffer, BUFFER_SIZE);
              break;
          case '2':
              /* Loop memory-to-memory transfer */
              triggerLoopTransfer(EDMA_CHN0_NUMBER, (uint8_t *)DMA_SAMPLE_BUFFER,
                                  buffer, strlen(DMA_SAMPLE_BUFFER));

              /* Display the source buffer */
              PRINTF("Source buffer:\r\n");
              PRINTF(DMA_SAMPLE_BUFFER);

              /* Display the destination buffer */
              PRINTF("Destination buffer (transferred in loop mode):\r\n");
              PRINTF((char *)buffer);

              /* Clear the destination buffer */
              clearBuff(buffer, BUFFER_SIZE);
              break;
          case '3':
              /* Scatter/gather memory-to-memory transfer */
              triggerScatterGather(EDMA_CHN0_NUMBER, (uint8_t *)DMA_SAMPLE_BUFFER,
                                   buffer, strlen(DMA_SAMPLE_BUFFER));

              /* Display the source buffer */
              PRINTF("Source buffer:\r\n");
              PRINTF(DMA_SAMPLE_BUFFER);

              /* Display the destination buffer */
              PRINTF("Destination buffer (transferred in scatter/gather mode):\r\n");
              PRINTF((char *)buffer);

              /* Clear the destination buffer */
              clearBuff(buffer, BUFFER_SIZE);
              break;
          case '0':
              /* Exit the demo */
              PRINTF("Goodbye!");
              EDMA_DRV_Deinit();
              LPUART_DRV_Deinit(INST_LPUART1);
              exitDemo = true;
              break;
      }
  }

  /*** Don't write any code pass this line, or it will be deleted during code generation. ***/
  /*** RTOS startup code. Macro PEX_RTOS_START is defined by the RTOS component. DON'T MODIFY THIS CODE!!! ***/
  #ifdef PEX_RTOS_START
    PEX_RTOS_START();                  /* Startup of the selected RTOS. Macro is defined by the RTOS component. */
  #endif
  /*** End of RTOS startup code.  ***/
  /*** Processor Expert end of main routine. DON'T MODIFY THIS CODE!!! ***/
  for(;;) {
    if(exit_code != 0) {
      break;
    }
  }
  return exit_code;
  /*** Processor Expert end of main routine. DON'T WRITE CODE BELOW!!! ***/
} /*** End of main routine. DO NOT MODIFY THIS TEXT!!! ***/

/* END main */
/*!
 ** @}
 */
/*
 ** ###################################################################
 **
 **     This file was created by Processor Expert 10.1 [05.21]
 **     for the Freescale S32K series of microcontrollers.
 **
 ** ###################################################################
 */
