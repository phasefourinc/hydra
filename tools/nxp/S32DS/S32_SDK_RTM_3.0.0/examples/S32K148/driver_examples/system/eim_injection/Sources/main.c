/*
 * Copyright (c) 2015 - 2016 , Freescale Semiconductor, Inc.
 * Copyright 2016-2017 NXP
 * All rights reserved.
 *
 * THIS SOFTWARE IS PROVIDED BY NXP "AS IS" AND ANY EXPRESSED OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL NXP OR ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */
/* ###################################################################
**     Filename    : main.c
**     Project     : eim_injection_s32k148
**     Processor   : S32K148_144
**     Version     : Driver 01.00
**     Compiler    : GNU C Compiler
**     Date/Time   : 2017-07-06, 16:54, # CodeGen: 0
**     Abstract    :
**         Main module.
**         This module contains user's application code.
**     Settings    :
**     Contents    :
**         No public methods
**
** ###################################################################*/
/*!
** @file main.c
** @version 01.00
** @brief
**         Main module.
**         This module contains user's application code.
*/
/*!
**  @addtogroup main_module main module documentation
**  @{
*/
/* MODULE main */


/* Including needed modules to compile this module/procedure */
#include "Cpu.h"
#include "eim1.h"
#include "erm1.h"
#include "pin_mux.h"
#include "clockMan1.h"

volatile int exit_code = 0;
/* User includes (#include below this line is not maintained by Processor Expert) */

#include <stdint.h>
#include <stdbool.h>

/* To use with other board than EVB please comment the following line */
#define EVB

#ifdef EVB
    #define LED0            21U /* pin PTE21 - LED RED of LED RGB on DEV-KIT    */
    #define LED1            22U /* pin PTE22 - LED GREEN of LED RGB on DEV-KIT  */
    #define LED_GPIO        PTE /* LED GPIO type */
    #define SW              12U /* pin PTC12 - SW3_BTN0 on DEV-KIT              */
    #define SW_GPIO         PTC /* SW GPIO type                                 */
#else
    #define LED0            0U /* pin PTC0 - LED0 on Motherboard */
    #define LED1            1U /* pin PTC1 - LED1 on Motherboard */
    #define LED_GPIO        PTC /* LED GPIO type */
	#define SW              12U /* pin PTC12 - SW3_BTN0 on DEV-KIT              */
	#define SW_GPIO         PTC /* SW GPIO type                                 */
#endif

/* Declaration variable global */
    uint32_t test;
    uint32_t addr;

/*!
  \brief The main function for the project.
  \details The startup initialization sequence is the following:
 * - __start (startup asm routine)
 * - __init_hardware()
 * - main()
 *   - PE_low_level_init()
 *     - Common_Init()
 *     - Peripherals_Init()
*/
int main(void)
{
  /* Write your local variable definition here */

  /*** Processor Expert internal initialization. DON'T REMOVE THIS CODE!!! ***/
  #ifdef PEX_RTOS_INIT
    PEX_RTOS_INIT();                 /* Initialization of the selected RTOS. Macro is defined by the RTOS component. */
  #endif
  /*** End of Processor Expert internal initialization.                    ***/
    /* Write your local variable definition here */


    /* Initialize and configure clocks
     *  -   Setup system clocks
     *  -   Enable clock feed for Ports and ERM and EIM
     *  -   See Clock Manager component for more info
     */
    CLOCK_SYS_Init(g_clockManConfigsArr, CLOCK_MANAGER_CONFIG_CNT,
                   g_clockManCallbacksArr, CLOCK_MANAGER_CALLBACK_CNT);
    CLOCK_SYS_UpdateConfiguration(0U, CLOCK_MANAGER_POLICY_AGREEMENT);

    /* Initialize pins
      *  -   Setup output pins for LEDs
      *  -   See PinSettings component for more info
      */
     PINS_DRV_Init(NUM_OF_CONFIGURED_PINS, g_pin_mux_InitConfigArr);

     /* Turn off GREEN LED */
 	PINS_DRV_WritePin(LED_GPIO, LED1, 0u);
 	/* Turn on RED LED */
 	PINS_DRV_WritePin(LED_GPIO, LED0, 1u);

    for(;;)
    {
        /* Initialize address used to test */
        *(uint32_t *)0x1FFFFFF0U = 0U;
        if (PINS_DRV_ReadPins(SW_GPIO) & (1U << SW))
        {
            /* Hardware initialization */
            /* Initial for ERM module */
            ERM_DRV_Init(INST_ERM1, ERM_CHANNEL_COUNT0, erm1_InitConfig0);
            /* Initial for EIM module */
            EIM_DRV_Init(INST_EIM1, EIM_CHANNEL_COUNT0, eim1_ChannelConfig0);

            /* Read any address on RAM  */
            /* Enable read region Ram (0x1FFF8000 - 0x20006FFF) when debug equal Flash */
            test = *(uint32_t *)0x1FFFFFF0U;
            (void)test;

            /* Deinit EIM module */
            EIM_DRV_Deinit(INST_EIM1);

            /* Get address which EIM injection error */
            ERM_DRV_GetErrorDetail(INST_ERM1, 0U, &addr);

            /* Check LED */
            if(test == 0)
            {
    			/* Turn off RED LED */
    			PINS_DRV_WritePin(LED_GPIO, LED0, 0u);
    			/* Turn on GREEN LED */
    			PINS_DRV_WritePin(LED_GPIO, LED1, 1u);
            }
            break;
        }
    }

    /*** Don't write any code pass this line, or it will be deleted during code generation. ***/
  /*** RTOS startup code. Macro PEX_RTOS_START is defined by the RTOS component. DON'T MODIFY THIS CODE!!! ***/
  #ifdef PEX_RTOS_START
    PEX_RTOS_START();                  /* Startup of the selected RTOS. Macro is defined by the RTOS component. */
  #endif
  /*** End of RTOS startup code.  ***/
  /*** Processor Expert end of main routine. DON'T MODIFY THIS CODE!!! ***/
  for(;;) {
    if(exit_code != 0) {
      break;
    }
  }
  return exit_code;
  /*** Processor Expert end of main routine. DON'T WRITE CODE BELOW!!! ***/
} /*** End of main routine. DO NOT MODIFY THIS TEXT!!! ***/

/* END main */
/*!
** @}
*/
/*
** ###################################################################
**
**     This file was created by Processor Expert 10.1 [05.21]
**     for the Freescale S32K series of microcontrollers.
**
** ###################################################################
*/

