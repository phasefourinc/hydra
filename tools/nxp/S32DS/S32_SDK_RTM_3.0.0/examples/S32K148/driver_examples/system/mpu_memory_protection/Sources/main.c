/*
 * Copyright (c) 2015 - 2016 , Freescale Semiconductor, Inc.
 * Copyright 2016-2017 NXP
 * All rights reserved.
 *
 * THIS SOFTWARE IS PROVIDED BY NXP "AS IS" AND ANY EXPRESSED OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL NXP OR ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */
/* ###################################################################
**     Filename    : main.c
**     Project     : mpu_memory_protection_s32k148
**     Processor   : S32K148_144
**     Version     : Driver 01.00
**     Compiler    : GNU C Compiler
**     Date/Time   : 2017-10-24, 12:00, # CodeGen: 0
**     Abstract    :
**         Main module.
**         This module contains user's application code.
**     Settings    :
**     Contents    :
**         No public methods
**
** ###################################################################*/
/*!
** @file main.c
** @version 01.00
** @brief
**         Main module.
**         This module contains user's application code.
*/
/*!
**  @addtogroup main_module main module documentation
**  @{
*/
/* MODULE main */


/* Including needed modules to compile this module/procedure */
#include "Cpu.h"
#include "clockMan1.h"
#include "pin_mux.h"
#include "memProtect1.h"

volatile int exit_code = 0U;
/* User includes (#include below this line is not maintained by Processor Expert) */

#include <stdint.h>
#include <stdbool.h>

/* This example is setup to work by default with EVB. To use it with other boards
   please comment the following line
*/
#define EVB

#ifdef EVB
    #define ON              1U         /* LED ON */
    #define OFF             0U         /* LED OFF */
    #define LED_GPIO        PTE        /* LED GPIO type */
    #define LED_RED         21U        /* pin PTE21 - LED RGB on DEV-KIT */
    #define LED_GREEN       22U        /* pin PTE22 - LED RGB on DEV-KIT */
    #define SW_GPIO         PTC        /* SW GPIO type */
    #define SW              12U        /* pin PTC12 - SW3_BTN0 on DEV-KIT */
#else
    #define ON              0U         /* LED ON */
    #define OFF             1U         /* LED OFF */
    #define LED_GPIO        PTC        /* LED GPIO type */
    #define LED_RED         0U         /* pin PTC0 - LED0 on Motherboard */
    #define LED_GREEN       1U         /* pin PTC1 - LED1 on Motherboard */
    #define SW_GPIO         PTC        /* SW GPIO type */
    #define SW              12U        /* pin PTC12 - SW3_BTN0 on Motherboard */
#endif

/* Protected address in Flash memory */
#define ADDRESS_PROTECT     0x0017FF04U

/* Expected error */
mpu_access_err_info_t expectedError =
{
    .master     = FEATURE_MPU_MASTER_CORE,            /* Error Core master */
    .attributes = MPU_DATA_ACCESS_IN_SUPERVISOR_MODE, /* Error data access in supervisor mode */
    .accessType = MPU_ERR_TYPE_READ,                  /* Error read access */
    .accessCtr  = 0xA000U,                            /* Error occurs on region 0 and region 2 */
    .addr       = ADDRESS_PROTECT,                    /* Error address */
#if FEATURE_MPU_HAS_PROCESS_IDENTIFIER
    .processorIdentification = 0U                     /* Error processor identifier */
#endif
};

/*!
 * @brief The error comparator
 *
 * @param[in] error1 The error access.
 * @param[in] error2 The error access.
 * @return Status
 *         - true : The errors are the same.
 *         - false: The errors are different.
 */
bool ErrorCompare(mpu_access_err_info_t error1, mpu_access_err_info_t error2)
{
    bool status = false;

    if ((error1.master == error2.master)
        && (error1.attributes == error2.attributes)
        && (error1.accessType == error2.accessType)
        && (error1.accessCtr  == error2.accessCtr)
        && (error1.addr == error2.addr))
    {
        status = true;
    #if FEATURE_MPU_HAS_PROCESS_IDENTIFIER
        if(error1.processorIdentification != error2.processorIdentification)
        {
            status = false;
        }
    #endif
    }

    return status;
}

/* HardFault Handler */
void HardFault_Handler(void)
{
    /* Enables region 3 to grant Core read permission */
    MPU_DRV_EnableRegion(MEMPROTECT1, 3U, true);
}

/*!
  \brief The main function for the project.
  \details The startup initialization sequence is the following:
 * - startup asm routine
 * - main()
*/
int main(void)
{
  /* Write your local variable definition here */
    bool status = false;
    status_t returnCode = STATUS_ERROR;
    volatile uint32_t test = 0U;
    mpu_access_err_info_t reportedError;

  /*** Processor Expert internal initialization. DON'T REMOVE THIS CODE!!! ***/
  #ifdef PEX_RTOS_INIT
    PEX_RTOS_INIT();                   /* Initialization of the selected RTOS. Macro is defined by the RTOS component. */
  #endif
  /*** End of Processor Expert internal initialization.                    ***/

    /* Initialize clock module */
    CLOCK_SYS_Init(g_clockManConfigsArr, CLOCK_MANAGER_CONFIG_CNT, g_clockManCallbacksArr, CLOCK_MANAGER_CALLBACK_CNT);
    CLOCK_SYS_UpdateConfiguration(0U, CLOCK_MANAGER_POLICY_AGREEMENT);

    /* Initialize LEDs and Button configuration */
    PINS_DRV_Init(NUM_OF_CONFIGURED_PINS, g_pin_mux_InitConfigArr);

    /* LEDs off */
    PINS_DRV_WritePin(LED_GPIO, LED_RED, OFF);
    PINS_DRV_WritePin(LED_GPIO, LED_GREEN, OFF);

    /* Initialize MPU module */
    returnCode = MPU_DRV_Init(MEMPROTECT1, MPU_NUM_OF_REGION_CFG0, memProtect1_UserConfig0);

    /* Check initialization */
    if (returnCode == STATUS_SUCCESS)
    {
        /* Turn on led green */
        PINS_DRV_WritePin(LED_GPIO, LED_GREEN, ON);
    }

    /* Infinite loop */
    for(;;)
    {
        /* Check button */
        if (PINS_DRV_ReadPins(SW_GPIO) & (1U << SW))
        {
            /* Disable region 3 to ignore Core read permission */
            MPU_DRV_EnableRegion(MEMPROTECT1, 3U, false);
        }

        /* Read address in flash memory where protected by MPU */
        test = *((uint32_t *)ADDRESS_PROTECT);
        /* Casting to void to avoid "Set but unused warnings" */
        (void)test;

        /* Get the detail of error access on slave port 0 */
        status = MPU_DRV_GetDetailErrorAccessInfo(MEMPROTECT1, FEATURE_MPU_SLAVE_FLASH_BOOTROM, &reportedError);

        /* Check error status */
        if (status)
        {
            /* Compare with expected error */
            status = ErrorCompare(reportedError, expectedError);

            /* If true */
            if (status)
            {
                /* Turn on led red and turn off led green */
                PINS_DRV_WritePin(LED_GPIO, LED_RED, ON);
                PINS_DRV_WritePin(LED_GPIO, LED_GREEN, OFF);
                break;
            }
        }
    }

  /*** Don't write any code pass this line, or it will be deleted during code generation. ***/
  /*** RTOS startup code. Macro PEX_RTOS_START is defined by the RTOS component. DON'T MODIFY THIS CODE!!! ***/
  #ifdef PEX_RTOS_START
    PEX_RTOS_START();                  /* Startup of the selected RTOS. Macro is defined by the RTOS component. */
  #endif
  /*** End of RTOS startup code.  ***/
  /*** Processor Expert end of main routine. DON'T MODIFY THIS CODE!!! ***/
  for(;;) {
    if(exit_code != 0) {
      break;
    }
  }
  return exit_code;
  /*** Processor Expert end of main routine. DON'T WRITE CODE BELOW!!! ***/
} /*** End of main routine. DO NOT MODIFY THIS TEXT!!! ***/

/* END main */
/*!
** @}
*/
/*
** ###################################################################
**
**     This file was created by Processor Expert 10.1 [05.21]
**     for the Freescale S32K series of microcontrollers.
**
** ###################################################################
*/
