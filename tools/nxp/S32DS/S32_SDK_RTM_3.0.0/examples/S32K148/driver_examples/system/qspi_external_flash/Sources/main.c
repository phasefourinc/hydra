/* 
 * Copyright (c) 2015 - 2016 , Freescale Semiconductor, Inc.                             
 * Copyright 2016-2017 NXP                                                                    
 * All rights reserved.                                                                  
 *                                                                                       
 * THIS SOFTWARE IS PROVIDED BY NXP "AS IS" AND ANY EXPRESSED OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL NXP OR ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.                          
 */


/* Including needed modules to compile this module/procedure */
#include "Cpu.h"
#include "clockMan1.h"
#include "pin_mux.h"
#include "flash_mx25l6433f1.h"
#include "qspi1.h"
#include "dmaController1.h"
#if CPU_INIT_CONFIG
  #include "Init_Config.h"
#endif

volatile int exit_code = 0;
/* User includes (#include below this line is not maintained by Processor Expert) */

#include <stdint.h>
#include <stdbool.h>


#define BUFFER_SIZE 1024
#define SECTOR_ADDR 0x2000U
uint8_t txBuffer[BUFFER_SIZE];
uint8_t rxBuffer[BUFFER_SIZE] = {0x00};
volatile status_t status;
flash_mx25l6433f_state_t flashState;
qspi_state_t qspiState;


/* Check if data in Tx and Rx buffers matches */
void checkBuffers(uint8_t *tx, uint8_t *rx, uint32_t size)
{
    uint32_t i;
    
    for (i = 0; i < size; i++)
    {
        DEV_ASSERT(rx[i] == tx[i]);
    }
}

/*! 
  \brief The main function for the project.
  \details The startup initialization sequence is the following:
 * - __start (startup asm routine)
 * - __init_hardware()
 * - main()
 *   - PE_low_level_init()
 *     - Common_Init()
 *     - Peripherals_Init()
*/
int main(void)
{
  uint32_t count;

  /*** Processor Expert internal initialization. DON'T REMOVE THIS CODE!!! ***/
  #ifdef PEX_RTOS_INIT
    PEX_RTOS_INIT();                 /* Initialization of the selected RTOS. Macro is defined by the RTOS component. */
  #endif
  /*** End of Processor Expert internal initialization.                    ***/

  /* Write your code here */
  /* Initialize and configure clocks
   *    -   see clock manager component for details
   */
  CLOCK_SYS_Init(g_clockManConfigsArr, CLOCK_MANAGER_CONFIG_CNT,
                        g_clockManCallbacksArr, CLOCK_MANAGER_CALLBACK_CNT);
  CLOCK_SYS_UpdateConfiguration(0U, CLOCK_MANAGER_POLICY_AGREEMENT);

  /* Initialize pins
   *    -   See PinSettings component for more info
   */
  PINS_DRV_Init(NUM_OF_CONFIGURED_PINS, g_pin_mux_InitConfigArr);

  /* Initialize txBuffer */
  for (count = 0; count < BUFFER_SIZE / 2; count++)
  {
      txBuffer[count] = (uint8_t)count;
  }
  for (count = BUFFER_SIZE / 2; count < BUFFER_SIZE; count++)
  {
      txBuffer[count] = (0xFFU - (uint8_t)count);
  }

  /* Initialize DMA
   *    -   See edma component for more info
   */
  EDMA_DRV_Init(&dmaController1_State, &dmaController1_InitConfig0, edmaChnStateArray, edmaChnConfigArray, EDMA_CONFIGURED_CHANNELS_COUNT);

  /* Initialize qspi driver */
  status = QSPI_DRV_Init(INST_QSPI1, &qspi1_Config0, &qspiState);
  DEV_ASSERT(status == STATUS_SUCCESS);

  /* Initialize external flash driver */
  status = FLASH_MX25L6433F_DRV_Init(INST_QSPI1, &flash_mx25l6433f1_Config0, &flashState);
  DEV_ASSERT(status == STATUS_SUCCESS);

  /* Erase one sector */
  status = FLASH_MX25L6433F_DRV_Erase4K(INST_QSPI1, SECTOR_ADDR);
  DEV_ASSERT(status == STATUS_SUCCESS);

  /* Wait for erase to finish */
  while ((status = FLASH_MX25L6433F_DRV_GetStatus(INST_QSPI1)) == STATUS_BUSY) { }
  DEV_ASSERT(status == STATUS_SUCCESS);

  for (count = 0; count < 4; count++)
  {
      /* Program 256 bytes at a time, this is the maximum allowed by the external flash device */
      status = FLASH_MX25L6433F_DRV_Program(INST_QSPI1, SECTOR_ADDR + (256 * count), txBuffer + (256 * count), 256);
      DEV_ASSERT(status == STATUS_SUCCESS);
 
      /* Wait for program to finish */
      while ((status = FLASH_MX25L6433F_DRV_GetStatus(INST_QSPI1)) == STATUS_BUSY) { }
      DEV_ASSERT(status == STATUS_SUCCESS);
  }

  /* Read all bytes */
  status = FLASH_MX25L6433F_DRV_Read(INST_QSPI1, SECTOR_ADDR, rxBuffer, BUFFER_SIZE);
  DEV_ASSERT(status == STATUS_SUCCESS);
 
  /* Wait for program to finish */
  while ((status = FLASH_MX25L6433F_DRV_GetStatus(INST_QSPI1)) == STATUS_BUSY) { }
  DEV_ASSERT(status == STATUS_SUCCESS);

  /* Check received data */
  checkBuffers(txBuffer, rxBuffer, BUFFER_SIZE);

  /* If we get here it means the test has passed */
  DEV_ASSERT(0);

  /*** Don't write any code pass this line, or it will be deleted during code generation. ***/
  /*** RTOS startup code. Macro PEX_RTOS_START is defined by the RTOS component. DON'T MODIFY THIS CODE!!! ***/
  #ifdef PEX_RTOS_START
    PEX_RTOS_START();                  /* Startup of the selected RTOS. Macro is defined by the RTOS component. */
  #endif
  /*** End of RTOS startup code.  ***/
  /*** Processor Expert end of main routine. DON'T MODIFY THIS CODE!!! ***/
  for(;;) {
    if(exit_code != 0) {
      break;
    }
  }
  return exit_code;
  /*** Processor Expert end of main routine. DON'T WRITE CODE BELOW!!! ***/
} /*** End of main routine. DO NOT MODIFY THIS TEXT!!! ***/

/* END main */
/*!
** @}
*/
/*
** ###################################################################
**
**     This file was created by Processor Expert 10.1 [05.21]
**     for the Freescale S32K series of microcontrollers.
**
** ###################################################################
*/
