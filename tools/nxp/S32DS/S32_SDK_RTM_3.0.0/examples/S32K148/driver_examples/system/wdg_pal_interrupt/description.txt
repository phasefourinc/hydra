This is a simple application created to show how WDG PAL works over different peripherals.
The example documentation can be found in the S32 SDK documentation at Examples and Demos section. (<SDK_PATH>/doc/Start_Here.html)
