/*!
    @page edma_transfer_s32k148_group eDMA Transfer 
    @brief Example application showing a subset of the eDMA functionalities
    
    ## Application description ##
    ______
    The purpose of this driver example is to show you how to use the eDMA in the following transfer scenarios for the S32K148 MCU with the S32 SDK API.
    
    - Single block memory-to-memory transfer
    - Loop memory-to-memory transfer
    - Scatter/gather memory-to-memory transfer
    - Memory-to-peripheral transfer (this feature is implicitly used with the LPUART driver)
    - Peripheral-to-memory transfer (this feature is implicitly used with the LPUART driver)
    
    ## Prerequisites ##
    ______
    To run the example you will need to have the following items:
    - 1 S32K148 board
    - 1 Power Adapter 12V (if the board cannot be powered from the USB port)
    - 1 Personal Computer
    - 1 Jlink Lite Debugger (optional, users can use Open SDA)
    - UART to USB converter if it is not included on the target board. (Please consult your boards documentation to check if UART-USB converter is present).
    
    ## Boards supported ##
    ______
    The following boards are supported by this application:
    - S32K148EVB-Q144
    - S32K148-MB
    
    ## Hardware Wiring ##
    ______
    The following connections must be done to for this example application to work:
    
    PIN FUNCTION           |         S32K148EVB-Q144        |     S32K148-MB  
    -----------------------|--------------------------------|-----------------
    LPUART1 TX (\b PTC7)   |  UART_TX - wired on the board  |   J11.26 - J20.2
    LPUART1 RX (\b PTC6)   |  UART_RX - wired on the board  |   J11.25 - J20.5
    
    ## How to run ##
    ______
    
    #### 1. Importing the project into the workspace ####
    After opening S32 Design Studio, go to \b File -> \b New \b S32DS \b Project \b From... and select \b edma_transfer_s32k148. Then click on \b Finish. \n
    The project should now be copied into you current workspace.
    #### 2. Generating the Processor Expert configuration ####
    First go to \b Project \b Explorer View in S32 DS and select the current project(\b edma_transfer_s32k148). Then go to \b Project and click on \b Generate \b Processor \b Expert \b Code \n
    Wait for the code generation to be completed before continuing to the next step.
    #### 3. Building the project ####
    Select the configuration to be built \b FLASH (Debug_FLASH) or \b RAM (Debug_RAM) by left clicking on the downward arrow corresponding to the \b build button(@image hammer.png). 
    Wait for the build action to be completed before continuing to the next step.
    #### 4. Running the project ####
    Go to \b Run and select \b Debug \b Configurations. There will be four debug configurations for this project:
     Configuration Name | Description
     -------------------|------------
     \b edma_transfer_s32k148_debug_ram_jlink | Debug the RAM configuration using Segger Jlink debuggers
     \b edma_transfer_s32k148_debug_flash_jlink | Debug the FLASH configuration using Segger Jlink debuggers
     \b edma_transfer_s32k148_debug_ram_pemicro | Debug the RAM configuration using PEMicro debuggers 
     \b edma_transfer_s32k148_debug_flash_pemicro | Debug the FLASH configuration using PEMicro debuggers 
    \n Select the desired debug configuration and click on \b Launch. Now the perspective will change to the \b Debug \b Perspective. \n
    Use the controls to control the program flow.
    
    @note For more detailed information related to S32 Design Studio usage please consult the available documentation.
    
    ## Notes ##
    ______
    
    For this example it is necessary to open a terminal emulator and configure it with:
    -   115200 baud
    -   One stop bit
    -   No parity
    -   No flow control
    -   '\\n' line ending
*/

