/*
 * Copyright (c) 2015 - 2016 , Freescale Semiconductor, Inc.
 * Copyright 2016-2017 NXP
 * All rights reserved.
 *
 * THIS SOFTWARE IS PROVIDED BY NXP "AS IS" AND ANY EXPRESSED OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL NXP OR ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */
/* ###################################################################
**     Filename    : main.c
**     Project     : lin_slave_s32k148
**     Processor   : S32K148_144
**     Version     : Driver 01.00
**     Compiler    : GNU C Compiler
**     Date/Time   : 2015-09-22, 10:58, # CodeGen: 0
**     Abstract    :
**         Main module.
**         This module contains user's application code.
**     Settings    :
**     Contents    :
**         No public methods
**
** ###################################################################*/
/*!
** @file main.c
** @version 01.00
** @brief
**         Main module.
**         This module contains user's application code.
*/
/*!
**  @addtogroup main_module main module documentation
**  @{
*/
/* MODULE main */


/* Including needed modules to compile this module/procedure */
#include "Cpu.h"
#include "clockMan1.h"
#include "sbc_uja113x1.h"
#include "lpspiCom1.h"
#include "pin_mux.h"
#include "dmaController1.h"
#include "lpTmr1.h"
#include "lin_cfg.h"
#include "lin1.h"
#include "lin_common_api.h"

#if CPU_INIT_CONFIG
  #include "Init_Config.h"
#endif
volatile int exit_code = 0;
/* User includes (#include below this line is not maintained by Processor Expert) */
/* This example is setup to work by default with EVB. To use it with other boards
   please comment the following line
*/
#define EVB

#ifdef EVB
    #define PORT_LED0_INDEX           (23u)
    #define PORT_LED1_INDEX           (21u)
    #define PORT_LED2_INDEX           (22u)
    #define PORT_LED0_MASK            (0x1u << PORT_LED0_INDEX)
    #define PORT_LED1_MASK            (0x1u << PORT_LED1_INDEX)
    #define PORT_LED2_MASK            (0x1u << PORT_LED2_INDEX)
    #define LED_GPIO_PORT             (PTE)
    #define PORT_BTN0_INDEX           (12u)
    #define PORT_BTN1_INDEX           (13u)
    #define PORT_BTN0_MASK            (0x1u << PORT_BTN0_INDEX)
    #define PORT_BTN1_MASK            (0x1u << PORT_BTN1_INDEX)
    #define BTN_GPIO_PORT             (PTC)
    #define BTN_PORT_NAME             (PORTC)
    #define BTN_PORT_IRQn             (PORTC_IRQn)
    #define SBC_FORCE_NORMAL_MODE     (1u)
#else
    #define PORT_LED0_INDEX           (0u)
    #define PORT_LED1_INDEX           (1u)
    #define PORT_LED2_INDEX           (2u)
    #define PORT_LED0_MASK            (0x1u << PORT_LED0_INDEX)
    #define PORT_LED1_MASK            (0x1u << PORT_LED1_INDEX)
    #define PORT_LED2_MASK            (0x1u << PORT_LED2_INDEX)
    #define LED_GPIO_PORT             (PTC)
    #define PORT_BTN0_INDEX           (12u)
    #define PORT_BTN1_INDEX           (13u)
    #define PORT_BTN0_MASK            (0x1u << PORT_BTN0_INDEX)
    #define PORT_BTN1_MASK            (0x1u << PORT_BTN1_INDEX)
    #define BTN_GPIO_PORT             (PTC)
    #define BTN_PORT_NAME             (PORTC)
    #define BTN_PORT_IRQn             (PORTC_IRQn)
#endif


/* (CLK (MHz)* timer period (us) / Prescaler) */
#define TIMER_COMPARE_VAL (uint16_t)(2000U)
#define TIMER_TICKS_1US   (uint16_t)(4U)

#define MOTOR_SELECTION_INCREASE (1u)
#define MOTOR_SELECTION_DECREASE (2u)
#define MOTOR_SELECTION_STOP     (3u)

#define MOTOR1_OVER_TEMP   (200u)
#define MOTOR1_MAX_TEMP    (100u)
#define MOTOR1_MIN_TEMP    (30u)

uint16_t timerOverflowInterruptCount = 0U;
volatile uint16_t capturedValue = 0U;
volatile bool linEnabled = false;
l_u8 Motor1_Selection = 0U;
l_u8 Motor1_temp = 30U;

#if (SBC_FORCE_NORMAL_MODE == 0)
/* SBC driver configuration structure.  */
uja113x_drv_cfg_t drvConfig =
{
    .chipType      = UJA1132HW5V0,  /* chipType */
    .lpspiInstance = 1U,            /* SPI instance */
    .timeout       = 1000U         /* SPI timeout */
};
/* LIN control register configuration structure */
uja113x_linc_cfg_t lincConfig =
{
    .lin2SlopeCtrl = LSC2_SLOPE_ACTIVE,
    .lin2ModeCtrl  = LMC2_TRANSCEIVER_ON,
    .lin1SlopeCtrl = LSC1_SLOPE_ACTIVE,
    .lin1ModeCtrl  = LMC2_TRANSCEIVER_ON
};

/*
 * @brief Function to initialize and configure the SBC
 */
void SBCInit(void)
{
    LPSPI_DRV_MasterInit(LPSPICOM1,&lpspiCom1State,&lpspiCom1_MasterConfig0);
    /* Initialize SBC */
    UJA113X_RegisterDevice(LPSPICOM1, &drvConfig);
    UJA113X_SetMode(LPSPICOM1, MC_NORMAL);
    UJA113X_SetLin(LPSPICOM1, &lincConfig);
}
#endif

/*!
 * @brief LPTMR Interrupt Service Routine
 * The ISR will call LIN timeout service every 500us
 * and will increase or decrease motor temperature every
 * 0.1s, depending on the motor speed trend.
 */
void LPTMR_ISR(void)
{
    /* Static variable, used to count if the timeout has passed to
     * provide the LIN scheme tick.
     */
    static uint32_t interruptCount = 0UL;
    /* Timer Interrupt Handler */
    lin_lld_timeout_service(LI0);

    /* Update motor temperature every 0.1s, based on the
     * motor speed trend.
     */
    if (++interruptCount >= 200u)
    {
        interruptCount = 0u;

        /* Increase temp */
        if (Motor1_Selection == MOTOR_SELECTION_INCREASE)
        {
            Motor1_temp++;
        }
        /* Decrease temp */
        else if (Motor1_Selection == MOTOR_SELECTION_DECREASE)
        {
            Motor1_temp--;
        }
    }

    /* Increment overflow count */
    timerOverflowInterruptCount++;
    /* Clear compare flag */
    LPTMR_DRV_ClearCompareFlag(INST_LPTMR1);
}

/*!
 * @brief Callback function to get time interval in nano seconds
 * @param[out] ns - number of nanoseconds passed since the last call of the function
 * @return dummy value
 */
uint32_t timerGetTimeIntervalCallback0(uint32_t *ns)
{
    static uint32_t previousCountValue = 0UL;
    uint32_t counterValue;
    counterValue = capturedValue;
    *ns = ((uint32_t)(counterValue + timerOverflowInterruptCount * TIMER_COMPARE_VAL - previousCountValue)) * 1000UL / TIMER_TICKS_1US;
    timerOverflowInterruptCount = 0UL;
    previousCountValue = counterValue;
    return 0UL;
}

/*!
 * @brief Interrupt Service Routine for buttons
 * Depending on which buttons were pressed LIN scheme will be
 * set to sleep mode or normal mode.
 */
void BTNPORT_IRQHandler(void)
{
    /* If SW3/Button 1 is pressed */
    if (PINS_DRV_GetPortIntFlag(BTN_PORT_NAME) & (1 << PORT_BTN0_INDEX))
    {
        PINS_DRV_ClearPinIntFlagCmd(BTN_PORT_NAME, PORT_BTN0_INDEX);
        if (Motor1_temp < 255U)
        {
          Motor1_temp += 60U;
        }
    }

    /* If SW4/Button 2 is pressed */
    if (PINS_DRV_GetPortIntFlag(BTN_PORT_NAME) & (1 << PORT_BTN1_INDEX))
    {
        PINS_DRV_ClearPinIntFlagCmd(BTN_PORT_NAME, PORT_BTN1_INDEX);
        Motor1_temp = 30U;
    }
}

/*!
 * @brief Interrupt handler for LPUART Rx pin port event
 * This function will capture the time stamp of the Rx pin
 * level transition and will run Auto Baudrate function
 * if required.
 */
void RXPIN_IRQHandler(void)
{
    static bool autoBaudComplete = false;

    /* Check if the interrupt is triggered by the LPUART Rx Pin */
    if(PINS_DRV_GetPortIntFlag(PORTA) & (1UL << 8))
    {
        /* Capture transition time stamp */
        capturedValue = LPTMR_DRV_GetCounterValueByCount(INST_LPTMR1);
        /* If the auto baud process is not completed and lin is enabled, call
         * LIN_DRV_AutoBaudCapture
         */
        if((!autoBaudComplete) && (linEnabled))
        {
            if (LIN_DRV_AutoBaudCapture(INST_LIN1) == STATUS_SUCCESS)
            {
                autoBaudComplete = true;
            }
        }
    }
    /* Clear PORT interrupt register */
    PINS_DRV_ClearPortIntFlagCmd(PORTA);
}

/*!
 * @brief Main LIN slave task
 * This function will initialize the LIN interface and manipulate
 * the received data from the master.
 * Depending on the received data, the motor speed will be increased/decreased
 * or the motor will be stopped.
 */
void lin_slave_task(void)
{
    /* Initialize LIN network interface */
    l_sys_init();
    l_ifc_init(LI0);
    linEnabled = true;
    /* Infinite loop */
    for (; ; )
    {
        /* Check if temp signal is updated */
        if (l_flg_tst_LI0_Motor1Selection_flag())
        {
            /* Clear this flag... */
            l_flg_clr_LI0_Motor1Selection_flag();

            /* Store selection data */
            Motor1_Selection = l_u8_rd_LI0_Motor1Selection();

            /* The application will change Motor selection in case
            the temperature is greater than maximum value to release motor power
            This will be transfered by unconditional frame type in LIN bus */
            l_u8_wr_LI0_Motor1Temp(Motor1_temp);

            /* Check if power off motor due to high temperature */
            if (Motor1_Selection == MOTOR_SELECTION_STOP)
            {
                /* Turn on Red LED */
                PINS_DRV_WritePin(LED_GPIO_PORT, PORT_LED1_INDEX, 1U);
                /* Turn off Blue LED */
                PINS_DRV_WritePin(LED_GPIO_PORT, PORT_LED0_INDEX, 0U);
                /* Turn off Green LED */
                PINS_DRV_WritePin(LED_GPIO_PORT, PORT_LED2_INDEX, 0U);
            }

            if (Motor1_Selection == MOTOR_SELECTION_INCREASE)
            {
                /* Turn on Green LED */
                PINS_DRV_WritePin(LED_GPIO_PORT, PORT_LED2_INDEX, 1U);
                /* Turn off Red LED */
                PINS_DRV_WritePin(LED_GPIO_PORT, PORT_LED1_INDEX, 0U);
                /* Turn off Blue LED */
                PINS_DRV_WritePin(LED_GPIO_PORT, PORT_LED0_INDEX, 0U);
            }

            if (Motor1_Selection == MOTOR_SELECTION_DECREASE)
            {
                /* Turn on Blue LED */
                PINS_DRV_WritePin(LED_GPIO_PORT, PORT_LED0_INDEX, 1U);
                /* Turn off Red LED */
                PINS_DRV_WritePin(LED_GPIO_PORT, PORT_LED1_INDEX, 0U);
                /* Turn off Green LED */
                PINS_DRV_WritePin(LED_GPIO_PORT, PORT_LED2_INDEX, 0U);
            }
        }

        /* Check node state */
        if (LIN_NODE_STATE_SLEEP_MODE == lin_lld_get_state(LI0))
        {
            /* Turn off all LEDs */
            PINS_DRV_WritePin(LED_GPIO_PORT, PORT_LED1_INDEX, 0U);
            PINS_DRV_WritePin(LED_GPIO_PORT, PORT_LED0_INDEX, 0U);
            PINS_DRV_WritePin(LED_GPIO_PORT, PORT_LED2_INDEX, 0U);
        }
    }
}

/*!
  \brief The main function for the project.
  \details The startup initialization sequence is the following:
 * - startup asm routine
 * - main()
*/
int main(void)
{
  /* Write your local variable definition here */

  /*** Processor Expert internal initialization. DON'T REMOVE THIS CODE!!! ***/
  #ifdef PEX_RTOS_INIT
    PEX_RTOS_INIT();                   /* Initialization of the selected RTOS. Macro is defined by the RTOS component. */
  #endif
  /*** End of Processor Expert internal initialization.                    ***/

    /* Initialize and configure clocks
     *  -   Setup system clocks, dividers
     *  -   Configure LPUART clock, GPIO clock
     *  -   see clock manager component for more details
     */
    CLOCK_SYS_Init(g_clockManConfigsArr, CLOCK_MANAGER_CONFIG_CNT,
                        g_clockManCallbacksArr, CLOCK_MANAGER_CALLBACK_CNT);
    CLOCK_SYS_UpdateConfiguration(0U, CLOCK_MANAGER_POLICY_AGREEMENT);

    /* Initialize pins
     *  -   Init LPUART and GPIO pins
     *  -   See PinSettings component for more info
     */
    PINS_DRV_Init(NUM_OF_CONFIGURED_PINS, g_pin_mux_InitConfigArr);

#if (SBC_FORCE_NORMAL_MODE == 0)
    /* Initialize SBC */
    SBCInit();
#endif

    /* Enable button interrupt */
    INT_SYS_InstallHandler(BTN_PORT_IRQn, BTNPORT_IRQHandler, (isr_t *)NULL);
    INT_SYS_EnableIRQ(BTN_PORT_IRQn);

    /* For auto baud rate, we need to activate the interrupt for the LPUART Rx Pin,
     * which needs to be triggered on both edges of the signal.
     */
    PINS_DRV_SetPinIntSel(PORTA, 8UL, PORT_INT_EITHER_EDGE);
    /* Install the handler for the LPUART RX pin */
    INT_SYS_InstallHandler(PORTA_IRQn, RXPIN_IRQHandler, (isr_t *)NULL);
    /* Enable PORT interrupt */
    INT_SYS_EnableIRQ(PORTA_IRQn);

    /* Initialize LPTMR */
    LPTMR_DRV_Init(INST_LPTMR1, &lpTmr1_config0, false);
    INT_SYS_InstallHandler(LPTMR0_IRQn, LPTMR_ISR, (isr_t *)NULL);
    INT_SYS_EnableIRQ(LPTMR0_IRQn);
    LPTMR_DRV_StartCounter(INST_LPTMR1);

    /* Start LIN slave task */
    lin_slave_task();

  /*** Don't write any code pass this line, or it will be deleted during code generation. ***/
  /*** RTOS startup code. Macro PEX_RTOS_START is defined by the RTOS component. DON'T MODIFY THIS CODE!!! ***/
  #ifdef PEX_RTOS_START
    PEX_RTOS_START();                  /* Startup of the selected RTOS. Macro is defined by the RTOS component. */
  #endif
  /*** End of RTOS startup code.  ***/
  /*** Processor Expert end of main routine. DON'T MODIFY THIS CODE!!! ***/
  for(;;) {
    if(exit_code != 0) {
      break;
    }
  }
  return exit_code;
  /*** Processor Expert end of main routine. DON'T WRITE CODE BELOW!!! ***/
} /*** End of main routine. DO NOT MODIFY THIS TEXT!!! ***/

/* END main */
/*!
** @}
*/
/*
** ###################################################################
**
**     This file was created by Processor Expert 10.1 [05.21]
**     for the Freescale S32K series of microcontrollers.
**
** ###################################################################
*/
