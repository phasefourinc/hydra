This is an application created to showcase the boot protection feature of the CSEc module.
 
The example documentation can be found in the S32 SDK documentation at Examples and Demos section. (<SDK_PATH>/doc/Start_Here.html)