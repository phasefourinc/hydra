/*
 * Copyright 2017 NXP
 * All rights reserved.
 *
 * THIS SOFTWARE IS PROVIDED BY NXP "AS IS" AND ANY EXPRESSED OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL NXP OR ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */
/* ###################################################################
**     Filename    : main.c
**     Project     : scst_s32k148
**     Processor   : S32K148_176
**     Version     : Driver 01.00
**     Compiler    : GNU C Compiler
**     Date/Time   : 2017-05-07, 10:29, # CodeGen: 0
**     Abstract    :
**         Main module.
**         This module contains user's application code.
**     Settings    :
**     Contents    :
**         No public methods
**
** ###################################################################*/
/*!
** @file main.c
** @version 01.00
** @brief
**         Main module.
**         This module contains user's application code.
*/
/*!
**  @addtogroup main_module main module documentation
**  @{
*/
/* MODULE main */

/* Including needed modules to compile this module/procedure */
#include "Cpu.h"
#include "clockMan1.h"
#include "pin_mux.h"
#include "scst1.h"
#if CPU_INIT_CONFIG
  #include "Init_Config.h"
#endif

volatile int exit_code = 0;
/* User includes (#include below this line is not maintained by Processor Expert) */

#include <stdint.h>
#include <stdbool.h>
#include <string.h>

/* This example is setup to work by default with EVB. To use it with other boards
   please comment the following line
*/

#define EVB

#ifdef EVB
    #define LED_PORT    PORTE
    #define GPIO_PORT   PTE
    #define PCC_CLOCK   PCC_PORTE_CLOCK
    #define LED1        22U
    #define LED2        21U
#else
    #define LED_PORT    PORTC
    #define GPIO_PORT   PTC
    #define PCC_CLOCK   PCC_PORTC_CLOCK
    #define LED1        28U
    #define LED2        29U
#endif

/* Define Test ID for test where will be injected error */
#define INJECT_ERROR_TO_TEST_NUM 40U
#define ACCUMULATED_SIGNATURE_ID0_43 0x929940F1U

/* Linker defined variables used to copy initialized data from ROM to RAM */
extern uint32_t __SCST_DATA_ROM[];
extern uint32_t __SCST_DATA_SIZE[];
extern uint32_t __scst_data_start__[];

/*!
  \brief The main function for the project.
  \details The startup initialization sequence is the following:
 * - __start (startup asm routine)
 * - __init_hardware()
 * - main()
 *   - PE_low_level_init()
 *     - Common_Init()
 *     - Peripherals_Init()
*/
int main(void)
{
  m4_scst_uint32_t result;
  /*** Processor Expert internal initialization. DON'T REMOVE THIS CODE!!! ***/
  #ifdef PEX_RTOS_INIT
    PEX_RTOS_INIT();                 /* Initialization of the selected RTOS. Macro is defined by the RTOS component. */
  #endif
  /*** End of Processor Expert internal initialization.                    ***/

  /* Initialize and configure clocks
   *    -   see clock manager component for details
   */
  CLOCK_SYS_Init(g_clockManConfigsArr, CLOCK_MANAGER_CONFIG_CNT,
                        g_clockManCallbacksArr, CLOCK_MANAGER_CALLBACK_CNT);
  CLOCK_SYS_UpdateConfiguration(0U, CLOCK_MANAGER_POLICY_AGREEMENT);

  /* Initialize pins
   *    -   See PinSettings component for more info
   */
  PINS_DRV_Init(NUM_OF_CONFIGURED_PINS, g_pin_mux_InitConfigArr);

  /* Output direction for LED0 & LED1 */
  PINS_DRV_SetPinsDirection(GPIO_PORT, ((1 << LED1) | (1 << LED2)));

  /* Set Output value LED0 & LED1 */
  PINS_DRV_SetPins(GPIO_PORT, 1 << LED1);
  PINS_DRV_ClearPins(GPIO_PORT, 1 << LED2);

  /* Copy sCST initialized data from ROM to RAM */
  memcpy(__scst_data_start__, __SCST_DATA_ROM, (uint32_t)__SCST_DATA_SIZE);
  
  /* Inject fault to test number defined by INJECT_ERROR_TO_TEST_NUM */
  /* Uncomment the following line to allow error injection */
  /*m4_scst_fault_inject_test_index = INJECT_ERROR_TO_TEST_NUM;*/

  /* Execute all tests */
  result = m4_scst_execute_core_tests(0U,43U);

  if(result == M4_SCST_WRONG_RANGE)
  {
    /* If the test range was incorrectly set, stop the execution flow */
    while(1);
  }
  else
  {
    /* All tests passed */
    if (m4_scst_accumulated_signature == ACCUMULATED_SIGNATURE_ID0_43)
    {
       /* Turn on GREEN LED to signal that the test was successful */
       PINS_DRV_ClearPins(GPIO_PORT, 1 << LED2);
       PINS_DRV_SetPins(GPIO_PORT, 1 << LED1);
    }
    /* Test was interrupted */
    else if (m4_scst_accumulated_signature == M4_SCST_TEST_WAS_INTERRUPTED)
    {
      /* Check the m4_scst_last_executed_test_number to verify which test was interrupted */
      /* Turn on RED LED to signal error */
      PINS_DRV_SetPins(GPIO_PORT, 1 << LED2);
      PINS_DRV_ClearPins(GPIO_PORT, 1 << LED1);
    }
    /* Some test failed or error injection was applied */
    else if ((m4_scst_accumulated_signature != 0x929940F1U) && (m4_scst_accumulated_signature != M4_SCST_TEST_WAS_INTERRUPTED))
    {
      if (m4_scst_last_executed_test_number == INJECT_ERROR_TO_TEST_NUM)
      {
        /* Stop here in case of error injection works correctly */
        /* Turn on GREEN LED to signal that the test was successful */
        PINS_DRV_ClearPins(GPIO_PORT, 1 << LED2);
        PINS_DRV_SetPins(GPIO_PORT, 1 << LED1);
      }
      else
      {
        /* Stop here in case of error injection does not work correctly */
        /* Turn on RED LED to signal error */
        PINS_DRV_SetPins(GPIO_PORT, 1 << LED2);
        PINS_DRV_ClearPins(GPIO_PORT, 1 << LED1);
      }
    }
  }

  /*** Don't write any code pass this line, or it will be deleted during code generation. ***/
  /*** RTOS startup code. Macro PEX_RTOS_START is defined by the RTOS component. DON'T MODIFY THIS CODE!!! ***/
  #ifdef PEX_RTOS_START
    PEX_RTOS_START();                  /* Startup of the selected RTOS. Macro is defined by the RTOS component. */
  #endif
  /*** End of RTOS startup code.  ***/
  /*** Processor Expert end of main routine. DON'T MODIFY THIS CODE!!! ***/
  for(;;) {
    if(exit_code != 0) {
      break;
    }
  }
  return exit_code;
  /*** Processor Expert end of main routine. DON'T WRITE CODE BELOW!!! ***/
} /*** End of main routine. DO NOT MODIFY THIS TEXT!!! ***/

/* END main */
/*!
** @}
*/
/*
** ###################################################################
**
**     This file was created by Processor Expert 10.1 [05.21]
**     for the Freescale S32K series of microcontrollers.
**
** ###################################################################
*/
