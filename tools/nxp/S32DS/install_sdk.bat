rem This file installs the SDK stored within this project repo
rem into an S32 Eclipse IDE.

set sdk_name=S32_SDK_RTM_3.0.0

set mypath=%~dp0
set curr_dir=%mypath:~0,-1%
set sdk_repo_dir="C:\nxp\S32DS_ARM_v2018.R1\eclipse\ProcessorExpert\Config\Repositories"
set examples_dir="%curr_dir%\%sdk_name%\examples"
set eclipse_examples_dir="C:\nxp\S32DS_ARM_v2018.R1\eclipse\plugins\com.nxp.s32ds.arm.examples.descriptor_1.0.0.201811172020\Examples"

rem Install SDK(s) by copying over all .cfg files into the eclipse
rem Processor Export directory, skipping files if they already exist.
robocopy %curr_dir% %sdk_repo_dir% *.cfg

rem Since having the examples show up in eclipse requires features to be
rem installed, we will instead hijack an already-installed feature and
rem include our examples there.

rem Copy examples from SDK over into ARM default examples, skipping files
rem if they already exist
robocopy %examples_dir% %eclipse_examples_dir% /s /e

