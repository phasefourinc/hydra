This file explains how to install the SDK that is stored within this project.

Current SDK Version: RTM 3.0.1 (3.0.0 with patch)

The entire SDK and examples are stored within this repository. Therefore it
is recommended users do not attempt to install a similar SDK version via
eclipse on the C:\ drive in order to prevent any possibility that S32 gets
confused and uses the wrong version.  The project should be configured
that this doesn't happen anyway, but it's best to be safe.


##########################################################################
SDK Installation
##########################################################################
 - Install S32 IDE for ARM (if not already installed)
 - Ensure S32 IDE has the latest updates: Help->Check for Updates
 - Close S32 IDE
 - Mount the project repo M:\ drive
 - Run install_sdk.bat
 - BOOM done!

Notes:
 - SDK examples are found under "S32DS Example Projects" because it's
   easier to hijack an eclipse "feature" that already exists than to create
   a new one.
 - If creating a new S32 application, the user should select another
   SDK (e.g. EAR_0.8.6) and then after the project has been loaded,
   convert it to this SDK through Project Properties via
   Properties->Processor Expert->S32 SDK Specific and pointing to the
   SDK installed on the M:\ drive.



##########################################################################
Configuring a new SDK
##########################################################################

Hopefully this doesn't need to happen, but if it does, here's what is
needed to install a new SDK in the project repo.

1. Close S32 IDE if open
2. Get the SDK from NXP
3. Install the SDK to the location C:\nxp\S32DS_ARM_v2018.R1\S32DS\<new_sdk>
4. Install any patches that come with and point to the correct install location above
5. Copy entire SDK to M:\tools\nxp\S32DS\<new_sdk>
6. Since the SDK is large, might be worth pruning unnecessary MCU examples and documentation
7. Uninstall SDK via Windows Add/Remove programs, ensure entire SDK is deleted on C:\ drive
8. Create a new Repository.cfg file in this directory, using the current one as an example
9. Since NXP SDK installer configures the SDK at a hardcoded path on the C:\ drive, this
   means that all example projects will use this path.
   
   You have 2 options:
   EITHER
     1) Do nothing and fix the paths manually later after importing the
        example project. This is a viable option if you don't intend to open any
        examples or don't want to muck with the SDK and risk changing something
        undesirable.  Areas to manually fix in Project Properties for this
        approach include:
         a) Processor Expert path (doing so should auto update PROJECT_KSDK_PATH)
         b) S32_SDK_PATH value set under Linked Resources
         c) S32_SDK_PATH value set under each Build environment variables
   OR
     2) Just fix it now using a one-liner sed script, which replaces all instances of
        the C:\ path with the new M:\ path:
         a) Make a backup copy of M:\tools\nxp\S32DS\<new_sdk> just in case things gloriously fail
         b) Open bash terminal and cd into M:\tools\nxp\S32DS\<new_sdk>
         c) Run the following line, replacing all instances of S32_SDK_RTM_3.0.0 with <new_sdk>

         grep -rl "S32_SDK_RTM_3.0.0" ./ | xargs sed -i 's#C:/nxp/S32DS_ARM_v2018.R1/S32DS/S32_SDK_RTM_3.0.0#M:/tools/nxp/S32DS/S32_SDK_RTM_3.0.0#g'

10. Update install_sdk.bat with new sdk_name variable
11. Run install_sdk.bat
12. Open S32, open the M:\ drive project and update with new SDK by adjusting the
    following project properties:
     a) Processor Expert path (doing so should auto update PROJECT_KSDK_PATH)
     b) S32_SDK_PATH value set under Linked Resources
     c) S32_SDK_PATH value set under each Build environment variables

Hopefully at this point things are in good working order. If there are issues,
it's likely that eclipse cannot find the new SDK due to path issues.  One common
one is if the path in the .cfg file does not exactly match (including '/' or '\')
the paths in the ProcessorExpert.pe file. Another common one is that S32_SDK_PATH
under Linked Resources does not point to the correct location. Good luck to you!
